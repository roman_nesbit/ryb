<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");

if($_REQUEST['q'] !== '') {
	// Split search text by words
	$searchWords = explode(' ', $_REQUEST['q']);
	// Make filter for every word
	$filter = [];
	foreach($searchWords as $searchWord) {
		$filter[] = [
			'SEARCHABLE_CONTENT' => '%' . toupper($searchWord) . '%'
		];
	}

	$APPLICATION->IncludeComponent(
		"bfs:catalog.filtered", 
		".default", 
		array(
			"COMPONENT_TEMPLATE" => ".default",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"PAGE_COUNT" => "15",
			"FILTER" => json_encode($filter),
			"SORT_FIELD" => "SHOW_COUNTER",
			"SORT_FIELD2" => "",
			"SORT_ORDER" => "DESC",
			"SORT_ORDER2" => "",
			"TITLE" => "Результаты поиска",
			"SHOW_FILTER" => "Y",
		),
		false
	);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");