<?php

/**
 * PURPOSE OF THE CODE
 *
 * This code was created in order not to lose traffic to the old product pages of rybalka.ua web shop
 */

use Bitrix\Main\Loader;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
const SHOP_ID_FOR_RYBALKA_UA = 5;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("iblock");

$offerId = intval($_GET['id']);

if (empty($offerId)) {
    LocalRedirect('/catalog/');
}

$filter = [
    'IBLOCK_ID' => InfoBlock::CATALOG,
    'PROPERTY_IMPORT_OFFER_ID' => $offerId,
    'PROPERTY_PARTNER' => SHOP_ID_FOR_RYBALKA_UA,
];

$result = CIBlockElement::GetList(
    [],
    $filter,
    false,
    false,
    ['CODE']
);

if ($offer = $result->Fetch()) {
    LocalRedirect('/p/' . urlencode($offer['CODE']) . '/');
} else {
    LocalRedirect('/404.php');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");