<?

use Bitrix\Main\Application;

define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?>
<header class="simpleHeader">
    <div class="container">
        <a href="/" class="logo">
            <img src="/local/templates/rybalka/images/logo.svg" alt="" />
        </a>
    </div>
</header>
<div id="root"></div>
<div class="footLine"></div>
<?php
    $checkoutJsRelativePath = '/personal/order/make/checkout.min.js';
    $checkoutJsPath = Application::getDocumentRoot() . $checkoutJsRelativePath;
    $modificationTime = is_file($checkoutJsPath)
        ? filemtime($checkoutJsPath)
        : md5('wtf');
?>
<script src="<?=$checkoutJsRelativePath . '?v=' . $modificationTime?>"></script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>