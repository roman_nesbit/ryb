<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

// exo($_REQUEST);

$APPLICATION->AddChainItem($_REQUEST['order_id']);

$APPLICATION->IncludeComponent("bitrix:sale.personal.order.detail",
	"detail",
	Array(
        "PATH_TO_LIST" => "/personal/order/",
        "PATH_TO_CANCEL" => "/personal/order/#ID#/cancel/",
        "PATH_TO_PAYMENT" => "/personal/order/payment/",
        "PATH_TO_COPY" => "",
        "ID" => $_REQUEST['order_id'],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "PICTURE_WIDTH" => "110",
        "PICTURE_HEIGHT" => "110",
        "PICTURE_RESAMPLE_TYPE" => "1",
        "CUSTOM_SELECT_PROPS" => [],
        "PROP_1" => [],
        "PROP_2" => [],
        "AUTH_FORM_IN_TEMPLATE" => "N",
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>