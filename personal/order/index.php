<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Main\ORM\Query;
use Bitrix\Main\Entity;
use \Bitrix\Sale\Internals\OrderTable;

if($_REQUEST['qo']) {
	// знайдемо номери замовлень, де є строка в номері, або в назві товара, або в назві магазина
	$result = OrderTable::getList([
		'select' => [
			'ACCOUNT_NUMBER',
			'PARTNER_ID' => 'ORDER_PROP.VALUE',
			'PARTNER' => 'ORDER_PARTNER.NAME',
		],
		'filter' => [
			'LID' => SITE_ID,
			'USER_ID' => $USER->getId(),
			[
				'LOGIC' => 'OR',
				'ACCOUNT_NUMBER' => '%' . $_REQUEST['qo'] . '%',
				'BASKET_ITEM.NAME' => '%' . $_REQUEST['qo'] . '%',
				'PARTNER' => '%' . $_REQUEST['qo'] . '%',
			],
			'ORDER_PROP.CODE' => 'PARTNER',
		],
		'runtime' => [
			new Entity\ReferenceField(
				'BASKET_ITEM',
				'\Bitrix\Sale\Internals\BasketTable',
				Query\Join::on('this.ID', 'ref.ORDER_ID')
			),
			new Entity\ReferenceField(
				'ORDER_PROP',
				'\Bitrix\Sale\Internals\OrderPropsValueTable',
				Query\Join::on('this.ID', 'ref.ORDER_ID')
			),
			new Entity\ReferenceField(
				'ORDER_PARTNER',
				'\Bitrix\Iblock\ElementTable',
				Query\Join::on('this.PARTNER_ID', 'ref.ID')
			),
		],
		'group' => 'ACCOUNT_NUMBER',
	]);
	while($order = $result->fetch()) {
		// exo($order);
		$_REQUEST['filter_id'][] = $order['ACCOUNT_NUMBER'];
	}
	// якщо нічого не знайшли
	if(!isset($_REQUEST['filter_id'])) {
		$_REQUEST['filter_id'] = '-';
	}
}

// to show all orders
$_REQUEST['show_all'] = 'Y';

$APPLICATION->IncludeComponent(
	"bfs:sale.personal.order.list", 
	"list", 
	array(
		"PATH_TO_DETAIL" => "#ID#/",
		"PATH_TO_COPY" => "basket.php",
		"PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PAYMENT" => "payment.php",
		"ORDERS_PER_PAGE" => "10",
		"ID" => $ID,
		"SET_TITLE" => "Y",
		"SAVE_IN_SESSION" => "Y",
		"NAV_TEMPLATE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"HISTORIC_STATUSES" => array(
		),
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"COMPONENT_TEMPLATE" => "list",
		"PATH_TO_CATALOG" => "/catalog/",
		"DISALLOW_CANCEL" => "Y",
		"RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "0",
		),
		"REFRESH_PRICES" => "N",
		"DEFAULT_SORT" => "DATE_INSERT",
		"AUTH_FORM_IN_TEMPLATE" => "N"
	),
	false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>