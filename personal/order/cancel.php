<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->IncludeComponent("bitrix:sale.personal.order.cancel",
    "",
    Array(
        "PATH_TO_LIST" => "/personal/order/",
        "PATH_TO_DETAIL" => "/personal/order/#ID#/",
        "ID" => $_REQUEST['order_id'],
        "SET_TITLE" => "Y"
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>