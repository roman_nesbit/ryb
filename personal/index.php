<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кабинет");
?><section class="personalData">
	<h1 class="pageTitle noBorders"><?=$APPLICATION->GetTitle()?></h1>
	<div class="innerPage twoCells"><?
		$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"personal",
			Array(
				"ROOT_MENU_TYPE" => "personal",
				"MAX_LEVEL" => "1", 
				"CHILD_MENU_TYPE" => "",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N",
				"MENU_CACHE_TYPE" => "A", 
				"MENU_CACHE_TIME" => "36000000", 
				"MENU_CACHE_USE_GROUPS" => "N", 
				"MENU_CACHE_GET_VARS" => "" 
			)
		);
		?><div class="innerWrap">
		</div>
	</div>
</section><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>