<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личные данные");
?>
<section class="personalData">
	<h1 class="pageTitle noBorders"><?=$APPLICATION->GetTitle()?></h1>
	<div class="innerPage twoCells">
		<? $APPLICATION->IncludeComponent(
			"bitrix:menu",
			"personal",
			Array(
				"ROOT_MENU_TYPE" => "personal",
				"MAX_LEVEL" => "1", 
				"CHILD_MENU_TYPE" => "",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N",
				"MENU_CACHE_TYPE" => "A", 
				"MENU_CACHE_TIME" => "36000000", 
				"MENU_CACHE_USE_GROUPS" => "N", 
				"MENU_CACHE_GET_VARS" => "", 
				"HIDE_BASE_CLASS" => "N",
			)
		);?>

		<div class="innerWrap">
			<div class="innerContent">
			<? $APPLICATION->IncludeComponent(
				"bitrix:main.profile",
				"personal",
				array(
					"USER_PROPERTY_NAME" => "",
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"USER_PROPERTY" => [],
					"SEND_INFO" => "Y",
					"CHECK_RIGHTS" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"FORM_ID" => "changePersonalData",
					"FORM_RESET" => "N",
				)
			);
			$APPLICATION->IncludeComponent(
				"bitrix:main.profile",
				"personal",
				array(
					"USER_PROPERTY_NAME" => "",
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"USER_PROPERTY" => ['UF_CITY_ID', 'UF_NP_OFFICE_ID'],
					"SEND_INFO" => "Y",
					"CHECK_RIGHTS" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"FORM_ID" => "changeDelivery",
					"FORM_RESET" => "N",
				)
			);
			$APPLICATION->IncludeComponent(
				"bitrix:main.profile",
				"personal",
				array(
					"USER_PROPERTY_NAME" => "",
					"SET_TITLE" => "N",
					"AJAX_MODE" => "N",
					"USER_PROPERTY" => [],
					"SEND_INFO" => "Y",
					"CHECK_RIGHTS" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"FORM_ID" => "changePassword",
					"FORM_RESET" => "Y",
				)
			);?>
			</div>
		</div>
	</div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>