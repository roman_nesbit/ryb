<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->IncludeComponent(
	"h2o:favorites.list", 
	"favorites", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"FAVORITES_COUNT" => "10",
		"SORT_BY" => "DATE_INSERT",
		"SORT_ORDER" => "DESC",
		"FILTER_NAME" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DATE_FORMAT" => "d.m.Y, h:i",
		"SET_TITLE" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"NAV_TEMPLATE" => ""
	),
	false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>