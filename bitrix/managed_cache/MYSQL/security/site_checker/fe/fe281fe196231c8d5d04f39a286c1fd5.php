<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001581977459';
$dateexpire = '001584569459';
$ser_content = 'a:2:{s:7:"CONTENT";s:0:"";s:4:"VARS";a:2:{s:7:"results";a:9:{i:0;a:5:{s:5:"title";s:78:"Не удалось выполнить внешнее сканирование";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:87:"Сканер безопасности не смог проверить ваш сайт.";s:14:"recommendation";s:165:"Дать рекомендации не представляется возможным в связи с большой вариантивностью проблем.";s:15:"additional_info";s:1287:"Причина: Непредвиденный код состояния HTTP<br>Адрес: <a href="https://marketplace.rybalka.ua/?rnd=0.785548387558" target="_blank">https://marketplace.rybalka.ua/?rnd=0.785548387558</a><br>Запрос/Ответ: <pre>GET /?rnd=0.785548387558 HTTP/1.1
host: marketplace.rybalka.ua
accept: */*
user-agent: BitrixCloud BitrixSecurityScanner/Robin-Scooter



HTTP/1.1 404
Access-Control-Allow-Headers: Content-Type, Authorization-Token
Pragma: no-cache
X-Powered-By: PHP/7.1.18
Transfer-Encoding: chunked
Set-Cookie: PHPSESSID=02bd084d53190ed5cd6900cd6697f0ab; path=/; domain=marketplace.rybalka.ua; HttpOnly
Set-Cookie: BITRIX_SM_ABTEST_s1=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/; domain=marketplace.rybalka.ua
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Server: nginx/1.4.1 (Ubuntu)
Connection: keep-alive
X-Powered-CMS: Bitrix Site Manager (944b60c156d8dd045c3175f92bf063e8)
Cache-Control: no-store, no-cache, must-revalidate
Date: Fri, 30 Nov 2018 08:04:59 GMT
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS
Content-Type: text/html; charset=UTF-8
P3P: policyref=&quot;/bitrix/p3p.xml&quot;, CP=&quot;NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA&quot;

</pre>";}i:1;a:5:{s:5:"title";s:128:"Уровень безопасности административной группы не является повышенным";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:182:"Пониженный уровень безопасности административной группы может значительно помочь злоумышленнику";s:14:"recommendation";s:337:"Ужесточить <a href="/bitrix/admin/group_edit.php?ID=1&tabControl_active_tab=edit2"  target="_blank">политики безопасности административной</a> группы или выбрать предопределенную настройку уровня безопасности "Повышенный".";s:15:"additional_info";s:0:"";}i:2;a:5:{s:5:"title";s:61:"Включен расширенный вывод ошибок";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:126:"Расширенный вывод ошибок может раскрыть важную информацию о ресурсе";s:14:"recommendation";s:63:"Выключить в файле настроек .settings.php";s:15:"additional_info";s:0:"";}i:3;a:5:{s:5:"title";s:77:"Используются устаревшие модули платформы";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:55:"Доступны новые версии модулей";s:14:"recommendation";s:275:"Рекомендуется своевременно обновлять модули платформы, установить рекомендуемые обновления: <a href="/bitrix/admin/update_system.php" target="_blank">Обновление платформы</a>";s:15:"additional_info";s:141:"Модули для которых доступны обновления:<br>main<br />
forum<br />
catalog<br />
sale<br />
ui<br />
landing";}i:4;a:5:{s:5:"title";s:291:"Обнаружено как минимум 1 файлов или директорий с доступом на запись для всех пользователей окружения в котором работает веб-сервер (не пользователей Bitrix Framework)";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:275:"Право на запись у всех системных пользователей может служить причиной полной компрометации ресурса, путем модификации исходного кода вашего проекта";s:14:"recommendation";s:119:"Необходимо отобрать лишние права у всех системных пользователей";s:15:"additional_info";s:64:"Последние 1 файлов/директорий:<br>/logs";}i:5;a:5:{s:5:"title";s:142:"Директория хранения файлов сессий доступна для всех системных пользователей";s:8:"critical";s:5:"HIGHT";s:6:"detail";s:180:"Это может позволить читать/изменять сессионные данные, через скрипты других виртуальных серверов";s:14:"recommendation";s:265:"Корректно настроить файловые права или сменить директорию хранения либо включить хранение сессий в БД: <a href="/bitrix/admin/security_session.php">Защита сессий</a>";s:15:"additional_info";s:92:"Директория хранения сессий: /tmp/sessions<br>
Права: drwxrwxrwx";}i:6;a:5:{s:5:"title";s:68:"Разрешено чтение файлов по URL (URL wrappers)";s:8:"critical";s:6:"MIDDLE";s:6:"detail";s:256:"Если эта, сомнительная, возможность PHP не требуется - рекомендуется отключить, т.к. она может стать отправной точкой для различного типа атак";s:14:"recommendation";s:89:"Необходимо в настройках php указать:<br>allow_url_fopen = Off";s:15:"additional_info";s:0:"";}i:7;a:5:{s:5:"title";s:110:"Установлен не корректный порядок формирования массива _REQUEST";s:8:"critical";s:6:"MIDDLE";s:6:"detail";s:392:"Зачастую в массив _REQUEST нет необходимости добавлять любые переменные, кроме массивов _GET и _POST. В противном случае это может привести к раскрытию информации о пользователе/сайте и иным не предсказуемым последствиям.";s:14:"recommendation";s:88:"Необходимо в настройках php указать:<br>request_order = "GP"";s:15:"additional_info";s:75:"Текущее значение: ""<br>Рекомендованное: "GP"";}i:8;a:5:{s:5:"title";s:119:"Временные файлы хранятся в пределах корневой директории проекта";s:8:"critical";s:6:"MIDDLE";s:6:"detail";s:271:"Хранение временных файлов, создаваемых при использовании CTempFile, в пределах корневой директории проекта не рекомендовано и несет с собой ряд рисков.";s:14:"recommendation";s:883:"Необходимо определить константу "BX_TEMPORARY_FILES_DIRECTORY" в "bitrix/php_interface/dbconn.php" с указанием необходимого пути.<br>
Выполните следующие шаги:<br>
1. Выберите директорию вне корня проекта. Например, это может быть "/home/bitrix/tmp/www"<br>
2. Создайте ее. Для этого выполните следующую комманду:
<pre>
mkdir -p -m 700 /полный/путь/к/директории
</pre>
3. В файле "bitrix/php_interface/dbconn.php" определите соответствующую константу, чтобы система начала использовать эту директорию:
<pre>
define("BX_TEMPORARY_FILES_DIRECTORY", "/полный/путь/к/директории");
</pre>";s:15:"additional_info";s:61:"Текущая директория: /var/www/html/upload/tmp";}}s:9:"test_date";s:10:"30.11.2018";}}';
return true;
?>