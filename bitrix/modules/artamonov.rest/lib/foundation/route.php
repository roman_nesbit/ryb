<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


class Route
{
    private static $_instance;
    private static $_route;
    private static $_path;

    public function get()
    {
        return self::$_route;
    }

    public function path()
    {
        return self::$_path;
    }

    public function isActive()
    {
        return self::$_route['active'] === false ? false : true;
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$_path = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/');
            if (config()->get('pathRestApi') !== helper()->ROOT()) {
                self::$_path = str_replace(config()->get('pathRestApi') . '/', '', self::$_path);
            }
            $dir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'routes';
            $files[$dir] = array_diff(scandir($dir), ['..', '.']);
            if (config()->get('localRouteMap')) {
                $dir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . config()->get('localRouteMap');
                $files[$dir] = array_diff(scandir($dir), ['..', '.']);
            }
            foreach ($files as $dir => $items) {
                foreach ($items as $file) {
                    $file = $dir . DIRECTORY_SEPARATOR . $file;
                    if (is_array($ar = require $file)) {
                        foreach ($ar as $type => $routes) {
                            if ($_SERVER['REQUEST_METHOD'] === $type) {
                                foreach ($routes as $route => $config) {
                                    $route = trim($route, '/');
                                    if ($route === self::$_path) {
                                        self::$_route = $config;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
