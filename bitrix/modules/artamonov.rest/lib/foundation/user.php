<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


class User
{
    private static $_instance;

    public function getIdByToken($token = '')
    {
        return \Bitrix\Main\UserTable::getList(['select' => ['ID'], 'filter' => ['=' . settings()->getTokenFieldCode() => $token ? $token : $this->get('token')], 'limit' => 1])->fetch()['ID'];
    }

    public function generateToken($id = '', $login = '')
    {
        $token = md5($id . '-' . $login . '=' . date('Y-m-dH:i:s') . helper()->generateUniqueCode());
        $token = str_split($token, 8);
        $token = implode('-', $token);
        return $token;
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
