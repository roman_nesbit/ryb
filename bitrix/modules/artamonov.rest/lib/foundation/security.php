<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


class Security
{
    private static $_instance;
    private $errors;

    /**
     * Получение настроек безопасности из настроек роута
     *
     * @return mixed
     */
    public function get()
    {
        return route()->get()['security'];
    }

    /**
     * Проверка активности роута
     *
     * @return bool
     */
    public function isActive()
    {
        return route()->isActive();
    }

    /**
     * Проверка наличия контроллера для входящего запроса
     *
     * @return bool
     */
    public function hasController()
    {
        return route()->get()['controller'] ? true : false;
    }

    /**
     * Проверка соответствия типа контента входящего запроса настройкам роута
     *
     * @return bool
     */
    public function isValidContentType()
    {
        return (route()->get()['contentType'] && strpos($_SERVER['CONTENT_TYPE'], route()->get()['contentType']) !== 0) ? false : true;
    }

    /**
     * Проверка соответствия параметров входящего запроса настройкам роута
     *
     * @return bool
     */
    public function isValidParameters()
    {
        if (!request()->map()['parameters']) return true;
        // Validation of request parameters
        if (request()->method() === helper()->GET()) {
            // Simple request
            foreach (request()->map()['parameters'] as $code => $ar) {
                $this->checkParameter(request()->get(), $code, $ar['required'], $ar['type']);
            }
        } else {
            // The check differs in that there is a request body
            foreach (request()->map()['parameters'] as $codeOne => $arOne) {
                // Checking first level
                $this->checkParameter(request()->get(), $codeOne, $arOne['required'], $arOne['type']);
                // Checking second level
                if (is_array($arOne['parameters'])) {
                    foreach ($arOne['parameters'] as $codeTwo => $arTwo) {
                        // If: param => value
                        if (is_string($codeTwo)) {
                            $this->checkParameter(request()->get($codeOne), $codeTwo, $arTwo['required'], $arTwo['type'], $codeOne);
                        } // If: params => array
                        elseif (is_integer($codeTwo) && is_array($arOne['parameters'][$codeTwo])) {
                            // Checking each element of the array
                            for ($i = 0, $c = count(request()->get($codeOne)); $i < $c; $i++) {
                                $index = $i + 1;
                                // Getting rule from map for one element
                                foreach ($arOne['parameters'][$codeTwo] as $parameterCodeTwo => $parameterArTwo) {
                                    $this->checkParameter(request()->get($codeOne)[$i], $parameterCodeTwo, $parameterArTwo['required'], $parameterArTwo['type'], $codeOne, $index);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->errors ? false : true;
    }

    /**
     * Проверка авторизации при доступе к интерфейсу
     *
     * @return bool
     */
    public function isAuthorized()
    {
        if (!$this->get()['auth']['required']) return true;
        if (!$this->get()['auth']['type']) return false;
        if ($this->get()['auth']['type'] === helper()->token()) return $this->checkAccessByToken();
        if ($this->get()['auth']['type'] === helper()->login()) return $this->checkAccessByLoginPassword();
        return true;
    }

    /**
     * Проверка доступа к интерфейсу используя токен
     *
     * @return bool
     */
    private function checkAccessByToken()
    {
        if (!config()->get('useToken') || !request()->header('authorization-token')) return false;
        $token = request()->header('authorization-token');
        // Token key
        if (config()->get('tokenKey')) {
            $token = explode(':', $token);
            $keyword = $token[0];
            $token = $token[1];
            if (config()->get('tokenKey') !== $keyword) return false;
        }
        // Whitelist
        if ($this->get()['token']['whitelist'] && !in_array($token, $this->get()['token']['whitelist'])) return false;
        // Filter
        $filter = ['=' . settings()->getTokenFieldCode() => $token];
        // Expire
        if (!$this->get()['token']['whitelist'] && $this->get()['token']['checkExpire'] === true) {
            $filter['>' . settings()->getTokenExpireFieldCode()] = date('d.m.Y H:i:s');
        }
        // User
        if (!$user = \Bitrix\Main\UserTable::getList(['select' => ['*', 'UF_*'], 'filter' => $filter, 'limit' => 1])->fetchRaw()) return false;
        $userGroups = \CUser::GetUserGroup($user['ID']);
        // Whitelist groups
        if (!$this->get()['token']['whitelist'] && $this->get()['group']['whitelist'] && !array_intersect($userGroups, $this->get()['group']['whitelist'])) return false;
        // Requests
        $this->requestLimit($token, $userGroups);
        // Save user
        request()->set('_user', $user);
        // Return
        return true;
    }

    /**
     * Проверка доступа к интерфейсу используя логин и пароль
     *
     * @return bool
     */
    private function checkAccessByLoginPassword()
    {
        if (!config()->get('useLoginPassword')) return false;
        $login = false;
        $password = false;
        $passwordCorrect = false;
        // Login, password
        if (request()->header('authorization-login') && request()->header('authorization-password')) {
            $login = request()->header('authorization-login');
            $password = request()->header('authorization-password');
        } elseif ($_SERVER['PHP_AUTH_USER'] && $_SERVER['PHP_AUTH_PW']) {
            $login = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];
        }
        // Whitelist
        if ($this->get()['login']['whitelist'] && !in_array($login, $this->get()['login']['whitelist'])) return false;
        // User
        if ($user = \Bitrix\Main\UserTable::getList(['select' => ['*', 'UF_*'], 'filter' => ['=LOGIN' => $login], 'limit' => 1])->fetchRaw()) {
            $length = strlen($user['PASSWORD']);
            if ($length > 32) {
                $salt = substr($user['PASSWORD'], 0, $length - 32);
                $db_password = substr($user['PASSWORD'], -32);
            } else {
                $salt = '';
                $db_password = $user['PASSWORD'];
            }
            $user_password = md5($salt . $password);
            if ($user_password === $db_password) {
                $passwordCorrect = true;
            }
        }
        if (!$passwordCorrect) return false;
        $userGroups = \CUser::GetUserGroup($user['ID']);
        // Whitelist groups
        if (!$this->get()['login']['whitelist'] && $this->get()['group']['whitelist'] && !array_intersect($userGroups, $this->get()['group']['whitelist'])) return false;
        // Requests
        $this->requestLimit($login, $userGroups);
        // Save user
        request()->set('_user', $user);
        // Return
        return true;
    }

    /**
     * Проверка ограничения количества запросов
     *
     * @param $clientId
     * @param $groups
     * @return bool
     */
    private function requestLimit($clientId, $groups)
    {
        if (!config()->get('useRequestLimit')) return true;
        $requestLimitGroup = json_decode(config()->get('requestLimit'), true);
        if ($intersect = array_intersect($groups, array_keys($requestLimitGroup))) {
            $tmp = [];
            foreach ($intersect as $id) {
                $tmp[$id] = $requestLimitGroup[$id];
            }
            $requestLimitGroup = helper()->sortByMultipleKey($tmp, ['number' => 'desc', 'period' => 'desc'])[0];
            $dateFrom = date('Y-m-d H:i:s', strtotime('now-' . $requestLimitGroup['period'] . 'seconds'));
            $dateTo = date('Y-m-d H:i:s');
            $number = journal()->getRequestLimitNumber($clientId, $dateFrom, $dateTo);
            if ($number >= $requestLimitGroup['number']) {
                response()->tooManyRequests();
            }
            journal()->add('request-limit', ['CLIENT_ID' => $clientId]);
        }
    }

    /**
     * Используется для определения доступа к странице модуля в админ. разделе
     *
     * @param $groups
     * @param $userGroups
     * @return array
     */
    public function checkAccessByGroups($groups, $userGroups)
    {
        $groups[] = helper()->adminGroupId();
        return array_intersect($groups, $userGroups);
    }

    /**
     * Используется для проверки параметров запроса
     *
     * @param $params
     * @param $code
     * @param $required
     * @param $type
     * @param bool $parent
     * @param bool $index
     */
    private function checkParameter($params, $code, $required, $type, $parent = false, $index = false)
    {
        if ($required && !array_key_exists($code, $params)) {
            if ($parent) {
                if ($index !== false) {
                    $this->errors['badParameters'][$parent][$index][$code]['required'] = $required;
                } else {
                    $this->errors['badParameters'][$parent][$code]['required'] = $required;
                }
            } else {
                $this->errors['badParameters'][$code]['required'] = $required;
            }
        } else if (array_key_exists($code, $params)) {
            $isType = 'is' . $type;
            $value = $params[$code];
            if (!helper()->$isType($value)) {
                if ($parent) {
                    if ($index !== false) {
                        $this->errors['badParameters'][$parent][$index][$code]['invalidValueType'] = $value;
                        $this->errors['badParameters'][$parent][$index][$code]['valueTypeMustBe'] = $type;
                    } else {
                        $this->errors['badParameters'][$parent][$code]['invalidValueType'] = $value;
                        $this->errors['badParameters'][$parent][$code]['valueTypeMustBe'] = $type;
                    }
                } else {
                    $this->errors['badParameters'][$code]['invalidValueType'] = $value;
                    $this->errors['badParameters'][$code]['valueTypeMustBe'] = $type;
                }
            }
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
