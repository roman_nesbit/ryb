<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


class Settings
{
    private static $_instance;
    private static $_settings;

    public function get($code = '')
    {
        return $code ? self::$_settings[$code] : self::$_settings;
    }

    public function getTokenFieldCode()
    {
        return config()->get('tokenFieldCode') ? config()->get('tokenFieldCode') : self::$_settings['config']['token']['code'];
    }

    public function getTokenExpireFieldCode()
    {
        return self::$_settings['config']['token']['expire']['code'];
    }

    public function getTokenExpire()
    {
        $lifetime = config()->get('tokenLifetime') ? config()->get('tokenLifetime') . ' seconds' : self::$_settings['config']['token']['expire']['lifetime'];
        return date('d.m.Y H:i:s', strtotime('+' . $lifetime));
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$_settings = require __DIR__ . '/../../settings.php';
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
