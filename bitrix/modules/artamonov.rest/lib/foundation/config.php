<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use CUserTypeEntity;

class Config
{
    private static $_instance;
    private $parameters;
    private $data;

    public function get($code)
    {
        if (!is_array($this->parameters)) {
            $result = Application::getConnection()->query('SELECT NAME, VALUE FROM b_option WHERE MODULE_ID="' . Settings::getInstance()->get('module')['id'] . '"');
            while ($parameter = $result->fetchRaw()) {
                $this->parameters[$parameter['NAME']] = $parameter['VALUE'];
            }
        }
        return $this->parameters[$code];
    }

    public function save()
    {
        $this->prepare();
        foreach ($this->data as $code => $value) {
            Option::set(Settings::getInstance()->get('module')['id'], $code, $value, false);
            $this->parameters[$code] = $value;
        }
        $this->addTokenField();
    }

    public function restore()
    {
        $this->prepare();
        foreach ($this->data as $code => $value) {
            Option::delete(Settings::getInstance()->get('module')['id'], ['name' => $code]);
            unset($this->parameters[$code]);
        }
    }

    private function prepare()
    {
        switch ($_POST['form']) {

            case 'rest-api-config':
                // Checkboxes
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useRestApi'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useRestApi'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useLateStart'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useLateStart'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useJournal'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useJournal'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'showExamples'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'showExamples'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useExampleRoute'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useExampleRoute'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useNativeRoute'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useNativeRoute'] = false;
                }
                // Arrays
                if (isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'accessDocumentation'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessDocumentation'] = implode('|', $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessDocumentation']);
                } else {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessDocumentation'] = false;
                }
                if (isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSecurity'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSecurity'] = implode('|', $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSecurity']);
                } else {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSecurity'] = false;
                }
                if (isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'accessJournal'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessJournal'] = implode('|', $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessJournal']);
                } else {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessJournal'] = false;
                }
                if (isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSupport'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSupport'] = implode('|', $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSupport']);
                } else {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'accessSupport'] = false;
                }
                // Strings
                if (!empty($_POST[Settings::getInstance()->get('config')['prefix'] . 'pathRestApi'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'pathRestApi'] = str_replace(' ', '', trim($_POST[Settings::getInstance()->get('config')['prefix'] . 'pathRestApi'], '/'));
                }
                if (!empty($_POST[Settings::getInstance()->get('config')['prefix'] . 'localRouteMap'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'localRouteMap'] = str_replace(' ', '', trim($_POST[Settings::getInstance()->get('config')['prefix'] . 'localRouteMap'], '/'));
                }
                break;

            case 'rest-api-security':
                // Checkboxes
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useLoginPassword'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useLoginPassword'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useToken'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useToken'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useRequestLimit'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useRequestLimit'] = false;
                }
                if (!isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'useCorsFilter'])) {
                    $_POST[Settings::getInstance()->get('config')['prefix'] . 'useCorsFilter'] = false;
                }
                if (isset($_POST[Settings::getInstance()->get('config')['prefix'] . 'corsListDomains'])) {
                    $list = &$_POST[Settings::getInstance()->get('config')['prefix'] . 'corsListDomains'];
                    $list = str_replace([' ', "\n\r", "\n", "\r"], '', $list);
                    $list = explode(';', $list);
                    foreach ($list as &$item) {
                        $item = rtrim($item, '/');
                    }
                    $list = implode(";\n", $list);
                }
                // Prepare request limit parameters
                $_POST[Settings::getInstance()->get('config')['prefix'] . 'requestLimit'] = [];
                foreach ($_POST as $key => $value) {
                    if (strpos($key, 'data:requestLimit') !== false) {
                        $code = explode('-', strtolower(str_replace('data:requestLimit', '', $key)));
                        $id = $code[1];
                        $code = $code[0];
                        $_POST[Settings::getInstance()->get('config')['prefix'] . 'requestLimit'][$id][$code] = $value;
                        unset($_POST[$key]);
                    }
                }
                $_POST[Settings::getInstance()->get('config')['prefix'] . 'requestLimit'] = json_encode($_POST[Settings::getInstance()->get('config')['prefix'] . 'requestLimit']);
                break;
        }

        foreach ($_POST as $key => $value) {
            if (stripos($key, Settings::getInstance()->get('config')['prefix']) !== false) {
                $code = str_replace(Settings::getInstance()->get('config')['prefix'], '', $key);
                $this->data[$code] = trim($value);
            }
        }
    }

    private function addTokenField()
    {
        $entity = new CUserTypeEntity;
        $fieldToken = [
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => Settings::getInstance()->getTokenFieldCode(),
            'XML_ID' => Settings::getInstance()->getTokenFieldCode(),
            'USER_TYPE_ID' => 'string',
            'SORT' => 100,
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'I',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 40,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => ''
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenField')
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenField')
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenField')
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenFieldHint', ['#MODULE_NAME#' => Settings::getInstance()->get('module')['name']])
            ],
        ];
        $entity->Add($fieldToken);
        $entity = new CUserTypeEntity;
        $fieldTokenExpire = [
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => Settings::getInstance()->get('config')['token']['expire']['code'],
            'XML_ID' => Settings::getInstance()->get('config')['token']['expire']['code'],
            'USER_TYPE_ID' => 'datetime',
            'SORT' => 100,
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'I',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 40,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => ''
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenFieldExpire')
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenFieldExpire')
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenFieldExpire')
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => loc('ArtamonovRestTokenFieldHint', ['#MODULE_NAME#' => Settings::getInstance()->get('module')['name']])
            ],
        ];
        $entity->Add($fieldTokenExpire);
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            Loc::loadLanguageFile(__FILE__);
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
