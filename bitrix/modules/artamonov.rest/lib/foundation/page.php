<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;

use Bitrix\Main\Localization\Loc;

class Page
{
    private static $_instance;

    public function checkAccess($code)
    {
        if (!$code === 'accessConfig') {
            if (!$GLOBALS['USER']->IsAdmin()) $GLOBALS['APPLICATION']->AuthForm(loc('ArtamonovRestAccessDenied'));
        } else {
            if (!security()->checkAccessByGroups(explode('|', config()->get($code)), $GLOBALS['USER']->GetUserGroupArray())) $GLOBALS['APPLICATION']->AuthForm(loc('ArtamonovRestAccessDenied'));
        }
    }

    public function loadLanguage($file)
    {
        Loc::loadLanguageFile($file);
    }

    public function loadMessages($file)
    {
        Loc::loadMessages($file);
    }

    public function loadCustomMessages($file)
    {
        Loc::loadCustomMessages($file);
    }

    public function setTitle($title)
    {
        $GLOBALS['APPLICATION']->SetTitle($title);
    }

    public function addCss($file)
    {
        if (!is_file($_SERVER['DOCUMENT_ROOT'] . $file)) {
            CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . settings()->get('module')['id'] . '/install/css', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/css', true, true);
        }
        $GLOBALS['APPLICATION']->SetAdditionalCSS($file);
    }

    public function addHeadString($string)
    {
        $GLOBALS['APPLICATION']->AddHeadString($string, true);
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance) && defined('ADMIN_SECTION')) {
            self::$_instance = new self();
            Loc::loadLanguageFile(__FILE__);
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
