<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


use Bitrix\Main\EventManager;

class Core
{
    private static $_instance;

    public function run()
    {
        if (defined('ADMIN_SECTION') || !Config::getInstance()->get('useRestApi') || !$this->isPath()) return false;
        if (Config::getInstance()->get('useLateStart')) {
            EventManager::getInstance()->addEventHandler('main', 'OnProlog', ['\\Artamonov\\Rest\\Foundation\\Event', 'OnProlog']);
        } else {
            $this->start();
        }
    }

    public function start()
    {
        $this->constant();
        $this->header();
        $this->security();
        $this->controller();
        die;
    }

    private function isPath()
    {
        return Config::getInstance()->get('pathRestApi') && (Config::getInstance()->get('pathRestApi') === Helper::getInstance()->ROOT() || Config::getInstance()->get('pathRestApi') === explode('/', trim($_SERVER['REQUEST_URI'], '/'))[0]);
    }

    private function constant()
    {
        define('SM_SAFE_MODE', true);
        define('PERFMON_STOP', true);
        define('PUBLIC_AJAX_MODE', true);
    }

    private function header()
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 200');
        header('Copyright: 2011-' . date('Y') . ', ' . Settings::getInstance()->get('author')['company'] . ', ' . Settings::getInstance()->get('author')['website']);
        header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Authorization, Authorization-Token, Authorization-Login, Authorization-Password');
        if ($_SERVER['HTTP_ORIGIN'] && Config::getInstance()->get('useCorsFilter')) {
            $list = Config::getInstance()->get('corsListDomains');
            if (strpos($list, '*') !== false) {
                header('Access-Control-Allow-Origin: *');
            } else {
                $list = str_replace(["\n", "\r"], '', $list);
                $list = explode(';', $list);
                if (in_array($_SERVER['HTTP_ORIGIN'], $list)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                }
            }
        }
        if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
            header('Access-Control-Max-Age: 604800');
            Response::getInstance()->ok();
        }
    }

    private function security()
    {
        if (!Security::getInstance()->hasController()) Response::getInstance()->badRequest('Request cannot be processed');
        if (!Security::getInstance()->isValidContentType()) Response::getInstance()->badRequest('Invalid content type. Must be: ' . Route::getInstance()->get()['contentType']);
        if (!Security::getInstance()->isActive()) Response::getInstance()->requestedHostUnavailable();
        if (!Security::getInstance()->isValidParameters()) Response::getInstance()->badRequest(Security::getInstance()->getErrors());
        if (!Security::getInstance()->isAuthorized()) Response::getInstance()->unauthorized();
    }

    private function controller()
    {
        Controller::getInstance()->run();
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    public function __call($name, $arguments)
    {
        Response::getInstance()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
