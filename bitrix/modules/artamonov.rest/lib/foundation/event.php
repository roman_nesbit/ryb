<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 6/24/19 9:36 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

namespace Artamonov\Rest\Foundation;


class Event
{
    public function OnProlog()
    {
        // Late Start
        Core::getInstance()->start();
    }
}
