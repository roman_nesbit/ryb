<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestUninstallPageTitle' => 'Удаление модуля: #MODULE_NAME#',
    'ArtamonovRestUninstallMessageHeader' => 'Спасибо, что использовали наш модуль!<br>',
    'ArtamonovRestUninstallMessageBody' => 'Модуль #MODULE_NAME# успешно удалён',
    'ArtamonovRestUninstallModuleDisconnected' => ' и отключен в файле #PATH#',
    'ArtamonovRestUninstallMessageFooter' => '.<br>Будем очень признательны, если Вы оставите свой отзыв о работе модуля.',
];
