<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestAccessDenied' => 'Вам запрещено просматривать данный раздел.<br>Для выяснения причины запрета свяжитесь с администратором сайта.'
];
