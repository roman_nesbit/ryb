<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestMenuItemDocumentation' => 'Документация',
    'ArtamonovRestMenuItemDocumentationGeneral' => 'Основные сведения',
    'ArtamonovRestMenuItemDocumentationRoutes' => 'Карта роутов',
    'ArtamonovRestMenuItemSecurity' => 'Безопасность',
    'ArtamonovRestMenuItemJournal' => 'Журналы',
    'ArtamonovRestMenuItemJournalRequestResponse' => 'Запрос/Ответ',
    'ArtamonovRestMenuItemJournalRequestLimit' => 'Ограничение запросов',
    'ArtamonovRestMenuItemSupport' => 'Поддержка',
    'ArtamonovRestMenuItemSettings' => 'Настройки',
];
