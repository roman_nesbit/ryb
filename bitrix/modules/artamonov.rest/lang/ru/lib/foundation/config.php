<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 6:43 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */


$MESS = [
    'ArtamonovRestTokenField' => 'API-токен',
    'ArtamonovRestTokenFieldExpire' => 'Срок годности API-токена',
    'ArtamonovRestTokenFieldHint' => 'Используется при работе модуля #MODULE_NAME#',
];
