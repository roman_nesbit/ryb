<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestInstallPageTitle' => 'Установка модуля: #MODULE_NAME#',
    'ArtamonovRestInstallMessageHeader' => 'Поздравляем!<br>',
    'ArtamonovRestInstallMessageBody' => 'Модуль #MODULE_NAME# был успешно установлен в раздел Сервисы и подключен в файле #PATH#.<br>Для начала работы необходимо перейти в Настройки и активировать модуль.<br>',
    'ArtamonovRestInstallMessageFooter' => 'Всегда готовы помочь и подсказать по работе модуля через раздел Техподдержки в меню модуля.',
];
