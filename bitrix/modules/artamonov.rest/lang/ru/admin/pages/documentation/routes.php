<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestPageTitle' => 'Карта роутов',
    'ArtamonovRestTabTitle' => '#METHOD_TYPE#',
    'ArtamonovRestTabGetTitle' => 'GET',
    'ArtamonovRestTabPostTitle' => 'POST',
    'ArtamonovRestTabPutTitle' => 'PUT',
    'ArtamonovRestTabDeleteTitle' => 'DELETE',
    'ArtamonovRestSecurityAuth' => 'Авторизация',
    'ArtamonovRestSecurityAuthHint' => 'Необходимо передать заголовок авторизации',
    'ArtamonovRestDisabledHint' => 'Роут отключен',
    'ArtamonovRestEditHint' => 'Редактировать',
    'ArtamonovRestContentTypedHint' => 'Необходимо передать заголовок Content-Type с указанным типом',
    'ArtamonovRestDescription' => 'Описание',
    'ArtamonovRestParameters' => 'Параметры запроса',
    'ArtamonovRestResponseExample' => 'Пример ответа',
    'ArtamonovRestSecurityUsersWhitelist' => 'Список разрешенных пользователей',
    'ArtamonovRestSecurityTokensWhitelist' => 'Список разрешенных токенов',
    'ArtamonovRestSecurityGroupsWhitelist' => 'Список разрешенных групп',
];
