<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestPageTitle' => 'Настройки',
    'ArtamonovRestTabMainTitle' => 'Основные',
    'ArtamonovRestTabRoutesTitle' => 'Роуты',
    'ArtamonovRestTabAccessTitle' => 'Доступы к разделам модуля',
    'ArtamonovRestUseRestApi' => 'Активировать интерфейс',
    'ArtamonovRestUseRestApiHint' => 'Активация/деактивация API-интерфейса.',
    'ArtamonovRestPathRestApi' => 'Путь интерфейса',
    'ArtamonovRestPathRestApiHint' => 'Путь по которому будет доступен интерфейс.<br><br>Пример: http://{domain}/{<b>api</b>}/example/check/<br><br>Также, можно указать константу <b>ROOT</b>.<br>Тогда интрефейс будет запускаться от корня.<br><br>Пример: http://{domain}/example/check/',
    'ArtamonovRestUseJournal' => 'Журналирование Запросов/Ответов',
    'ArtamonovRestUseJournalHint' => 'Если параметр активирован, тогда будет выполняться журналирование запросов.',
    'ArtamonovRestShowExamples' => 'Отображать примеры и подсказки',
    'ArtamonovRestShowExamplesHint' => 'Если параметр активирован, тогда будут отображаться различные подсказки и примеры на страницах модуля.',
    'ArtamonovRestUseExampleRoute' => 'Активировать тестовый роут',
    'ArtamonovRestUseExampleRouteHint' => 'Параметр активирует тестовый роут http|https://{domain}/{api}/example/check.<br><br>Внимание, после проверки работы интерфейса, тестовый роут желательно отключить, чтобы какой-нибудь злоумышленник не мог использовать его.',

    'ArtamonovRestUseNativeRoute' => 'Активировать нативные роуты',
    'ArtamonovRestUseNativeRouteHint' => 'Параметр активирует нативные роуты модуля, расположенные в карте _native.php.',

    'ArtamonovRestLocalRouteMap' => 'Путь к собственным картам роутов',
    'ArtamonovRestLocalRouteMapHint' => 'Путь по которому будут доступны собственные карты роутов.<br>Карты по-прежнему можно хранить в папке модуля.<br>Собственные карты роутов, при совпадении роутов, будут перекрывать нативные карты роутов.',

    'ArtamonovRestAccessDocumentation' => 'Документация',
    'ArtamonovRestAccessDocumentationHint' => 'Группы пользователей, которые имеют доступ к разделу Документация.',
    'ArtamonovRestAccessSecurity' => 'Безопасность',
    'ArtamonovRestAccessSecurityHint' => 'Группы пользователей, которые имеют доступ к разделу Безопасность.',
    'ArtamonovRestAccessJournal' => 'Журнал',
    'ArtamonovRestAccessJournalHint' => 'Группы пользователей, которые имеют доступ к разделу Журнал.',
    'ArtamonovRestAccessSupport' => 'Поддержка',
    'ArtamonovRestAccessSupportHint' => 'Группы пользователей, которые имеют доступ к разделу Поддержка.',
    'ArtamonovRestButtonSave' => 'Сохранить',
    'ArtamonovRestButtonRestore' => 'Сбросить',
    'ArtamonovRestSaved' => 'Настройки сохранены',
    'ArtamonovRestRestored' => 'Настройки сброшены',
];
