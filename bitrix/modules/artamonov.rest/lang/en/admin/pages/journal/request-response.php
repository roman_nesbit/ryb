<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestPageTitle' => 'Журнал: запрос/ответ',
    'ArtamonovRestRequests' => 'Запросы',
    'ArtamonovRestId' => 'ID',
    'ArtamonovRestDateTime' => 'Время',
    'ArtamonovRestIp' => 'IP адрес',
    'ArtamonovRestClientId' => 'ID клиента',
    'ArtamonovRestRequest' => 'Запрос',
    'ArtamonovRestResponse' => 'Ответ',
    'ArtamonovRestButtonDelete' => 'Удалить',
    'ArtamonovRestButtonView' => 'Просмотреть',
    'ArtamonovRestConfirmDelete' => 'Вы уверены, что хотите удалить запись?',
];
