<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestPageTitle' => 'Журнал: ограничение запросов',
    'ArtamonovRestRequests' => 'Запросы',
    'ArtamonovRestId' => 'ID',
    'ArtamonovRestDateTime' => 'Время',
    'ArtamonovRestClientId' => 'ID клиента',
    'ArtamonovRestButtonDelete' => 'Удалить',
    'ArtamonovRestConfirmDelete' => 'Вы уверены, что хотите удалить запись?',
];
