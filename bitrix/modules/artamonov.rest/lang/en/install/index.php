<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestModuleId' => 'artamonov.rest',
    'ArtamonovRestModuleName' => 'REST API Business Edition',
    'ArtamonovRestModuleDescription' => 'Модуль помогает организовать программный интерфейс для внешних и внутренних приложений',
];
