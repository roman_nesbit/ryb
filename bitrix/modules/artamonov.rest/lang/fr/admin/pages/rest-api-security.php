<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$MESS = [
    'ArtamonovRestPageTitle' => 'Безопасность',
    'ArtamonovRestTabAuthorizationTitle' => 'Авторизация',
    'ArtamonovRestTabRequestLimitTitle' => 'Ограничение количества запросов',
    'ArtamonovRestTabFiltersTitle' => 'Фильтры',

    'ArtamonovRestUseToken' => 'Авторизация по токену',
    'ArtamonovRestUseTokenHint' => 'Если параметр активен, тогда при запросе будет проверяться наличие токена в "белом" списке роута, иначе в базе данных.<br><br><b>Внимание</b>:<br>1. При активации параметра, пользователю будут добавлены Пользовательские поля <b>#FIELD_NAME_REST_API_TOKEN#</b>, <b>#FIELD_NAME_REST_API_TOKEN_EXPIRE#</b>;<br>2. Важно, чтобы при запросе со стороны клиента был отправлен заголовок Authorization-Token.',

    'ArtamonovRestTokenKey' => 'Ключевая фраза токена',
    'ArtamonovRestTokenKeyHint' => 'Ключевая фраза является дополнением к токену пользователя. Пример: Authorization-Token: {KEYWORD}:{TOKEN}',

    'ArtamonovRestTokenLifetime' => 'Срок жизни токена',
    'ArtamonovRestTokenLifetimeHint' => 'Срок жизни токена с момента его создания. Указывается в секундах. По умолчанию срок годности токена 3 года.<br><br>Параметр применяется при генерации токена.',

    'ArtamonovRestTokenFieldCode' => 'Код поля для хранения токена',
    'ArtamonovRestTokenFieldCodeNotFound' => 'Поле для хранения токена с кодом \'#REST_API_TOKEN_FIELD_CODE#\' не найдено',
    'ArtamonovRestTokenFieldCodeHint' => 'Код пользовательского поля для хранения токена.<br><br>По умолчанию используется #REST_API_TOKEN_FIELD_CODE#.<br><br>В случае указания собственного поля, поле по умолчанию удалено не будет.',

    'ArtamonovRestGenerateToken' => 'Сгенерировать токены',
    'ArtamonovRestGenerateTokenHint' => 'Если включена авторизация по токену, тогда при клике на ссылку будут сгенерированы токены для тех пользователей, у которых они отсутствуют. При указании собственного поля для токена, оно также будет учтено.',
    'ArtamonovRestGenerateTokenLinkText' => 'Выполнить',
    'ArtamonovRestExample' => 'Пример заголовка',
    'ArtamonovRestExampleHint' => 'Токен, который должен быть передан в заголовке запроса.',
    'ArtamonovRestExampleToken' => 'Authorization-Token: #KEYWORD##TOKEN#',

    'ArtamonovRestTokenGenerated' => 'Сгенерировано токенов: #COUNT#',
    'ArtamonovRestTokenNotGenerated' => 'Токены не были сгенерированы',
    'ArtamonovRestUseRequestLimit' => 'Ограничение запросов',
    'ArtamonovRestUseRequestLimitHint' => 'Если параметр активен, тогда при запросе будет проверяться количество выполненных запросов от конкретного токена.<br><br>Если количество запросов превышает максимально допустимое количество в единицу времени, тогда доступ для токена будет ограничен.<br><br>Необходима авторизация по токену.',
    'ArtamonovRestNoteNotEnoughGroups' => 'Отсутствуют группы доступные для манипулирования. <a href="group_edit.php?lang=#LANG#">Добавить группу</a>',
    'ArtamonovRestGroup' => 'Группа',
    'ArtamonovRestGroupHint' => 'Группа для которой будет применяться ограничение.',
    'ArtamonovRestNumber' => 'Количество',
    'ArtamonovRestNumberHint' => 'Максимально допустимое количество запросов для токена относящегося к группе.<br><br>Если токен имеет отношение к нескольким группам, тогда приоритет отдается группе с наибольшим количеством запросов.',
    'ArtamonovRestPeriod' => 'Период (сек)',
    'ArtamonovRestPeriodHint' => 'Период подсчета количества запросов. Задается в секундах.<br><br>Например, задано 3600 секунд, то есть 1 час, значит количество запросов будет рассчитываться за последний час.',
    'ArtamonovRestUseLoginPassword' => 'Авторизация по логину и паролю',
    'ArtamonovRestUseLoginPasswordHint' => 'Если параметр активен, тогда при запросе будет проверяться наличие пользователя в базе по указанным логину и паролю.<br><br>Внимание: важно чтобы при запросе со стороны клиента были отправлены заголовки Authorization-Login и Authorization-Password.',


    'ArtamonovRestUseCorsFilter' => 'Кросс-доменные запросы',
    'ArtamonovRestUseCorsFilterHint' => 'Если параметр включён, тогда при обработке входящего запроса будет проверяться домен источника запроса.',
    'ArtamonovRestCorsListDomains' => 'Список доменов',
    'ArtamonovRestCorsListDomainsHint' => 'Список разрешенных доменов, с которых API-интерфейс будет обрабатывать входящие запросы.<br>Домены нужно писать через точку с запятой.<br><br>Пример: http://site1.com; http://site2.com<br><br>Можно указать символ Звёздочка (*), в этом случае интерфейс будет доступен всем доменам.',

    'ArtamonovRestButtonSave' => 'Сохранить',
    'ArtamonovRestButtonRestore' => 'Сбросить',
    'ArtamonovRestSaved' => 'Настройки сохранены',
    'ArtamonovRestRestored' => 'Настройки сброшены',
];
