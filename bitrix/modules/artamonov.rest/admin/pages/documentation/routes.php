<?
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
Bitrix\Main\Loader::includeModule('artamonov.rest');
page()->checkAccess('accessDocumentation');
page()->addCss('/bitrix/css/' . settings()->get('module')['id'] . '/' . basename(__DIR__) . '-' . basename(__FILE__, '.php') . '.min.css');
page()->setTitle(loc('ArtamonovRestRoutesPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => helper()->GET(), 'TITLE' => helper()->GET()],
    ['DIV' => 'tab-2', 'TAB' => helper()->POST(), 'TITLE' => helper()->POST()],
    ['DIV' => 'tab-3', 'TAB' => helper()->PUT(), 'TITLE' => helper()->PUT()],
    ['DIV' => 'tab-4', 'TAB' => helper()->PATCH(), 'TITLE' => helper()->PATCH()],
    ['DIV' => 'tab-5', 'TAB' => helper()->DELETE(), 'TITLE' => helper()->DELETE()],
    ['DIV' => 'tab-6', 'TAB' => helper()->HEAD(), 'TITLE' => helper()->HEAD()],
];
$tabControl = new CAdminTabControl('tabControl', $tabs);
$routes = [];
$dir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'routes';
$files[$dir] = array_diff(scandir($dir), ['..', '.']);
if (config()->get('localRouteMap')) {
    $dir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . config()->get('localRouteMap');
    $files[$dir] = array_diff(scandir($dir), ['..', '.']);
}
foreach ($files as $dir => $items) {
    foreach ($items as $file) {
        if ((!config()->get('useNativeRoute') && $file === settings()->get('file')['native']) || (!config()->get('useExampleRoute') && $file === settings()->get('file')['example'])) continue;
        if (config()->get('localRouteMap') && strpos($dir, config()->get('localRouteMap'))) {
            $file = DIRECTORY_SEPARATOR . config()->get('localRouteMap') . DIRECTORY_SEPARATOR . $file;
        } else {
            $file = BX_ROOT . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . settings()->get('module')['id'] . DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . $file;
        }
        if (is_array($ar = require $_SERVER['DOCUMENT_ROOT'] . $file)) {
            foreach ($ar as $type => $r) {
                foreach ($r as $route => $config) {
                    if ($config['documentation']['exclude']['admin']) continue;
                    $routes[$type][$file][$route] = $config;
                }
            }
        }
    }
}
$groups = [];
$result = CGroup::GetList($by = 'NAME', $order = 'ASC', ['ACTIVE' => 'Y', 'ANONYMOUS' => 'N']);
while ($group = $result->fetch()) {
    $groups[$group['ID']] = $group['NAME'];
}
$pathClass = [];
function printRoutes($data, $type, $groups, &$pathClass)
{
    if (!$data) {
        ?>
        <div class="routes-empty"><i class="far fa-folder-open"></i> ...</div>
        <?
        return false;
    }
    ?>
    <div class="routes <?= strtolower($type) ?>">
        <? foreach ($data as $file => $routes): ?>
            <? foreach ($routes as $route => $config): ?>
                <div class="route">
                    <ul class="meta">
                        <li class="path">
                            <a title="<?= loc('ArtamonovRestEditHint') ?>"
                               href="/bitrix/admin/fileman_file_edit.php?path=<?= $file ?>&full_src=Y&lang=<?= LANG ?>">
                                <?= config()->get('pathRestApi') !== helper()->ROOT() ? config()->get('pathRestApi') . '/' : '' ?><?= $route ?>
                            </a>
                        </li>
                        <? if ($config['controller']): ?>
                            <li class="controller">
                                <?
                                $namespace = explode('@', $config['controller'])[0];
                                if (!$pathClass[$namespace]) {
                                    if (!class_exists($namespace)) {
                                        spl_autoload_register(function ($file) {
                                            $file = strtolower($file);
                                            $file = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $file) . '.php';
                                            if (is_file($file)) {
                                                require $file;
                                            }
                                        });
                                    }
                                    $reflector = new \ReflectionClass($namespace);
                                    $pathClass[$namespace] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $reflector->getFileName());
                                }
                                ?>
                                <a title="<?= loc('ArtamonovRestEditHint') ?>"
                                   href="/bitrix/admin/fileman_file_edit.php?path=<?= $pathClass[$namespace] ?>&full_src=Y&lang=<?= LANG ?>">
                                    <?= trim($config['controller'], '\\') ?>
                                </a>
                            </li>
                        <? endif ?>
                        <? if ($config['contentType']): ?>
                            <li class="content-type"
                                title="<?= loc('ArtamonovRestContentTypedHint') ?>"><?= $config['contentType'] ?></li>
                        <? endif ?>
                        <? if ((config()->get('useToken') || config()->get('useLoginPassword')) && $config['security']['auth']['required']): ?>
                            <li class="security-auth"
                                title="<?= loc('ArtamonovRestSecurityAuthHint') ?>"><?= loc('ArtamonovRestSecurityAuth') ?></li>
                        <? endif ?>
                        <? if ($config['active'] === false): ?>
                            <li class="disabled"
                                title="<?= loc('ArtamonovRestDisabledHint') ?>"><i class="fas fa-power-off"></i></li>
                        <? endif ?>
                    </ul>
                    <? if ($config['description']): ?>
                        <div class="description">
                            <section>
                                <div class="header"><i
                                            class="fas fa-file-alt"></i><?= loc('ArtamonovRestDescription') ?></div>
                                <div class="body"><?= $config['description'] ?></div>
                            </section>
                        </div>
                    <? endif ?>
                    <? if ($config['parameters']): ?>
                        <section>
                            <div class="header"><i class="fas fa-upload"></i><?= loc('ArtamonovRestParameters') ?></div>
                            <div class="body">
                                <?
                                foreach ($config['parameters'] as $code => $ar) { // level one
                                    printParam(1, $code, $ar);
                                    if ($ar['parameters']) { // level two
                                        foreach ($ar['parameters'] as $paramCode => $ar) {
                                            // If: param => value
                                            if (is_string($paramCode)) {
                                                printParam(2, $paramCode, $ar, $code);
                                            } // If: params => array
                                            else if (is_integer($paramCode) && is_array($ar)) {
                                                foreach ($ar as $paramCode => $ar) {
                                                    printParam(2, $paramCode, $ar, $code);
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </section>
                    <? endif ?>
                    <? if ($config['example']['request']['url'] && $config['example']['request']['response']['json']): ?>
                        <section>
                            <div class="header"><i
                                        class="fas fa-download"></i><?= loc('ArtamonovRestResponseExample') ?>
                            </div>
                            <div class="body" style="margin-bottom: 15px">
                                <?
                                $config['example']['request']['url'] = str_replace('#DOMAIN#', $_SERVER['HTTP_HOST'], $config['example']['request']['url']);
                                $config['example']['request']['url'] = str_replace('#API#', config()->get('pathRestApi'), $config['example']['request']['url']);
                                ?>
                                <a href="<?= $config['example']['request']['url'] ?>"
                                   target="_blank"><?= $config['example']['request']['url'] ?></a>
                            </div>
                            <div class="body">
                                <pre><?= json_encode(json_decode($config['example']['request']['response']['json']), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) ?></pre>
                            </div>
                        </section>
                    <? endif ?>
                    <? if ((config()->get('useToken') || config()->get('useLoginPassword')) && $config['security']['auth']['required'] && ($config['security']['login']['whitelist'] || $config['security']['token']['whitelist'] || $config['security']['group']['whitelist'])): ?>
                        <div class="whitelist" style="grid-template-columns: 1fr 1fr 1fr;">
                            <? if (config()->get('useLoginPassword') && $config['security']['login']['whitelist']): ?>
                                <section>
                                    <div class="header"><i
                                                class="fas fa-key"></i><?= loc('ArtamonovRestSecurityUsersWhitelist') ?>
                                    </div>
                                    <div class="body">
                                        <? foreach ($config['security']['login']['whitelist'] as $item): ?>
                                            <div class="row">
                                                <div class="cell"><?= $item ?></div>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </section>
                            <? endif ?>
                            <? if (config()->get('useToken') && $config['security']['token']['whitelist']): ?>
                                <section>
                                    <div class="header"><i
                                                class="fas fa-key"></i><?= loc('ArtamonovRestSecurityTokensWhitelist') ?>
                                    </div>
                                    <div class="body">
                                        <? foreach ($config['security']['token']['whitelist'] as $item): ?>
                                            <div class="row">
                                                <div class="cell"><?= $item ?></div>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </section>
                            <? endif ?>
                            <? if ($config['security']['group']['whitelist']): ?>
                                <section>
                                    <div class="header"><i
                                                class="fas fa-user-friends"></i><?= loc('ArtamonovRestSecurityGroupsWhitelist') ?>
                                    </div>
                                    <div class="body">
                                        <? foreach ($config['security']['group']['whitelist'] as $id): ?>
                                            <? if ($groups[$id]): ?>
                                                <div class="row">
                                                    <div class="cell"><?= $groups[$id] ?></div>
                                                </div>
                                            <? endif ?>
                                        <? endforeach ?>
                                    </div>
                                </section>
                            <? endif ?>
                        </div>

                    <? endif ?>
                </div>
                <br>
            <? endforeach ?>
        <? endforeach ?>
    </div>
    <?
}

function printParam($level, $code, $ar, $parentCode = null)
{
    if ($level === 1):
        ?>
        <div class="row">
            <div class="cell"><?= $code ?></div>
            <div class="cell"><?= $ar['type'] ?></div>
            <div class="cell"><? if ($ar['required']): ?><span
                        class="required">required</span><? else: ?>optional<? endif ?></div>
            <div class="cell"
                 style="max-width: 600px;"><?= $ar['possibleValue'] ? implode('<span style="color: #c6c6c6; margin: 0 2px">,</span>', $ar['possibleValue']) : '<span style="color: #c6c6c6">-</span>' ?></div>
            <div class="cell"><?= $ar['description'] ?></div>
        </div>
    <?
    else:
        ?>
        <div class="row">
            <div class="cell"><?= $parentCode . ' -> ' . $code ?></div>
            <div class="cell"><?= $ar['type'] ?></div>
            <div class="cell"><? if ($ar['required']): ?><span
                        class="required">required</span><? else: ?>optional<? endif ?>
            </div>
            <div class="cell"
                 style="max-width: 600px;"><?= $ar['possibleValue'] ? implode('<span style="color: #c6c6c6; margin: 0 2px">,</span>', $ar['possibleValue']) : '<span style="color: #c6c6c6">-</span>' ?></div>
            <div class="cell"><?= $ar['description'] ?></div>
        </div>
    <?
    endif;
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
$tabControl->Begin();
$tabControl->BeginNextTab();
printRoutes($routes[helper()->GET()], helper()->GET(), $groups, $pathClass);
$tabControl->BeginNextTab();
printRoutes($routes[helper()->POST()], helper()->POST(), $groups, $pathClass);
$tabControl->BeginNextTab();
printRoutes($routes[helper()->PUT()], helper()->PUT(), $groups, $pathClass);
$tabControl->BeginNextTab();
printRoutes($routes[helper()->PATCH()], helper()->PATCH(), $groups, $pathClass);
$tabControl->BeginNextTab();
printRoutes($routes[helper()->DELETE()], helper()->DELETE(), $groups, $pathClass);
$tabControl->BeginNextTab();
printRoutes($routes[helper()->HEAD()], helper()->HEAD(), $groups, $pathClass);
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
