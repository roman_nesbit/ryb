<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

page()->loadLanguage(__FILE__);
$methods = get_class_methods('\Artamonov\Rest\Foundation\Response');
sort($methods);
$listMethods = '';
foreach ($methods as $method) {
    if ($method === 'getInstance' || $method === 'json' || $method === '__call') continue;
    $listMethods .= '<div class="row"><div class="cell">request()->' . $method . '()</div></div>';
}
echo loc('ArtamonovRestContent', ['#METHODS#' => $listMethods]);
