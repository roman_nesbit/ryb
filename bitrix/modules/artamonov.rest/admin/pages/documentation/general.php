<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
Bitrix\Main\Loader::includeModule('artamonov.rest');
page()->checkAccess('accessDocumentation');
page()->addCss('/bitrix/css/' . settings()->get('module')['id'] . '/' . basename(__DIR__) . '-' . basename(__FILE__, '.php') . '.min.css');
page()->setTitle(loc('ArtamonovRestDocumentationGeneralPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabFunctionsTitle'), 'TITLE' => loc('ArtamonovRestTabFunctionsDescription')]
];
if (config()->get('showExamples')) {
    $tabs[] = ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabExamplesTitle'), 'TITLE' => loc('ArtamonovRestTabExamplesDescription')];
};
$tabControl = new CAdminTabControl('tabControl', $tabs);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
if (!config()->get('showExamples')) helper()->note(loc('ArtamonovRestWarningExamplesHidden', ['#LANG#' => LANG]));
$tabControl->Begin();
$tabControl->BeginNextTab();
include 'general-functions.php';
if (config()->get('showExamples')) {
    $tabControl->BeginNextTab();
    include 'general-examples.php';
}
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
