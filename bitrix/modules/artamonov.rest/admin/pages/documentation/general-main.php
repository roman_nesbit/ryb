<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

page()->loadLanguage(__FILE__);
echo loc('ArtamonovRestContent', ['#MODULE_NAME#' => settings()->get('module')['name'], '#MODULE_DESCRIPTION#' => settings()->get('module')['description']]);
