<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
Bitrix\Main\Loader::includeModule('artamonov.rest');
page()->checkAccess('accessJournal');
page()->setTitle(loc('ArtamonovRestMenuItemJournalRequestResponse'));
$sTableID = 'tbl_' . settings()->get('config')['table']['request-response'];
$oSort = new CAdminSorting($sTableID, $by, $order);
$lAdmin = new CAdminList($sTableID, $oSort);
if ($ids = $lAdmin->GroupAction()) {
    if ($_REQUEST['action_target'] === 'selected') {
        $ids = '*';
    }
    if ($ids) {
        switch ($_REQUEST['action']) {
            case 'delete':
                journal()->delete('request-response', $ids);
                break;
        }
    }
}
function CheckFilter()
{
    global $arFilterFields, $lAdmin;
    foreach ($arFilterFields as $f) global $$f;
    return count($lAdmin->arFilterErrors) == 0;
}

$arFilterFields = [
    'find_id',
    'find_date_from',
    'find_date_to',
    'find_client_id',
    'find_ip',
    'find_method',
];
$arFilter = [];
$lAdmin->InitFilter($arFilterFields);
InitSorting();
if (CheckFilter()) {
    $arFilter = [
        'ID' => $find_id,
        'DATETIME_FROM' => $find_date_from,
        'DATETIME_TO' => $find_date_to,
        'CLIENT_ID' => $find_client_id,
        'IP' => $find_ip,
        'METHOD' => $find_method,
    ];
}
$arSort = [
    'field' => $by ? $by : 'ID',
    'order' => $order ? $order : 'DESC'
];
$arNavParams = (isset($_REQUEST['mode']) && $_REQUEST['mode'] === 'excel') ? false : ['nPageSize' => CAdminResult::GetNavSize($sTableID)];
$rsData = journal()->get('request-response', $arFilter, $arSort);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(loc('ArtamonovRestRequests')));
$arHeaders = [
    [
        'id' => 'ID',
        'content' => loc('ArtamonovRestId'),
        'sort' => 'ID',
        'default' => true,
        'align' => 'right',
    ],
    [
        'id' => 'DATETIME',
        'content' => loc('ArtamonovRestDateTime'),
        'sort' => 'DATETIME',
        'default' => true
    ],
    [
        'id' => 'CLIENT_ID',
        'content' => loc('ArtamonovRestClientId'),
        'sort' => 'CLIENT_ID',
        'default' => true
    ],
    [
        'id' => 'IP',
        'content' => loc('ArtamonovRestIp'),
        'sort' => 'IP',
        'default' => true
    ],
    [
        'id' => 'METHOD',
        'content' => loc('ArtamonovRestMethod'),
        'sort' => 'METHOD',
        'default' => true
    ],
];
$lAdmin->AddHeaders($arHeaders);
while ($ar = $rsData->fetch()) {
    $row =& $lAdmin->AddRow($ar['ID'], $ar);
    if ($ar['ID']) {
        $row->AddViewField('ID', '<a href="/bitrix/admin/rest-api-journal-request-response-record.php?lang=' . LANGUAGE_ID . '&id=' . $ar['ID'] . '">' . $ar['ID'] . '</a>');
    }
    $arActions = [];
    $arActions[] = [
        'ICON' => 'view',
        'DEFAULT' => 'Y',
        'TEXT' => loc('ArtamonovRestButtonView'),
        'LINK' => 'rest-api-journal-request-response-record.php?lang=' . LANGUAGE_ID . '&id=' . $ar['ID'],
    ];
    $arActions[] = [
        'ICON' => 'delete',
        'DEFAULT' => 'N',
        'TEXT' => loc('ArtamonovRestButtonDelete'),
        'ACTION' => "if(confirm('" . GetMessageJS('ArtamonovRestConfirmDelete') . "')) " . $lAdmin->ActionDoGroup($ar['ID'], 'delete'),
    ];
    $row->AddActions($arActions);
}
$lAdmin->AddGroupActionTable(['delete' => true]);
$lAdmin->CheckListMode();
$arFilterNames = [
    'find_id' => loc('ArtamonovRestId'),
    'find_date_from' => loc('ArtamonovRestDateTime'),
    'find_client_id' => loc('ArtamonovRestClientId'),
    'find_ip' => loc('ArtamonovRestIp'),
    'find_method' => loc('ArtamonovRestMethod'),
];
$oFilter = new CAdminFilter($sTableID . '_filter', $arFilterNames);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
?>
    <form name="filter" method="GET" action="<?= $APPLICATION->GetCurPage() ?>?">
        <input type="hidden" name="lang" value="<?= LANG ?>">
        <? $oFilter->Begin() ?>
        <tr>
            <td><?= loc('ArtamonovRestId') ?>:</td>
            <td><?= InputType('text', 'find_id', htmlspecialcharsbx($find_id), false) ?></td>
        </tr>
        <tr>
            <td><?= loc('ArtamonovRestDateTime') ?>:</td>
            <td><?= CalendarPeriod('find_date_from', $find_date_from, 'find_date_to', $find_date_to, 'filter', 'Y') ?></td>
        </tr>
        <tr>
            <td><?= loc('ArtamonovRestClientId') ?>:</td>
            <td><?= InputType('text', 'find_client_id', htmlspecialcharsbx($find_client_id), false) ?></td>
        </tr>
        <tr>
            <td><?= loc('ArtamonovRestIp') ?>:</td>
            <td><?= InputType('text', 'find_ip', htmlspecialcharsbx($find_ip), false) ?></td>
        </tr>
        <tr>
            <td><?= loc('ArtamonovRestMethod') ?>:</td>
            <td><?= InputType('text', 'find_method', htmlspecialcharsbx($find_method), false) ?></td>
        </tr>
        <?
        $oFilter->Buttons(['table_id' => $sTableID, 'url' => $APPLICATION->GetCurPage(), 'form' => 'filter']);
        $oFilter->End();
        ?>
    </form>
<?php
$lAdmin->DisplayList();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
