<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
Bitrix\Main\Loader::includeModule('artamonov.rest');
page()->checkAccess('accessSecurity');
page()->setTitle(loc('ArtamonovRestSecurityPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabAuthorizationTitle')],
    ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabRequestLimitTitle')],
    ['DIV' => 'tab-3', 'TAB' => loc('ArtamonovRestTabFiltersTitle')],
];
$tabControl = new CAdminTabControl('tabControl', $tabs);
$groups = [];
$result = CGroup::GetList($by = 'NAME', $order = 'ASC', ['ACTIVE' => 'Y', 'ANONYMOUS' => 'N']);
while ($group = $result->fetch()) {
    if ($group['ID'] == helper()->adminGroupId() || $group['ID'] == helper()->ratingVoteGroupId() || $group['ID'] == helper()->ratingVoteAuthorityGroupId()) continue;
    $groups[$group['ID']] = $group['NAME'];
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
if ($_POST) {
    $_POST['form'] = basename(__FILE__, '.php');
    if (isset($_POST['save'])) {
        config()->save();
        echo CAdminMessage::ShowNote(loc('ArtamonovRestSaved'));
    } elseif (isset($_POST['restore'])) {
        config()->restore();
        echo CAdminMessage::ShowNote(loc('ArtamonovRestRestored'));
    }
}
if ($_GET['generateToken'] == 'Y') {
    $user = new CUser();
    $counter = 0;
    if ($users = CUser::GetList($by = 'ID', $order = 'DESC', [settings()->getTokenFieldCode() => false, 'ACTIVE' => 'Y'], ['FIELDS' => ['ID', 'LOGIN'], 'SELECT' => [settings()->getTokenFieldCode()]])) {
        while ($ar = $users->fetch()) {
            if (!array_key_exists(settings()->getTokenFieldCode(), $ar)) continue;
            $token = helper()->generateToken($ar['ID'], $ar['LOGIN']);
            if ($user->update($ar['ID'], [
                settings()->getTokenFieldCode() => $token,
                settings()->get('config')['token']['expire']['code'] => settings()->getTokenExpire()
            ])) {
                $counter++;
            }
        }
    }
    if ($counter > 0) {
        echo CAdminMessage::ShowNote(loc('ArtamonovRestTokenGenerated', ['#COUNT#' => $counter]));
    } else {
        echo CAdminMessage::ShowNote(loc('ArtamonovRestTokenNotGenerated'));
    }
}
if (config()->get('tokenFieldCode')) {
    if (!db()->query('SELECT FIELD_NAME FROM b_user_field WHERE FIELD_NAME="' . config()->get('tokenFieldCode') . '" LIMIT 1')->fetchRaw()) {
        CAdminMessage::ShowMessage(loc('ArtamonovRestTokenFieldCodeNotFound', ['#REST_API_TOKEN_FIELD_CODE#' => config()->get('tokenFieldCode')]));
    };
}
$tabControl->Begin();
?>
    <form method="POST" name="<?= basename(__FILE__, '.php') ?>" action="<?= $APPLICATION->GetCurUri() ?>">
        <?= bitrix_sessid_post() ?>
        <? $tabControl->BeginNextTab() ?>
        <tr>
            <td width='45%' valign='top'><?= loc('ArtamonovRestUseLoginPassword') ?>
            <td>
            <td width='55%' valign='middle'>
                <?= InputType('checkbox', 'parameter:useLoginPassword', true, config()->get('useLoginPassword')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseLoginPasswordHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestUseToken') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:useToken', true, config()->get('useToken')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseTokenHint', ['#FIELD_NAME_REST_API_TOKEN#' => loc('ArtamonovRestTokenField'), '#FIELD_NAME_REST_API_TOKEN_EXPIRE#' => loc('ArtamonovRestTokenFieldExpire')])) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestTokenKey') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('text', 'parameter:tokenKey', config()->get('tokenKey'), false, false, false, config()->get('useToken') ? '' : 'disabled') ?>
                <? ShowJSHint(loc('ArtamonovRestTokenKeyHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestTokenLifetime') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('text', 'parameter:tokenLifetime', config()->get('tokenLifetime'), false, false, false, config()->get('useToken') ? '' : 'disabled') ?>
                <? ShowJSHint(loc('ArtamonovRestTokenLifetimeHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestTokenFieldCode') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('text', 'parameter:tokenFieldCode', config()->get('tokenFieldCode'), false, false, false, config()->get('useToken') ? '' : 'disabled') ?>
                <? ShowJSHint(loc('ArtamonovRestTokenFieldCodeHint', ['#REST_API_TOKEN_FIELD_CODE#' => settings()->get('config')['token']['code']])) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width='45%' valign='middle'><?= loc('ArtamonovRestGenerateToken') ?>
            <td>
            <td width='55%' valign='middle'>
                <?= print_url(str_replace('&generateToken=Y', '', $APPLICATION->GetCurUri()) . '&generateToken=Y', loc('ArtamonovRestGenerateTokenLinkText')) ?>
                <? ShowJSHint(loc('ArtamonovRestGenerateTokenHint')) ?>
            <td>
        </tr>
        <? if (config()->get('showExamples')): ?>
            <tr>
                <td width="45%" valign="middle"><?= loc('ArtamonovRestExample') ?>
                <td>
                <td width="55%" valign="middle"
                    style="color: <?= config()->get('useToken') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>">
                    <?= loc('ArtamonovRestExampleToken', ['#KEYWORD#' => config()->get('tokenKey') ? config()->get('tokenKey') . ':' : '', '#TOKEN#' => '434337b6-f12691d2-47bf6fb9-c040ae6b']) ?>
                    <? ShowJSHint(loc('ArtamonovRestExampleHint')) ?>
                <td>
            </tr>
        <? endif ?>
        <? $tabControl->BeginNextTab() ?>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestUseRequestLimit') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:useRequestLimit', true, config()->get('useRequestLimit'), false, false, config()->get('useToken') || config()->get('useLoginPassword') ? '' : 'disabled') ?>
                <? ShowJSHint(loc('ArtamonovRestUseRequestLimitHint')) ?>
            <td>
        </tr>

        <? if (count($groups) > 0): ?>
            <? $current = json_decode(config()->get('requestLimit'), true) ?>
            <? foreach ($groups as $id => $name): ?>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td width="45%" valign="middle"><?= loc('ArtamonovRestGroup') ?>
                    <td>
                    <td width="55%" valign="middle">
                        <?= $name ?>
                        <? ShowJSHint(loc('ArtamonovRestGroupHint')) ?>
                    <td>
                </tr>

                <tr>
                    <td width="45%" valign="middle"><?= loc('ArtamonovRestNumber') ?>
                    <td>
                    <td width="55%" valign="middle">
                        <?= InputType('text', 'data:requestLimitNumber-' . $id, $current[$id]['number'], false, false, false) ?>
                        <? ShowJSHint(loc('ArtamonovRestNumberHint')) ?>
                    <td>
                </tr>

                <tr>
                    <td width="45%" valign="middle"><?= loc('ArtamonovRestPeriod') ?>
                    <td>
                    <td width="55%" valign="middle">
                        <?= InputType('text', 'data:requestLimitPeriod-' . $id, $current[$id]['period'], false, false, false) ?>
                        <? ShowJSHint(loc('ArtamonovRestPeriodHint')) ?>
                    <td>
                </tr>

            <? endforeach ?>
        <? else: ?>
            <tr>
                <td colspan="4" align="center">
                    <? helper()->note(loc('ArtamonovRestNoteNotEnoughGroups', ['#LANG#' => LANG])) ?>
                </td>
            </tr>
        <? endif ?>

        <? $tabControl->BeginNextTab() ?>

        <tr>
            <td width='45%' valign='top'><?= loc('ArtamonovRestUseCorsFilter') ?>
            <td>
            <td width='55%' valign='middle'>
                <?= InputType('checkbox', 'parameter:useCorsFilter', true, config()->get('useCorsFilter')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseCorsFilterHint')) ?>
            <td>
        </tr>
        <tr>
            <td width='45%'
                valign='top'><?= loc('ArtamonovRestCorsListDomains') ?>
            <td>
            <td width='55%' valign='top'>
                <textarea name="parameter:corsListDomains" cols="50"
                          rows="5"><?= config()->get('corsListDomains') ?></textarea>
                <? ShowJSHint(loc('ArtamonovRestCorsListDomainsHint')) ?>
            <td>
        </tr>

        <tr>
            <td colspan="4" valign="middle">
                <? $tabControl->Buttons() ?>
                <?= InputType('submit', 'save', loc('ArtamonovRestButtonSave'), false, false, false, config()->get('useRestApi') ? 'class="adm-btn-save"' : 'disabled') ?>
                <?= InputType('submit', 'restore', loc('ArtamonovRestButtonRestore'), false, false, false, config()->get('useRestApi') ? '' : 'disabled') ?>
            </td>
        </tr>
    </form>
<?php
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
