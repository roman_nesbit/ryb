<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
Bitrix\Main\Loader::includeModule('artamonov.rest');
page()->checkAccess('accessSupport');
page()->addCss('/bitrix/css/' . settings()->get('module')['id'] . '/' . basename(__FILE__, '.php') . '.min.css');
page()->setTitle(loc('ArtamonovRestSupportPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabMainTitle'), 'TITLE' => loc('ArtamonovRestTabMainDescription')],
    ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabMonitorTitle'), 'TITLE' => loc('ArtamonovRestTabMonitorDescription')]
];
$tabControl = new CAdminTabControl('tabControl', $tabs);
$routes = [];
$dir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'routes';
$files[$dir] = array_diff(scandir($dir), ['..', '.']);
if (config()->get('localRouteMap')) {
    $dir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . config()->get('localRouteMap');
    $files[$dir] = array_diff(scandir($dir), ['..', '.']);
}
foreach ($files as $dir => $items) {
    foreach ($items as $file) {
        if ((!config()->get('useNativeRoute') && $file === settings()->get('file')['native']) || (!config()->get('useExampleRoute') && $file === settings()->get('file')['example'])) continue;
        $file = $dir . DIRECTORY_SEPARATOR . $file;
        if (is_array($ar = require $file)) {
            foreach ($ar as $type => $r) {
                foreach ($r as $route => $config) {
                    $routes[$type][] = $route;
                    if ($config['active'] === false) $routes['disabled'][] = $route;
                    if ($config['security']['auth']['required']) $routes['auth'][] = $route;
                    if ($config['security']['login']['whitelist']) $routes['login-whitelist'][] = $route;
                    if ($config['security']['token']['whitelist']) $routes['token-whitelist'][] = $route;
                    if ($config['security']['group']['whitelist']) $routes['group-whitelist'][] = $route;
                }
            }
        }
    }
}
$totalTokens = (config()->get('useToken')) ? db()->query('SELECT COUNT(VALUE_ID) as COUNT FROM b_uts_user WHERE ' . settings()->getTokenFieldCode() . ' IS NOT NULL')->fetchRaw()['COUNT'] : 0;
$sql = '
        SELECT
            COUNT(ID) as total,
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="GET") as totalGet, 
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="POST") as totalPost, 
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="PUT") as totalPut, 
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="PATCH") as totalPatch, 
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="DELETE") as totalDelete,
            (SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="HEAD") as totalHead
        FROM ' . settings()->get('config')['table']['request-response'];

$requests = db()->query($sql)->fetchRaw();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
$tabControl->Begin();
?>
<? $tabControl->BeginNextTab() ?>
    <div class="paragraphs main">
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= settings()->get('module')['name'] ?></li>
                    <li class="version"
                        title="<?= loc('ArtamonovRestVersion') ?>"><?= settings()->get('module')['version'] ?></li>
                    <li class="version-date"
                        title="<?= loc('ArtamonovRestVersionDate') ?>"><?= date_format(date_create(settings()->get('module')['versionDate']), 'd.m.Y') ?></li>
                </ul>
            </div>
            <div class="description">
                <section>
                    <div class="header"><i class="fas fa-file-alt"></i><?= loc('ArtamonovRestDescription') ?></div>
                    <div class="body"><?= settings()->get('module')['description'] ?></div>
                </section>
            </div>
        </div>
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li title="<?= loc('ArtamonovRestVendor') ?>">
                        <?= loc('ArtamonovRestVendor') ?>:
                        <?= print_url(settings()->get('partner')['website'], settings()->get('partner')['name'], 'target="_blank"') ?>
                    </li>
                    <li class="marketplace"
                        title="<?= loc('ArtamonovRestMarketplace') ?>"><?= print_url(settings()->get('path')['marketplace'], loc('ArtamonovRestMarketplace'), 'target="_blank"') ?></li>
                </ul>
            </div>
        </div>
    </div>

<? $tabControl->BeginNextTab() ?>
    <div class="paragraphs monitor">
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= loc('ArtamonovRestModule') ?></li>
                </ul>
            </div>
            <div class="table-two-column">
                <section>
                    <div class="header"><i class="fas fa-cog"></i><?= loc('ArtamonovRestConfig') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestApi') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useRestApi') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useRestApi') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestPathRestApi') ?></div>
                            <div class="cell"><?= config()->get('pathRestApi') ? config()->get('pathRestApi') : '-' ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestNativeRoute') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useExampleRoute') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useNativeRoute') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestExampleRoute') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useExampleRoute') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useExampleRoute') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseJournal') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useJournal') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useJournal') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestShowExamples') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('showExamples') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('showExamples') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestPhp') ?></div>
                            <div class="cell"><?= PHP_VERSION ?></div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="header"><i class="fas fa-key"></i><?= loc('ArtamonovRestAuth') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseLoginPassword') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useLoginPassword') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useLoginPassword') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseToken') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useToken') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useToken') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTokenKey') ?></div>
                            <div class="cell"><?= config()->get('tokenKey') ? config()->get('tokenKey') : '-' ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTokenLifetime') ?></div>
                            <div class="cell"><?= config()->get('tokenLifetime') ? config()->get('tokenLifetime') : '-' ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTokenFiledCode') ?></div>
                            <div class="cell"><?= settings()->getTokenFieldCode() ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseRequestLimit') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useRequestLimit') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useRequestLimit') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalTokens') ?></div>
                            <div class="cell"><?= $totalTokens ?></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= loc('ArtamonovRestStatistics') ?></li>
                </ul>
            </div>
            <div class="table-two-column">
                <section>
                    <div class="header"><i class="fas fa-sitemap"></i><?= loc('ArtamonovRestRoutes') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= helper()->GET() ?></div>
                            <div class="cell"><?= $routes[helper()->GET()] ? count($routes[helper()->GET()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->POST() ?></div>
                            <div class="cell"><?= $routes[helper()->POST()] ? count($routes[helper()->POST()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->PUT() ?></div>
                            <div class="cell"><?= $routes[helper()->PUT()] ? count($routes[helper()->PUT()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->PATCH() ?></div>
                            <div class="cell"><?= $routes[helper()->PATCH()] ? count($routes[helper()->PATCH()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->DELETE() ?></div>
                            <div class="cell"><?= $routes[helper()->DELETE()] ? count($routes[helper()->DELETE()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->HEAD() ?></div>
                            <div class="cell"><?= $routes[helper()->HEAD()] ? count($routes[helper()->HEAD()]) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesDisabled') ?></div>
                            <div class="cell"><?= ($routes['disabled']) ? count($routes['disabled']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesAuth') ?></div>
                            <div class="cell"><?= ($routes['auth']) ? count($routes['auth']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesLoginWhitelist') ?></div>
                            <div class="cell"><?= ($routes['login-whitelist']) ? count($routes['login-whitelist']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesTokensWhitelist') ?></div>
                            <div class="cell"><?= ($routes['token-whitelist']) ? count($routes['token-whitelist']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesGroupsWhitelist') ?></div>
                            <div class="cell"><?= ($routes['group-whitelist']) ? count($routes['group-whitelist']) : 0 ?></div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="header"><i class="fas fa-sign-in-alt"></i><?= loc('ArtamonovRestRequests') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequest') ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?lang=<?= LANG ?>"><?= $requests['total'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->GET() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=GET"><?= $requests['totalGet'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->POST() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=POST"><?= $requests['totalPost'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->PUT() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=PUT"><?= $requests['totalPut'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->PATCH() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=PATCH"><?= $requests['totalPatch'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->DELETE() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=DELETE"><?= $requests['totalDelete'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= helper()->HEAD() ?></div>
                            <div class="cell">
                                <a href="/bitrix/admin/rest-api-journal-request-response.php?PAGEN_1=1&SIZEN_1=20&lang=<?= LANG ?>&set_filter=Y&adm_filter_applied=0&find_method=HEAD"><?= $requests['totalHead'] ?></a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
