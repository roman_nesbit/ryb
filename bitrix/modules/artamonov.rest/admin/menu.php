<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);
Bitrix\Main\Loader::IncludeModule('artamonov.rest');

return [
    [
        'parent_menu' => 'global_menu_services',
        'text' => settings()->get('module')['name'],
        'section' => settings()->get('module')['id'],
        'module_id' => settings()->get('module')['id'],
        'items_id' => 'menu_' . settings()->get('module')['id'],
        'icon' => 'clouds_menu_icon',
        'page_icon' => 'clouds_menu_icon',
        'sort' => 1,
        'items' => [

            [
                'items_id' => 'menu_' . settings()->get('module')['id'] . '_documentation',
                'icon' => 'learning_menu_icon',
                'page_icon' => 'learning_menu_icon',
                'text' => loc('ArtamonovRestMenuItemDocumentation'),
                'items' => [
                    [
                        'items_id' => 'menu_' . settings()->get('module')['id'] . '_documentation_general',
                        'text' => loc('ArtamonovRestMenuItemDocumentationGeneral'),
                        'url' => 'https://gitlab.com/artamonov.documentation/modules/bitrix/artamonov.rest/wikis/home'
                    ],
                    [
                        'items_id' => 'menu_' . settings()->get('module')['id'] . '_documentation_routes',
                        'text' => loc('ArtamonovRestMenuItemDocumentationRoutes'),
                        'url' => 'rest-api-documentation-routes.php?lang=' . LANG
                    ]
                ]
            ],

            [
                'items_id' => 'menu_' . settings()->get('module')['id'] . '_security',
                'icon' => 'security_menu_ddos_icon',
                'page_icon' => 'security_menu_ddos_icon',
                'text' => loc('ArtamonovRestMenuItemSecurity'),
                'url' => 'rest-api-security.php?lang=' . LANG
            ],

            [
                'items_id' => 'menu_' . settings()->get('module')['id'] . '_journal',
                'icon' => 'update_marketplace',
                'page_icon' => 'update_marketplace',
                'text' => loc('ArtamonovRestMenuItemJournal'),
                'items' => [
                    [
                        'items_id' => 'menu_' . settings()->get('module')['id'] . '_journal_request_response',
                        'text' => loc('ArtamonovRestMenuItemJournalRequestResponse'),
                        'url' => 'rest-api-journal-request-response.php?lang=' . LANG,
                        'more_url' => [
                            'rest-api-journal-request-response-record.php?lang=' . LANG
                        ]
                    ],
                    [
                        'items_id' => 'menu_' . settings()->get('module')['id'] . '_journal_request_limit',
                        'text' => loc('ArtamonovRestMenuItemJournalRequestLimit'),
                        'url' => 'rest-api-journal-request-limit.php?lang=' . LANG
                    ]
                ]
            ],

            [
                'items_id' => 'menu_' . settings()->get('module')['id'] . '_support',
                'icon' => 'support_menu_icon',
                'page_icon' => 'support_menu_icon',
                'text' => loc('ArtamonovRestMenuItemSupport'),
                'url' => 'rest-api-support.php?lang=' . LANG
            ],

            [
                'items_id' => 'menu_' . settings()->get('module')['id'] . '_settings',
                'icon' => 'sys_menu_icon',
                'page_icon' => 'sys_menu_icon',
                'text' => loc('ArtamonovRestMenuItemSettings'),
                'url' => 'rest-api-config.php?lang=' . LANG
            ]
        ]
    ]
];
