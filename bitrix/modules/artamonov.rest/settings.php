<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

use Bitrix\Main\Localization\Loc;

return [
    'module' => [
        'id' => Loc::getMessage('ArtamonovRestModuleId'),
        'name' => Loc::getMessage('ArtamonovRestModuleName'),
        'description' => Loc::getMessage('ArtamonovRestModuleDescription'),
        'connectionString' => 'if (Bitrix\Main\Loader::includeModule(\'' . Loc::getMessage('ArtamonovRestModuleId') . '\')) \Artamonov\Rest\Foundation\Core::getInstance()->run();',
        'version' => '3.3.0',
        'versionDate' => '2019-06-24 20:00:00'
    ],
    'author' => [
        'company' => Loc::getMessage('ArtamonovRestAuthorCompany'),
        'name' => Loc::getMessage('ArtamonovRestAuthorName'),
        'email' => 'artamonov.ceo@gmail.com',
        'website' => 'http://artamonov.biz/'
    ],
    'partner' => [
        'name' => Loc::getMessage('ArtamonovRestAuthorName'),
        'website' => 'http://artamonov.biz/'
    ],
    'config' => [
        'prefix' => 'parameter:',
        'token' => [
            'code' => 'UF_REST_API_TOKEN',
            'expire' => [
                'code' => 'UF_API_TOKEN_EXPIRE',
                'lifetime' => '3 years'
            ]
        ],
        'table' => [
            'request-response' => Loc::getMessage('ArtamonovRestTablePrefix') . 'request_response',
            'request-limit' => Loc::getMessage('ArtamonovRestTablePrefix') . 'request_limit'
        ]
    ],
    'path' => [
        'reviews' => 'http://marketplace.1c-bitrix.ru/solutions/' . Loc::getMessage('ArtamonovRestModuleId') . '/#tab-rating-link',
        'marketplace' => 'http://marketplace.1c-bitrix.ru/solutions/' . Loc::getMessage('ArtamonovRestModuleId') . '/'
    ],
    'file' => [
        'native' => '_native.php',
        'example' => '_example.php',
    ],
];
