<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

use Bitrix\Main\Application;

$settings = require __DIR__ . '/../../settings.php';

Application::getConnection()->queryExecute('DROP TABLE IF EXISTS ' . $settings['config']['table']['request-response']);
Application::getConnection()->queryExecute('DROP TABLE IF EXISTS ' . $settings['config']['table']['request-limit']);
