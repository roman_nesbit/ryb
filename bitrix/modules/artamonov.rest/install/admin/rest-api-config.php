<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/artamonov.rest/admin/pages/rest-api-config.php';
