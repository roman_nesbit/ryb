<?php
/**
 * Copyright (c) 2019 Denis Artamonov
 * Created: 1/13/19 4:57 PM
 * Author: Denis Artamonov
 * Email: artamonov.ceo@gmail.com
 */

$arModuleVersion = [
    'VERSION' => '3.3.0',
    'VERSION_DATE' => '2019-06-24 20:00:00'
];
