<?
$MESS["ACTIVE"] = "Активна:";
$MESS["ADD"] = "Додати";
$MESS["ALL_SITES"] = "Всі сайти:";
$MESS["DEFAULT"] = "< за умовчанням >";
$MESS["DESCRIPTION"] = "Опис:";
$MESS["EDIT_GROUP_TITLE"] = "Редагування групи # #ID#";
$MESS["EDIT_GROUP_TITLE_VIEW"] = "Перегляд групи # #ID#";
$MESS["GP_BLOCK_LOGIN_ATTEMPTS"] = "Кількість спроб введення пароля до тимчасового блокування користувача";
$MESS["GP_BLOCK_TIME"] = "Період блокування користувача (хвилин)";
$MESS["GP_CHECKWORD_TIMEOUT"] = "Термін дії контрольного слова для відновлення паролю, хвилин";
$MESS["GP_LOGIN_ATTEMPTS"] = "Кількість спроб вводу пароля до показу CAPTCHA";
$MESS["GP_MAX_STORE_NUM"] = "Максимальна кількість комп'ютерів, на яких може бути одночасно бути збережена авторизація";
$MESS["GP_PASSWORD_DIGITS"] = "Пароль повинен містити цифри (0-9)";
$MESS["GP_PASSWORD_LENGTH"] = "Мінімальна довжина паролю";
$MESS["GP_PASSWORD_LOWERCASE"] = "Пароль повинен містити латинські символи нижнього регістру (a-z)";
$MESS["GP_PASSWORD_PUNCTUATION"] = "Пароль повинен містити знаки пунктуації (,.<>/?;:'\"[]{}\\|`~!@#\$%^&*()-_+=)";
$MESS["GP_PASSWORD_UPPERCASE"] = "Пароль повинен містити латинські символи верхнього регістру (A-Z)";
$MESS["GP_SESSION_IP_MASK"] = "Маска мережі для прив'язки сесії";
$MESS["GP_SESSION_TIMEOUT"] = "Час життя сесії, хвилин";
$MESS["GP_STORE_IP_MASK"] = "Маска мережі для прив'язки збереженої авторизації";
$MESS["GP_STORE_TIMEOUT"] = "Термін зберігання авторизації, що запам'ятова на комп'ютері користувача, хвилин";
$MESS["KERNEL"] = "Головний модуль:";
$MESS["LAST_UPDATE"] = "Останнє оновлення:";
$MESS["MAIN_COPY_RECORD"] = "Копіювати групу";
$MESS["MAIN_COPY_RECORD_TITLE"] = "Копіювати групу користувачів";
$MESS["MAIN_C_SORT"] = "Порядок сортування:";
$MESS["MAIN_DELETE_RECORD"] = "Видалити групу";
$MESS["MAIN_DELETE_RECORD_CONF"] = "Ви впевнені, що хочете видалити групу?";
$MESS["MAIN_DELETE_RECORD_TITLE"] = "Видалити поточну групу користувачів";
$MESS["MAIN_NEW_RECORD"] = "Додати групу";
$MESS["MAIN_NEW_RECORD_TITLE"] = "Додати нову групи користувачів";
$MESS["MAIN_TAB"] = "Параметри";
$MESS["MAIN_TAB_TITLE"] = "Параметри групи користувачів";
$MESS["MAIN_TOTAL_USERS"] = "Користувачів:";
$MESS["MAIN_VIEW_USER"] = "Перейти до редагування користувача";
$MESS["MAIN_VIEW_USER_GROUPS"] = "Переглянути користувачів групи";
$MESS["MODULE_RIGHTS"] = "Права до адміністративних частин модулів";
$MESS["MUG_GP_PARENT"] = "Не перевизначати";
$MESS["MUG_POLICY_TITLE"] = "Налаштування політики безпеки";
$MESS["MUG_PREDEFINED_FIELD"] = "Попереднєвизначене налаштування рівня безпеки";
$MESS["MUG_PREDEFINED_HIGH"] = "&nbsp;Підвищений&nbsp;";
$MESS["MUG_PREDEFINED_LOW"] = "&nbsp;Низький&nbsp;";
$MESS["MUG_PREDEFINED_MIDDLE"] = "&nbsp;Звичайний&nbsp;";
$MESS["MUG_PREDEFINED_PARENT"] = "&nbsp;Не перевизначати&nbsp;";
$MESS["MUG_SELECT_LEVEL1"] = "— Виберіть рівень —";
$MESS["NAME"] = "Найменування:";
$MESS["NEW_GROUP_TITLE"] = "Додавання групи";
$MESS["RECORD_LIST"] = "Список груп";
$MESS["RECORD_LIST_TITLE"] = "Перейти до списку груп";
$MESS["RESET"] = "Скинути";
$MESS["RIGHTS_ADD"] = "Додати права для сайту";
$MESS["SAVE"] = "Зберегти зміни";
$MESS["SITE_SELECT"] = "(виберіть сайт)";
$MESS["STRING_ID"] = "Символьний ідентифікатор:";
$MESS["SUBORDINATE_GROUPS"] = "Групи користувачів, профайли яких можна змінювати (переглядати)";
$MESS["TAB_2"] = "Безпека";
$MESS["TAB_3"] = "Доступ";
$MESS["TBL_GROUP"] = "Користувач";
$MESS["TBL_GROUP_DATE"] = "Період активності";
$MESS["USERS"] = "Користувачі в групі";
$MESS["USER_GROUP_DATE_FROM"] = "з";
$MESS["USER_GROUP_DATE_TO"] = "до";
$MESS["USER_LIST"] = "Користувач";
?>