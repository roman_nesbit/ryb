<?
$MESS["H2O_FAVORITES_EDIT_TITLE"] = "Редактирование избранного товара #";
$MESS["H2O_FAVORITES_ADD_TITLE"] = "Добавление избранного товара #";
$MESS["H2O_FAVORITES_LIST"] = "Список избранный товаров";
$MESS["H2O_FAVORITES_LIST_TITLE"] = "Список избранных товаров";
$MESS["H2O_FAVORITES_ADD"] = "Добавить новый избранный товар";
$MESS["H2O_FAVORITES_DELETE"] = "Удалить избранный товар";
$MESS["H2O_FAVORITES_DELETE_CONF"] = "Действительно удалить избранный товар?";
$MESS["H2O_FAVORITES_SAVED"] = "Избранный товар успешно сохранен";
$MESS["H2O_FAVORITES_TAB_MAIN"] = "Основная";
$MESS["H2O_FAVORITES_FIELD_CREATE"] = "";
$MESS["H2O_FAVORITES_ERROR"] = "Ошибка: ";