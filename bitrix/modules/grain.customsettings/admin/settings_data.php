<?

$arCustomPage = array (
  'PARENT_MENU' => 'global_menu_services',
  'SORT' => '1780',
  'LANG' => 
  array (
    'ru' => 
    array (
      'MENU_TEXT' => 'Дополнительные настройки',
      'MENU_TITLE' => 'Дополнительные настройки',
      'PAGE_TITLE' => 'Дополнительные настройки',
    ),
    'en' => 
    array (
      'MENU_TEXT' => 'Additional settings',
      'MENU_TITLE' => 'Additional settings',
      'PAGE_TITLE' => 'Additional settings',
    ),
    'ua' => 
    array (
      'MENU_TEXT' => '',
      'MENU_TITLE' => '',
      'PAGE_TITLE' => '',
    ),
  ),
);

$arCustomSettings = array (
  1 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Бекофис',
        'TITLE' => 'Бекофис',
      ),
      'en' => 
      array (
        'NAME' => 'Backoffice',
        'TITLE' => 'Backoffice',
      ),
      'ua' => 
      array (
        'NAME' => 'Бекофіс',
        'TITLE' => 'Бекофіс',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'ORDER_STATUS_CHAINS',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Переходы статусов заказов',
            'TOOLTIP' => 'Цепочки переходов статусов заказов',
          ),
          'en' => 
          array (
            'NAME' => 'Order status changes',
            'TOOLTIP' => 'Order status changing chains',
          ),
          'ua' => 
          array (
            'NAME' => 'Переходи статусів замовлень',
            'TOOLTIP' => 'Ланцюги переходів статусів замовлень',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '40',
        'ROWS' => '5',
      ),
    ),
  ),
  2 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Каталог товаров',
        'TITLE' => 'Каталог товаров',
      ),
      'en' => 
      array (
        'NAME' => 'Catalog',
        'TITLE' => 'Catalog',
      ),
      'ua' => 
      array (
        'NAME' => 'Каталог товарів',
        'TITLE' => 'Каталог товарів',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'PRODUCT_NEW_PERIOD',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Период для вывода "Новинка" (дн.)',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'New label period (days)',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Період для виведення "Новинка" (дн.)',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '',
        'SIZE' => '20',
      ),
      2 => 
      array (
        'NAME' => 'PROPS_ARE_CHARACTERISTICS',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Свойства, которые выводить в Характеристики',
            'TOOLTIP' => 'Один символьный код свойства в строке',
          ),
          'en' => 
          array (
            'NAME' => 'Свойства, которые выводить в Характеристики',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Свойства, которые выводить в Характеристики',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '40',
        'ROWS' => '10',
      ),
    ),
  ),
  3 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'API Нова пошта',
        'TITLE' => 'API Нова пошта',
      ),
      'en' => 
      array (
        'NAME' => 'API Нова пошта',
        'TITLE' => 'API Нова пошта',
      ),
      'ua' => 
      array (
        'NAME' => 'API Нова пошта',
        'TITLE' => 'API Нова пошта',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'NP_API_URL',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'URL-адрес запроса',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'URL-адрес запроса',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'URL-адрес запроса',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '',
        'SIZE' => '50',
      ),
      2 => 
      array (
        'NAME' => 'NP_API_KEY',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Ключ',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Key',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Ключ',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '',
        'SIZE' => '50',
      ),
      3 => 
      array (
        'NAME' => 'NP_SETTLEMENTS_TEMP_TABLE_NAME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Имя временной таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Имя временной таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Имя временной таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'geo_np_locations_temp',
        'SIZE' => '50',
      ),
      4 => 
      array (
        'NAME' => 'NP_SETTLEMENTS_TABLE_NAME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Имя таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Имя таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Имя таблицы населенных пунктов',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'geo_np_locations',
        'SIZE' => '50',
      ),
      5 => 
      array (
        'NAME' => 'NP_OFFICES_TEMP_TABLE_NAME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Имя временной таблицы отделений',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Имя временной таблицы отделений',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Имя временной таблицы отделений',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'geo_np_offices_temp',
        'SIZE' => '50',
      ),
      6 => 
      array (
        'NAME' => 'NP_OFFICES_TABLE_NAME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Имя таблицы отделений',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Имя таблицы отделений',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Имя таблицы отделений',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'geo_np_offices',
        'SIZE' => '50',
      ),
      7 => 
      array (
        'NAME' => 'NP_REF2CITYREF_TABLE_NAME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Имя таблицы соответствия Ref и CityRef',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Имя таблицы соответствия Ref и CityRef',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Имя таблицы соответствия Ref и CityRef',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'geo_np_ref2cityref',
        'SIZE' => '50',
      ),
    ),
  ),
  4 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Поиск',
        'TITLE' => '',
      ),
      'en' => 
      array (
        'NAME' => 'Search',
        'TITLE' => '',
      ),
      'ua' => 
      array (
        'NAME' => 'Пошук',
        'TITLE' => '',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'FIELDS_TO_SHOW',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Какие параметры показывать в фильтре',
            'TOOLTIP' => 'Один код в строке',
          ),
          'en' => 
          array (
            'NAME' => 'Какие параметры показывать в фильтре',
            'TOOLTIP' => 'Один код в строке',
          ),
          'ua' => 
          array (
            'NAME' => 'Какие параметры показывать в фильтре',
            'TOOLTIP' => 'Один код в строке',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '40',
        'ROWS' => '5',
      ),
    ),
  ),
  5 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Верификация телефона',
        'TITLE' => '',
      ),
      'en' => 
      array (
        'NAME' => 'Phone Verification',
        'TITLE' => '',
      ),
      'ua' => 
      array (
        'NAME' => 'Верифікація телефону',
        'TITLE' => '',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'PV_PERIOD_BETWEEN_SMS',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Задержка между отправками СМС (сек.)',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Задержка между отправками СМС (сек.)',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Задержка между отправками СМС (сек.)',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '60',
        'SIZE' => '5',
      ),
      2 => 
      array (
        'NAME' => 'PV_MAX_SMS_FOR_NUMBER',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Максимальное кол-во СМС на один номер в течение часа',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Максимальное кол-во СМС на один номер в течение часа',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Максимальное кол-во СМС на один номер в течение часа',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '5',
        'SIZE' => '5',
      ),
      3 => 
      array (
        'NAME' => 'PV_MAX_SMS_FROM_IP',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Максимальное кол-во СМС с одного IP в течение часа',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Максимальное кол-во СМС с одного IP в течение часа',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Максимальное кол-во СМС с одного IP в течение часа',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '20',
        'SIZE' => '5',
      ),
      4 => 
      array (
        'NAME' => 'PV_MAX_ATTEMPTS',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Максимальное кол-во попыток ввода кода',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Максимальное кол-во попыток ввода кода',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Максимальное кол-во попыток ввода кода',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '5',
        'SIZE' => '5',
      ),
      5 => 
      array (
        'NAME' => 'PV_TOKEN_LIFETIME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Время жизни токена верифицированного номера (сек.)',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Время жизни токена верифицированного номера (сек.)',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Время жизни токена верифицированного номера (сек.)',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '600',
        'SIZE' => '5',
      ),
      6 => 
      array (
        'NAME' => 'PV_CODE_LIFETIME',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Время жизни кода верификации (сек.)',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Время жизни кода верификации (сек.)',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Время жизни кода верификации (сек.)',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '120',
        'SIZE' => '5',
      ),
      7 => 
      array (
        'NAME' => 'PV_TEST_MODE',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Режим тестирования',
            'TOOLTIP' => 'Код всегда 1234 и нет отправки СМС',
          ),
          'en' => 
          array (
            'NAME' => 'Режим тестирования',
            'TOOLTIP' => 'Код всегда 1234 и нет отправки СМС',
          ),
          'ua' => 
          array (
            'NAME' => 'Режим тестирования',
            'TOOLTIP' => 'Код всегда 1234 и нет отправки СМС',
          ),
        ),
        'TYPE' => 'checkbox',
        'DEFAULT_VALUE' => 'Y',
      ),
    ),
  ),
  6 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Recaptcha',
        'TITLE' => '',
      ),
      'en' => 
      array (
        'NAME' => 'Recaptcha',
        'TITLE' => '',
      ),
      'ua' => 
      array (
        'NAME' => 'Recaptcha',
        'TITLE' => '',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'RECAPTCHA_URL',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Адрес для отправки запроса',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Адрес для отправки запроса',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Адрес для отправки запроса',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'https://www.google.com/recaptcha/api/siteverify',
        'SIZE' => '120',
      ),
      2 => 
      array (
        'NAME' => 'RECAPTCHA_MAX_HUMAN_SCORE',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Макс. оценка для Человека',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Макс. оценка для Человека',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Макс. оценка для Человека',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '0.6',
        'SIZE' => '5',
      ),
      3 => 
      array (
        'NAME' => 'RECAPTCHA_ACTIVE',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Активно',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => 'Активно',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => 'Активно',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'checkbox',
        'DEFAULT_VALUE' => 'Y',
      ),
    ),
  ),
  7 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'СМС',
        'TITLE' => '',
      ),
      'en' => 
      array (
        'NAME' => 'SMS',
        'TITLE' => '',
      ),
      'ua' => 
      array (
        'NAME' => 'СМС',
        'TITLE' => '',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'SMS_TEST_MODE',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Тестовый режим',
            'TOOLTIP' => 'Смс не отправляется',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'checkbox',
        'DEFAULT_VALUE' => 'Y',
      ),
      2 => 
      array (
        'NAME' => 'SMS_NEW_ORDER',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Сообщение о новом заказе',
            'TOOLTIP' => 'Доступные макросы:<br> #ORDER_DATE# - дата заказа<br> #ORDER_NUMBER# - номер заказа<br> #NAME# - имя покапателя<br> #LAST_NAME# - Фамилия покапателя<br> #SUPPORT_PHONE# - телефон поддержки<br> #SUPPORT_EMAIL# - email поддержки<br>#INVOICE_LINK# - ссылка на счет (меняется на текст <b>Счет на оплату: [ссылка]. </b>)',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '80',
        'ROWS' => '5',
      ),
      3 => 
      array (
        'NAME' => 'SMS_SUPPORT_PHONE',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Телефон поддержки',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '+380998887766',
        'SIZE' => '60',
      ),
      4 => 
      array (
        'NAME' => 'SMS_SUPPORT_EMAIL',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Email поддержки',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => 'support@rybalka.ua',
        'SIZE' => '60',
      ),
    ),
  ),
  8 => 
  array (
    'LANG' => 
    array (
      'ru' => 
      array (
        'NAME' => 'Счет на оплату',
        'TITLE' => '',
      ),
      'en' => 
      array (
        'NAME' => 'Invoice',
        'TITLE' => '',
      ),
      'ua' => 
      array (
        'NAME' => 'Рахунок на оплату',
        'TITLE' => '',
      ),
    ),
    'FIELDS' => 
    array (
      1 => 
      array (
        'NAME' => 'INVOICE_PAYSYS_IDS',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Платежные системы',
            'TOOLTIP' => 'Перечень ID платежных систем, при выборе которых создавать счет, через запятую',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'text',
        'DEFAULT_VALUE' => '5',
        'SIZE' => '20',
      ),
      2 => 
      array (
        'NAME' => 'INVOICE_BANK_DATA',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Банковские реквизиты',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '60',
        'ROWS' => '7',
      ),
      3 => 
      array (
        'NAME' => 'INVOICE_CARD_DATA',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Карточные реквизиты',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '60',
        'ROWS' => '3',
      ),
      4 => 
      array (
        'NAME' => 'INVOICE_ATTACHED_TEXT',
        'LANG' => 
        array (
          'ru' => 
          array (
            'NAME' => 'Текст о том, что инвойс прикреплен к сообщению',
            'TOOLTIP' => '',
          ),
          'en' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
          'ua' => 
          array (
            'NAME' => '',
            'TOOLTIP' => '',
          ),
        ),
        'TYPE' => 'textarea',
        'DEFAULT_VALUE' => '',
        'COLS' => '60',
        'ROWS' => '3',
      ),
    ),
  ),
);

?>