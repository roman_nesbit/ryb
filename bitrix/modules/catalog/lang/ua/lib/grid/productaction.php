<?
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_BAD_CATALOG"] = "Інфоблок не є торговим каталогом";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_BAD_IBLOCK_ID"] = "Неправильний ідентифікатор інфоблока";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_CANNOT_MODIFY_SET"] = "Параметри кількісного обліку, вага та одиниця виміру для комплекту встановлюються автоматично";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_CANNOT_MODIFY_SKU"] = "Не можна встановити значення полів для товару з торговими пропозиціями";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_EMPTY_ELEMENTS"] = "Не заданий список елементів для змін";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_EMPTY_FIELDS"] = "Відсутні параметри товару для оновлення";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_SECTION_PRODUCTS_UPDATE"] = "При оновленні товарів розділу [#ID#] #NAME# сталися помилки. Перейдіть у розділ і повторіть операцію.";
?>