<?
$MESS["LANDING_HOOK_HEADBLOCK_CODE"] = "Блок HEAD";
$MESS["LANDING_HOOK_HEADBLOCK_CODE_HELP2"] = "Вставка довільного html-коду для всіх сторінок сайту всередині тега HEAD. Зазвичай це буває код статистики, мета-теги підтвердження, і так далі.";
$MESS["LANDING_HOOK_HEADBLOCK_NAME2"] = " HTML користувача";
$MESS["LANDING_HOOK_HEADBLOCK_USE"] = "Додати/редагувати";
$MESS['LANDING_HOOK_HEADBLOCK_LOCKED'] = '<p>На вашому тарифному плані є обмеження по додаванню HTML коду користувача.</p><p>Щоб використовувати власний код на сайті, перейдіть на інший тарифний план.</p>';
?>