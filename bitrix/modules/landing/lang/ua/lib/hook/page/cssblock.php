<?
$MESS["LANDING_HOOK_HEADBLOCK_CSS_CODE"] = "CSS-код";
$MESS["LANDING_HOOK_HEADBLOCK_CSS_CODE_HELP"] = "Додавання CSS коду для всіх сторінок сайту. Використовуйте, щоб підключити свої шрифти або свої стилі оформлення блоків.";
$MESS["LANDING_HOOK_HEADBLOCK_CSS_FILE"] = "CSS-файл";
$MESS["LANDING_HOOK_CSSBLOCK_NAME"] = "CSS користувача";
$MESS["LANDING_HOOK_HEADBLOCK_USE"] = "Додати/редагувати";
?>