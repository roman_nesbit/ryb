<?
$MESS["LANDING_HOOK_DETAIL_HELP"] = "Детальніше";
$MESS["LANDING_HOOK_GACOUNTER_COUNTER"] = "Google Analytics";
$MESS["LANDING_HOOK_GACOUNTER_LOCKED"] = "<p> Збирайте й аналізуйте статистику відвідуваності сайту за допомогою Google Analytics. Підключення лічильника можливо тільки на розширеному тарифі. </p> <p> Перейдіть на інший тариф і використовуйте більше можливостей у налаштуванні сайту.</p>";
$MESS["LANDING_HOOK_GACOUNTER_PLACEHOLDER"] = "Введіть ID";
$MESS["LANDING_HOOK_GACOUNTER_SEND_CLICK"] = "Відправляти дані про кліки по кнопках і посиланнях";
$MESS["LANDING_HOOK_GACOUNTER_SEND_SHOW"] = "Відправляти дані про перегляд блоків сторінки";
$MESS["LANDING_HOOK_GACOUNTER_USE"] = "Google Analytics";
?>