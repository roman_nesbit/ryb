<?
$MESS["LANDING_HOOK_DETAIL_HELP"] = "Детальніше";
$MESS["LANDING_HOOK_YACOUNTER_COUNTER"] = "Яндекс.Метрика";
$MESS["LANDING_HOOK_YACOUNTER_LOCKED"] = "<p>Збирайте дані про статистику відвідування сайтів і аналізуйте поведінку користувачів за допомогою Яндекс.Метрики. Підключення лічильника можливо тільки на розширеному тарифі.</p> <p>Перейдіть на інший тариф і використовуйте більше можливостей у налаштуванні сайту.</p>";
$MESS["LANDING_HOOK_YACOUNTER_PLACEHOLDER"] = "Введіть ID";
$MESS["LANDING_HOOK_YACOUNTER_USE"] = "Яндекс.Метрика";
?>