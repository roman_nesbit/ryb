<?
$MESS["LANDING_LICENSE_EXPIRED"] = "Ліцензія вашого продукту закінчилася";
$MESS["LANDING_PUBLIC_HTML_DISALLOWED"] = "<p>На вашому тарифному плані є обмеження по додаванню HTML коду користувача.</p><p>Щоб використовувати власний код на сайті, перейдіть на інший тарифний план.</p>";
$MESS["LANDING_PUBLIC_PAGE_REACHED"] = "<p>На вашому тарифному плані є обмеження по кількості опублікованих сторінок. Щоб публікувати нові сторінки, перейдіть на інший тарифний план.</p><p>В верхньому тарифі &laquo;Компанія&raquo; кількість сторінок необмежена.</p>";
$MESS["LANDING_PUBLIC_SITE_REACHED"] = "<p>На вашому тарифному плані є обмеження по кількості опублікованих сайтів. Щоб публікувати нові сайти, перейдіть на інший тарифний план.</p><p>В верхньому тарифі &laquo;Компанія&raquo; кількість сайтів необмежена.</p>";
?>