<?
$MESS["LANDING_CLB_ERROR_DELETE_SMN"] = "Вам потрібно спочатку видалити сторінки відповідного сайту в розділі Сайти24.";
$MESS["LANDING_EXPORT_ERROR"] = "Параметр code може складатися тільки з латинських букв та цифр.";
$MESS["LANDING_TYPE_GROUP"] = "Групи";
$MESS["LANDING_TYPE_KNOWLEDGE"] = "База знань";
$MESS["LANDING_TYPE_PAGE"] = "Лендінг";
$MESS["LANDING_TYPE_PREVIEW"] = "Попередній перегляд";
$MESS["LANDING_TYPE_SMN"] = "Проєкт";
$MESS["LANDING_TYPE_STORE"] = "Магазин";
?>