<?php


namespace Bitrix\Sale\Controller;


use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Sale\Internals\StatusLangTable;

class StatusLang extends Controller
{
	public function listAction($select=[], $filter, $order=[], PageNavigation $pageNavigation)
	{
		$select = empty($select)? ['*']:$select;
		$order = empty($order)? ['STATUS_ID'=>'ASC']:$order;

		$payments = StatusLangTable::getList(
			[
				'select'=>$select,
				'filter'=>$filter,
				'order'=>$order,
				'offset' => $pageNavigation->getOffset(),
				'limit' => $pageNavigation->getLimit()
			]
		)->fetchAll();

		return new Page('STATUS_LANGS', $payments, function() use ($filter)
		{
			return count(
				StatusLangTable::getList(['filter'=>$filter])->fetchAll()
			);
		});
	}
}