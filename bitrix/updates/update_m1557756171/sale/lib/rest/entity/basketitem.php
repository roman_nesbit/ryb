<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Main\Error;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Result;

class BasketItem extends Base
{
	public function getFields()
	{
		return [
			'ORDER_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[
					Attributes::Immutable,
					Attributes::Required
				]
			],
			'PRODUCT_PROVIDER_CLASS'=>[
				'TYPE'=>self::TYPE_STRING,
			],
			'MODULE'=>[
				'TYPE'=>self::TYPE_STRING,
			],
			'TYPE'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'SET_PARENT_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'NAME'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[
					Attributes::Required //for builder
				]
			],
			'LID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'SORT'=>[
				'TYPE'=>self::TYPE_INT
			],
			'PRODUCT_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required //for builder
				]
			],
			'PRODUCT_PRICE_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'PRICE_TYPE_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'CATALOG_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'PRODUCT_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'DETAIL_PAGE_URL'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'BASE_PRICE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'PRICE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'DISCOUNT_PRICE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'CURRENCY'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'CUSTOM_PRICE'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'QUANTITY'=>[
				'TYPE'=>self::TYPE_FLOAT,
				'ATTRIBUTES'=>[
					Attributes::Required //for builder
				]
			],
			'WEIGHT'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'DIMENSIONS'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'MEASURE_CODE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'MEASURE_NAME'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'NOTES'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'VAT_RATE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'VAT_INCLUDED'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'BARCODE_MULTI'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'SUBSCRIBE'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'DISCOUNT_NAME'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'DISCOUNT_VALUE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'DISCOUNT_COUPON'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'RECOMMENDATION'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'DATE_INSERT'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'DATE_UPDATE'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'DATE_REFRESH'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'PROPERTIES'=>[
				'TYPE'=>self::TYPE_LIST,
				'ATTRIBUTES'=>[Attributes::Hidden]
			]
		];
	}

	public function internalizeFieldsModify($fields)
	{
		$result = [];

		if(isset($fields['ORDER']['ID']))
			$result['ORDER']['ID'] = (int)$fields['ORDER']['ID'];

		if(isset($fields['ORDER']['BASKET_ITEMS']))
			$result['ORDER']['BASKET_ITEMS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['BASKET_ITEMS'], new \Bitrix\Sale\Rest\Entity\BasketItem(), ['PROPERTIES']);

		foreach ($result['ORDER']['BASKET_ITEMS'] as $k=>$items)
		{
			if(isset($items['PROPERTIES']))
				$result['ORDER']['BASKET_ITEMS'][$k]['PROPERTIES'] = $this->internalizeFieldsCollectionWithExcludeFields($items['PROPERTIES'], new \Bitrix\Sale\Rest\Entity\BasketProperties());
		}
		return $result;
	}

	public function externalizeFields($fields)
	{
		$basketProperties = new \Bitrix\Sale\Rest\Entity\BasketProperties();

		$result = parent::externalizeFields($fields);

		if(isset($fields['PROPERTIES']))
			$result['PROPERTIES'] = $basketProperties->externalizeListFields($fields['PROPERTIES']);

		return $result;
	}

	public function externalizeFieldsModify($fields)
	{
		return $this->externalizeListFields($fields);
	}

	public function checkFieldsModify($fields)
	{
		$r = new Result();

		$emptyFields = [];
		if(!isset($fields['ORDER']['ID']))
		{
			$emptyFields[] = '[order][id]';
		}
		if(!isset($fields['ORDER']['BASKET_ITEMS']) || !is_array($fields['ORDER']['BASKET_ITEMS']))
		{
			$emptyFields[] = '[order][basketItems][]';
		}

		if(count($emptyFields)>0)
		{
			$r->addError(new Error('Required fields: '.implode(', ', $emptyFields)));
		}
		else
		{
			$r = parent::checkFieldsModify($fields);
		}

		return $r;
	}

	public function checkRequiredFieldsModify($fields)
	{
		$r = new Result();

		$basketProperties = new BasketProperties();

		foreach ($fields['ORDER']['BASKET_ITEMS'] as $k=>$fieldsBasketItem)
		{
			$required = $this->checkRequiredFields($fieldsBasketItem, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly, Attributes::Immutable]]);
			if(!$required->isSuccess())
			{
				$r->addError(new Error('[basketItems]['.$k.'] - '.implode(', ', $required->getErrorMessages()).'.'));
			}

			if(isset($fieldsBasketItem['PROPERTIES']))
			{
				$requiredProperties = $basketProperties->checkRequiredFieldsModify(['BASKET_ITEM'=>['PROPERTIES'=>$fieldsBasketItem['PROPERTIES']]]);
				if(!$requiredProperties->isSuccess())
				{
					$requiredPropertiesFields = [];
					foreach ($requiredProperties->getErrorMessages() as $errorMessage)
					{
						$requiredPropertiesFields[] = '[basketItems]['.$k.']'.$errorMessage;
					}
					$r->addError(new Error(implode( ' ', $requiredPropertiesFields)));
				}

			}
		}
		return $r;
	}
}