<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Sale\Rest\Attributes;

class TradeBinding extends Base
{
	public function getFields()
	{
		return [
			'ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'ORDER_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'EXTERNAL_ORDER_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'PARAMS'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'TRADING_PLATFORM_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'TRADING_PLATFORM_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Immutable]
			],
		];
	}

	protected function getRewritedFields()
	{
		return [
			'TRADING_PLATFORM_XML_ID'=>[
				'REFERENCE_FIELD'=>'TRADING_PLATFORM.XML_ID'
			]
		];
	}

	public function internalizeFieldsModify($fields)
	{
		//TODO: �������� rest ��� ��������� ������
		parent::internalizeFieldsModify($fields);
	}
}