<?
// install files
if (IsModuleInstalled('landing'))
{
	$updater->CopyFiles('install/blocks', 'blocks');
	$updater->CopyFiles('install/components', 'components');
	$updater->CopyFiles('install/images', 'images');
	$updater->CopyFiles('install/js', 'js');
	$updater->CopyFiles('install/templates', 'templates');
}

if (defined('BX_COMP_MANAGED_CACHE'))
{
	$GLOBALS['CACHE_MANAGER']->clearByTag('landing_blocks');
	$GLOBALS['CACHE_MANAGER']->clearByTag('landing_demo');
}

// run block updaters
if ($updater->CanUpdateDatabase() && \Bitrix\Main\Loader::includeModule('landing'))
{
	$updateBlocks = [
		'store.catalog.list',
		'store.catalog.detail',
		'store.catalog.filter',
		'store.catalog.list',
		'store.order',
		'store.payment',
		'store.personal',
	];
	foreach ($updateBlocks as $code)
	{
		\Bitrix\Landing\Update\Block::register(
			$code
		);
	}
}

// create new tables
if ($updater->CanUpdateDatabase() && $updater->TableExists('b_landing'))
{
	if ($DB->type == 'MYSQL')
	{
		if (!$updater->TableExists('b_landing_entity_rights'))
		{
			$DB->Query("
				create table if not exists b_landing_entity_rights (
					ID int(18) not null auto_increment,
					ENTITY_ID int(18) not null,
					ENTITY_TYPE char(1) not null,
					TASK_ID int(11) not null,
					ACCESS_CODE varchar(50) not null,
					ROLE_ID int(18) default 0,
					INDEX IX_ENTITY (ENTITY_ID, ENTITY_TYPE),
					INDEX IX_ROLE (ROLE_ID),
					PRIMARY KEY (ID)
				);
			");
		}

		if (!$updater->TableExists('b_landing_role'))
		{
			$DB->Query("
				create table if not exists b_landing_role (
					ID int(18) not null auto_increment,
					TITLE varchar(255) default null,
					XML_ID varchar(255) default null,
					ACCESS_CODES text default null,
					ADDITIONAL_RIGHTS text default null,
					CREATED_BY_ID int(18) not null,
					MODIFIED_BY_ID int(18) not null,
					DATE_CREATE timestamp null,
					DATE_MODIFY timestamp not null,
					PRIMARY KEY(ID)
				);
			");
		}
	}
}

// install tasks of rights
if (IsModuleInstalled('landing') && $updater->CanUpdateDatabase())
{
	if ($updater->CanUpdateKernel())
	{
		$strBasePath = $_SERVER['DOCUMENT_ROOT'] . $updater->curModulePath;
	}
	else
	{
		$strBasePath = $_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/landing';
	}
	include_once($strBasePath . '/install/index.php');

	$module = new landing();
	$module->InstallTasks();
}

// remove old files
if ($updater->CanUpdateKernel())
{
	$arToDelete = array(
		'images/landing/business/830x860/1.jpg',
		'modules/landing/install/images/landing/business/830x860/1.jpg',
		'images/landing/business/830x860/2.jpg',
		'modules/landing/install/images/landing/business/830x860/2.jpg',
	);
	foreach($arToDelete as $file)
	{
		CUpdateSystem::DeleteDirFilesEx($_SERVER['DOCUMENT_ROOT'].$updater->kernelPath.'/'.$file);
	}
}

// UPDATERS
if (IsModuleInstalled('landing'))
{
	$fixesUpdater = [
		'PARAMS' => [
			'UPDATE_PUBLISHED_SITES' => 'N',
		],
		'BLOCKS' => [],
	];

//		find unique updaterId by current time
	$updaterUniqueId = time();
	while (true)
	{
		if (\Bitrix\Main\Config\Option::get('landing', 'blocks_attrs_update' . $updaterUniqueId) == '')
		{
			break;
		}
		$updaterUniqueId++;
	}
	\Bitrix\Main\Config\Option::set(
		'landing', 'blocks_attrs_update' . $updaterUniqueId, serialize($fixesUpdater)
	);
	
	
	$bugsUpdater = [
		'PARAMS' => [
			'UPDATE_PUBLISHED_SITES' => 'Y',
		],
		'BLOCKS' => [
		],
	];

//		find unique updaterId by current time
	$updaterUniqueId = time();
	while (true)
	{
		if (\Bitrix\Main\Config\Option::get('landing', 'blocks_attrs_update' . $updaterUniqueId) == '')
		{
			break;
		}
		$updaterUniqueId++;
	}
	\Bitrix\Main\Config\Option::set(
		'landing', 'blocks_attrs_update' . $updaterUniqueId, serialize($bugsUpdater)
	);


//		for all
	\Bitrix\Main\Update\Stepper::bindClass(
		'Bitrix\Landing\Update\Block\NodeAttributes', 'landing', 60
	);


//		-----------
//		NEW updater
	\Bitrix\Landing\Update\Block::register([
		"44.1.four_columns_with_img_and_text",
		"15.social",
	]);
	
	\Bitrix\Landing\Update\Block::register(
		"36.1.shedule",
		[
			'.landing-block-node-card-time-line' => [
				'new_class' => ['g-mb-0--md', 'order-2', 'g-mb-0'],
				'remove_class' => ['g-mb-0--sm', 'order-1', 'order-md-2', 'g-mb-20', 'g-mb-0--md'],
			],
			'.landing-block-node-card' => [
				'new_class' => ['u-timeline-v3-2'],
				'remove_class' => ['u-timeline-v3'],
			],
			'.landing-block-node-card-img-container' => [
				'new_class' => ['order-1 g-mb-20 g-mb-0--md'],
				'remove_class' => ['order-2 order-md-1'],
			],
		]
	);
	
	\Bitrix\Landing\Update\Block::register(
		"44.5.three_cols_images_with_price",
		[
			'.landing-block-node-card-bg-hover' => [
				'new_class' => ['opacity-0 g-opacity-1--parent-hover g-transition-0_2 g-transition--ease-in'],
			],
		]
	);
	
	\Bitrix\Landing\Update\Block::register(
		"45.1.gallery_app_wo_slider",
		[
			'.landing-block-node-card-title-container' => [
				'new_class' => ['g-pointer-events-all'],
			],
		]
	);
	
	\Bitrix\Landing\Update\Block::register(
		"45.2.gallery_app_with_slider",
		[
			'.landing-block-node-card' => [
				'new_class' => ['g-px-15'],
				'remove_class' => ['g-min-width-300'],
			],
			'.landing-block-node-card-title-container' => [
				'new_class' => ['g-pointer-events-all'],
			],
		]
	);
}

// new blocks
if (IsModuleInstalled('landing') && $updater->CanUpdateDatabase())
{
	\Bitrix\Main\Config\Option::set('landing', 'new_blocks', serialize(array(
	'date' => time(),
		'items' => array(
			'55.1.list_of_links',
			'52.4.mini_text_titile_with_btn_left',
			'52.3.mini_text_titile_with_btn_right',
			'35.8.header_logo_and_slogan_row',
			'35.7.header_logo_and_slogan',
		)
	)));
}

?>