<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;

return array(
	'code' => 'store-chats-light/mainpage',
	'name' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_LIGHT-MAIN-NAME'),
	'description' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_LIGHT-MAIN-DESC'),
	'type' => 'store',
	'version' => 2,
	'fields' => array(
		'RULE' => NULL,
		'ADDITIONAL_FIELDS' => array(
			'VIEW_USE' => 'N',
			'VIEW_TYPE' => 'no',
			'THEME_CODE' => 'app',
			'THEME_CODE_TYPO' => 'app',
		),
	),
	'site_group_item' => 'Y',
	
	'items' => array(
		'0' => array(
			'code' => '01.big_with_text_3',
			'nodes' => array(
				'.landing-block-node-img' => array(
					0 => array(
						'src' => 'https://cdn.bitrix24.site/bitrix/images/landing/business/1400x700/img9.jpg',
						'data-pseudo-url' => '{"text":"","href":"","target":"_self","enabled":false}',
					),
				),
				'.landing-block-node-title' => array(
					0 => 'LIGHT variant',
				),
				'.landing-block-node-text' => array(
					0 => 'Store on chats test',
				),
			),
			'style' => array(
				'.landing-block-node-container' => array(
					0 => 'landing-block-node-container container g-max-width-800 js-animation fadeInDown text-center u-bg-overlay__inner g-mx-1',
				),
				'.landing-block-node-button-container' => array(
					0 => 'landing-block-node-button-container',
				),
				'.landing-block-node-title' => array(
					0 => 'landing-block-node-title g-font-weight-700 g-color-white g-mb-20 g-text-transform-none g-font-montserrat g-font-size-86 g-line-height-1_2',
				),
				'.landing-block-node-text' => array(
					0 => 'landing-block-node-text g-color-white-opacity-0_7 g-mb-35 g-font-montserrat',
				),
				'.landing-block-node-button' => array(
					0 => 'landing-block-node-button btn btn-xl u-btn-primary text-uppercase g-font-weight-700 g-font-size-12 g-rounded-50 g-py-15 g-px-40',
				),
				'#wrapper' => array(
					0 => 'landing-block landing-block-node-img u-bg-overlay g-flex-centered g-min-height-100vh g-bg-img-hero g-bg-black-opacity-0_5--after g-pt-80 g-pb-80',
				),
			),
		),
	),
);