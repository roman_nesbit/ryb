<?
$MESS["LANDING_FEATURE_NOT_AVAIL_ERROR"] = "Die Funktionalität der Zugriffsrechte ist in Ihrem aktuellen Tarif nicht verfügbar.";
$MESS["LANDING_FILE_ERROR"] = "Das Datei-Upload ist fehlgeschlagen. Diese Datei ist wahrscheinlich keine Bilddatei.";
$MESS["LANDING_IS_NOT_ADMIN_ERROR"] = "Für diese Aktion sind die Zugriffsrechte eines Administrators erforderlich.";
?>