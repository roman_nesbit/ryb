<?
$MESS["LANDING_HOOK_HEADBLOCK_CODE"] = "HEAD-Block";
$MESS["LANDING_HOOK_HEADBLOCK_CODE_HELP2"] = "Fügen Sie benutzerdefinierten HTML-Code (Statistik-Tracker, Meta-Tags etc.) zum HEAD-Tag auf allen Seiten hinzu.";
$MESS["LANDING_HOOK_HEADBLOCK_CSS_CODE"] = "CSS-Code";
$MESS["LANDING_HOOK_HEADBLOCK_CSS_CODE_HELP"] = "Fügt einen CSS-Code zu allen Website-Seiten hinzu. Nutzen Sie es, um benutzerdefinierte Schriften oder Stile zu definieren.";
$MESS["LANDING_HOOK_HEADBLOCK_CSS_FILE"] = "CSS-Datei";
$MESS["LANDING_HOOK_HEADBLOCK_NAME"] = "Benutzerdefinierte HTML und CSS";
$MESS["LANDING_HOOK_HEADBLOCK_USE"] = "Hinzufügen / Bearbeiten";
?>