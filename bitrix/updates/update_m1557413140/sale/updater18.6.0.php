<?
if(IsModuleInstalled('sale'))
{
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/services", "services");
	$updater->CopyFiles("install/tools", "tools");
	$updater->CopyFiles("install/components", "components");
}
if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sale_auxiliary'))
{
	if ($DB->type == "MYSQL")
	{
		if (!$updater->TableExists("b_sale_usergroup_restr"))
		{
			$DB->Query("
				CREATE TABLE b_sale_usergroup_restr(
					ID INT NOT NULL AUTO_INCREMENT,
					ENTITY_ID INT NOT NULL,
					ENTITY_TYPE_ID INT NOT NULL,
					GROUP_ID INT NOT NULL,
					PRIMARY KEY (ID)
				);
			");
		}
		if ($updater->TableExists("b_sale_usergroup_restr"))
		{
			if (!$DB->IndexExists("b_sale_usergroup_restr", array("ENTITY_TYPE_ID", "ENTITY_ID", "GROUP_ID", ), true))
			{
				$DB->Query("CREATE INDEX IX_BSUG_ETI_EI_GI ON b_sale_usergroup_restr(ENTITY_TYPE_ID, ENTITY_ID, GROUP_ID)");
			}
		}

		if ($updater->TableExists("b_sale_order"))
		{
			$DB->Query("UPDATE b_sale_order SET XML_ID = concat('bx_', md5(concat('b_sale_order', ID))) where XML_ID is null");
		}		
		if ($updater->TableExists("b_sale_basket"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_basket WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_basket ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_basket SET XML_ID = concat('bx_', md5(concat('b_sale_basket', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_basket_props"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_basket_props WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_basket_props ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_basket_props SET XML_ID = concat('bx_', md5(concat('b_sale_basket_props', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_person_type"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_person_type WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_person_type ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_person_type SET XML_ID = concat('bx_', md5(concat('b_sale_person_type', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_order_props"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_order_props WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_props ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_order_props SET XML_ID = concat('bx_', md5(concat('b_sale_order_props', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_order_props_value"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_order_props_value WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_props_value ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_order_props_value SET XML_ID = concat('bx_', md5(concat('b_sale_order_props_value', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_order_props_variant"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_order_props_variant WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_props_variant ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_order_props_variant SET XML_ID = concat('bx_', md5(concat('b_sale_order_props_variant', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_pay_system_action"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD XML_ID varchar(255) null");				
			}
			$DB->Query("UPDATE b_sale_pay_system_action SET XML_ID = concat('bx_', md5(concat('b_sale_pay_system_action', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_status"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_status WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_status ADD XML_ID varchar(255) null");
			}
			$DB->Query("UPDATE b_sale_status SET XML_ID = concat('bx_', md5(concat('b_sale_status', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_order_dlv_basket"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_order_dlv_basket WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_dlv_basket ADD XML_ID varchar(255) null");
			}
			$DB->Query("UPDATE b_sale_order_dlv_basket SET XML_ID = concat('bx_', md5(concat('b_sale_order_dlv_basket', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_tp"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_tp WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_tp ADD XML_ID varchar(255) null");
			}
			$DB->Query("UPDATE b_sale_tp SET XML_ID = concat('bx_', md5(concat('b_sale_tp', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_delivery_srv"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_delivery_srv WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_delivery_srv ADD XML_ID varchar(255) null");
			}
			$DB->Query("UPDATE b_sale_delivery_srv SET XML_ID = concat('bx_', md5(concat('b_sale_delivery_srv', ID))) where XML_ID is null");
		}
		if ($updater->TableExists("b_sale_tp_order"))
		{
			if (!$DB->Query("SELECT XML_ID FROM b_sale_tp_order WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_tp_order ADD XML_ID varchar(255) null");
			}
			$DB->Query("UPDATE b_sale_tp_order SET XML_ID = concat('bx_', md5(concat('b_sale_tp_order', ID))) where XML_ID is null");
		}
		if (!$updater->TableExists("b_sale_synchronizer_log"))
		{
			$DB->Query("
				CREATE TABLE b_sale_synchronizer_log(
					ID INT NOT NULL AUTO_INCREMENT,
					MESSAGE_ID TEXT NULL,
					MESSAGE LONGTEXT NULL,
					DATE_INSERT DATETIME NULL DEFAULT NULL,
					PRIMARY KEY (ID)
				);
			");
		}
		if ($updater->TableExists("b_sale_synchronizer_log"))
		{
			if (!$DB->IndexExists("b_sale_synchronizer_log", array("DATE_INSERT", )))
			{
				$DB->Query("CREATE INDEX IX_SYNCHRONIZER_LOG1 ON b_sale_synchronizer_log(DATE_INSERT)");
			}
		}

		if ($updater->TableExists("b_sale_order_props") && $updater->TableExists("b_sale_person_type") && $updater->TableExists("b_sale_person_type_site"))
		{
			$DB->Query("DELETE bspt FROM b_sale_person_type bspt
						LEFT JOIN b_sale_order_props bsop ON bsop.PERSON_TYPE_ID=bspt.ID
						WHERE bsop.ID IS NULL AND (bspt.CODE='CRM_CONTACT' OR bspt.CODE='CRM_COMPANY')
			");

			$DB->Query("DELETE bspts FROM b_sale_person_type_site bspts
						LEFT JOIN b_sale_person_type bspt ON bspts.PERSON_TYPE_ID=bspt.ID
						WHERE bspt.ID IS NULL
			");
		}
	}
}

if(IsModuleInstalled('sale') && $updater->CanUpdateDatabase())
{
	RegisterModuleDependences("rest", "OnRestServiceBuildDescription", "sale", "\\Bitrix\\Sale\\Rest\\RestManager", "onRestServiceBuildDescription");
	UnRegisterModuleDependences('sale', 'OnSaleOrderSaved', 'sale', '\Bitrix\Sale\Compatible\EventCompatibility', 'onOrderAdd');
}

?><?
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/sale/lib/controller/basket.php",
		"modules/sale/lib/rest/entity/basket.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
