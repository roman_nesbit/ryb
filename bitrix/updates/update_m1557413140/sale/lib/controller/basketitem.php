<?php


namespace Bitrix\Sale\Controller;


use Bitrix\Main\Engine\AutoWire\ExactParameter;
use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Sale\Helpers\Order\Builder\SettingsContainer;
use Bitrix\Sale\Provider;

class BasketItem extends Controller
{
	public function getPrimaryAutoWiredParameter()
	{
		return new ExactParameter(
			\Bitrix\Sale\BasketItem::class,
			'basket',
			function($className, $id) {

				$r = \Bitrix\Sale\Basket::getList([
					'select'=>['ORDER_ID'],
					'filter'=>['ID'=>$id]
				]);

				if($row = $r->fetch())
				{
					$order = \Bitrix\Sale\Order::load($row['ORDER_ID']);
					$basket = $order->getBasket()->getItemByBasketCode($id);
					if($basket instanceof \Bitrix\Sale\BasketItem)
						return $basket;
				}
				else
				{
					$this->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_BASEKT_NOT_EXISTS', ['#ID#'=>$id]), 'BASEKT_NOT_EXISTS'));
				}
				return null;

			}
		);
	}

	public function getAutoWiredParameters()
	{
		return [
			new \Bitrix\Main\Engine\AutoWire\Parameter(
				\Bitrix\Sale\BasketItem::class,
				function($className, $id) {

					$r = \Bitrix\Sale\Basket::getList([
						'select'=>['ORDER_ID'],
						'filter'=>['ID'=>$id]
					]);

					if($row = $r->fetch())
					{
						$order = \Bitrix\Sale\Order::load($row['ORDER_ID']);
						$basket = $order->getBasket()->getItemByBasketCode($id);
						if($basket instanceof \Bitrix\Sale\BasketItem)
							return $basket;
					}
					else
					{
						$this->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_BASEKT_NOT_EXISTS', ['#ID#'=>$id]), 'BASEKT_NOT_EXISTS'));
					}
					return null;
				}
			)
		];
	}

	//region Actions
	public function getFieldsAction()
	{
		$entity = new \Bitrix\Sale\Rest\Entity\BasketItem();
		return ['BASKET_ITEM'=>$entity->prepareFieldInfos(
			$entity->getFields()
		)];
	}

	public function modifyAction(array $fields)
	{
		$builder = $this->getBuilder();
		$builder->buildEntityBasket($fields);

		if($builder->getErrorsContainer()->getErrorCollection()->count()>0)
		{
			$this->addErrors($builder->getErrorsContainer()->getErrors());
			return null;
		}

		$order = $builder->getOrder();

		$r = $order->save();
		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}

		//TODO: return $basket->toArray();
		return ['BASKET_ITEMS'=>$this->toArray($order)['ORDER']['BASKET']['ITEMS']];
	}

	public function addAction(array $fields)
	{
		$data = [];

		$data['ORDER']['ID'] = $fields['ORDER_ID'];
		$data['ORDER']['BASKET_ITEMS'] = [$fields];

		$builder = $this->getBuilder(
			new SettingsContainer([
				'deleteBaketItemsIfNotExists' => false
			])
		);
		$builder->buildEntityBasket($data);

		if($builder->getErrorsContainer()->getErrorCollection()->count()>0)
		{
			$this->addErrors($builder->getErrorsContainer()->getErrors());
			return null;
		}

		$order = $builder->getOrder();

		$idx=0;
		$collection = $order->getBasket();
		/** @var \Bitrix\Sale\BasketItem $basketItem */
		foreach($collection as $basketItem)
		{
			if($basketItem->getId() <= 0)
			{
				$idx = $basketItem->getInternalIndex();
				break;
			}
		}

		$r = $order->save();
		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}

		//TODO: return $basketItem->toArray();
		return new Page('BASKET_ITEM', $this->get($order->getBasket()->getItemByIndex($idx)), 1);
	}

	public function updateAction(\Bitrix\Sale\BasketItem $basket, array $fields)
	{
		$data = [];

		$fields['ID'] = $basket->getBasketCode();
		$fields['ORDER_ID'] = $basket->getCollection()->getOrderId();

		$data['ORDER']['ID'] = $fields['ORDER_ID'];
		$data['ORDER']['BASKET_ITEMS'] = [$fields];

		$builder = $this->getBuilder(
			new SettingsContainer([
				'deleteBaketItemsIfNotExists' => false
			])
		);

		$builder->buildEntityBasket($data);

		if($builder->getErrorsContainer()->getErrorCollection()->count()>0)
		{
			$this->addErrors($builder->getErrorsContainer()->getErrors());
			return null;
		}

		$order = $builder->getOrder();

		$r = $order->save();
		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}

		//TODO: return $order->toArray();
		return new Page('BASKET_ITEM', $this->get($basket), 1);
	}

	public function getAction(\Bitrix\Sale\BasketItem $basket)
	{
		//TODO: $basketItem->toArray();
		return ['BASKET_ITEM' => $this->get($basket)];
	}

	public function deleteAction(\Bitrix\Sale\BasketItem $basket)
	{
		/** @var \Bitrix\Sale\Basket $basketCollection */
		$basketCollection = $basket->getCollection();
		$order = $basketCollection->getOrder();

		$r = $basket->delete();
		if($r->isSuccess())
			$r = $order->save();

		if(!$r->isSuccess())
			$this->addErrors($r->getErrors());

		return $r->isSuccess();
	}

	public function listAction($select=[], $filter=[], $order=[], PageNavigation $pageNavigation)
	{
		$select = empty($select)? ['*']:$select;
		$order = empty($order)? ['ID'=>'ASC']:$order;

		$items = \Bitrix\Sale\Basket::getList(
			[
				'select'=>$select,
				'filter'=>$filter,
				'order'=>$order,
				'offset' => $pageNavigation->getOffset(),
				'limit' => $pageNavigation->getLimit()
			]
		)->fetchAll();

		return new Page('BASKET_ITEMS', $items, function() use ($filter)
		{
			return count(
				\Bitrix\Sale\Basket::getList(['filter'=>$filter])->fetchAll()
			);
		});
	}

	public function canBuyAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->canBuy()?'Y':'N';
	}

	public function getBasePriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getBasketCode();
	}

	public function getBasePriceWithVatAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getBasePriceWithVat();
	}

	public function getCurrencyAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getCurrency();
	}

	public function getDefaultPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getDefaultPrice();
	}

	public function getDiscountPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getDiscountPrice();
	}

	public function getFinalPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getFinalPrice();
	}

	public function getInitialPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getInitialPrice();
	}

	public function getPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getPrice();
	}

	public function getPriceWithVatAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getPriceWithVat();
	}

	public function getProductIdAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getProductId();
	}

	public function getQuantityAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getQuantity();
	}

	public function getReservedQuantityAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getReservedQuantity();
	}

	public function getVatAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getVat();
	}

	public function getVatRateAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getVatRate();
	}

	public function getWeightAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->getWeight();
	}

	public function isBarcodeMultiAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->isBarcodeMulti();
	}

	public function isCustomMultiAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->isCustom();
	}

	public function isCustomPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->isCustomPrice();
	}

	public function isDelayAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->isDelay();
	}

	public function isVatInPriceAction(\Bitrix\Sale\BasketItem $basket)
	{
		return $basket->isVatInPrice();
	}

	public function checkProductBarcodeAction(\Bitrix\Sale\BasketItem $basket, array $fields)
	{
		$r = false;
		if ($basket)
		{
			/*
			 * fields = ['BARCODE'=>..., 'STORE_ID'=>...];
			 * */
			$r = Provider::checkProductBarcode($basket, $fields);
		}

		if ($r)
			return true;
		else
		{
			$this->addError(new Error('','BARCODE_CHECK_FAILED'));
			return null;
		}

	}

	//endregion

	protected function get(\Bitrix\Sale\BasketItem $basketItem, array $fields=[])
	{
		$items = $this->toArray($basketItem->getCollection()->getOrder(), $fields)['ORDER']['BASKET']['ITEMS'];
		foreach ($items as $item)
		{
			if($item['ID'] == $basketItem->getId())
			{
				return $item;
			}
		}
		return [];
	}

	static public function prepareFields($fields)
	{
		$data = null;
		Loader::includeModule('catalog');

		if(isset($fields['BASKET_ITEMS']))
		{
			$i=0;
			foreach ($fields['BASKET_ITEMS'] as $item)
			{
				$item['OFFER_ID'] = $item['PRODUCT_ID'];
				unset($item['PRODUCT_ID']);

				$item['CUSTOM_PRICE'] = isset($item['PRICE'])? 'Y':'N';

				$item['MODULE'] = isset($item['MODULE'])? $item['MODULE']:null;
				$item['PRODUCT_PROVIDER_CLASS'] = isset($item['PRODUCT_PROVIDER_CLASS'])? $item['PRODUCT_PROVIDER_CLASS']:null;

				if($item['MODULE'] == 'catalog' && $item['PRODUCT_PROVIDER_CLASS'] === null)
				{
					$item['PRODUCT_PROVIDER_CLASS'] = \Bitrix\Catalog\Product\Basket::getDefaultProviderName();
				}

				$properties = isset($item['PROPERTIES'])? $item['PROPERTIES']:[];
				foreach ($properties as &$property)
				{
					if(isset($property['BASKET_ID']))
						unset($property['BASKET_ID']);
				}

				$item['PROPS'] = $properties;
				unset($item['PROPERTIES']);

				$basketCode = isset($item['ID'])? $item['ID']:'n'.++$i;
				if(isset($item['ID']))
					unset($item['ID']);

				$data[$basketCode] = $item;
			}
			unset($fields['BASKET_ITEMS']);
		}

		return is_array($data)?['PRODUCT'=>$data]:[];
	}

	protected function checkPermission($name)
	{
		if(substr($name,0,6) === 'canbuy')
		{
			$name = 'get';
		}

		parent::checkPermission($name);
	}
}