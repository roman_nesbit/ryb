<?php


namespace Bitrix\Sale\Controller;


use Bitrix\Main\Engine\AutoWire\ExactParameter;
use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\RandomSequence;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Sale\Helpers\Order\Builder\BuildingException;
use Bitrix\Sale\PaySystem\Manager;
use Bitrix\Sale\Registry;
use Bitrix\Sale\Rest\Synchronization\Loader\Factory;
use Bitrix\Sale\Rest\Synchronization\LoggerDiag;
use Bitrix\Sale\Result;

class Order extends Controller
{
	public function getPrimaryAutoWiredParameter()
	{
		return new ExactParameter(
			\Bitrix\Sale\Order::class,
			'order',
			function($className, $id) {

				/** @var \Bitrix\Sale\Order $className */
				$order = $className::load($id);
				if($order instanceof $className)
				{
					return $order;
				}
				else
				{
					$this->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_ORDER_NOT_EXISTS', ['#ID#'=>$id]), 'ORDER_NOT_EXISTS'));
				}
				return null;
			}
		);
	}

	public function getAutoWiredParameters()
	{
		return [
			new \Bitrix\Main\Engine\AutoWire\Parameter(
				\Bitrix\Sale\Order::class,
				function($className, $id) {

					/** @var \Bitrix\Sale\Order $className */
					$order = $className::load($id);
					if($order instanceof $className)
					{
						return $order;
					}
					else
					{
						$this->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_ORDER_NOT_EXISTS', ['#ID#'=>$id]), 'ORDER_NOT_EXISTS'));
					}
					return null;
				})
			];
	}

	//region Actions
	public function getFieldsAction()
	{
		$entity = new \Bitrix\Sale\Rest\Entity\Order();
		return ['ORDER'=>$entity->prepareFieldInfos(
			$entity->getFields()
		)];
	}

	public function getAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->toArray();
		return $this->toArray($order);
	}

	public function tryModifyAction(array $fields)
	{
		$r = $this->modify($fields);

		if($r->isSuccess())
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $r->getData()['ORDER'];

			$result = $this->toArray($order);

			$result['ORDER'] = array_merge(
				$result['ORDER'], [
					'DELIIVERY_ID_LIST_ACTION'=>$this->getDeliveryIdListAction($order),
					'PAY_SYSTEM_ID_LIST'=>$this->getPaySystemIdListAction($order)
				]
			);

			if(is_array($result['PAYMENTS']))
			{
				foreach ($result['PAYMENTS'] as &$fields)
				{
					$paySystems = Manager::getListWithRestrictions(
						$order
							->getPaymentCollection()
							->getItemByIndex($fields['ENTITY']['INTERNAL_INDEX'])
					);

					foreach ($paySystems as $paySystem)
					{
						if((int)$paySystem['PAY_SYSTEM_ID']>0) //Without Inner
						{
							$fields['ENTITY']['LIST_PAY_SYSTEM_WITH_RESTRICTIONS'][]=[
								'ID'=>$paySystem['PAY_SYSTEM_ID'],
							];
						}
					}
				}
			}

			if(is_array($result['SHIPMENTS']))
			{
				foreach ($result['SHIPMENTS'] as &$fields)
				{
					$services = \Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList(
						$order
							->getShipmentCollection()
							->getItemByIndex($fields['ENTITY']['INTERNAL_INDEX'])
					);

					foreach ($services as $service)
					{
						$fields['ENTITY']['LIST_DELIIVERY_SERVICES_RESTRICTIONS'][]=[
							'ID'=>$service->getId(),
						];
					}
				}
			}
			return $result;
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function modifyAction(array $fields)
	{
		$r = $this->modify($fields);

		if($r->isSuccess())
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $r->getData()['ORDER'];

			$r = $order->save();
			if(!$r->isSuccess())
			{
				$this->addErrors($r->getErrors());
				return null;
			}
			elseif($r->hasWarnings())
			{
				$this->addErrors($r->getWarnings());
				return null;
			}

			//TODO: return $order->toArray();
			return $this->toArray($order);
		}
		elseif($r->hasWarnings())
		{
			$this->addErrors($r->getWarnings());
			return null;
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function tryAddAction(array $fields)
	{
		$r = $this->add($fields);

		if($r->isSuccess())
		{
			$order = $r->getData()['ORDER'];
			return array_merge(
				$this->toArray($order),
				[
					'DELIIVERY_ID_LIST_ACTION'=>$this->getDeliveryIdListAction($order),
					'PAY_SYSTEM_ID_LIST'=>$this->getPaySystemIdListAction($order)
				]
			);
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function addAction(array $fields)
	{
		$result = null;

		$r = $this->add($fields);

		if($r->isSuccess())
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $r->getData()['ORDER'];

			$r = $order->save();
			if(!$r->isSuccess())
			{
				$this->addErrors($r->getErrors());
				return null;
			}

			//TODO: return $order->toArray();
			return $this->toArray($order);
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function tryUpdateAction(\Bitrix\Sale\Order $order, array $fields)
	{
		$r = $this->update($order, $fields);

		if($r->isSuccess())
		{
			$order = $r->getData()['ORDER'];
			return array_merge(
				$this->toArray($order),
				[
					'DELIIVERY_ID_LIST_ACTION'=>$this->getDeliveryIdListAction($order),
					'PAY_SYSTEM_ID_LIST'=>$this->getPaySystemIdListAction($order)
				]
			);
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function updateAction(\Bitrix\Sale\Order $order, array $fields)
	{
		$result = null;
		$r = $this->update($order, $fields);

		if($r->isSuccess())
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $r->getData()['ORDER'];

			$r = $order->save();
			if(!$r->isSuccess())
			{
				$this->addErrors($r->getErrors());
				return null;
			}

			//TODO: return $order->toArray();
			return $this->toArray($order);
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function listAction($select=[], $filter=[], $order=[], PageNavigation $pageNavigation=null)
	{
		$select = empty($select)? ['*']:$select;
		$order = empty($order)? ['ID'=>'ASC']:$order;
		$runtime = [
			new \Bitrix\Main\Entity\ReferenceField(
				'PERSON_TYPE',
				'\Bitrix\Sale\Internals\PersonType',
				array('=this.PERSON_TYPE_ID' => 'ref.ID')
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'STATUS_TABLE',
				'\Bitrix\Sale\Internals\StatusTable',
				array('=this.STATUS_ID' => 'ref.ID')
			)
		];

		$orders = \Bitrix\Sale\Order::getList(
			[
				'select'=>$select,
				'filter'=>$filter,
				'order'=>$order,
				'offset'=>$pageNavigation->getOffset(),
				'limit'=>$pageNavigation->getLimit(),
				'runtime'=>$runtime
			]
		)->fetchAll();

		return new Page('ORDERS', $orders, function() use ($select, $filter, $runtime)
		{
			return count(
				\Bitrix\Sale\Order::getList(['select'=>$select, 'filter'=>$filter, 'runtime'=>$runtime])->fetchAll()
			);
		});
	}

	public function deleteAction(\Bitrix\Sale\Order $order)
	{
		$r = $order->delete($order->getId());
		if($r->isSuccess())
			$r = $order->save();

		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}

		if ($r->hasWarnings())
		{
			$this->addErrors($r->getWarnings());
			return null;
		}

		return true;
	}

	public function getDeliveryIdListAction(\Bitrix\Sale\Order $order)
	{
		return $order->getDeliveryIdList();
	}

	public function getPaymentsAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getPaymentCollection()->toArray();
		return $this->toArray($order)['PAYMENTS'];
	}

	public function getPaySystemIdListAction(\Bitrix\Sale\Order $order)
	{
		return $order->getPaySystemIdList();
	}

	public function getPrintedChecksAction(\Bitrix\Sale\Order $order)
	{
		//TODO: ->toArray() ???
		return $order->getPrintedChecks();
	}

	public function getShipmentsAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getShipmentCollection()->toArray();
		return $this->toArray($order)['SHIPMENTS'];
	}

	public function getBasketAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getBasket()->toArray();
		return $this->toArray($order)['BASKET']['ITEMS'];
	}

	public function getCurrencyAction(\Bitrix\Sale\Order $order)
	{
		return $order->getField('CURRENCY');
	}

	public function getDateInsertAction(\Bitrix\Sale\Order $order)
	{
		return $order->getField('DATE_INSERT');
	}

	public function getDeliveryLocationAction(\Bitrix\Sale\Order $order)
	{
		return $order->getDeliveryLocation();
	}

	public function getApplyDiscountAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getDiscount()->toArray();
		return $this->toArray($order)['DISCOUNTS'];
	}

	public function getPersonTypeIdAction(\Bitrix\Sale\Order $order)
	{
		return $order->getPersonTypeId();
	}

	public function getPriceAction(\Bitrix\Sale\Order $order)
	{
		return $order->getPrice();
	}

	public function getPropertiesAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getPropertyCollection()->toArray();
		return $this->toArray($order)['PROPERTIES'];
	}

	public function getSiteIdAction(\Bitrix\Sale\Order $order)
	{
		return $order->getSiteId();
	}

	public function getSumPaidAction(\Bitrix\Sale\Order $order)
	{
		return $order->getSumPaid();
	}

	public function getTaxListAction(\Bitrix\Sale\Order $order)
	{
		//TODO: return $order->getTax()->toArray();
		return $this->toArray($order)['TAX'];
	}

	public function getTaxLocationAction(\Bitrix\Sale\Order $order)
	{
		return $order->getTaxLocation();
	}

	public function getTaxPriceAction(\Bitrix\Sale\Order $order)
	{
		return $order->getTaxPrice();
	}

	public function getTaxValueAction(\Bitrix\Sale\Order $order)
	{
		return $order->getTaxValue();
	}

	public function getUserIdAction(\Bitrix\Sale\Order $order)
	{
		return $order->getUserId();
	}

	public function getVatRateAction(\Bitrix\Sale\Order $order)
	{
		return $order->getVatRate();
	}

	public function getVatSumAction(\Bitrix\Sale\Order $order)
	{
		return $order->getVatSum();
	}

	public function isAllowDeliveryAction(\Bitrix\Sale\Order $order)
	{
		return $order->isAllowDelivery()?'Y':'N';
	}

	public function isCanceledAction(\Bitrix\Sale\Order $order)
	{
		return $order->isCanceled()?'Y':'N';
	}

	public function isExternalAction(\Bitrix\Sale\Order $order)
	{
		return $order->isExternal()?'Y':'N';
	}

	public function isMarkedAction(\Bitrix\Sale\Order $order)
	{
		return $order->isMarked()?'Y':'N';
	}

	public function isPaidAction(\Bitrix\Sale\Order $order)
	{
		return $order->isPaid()?'Y':'N';
	}

	public function isShippedAction(\Bitrix\Sale\Order $order)
	{
		return $order->isShipped()?'Y':'N';
	}

	public function isUsedVatAction(\Bitrix\Sale\Order $order)
	{
		return $order->isUsedVat()?'Y':'N';
	}

	public function getPropertyGroupsAction(\Bitrix\Sale\Order $order)
	{
		return $order->getPropertyCollection()
			->getArray();
	}

	//public function applyDiscountAction(\Bitrix\Sale\Order $order, array $data)
	//public function refreshAction(\Bitrix\Sale\Order $order, array $data)

	//endregion

	//region admin Actions
	//public function cancelOrderAction(\Bitrix\Sale\Order $order, array $data)
	//public function saveCommentsAction(\Bitrix\Sale\Order $order, array $data)
	//public function saveStatusAction(\Bitrix\Sale\Order $order, array $data)
	//public function changeResponsibleUserAction(\Bitrix\Sale\Order $order, array $data)
	//public function updatePaymentStatusAction()
	//public function updateShipmentStatusAction()
	//public function changeDeliveryServiceAction()
	//public function checkProductBarcodeAction()
	//public function deleteCouponAction(\Bitrix\Sale\Order $order, array $data)
	//public function addCouponsAction(\Bitrix\Sale\Order $order, array $data)
	//public function getProductIdByBarcodeAction()
	//public function refreshOrderDataAction(\Bitrix\Sale\Order $order, array $data)
	//endregion

	protected function modify(array $fields)
	{
		$r = new Result();

		$builder = $this->getBuilder();
		try{
			$builder->build($fields);
			$errorsContainer = $builder->getErrorsContainer();
		}
		catch(BuildingException $e)
		{
			if($builder->getErrorsContainer()->getErrorCollection()->count()<=0)
			{
				$builder->getErrorsContainer()->addError(new Error('UNKNOW_ERROR'));
			}
			$errorsContainer = $builder->getErrorsContainer();
		}

		if($errorsContainer->getErrorCollection()->count()>0)
			$r->addErrors($errorsContainer->getErrors());
		else
			$r->setData(['ORDER'=>$builder->getOrder()]);

		return $r;
	}

	protected function add(array $fields)
	{
		$r = new Result();

		$fields = ['ORDER'=>$fields];

		if($fields['ORDER']['ID'])
			unset($fields['ORDER']['ID']);

		$orderBuilder = $this->getBuilder();
		$order = $orderBuilder->buildEntityOrder($fields);

		if($orderBuilder->getErrorsContainer()->getErrorCollection()->count()>0)
			$r->addErrors($orderBuilder->getErrorsContainer()->getErrors());
		else
			$r->setData(['ORDER'=>$order]);

		return $r;
	}

	protected function update(\Bitrix\Sale\Order $order, array $fields)
	{
		$r = new Result();

		$data = [
			'ORDER'=>array_merge([
				'ID'=>$order->getId()
			], $fields)
		];

		$orderBuilder = $this->getBuilder();
		$order = $orderBuilder->buildEntityOrder($data);

		if($orderBuilder->getErrorsContainer()->getErrorCollection()->count()>0)
			$r->addErrors($orderBuilder->getErrorsContainer()->getErrors());
		else
			$r->setData(['ORDER'=>$order]);

		return $r;
	}

	protected function get(\Bitrix\Sale\Order $order, array $fields=[])
	{
		return $this->toArray($order, $fields);
	}

	static public function prepareFields(array $fields)
	{
		$fields = isset($fields['ORDER'])? $fields['ORDER']:[];

		if(isset($fields['BASKET_ITEMS']))
			unset($fields['BASKET_ITEMS']);
		if(isset($fields['PROPERTY_VALUES']))
			unset($fields['PROPERTY_VALUES']);
		if(isset($fields['PAYMENTS']))
			unset($fields['PAYMENTS']);
		if(isset($fields['SHIPMENTS']))
			unset($fields['SHIPMENTS']);
		if(isset($fields['TRADE_BINDINGS']))
			unset($fields['TRADE_BINDINGS']);
		if(isset($fields['CLIENTS']))
			unset($fields['CLIENTS']);
		if(isset($fields['REQUISITE_LINKS']))
			unset($fields['REQUISITE_LINKS']);

		return $fields;
	}

	private static function setFlagActionImport()
	{
		//TODO: huck ��� ���������� ���������� ������� � \Bitrix\Sale\Rest\RestManager::processEvent(). ����������� ��������� - import �.�. ������ ��������
		$instance = \Bitrix\Sale\Rest\Synchronization\Manager::getInstance();
		$instance->setAction(\Bitrix\Sale\Rest\Synchronization\Manager::ACTION_IMPORT);
	}

	public function importDeleteAction(\Bitrix\Sale\Order $order)
	{
		self::setFlagActionImport();

		return $this->deleteAction($order);
	}

	public function resolveExternalIdToInternalId(array $fields)
	{
		LoggerDiag::addMessage('ORDER_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID_SOURCE_FIELDS', var_export($fields, true));

		$result = new Result();

		$instance = \Bitrix\Sale\Rest\Synchronization\Manager::getInstance();

		$ixInternal = [];
		$ixExternal = [];

		$externalId = $fields['ORDER']['XML_ID'];
		$ixExternal['ORDER']['MAP'][$externalId] = $fields['ORDER']['ID'];

		unset($fields['ORDER']['ID']);
		$internalId = $this->getInternalId($fields['ORDER']['XML_ID'], Registry::ENTITY_ORDER);
		if(intval($internalId)>0)
		{
			$fields['ORDER']['ID'] = $internalId;
			$ixInternal['ORDER']['MAP'][$externalId] = $internalId;
		}

		// �������� ����������� ������ ��� ������ ������.
		// � ������� �� ����� �������� � ������ ����� ������� ��������� �������� ����� ������ - �����, ���� ����������� ��� ������������ �� ������������
		if(intval($internalId)<=0)
		{
			//TODO: ������������� ����� � ������� ��������
			$internalPersonTypeId = $this->getInternalId($fields['ORDER']['PERSON_TYPE_XML_ID'], 'PERSON_TYPE_TYPE');
			$fields['ORDER']['PERSON_TYPE_ID'] = $internalPersonTypeId>0 ?  $internalPersonTypeId:$instance->getDefaultPersonTypeId();
			$fields['ORDER']['USER_ID'] = \CSaleUser::GetAnonymousUserID();
			$fields['ORDER']['SITE_ID'] = $instance->getDefaultSiteId();
			$internalOrderStatusId = $this->getInternalId($fields['ORDER']['STATUS_XML_ID'], Registry::ENTITY_ORDER_STATUS);
			$fields['ORDER']['STATUS_ID' ] = $internalOrderStatusId<>''? $internalOrderStatusId:$instance->getDefaultOrderStatusId();

			//Registry::
		}
		else
		{
			$order = \Bitrix\Sale\Order::load($internalId);
			$fields['ORDER']['PERSON_TYPE_ID'] = $order->getPersonTypeId();
			$fields['ORDER']['USER_ID'] = $order->getUserId();
			$fields['ORDER']['SITE_ID'] = $order->getSiteId();
		}

		if(is_array($fields['ORDER']['PROPERTY_VALUES']))
		{
			foreach($fields['ORDER']['PROPERTY_VALUES'] as $k=>&$item)
			{
				$internalIdExternalSystem = $item['ORDER_PROPS_ID'];
				$externalId = $item['ORDER_PROPS_XML_ID'];

				unset($item['ORDER_PROPS_ID']);
				unset($item['ORDER_PROPS_XML_ID']);
				unset($item['ID']);//id �� ���������� �.�. ������ �������� �������� ���������������� ������ �� orderPropsId

				if($externalId<>'')
				{
					$ixExternal['PROPERTIES'][$k]['MAP'][$externalId] = $internalIdExternalSystem;

					$internalId = $this->getInternalId($externalId, Registry::ENTITY_PROPERTY);
					if(intval($internalId)>0)
					{
						$item['ORDER_PROPS_ID'] = $internalId;
						$ixInternal['PROPERTIES'][$k]['MAP'][$externalId] = $internalId;
					}
				}
				else
				{
					unset($item);
				}
			}
		}

		if(is_array($fields['ORDER']['BASKET']['ITEMS']))
		{
			$n=1;
			foreach($fields['ORDER']['BASKET']['ITEMS'] as $k=>&$item)
			{
				$internalIdExternalSystem = $item['ID'];
				$externalId = $item['XML_ID'];

				$internalId = $this->getInternalId($externalId, Registry::ENTITY_BASKET);
				$ixInternal['BASKET']['ITEMS'][$k]['MAP'][$externalId] = (intval($internalId)>0)? $internalId:'n'.$n++;
				$ixExternal['BASKET']['ITEMS'][$k]['MAP'][$externalId] = $internalIdExternalSystem;

				$properties = $item['PROPERTIES'];
				if(count($properties)>0)
				{
					foreach ($properties as $kp=>&$property)
					{
						$property['BASKET_ID'] = $ixInternal['BASKET']['ITEMS'][$k]['MAP'][$externalId];
						$internalIdBasketProps = $this->getInternalId($property['XML_ID'], Registry::ENTITY_BASKET_PROPERTIES_COLLECTION);
						if(intval($internalIdBasketProps)>0)
						{
							$ixInternal['BASKET']['ITEMS'][$k]['PROPERTIES'][$kp][$property['XML_ID']] = $internalIdBasketProps;
							$property['ID'] = $internalIdBasketProps;
						}
						$ixExternal['BASKET']['ITEMS'][$k]['PROPERTIES'][$kp]['MAP'][$property['XML_ID']] = $property['ID'];
					}
				}


				$item = array_merge(
					['PROPERTIES'=>$properties],
					$this->prepareFieldsBasketItem($item)
				);

				$item['ID'] = $ixInternal['BASKET']['ITEMS'][$k]['MAP'][$externalId];
			}
		}

		if(is_array($fields['ORDER']['PAYMENTS']))
		{
			foreach($fields['ORDER']['PAYMENTS'] as $k=>&$item)
			{
				$externalId = $item['XML_ID'];
				$ixExternal['PAYMENTS'][$k]['MAP'][$externalId] = $item['ID'];

				unset($item['ID']);
				$internalId = $this->getInternalId($externalId, Registry::ENTITY_PAYMENT_COLLECTION);
				if(intval($internalId)>0)
				{
					$item['ID'] = $internalId;
					$ixInternal['PAYMENTS'][$k]['MAP'][$externalId] = $internalId;
				}

				$externalPaySystemId = $item['PAY_SYSTEM_XML_ID'];
				$ixExternal['PAYMENTS'][$k]['PAY_SYSTEMS']['MAP'][$externalPaySystemId] = $item['PAY_SYSTEM_ID'];

				unset($item['PAY_SYSTEM_XML_ID']);
				$internalPaySystemId = $this->getInternalId($externalPaySystemId, 'PAY_SYSTEM_TYPE');
				$item['PAY_SYSTEM_ID'] = $internalPaySystemId>0 ? $internalPaySystemId:$instance->getDefaultPaySystemId();
				$ixInternal['PAYMENTS'][$k]['PAY_SYSTEM']['MAP'][$externalPaySystemId] = $item['PAY_SYSTEM_ID'];
			}
		}

		if(is_array($fields['ORDER']['SHIPMENTS']))
		{
			foreach($fields['ORDER']['SHIPMENTS'] as $k=>&$item)
			{
				$externalId = $item['XML_ID'];
				$ixExternal['SHIPMENTS'][$k]['MAP'][$externalId] = $item['ID'];

				unset($item['ID']);
				$internalId = $this->getInternalId($item['XML_ID'], Registry::ENTITY_SHIPMENT_COLLECTION);
				if(intval($internalId)>0)
				{
					$item['ID'] = $internalId;
					$ixInternal['SHIPMENTS'][$k][$externalId] = $internalId;
				}

				$externalDeliveryId = $item['DELIVERY_XML_ID'];
				$ixExternal['SHIPMENTS'][$k]['DELIVERY_SYSTEM']['MAP'][$externalDeliveryId] = $item['DELIVERY_ID'];

				unset($item['DELIVERY_XML_ID']);
				$internalDeliveryId = $this->getInternalId($externalDeliveryId, 'DELIVERY_SYSTEM_TYPE');
				$item['DELIVERY_ID'] = $internalDeliveryId>0 ? $internalDeliveryId:$instance->getDefaultDeliverySystemId();
				$ixInternal['SHIPMENTS'][$k]['DELIVERY_SYSTEM']['MAP'][$externalDeliveryId] = $item['DELIVERY_ID'];

				$externalDeliveryStatusId = $item['STATUS_XML_ID'];
				$ixExternal['SHIPMENTS'][$k]['DELIVERY_STATUS']['MAP'][$externalDeliveryStatusId] = $item['STATUS_ID'];

				unset($item['STATUS_XML_ID']);
				$internalDeliveryStatusId = $this->getInternalId($externalDeliveryStatusId, Registry::ENTITY_DELIVERY_STATUS);
				$item['STATUS_ID'] = $internalDeliveryStatusId<>''? $internalDeliveryStatusId:$instance->getDefaultDeliveryStatusId();
				$ixInternal['SHIPMENTS'][$k]['DELIVERY_STATUS']['MAP'][$externalDeliveryStatusId] = $item['STATUS_ID'];

				foreach($item['BASKET']['ITEMS'] as $kb=>&$shipmentItem)
				{
					unset($shipmentItem['ID']);
					unset($shipmentItem['ORDER_DELIVERY_ID']);
					$internalIdShipmentItem = $this->getInternalId($shipmentItem['XML_ID'], Registry::ENTITY_SHIPMENT_ITEM_COLLECTION);

					if(intval($internalIdShipmentItem)>0)
					{
						$shipmentItem['ID'] = $internalIdShipmentItem;
						if(intval($internalId)>0)
							$shipmentItem['ORDER_DELIVERY_ID'] = $internalId;

						$ixInternal['SHIPMENTS'][$k]['BASKET']['ITEMS'][$kb]['MAP'][$externalId] = $internalIdShipmentItem;
					}

					// ������� �� �������� ������������ xmlId => id.������� ��������, ������� ������������� �� ����������� id �������� ��������
					$external = '';
					foreach ($ixExternal['BASKET']['ITEMS'] as $map)
					{
						$internal = current($map['MAP']);

						if($shipmentItem['BASKET_ID'] == $internal)
						{
							$external = key($map['MAP']);
							break;
						}
					}

					if($external=='')
						$result->addError(new Error('Modify fields error. ShipmentItem xmlId is invalid'));

					if($external<>'')
					{
						// ������� �������� id ������� �� ����������� ����������� xmlId => id.��������� ��������
						foreach ($ixInternal['BASKET']['ITEMS'] as $map)
						{
							if(isset($map['MAP'][$external]))
							{
								$shipmentItem['BASKET_ID'] = $map['MAP'][$external];
								break;
							}
						}
					}
				}

				$item = $this->prepareFieldsShipment($item);
			}
		}
		if($this->isB24())
		{
			if(is_array($fields['ORDER']['TRADE_BINDINGS']))
			{
				foreach($fields['ORDER']['TRADE_BINDINGS'] as $k=>&$item)
				{

					$externalId = $item['XML_ID'];
					$ixExternal['TRADE_BINDINGS'][$k]['MAP'][$externalId] = $item['ID'];

					unset($item['ID']);
					if($externalId<>'') // ������� ��� ���. xmlId �� ��� �� ����������
					{
						$internalId = $this->getInternalId($externalId, Registry::ENTITY_TRADE_BINDING_COLLECTION);
						if(intval($internalId)>0)
						{
							$item['ID'] = $internalId;
							$ixInternal['TRADE_BINDINGS'][$k]['MAP'][$externalId] = $internalId;
						}
					}

					$externalTradePlatformId = $item['TRADING_PLATFORM_XML_ID'];
					$ixExternal['TRADE_BINDINGS'][$k]['TRADING_PLATFORMS']['MAP'][$externalTradePlatformId] = $item['TRADING_PLATFORM_ID'];

					unset($item['TRADING_PLATFORM_XML_ID']);
					$internalTradePlatformId = $this->getInternalId($externalTradePlatformId, 'TRADING_PLATFORM_TYPE');
					//TODO: need default value <> 0
					$item['TRADING_PLATFORM_ID'] = $internalTradePlatformId>0 ? $internalTradePlatformId:0;
					$ixInternal['TRADE_BINDINGS'][$k]['TRADING_PLATFORM']['MAP'][$externalTradePlatformId] = $item['TRADING_PLATFORM_ID'];
				}
			}

			if(is_array($fields['ORDER']['CLIENTS']))
			{
				foreach($fields['ORDER']['CLIENTS'] as $k=>&$item)
				{
					$externalId = $item['XML_ID'];
					$ixExternal['CLIENTS'][$k]['MAP'][$externalId] = $item['ID'];

					unset($item['ID']);
					$internalId = $this->getInternalId($externalId, ENTITY_CRM_CONTACT_COMPANY_COLLECTION);
					if(intval($internalId)>0)
					{
						$item['ID'] = $internalId;
						$ixInternal['CLIENTS'][$k]['MAP'][$externalId] = $internalId;
					}
				}
			}
		}
		else
		{
			// ��������� ���� TRADE_BINDINGS ����� �� ������ ��� �� ������� �������� �������� ���������� � �������.
			// ��������� � ������ ������ �� ��������������. �� ��������� ��������� ���� �� ������� ��� � �������� �� ������ �24
			$fields['ORDER']['TRADE_BINDINGS'] = [];
			unset($fields['ORDER']['CLIENTS']);
		}

		if($result->isSuccess())
		{
			$result->setData(['DATA'=>$fields]);
			LoggerDiag::addMessage('ORDER_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID_SUCCESS', var_export($fields, true));
		}
		else
		{
			LoggerDiag::addMessage('ORDER_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID_ERROR');
		}

		return $result;
	}

	protected function getInternalId($externalId, $typeName)
	{
		$loader = Factory::create($typeName);
		return $loader->getFieldsByExternalId($externalId);
	}

	private function prepareFieldsBasketItem($fields)
	{
		$instance = \Bitrix\Sale\Rest\Synchronization\Manager::getInstance();
		$loader = Factory::create('PRODUCT');

		$code = $loader->getCodeAfterDelimiter($fields['PRODUCT_XML_ID']);
		$product = $code<>'' ? $loader->getFieldsByExternalId($code):array();
		if(empty($product))
			$product = $loader->getFieldsByExternalId($fields['PRODUCT_XML_ID']);

		if(!empty($product))
		{
			$result = array(
				"PRODUCT_ID" => $product["ID"],
				"NAME" => $product["NAME"],
				"MODULE" => "catalog",
				"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
				"CATALOG_XML_ID" => $product["IBLOCK_XML_ID"],
				"DETAIL_PAGE_URL" => $product["DETAIL_PAGE_URL"],
				"WEIGHT" => $product["WEIGHT"],
				"NOTES" => $product["CATALOG_GROUP_NAME"]
			);
		}
		else
		{
			$ri = new RandomSequence($fields['PRODUCT_XML_ID']);
			$result = array(
				"PRODUCT_ID" => $ri->rand(1000000, 9999999),
				"NAME" => $fields["NAME"],
				"MODULE" => null,
				"PRODUCT_PROVIDER_CLASS" => null,
				"CATALOG_XML_ID" => null,
				"MEASURE_CODE" => $fields["MEASURE_CODE"],
				"MEASURE_NAME" => $fields["MEASURE_NAME"],
				//"DISCOUNT_PRICE" => $item['DISCOUNT']['PRICE'],
			);
		}

		$result["LID"] = $instance->getDefaultSiteId();
		$result["QUANTITY"] = $fields["QUANTITY"];
		$result["DELAY"] = "N";
		$result["CAN_BUY"] = "Y";
		$result["IGNORE_CALLBACK_FUNC"] = "Y";
		$result["PRODUCT_XML_ID"] = $fields["PRODUCT_XML_ID"];
		$result["XML_ID"] = $fields["XML_ID"];

		$result["PRICE"] = $fields["PRICE"];

		$result["VAT_RATE"] = $fields["VAT_RATE"];
		$result["VAT_INCLUDED"] = $fields["VAT_INCLUDED"];

		return $result;
	}

	private function prepareFieldsShipment($item)
	{
		// �.�. ������������� ����� �������� ����� xml ������������� �������� ����� �� ��� � �24 (� �� ������)
		// ������������� ��������� ��� ���� ���������
		$item['CUSTOM_PRICE_DELIVERY'] = 'Y';

		return $item;
	}

	public function importAction(array $fields)
	{
		$result = new Result();

		self::setFlagActionImport();

		LoggerDiag::addMessage('ORDER_IMPORT_ACTION_WITH_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID', var_export($fields, true));

		$r = $this->resolveExternalIdToInternalId($fields);

		if($r->isSuccess())
		{
			$result = $this->modifyAction($r->getData()['DATA']);
		}
		else
		{
			$this->addErrors($r->getErrors());
		}

		if(count($this->getErrors())>0)
		{
			LoggerDiag::addMessage('ORDER_IMPORT_ACTION_WITH_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID_ERROR', var_export($this->getErrors(), true));
			return null;
		}
		else
		{
			LoggerDiag::addMessage('ORDER_IMPORT_ACTION_WITH_RESOLVE_EXTERNAL_ID_TO_INTERNAL_ID_SUCCESS');
			return $result;
		}
	}
}