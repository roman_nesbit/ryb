<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Main\Error;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Result;

class PropertyValue extends Base
{
	public function getFields()
	{
		return [
			'NAME'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'CODE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'ORDER_PROPS_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'ORDER_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
				]
			],
			'VALUE'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'ORDER_PROPS_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
				]
			]
		];
	}

	public function internalizeFieldsModify($fields)
	{
		$result = [];

		if(isset($fields['ORDER']['ID']))
			$result['ORDER']['ID'] = (int)$fields['ORDER']['ID'];

		if(isset($fields['ORDER']['PROPERTY_VALUES']))
			$result['ORDER']['PROPERTY_VALUES'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['PROPERTY_VALUES'], new \Bitrix\Sale\Rest\Entity\PropertyValue());

		return $result;
	}

	protected function getRewritedFields()
	{
		return [
			'ORDER_PROPS_XML_ID'=>[
				'REFERENCE_FIELD'=>'ORDER_PROPS.XML_ID'
			]
		];
	}

	public function externalizeFieldsModify($fields)
	{
		return $this->externalizeListFields($fields);
	}

	public function checkFieldsModify($fields)
	{
		$r = new Result();

		$emptyFields = [];
		if(!isset($fields['ORDER']['ID']))
		{
			$emptyFields[] = '[order][id]';
		}
		if(!isset($fields['ORDER']['PROPERTY_VALUES']) || !is_array($fields['ORDER']['PROPERTY_VALUES']))
		{
			$emptyFields[] = '[order][propertyValues][]';
		}

		if(count($emptyFields)>0)
		{
			$r->addError(new Error('Required fields: '.implode(', ', $emptyFields)));
		}
		else
		{
			$r = parent::checkFieldsModify($fields);
		}

		return $r;
	}

	public function checkRequiredFieldsModify($fields)
	{
		$r = new Result();

		foreach ($fields['ORDER']['PROPERTY_VALUES'] as $k=>$fieldsPropertyValue)
		{
			$required = $this->checkRequiredFields($fieldsPropertyValue, [
				'ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly],
				'-required'=>['ORDER_ID']
			]);
			if(!$required->isSuccess())
			{
				$r->addError(new Error('[propertyValues]['.$k.'] - '.implode(', ', $required->getErrorMessages()).'.'));
			}
		}
		return $r;
	}
}