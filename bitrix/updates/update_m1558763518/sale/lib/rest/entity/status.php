<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Sale\Rest\Attributes;

class Status extends Base
{
	public function getFields()
	{
		return [
			'ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'TYPE'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'SORT'=>[
				'TYPE'=>self::TYPE_INT
			],
			'NOTIFY'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'COLOR'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'LANGS'=>[
				'TYPE'=>self::TYPE_LIST,
				'ATTRIBUTES'=>[Attributes::Hidden]
			],
			'GROUP_TASKS'=>[
				'TYPE'=>self::TYPE_LIST,
				'ATTRIBUTES'=>[Attributes::Hidden]
			],
		];
	}

	public function internalizeFieldsModify($fields)
	{
		$result = [];

		if(isset($fields['STATUS']))
			$result['STATUS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['STATUS'], new \Bitrix\Sale\Rest\Entity\Status());

		return $result;
	}

	public function externalizeFieldsModify($fields)
	{
		return $this->externalizeFields($fields);
	}

	public function externalizeFields($fields)
	{
		$result = parent::externalizeFields($fields);

		//TODO: �������� ��������� ������
		if(isset($fields['LANGS']))
			$result['LANGS'] = $fields['LANGS'];

		//TODO: �������� ��������� ������
		if(isset($fields['GROUP_TASKS']))
			$result['GROUP_TASKS'] = $fields['GROUP_TASKS'];

		return $result;
	}
}