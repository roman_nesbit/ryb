<?
$MESS["SALE_HPS_YANDEX_CHECKOUT"] = "Yandex.Checkout (ab Version 3.?)";
$MESS["SALE_HPS_YANDEX_CHECKOUT_SHOP_ID_DESC"] = "Kopieren Sie shopId in Ihrem Yandex.Checkout Account";
$MESS["SALE_HPS_YANDEX_CHECKOUT_SHOP_ARTICLE_ID_DESC"] = "Konsultieren Sie Ihren Yandex.Checkout Manager wegen des Wertes shopArticleId";
$MESS["SALE_HPS_YANDEX_CHECKOUT_SECRET_KEY"] = "Geheimschlüssel";
$MESS["SALE_HPS_YANDEX_CHECKOUT_SECRET_KEY_DESC"] = "Erstellen und aktivieren Sie einen Geheimschlüssel in Ihrem Yandex.Checkout Account";
$MESS["SALE_HPS_YANDEX_CHECKOUT_RETURN_URL"] = "URL der Zurück-Seite ";
$MESS["SALE_HPS_YANDEX_CHECKOUT_RETURN_URL_DESC"] = "Die URL der Seite, auf die ein Kunde zurückgeht, wenn die Zahlung gemacht wird";
$MESS["SALE_HPS_YANDEX_CHECKOUT_PAYMENT_ID"] = "Zahlung #";
$MESS["SALE_HPS_YANDEX_CHECKOUT_SHOULD_PAY"] = "Gesamtbetrag";
$MESS["SALE_HPS_YANDEX_CHECKOUT_PAYMENT_DATE"] = "Zahlung erstellt am";
$MESS["SALE_HPS_YANDEX_CHECKOUT_IS_TEST"] = "Testmodus";
$MESS["SALE_HPS_YANDEX_CHECKOUT_CHANGE_STATUS_PAY"] = "Bestellstatus automatisch auf Bezahlt ändern, wenn der Status der erfolgreichen Bezahlung zugestellt wird";
$MESS["SALE_HPS_YANDEX_CHECKOUT_PAYMENT_TYPE"] = "Typ des Zahlungssystems";
$MESS["SALE_HPS_YANDEX_CHECKOUT_BUYER_ID"] = "ID des Kunden";
$MESS["SALE_HPS_YANDEX_CHECKOUT_RETURN"] = "Rückbuchung wird nicht unterstützt";
$MESS["SALE_HPS_YANDEX_CHECKOUT_RESTRICTION"] = "Einschränkungen des Zahlungsbetrags hängen von der Zahlungsart ab, die Kunden auswählen";
$MESS["SALE_HPS_YANDEX_CHECKOUT_COMMISSION"] = "Gebührenfrei";
$MESS["SALE_HPS_YANDEX_CHECKOUT_REFERRER"] = "<a href=\"https://money.yandex.ru/joinups/?source=bitrix24\" target=\"_blank\">Schnellregistrierung</a>";
?>