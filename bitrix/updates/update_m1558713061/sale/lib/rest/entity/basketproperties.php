<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Main\Error;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Result;

class BasketProperties extends Base
{
	public function getFields()
	{
		return [
			'ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'BASKET_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::ReadOnly,
					Attributes::Immutable,
				]
			],
			'NAME'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'VALUE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'CODE'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
			],
			'SORT'=>[
				'TYPE'=>self::TYPE_INT
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				//'ATTRIBUTES'=>[Attributes::Required]
			]
		];
	}

	public function checkFieldsModify($fields)
	{
		$r = new Result();

		$emptyFields = [];

		if(!isset($fields['BASKET_ITEM']['ID']))
		{
			$emptyFields[] = '[basketItem][id]';
		}
		if(!isset($fields['BASKET_ITEM']['PROPERTIES']) || !is_array($fields['BASKET_ITEM']['PROPERTIES']))
		{
			$emptyFields[] = '[basketItem][properties][]';
		}

		if(count($emptyFields)>0)
		{
			$r->addError(new Error(implode(', ', $emptyFields)));
		}
		else
		{
			$r = parent::checkFieldsModify($fields);
		}

		return $r;
	}

	public function checkRequiredFieldsModify($fields)
	{
		$r = new Result();

		foreach ($fields['BASKET_ITEM']['PROPERTIES'] as $k=>$fieldsBasketItem)
		{
			$required = $this->checkRequiredFields($fieldsBasketItem, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly, Attributes::Immutable]]);
			if(!$required->isSuccess())
			{
				$r->addError(new Error('[properties]['.$k.'] - '.implode(', ', $required->getErrorMessages()).'.'));
			}
		}

		return $r;
	}
}