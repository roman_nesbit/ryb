<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Main\Engine\Response\Converter;
use Bitrix\Main\Error;
use Bitrix\Main\NotImplementedException;
use Bitrix\Main\Type\Date;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Result;

abstract class Base
{
	const TYPE_INT = 'integer';
	const TYPE_FLOAT = 'double';
	const TYPE_STRING = 'string';
	const TYPE_CHAR = 'char';
	const TYPE_LIST = 'list';
	const TYPE_TEXT = 'text';
	const TYPE_FILE = 'file';
	const TYPE_DATE = 'date';
	const TYPE_DATETIME = 'datetime';

	abstract public function getFields();

	public function prepareFieldInfos($fields)
	{
		$result = [];
		foreach($fields as $name => $info)
		{
			$attributs = isset($info['ATTRIBUTES']) ? $info['ATTRIBUTES'] : [];

			if(in_array(Attributes::Hidden, $attributs, true))
			{
				continue;
			}

			$result[$name] = array(
				'TYPE' => $info['TYPE'],
				'IS_REQUIRED' => in_array(Attributes::Required, $attributs, true),
				'IS_READ_ONLY' => in_array(Attributes::ReadOnly, $attributs, true),
				'IS_IMMUTABLE' => in_array(Attributes::Immutable, $attributs, true)
			);
		}

		return $result;
	}

	public function getSettableFields()
	{
		return array_keys(
			$this->getListFieldInfo(['filter'=>['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly, Attributes::Immutable]]])
		);
	}

	public function getListFieldInfo($params=[])
	{
		$list = [];

		$filter = is_set($params, 'filter')?$params['filter']:[];
		$ignoredAttributes = is_set($filter, 'ignoredAttributes')?$filter['ignoredAttributes']:[];

		foreach ($this->getFields() as $name=>$info)
		{
			if(isset($info['ATTRIBUTES']))
			{
				$skipAttr = array_intersect($ignoredAttributes, $info['ATTRIBUTES']);
				if(!empty($skipAttr))
				{
					continue;
				}
			}

			$list[$name] = $info;
		}

		return $list;
	}

	//region convert keys to snake case
	public function convertKeysToSnakeCaseFields($fields)
	{
		return $this->convertKeysToSnakeCase($fields);
	}

	public function convertKeysToSnakeCaseSelect($fields)
	{
		$converter = new Converter(Converter::VALUES | Converter::TO_SNAKE | Converter::TO_UPPER);
		return $converter->process($fields);
	}

	public function convertKeysToSnakeCaseFilter($fields)
	{
		return $this->convertKeysToSnakeCase($fields);
	}

	public function convertKeysToSnakeCaseOrder($fields)
	{
		$converter = new Converter(Converter::KEYS | Converter::VALUES | Converter::TO_UPPER);
		return $converter->process($fields);
	}

	public function convertKeysToSnakeCaseArguments($name, $arguments)
	{
		return $arguments;
	}

	protected function convertKeysToSnakeCase($data)
	{
		$converter = new Converter(Converter::KEYS | Converter::RECURSIVE | Converter::TO_SNAKE | Converter::TO_UPPER);
		return $converter->process($data);
	}
	//endregion

	//region internalize fields
	/**
	 * @param $name
	 * @param $arguments
	 * @throws NotImplementedException
	 * @return array
	 */
	public function internalizeArguments($name, $arguments)
	{
		throw new NotImplementedException('The method '.$name.' is not implemented.');
	}

	public function internalizeFieldsList($arguments)
	{
		$filter = isset($arguments['filter']) ? $this->internalizeFilterFields($arguments['filter']):[];
		$select = isset($arguments['select']) ? $this->internalizeSelectFields($arguments['select']):[];
		$order = isset($arguments['order']) ? $this->internalizeOrderFields($arguments['order']):[];

		return [
			'filter'=>$filter,
			'select'=>$select,
			'order'=>$order,
		];
	}

	public function rewriteFieldsList($arguments)
	{
		$filter = isset($arguments['filter']) ? $this->rewriteFilterFields($arguments['filter']):[];
		$select = isset($arguments['select']) ? $this->rewriteSelectFields($arguments['select']):[];
		$order = isset($arguments['order']) ? $this->rewriteOrderFields($arguments['order']):[];

		return [
			'filter'=>$filter,
			'select'=>$select,
			'order'=>$order,
		];
	}

	public function internalizeFieldsAdd($fields)
	{
		return $this->internalizeFields($fields, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly]]);
	}

	public function internalizeFieldsUpdate($fields)
	{
		return $this->internalizeFields($fields, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly, Attributes::Immutable]]);
	}

	/**
	 * @param $fields
	 * @throws NotImplementedException
	 * @return array
	 */
	public function internalizeFieldsModify($fields)
	{
		throw new NotImplementedException('The method internalizeFieldsModify is not implemented.');
	}

	public function internalizeFieldsTryAdd($fields)
	{
		return $this->internalizeFieldsAdd($fields);
	}

	public function internalizeFieldsTryUpdate($fields)
	{
		return $this->internalizeFieldsUpdate($fields);
	}

	public function internalizeFieldsTryModify($fields)
	{
		return $this->internalizeFieldsModify($fields);
	}

	protected function internalizeCollectionFields($list, $params=[])
	{
		if(!empty($list) && is_array($list))
		{
			foreach ($list as $k=>$fields)
			{
				$list[$k] = $this->internalizeFields($fields, $params);
			}
		}
		return $list;
	}

	protected function internalizeFields($fields, $params=[])
	{
		$result = [];
		$fieldsInfo = $this->getFields();

		$ignoredAttributes = is_set($params, 'ignoredAttributes') ? $params['ignoredAttributes']:[];

		foreach ($fields as $name=>$value)
		{
			$info = isset($fieldsInfo[$name]) ? $fieldsInfo[$name]:null;
			if(!$info)
			{
				continue;
			}

			$attributes = isset($info['ATTRIBUTES']) ? $info['ATTRIBUTES']:[];

			$skipAttr = array_intersect($ignoredAttributes, $attributes);
			if(!empty($skipAttr))
			{
				continue;
			}

			$type = isset($info['TYPE']) ? $info['TYPE']:'';

			if($type === self::TYPE_DATE || $type === self::TYPE_DATETIME)
			{
				if($value === '')
				{
					$date = '';
				}
				else
				{
					$time = strtotime($value);
					$date = ($time) ? \Bitrix\Main\Type\DateTime::createFromTimestamp($time):'';
				}

				if($date instanceof Date)
				{
					$value = $date;
				}
				else
				{
					continue;
				}
			}
			elseif($type === self::TYPE_FILE)
			{
				//InternalizeFileField()
			}

			$result[$name] = $value;
		}
		return $result;
	}

	protected function internalizeFilterFields($fields)
	{
		$result = [];

		if(is_array($fields) && count($fields)>0)
		{
			$fieldsInfo = $this->getListFieldInfo(['filter'=>['ignoredAttributes'=>[Attributes::Hidden]]]);

			foreach ($fields as $rawName=>$value)
			{
				$field = \CSqlUtil::GetFilterOperation($rawName);

				$info = isset($fieldsInfo[$field['FIELD']]) ? $fieldsInfo[$field['FIELD']]:null;
				if(!$info)
				{
					continue;
				}

				$operation = substr($rawName, 0, strlen($rawName) - strlen($field['FIELD']));
				if(isset($info['FORBIDDEN_FILTERS'])
					&& is_array($info['FORBIDDEN_FILTERS'])
					&& in_array($operation, $info['FORBIDDEN_FILTERS'], true))
				{
					continue;
				}

				$result[$rawName]=$value;
			}
		}

		return $result;
	}

	protected function internalizeSelectFields($fields)
	{
		$result = [];

		$fieldsInfo = $this->getListFieldInfo(['filter'=>['ignoredAttributes'=>[Attributes::Hidden]]]);

		if(empty($fields) || in_array('*', $fields, true))
		{
			$result = array_keys($fieldsInfo);
		}
		else
		{
			foreach ($fields as $name)
			{
				$info = isset($fieldsInfo[$name]) ? $fieldsInfo[$name]:null;
				if(!$info)
				{
					continue;
				}

				$result[] = $name;
			}
		}

		return $result;
	}

	protected function internalizeOrderFields($fields)
	{
		$result = [];

		if(is_array($fields)
			&& count($fields)>0)
		{
			$fieldsInfo = $this->getListFieldInfo(['filter'=>['ignoredAttributes'=>[Attributes::Hidden]]]);

			foreach ($fields as $field=>$order)
			{
				$info = isset($fieldsInfo[$field]) ? $fieldsInfo[$field]:null;
				if(!$info)
				{
					continue;
				}

				$result[$field]=$order;
			}
		}

		return $result;
	}

	protected function rewriteSelectFields($fields)
	{
		$result = [];
		$rewriteFields = $this->getRewritedFields();

		foreach ($fields as $name)
		{
			$fieldsIsAlias = isset($rewriteFields[$name]);

			if($fieldsIsAlias)
			{
				if(isset($rewriteFields[$name]['REFERENCE_FIELD']))
				{
					$result[$name] = $rewriteFields[$name]['REFERENCE_FIELD'];
				}
			}
			else
			{
				$result[] = $name;
			}
		}

		return $result;
	}

	protected function rewriteFilterFields($fields)
	{
		$result = [];
		$rewriteFields = $this->getRewritedFields();


		foreach ($fields as $rawName=>$value)
		{
			$field = \CSqlUtil::GetFilterOperation($rawName);

			$fieldsIsAlias = isset($rewriteFields[$field['FIELD']]);

			if($fieldsIsAlias)
			{
				if(isset($rewriteFields[$field['FIELD']]['REFERENCE_FIELD']))
				{
					$originalName = $rewriteFields[$field['FIELD']]['REFERENCE_FIELD'];
					$operation = substr($rawName, 0, strlen($rawName) - strlen($field['FIELD']));
					$result[$operation.$originalName] = $value;
				}
			}
			else
			{
				$result[$rawName] = $value;
			}
		}

		return $result;
	}

	protected function rewriteOrderFields($fields)
	{
		$result = [];
		$rewriteFields = $this->getRewritedFields();

		foreach ($fields as $name=>$value)
		{
			$fieldsIsAlias = isset($rewriteFields[$name]);

			if($fieldsIsAlias)
			{
				if(isset($rewriteFields[$name]['REFERENCE_FIELD']))
				{
					$result[$rewriteFields[$name]['REFERENCE_FIELD']] = $value;
				}
			}
			else
			{
				$result[$name] = $value;
			}
		}

		return $result;
	}

	/**
	 * @throws NotImplementedException
	 * @return array
	 */
	protected function getRewritedFields()
	{
		return [];
	}

	protected function internalizeFieldsCollectionWithExcludeFields($fields, Base $entity, $excludeFields=[])
	{
		$result = [];

		$paramsImport = ['ignoredAttributes'=>[Attributes::Hidden]];

		if(!empty($fields) && is_array($fields))
		{
			foreach ($fields as $k=>$item)
			{
				$result[$k] = $entity->internalizeFields($item, $paramsImport);

				//region extend fields value
				/*if(!empty($extendsFields) && is_array($extendsFields))
				{
					$fieldsImport = $collectionFieldsImport[$k];
					foreach ($extendsFields as $extendsFieldName)
					{
						if(is_set($fieldsImport, $extendsFieldName))
						{
							$result[$k][$extendsFieldName] = $fieldsImport[$extendsFieldName];
						}
					}
				}*/
				//endregion

				//region exclude raw fields value
				if(!empty($excludeFields) && is_array($excludeFields))
				{
					foreach ($excludeFields as $excludeField)
					{
						if(is_set($item, $excludeField))
						{
							$result[$k][$excludeField] = $item[$excludeField];
						}
					}
				}
				//endregion
			}
		}
		return $result;
	}
	//endregion

	// region externalize fields
	public function externalizeFields($fields)
	{
		$result = [];
		$fieldsInfo = $this->getFields();

		if(is_array($fields) && count($fields)>0)
		{
			foreach($fields as $name => $value)
			{
				$info = isset($fieldsInfo[$name]) ? $fieldsInfo[$name] : null;
				if(!$info)
				{
					continue;
				}

				$attributes = isset($info['ATTRIBUTES']) ? $info['ATTRIBUTES']:[];
				$skipAttr = in_array(Attributes::Hidden, $attributes, true);

				if($skipAttr)
				{
					continue;
				}

				$type = isset($info['TYPE']) ? $info['TYPE']:'';
				if($type === self::TYPE_FILE)
				{
					//externalizeFileField()
				}

				$result[$name] = $value;
			}
		}
		return $result;
	}

	public function externalizeListFields($list)
	{
		$result = [];
		if(is_array($list) && count($list)>0)
		{
			foreach($list as $k=>$fields)
				$result[$k] = $this->externalizeFields($fields);
		}
		return $result;
	}

	/**
	 * @param $fields
	 * @throws NotImplementedException
	 * @return array
	 */
	protected function externalizeFieldsModify($fields)
	{
		throw new NotImplementedException('The method externalizeFieldsModify is not implemented.');
	}

	public function externalizeFieldsTryModify($fields)
	{
		return $this->externalizeFieldsModify($fields);
	}

	/**
	 * @param $name
	 * @param $fields
	 * @throws NotImplementedException
	 * @return array
	 */
	public function externalizeResult($name, $fields)
	{
		throw new NotImplementedException('The method '.$name.' is not implemented.');
	}
	// endregion

	//region convert keys to camel case
	public function convertKeysToCamelCase($fields)
	{
		return Converter::toJson()
			->process($fields);
	}
	// endregion

	//region check fields
	public function checkFieldsAdd($fields)
	{
		$r = new Result();

		$required = $this->checkRequiredFieldsAdd($fields);
		if(!$required->isSuccess())
			$r->addError(new Error('Required fields: '.implode(', ', $required->getErrorMessages())));

		return $r;
	}

	public function checkFieldsUpdate($fields)
	{
		$r = new Result();

		$required = $this->checkRequiredFieldsUpdate($fields);
		if(!$required->isSuccess())
			$r->addError(new Error('Required fields: '.implode(', ', $required->getErrorMessages())));

		return $r;
	}

	public function checkFieldsModify($fields)
	{
		$r = new Result();

		$required = $this->checkRequiredFieldsModify($fields);
		if(!$required->isSuccess())
			$r->addError(new Error('Required fields: '.implode(' ', $required->getErrorMessages())));

		return $r;
	}

	public function checkArguments($name, $arguments)
	{
		return new Result();
	}

	protected function checkRequiredFieldsAdd($fields)
	{
		return $this->checkRequiredFields($fields, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly]]);
	}

	protected function checkRequiredFieldsUpdate($fields)
	{
		return $this->checkRequiredFields($fields, ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly, Attributes::Immutable]]);
	}

	/**
	 * @param $fields
	 * @throws NotImplementedException
	 * @return Result
	 */
	protected function checkRequiredFieldsModify($fields)
	{
		throw new NotImplementedException('The method checkFieldsModify is not implemented.');
	}

	protected function checkRequiredFields($fields, $params=[])
	{
		$r = new Result();

		$fieldsInfo = $this->getListFieldInfo([
			'filter'=>[
				'ignoredAttributes'=>is_set($params, 'ignoredAttributes') ? $params['ignoredAttributes']:[]
			]
		]);

		$addRequiredFields = is_set($params, '+required') ? $params['+required']:[];
		$delRequiredFields = is_set($params, '-required') ? $params['-required']:[];

		foreach ($this->prepareFieldInfos($fieldsInfo) as $name=>$info)
		{
			if(in_array($name, $delRequiredFields))
			{
				continue;
			}
			elseif($info['IS_REQUIRED'] == 'Y' || in_array($name, $addRequiredFields))
			{
				if(!is_set($fields, $name))
					$r->addError(new Error($this->convertKeysToCamelCase($name)));
			}
		}

		return $r;
	}
	//endregion
}