<?php


namespace Bitrix\Sale\Helpers\Order\Builder;

/**
 * Class BasketBuilderRest
 * @package Bitrix\Sale\Helpers\Order\Builder
 * @internal
 */
final class BasketBuilderRest extends BasketBuilder
{
	protected function getDelegate($orderId)
	{
		return (int)$orderId > 0 ? new BasketBuildeRestExist($this) : new BasketBuilderNew($this);
	}

	// �������������� ������������ �����,
	// ��� ��������� ������� �.�. � ������� �� �������������� ������ � �������� � ������� ���������� �����
	protected function getExistsItem($moduleId, $productId, array $properties = array())
	{
		return null;
	}
}