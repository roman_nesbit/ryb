<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Main\Error;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Result;

class Order extends Base
{
	public function getFields()
	{
		return [
			'PERSON_TYPE_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
				]
			],
			'USER_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
				]
			],
			'CURRENCY'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
					]
			],
			'LID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable,
					]
			],
			'PERSON_TYPE_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'STATUS_XML_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'ACCOUNT_NUMBER'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'CANCELED'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'DATE_CANCELED'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'EMP_CANCELED_ID'=>[
					'TYPE'=>self::TYPE_INT
				],
			'REASON_CANCELED'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'STATUS_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'DATE_STATUS'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'EMP_STATUS_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'MARKED'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'DATE_MARKED'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'EMP_MARKED_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'REASON_MARKED'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'PRICE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'DISCOUNT_VALUE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'DATE_INSERT'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'DATE_UPDATE'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'USER_DESCRIPTION'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'ADDITIONAL_INFO'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'COMMENTS'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'TAX_VALUE'=>[
				'TYPE'=>self::TYPE_FLOAT
			],
			'RECURRING_ID'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'LOCKED_BY'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'DATE_LOCK'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'RECOUNT_FLAG'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'AFFILIATE_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'DELIVERY_DOC_NUM'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'DELIVERY_DOC_DATE'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'UPDATED_1C'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'STORE_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'ORDER_TOPIC'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'RESPONSIBLE_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'DATE_BILL'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'DATE_PAY_BEFORE'=>[
				'TYPE'=>self::TYPE_DATETIME
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'ID_1C'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'VERSION_1C'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'VERSION'=>[
				'TYPE'=>self::TYPE_INT
			],
			'EXTERNAL_ORDER'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'COMPANY_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			//region List fields
			'PAYMENTS'=>['TYPE'=>self::TYPE_LIST, 'ATTRIBUTES'=>[Attributes::Hidden]],
			'SHIPMENTS'=>['TYPE'=>self::TYPE_LIST, 'ATTRIBUTES'=>[Attributes::Hidden]],
			'PROPERTY_VALUES'=>['TYPE'=>self::TYPE_LIST, 'ATTRIBUTES'=>[Attributes::Hidden]],
			'BASKET_ITEMS'=>['TYPE'=>self::TYPE_LIST, 'ATTRIBUTES'=>[Attributes::Hidden]],
			//endregion
		];
	}

	public function internalizeFieldsModify($fields)
	{
		$result=[];

		$params = ['ignoredAttributes'=>[Attributes::Hidden]];

		if(isset($fields['ORDER']))
		{
			$result['ORDER'] = $this->internalizeFields($fields['ORDER'], $params);
		}

		if(isset($fields['ORDER']['BASKET_ITEMS']))
		{
			$result['ORDER']['BASKET_ITEMS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['BASKET_ITEMS'], new \Bitrix\Sale\Rest\Entity\BasketItem(), ['PROPERTIES']);

			foreach ($result['ORDER']['BASKET_ITEMS'] as $k=>$items)
			{
				if(isset($items['PROPERTIES']))
					$result['ORDER']['BASKET_ITEMS'][$k]['PROPERTIES'] = $this->internalizeFieldsCollectionWithExcludeFields($items['PROPERTIES'], new \Bitrix\Sale\Rest\Entity\BasketProperties());
			}
		}

		if(isset($fields['ORDER']['PROPERTY_VALUES']))
			$result['ORDER']['PROPERTY_VALUES'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['PROPERTY_VALUES'], new \Bitrix\Sale\Rest\Entity\PropertyValue());

		if(isset($fields['ORDER']['PAYMENTS']))
			$result['ORDER']['PAYMENTS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['PAYMENTS'], new \Bitrix\Sale\Rest\Entity\Payment());

		if(isset($fields['ORDER']['SHIPMENTS']))
		{
			$result['ORDER']['SHIPMENTS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['SHIPMENTS'], new \Bitrix\Sale\Rest\Entity\Shipment(), ['SHIPMENT_ITEMS']);

			foreach ($result['ORDER']['SHIPMENTS'] as $k=>$items)
			{
				if(isset($items['SHIPMENT_ITEMS']))
					$result['ORDER']['SHIPMENTS'][$k]['SHIPMENT_ITEMS'] = $this->internalizeFieldsCollectionWithExcludeFields($items['SHIPMENT_ITEMS'], new \Bitrix\Sale\Rest\Entity\ShipmentItem());
			}
		}

		if(isset($fields['ORDER']['TRADE_BINDINGS']))
			$result['ORDER']['TRADE_BINDINGS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['TRADE_BINDINGS'], new \Bitrix\Sale\Rest\Entity\TradeBinding());

		return $result;
	}

	public function convertKeysToSnakeCaseArguments($name, $arguments)
	{
		if($name == 'import')
		{
			if(isset($arguments['fields']))
			{
				$fields = $arguments['fields'];
				if(!empty($fields))
					$arguments['fields'] = $this->convertKeysToSnakeCaseFields($fields);
			}
		}
		return $arguments;
	}

	public function internalizeArguments($name, $arguments)
	{
		if($name == 'import')
		{
			$fields = $arguments['fields'];
			if(!empty($fields))
				$arguments['fields'] = $this->internalizeFieldsImport($fields);
		}
		else
		{
			parent::internalizeArguments($name, $arguments);
		}

		return $arguments;
	}

	protected function internalizeFieldsImport($fields)
	{
		$result = [];

		$paramsImport = ['ignoredAttributes'=>[Attributes::Hidden]];

		if(isset($fields['ORDER']))
		{
			$result['ORDER'] = $this->internalizeFields($fields['ORDER'], $paramsImport);//only for importAction PERSON_TYPE_XML_ID, STATUS_XML_ID
		}

		if(isset($fields['ORDER']['BASKET_ITEMS']))
		{
			$result['ORDER']['BASKET_ITEMS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['BASKET_ITEMS'], new \Bitrix\Sale\Rest\Entity\BasketItem(), ['PROPERTIES']);

			foreach ($result['ORDER']['BASKET_ITEMS'] as $k=>$items)
			{
				if(isset($items['PROPERTIES']))
					$result['ORDER']['BASKET_ITEMS'][$k]['PROPERTIES'] = $this->internalizeFieldsCollectionWithExcludeFields($items['PROPERTIES'], new \Bitrix\Sale\Rest\Entity\BasketProperties());
			}
		}

		if(isset($fields['ORDER']['PROPERTY_VALUES']))
			$result['ORDER']['PROPERTY_VALUES'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['PROPERTY_VALUES'], new \Bitrix\Sale\Rest\Entity\PropertyValue());//only for importAction ORDER_PROPS_XML_ID

		if(isset($fields['ORDER']['PAYMENTS']))
			$result['ORDER']['PAYMENTS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['PAYMENTS'], new \Bitrix\Sale\Rest\Entity\Payment());//only for importAction PAY_SYSTEMS_XML_ID

		if(isset($fields['ORDER']['SHIPMENTS']))
		{
			$result['ORDER']['SHIPMENTS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['SHIPMENTS'], new \Bitrix\Sale\Rest\Entity\Shipment(), ['SHIPMENT_ITEMS']);//only for importAction DELIVERY_XML_ID, STATUS_XML_ID

			foreach ($result['ORDER']['SHIPMENTS'] as $k=>$items)
			{
				if(isset($items['SHIPMENT_ITEMS']))
					$result['ORDER']['SHIPMENTS'][$k]['SHIPMENT_ITEMS'] = $this->internalizeFieldsCollectionWithExcludeFields($items['SHIPMENT_ITEMS'], new \Bitrix\Sale\Rest\Entity\ShipmentItem());
			}
		}

		if(isset($fields['ORDER']['TRADE_BINDINGS']))
			$result['ORDER']['TRADE_BINDINGS'] = $this->internalizeFieldsCollectionWithExcludeFields($fields['ORDER']['TRADE_BINDINGS'], new \Bitrix\Sale\Rest\Entity\TradeBinding());//only for importAction TRADING_PLATFORM_XML_ID

		return $result;
	}

	protected function getRewritedFields()
	{
		return [
			'PERSON_TYPE_XML_ID'=>[
				'REFERENCE_FIELD'=>'PERSON_TYPE.XML_ID'
			],
			'STATUS_XML_ID'=>[
				'REFERENCE_FIELD'=>'STATUS_TABLE.XML_ID'
			]
		];
	}

	public function externalizeFields($fields)
	{
		$basketItem = new \Bitrix\Sale\Rest\Entity\BasketItem();
		$payment = new \Bitrix\Sale\Rest\Entity\Payment();
		$shipment = new \Bitrix\Sale\Rest\Entity\Shipment();
		$shipmentItem = new \Bitrix\Sale\Rest\Entity\ShipmentItem();
		$tradeBinding = new \Bitrix\Sale\Rest\Entity\TradeBinding();
		$propertyValue = new \Bitrix\Sale\Rest\Entity\PropertyValue();
		$basketProperties = new \Bitrix\Sale\Rest\Entity\BasketProperties();

		$result = parent::externalizeFields($fields);

		if(isset($fields['PROPERTY_VALUES']))
			$result['PROPERTY_VALUES'] = $propertyValue->externalizeListFields($fields['PROPERTY_VALUES']);

		if(isset($fields['BASKET_ITEMS']))
		{
			foreach ($fields['BASKET_ITEMS'] as $k=>$item)
			{
				$result['BASKET_ITEMS'][$k] = $basketItem->externalizeFields($item);
				$result['BASKET_ITEMS'][$k]['PROPERTIES'] = $basketProperties->externalizeFields($item);
			}
		}

		if(isset($fields['PAYMENTS']))
		{
			$result['PAYMENTS'] = $payment->externalizeListFields($fields['PAYMENTS']);
		}

		if(isset($fields['SHIPMENTS']))
		{
			foreach($fields['SHIPMENTS'] as $k=>$item)
			{
				$result['SHIPMENTS'][$k] = $shipment->externalizeFields($item);
				if(isset($item['SHIPMENT_ITEMS']))
				{
					$data['SHIPMENTS'][$k]['SHIPMENT_ITEMS'] = $shipmentItem->externalizeListFields($item['SHIPMENT_ITEMS']);
				}
			}
		}

		//TODO: �������� ��� ������ ����� TradeBinding ���������� ��� ��������������� rest
		if(isset($data['TRADE_BINDINGS']))
			$result['TRADE_BINDINGS'] = $tradeBinding->externalizeListFields($fields['TRADE_BINDINGS']);

		return $result;
	}

	public function externalizeFieldsModify($fields)
	{
		return $this->externalizeFields($fields);
	}

	public function externalizeResult($name, $fields)
	{
		if($name == 'import')
		{
			$result = $this->externalizeFieldsModify($fields);
		}
		else
		{
			$result = parent::externalizeResult($name, $fields);
		}

		return $result;
	}

	public function checkRequiredFieldsModify($fields)
	{
		$r = new Result();

		$payment = new Payment();
		$shipment = new Shipment();
		$basketItem = new BasketItem();
		$propertyValue = new PropertyValue();

		$required = $this->checkRequiredFields($fields['ORDER'], ['ignoredAttributes'=>[Attributes::Hidden, Attributes::ReadOnly]]);
		if($required->isSuccess() == false)
		{
			$r->addError(new Error(implode(', ', $required->getErrorMessages()).'.'));
		}

		$required = $propertyValue->checkRequiredFieldsModify($fields);
		if($required->isSuccess() == false)
		{
			$r->addError(new Error(implode(', ', $required->getErrorMessages())));
		}

		if(isset($fields['ORDER']['BASKET_ITEMS']))
		{
			$required = $basketItem->checkRequiredFieldsModify($fields);
			if($required->isSuccess() == false)
			{
				$r->addError(new Error(implode(', ', $required->getErrorMessages())));
			}
		}

		if(isset($fields['ORDER']['PAYMENTS']))
		{
			$required = $payment->checkRequiredFieldsModify($fields);
			if($required->isSuccess() == false)
			{
				$r->addError(new Error(implode(', ', $required->getErrorMessages())));
			}
		}

		if(isset($fields['ORDER']['SHIPMENTS']))
		{
			$required = $shipment->checkRequiredFieldsModify($fields);
			if($required->isSuccess() == false)
			{
				$r->addError(new Error(implode(', ', $required->getErrorMessages())));
			}
		}

		return $r;
	}
}