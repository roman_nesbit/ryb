<?php


namespace Bitrix\Sale\Controller;


use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\LanguageTable;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\TaskTable;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Sale\Internals\StatusGroupTaskTable;
use Bitrix\Sale\Internals\StatusLangTable;
use Bitrix\Sale\Internals\StatusTable;
use Bitrix\Sale\Result;

class Status extends Controller
{
	//region Actions
	public function getFieldsAction()
	{
		$entity = new \Bitrix\Sale\Rest\Entity\Status();
		return ['STATUS'=>$entity->prepareFieldInfos(
			$entity->getFields()
		)];
	}

	public function modifyAction(array $fields)
	{
		$status = new \CSaleStatus();

		$fields = self::prepareFields($fields);

		$r = $this->validate($fields);
		if($r->isSuccess())
		{
			$isNew = empty($this->get($fields['STATUS']['ID'])['STATUS']['ID']);

			$r = $this->save($fields);
			if($r->isSuccess())
			{
				if($isNew)
				{
					$status->CreateMailTemplate($fields['STATUS']['ID']);
				}
			}
		}

		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}
		else
		{
			return $this->get($fields['STATUS']['ID']);
		}
	}

	public function deleteAction($id)
	{
		global $APPLICATION;

		$status = new \CSaleStatus();

		$r = new Result();

		if (in_array($id, [
			\Bitrix\Sale\OrderStatus::getInitialStatus(),
			\Bitrix\Sale\OrderStatus::getFinalStatus(),
			\Bitrix\Sale\DeliveryStatus::getInitialStatus(),
			\Bitrix\Sale\DeliveryStatus::getFinalStatus()]))
		{
			 $r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_DELETE_STATUS_TYPE_LOCKED', ['#ID#'=>$id]),'ERROR_DELETE_STATUS_TYPE_LOCKED'));
		}
		else
		{
			$r = $this->exists($id);
			if($r->isSuccess())
			{
				if (!$status->Delete($id))
				{
					if ($ex = $APPLICATION->GetException())
						$r->addError(new Error($ex->GetString(), $id));
					else
						$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_DELETE_STATUS', ['#ID#'=>$id]), 'ERROR_DELETE_STATUS'));
				}
			}
		}

		if($r->isSuccess())
		{
			return true;
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function getAction($id)
	{
		return $this->get($id);
	}

	public function listAction($select=[], $filter, $order=[], PageNavigation $pageNavigation)
	{
		$result = [];
		$select = empty($select)? ['*']:$select;
		$order = empty($order)? ['ID'=>'ASC']:$order;

		$r = \CSaleStatus::GetList($order, $filter, false, self::getNavData($pageNavigation->getOffset()), $select);
		while ($l = $r->fetch())
			$result[] = $l;

		return new Page('STATUSES', $result, function() use ($filter)
		{
			$list = [];
			$r = \CSaleStatus::GetList([], $filter);
			while ($l = $r->fetch())
				$list[] = $l;

			return count($list);
		});
	}
	//endregion
	protected function get($id)
	{
		if(empty($id))
		{
			return [];
		}

		$r['STATUS'] = StatusTable::getById($id)->fetch();

		$result = StatusLangTable::getList([
			'select' => ['*'],
			'filter' => ['=STATUS_ID' => $id],
		]);
		while ($row = $result->fetch())
			$r['STATUS']['LANGS'][] = $row;


		$result = StatusGroupTaskTable::getList([
			'select' => ['*'],
			'filter' => ['=STATUS_ID' => $id],
		]);
		while ($row = $result->fetch())
			$r['STATUS']['GROUP_TASKS'][] = $row;

		return $r;
	}

	static public function prepareFields(array $fields)
	{
		$fields['STATUS']['SORT'] = intval($fields['STATUS']['SORT'])?$fields['STATUS']['SORT']:100;
		$fields['STATUS']['NOTIFY'] = isset($fields['STATUS']['NOTIFY']) && $fields['STATUS']['NOTIFY']=='Y'?'Y':'N';

		if(isset($fields['STATUS']['ID']))
		{
			if(isset($fields['LANGS']))
			{
				foreach ($fields['LANGS'] as $kl=>$lang)
				{
					$fields['LANGS'][$kl]['STATUS_ID'] = $fields['STATUS']['ID'];
				}
			}

			if(isset($fields['GROUP_TASKS']))
			{
				foreach ($fields['GROUP_TASKS'] as $kg=>$group)
				{
					$fields['GROUP_TASKS'][$kg]['STATUS_ID'] = $fields['STATUS']['ID'];
				}
			}
		}

		$fields['LANGS'] = isset($fields['LANGS'])? $fields['LANGS']:[];
		$fields['GROUP_TASKS'] = isset($fields['GROUP_TASKS'])? $fields['GROUP_TASKS']:[];

		return $fields;
	}

	protected function getLockedStatusType($statusId)
	{
		$lockedStatusList = [
			\Bitrix\Sale\OrderStatus::TYPE=>[
				\Bitrix\Sale\OrderStatus::getInitialStatus(),
				\Bitrix\Sale\OrderStatus::getFinalStatus()
			],
			\Bitrix\Sale\DeliveryStatus::TYPE=>[
				\Bitrix\Sale\DeliveryStatus::getInitialStatus(),
				\Bitrix\Sale\DeliveryStatus::getFinalStatus()
			]
		];

		foreach ($lockedStatusList as $lockStatusType=>$lockStatusIdList)
		{
			foreach ($lockStatusIdList as $lockStatusId)
			{
				if ($lockStatusId == $statusId)
				{
					return $lockStatusType;
				}
			}
		}
		return '';
	}

	protected function getListLangs()
	{
		$r=[];
		$result = LanguageTable::getList([
			'select' => ['LID', 'NAME'],
			'filter' => ['=ACTIVE'=>'Y']
		]);
		while ($row = $result->fetch())
			$r[$row['LID']] = $row['NAME'];
		return $r;
	}

	protected function getListTasks()
	{
		$r=[];
		$result = TaskTable::getList(array(
			'select' => array('*'),
			'filter' => array('=MODULE_ID' => 'sale', '=BINDING' => 'status'),
		));
		while ($row = $result->fetch())
			$r[$row['ID']] = $row;
		asort($r);
		return $r;
	}

	protected function validate(array $fields)
	{
		$r = $this->validateStatus($fields['STATUS']);
		if(!$r->isSuccess())
			return $r;

		$r = $this->validateLang($fields['LANGS']);
		if(!$r->isSuccess())
			return $r;

		$r = $this->validateTasks($fields['GROUP_TASKS']);
		if(!$r->isSuccess())
			return $r;

		return $r;
	}

	protected function validateStatus(array $fields)
	{
		$r = new Result();

		if(!in_array($fields['TYPE'], [
			\Bitrix\Sale\OrderStatus::TYPE,
			\Bitrix\Sale\DeliveryStatus::TYPE
		]))
		{
			$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TYPE_OUT_OF_RANGE'), 'ERROR_STATUS_TYPE_OUT_OF_RANGE'));
		}

		if(trim($fields['ID'])=='')
		{
			$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TYPE_ID_EMPTY'), 'ERROR_STATUS_TYPE_ID_EMPTY'));
		}
		elseif(strlen($fields['ID'])>2)
		{
			$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TYPE_STRLEN'), 'ERROR_STATUS_TYPE_STRLEN'));
		}

		/* TODO: check is_latin()
		 * if(!is_latin($fields['TYPE']))
		{
			$r->addError(new Error('', 'ERROR_STATUS_TYPE_LATIN_ONLY'));
		}*/

		if($r->isSuccess())
		{
			if($status = $this->get($fields['ID'])['STATUS']['FIELDS'])
			{
				$lockedType = $this->getLockedStatusType($fields['ID']);
				if($lockedType<>'' && $lockedType!=$fields['TYPE'])
				{
					$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_WRONG_TYPE'),'STATUS_WRONG_TYPE'));
				}

				if ($status['TYPE'] != $fields['TYPE'])
				{
					if ($status['TYPE'] == \Bitrix\Sale\OrderStatus::TYPE)
					{
						if(\Bitrix\Sale\Internals\OrderTable::getList([
							'select'=>['ID'],
							'filter'=>['STATUS_ID'=>$status['ID']],
							'limit'=>1
						])->fetch())
						{
							$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TYPE_ORDER_EXISTS'),'STATUS_TYPE_ORDER_EXISTS'));
						}
					}
					else
					{
						if(\Bitrix\Sale\Internals\ShipmentTable::getList([
							'select'=>['ID'],
							'filter'=>['STATUS_ID'=>$status['ID']],
							'limit'=>1
						])->fetch())
						{
							$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TYPE_SHIPMENT_EXISTS'),'STATUS_TYPE_SHIPMENT_EXISTS'));
						}
					}
				}

				if(isset($fields['ID']))
				{
					// only for checkFields()
					unset($fields['ID']);
				}

				StatusTable::checkFields($r, $status['ID'], $fields);
			}
			else
			{
				StatusTable::checkFields($r, null, $fields);
			}
		}

		return $r;
	}

	protected function validateLang(array $list)
	{
		$r = new Result();
		$listLid = [];

		if(count($list)<=0)
		{
			$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_LANGS_EMPTY'), 'ERROR_STATUS_LANGS_EMPTY'));
		}
		else
		{
			foreach ($list as $fields)
				$listLid[] = $fields['LID'];

			foreach (array_keys($this->getListLangs()) as $languageId)
			{
				if(!in_array($languageId, $listLid))
				{
					$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_LANG_NO_NAME', ['#ID#'=>$languageId]),'ERROR_STATUS_LANG_NO_NAME'));
				}
			}
		}

		return $r;
	}

	protected function validateTasks(array $list)
	{
		$r = new Result();

		if(count($list)<=0)
		{
			$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_GROUPS_TASK_ID_EMPTY'), 'ERROR_STATUS_GROUPS_TASK_ID_EMPTY'));
		}
		else
		{
			foreach ($list as $fields)
			{
				if(intval($fields['TASK_ID'])<=0)
				{
					$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TASK_ID_EMPTY'), 'ERROR_STATUS_TASK_ID_EMPTY'));
				}
				else
				{
					if(!in_array($fields['TASK_ID'], array_keys($this->getListTasks())))
					{
						$r->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_TASK_ID_INVALID'), 'ERROR_STATUS_TASK_ID_INVALID'));
					}
				}
			}
		}

		return $r;
	}

	protected function save(array $fields)
	{
		$r = $this->saveStatus($fields['STATUS']);
		if(!$r->isSuccess())
			return $r;

		$r = $this->saveLangs($fields['LANGS']);
		if(!$r->isSuccess())
			return $r;

		$this->saveGroups($fields['GROUP_TASKS']);
		if(!$r->isSuccess())
			return $r;

		return $r;
	}

	protected function saveStatus(array $fields)
	{
		if($status = $this->get($fields['ID'])['STATUS']['FIELDS'])
		{
			$r = StatusTable::update($status['ID'], $fields);
			if ($r->isSuccess())
			{
				StatusLangTable::deleteByStatus($status['ID']);
				StatusGroupTaskTable::deleteByStatus($status['ID']);
			}
		}
		else
		{
			$r = StatusTable::add($fields);
		}
		return $r;
	}

	protected function saveLangs(array $list)
	{
		$r = new Result();
		foreach ($list as $fields)
		{
			StatusLangTable::add([
				'STATUS_ID'=>$fields['STATUS_ID'],
				'LID'=>$fields['LID'],
				'NAME'=>$fields['NAME'],
				'DESCRIPTION'=>$fields['DESCRIPTION'],
			]);
		}
		return $r;
	}

	protected function saveGroups(array $list)
	{
		$r = new Result();
		foreach ($list as $fields)
		{
			StatusGroupTaskTable::add([
				'STATUS_ID'=>$fields['STATUS_ID'],
				'GROUP_ID'=>$fields['GROUP_ID'],
				'TASK_ID'=>$fields['TASK_ID'],
			]);
		}
		return $r;
	}

	protected function exists($id)
	{
		$r = new Result();
		if(strlen($this->get($id)['STATUS']['FIELDS']['ID'])!==2)
			$this->addError(new Error(Loc::getMessage('CONTROLLER_ERROR_STATUS_NOT_EXISTS', ['#ID#'=>$id]), 'STATUS_NOT_EXISTS'));

		return $r;
	}
}