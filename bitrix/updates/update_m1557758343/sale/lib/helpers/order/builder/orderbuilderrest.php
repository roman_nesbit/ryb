<?php


namespace Bitrix\Sale\Helpers\Order\Builder;


use Bitrix\Sale\Delivery\Services\EmptyDeliveryService;
use Bitrix\Sale\Internals\Input\File;
use Bitrix\Sale\PaySystem\Manager;
use Bitrix\Sale\PropertyValue;
use Bitrix\Sale\PropertyValueBase;
use Bitrix\Sale\PropertyValueCollection;
use Bitrix\Sale\Rest\Attributes;
use Bitrix\Sale\Rest\Synchronization\LoggerDiag;
use Bitrix\Sale\Result;
use Bitrix\Sale\Shipment;
use Bitrix\Sale\ShipmentItem;

/**
 * Class OrderBuilderRestSale
 * @package Bitrix\Sale\Helpers\Order\Builder
 * @internal
 */
class OrderBuilderRest extends OrderBuilder
{
	public function __construct(SettingsContainer $settings)
	{
		parent::__construct($settings);
		$this->setBasketBuilder(new BasketBuilderRest($this));
	}

	protected function prepareFields(array $fields)
	{
		$fields = array_merge(
			\Bitrix\Sale\Controller\Order::prepareFields($fields),
			\Bitrix\Sale\Controller\PropertyValue::prepareFields($fields['ORDER']),
			\Bitrix\Sale\Controller\BasketItem::prepareFields($fields['ORDER']),
			\Bitrix\Sale\Controller\Payment::prepareFields($fields['ORDER']),
			\Bitrix\Sale\Controller\Shipment::prepareFields($fields['ORDER']),
			\Bitrix\Sale\Controller\TradeBinding::prepareFields($fields['ORDER'])
		);

		return parent::prepareFields($fields);
	}

	protected function createEmptyPayment()
	{
		if($this->getSettingsContainer()->getItemValue('createDefaultPaymentIfNeed'))
		{
			$this->formData["PAYMENT"] = [
				[
					'SUM'=>$this->getOrder()->getPrice(),
					'PAID'=>'N',
					'PAY_SYSTEM_ID'=>Manager::getInnerPaySystemId()
				]
			];

			parent::buildPayments();
		}
		return $this;
	}

	protected function createEmptyShipment()
	{
		if($this->getSettingsContainer()->getItemValue('createDefaultShipmentIfNeed'))
		{
			$this->formData["SHIPMENT"] = [
				[
					'DEDUCTED'=>'N',
					'DELIVERY_ID'=>EmptyDeliveryService::getEmptyDeliveryServiceId()
				]
			];
			parent::buildShipments();
		}
		return $this;
	}

	protected function prepareFieldsStatusId($isNew, $item, $defaultFields)
	{
		$statusId = '';
		if($isNew)
		{
			if (isset($item['STATUS_ID']))
			{
				$statusId = $item['STATUS_ID'];
			}
		}
		else
		{
			$statusId = parent::prepareFieldsStatusId($isNew, $item, $defaultFields);
		}

		return $statusId;
	}

	public function buildShipments()
	{
		if(isset($this->formData["SHIPMENT"]) && is_array($this->formData["SHIPMENT"]))
		{
			$orderShipmentItems = [];
			$shipmentCollection = $this->order->getShipmentCollection();

			/** @var Shipment $shipment */
			foreach($shipmentCollection as $shipment)
			{
				if($shipment->isSystem())
					continue;

				/** @var ShipmentItem $shipmentItem */
				foreach($shipment->getShipmentItemCollection() as $shipmentItem)
				{
					$orderShipmentItems[$shipment->getId()][$shipmentItem->getId()]=$shipmentItem->getBasketCode();
				}
			}

			foreach($this->formData['SHIPMENT'] as $k=>$shipmentFormData)
			{
				$shipmentIdFormData=0;
				if(isset($shipmentFormData['ID']))
					$shipmentIdFormData = $shipmentFormData['ID'];

				if(is_array($shipmentFormData['PRODUCT']))
				{
					foreach(array_keys($shipmentFormData['PRODUCT']) as $basketCode)
					{
						$orderDeliverybasketId = 0;

						//region fill Id - ShipmentItems
						if($shipmentIdFormData>0)
						{
							if(is_array($orderShipmentItems[$shipmentIdFormData]))
							{
								foreach ($orderShipmentItems[$shipmentIdFormData] as $itemId=>$itemBasketCode)
								{
									if($itemBasketCode == $basketCode)
									{
										$orderDeliverybasketId = $itemId;
										break;
									}
								}
							}
						}
						//endregion

						$this->formData['SHIPMENT'][$k]['PRODUCT'][$basketCode]['ORDER_DELIVERY_BASKET_ID'] = $orderDeliverybasketId;
					}
				}
			}
		}
		return parent::buildShipments();
	}

	protected function removeShipmentItems(\Bitrix\Sale\Shipment $shipment, $products, $idsFromForm)
	{
		if(is_array($products))// ���� ������� products, �� �������, ��� ��������� ������ ��� �������� ��������
		{
			return parent::removeShipmentItems($shipment, $products, $idsFromForm);
		}
		return new Result();
	}

	public function setProperties()
	{
		$result = new Result();

		if(!isset($this->formData["PROPERTIES"]))
		{
			return $this;
		}

		$r = $this->removePropertyValues();
		if($r->isSuccess() == false)
		{
			$this->getErrorsContainer()->addErrors($r->getErrors());
			return $this;
		}

		$this->formData["PROPERTIES"] = File::getPostWithFiles(
			$this->formData["PROPERTIES"],
			$this->settingsContainer->getItemValue('propsFiles')
		);

		$propCollection = $this->order->getPropertyCollection();

		foreach ($this->formData["PROPERTIES"] as $id=>$value)
		{
			if(($propertyValue = $propCollection->getItemByOrderPropertyId($id)))
			{
				$propertyValue->setValue($value);
			}
		}

		return $result;
	}

	protected function removePropertyValues()
	{
		$result = new Result();

		if($this->getSettingsContainer()->getItemValue('deletePropertyValuesIfNotExists'))
		{
			$propCollection = $this->order->getPropertyCollection();
			/** @var PropertyValueBase $propertyValue */
			foreach($propCollection as $propertyValue)
			{
				if(is_set($this->formData["PROPERTIES"],$propertyValue->getPropertyId()) == false)
				{
					$r = $propertyValue->delete();
					if (!$r->isSuccess())
					{
						$result->addErrors($r->getErrors());
					}
				}
			}
		}

		return $result;
	}

	protected function getSettableTradeBindingFields()
	{
		$binding = new \Bitrix\Sale\Rest\Entity\TradeBinding();
		return $binding->getSettableFields();
	}

	protected function getSettableShipmentFields()
	{
		$shipment = new \Bitrix\Sale\Rest\Entity\Shipment();
		return $shipment->getSettableFields();
	}

	protected function getSettablePaymentFields()
	{
		$payment = new \Bitrix\Sale\Rest\Entity\Payment();
		return $payment->getSettableFields();
	}

	protected function getSettableOrderFields()
	{
		$order = new \Bitrix\Sale\Rest\Entity\Order;
		return $order->getSettableFields();
	}

	protected function checkDeliveryRestricted($shipment, $deliveryService, $shipmentFields)
	{
		// ��� rest ��� �������� �� �����������. �� ��� �������� �� �������� ������� ����������
		return true;
	}

	public function buildEntityShipments(array $fields)
	{
		try{
			$this->initFields($fields)
				->delegate()
				->createOrder()
				->setDiscounts()
				->buildShipments()
				->setDiscounts()
				->finalActions();
		}
		catch(BuildingException $e)
		{
			return null;
		}

		return $this->getOrder();
	}

	public function buildEntityPayments(array $fields)
	{
		try{
			$this->initFields($fields)
				->delegate()
				->createOrder()
				->setDiscounts()
				->buildPayments()
				->setDiscounts()
				->finalActions();
		}
		catch(BuildingException $e)
		{
			return null;
		}

		return $this->getOrder();
	}

	public function buildEntityBasket(array $fields)
	{
		try{
			$this->initFields($fields)
				->delegate()
				->createOrder()
				->setDiscounts() //?
				->buildBasket()
				->setDiscounts() //?
				->finalActions();
		}
		catch(BuildingException $e)
		{
			return null;
		}

		return $this->getOrder();
	}

	public function buildEntityOrder(array $fields)
	{
		try{
			$this->initFields($fields)
				->delegate()
				->createOrder()
				->setDiscounts() //?
				->setFields()
				->setUser()
				->finalActions();
		}
		catch(BuildingException $e)
		{
			return null;
		}

		return $this->getOrder();
	}

	public function buildEntityProperties(array $fields)
	{
		try{
			$this->initFields($fields)
				->delegate()
				->createOrder()
				->setDiscounts() //?
				->setProperties()
				->setDiscounts() //?
				->finalActions();
		}
		catch(BuildingException $e)
		{
			return null;
		}

		return $this->getOrder();
	}
}