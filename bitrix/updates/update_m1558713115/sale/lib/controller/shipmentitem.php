<?php


namespace Bitrix\Sale\Controller;


use Bitrix\Main\Engine\AutoWire\ExactParameter;
use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\Error;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Result;
use Bitrix\Sale\ShipmentCollection;

class ShipmentItem extends Controller
{
	public function getPrimaryAutoWiredParameter()
	{
		return new ExactParameter(
			\Bitrix\Sale\ShipmentItem::class,
			'shipmentItem',
			function($className, $id) {

				$si = \Bitrix\Sale\ShipmentItem::getList([
					'select'=>['ORDER_DELIVERY_ID'],
					'filter'=>['ID'=>$id]
				]);

				if($siRow = $si->fetch())
				{
					$r = \Bitrix\Sale\Shipment::getList([
						'select'=>['ORDER_ID'],
						'filter'=>['ID'=>$siRow['ORDER_DELIVERY_ID']]
					]);

					if($row = $r->fetch())
					{
						$order = \Bitrix\Sale\Order::load($row['ORDER_ID']);
						/** @var \Bitrix\Sale\Shipment $shipment */
						$shipment = $order->getShipmentCollection()->getItemById($siRow['ORDER_DELIVERY_ID']);
						$shipmentItem = $shipment->getShipmentItemCollection()->getItemById($id);

						if($shipmentItem instanceof \Bitrix\Sale\ShipmentItem)
						{
							return $shipmentItem;
						}
					}
				}

				$this->addError(new Error('shipmentItem is not exists', 1200010));
				return null;
			}
		);
	}

	public function getAutoWiredParameters()
	{
		return [
			new \Bitrix\Main\Engine\AutoWire\Parameter(
				\Bitrix\Sale\ShipmentItem::class,
				function($className, $id) {

					$si = \Bitrix\Sale\ShipmentItem::getList([
						'select'=>['ORDER_DELIVERY_ID'],
						'filter'=>['ID'=>$id]
					]);

					if($siRow = $si->fetch())
					{
						$r = \Bitrix\Sale\Shipment::getList([
							'select'=>['ORDER_ID'],
							'filter'=>['ID'=>$siRow['ORDER_DELIVERY_ID']]
						]);

						if($row = $r->fetch())
						{
							$order = \Bitrix\Sale\Order::load($row['ORDER_ID']);
							/** @var \Bitrix\Sale\Shipment $shipment */
							$shipment = $order->getShipmentCollection()->getItemById($siRow['ORDER_DELIVERY_ID']);
							$shipmentItem = $shipment->getShipmentItemCollection()->getItemById($id);

							if($shipmentItem instanceof \Bitrix\Sale\ShipmentItem)
							{
								return $shipmentItem;
							}
						}
					}
					$this->addError(new Error('shipmentItem is not exists', 1200020));
					return null;
				})
		];
	}

	//region Actions
	public function getFieldsAction()
	{
		$entity = new \Bitrix\Sale\Rest\Entity\ShipmentItem();
		return ['SHIPMENT_ITEM'=>$entity->prepareFieldInfos(
			$entity->getFields()
		)];
	}

	public function getAction(\Bitrix\Sale\ShipmentItem $shipmentItem)
	{
		return ['SHIPMENT_ITEM'=>$this->get($shipmentItem)];
	}

	public function listAction($select=[], $filter=[], $order=[], PageNavigation $pageNavigation)
	{
		$select = empty($select)? ['*']:$select;
		$order = empty($order)? ['ID'=>'ASC']:$order;

		$shipmentItems = \Bitrix\Sale\ShipmentItem::getList(
			[
				'select'=>$select,
				'filter'=>$filter,
				'order'=>$order,
				'offset' => $pageNavigation->getOffset(),
				'limit' => $pageNavigation->getLimit()
			]
		)->fetchAll();

		return new Page('SHIPMENT_ITEMS', $shipmentItems, function() use ($select, $filter)
		{
			return count(
				\Bitrix\Sale\ShipmentItem::getList(['select'=>$select, 'filter'=>$filter])->fetchAll()
			);
		});
	}

	public function addAction(array $fields)
	{
		$result = new Result();

		$basketId = $fields['BASKET_ID'];
		$shipmentId = $fields['ORDER_DELIVERY_ID'];

		unset($fields['ORDER_DELIVERY_ID'], $fields['BASKET_ID']);

		$r = \Bitrix\Sale\Basket::getList([
			'select'=>['ORDER_ID'],
			'filter'=>['ID'=>$basketId]
		]);

		if($row = $r->fetch())
		{
			$order = \Bitrix\Sale\Order::load($row['ORDER_ID']);
			$basketItem = $order->getBasket()->getItemByBasketCode($basketId);
			if($basketItem instanceof BasketItem)
			{
				/** @var ShipmentCollection $collection */
				$collection = $order->getShipmentCollection();
				$shipment = $collection->getItemById($shipmentId);
				if($shipment instanceof \Bitrix\Sale\Shipment)
				{
					$shipmentItemCollection = $shipment->getShipmentItemCollection();
					if($shipmentItemCollection->isExistBasketItem($basketItem) == false)
					{
						/** @var \Bitrix\Sale\ShipmentItem $shipmentItem */
						$shipmentItem = $shipmentItemCollection->createItem($basketItem);
						$result = $shipmentItem->setFields($fields);
						if($result->isSuccess())
						{
							/** @var \Bitrix\Sale\Order $order */
							$order = $shipmentItem->getCollection()->getShipment()->getCollection()->getOrder();
							$result = $order->save();
						}
					}
					else
					{
						$result->addError(new Error('Duplicate entry for key [basketId, orderDeliveryId]'));
					}
				}
				else
				{
					$result->addError(new Error('Shipment not exists', 1200050));
				}
			}
		}

		if(!$result->isSuccess())
		{
			$this->addErrors($result->getErrors());
			return null;
		}
		elseif($result->hasWarnings())
		{
			$this->addErrors($result->getWarnings());
			return null;
		}
		else
		{
			return ['SHIPMENT_ITEM'=>$this->get($shipmentItem)];
		}
	}

	public function updateAction(\Bitrix\Sale\ShipmentItem $shipmentItem, array $fields)
	{
		$r = $shipmentItem->setFields($fields);

		if($r->isSuccess())
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $shipmentItem->getCollection()->getShipment()->getCollection()->getOrder();

			$r = $order->save();
			if(!$r->isSuccess())
			{
				$this->addErrors($r->getErrors());
				return null;
			}
			elseif($r->hasWarnings())
			{
				$this->addErrors($r->getWarnings());
				return null;
			}

			return ['SHIPMENT_ITEM'=>$this->get($shipmentItem)];
		}
		elseif($r->hasWarnings())
		{
			$this->addErrors($r->getWarnings());
			return null;
		}
		else
		{
			$this->addErrors($r->getErrors());
			return null;
		}
	}

	public function deleteAction(\Bitrix\Sale\ShipmentItem $shipmentItem)
	{
		$r = $shipmentItem->delete();
		return $this->save($shipmentItem, $r);
	}
	//endregion

	protected function get(\Bitrix\Sale\ShipmentItem $shipmentItem, array $fields=[])
	{
		$shipments = $this->toArray($shipmentItem->getCollection()->getShipment()->getCollection()->getOrder(), $fields)['ORDER']['SHIPMENTS'];
		foreach ($shipments as $shipment)
		{
			foreach ($shipment['BASKET']['ITEMS'] as $item)
			{
				if($item['ID']==$shipmentItem->getId())
				{
					return $item;
				}
			}
		}
		return [];
	}

	private function save(\Bitrix\Sale\ShipmentItem $shipmentItem, Result $r)
	{
		if(!$r->isSuccess())
		{
			$this->addErrors($r->getErrors());
			return null;
		}
		else
		{
			/** @var \Bitrix\Sale\Order $order */
			$order = $shipmentItem->getCollection()->getShipment()->getCollection()->getOrder();
			$r = $order->save();
			if(!$r->isSuccess())
			{
				$this->addErrors($r->getErrors());
				return null;
			}
		}

		return $r->isSuccess();
	}
}