<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Sale\Rest\Attributes;

class StatusLang extends Base
{
	public function getFields()
	{
		return [
			'STATUS_ID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
				],
			'LID'=>[
				'TYPE'=>self::TYPE_STRING,
				'ATTRIBUTES'=>[Attributes::Required]
				],
			'NAME'=>[
				'TYPE'=>self::TYPE_STRING,
				],
			'DESCRIPTION'=>[
				'TYPE'=>self::TYPE_STRING,
				]
		];
	}
}