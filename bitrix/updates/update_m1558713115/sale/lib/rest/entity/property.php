<?php


namespace Bitrix\Sale\Rest\Entity;


use Bitrix\Sale\Rest\Attributes;

class Property extends Base
{
	public function getFields()
	{
		return [
			'ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[Attributes::ReadOnly]
			],
			'NAME'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'TYPE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'PERSON_TYPE_ID'=>[
				'TYPE'=>self::TYPE_INT,
				'ATTRIBUTES'=>[
					Attributes::Required,
					Attributes::Immutable
				]
			],
			'REQUIRED'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'DEFAULT_VALUE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'SORT'=>[
				'TYPE'=>self::TYPE_INT
			],
			'USER_PROPS'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_LOCATION'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'PROPS_GROUP_ID'=>[
				'TYPE'=>self::TYPE_INT
			],
			'DESCRIPTION'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'IS_EMAIL'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_PROFILE_NAME'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_PAYER'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_LOCATION4TAX'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_FILTERED'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'CODE'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'IS_ZIP'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_PHONE'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'IS_ADDRESS'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'ACTIVE'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'UTIL'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'INPUT_FIELD_LOCATION'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'MULTIPLE'=>[
				'TYPE'=>self::TYPE_CHAR
			],
			'SETTINGS'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'XML_ID'=>[
				'TYPE'=>self::TYPE_STRING
			],
			'VARIANTS'=>[
				'TYPE'=>self::TYPE_LIST,
				'ATTRIBUTES'=>[Attributes::Hidden]
			]
		];
	}
}