<?
$updater->CopyFiles("install/components", "components");
$updater->CopyFiles("install/js", "js");

if ($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"components/bitrix/main.ui.grid/templates/.default/js/updater.js"
	);

	foreach($arToDelete as $file)
	{
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
	}
}

if($updater->CanUpdateDatabase())
{
	$agent = $DB->ForSql("\\Bitrix\\Main\\UI\\Viewer\\FilePreviewTable::deleteOldAgent();");
	$res = $DB->Query("SELECT 'x' FROM b_agent WHERE MODULE_ID='main' AND NAME='".$agent."'");
	if(!$res->Fetch())
	{
		$updater->Query("INSERT INTO b_agent (MODULE_ID, SORT, NAME, ACTIVE, AGENT_INTERVAL, IS_PERIOD, NEXT_EXEC) VALUES('main', 100, '".$agent."', 'Y', 86400, 'N', ".$DB->GetNowDate().")");
	}

	$entity = \Bitrix\Main\UserIndexTable::getEntity();
	if($DB->IndexExists("b_user_index", array("SEARCH_USER_CONTENT"), true))
	{
		$entity->enableFullTextIndex("SEARCH_USER_CONTENT", true);
	}
	if($DB->IndexExists("b_user_index", array("SEARCH_DEPARTMENT_CONTENT"), true))
	{
		$entity->enableFullTextIndex("SEARCH_DEPARTMENT_CONTENT", true);
	}
	if($DB->IndexExists("b_user_index", array("SEARCH_ADMIN_CONTENT"), true))
	{
		$entity->enableFullTextIndex("SEARCH_ADMIN_CONTENT", true);
	}

	COption::RemoveOption("main", "~ft_b_user");
}
