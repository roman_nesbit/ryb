<?php

if ($updater->CanUpdateDatabase()) {
    if ($updater->TableExists('artamonov_rest_request_response')) {
        $updater->Query([
            'MySQL' => 'ALTER TABLE artamonov_rest_request_response MODIFY COLUMN IP CHAR(120) DEFAULT NULL;',
        ]);
    }
}
