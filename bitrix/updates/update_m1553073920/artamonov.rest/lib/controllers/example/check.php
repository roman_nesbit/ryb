<?php

namespace Artamonov\Rest\Controllers\Example;


use Bitrix\Main\Loader;
use CFile;
use CIBlockElement;

class Check
{
    // Время хранения кэша
    private $cacheTtl = 604800;
    // Директория хранения кэша относительно /bitrix/cache
    private $cacheDir = 'example/check';

    public function __construct()
    {
        if (!config()->get('useExampleRoute')) {
            response()->json('Showing examples is disabled in the settings');
        }
        Loader::includeModule('iblock');
    }

    // Пример запроса получения данных из инфоблока
    // http://{domain}/{api}/example/check?iblock_id=1&active=Y&name=Шар&color=Белый&fields=preview_text,detail_text,form,preview_picture&sort=id:asc,name:asc&limit=5&page=1
    public function read()
    {
        // Переводим все ключи в верхний регистр
        $arParams = array_change_key_case(request()->get(), CASE_UPPER);
        // Дополнительные заголовки ответа - здесь используются только для демонстрации такой возможности
        $headers = [
            'Header-One' => 'Value of header one',
            'Header-Two' => 'Value of header two',
            'Header-Three' => 'Value of header three'
        ];
        // Поля по умолчанию для выборки
        $arSelect = [
            'ID',
            'NAME'
        ];
        // Дополнительные поля из запроса
        if ($arParams['FIELDS']) {
            $arParams['FIELDS'] = strtoupper($arParams['FIELDS']);
            $arParams['FIELDS'] = explode(',', $arParams['FIELDS']);
            foreach ($arParams['FIELDS'] as $field) {
                if ($field === 'COLOR' || $field === 'FORM') {
                    $field = 'PROPERTY_' . $field;
                }
                if (!in_array($field, $arSelect)) {
                    $arSelect[] = $field;
                }
            }
        }
        // Фильтр по умолчанию для выборки
        $arFilter = [
            'ACTIVE' => 'Y'
        ];
        // Дополнительные параметры фильтра из запроса
        if ($arParams['IBLOCK_ID']) $arFilter['IBLOCK_ID'] = &$arParams['IBLOCK_ID'];
        if ($arParams['ID']) $arFilter['ID'] = &$arParams['ID'];
        if ($arParams['ACTIVE']) $arFilter['ACTIVE'] = &$arParams['ACTIVE'];
        if ($arParams['NAME']) $arFilter['@NAME'] = &$arParams['NAME'];
        if ($arParams['COLOR']) $arFilter['PROPERTY_COLOR'] = &$arParams['COLOR'];
        // Параметры по умолчанию для постраничной навигации и ограничения количества выводимых элементов
        $arNavStartParams = [
            'nPageSize' => 10,
            'iNumPage' => 1
        ];
        // Дополнительные параметры для постраничной навигации из запроса
        // Ограничиваем максимальное количество записей при выборке
        if ($arParams['LIMIT']) $arNavStartParams['nPageSize'] = $arParams['LIMIT'] < 150 ? $arParams['LIMIT'] : 150;
        if ($arParams['PAGE']) $arNavStartParams['iNumPage'] = &$arParams['PAGE'];
        // Сортировка по умолчанию для выборки
        $arOrder = [
            'ID' => 'DESC'
        ];
        // Дополнительная сортировка для выборки
        if ($arParams['SORT']) {
            $arParams['SORT'] = strtoupper($arParams['SORT']);
            $arParams['SORT'] = explode(',', $arParams['SORT']);
            foreach ($arParams['SORT'] as $string) {
                $sort = explode(':', $string);
                $arOrder[$sort[0]] = $sort[1];
            }
        }
        // Уникальный ключ для кэша
        $cacheId = __CLASS__ . ':' . __FUNCTION__;
        if (is_array($arOrder)) {
            foreach ($arOrder as $field => $value) {
                $cacheId .= $field . ':' . $value . ':';
            }
        }
        if (is_array($arFilter)) {
            ksort($arFilter);
            foreach ($arFilter as $field => $value) {
                $cacheId .= $field . ':' . $value . ':';
            }
        }
        if (is_array($arNavStartParams)) {
            ksort($arNavStartParams);
            foreach ($arNavStartParams as $field => $value) {
                $cacheId .= $field . ':' . $value . ':';
            }
        }
        if (is_array($arSelect)) {
            sort($arSelect);
            $cacheId .= implode(':', $arSelect);
        }
        // Отдадим данные из кэша если они в нем имеются
        // Иначе, получим данные из базы и запишем в кэш
        // Срок кэша - 7 дней
        // Место хранения /bitrix/cache/example/check
        if (!$response = cache()->get($cacheId, $this->cacheTtl, $this->cacheDir)) {
            $response = [
                'page' => $arParams['PAGE'] ? (int)$arParams['PAGE'] : 1,
                'total' => 0,
                'items' => []
            ];
            // Выборка данных
            if ($items = CIBlockElement::getList($arOrder, $arFilter, false, $arNavStartParams, $arSelect)) {
                while ($item = $items->fetch()) {
                    $ar['ID'] = (int)$item['ID'];
                    $ar['NAME'] = $item['NAME'];
                    if ($item['PREVIEW_TEXT']) $ar['PREVIEW_TEXT'] = $item['PREVIEW_TEXT'];
                    if ($item['DETAIL_TEXT']) $ar['DETAIL_TEXT'] = $item['DETAIL_TEXT'];
                    if ($item['FORM']) $ar['PREVIEW_TEXT'] = $item['PROPERTY_FORM_VALUE'];
                    if ($item['PREVIEW_PICTURE']) $ar['PREVIEW_PICTURE'] = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . CFile::GetPath($item['PREVIEW_PICTURE']);
                    $response['total']++;
                    $response['items'][] = $ar;
                }
            }
            // Сохраняем данные в кэш чтобы при следующем запросе именно с такими входными параметрами уже не делать запросы в базу
            if ($response['items']) {
                cache()->set($response);
            }
        }
        // Запишем информацию в Журнал: Запрос/Ответ
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);

        // Возвращаем результат клиенту
        response()->json($response, 200, JSON_UNESCAPED_UNICODE, $headers);
    }

    // Пример запроса добавления элементов в инфоблок
    // http://{domain}/{api}/example/check
    // Элементы передаются в теле запроса
    /* Входящий json
    {
        "iblock_id": 1,
        "items": [
                {
                    "name":"test",
                    "color":"blue",
                    "preview_text":"Описание анонса"
                },
                {
                    "name":"test2",
                    "color":"blue",
                    "detail_text":"Детальное описание"
                },
                {
                    "name":"test3",
                    "color":"blue",
                    "preview_picture_url":"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/1200px-Apple_logo_black.svg.png"
                }
            ]
    }
    */
    public function create()
    {
        $response = [];
        // Если в запросе используется авторизация по токену и не используется whitelist токенов
        // Тогда дальше в работе методов можно использовать ID пользователя (владельца токена)
        $userId = user()->get('id');
        foreach (request()->get('items') as $item) {
            $element = new CIBlockElement;
            $arFields = [
                'IBLOCK_ID' => request()->get('iblock_id'),
                'NAME' => $item['name'],
                'MODIFIED_BY' => $userId
            ];
            if ($item['preview_text']) $arFields['PREVIEW_TEXT'] = $item['preview_text'];
            if ($item['detail_text']) $arFields['DETAIL_TEXT'] = $item['detail_text'];
            if ($item['color']) $arFields['PROPERTY_VALUES']['COLOR'] = $item['color'];
            if ($item['form']) $arFields['PROPERTY_VALUES']['FORM'] = $item['form'];
            if ($item['preview_picture_url']) $arFields['PREVIEW_PICTURE'] = CFile::MakeFileArray($item['preview_picture_url']);
            if ($id = $element->Add($arFields, false, false, true)) {
                $response['successful'][$id] = [
                    'ID' => $id,
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                    'NAME' => $arFields['NAME'],
                ];
                if ($item['preview_text']) $response['successful'][$id]['PREVIEW_TEXT'] = $item['preview_text'];
                if ($item['detail_text']) $response['successful'][$id]['DETAIL_TEXT'] = $item['detail_text'];
                if ($item['color']) $response['successful'][$id]['PROPERTY_COLOR'] = $item['color'];
                if ($item['form']) $response['successful'][$id]['PROPERTY_FORM'] = $item['form'];
            } else {
                $response['error'] = $element->LAST_ERROR;
            }
        }
        // Очистим кэш для текущего роута чтобы актуализировать данные
        if ($response['successful']) {
            cache()->clear($this->cacheDir);
        }
        response()->json($response, 200, JSON_UNESCAPED_UNICODE);
    }

    // Пример запроса обновления данных элемента
    // http://{domain}/{api}/example/check
    /* Входящий json
    {
        "element_id":35,
        "preview_text":"Описание анонса"
    }
    */
    public function update()
    {
        $response = [];
        $arFields = [];
        if (request()->get('name')) $arFields['NAME'] = request()->get('name');
        if (request()->get('preview_text')) $arFields['PREVIEW_TEXT'] = request()->get('preview_text');
        if (request()->get('detail_text')) $arFields['DETAIL_TEXT'] = request()->get('detail_text');
        if ($arFields) {
            $element = new CIBlockElement;
            $arFields['MODIFIED_BY'] = user()->get('id');
            if ($element->Update(request()->get('element_id'), $arFields, false, false, true, false)) {
                if (user()->get('id')) {
                    $response['successful']['MODIFIED_BY'] = user()->get('id');
                }
                if (request()->get('name')) $response['successful']['NAME'] = request()->get('name');
                if (request()->get('preview_text')) $response['successful']['PREVIEW_TEXT'] = request()->get('preview_text');
                if (request()->get('detail_text')) $response['successful']['DETAIL_TEXT'] = request()->get('detail_text');
            } else {
                $response['error'] = $element->LAST_ERROR;
            }
            // Очистим кэш для текущего роута чтобы актуализировать данные
            if ($response['successful']) {
                cache()->clear($this->cacheDir);
            }
        }
        response()->json($response, 200, JSON_UNESCAPED_UNICODE);
    }

    // Пример запроса удаления элемента
    // http://{domain}/{api}/example/check
    /* Входящий json
    {
        "element_ids": [1,2,4]
    }
    */
    public function delete()
    {
        if (is_array(request()->get('element_ids'))) {
            foreach (request()->get('element_ids') as $id) {
                CIBlockElement::Delete($id);
            }
        }
        cache()->clear($this->cacheDir);
        response()->json([
            'message' => 'Deleted',
            'ids' => request()->get('element_ids')
        ], 200, JSON_UNESCAPED_UNICODE);
    }
}