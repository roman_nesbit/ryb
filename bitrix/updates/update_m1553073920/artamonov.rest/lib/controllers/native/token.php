<?php


namespace Artamonov\Rest\Controllers\Native;


use Artamonov\Rest\Foundation\Settings;
use CUser;

class Token
{
    private $user;

    public function __construct()
    {
        if (!$this->checkUser()) {
            $response = ['error' => 'User not found'];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->notFound();
        }
    }

    public function get()
    {
        $response = ['token' => $this->getUser()[Settings::getInstance()->getCodeTokenFiled()]];
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        response()->json($response);
    }

    public function create()
    {
        $user = new CUser();
        $token = \Artamonov\Rest\Foundation\User::getInstance()->generateToken($this->getUser()['ID'], $this->getUser()['LOGIN']);
        $user->update($this->getUser()['ID'], [Settings::getInstance()->getCodeTokenFiled() => $token]);
        $response = ['token' => $token];
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        response()->created($response);
    }

    public function delete()
    {
        $user = new CUser();
        $user->update($this->getUser()['ID'], [Settings::getInstance()->getCodeTokenFiled() => '']);
        $response['successful'] = true;
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        response()->ok();
    }

    private function checkUser()
    {
        $passwordCorrect = false;
        if ($this->user = CUser::GetByLogin(request()->get('login'))->fetch()) {
            $length = strlen($this->user['PASSWORD']);
            if ($length > 32) {
                $salt = substr($this->user['PASSWORD'], 0, $length - 32);
                $db_password = substr($this->user['PASSWORD'], -32);
            } else {
                $salt = '';
                $db_password = $this->user['PASSWORD'];
            }
            $user_password = md5($salt . request()->get('password'));
            if ($user_password === $db_password) $passwordCorrect = true;
        }
        return $passwordCorrect;
    }

    private function getUser()
    {
        return $this->user;
    }
}
