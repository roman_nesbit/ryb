<?php


namespace Artamonov\Rest\Controllers\Native;


use CUser;
use CFile;

class User
{
    public function get()
    {
        $response = CUser::GetByID($this->id())->fetch();
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        response()->json($response);
    }

    public function create()
    {
        $user = new CUser;
        $arFields = array_change_key_case(request()->get(), CASE_UPPER);
        $arFields['CONFIRM_PASSWORD'] = $arFields['PASSWORD'];
        $id = $user->Add($arFields);
        if ($id) {
            $response = ['user' => CUser::GetByID($id)->fetch()];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->created($response);
        } else {
            $response = ['error' => $user->LAST_ERROR];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->json($response);
        }
    }

    public function update()
    {
        $id = $this->id();
        $user = new CUser;
        $arFields = array_change_key_case(request()->get(), CASE_UPPER);
        $arFields['CONFIRM_PASSWORD'] = $arFields['PASSWORD'];
        $user->Update($id, $arFields);
        if ($user->LAST_ERROR) {
            $response = ['error' => $user->LAST_ERROR];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->json($response);
        } else {
            $response = ['user' => CUser::GetByID($id)->fetch()];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->json($response);
        }
    }

    public function delete()
    {
        $id = $this->id();
        $response = [];
        global $DB;
        $strSql = 'SELECT F.ID FROM	b_user U, b_file F WHERE U.ID=' . $id . ' and (F.ID=U.PERSONAL_PHOTO or F.ID=U.WORK_LOGO)';
        $z = $DB->Query($strSql, false, 'FILE: ' . __FILE__ . ' LINE:' . __LINE__);
        while ($zr = $z->Fetch()) {
            CFile::Delete($zr['ID']);
        }
        if (!$DB->Query('DELETE FROM b_user_group WHERE USER_ID=' . $id)) {
            $response['error'][] = 'Failed attempt to delete from table: b_user_group';
        }
        if (!$DB->Query('DELETE FROM b_user_digest WHERE USER_ID=' . $id)) {
            $response['error'][] = 'Failed attempt to delete from table: b_user_digest';
        }
        if (!$DB->Query('DELETE FROM b_app_password WHERE USER_ID=' . $id)) {
            $response['error'][] = 'Failed attempt to delete from table: b_app_password';
        }
        if (!$DB->Query('DELETE FROM b_user WHERE ID=' . $id . ' AND ID<>1')) {
            $response['error'][] = 'Failed attempt to delete from table: b_app_password';
        }
        if ($response['error']) {
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->json($response);
        } else {
            $response['successful'] = true;
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->ok();
        }
    }

    private function id()
    {
        $id = null;
        if (request()->get('id')) {
            $id = (int)CUser::GetByID(request()->get('id'))->fetch()['ID'];
        } else if (request()->get('login')) {
            $id = (int)CUser::GetByLogin(request()->get('login'))->fetch()['ID'];
        } else if (request()->get('token')) {
            $id = (int)\Artamonov\Rest\Foundation\User::getInstance()->getIdByToken(request()->get('token'));
        }
        if (!$id) {
            $response = ['error' => 'User id not found'];
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            response()->json($response);
        } else {
            return $id;
        }
    }
}
