<?php

namespace Artamonov\Rest\Foundation;


class Request
{
    private static $_instance;
    private $parameters;
    private $headers;
    private $ip;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param string $code
     * @return array | string
     */
    public function get($code = '')
    {
        if (!$this->parameters) {
            $this->parameters = $_GET;
            if ($_SERVER['REQUEST_METHOD'] === helper()->POST() || $_SERVER['REQUEST_METHOD'] === helper()->PUT() || $_SERVER['REQUEST_METHOD'] === helper()->DELETE()) {
                if (strpos($_SERVER['CONTENT_TYPE'], helper()->contentTypeJson()) !== false) {
                    $ar = json_decode(file_get_contents('php://input'), true);
                    foreach ($ar as $k => $v) {
                        $this->parameters[$k] = $v;
                    }
                } else {
                    $this->parameters = array_merge($this->parameters, $_POST);
                }
            }
        }
        return $code ? $this->parameters[$code] : $this->parameters;
    }

    /**
     * @param string $code
     * @return array | string
     */
    public function header($code = '')
    {
        if (!$this->headers) {
            if (function_exists('getallheaders')) {
                $this->headers = getallheaders();
            } else if (is_array($_SERVER)) {
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $this->headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
            }
        }
        return $code ? $this->headers[$code] : $this->headers;
    }

    public function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function ip()
    {
        if (!$this->ip) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $this->ip = $_SERVER['HTTP_CLIENT_IP'];
            } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $this->ip = $_SERVER['REMOTE_ADDR'];
            }
        }
        return $this->ip;
    }

    public function path()
    {
        return route()->path();
    }

    public function map()
    {
        return route()->get();
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
