<?php

namespace Artamonov\Rest\Foundation;


class Helper
{
    private static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function printArray($array)
    {
        echo '<pre>' . print_r($array, true) . '</pre>';
    }

    public function sortByMultipleKey($array, $args)
    {
        usort($array, function ($a, $b) use ($args) {
            $res = 0;
            $a = (object)$a;
            $b = (object)$b;
            foreach ($args as $k => $v) {
                if ($a->$k === $b->$k) continue;

                $res = ($a->$k < $b->$k) ? -1 : 1;
                if ($v === 'desc') $res = -$res;
                break;
            }
            return $res;
        });
        return $array;
    }

    public function note($message)
    {
        echo BeginNote() . $message . EndNote();
    }

    public function isAdminSection()
    {
        return defined('ADMIN_SECTION');
    }

    public function ROOT()
    {
        return 'ROOT';
    }

    public function GET()
    {
        return 'GET';
    }

    public function POST()
    {
        return 'POST';
    }

    public function PUT()
    {
        return 'PUT';
    }

    public function DELETE()
    {
        return 'DELETE';
    }

    public function contentTypeJson()
    {
        return 'application/json';
    }

    public function cgi()
    {
        return strpos(php_sapi_name(), 'cgi') !== false;
    }

    public function isArray($value)
    {
        return is_array($value);
    }

    public function isObject($value)
    {
        return is_object($value);
    }

    public function isString($value)
    {
        return is_string($value);
    }

    public function isInteger($value)
    {
        return filter_var($value, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 999999999999]]);
    }

    public function isFloat($value)
    {
        return filter_var($value, FILTER_VALIDATE_FLOAT);
    }

    public function isEmail($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function isIp($value)
    {
        return filter_var($value, FILTER_VALIDATE_IP);
    }

    public function isDomain($value)
    {
        return filter_var($value, FILTER_VALIDATE_DOMAIN);
    }

    public function isUrl($value)
    {
        return filter_var($value, FILTER_VALIDATE_URL);
    }

    public function adminGroupId()
    {
        return 1;
    }

    public function ratingVoteGroupId()
    {
        return 3;
    }

    public function ratingVoteAuthorityGroupId()
    {
        return 4;
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}