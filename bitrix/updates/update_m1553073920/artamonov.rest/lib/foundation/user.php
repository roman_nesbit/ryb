<?php

namespace Artamonov\Rest\Foundation;


use CUser;

class User
{
    private static $_instance;
    private $data;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get($code = '')
    {
        return $code ? $this->data[$code] : $this->data;
    }

    public function set($data)
    {
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $this->data[$k] = $v;
            }
        }
    }

    public function getIdByToken($token = '')
    {
        $token = $token ? $token : $this->get('token');
        $sql = 'SELECT VALUE_ID FROM b_uts_user WHERE ' . settings()->getCodeTokenFiled() . '="' . $token . '" LIMIT 1';
        if ($id = (int)db()->query($sql)->fetch()['VALUE_ID']) {
            $this->data = [
                'id' => $id,
                'token' => $token
            ];
        }
        return $this->data['id'];
    }

    public function generateToken($userId, $userLogin)
    {
        $token = md5($userId . '-' . $userLogin . '=' . date('Y-m-d H:i:s'));
        $token = str_split($token, 8);
        $token = implode('-', $token);
        return $token;
    }

    public function getGroups()
    {
        if (!$this->data['groups']) {
            if (!$this->data['id']) {
                $this->getIdByToken();
            }
            $this->data['groups'] = CUser::GetUserGroup($this->data['id']);
        }
        return $this->data['groups'];
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
