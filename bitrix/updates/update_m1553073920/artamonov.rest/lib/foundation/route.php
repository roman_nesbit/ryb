<?php

namespace Artamonov\Rest\Foundation;


class Route
{
    private static $_instance;
    private static $route;
    private static $path;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$path = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/');
            if (config()->get('pathRestApi') !== helper()->ROOT()) {
                self::$path = str_replace(config()->get('pathRestApi') . '/', '', self::$path);
            }
            $dir = __DIR__ . '/../../routes';
            $files = array_diff(scandir($dir), ['..', '.']);
            foreach ($files as $file) {
                if (is_array($ar = require $dir . '/' . $file)) {
                    foreach ($ar as $type => $routes) {
                        if ($_SERVER['REQUEST_METHOD'] === $type) {
                            foreach ($routes as $route => $config) {
                                $route = trim($route, '/');
                                if ($route === self::$path) {
                                    self::$route = $config;
                                }
                            }
                        }
                    }
                }
            }
        }
        return self::$_instance;
    }

    public function get()
    {
        return self::$route;
    }

    public function path()
    {
        return self::$path;
    }

    public function isActive()
    {
        return self::$route['active'] === false ? false : true;
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}