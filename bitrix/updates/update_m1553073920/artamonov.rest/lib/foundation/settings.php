<?php

namespace Artamonov\Rest\Foundation;


class Settings
{
    private static $_instance;
    private static $settings;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$settings = require __DIR__ . '/../../settings.php';
        }
        return self::$_instance;
    }

    public function get($code = '')
    {
        return $code ? self::$settings[$code] : self::$settings;
    }

    public function getCodeTokenFiled()
    {
        return config()->get('tokenFieldCode') ? config()->get('tokenFieldCode') : self::$settings['config']['tokenField'];
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}