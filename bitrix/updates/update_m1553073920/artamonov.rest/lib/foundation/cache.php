<?php

namespace Artamonov\Rest\Foundation;


class Cache
{
    private static $_instance;
    private static $cache;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            self::$cache = \Bitrix\Main\Data\Cache::createInstance();
        }
        return self::$_instance;
    }

    public function get($id, $ttl = 86400, $dir = false)
    {
        return self::$cache->initCache($ttl, $id, $dir) ? self::$cache->getVars() : false;
    }

    public function set($data)
    {
        if (self::$cache->startDataCache()) {
            self::$cache->endDataCache($data);
        }
    }

    public function clear($dir)
    {
        BXClearCache(true, $dir);
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}