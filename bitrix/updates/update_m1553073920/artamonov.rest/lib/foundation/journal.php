<?php

namespace Artamonov\Rest\Foundation;


class Journal
{
    private static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function add($type, $data = [])
    {
        switch ($type) {
            case 'request-response':
                $this->addRequestResponse($data['request'], $data['response']);
                break;
            case 'request-limit':
                $this->addRequestLimit();
                break;
        }
    }

    public function get($type, $filter = [], $sort = [])
    {
        switch ($type) {
            case 'request-response':
                return $this->getRequestResponse($filter, $sort);
                break;
            case 'request-limit':
                return $this->getRequestLimit($filter, $sort);
                break;
            default:
                return false;
        }
    }

    public function delete($type, $ids)
    {
        switch ($type) {
            case 'request-response':
                $this->deleteRequestResponse($ids);
                break;
        }
        switch ($type) {
            case 'request-limit':
                $this->deleteRequestLimit($ids);
                break;
        }
    }

    // Request-Response

    private function addRequestResponse($request = [], $response = [])
    {
        if (!config()->get('useJournal')) return false;
        if (!$request) {
            $request = [
                'path' => request()->path(),
                'params' => request()->get(),
                'map' => request()->map(),
            ];
        }
        $data = [
            'METHOD' => request()->method(),
            'IP' => request()->ip(),
            'CLIENT_ID' => user()->get('token'),
            'DATETIME' => new \Bitrix\Main\Type\DateTime(),
            'REQUEST' => json_encode($request, JSON_UNESCAPED_UNICODE)
        ];
        if ($response) $data['RESPONSE'] = json_encode($response, JSON_UNESCAPED_UNICODE);
        db()->add(settings()->get('config')['table']['request-response'], $data);
    }

    private function getRequestResponse($filter = [], $sort = [])
    {
        $sql = 'SELECT * FROM ' . settings()->get('config')['table']['request-response'];
        if (is_array($filter)) foreach ($filter as $k => $v) if (empty($v)) unset($filter[$k]);
        if ($filter) {
            $sql .= ' WHERE';
            if (!$filter['DATETIME_FROM']) {
                $filter['DATETIME_FROM'] = '2000-12-31';
            }
            if (!$filter['DATETIME_TO']) {
                $filter['DATETIME_TO'] = '2100-12-31';
            }
            $sql .= ' (DATETIME BETWEEN "' . date_format(date_create($filter['DATETIME_FROM']), 'Y-m-d') . '" AND "' . date_format(date_create($filter['DATETIME_TO']), 'Y-m-d') . '")';
            unset($filter['DATETIME_FROM'], $filter['DATETIME_TO']);
            foreach ($filter as $field => $value) {
                $sql .= ' AND ' . $field . '="' . $value . '"';
            }
        }
        if ($sort && $sort['field']) $sql .= ' ORDER BY ' . $sort['field'] . ' ' . $sort['order'];
        return db()->query($sql);
    }

    private function deleteRequestResponse($ids)
    {
        if ($ids === '*') {
            db()->truncateTable(settings()->get('config')['table']['request-response']);
        } elseif (is_array($ids)) {
            db()->queryExecute('DELETE FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE ID IN(' . implode(',', $ids) . ')');
        }
    }

    // Request-Limit

    private function addRequestLimit()
    {
        $data = [
            'CLIENT_ID' => user()->get('token'),
            'DATETIME' => new \Bitrix\Main\Type\DateTime()
        ];
        db()->add(settings()->get('config')['table']['request-limit'], $data);
    }

    private function getRequestLimit($filter = [], $sort = [])
    {
        $sql = 'SELECT * FROM ' . settings()->get('config')['table']['request-limit'];
        if (is_array($filter)) foreach ($filter as $k => $v) if (empty($v)) unset($filter[$k]);
        if ($filter) {
            $sql .= ' WHERE';
            if (!$filter['DATETIME_FROM']) {
                $filter['DATETIME_FROM'] = '2000-12-31';
            }
            if (!$filter['DATETIME_TO']) {
                $filter['DATETIME_TO'] = '2100-12-31';
            }
            $sql .= ' (DATETIME BETWEEN "' . date_format(date_create($filter['DATETIME_FROM']), 'Y-m-d') . '" AND "' . date_format(date_create($filter['DATETIME_TO']), 'Y-m-d') . '")';
            unset($filter['DATETIME_FROM'], $filter['DATETIME_TO']);
            foreach ($filter as $field => $value) {
                $sql .= ' AND ' . $field . '="' . $value . '"';
            }
        }
        if ($sort && $sort['field']) $sql .= ' ORDER BY ' . $sort['field'] . ' ' . $sort['order'];
        return db()->query($sql);
    }

    private function deleteRequestLimit($ids)
    {
        if ($ids === '*') {
            db()->truncateTable(settings()->get('config')['table']['request-limit']);
        } elseif (is_array($ids)) {
            db()->queryExecute('DELETE FROM ' . settings()->get('config')['table']['request-limit'] . ' WHERE ID IN(' . implode(',', $ids) . ')');
        }
    }

    public function getRequestLimitNumber($token, $dateForm, $dateTo)
    {
        return db()->query('SELECT COUNT(CLIENT_ID) as COUNT FROM ' . settings()->get('config')['table']['request-limit'] . ' WHERE CLIENT_ID="' . $token . '" AND (DATETIME BETWEEN "' . $dateForm . '" AND "' . $dateTo . '")')->fetch()['COUNT'];
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}