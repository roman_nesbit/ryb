<?php

namespace Artamonov\Rest\Foundation;

use Bitrix\Main\Localization\Loc;

class Page
{
    private static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance) && helper()->isAdminSection()) {
            self::$_instance = new self();
            Loc::loadLanguageFile(__FILE__);
        }
        return self::$_instance;
    }

    public function checkAccess($code)
    {
        if (!$code === 'accessConfig') {
            if (!$GLOBALS['USER']->IsAdmin()) $GLOBALS['APPLICATION']->AuthForm(loc('ArtamonovRestAccessDenied'));
        } else {
            if (!security()->checkAccessByGroups(explode('|', config()->get($code)), $GLOBALS['USER']->GetUserGroupArray())) $GLOBALS['APPLICATION']->AuthForm(loc('ArtamonovRestAccessDenied'));
        }
    }

    public function loadLanguage($file)
    {
        Loc::loadLanguageFile($file);
    }

    public function setTitle($title)
    {
        $GLOBALS['APPLICATION']->SetTitle($title);
    }

    public function addCss($file)
    {
        $GLOBALS['APPLICATION']->SetAdditionalCSS($file);
    }

    public function addHeadString($string)
    {
        $GLOBALS['APPLICATION']->AddHeadString($string, true);
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}