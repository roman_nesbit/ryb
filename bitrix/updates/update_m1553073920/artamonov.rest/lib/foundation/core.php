<?php

namespace Artamonov\Rest\Foundation;


class Core
{
    private static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            define('SM_SAFE_MODE', true);
            define('PERFMON_STOP', true);
            define('PUBLIC_AJAX_MODE', true);
            header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers: Content-Type, Authorization-Token');
            header('Access-Control-Allow-Origin: *');
        }
        return self::$_instance;
    }

    public function run()
    {
        if (helper()->isAdminSection() || !config()->get('useRestApi') || !$this->isPath()) return false;
        if (!security()->hasController()) response()->badRequest('Request cannot be processed');
        if (!security()->isValidContentType()) response()->badRequest('Invalid content type');
        if (!security()->isActive()) response()->requestedHostUnavailable();
        if (!security()->isValidParameters()) response()->badRequest(security()->getErrors());
        if (!security()->isAuthorized()) response()->unauthorized();
        controller()->run();
        die;
    }

    private function isPath()
    {
        return config()->get('pathRestApi') && (config()->get('pathRestApi') === helper()->ROOT() || config()->get('pathRestApi') === explode('/', trim($_SERVER['REQUEST_URI'], '/'))[0]);
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
