<?php

namespace Artamonov\Rest\Foundation;


class Controller
{
    private static $_instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function run()
    {
        $controller = explode('@', route()->get()['controller']);
        $action = $controller[1];
        $controller = $controller[0];
        $controller = new $controller();
        $controller->$action();
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}