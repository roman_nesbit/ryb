<?php

namespace Artamonov\Rest\Foundation;


class Security
{
    private static $_instance;
    private $errors;
    private $token;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get()
    {
        return route()->get()['security'];
    }

    public function isActive()
    {
        return route()->isActive();
    }

    public function hasController()
    {
        return route()->get()['controller'] ? true : false;
    }

    public function isValidContentType()
    {
        return (route()->get()['contentType'] && strpos($_SERVER['CONTENT_TYPE'], route()->get()['contentType']) !== 0) ? false : true;
    }

    public function isValidParameters()
    {
        if (!request()->map()['parameters']) return true;
        if (!request()->get()) {
            $this->errors = 'There are no parameters to check';
            return false;
        }
        // Validation of request parameters
        if (request()->method() === helper()->GET()) {
            // Simple request
            foreach (request()->map()['parameters'] as $code => $ar) {
                $this->checkParameter(request()->get(), $code, $ar['required'], $ar['type']);
            }
        } else {
            // The check differs in that there is a request body
            foreach (request()->map()['parameters'] as $codeOne => $arOne) {
                // Checking first level
                $this->checkParameter(request()->get(), $codeOne, $arOne['required'], $arOne['type']);
                // Checking second level
                if (is_array($arOne['parameters'])) {
                    foreach ($arOne['parameters'] as $codeTwo => $arTwo) {
                        // If: param => value
                        if (is_string($codeTwo)) {
                            $this->checkParameter(request()->get($codeOne), $codeTwo, $arTwo['required'], $arTwo['type'], $codeOne);
                        } // If: params => array
                        elseif (is_integer($codeTwo) && is_array($arOne['parameters'][$codeTwo])) {
                            // Checking each element of the array
                            for ($i = 0, $c = count(request()->get($codeOne)); $i < $c; $i++) {
                                $index = $i + 1;
                                // Getting rule from map for one element
                                foreach ($arOne['parameters'][$codeTwo] as $parameterCodeTwo => $parameterArTwo) {
                                    $this->checkParameter(request()->get($codeOne)[$i], $parameterCodeTwo, $parameterArTwo['required'], $parameterArTwo['type'], $codeOne, $index);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->errors ? false : true;
    }

    public function isAuthorized()
    {
        return $this->get()['auth'] ? $this->checkAccess() : true;
    }

    private function checkAccess()
    {
        if (!config()->get('useToken')) {
            return true;
        }
        if (!$token = request()->header('Authorization-Token')) {
            return false;
        }
        $this->token = $token;
        if (config()->get('tokenKey')) {
            $token = explode(':', $token);
            $keyword = $token[0];
            $this->token = $token[1];
            if (config()->get('tokenKey') !== $keyword) {
                return false;
            }
        }
        if (!$this->checkToken()) {
            return false;
        } elseif (!$this->checkRequestLimit()) {
            return false;
        } elseif (!$this->checkWhiteListGroups()) {
            return false;
        }
        return true;
    }

    private function checkToken()
    {
        $access = false;
        if ($this->get()['tokens']['whitelist']) {
            if (in_array($this->token, $this->get()['tokens']['whitelist'])) {
                user()->set(['token' => $this->token]);
                $access = true;
            } else {
                $access = false;
            }
        } elseif (user()->getIdByToken($this->token)) {
            $access = true;
        }
        return $access;
    }

    private function checkRequestLimit()
    {
        if (!config()->get('useRequestLimit')) {
            return true;
        }
        $userGroups = user()->getGroups();
        $requestLimitGroup = json_decode(config()->get('requestLimit'), true);
        if (!$intersect = array_intersect($userGroups, array_keys($requestLimitGroup))) {
            return false;
        }
        $tmp = [];
        foreach ($intersect as $id) {
            $tmp[$id] = $requestLimitGroup[$id];
        }
        $requestLimitGroup = helper()->sortByMultipleKey($tmp, ['number' => 'desc', 'period' => 'desc'])[0];
        $dateFrom = date('Y-m-d H:i:s', strtotime('now-' . $requestLimitGroup['period'] . 'seconds'));
        $dateTo = date('Y-m-d H:i:s');
        $number = journal()->getRequestLimitNumber(user()->get('token'), $dateFrom, $dateTo);
        if ($number >= $requestLimitGroup['number']) {
            response()->tooManyRequests();
        }
        journal()->add('request-limit');
        return true;
    }

    private function checkWhiteListGroups()
    {
        if (!$this->get()['groups']['whitelist']) {
            return true;
        }
        return array_intersect(user()->getGroups(), $this->get()['groups']['whitelist']) ? true : false;
    }

    public function checkAccessByGroups($groups, $userGroups)
    {
        $groups[] = helper()->adminGroupId();
        return array_intersect($groups, $userGroups);
    }

    private function checkParameter($params, $code, $required, $type, $parent = false, $index = false)
    {
        if ($required && !array_key_exists($code, $params)) {
            if ($parent) {
                if ($index !== false) {
                    $this->errors['badParameters'][$parent][$index][$code]['required'] = $required;
                } else {
                    $this->errors['badParameters'][$parent][$code]['required'] = $required;
                }
            } else {
                $this->errors['badParameters'][$code]['required'] = $required;
            }
        } else if (array_key_exists($code, $params)) {
            $isType = 'is' . $type;
            $value = $params[$code];
            if (!helper()->$isType($value)) {
                if ($parent) {
                    if ($index !== false) {
                        $this->errors['badParameters'][$parent][$index][$code]['invalidValueType'] = $value;
                        $this->errors['badParameters'][$parent][$index][$code]['valueTypeMustBe'] = $type;
                    } else {
                        $this->errors['badParameters'][$parent][$code]['invalidValueType'] = $value;
                        $this->errors['badParameters'][$parent][$code]['valueTypeMustBe'] = $type;
                    }
                } else {
                    $this->errors['badParameters'][$code]['invalidValueType'] = $value;
                    $this->errors['badParameters'][$code]['valueTypeMustBe'] = $type;
                }
            }
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}
