<?php

namespace Artamonov\Rest\Foundation;


use Bitrix\Main\Config\Option;

class Config
{
    private static $_instance;
    private $parameters;
    private $data;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get($code)
    {
        if (!is_array($this->parameters)) {
            $result = db()->query('SELECT NAME, VALUE FROM b_option WHERE MODULE_ID="' . settings()->get('module')['id'] . '"');
            while ($parameter = $result->fetch()) {
                $this->parameters[$parameter['NAME']] = $parameter['VALUE'];
            }
        }
        return $this->parameters[$code];
    }

    public function save()
    {
        $this->prepare();
        foreach ($this->data as $code => $value) {
            Option::set(settings()->get('module')['id'], $code, $value, false);
            $this->parameters[$code] = $value;
        }
    }

    public function restore()
    {
        $this->prepare();
        foreach ($this->data as $code => $value) {
            Option::delete(settings()->get('module')['id'], ['name' => $code]);
            unset($this->parameters[$code]);
        }
    }

    private function prepare()
    {
        switch ($_POST['form']) {

            case 'rest-api-config':
                // Checkboxes
                if (!isset($_POST[settings()->get('config')['prefix'] . 'useRestApi'])) {
                    $_POST[settings()->get('config')['prefix'] . 'useRestApi'] = false;
                }
                if (!empty($_POST[settings()->get('config')['prefix'] . 'pathRestApi'])) {
                    $_POST[settings()->get('config')['prefix'] . 'pathRestApi'] = str_replace(' ', '', trim($_POST[settings()->get('config')['prefix'] . 'pathRestApi'], '/'));
                }
                if (!isset($_POST[settings()->get('config')['prefix'] . 'useJournal'])) {
                    $_POST[settings()->get('config')['prefix'] . 'useJournal'] = false;
                }
                if (!isset($_POST[settings()->get('config')['prefix'] . 'showExamples'])) {
                    $_POST[settings()->get('config')['prefix'] . 'showExamples'] = false;
                }
                if (!isset($_POST[settings()->get('config')['prefix'] . 'useExampleRoute'])) {
                    $_POST[settings()->get('config')['prefix'] . 'useExampleRoute'] = false;
                }
                // Arrays
                if (isset($_POST[settings()->get('config')['prefix'] . 'accessDocumentation'])) {
                    $_POST[settings()->get('config')['prefix'] . 'accessDocumentation'] = implode('|', $_POST[settings()->get('config')['prefix'] . 'accessDocumentation']);
                } else {
                    $_POST[settings()->get('config')['prefix'] . 'accessDocumentation'] = false;
                }
                if (isset($_POST[settings()->get('config')['prefix'] . 'accessSecurity'])) {
                    $_POST[settings()->get('config')['prefix'] . 'accessSecurity'] = implode('|', $_POST[settings()->get('config')['prefix'] . 'accessSecurity']);
                } else {
                    $_POST[settings()->get('config')['prefix'] . 'accessSecurity'] = false;
                }
                if (isset($_POST[settings()->get('config')['prefix'] . 'accessJournal'])) {
                    $_POST[settings()->get('config')['prefix'] . 'accessJournal'] = implode('|', $_POST[settings()->get('config')['prefix'] . 'accessJournal']);
                } else {
                    $_POST[settings()->get('config')['prefix'] . 'accessJournal'] = false;
                }
                if (isset($_POST[settings()->get('config')['prefix'] . 'accessSupport'])) {
                    $_POST[settings()->get('config')['prefix'] . 'accessSupport'] = implode('|', $_POST[settings()->get('config')['prefix'] . 'accessSupport']);
                } else {
                    $_POST[settings()->get('config')['prefix'] . 'accessSupport'] = false;
                }
                break;

            case 'rest-api-security':
                // Checkboxes
                if (!isset($_POST[settings()->get('config')['prefix'] . 'useToken'])) {
                    $_POST[settings()->get('config')['prefix'] . 'useToken'] = false;
                }
                if (!isset($_POST[settings()->get('config')['prefix'] . 'useRequestLimit'])) {
                    $_POST[settings()->get('config')['prefix'] . 'useRequestLimit'] = false;
                }
                // Prepare request limit parameters
                $_POST['parameter:requestLimit'] = [];
                foreach ($_POST as $key => $value) {
                    if (strpos($key, 'data:requestLimit') !== false) {
                        $code = explode('-', strtolower(str_replace('data:requestLimit', '', $key)));
                        $id = $code[1];
                        $code = $code[0];
                        $_POST['parameter:requestLimit'][$id][$code] = $value;
                        unset($_POST[$key]);
                    }
                }
                $_POST['parameter:requestLimit'] = json_encode($_POST['parameter:requestLimit']);
                break;
        }

        foreach ($_POST as $key => $value) {
            if (stripos($key, settings()->get('config')['prefix']) !== false) {
                $code = str_replace(settings()->get('config')['prefix'], '', $key);
                $this->data[$code] = trim($value);
            }
        }
    }

    public function __call($name, $arguments)
    {
        response()->internalServerError('Method \'' . $name . '\' is not defined');
    }

    private function __clone()
    {
    }
}