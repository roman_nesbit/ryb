<?php

use Bitrix\Main\Localization\Loc;

return [
    'charset' => LANG_CHARSET,
    'module' => [
        'id' => Loc::getMessage('ArtamonovRestModuleId'),
        'name' => Loc::getMessage('ArtamonovRestModuleName'),
        'description' => Loc::getMessage('ArtamonovRestModuleDescription'),
        'connectionString' => 'if (Bitrix\Main\Loader::includeModule(\'' . Loc::getMessage('ArtamonovRestModuleId') . '\')) \Artamonov\Rest\Foundation\Core::getInstance()->run();',
        'version' => '2.0.0',
        'versionDate' => '2019-01-05 01:00:00'
    ],
    'author' => [
        'company' => Loc::getMessage('ArtamonovRestAuthorCompany'),
        'name' => Loc::getMessage('ArtamonovRestAuthorName'),
        'email' => 'artamonov.ceo@gmail.com',
        'website' => 'http://artamonov.biz/'
    ],
    'partner' => [
        'name' => Loc::getMessage('ArtamonovRestAuthorName'),
        'website' => 'http://artamonov.biz/'
    ],
    'config' => [
        'prefix' => 'parameter:',
        'tokenField' => 'UF_REST_API_TOKEN',
        'table' => [
            'request-response' => Loc::getMessage('ArtamonovRestTablePrefix') . 'request_response',
            'request-limit' => Loc::getMessage('ArtamonovRestTablePrefix') . 'request_limit'
        ]
    ],
    'path' => [
        'reviews' => 'http://marketplace.1c-bitrix.ru/solutions/' . Loc::getMessage('ArtamonovRestModuleId') . '/#tab-rating-link',
        'marketplace' => 'http://marketplace.1c-bitrix.ru/solutions/' . Loc::getMessage('ArtamonovRestModuleId') . '/'
    ]
];
