<?php

function db()
{
    return \Bitrix\Main\Application::getConnection();
}

function loc($code, $params = [])
{
    return is_array($params) ? \Bitrix\Main\Localization\Loc::getMessage($code, $params) : \Bitrix\Main\Localization\Loc::getMessage($code);
}

function settings()
{
    return \Artamonov\Rest\Foundation\Settings::getInstance();
}

function config()
{
    return \Artamonov\Rest\Foundation\Config::getInstance();
}

function helper()
{
    return \Artamonov\Rest\Foundation\Helper::getInstance();
}

function route()
{
    return \Artamonov\Rest\Foundation\Route::getInstance();
}

function controller()
{
    return \Artamonov\Rest\Foundation\Controller::getInstance();
}

function request()
{
    return \Artamonov\Rest\Foundation\Request::getInstance();
}

function response()
{
    return \Artamonov\Rest\Foundation\Response::getInstance();
}

function security()
{
    return \Artamonov\Rest\Foundation\Security::getInstance();
}

function cache()
{
    return \Artamonov\Rest\Foundation\Cache::getInstance();
}

function journal()
{
    return \Artamonov\Rest\Foundation\Journal::getInstance();
}

function user()
{
    return \Artamonov\Rest\Foundation\User::getInstance();
}

function page()
{
    return \Artamonov\Rest\Foundation\Page::getInstance();
}

?>