<?php

use Bitrix\Main\Application;

$settings = require __DIR__ . '/../../settings.php';

Application::getConnection()->dropTable($settings['config']['table']['request-response']);
Application::getConnection()->dropTable($settings['config']['table']['request-limit']);