<?php

use Bitrix\Main\IO\File;
use Bitrix\Main\Localization\Loc;

$APPLICATION->SetTitle(Loc::getMessage('ArtamonovRestInstallPageTitle', ['#MODULE_NAME#' => settings()->get('module')['name']]));
$textPage = Loc::getMessage('ArtamonovRestInstallMessageHeader');
$textPage .= Loc::getMessage('ArtamonovRestInstallMessageBody', ['#MODULE_NAME#' => settings()->get('module')['name']]);
$bitrixDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/init.php';
$localDir = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/init.php';
$textConnection = "<?php" . PHP_EOL . PHP_EOL;
$textConnection .= "/**" . PHP_EOL;
$textConnection .= " * Module " . settings()->get('module')['name'] . PHP_EOL;
$textConnection .= " *" . PHP_EOL;
$textConnection .= " * @package  " . settings()->get('module')['id'] . PHP_EOL;
$textConnection .= " * @author   " . settings()->get('author')['name'] . " <" . settings()->get('author')['email'] . ">" . PHP_EOL;
$textConnection .= " * @website  " . settings()->get('author')['website'] . PHP_EOL;
$textConnection .= " */" . PHP_EOL;
$textConnection .= settings()->get('module')['connectionString'] . PHP_EOL;
if (is_file($localDir)) {
    $path = $localDir;
} elseif (is_file($bitrixDir)) {
    $path = $bitrixDir;
}
if (!$path) {
    File::putFileContents($localDir, $textConnection);
    $textPage = str_replace('#PATH#', str_replace($_SERVER['DOCUMENT_ROOT'], '', $localDir), $textPage);
} else {
    $content = File::getFileContents($path);
    if (stripos($content, settings()->get('module')['connectionString']) === false) {
        File::putFileContents(str_replace('.php', '-bcp-' . date('Y-m-d') . '.php', $path), $content);
        if (stripos($content, '<?php') !== false) {
            $content = str_replace_once('<?php', $textConnection, $content);
        } elseif (stripos($content, '<?') !== false) {
            $content = str_replace_once('<?', $textConnection, $content);
        }
    } elseif (stripos($content, '// ' . settings()->get('module')['connectionString']) !== false) {
        $content = str_replace('// ' . settings()->get('module')['connectionString'], settings()->get('module')['connectionString'], $content);
    }
    File::putFileContents($path, $content);
    $textPage = str_replace('#PATH#', str_replace($_SERVER['DOCUMENT_ROOT'], '', $path), $textPage);
}
function str_replace_once($search, $replace, $text)
{
    $pos = strpos($text, $search);
    return $pos !== false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
}

$textPage .= Loc::getMessage('ArtamonovRestInstallMessageFooter');
echo CAdminMessage::ShowNote($textPage);