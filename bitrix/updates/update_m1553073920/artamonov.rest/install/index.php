<?php

use Bitrix\Main\Localization\Loc;

class artamonov_rest extends CModule
{
    private $settings;
    var $MODULE_ID = 'artamonov.rest';

    public function __construct()
    {
        $this->settings = require __DIR__ . '/../settings.php';
        $this->init();
    }

    private function init()
    {
        $this->MODULE_ID = Loc::getMessage('ArtamonovRestModuleId');
        $this->MODULE_NAME = Loc::getMessage('ArtamonovRestModuleName');
        $this->MODULE_DESCRIPTION = Loc::getMessage('ArtamonovRestModuleDescription');
        $this->PARTNER_NAME = 'Artamonov Business';
        $this->PARTNER_URI = 'http://artamonov.biz/';
        $this->MODULE_VERSION = '2.0.0';
        $this->MODULE_VERSION_DATE = '2019-01-05 01:00:00';
    }

    public function DoInstall()
    {
        $this->InstallDB();
        $this->InstallFiles();
        $GLOBALS['APPLICATION']->IncludeAdminFile(Loc::getMessage('restApiInstall'), __DIR__ . '/step.php');
    }

    public function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $GLOBALS['APPLICATION']->IncludeAdminFile(Loc::getMessage('restApiUninstall'), __DIR__ . '/unstep.php');
    }

    public function InstallDB()
    {
        RegisterModule($this->MODULE_ID);
        require __DIR__ . '/migration/up.php';
        return true;
    }

    public function UnInstallDB()
    {
        UnRegisterModule($this->MODULE_ID);
        require __DIR__ . '/migration/down.php';
        return true;
    }

    public function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        CopyDirFiles(__DIR__ . '/css', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/css', true, true);
        return true;
    }

    public function UnInstallFiles()
    {
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFilesEx('/bitrix/css/' . $this->settings['module']['id']);
        return true;
    }
}
