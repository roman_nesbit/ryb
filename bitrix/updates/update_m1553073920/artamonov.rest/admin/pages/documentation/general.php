<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
page()->checkAccess('accessDocumentation');
page()->loadLanguage(__FILE__);
page()->addCss('/bitrix/css/' . settings()->get('module')['id'] . '/' . basename(__DIR__) . '-' . basename(__FILE__, '.php') . '.css');
page()->setTitle(loc('ArtamonovRestPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabMainTitle'), 'TITLE' => loc('ArtamonovRestTabMainDescription')],
    ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabFunctionsTitle'), 'TITLE' => loc('ArtamonovRestTabFunctionsDescription')]
];
if (config()->get('showExamples')) {
    $tabs[] = ['DIV' => 'tab-3', 'TAB' => loc('ArtamonovRestTabExamplesTitle'), 'TITLE' => loc('ArtamonovRestTabExamplesDescription')];
};
$tabControl = new CAdminTabControl('tabControl', $tabs);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
if (!config()->get('showExamples')) helper()->note(loc('ArtamonovRestWarningExamplesHidden', ['#LANG#' => LANG]));
$tabControl->Begin();
$tabControl->BeginNextTab();
include 'general-main.php';
$tabControl->BeginNextTab();
include 'general-functions.php';
if (config()->get('showExamples')) {
    $tabControl->BeginNextTab();
    include 'general-examples.php';
}
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';