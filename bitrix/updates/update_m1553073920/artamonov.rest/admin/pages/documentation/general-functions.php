<?php
page()->loadLanguage(__FILE__);

$methods = get_class_methods('\Artamonov\Rest\Foundation\Response');
sort($methods);
$listMethods = '';
foreach ($methods as $method) {
    if ($method === 'getInstance' || $method === 'json' || $method === '__call') continue;
    $listMethods .= '<div class="row"><div class="cell">request()->' . $method . '()</div></div>';
}
echo loc('ArtamonovRestContent', ['#METHODS#' => $listMethods]);