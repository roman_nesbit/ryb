<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
page()->checkAccess('accessConfig');
page()->loadLanguage(__FILE__);
page()->setTitle(loc('ArtamonovRestPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabMainTitle')],
    ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabAccessTitle')]
];
$tabControl = new CAdminTabControl('tabControl', $tabs);
$groups = [];
$result = CGroup::GetList($by = 'NAME', $order = 'ASC', ['ACTIVE' => 'Y', 'ANONYMOUS' => 'N']);
while ($group = $result->fetch()) {
    if ($group['ID'] == helper()->adminGroupId() || $group['ID'] == helper()->ratingVoteGroupId() || $group['ID'] == helper()->ratingVoteAuthorityGroupId()) continue;
    $groups['REFERENCE_ID'][] = $group['ID'];
    $groups['REFERENCE'][] = $group['NAME'];
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
if ($_POST) {
    $_POST['form'] = basename(__FILE__, '.php');
    if (isset($_POST['save'])) {
        config()->save();
        echo CAdminMessage::ShowNote(loc('ArtamonovRestSaved'));
    } elseif (isset($_POST['restore'])) {
        config()->restore();
        echo CAdminMessage::ShowNote(loc('ArtamonovRestRestored'));
    }
}
$tabControl->Begin()
?>
    <form method="POST" name="<?= basename(__FILE__, '.php') ?>" action="<?= $APPLICATION->GetCurUri() ?>">
        <?= bitrix_sessid_post() ?>
        <? $tabControl->BeginNextTab() ?>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestUseRestApi') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:useRestApi', true, config()->get('useRestApi')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseRestApiHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestPathRestApi') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('text', 'parameter:pathRestApi', config()->get('pathRestApi'), false, false, false, 'size="10"') ?>
                <? ShowJSHint(loc('ArtamonovRestPathRestApiHint')) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4" align="center">&nbsp;</td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestUseJournal') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:useJournal', true, config()->get('useJournal')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseJournalHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestShowExamples') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:showExamples', true, config()->get('showExamples')) ?>
                <? ShowJSHint(loc('ArtamonovRestShowExamplesHint')) ?>
            <td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestUseExampleRoute') ?>
            <td>
            <td width="55%" valign="middle">
                <?= InputType('checkbox', 'parameter:useExampleRoute', true, config()->get('useExampleRoute')) ?>
                <? ShowJSHint(loc('ArtamonovRestUseExampleRouteHint')) ?>
            <td>
        </tr>
        <? $tabControl->BeginNextTab() ?>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestAccessDocumentation') ?>
            <td>
            <td width="55%" valign="middle">
                <?= SelectBoxMFromArray('parameter:accessDocumentation[]', $groups, explode('|', config()->get('accessDocumentation')), '', false, 5, 'class ="inputselect"') ?>
                <? ShowJSHint(loc('ArtamonovRestAccessDocumentationHint')) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestAccessSecurity') ?>
            <td>
            <td width="55%" valign="middle">
                <?= SelectBoxMFromArray('parameter:accessSecurity[]', $groups, explode('|', config()->get('accessSecurity')), '', false, 5, 'class ="inputselect"') ?>
                <? ShowJSHint(loc('ArtamonovRestAccessSecurityHint')) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestAccessJournal') ?>
            <td>
            <td width="55%" valign="middle">
                <?= SelectBoxMFromArray('parameter:accessJournal[]', $groups, explode('|', config()->get('accessJournal')), '', false, 5, 'class ="inputselect"') ?>
                <? ShowJSHint(loc('ArtamonovRestAccessJournalHint')) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td width="45%" valign="middle"><?= loc('ArtamonovRestAccessSupport') ?>
            <td>
            <td width="55%" valign="middle">
                <?= SelectBoxMFromArray('parameter:accessSupport[]', $groups, explode('|', config()->get('accessSupport')), '', false, 5, 'class ="inputselect"') ?>
                <? ShowJSHint(loc('ArtamonovRestAccessSupportHint')) ?>
            <td>
        </tr>
        <tr>
            <td colspan="4" valign="middle">
                <? $tabControl->Buttons() ?>
                <?= InputType('submit', 'save', loc('ArtamonovRestButtonSave'), false, false, false, 'class="adm-btn-save"') ?>
                <?= InputType('submit', 'restore', loc('ArtamonovRestButtonRestore'), false) ?>
            </td>
        </tr>
    </form>
<?php
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';