<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
page()->checkAccess('accessSupport');
page()->loadLanguage(__FILE__);
page()->addCss('/bitrix/css/' . settings()->get('module')['id'] . '/' . basename(__FILE__, '.php') . '.css');
page()->setTitle(loc('ArtamonovRestPageTitle'));
$tabs = [
    ['DIV' => 'tab-1', 'TAB' => loc('ArtamonovRestTabMainTitle'), 'TITLE' => loc('ArtamonovRestTabMainDescription')],
    ['DIV' => 'tab-2', 'TAB' => loc('ArtamonovRestTabMonitorTitle'), 'TITLE' => loc('ArtamonovRestTabMonitorDescription')]
];
$tabControl = new CAdminTabControl('tabControl', $tabs);
$routes = [];
$dir = __DIR__ . '/../../routes';
$files = array_diff(scandir($dir), ['..', '.']);
foreach ($files as $file) {
    if (!config()->get('showExamples') && $file === '_example.php') continue;
    if (is_array($ar = require $dir . '/' . $file)) {
        foreach ($ar as $type => $r) {
            foreach ($r as $route => $config) {
                $routes[$type][] = $route;
                if ($config['active'] === false) $routes['disabled'][] = $route;
                if ($config['security']['auth']) $routes['auth'][] = $route;
                if ($config['security']['tokens']['whitelist']) $routes['tokens-whitelist'][] = $route;
                if ($config['security']['tokens']['whitelist']) $routes['groups-whitelist'][] = $route;
            }
        }
    }
}
$totalTokens = (config()->get('useToken')) ? db()->query('SELECT COUNT(VALUE_ID) as COUNT FROM b_uts_user WHERE ' . settings()->getCodeTokenFiled() . ' IS NOT NULL')->fetch()['COUNT'] : 0;
$totalRequest = db()->query('SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'])->fetch()['COUNT'];
$totalRequestGet = db()->query('SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="GET"')->fetch()['COUNT'];
$totalRequestPost = db()->query('SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="POST"')->fetch()['COUNT'];
$totalRequestPut = db()->query('SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="PUT"')->fetch()['COUNT'];
$totalRequestDelete = db()->query('SELECT COUNT(ID) as COUNT FROM ' . settings()->get('config')['table']['request-response'] . ' WHERE METHOD="DELETE"')->fetch()['COUNT'];
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
$tabControl->Begin();
?>
<? $tabControl->BeginNextTab() ?>
    <div class="paragraphs main">
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= settings()->get('module')['name'] ?></li>
                    <li class="version"
                        title="<?= loc('ArtamonovRestVersion') ?>"><?= settings()->get('module')['version'] ?></li>
                    <li class="version-date"
                        title="<?= loc('ArtamonovRestVersionDate') ?>"><?= date_format(date_create(settings()->get('module')['versionDate']), 'd.m.Y') ?></li>
                </ul>
            </div>
            <div class="description">
                <section>
                    <div class="header"><i class="fas fa-file-alt"></i><?= loc('ArtamonovRestDescription') ?></div>
                    <div class="body"><?= settings()->get('module')['description'] ?></div>
                </section>
            </div>
        </div>
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li title="<?= loc('ArtamonovRestVendor') ?>"><?= loc('ArtamonovRestPageTitle') ?>
                        : <?= print_url(settings()->get('partner')['website'], settings()->get('partner')['name'], 'target="_blank"') ?></li>
                    <li class="marketplace"
                        title="<?= loc('ArtamonovRestMarketplace') ?>"><?= print_url(settings()->get('path')['marketplace'], loc('ArtamonovRestMarketplace'), 'target="_blank"') ?></li>
                </ul>
            </div>
        </div>
    </div>

<? $tabControl->BeginNextTab() ?>
    <div class="paragraphs monitor">
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= loc('ArtamonovRestModule') ?></li>
                </ul>
            </div>
            <div class="table-two-column">
                <section>
                    <div class="header"><i class="fas fa-cog"></i><?= loc('ArtamonovRestConfig') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseRestApi') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useRestApi') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useRestApi') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestPathRestApi') ?></div>
                            <div class="cell"><?= config()->get('pathRestApi') ? config()->get('pathRestApi') : '-' ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseJournal') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useJournal') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useJournal') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestShowExamples') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('showExamples') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('showExamples') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseExampleRoute') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useExampleRoute') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useExampleRoute') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestPhp') ?></div>
                            <div class="cell"><?= PHP_VERSION ?></div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="header"><i class="fas fa-key"></i><?= loc('ArtamonovRestauth') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseToken') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useToken') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useToken') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTokenKey') ?></div>
                            <div class="cell"><?= config()->get('tokenKey') ? config()->get('tokenKey') : '-' ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestUseRequestLimit') ?></div>
                            <div class="cell"
                                 style="color: <?= config()->get('useRequestLimit') ? 'rgb(34, 162, 59)' : 'rgb(206, 0, 0)' ?>"><?= config()->get('useRequestLimit') ? loc('ArtamonovRestEnabled') : loc('ArtamonovRestDisabled') ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalTokens') ?></div>
                            <div class="cell"><?= $totalTokens ?></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="paragraph">
            <div class="meta">
                <ul>
                    <li><?= loc('ArtamonovRestStatistics') ?></li>
                </ul>
            </div>
            <div class="table-two-column">
                <section>
                    <div class="header"><i class="fas fa-sitemap"></i><?= loc('ArtamonovRestRoutes') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesGet') ?></div>
                            <div class="cell"><?= ($routes['GET']) ? count($routes['GET']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesPost') ?></div>
                            <div class="cell"><?= ($routes['POST']) ? count($routes['POST']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesPut') ?></div>
                            <div class="cell"><?= ($routes['PUT']) ? count($routes['PUT']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesDelete') ?></div>
                            <div class="cell"><?= ($routes['DELETE']) ? count($routes['DELETE']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesDisabled') ?></div>
                            <div class="cell"><?= ($routes['disabled']) ? count($routes['disabled']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesAuth') ?></div>
                            <div class="cell"><?= ($routes['auth']) ? count($routes['auth']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesTokensWhitelist') ?></div>
                            <div class="cell"><?= ($routes['tokens-whitelist']) ? count($routes['tokens-whitelist']) : 0 ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestRoutesGroupsWhitelist') ?></div>
                            <div class="cell"><?= ($routes['groups-whitelist']) ? count($routes['groups-whitelist']) : 0 ?></div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="header"><i class="fas fa-sign-in-alt"></i><?= loc('ArtamonovRestRequests') ?></div>
                    <div class="body">
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequest') ?></div>
                            <div class="cell"><?= $totalRequest ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequestGet') ?></div>
                            <div class="cell"><?= $totalRequestGet ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequestPost') ?></div>
                            <div class="cell"><?= $totalRequestPost ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequestPut') ?></div>
                            <div class="cell"><?= $totalRequestPut ?></div>
                        </div>
                        <div class="row">
                            <div class="cell"><?= loc('ArtamonovRestTotalRequestDelete') ?></div>
                            <div class="cell"><?= $totalRequestDelete ?></div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
<?php
$tabControl->End();
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';