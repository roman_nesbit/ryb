<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

/*
|--------------------------------------------------------------------------
| Пример карты роутов
|--------------------------------------------------------------------------
|
| Внимание, данный файл может быть перезаписан при обновлении модуля.
|
| Для добавления своих роутов, создайте собственную карту.
| Например:
| /routes/sale.php
| /routes/iblock.php
| И так далее, количество карт неограниченно.
| Карты будут подгружены автоматически.
|
| Совет: не желательно разбивать карту на множество файлов,
|        так как, чем больше файлов, тем больше будет происходить их подключений,
|        соответственно, затрачивая дополнительное время при запуске интерфейса.
|
| Чтобы локализовать карты воспользуйтесь директорией lang.
| Например:
| /lang/ru/routes/sale.php
| /lang/ru/routes/iblock.php
|
| А также, не забудьте указать контроллеры для обработки роутов.
| Контроллеры могут располагаться где угодно, главное чтобы они были доступны через пространство имён.
|
| Для получения карты из контроллера можно воспользоваться методом request()->map()
|
| Подсказка: за счет наличия карты роутов
|            вы можете сгенерировать документацию и для сторонних разработчиков,
|            которым не хотите предоставлять доступ к документации в админ. разделе,
|            например, /api/docs/.
|
| Поддерживаемые типы параметров: string, integer, float, email, ip, domain, url
|
*/

return [
    // Тип запроса
    'GET' => [
        // Роут
        // Для получения всех параметров request()->get()
        // Для получения конкретного параметра request()->get({parameter_name})
        'example/check' => [
            // Описание роута
            'description' => loc('GET:check'),
            // Активность роутра
            // Необязательный параметр (по умолчанию роут активен)
            //'active' => false,
            // Контроллер обрабатывающий роут
            // Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
            // Где Check - класс, read - метод класса
            'controller' => '\Artamonov\Rest\Controllers\Example\Check@read',
            // Безопасность участвует при инициализации ядра интерфейса
            // Для работы параметра необходимо чтобы была включена Авторизация по токену в настройках модуля
            'security' => [
                // Необходимость авторизации при запросе
                // Происходит проверка на наличие заголовка Authorization-Token
                'auth' => true,
                // Доступ к методу будет доступен только для токенов из списка
                // Если имеется данный список, тогда запрос к базе данных выполняться не будет
                'tokens' => [
                    'whitelist' => [
                        'bc95d11b-f2fdf7f4-15e869d3-882e72b5',
                        '408f4f2e-d5a6e4a7-06930a16-8301b343'
                    ]
                ],
                // Доступ к методу будет доступен только для групп из списка
                // Указывается ID группы
                // Если имеется белый список токенов, тогда он будет иметь больший приоритет, чем текущий список
                'groups' => [
                    'whitelist' => [
                        6,
                        7
                    ]
                ]
            ],
            // Параметры запроса
            // Необходимы лишь для документации, при обработке запросов не используются
            // Все проверки параметров нужно реализовывать самостоятельно, непосредственно в методе контроллера
            // Напоминание: для получения текущих параметров в контроллере, можно воспользоваться методом request()->map()
            'parameters' => [
                'iblock_id' => [
                    'required' => true,
                    'type' => 'integer',
                    // Возможные значения параметра
                    'possibleValue' => [
                        '1',
                        '...',
                        '50'
                    ],
                    'description' => loc('parameter:iblock_id')
                ],
                'active' => [
                    'type' => 'string',
                    'possibleValue' => [
                        'Y',
                        'N'
                    ],
                    'description' => loc('parameter:active')
                ],
                'name' => [
                    'type' => 'string',
                    'description' => loc('parameter:name')
                ],
                'color' => [
                    'type' => 'string',
                    'possibleValue' => [
                        loc('parameter:value:white'),
                        loc('parameter:value:red')
                    ],
                    'description' => loc('parameter:color')
                ],
                'form' => [
                    'type' => 'string',
                    'possibleValue' => [
                        loc('parameter:value:round'),
                        loc('parameter:value:square')
                    ],
                    'description' => loc('parameter:form')
                ],
                'fields' => [
                    'type' => 'string',
                    'possibleValue' => [
                        'preview_text',
                        'detail_text',
                        'preview_picture',
                        'form'
                    ],
                    'description' => loc('parameter:fields')
                ],
                'sort' => [
                    'type' => 'string',
                    'description' => loc('parameter:sort')
                ],
                'limit' => [
                    'type' => 'integer',
                    'possibleValue' => [
                        '1',
                        '...',
                        '150'
                    ],
                    'description' => loc('parameter:limit')
                ],
                'page' => [
                    'type' => 'integer',
                    'description' => loc('parameter:page')
                ]
            ],
            // Пример ответа
            // Необходим лишь для документации
            // #DOMAIN# и #API# будут автоматически заменены на реальные данные при выводе на экран
            'example' => [
                'request' => [
                    'url' => 'http://#DOMAIN#/#API#/example/check/?iblock_id=1&sort=id:asc',
                    'response' => [
                        'json' => '{"page":1,"total":3,"items":[{"ID":1,"NAME":"item1"},{"ID":2,"NAME":"item2"},{"ID":3,"NAME":"item3"}]}'
                    ]
                ]
            ],
            // Настройки для поведения роута в документации
            'documentation' => [
                'exclude' => [
                    // Исключить из документации в административной части сайта
                    'admin' => false // true | false
                ]
            ]
        ]
    ],
    // Запрос поддерживает все возможные параметры: из строки запроса (GET) и из тела запроса
    'POST' => [
        'example/check' => [
            'description' => loc('POST:check'),
            // Роут отключен
            // Клиент получит ответ со статусом: 434 Requested host unavailable
            //'active' => false,
            'controller' => '\Artamonov\Rest\Controllers\Example\Check@create',
            // Сервер ожидает запрос с типом контента application/json
            // Если тип будет отличаться, запрос будет отклонен
            'contentType' => 'application/json',
            'security' => [
                'auth' => true,
                'tokens' => [
                    'whitelist' => [
                        '408f4f2e-d5a6e4a7-06930a16-8301b343'
                    ]
                ]
            ],
            'parameters' => [
                // Параметры: уровень 1
                'iblock_id' => [
                    'required' => true,
                    'type' => 'integer',
                    'description' => loc('parameter:iblock_id')
                ],

                'user' => [
                    'required' => true,
                    'type' => 'array',
                    'description' => loc('parameter:user'),
                    // Параметры: уровень 2
                    'parameters' => [
                        'id' => [
                            'required' => true,
                            'type' => 'integer',
                            'description' => loc('parameter:user:id')
                        ],
                        'name' => [
                            'required' => true,
                            'type' => 'string',
                            'description' => loc('parameter:user:name')
                        ]
                    ]
                ],
                'items' => [
                    'required' => true,
                    'type' => 'array',
                    'description' => loc('parameter:items'),
                    'parameters' => [
                        // Параметры для элемента типа массив
                        // Правила прописыаются только для одного item, но проверка происходит для каждого объекта/массива
                        //
                        // Пример items при запросе:
                        // "items": [
                        //		{
                        //			"name":"test",
                        //			"color":"blue",
                        //			"preview_text":"Описание анонса"
                        //		},
                        //		{
                        //			"name":"test2",
                        //			"color":"blue",
                        //			"detail_text":"Детальное описание"
                        //		},
                        //		{
                        //			"name":"test3",
                        //			"color":"blue"
                        //		}
                        //	],
                        [
                            'name' => [
                                'required' => true,
                                'type' => 'string',
                                'description' => loc('parameter:name')
                            ],
                            'preview_text' => [
                                'type' => 'string',
                                'description' => loc('parameter:preview_text')
                            ],
                            'detail_text' => [
                                'type' => 'string',
                                'description' => loc('parameter:detail_text')
                            ],
                            'preview_picture_url' => [
                                'type' => 'url',
                                'description' => loc('parameter:preview_picture_url')
                            ],
                            'detail_picture_url' => [
                                'type' => 'url',
                                'description' => loc('parameter:detail_picture_url')
                            ],
                            'color' => [
                                'required' => true,
                                'type' => 'string',
                                'description' => loc('parameter:color')
                            ],
                            'form' => [
                                'type' => 'string',
                                'description' => loc('parameter:form')
                            ]
                        ]

                    ]
                ]
            ]
        ]
    ],
    'PUT' => [
        'example/check' => [
            'description' => loc('PUT:check'),
            'controller' => '\Artamonov\Rest\Controllers\Example\Check@update',
            'contentType' => 'application/json',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'element_id' => [
                    'required' => true,
                    'type' => 'integer',
                    'description' => loc('parameter:element_id')
                ],
                'name' => [
                    'type' => 'string',
                    'description' => loc('parameter:name')
                ],
                'preview_text' => [
                    'type' => 'string',
                    'description' => loc('parameter:preview_text')
                ],
                'detail_text' => [
                    'type' => 'string',
                    'description' => loc('parameter:detail_text')
                ],
            ]
        ]
    ],
    'DELETE' => [
        'example/check' => [
            'description' => loc('DELETE:check'),
            'controller' => '\Artamonov\Rest\Controllers\Example\Check@delete',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'element_ids' => [
                    'required' => true,
                    'type' => 'array',
                    'description' => loc('parameter:element_ids')
                ]
            ]
        ]
    ]
];