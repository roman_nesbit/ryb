<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

/*
|--------------------------------------------------------------------------
| Системные маршруты
|--------------------------------------------------------------------------
|
| Внимание, данный файл может быть перезаписан при обновлении модуля.
|
|--------------------------------------------------------------------------
*/

return [
    'GET' => [
        'user' => [
            'description' => loc('GET:user'),
            'controller' => '\Artamonov\Rest\Controllers\Native\User@get',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'id' => [
                    'required' => false,
                    'type' => 'integer',
                    'description' => loc('parameter:id')
                ],
                'login' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'token' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:token')
                ],
            ],
        ],
        'user/token' => [
            'description' => loc('GET:user/token'),
            'controller' => '\Artamonov\Rest\Controllers\Native\Token@get',
            'parameters' => [
                'login' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'password' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:password')
                ]
            ]
        ]
    ],
    'POST' => [
        'user' => [
            'description' => loc('POST:user'),
            'controller' => '\Artamonov\Rest\Controllers\Native\User@create',
            'contentType' => 'application/json',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'login' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'email' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:email')
                ],
                'password' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:password')
                ]
            ],
        ],
        'user/token' => [
            'description' => loc('POST:user/token'),
            'controller' => '\Artamonov\Rest\Controllers\Native\Token@create',
            'contentType' => 'application/json',
            'parameters' => [
                'login' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'password' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:password')
                ]
            ]
        ],
    ],
    'PUT' => [
        'user' => [
            'description' => loc('PUT:user'),
            'controller' => '\Artamonov\Rest\Controllers\Native\User@update',
            'contentType' => 'application/json',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'id' => [
                    'required' => false,
                    'type' => 'integer',
                    'description' => loc('parameter:id')
                ],
                'login' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'token' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:token')
                ],
            ],
        ]
    ],
    'DELETE' => [
        'user' => [
            'description' => loc('DELETE:user'),
            'controller' => '\Artamonov\Rest\Controllers\Native\User@delete',
            'contentType' => 'application/json',
            'security' => [
                'auth' => true
            ],
            'parameters' => [
                'id' => [
                    'required' => false,
                    'type' => 'integer',
                    'description' => loc('parameter:id')
                ],
                'login' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'token' => [
                    'required' => false,
                    'type' => 'string',
                    'description' => loc('parameter:token')
                ],
            ],
        ],
        'user/token' => [
            'description' => loc('DELETE:user/token'),
            'controller' => '\Artamonov\Rest\Controllers\Native\Token@delete',
            'contentType' => 'application/json',
            'parameters' => [
                'login' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:login')
                ],
                'password' => [
                    'required' => true,
                    'type' => 'string',
                    'description' => loc('parameter:password')
                ]
            ]
        ]
    ]
];
