<?php

$MESS = [
    'ArtamonovRestModuleId' => 'artamonov.rest',
    'ArtamonovRestModuleName' => 'REST API Business Edition',
    'ArtamonovRestModuleDescription' => 'Модуль помогает организовать программный интерфейс для внешних и внутренних приложений',
    'ArtamonovRestTablePrefix' => 'artamonov_rest_',
    'ArtamonovRestAuthorName' => 'Денис Артамонов',
    'ArtamonovRestAuthorCompany' => 'Artamonov Business',
];