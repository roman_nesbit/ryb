<?php

$MESS = [
    'ArtamonovRestModuleId' => 'artamonov.rest',
    'ArtamonovRestModuleName' => 'REST API Business Edition',
    'ArtamonovRestModuleDescription' => 'Модуль помогает организовать программный интерфейс для внешних и внутренних приложений',
];