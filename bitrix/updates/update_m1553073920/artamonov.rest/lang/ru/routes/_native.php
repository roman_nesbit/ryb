<?php

$MESS = [
    'GET:user' => 'Получение данных пользователя. Можно передать любой из параметров.',
    'POST:user' => 'Создание нового пользователя. Доступны все параметры согласно <a href="https://dev.1c-bitrix.ru/api_help/main/reference/cuser/add.php" target="_blank">документации</a>. При обработке все параметры приводятся к верхнему регистру, то есть confirm_password равнозначно CONFIRM_PASSWORD.',
    'PUT:user' => 'Обновление данных пользователя. Для идентификации пользователя можно передать любой из параметров: id, login, token. Доступны все параметры согласно <a href="https://dev.1c-bitrix.ru/api_help/main/reference/cuser/update.php" target="_blank">документации</a>. При обработке все параметры приводятся к верхнему регистру, то есть confirm_password равнозначно CONFIRM_PASSWORD.',
    'DELETE:user' => 'Удаление пользователя. Можно передать любой из параметров.',
    'GET:user/token' => 'Получение токена пользователя',
    'POST:user/token' => 'Генерация токена для пользователя',
    'DELETE:user/token' => 'Удаление токена пользователя',
    'parameter:id' => 'ID пользователя',
    'parameter:login' => 'Логин пользователя',
    'parameter:email' => 'E-Mail адрес пользователя',
    'parameter:password' => 'Пароль пользователя',
    'parameter:token' => 'Токен пользователя',
];
