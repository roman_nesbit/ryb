<?php

$MESS = [
    'ArtamonovRestContent' => '<div class="paragraphs examples">
    <div class="paragraph">
        <div class="meta">
            <ul>
                <li>Добавление карты роутов</li>
            </ul>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-file-alt"></i>Описание</div>
                <div class="body">
                    Карты роутов хранятся в директории, расположенной по пути <a href="/bitrix/admin/fileman_admin.php?lang=#LANG&path=/bitrix/modules/#MODULE_ID#/routes/&show_perms_for=0">/bitrix/modules/#MODULE_ID#/routes/</a>.
                    <br>Для добавления карты нужно создать php-файл, возвращающий массив, описывающий необходимые роуты.
                    <br>За основу можно взять карту расположенную по пути <a href="/bitrix/admin/fileman_file_edit.php?path=/bitrix/modules/#MODULE_ID#/routes/_example.php&full_src=Y&lang=#LANG#">/bitrix/modules/#MODULE_ID#/routes/_example.php</a>.
                    <br>Количество карт роутов не ограничено.
                </div>
            </section>
        </div>
    </div>

    <div class="paragraph">
        <div class="meta">
            <ul>
                <li>Добавление роута в карту</li>
            </ul>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-file-alt"></i>Описание</div>
                <div class="body">
                    Главное при добавление роута - сохранять структуру массива.
                    <br>Минимально необходимый набор ключей для работы роута: Тип запроса, Роут, Контроллер.
                </div>
            </section>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-code"></i>Пример роута для типа запроса GET</div>
                <div class="body">
                        <pre>
return [
    // Тип запроса
    \'GET\' => [
        // Роут
        \'example/check\' => [
            // Описание роута
            \'description\' => \'Пример запроса получения элементов из инфоблока\',
            // Контроллер обрабатывающий роут
            // Контроллер может лежать за пределами модуля, главное чтобы он был доступен через пространство имён
            // Где Check - класс, read - метод класса
            \'controller\' => \'\Artamonov\Rest\Controllers\Example\Check@read\',
            // Безопасность участвует при инициализации ядра интерфейса
            // Для работы параметра необходимо чтобы была включена Авторизация по токену в настройках модуля
            \'security\' => [
                // Необходимость авторизации при запросе
                \'auth\' => true,
                // Доступ к методу будет доступен только для токенов из списка
                \'tokens\' => [
                    \'whitelist\' => [
                        \'2e6b0ced-e13551d8-d08de103-8cd310b9\',
                        \'408f4f2e-d5a6e4a7-06930a16-8301b343\',
                    ]
                ],
                // Доступ к методу будет доступен только для групп из списка
                \'groups\' => [
                    \'whitelist\' => [6, 7]
                ]
            ],
            // Параметры модуля
            // Необходимы лишь для документации, при обработке запросов не используются
            // Все проверки параметров нужно реализовывать непосредственно в методе контроллера
            \'parameters\' => [
                \'iblock_id\' => [
                    \'required\' => true,
                    \'type\' => \'integer\',
                    \'description\' => \'ID инфоблока\'
                ]
            ]
        ]
    ]
];
            </pre>
                </div>
            </section>
        </div>
    </div>

    <div class="paragraph">
        <div class="meta">
            <ul>
                <li>Добавление контроллера</li>
            </ul>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-file-alt"></i>Описание</div>
                <div class="body">
                    По умолчанию контроллеры хранятся в директории расположенной по пути <a href="/bitrix/admin/fileman_admin.php?lang=#LANG#&show_perms_for=0&path=/bitrix/modules/#MODULE_ID#/lib/controllers/">/bitrix/modules/#MODULE_ID#/lib/controllers/</a>.
                    <br>Контроллером является обычный класс с методами.
                    <br>За основу можно взять класс расположенный по пути <a href="/bitrix/admin/fileman_file_edit.php?lang=#LANG#&full_src=Y&path=/bitrix/modules/#MODULE_ID#/lib/controllers/example/check.php">/bitrix/modules/#MODULE_ID#/lib/controllers/example/check.php</a>.
                    <br>Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через
                    пространство имён.
                </div>
            </section>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-code"></i>Пример контроллера</div>
                <div class="body">
                        <pre>
namespace Artamonov\Rest\Controllers\Example;

class Check
{
    public function read()
    {
        response()->json([\'message\' => \'Hello world!\']);
    }
}
            </pre>
                </div>
            </section>
        </div>
    </div>
    <div class="paragraph">
        <div class="meta">
            <ul>
                <li>Работа с кэшем</li>
            </ul>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-file-alt"></i>Описание</div>
                <div class="body">
                    Взаимодействие с системным кэшем происходит за счет функции cache().
                    <br>Общий смысл таков: сначала проверяем наличие необходимого кэша, если его нет, тогда создаем
                    его.
                    <br>В следующий раз, данные уже будут подгружаться из кэша.
                    <br>Весь кэш сохраняется в директорию <a href="/bitrix/admin/fileman_admin.php?lang=#LANG#&show_perms_for=0&path=/bitrix/cache/">/bitrix/cache/</a>.
                </div>
            </section>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-code"></i>Пример работы с кэшем</div>
                <div class="body">
                        <pre>
namespace Artamonov\Rest\Controllers\Example;

class Check
{
    public function read()
    {
        // Отдадим данные из кэша если они в нем имеются
        // Иначе, получим данные из базы и запишем в кэш
        // $cacheId - уникальный идентификатор кэша
        // Срок кэша - 7 дней
        // Место хранения /bitrix/cache/example/check
        if (!$response = cache()->get($cacheId, 604800, \'example/check\')) {
            // Массив ответа для клиента
            // Какие-то данные из базы
            $response = [1, 2, 3];
            // Сохраняем данные в кэш чтобы при следующем запросе уже не делать запросы в базу
            if ($response) {
                cache()->set($response);
            }
        }
        // Возвращаем ответ клиенту
        response()->json($response);
        // Отдадим данные из кэша если они в нем имеются
        // Иначе, получим данные из базы и запишем в кэш
        // $cacheId - уникальный идентификатор кэша
        // Срок кэша - 7 дней
        // Место хранения /bitrix/cache/example/check
        if (!$response = cache()->get($cacheId, 604800, \'example/check\')) {
            // Массив ответа для клиента
            // Какие-то данные из базы
            $response = [1, 2, 3];
            // Сохраняем данные в кэш чтобы при следующем запросе уже не делать запросы в базу
            if ($response) {
                cache()->set($response);
            }
        }
        // Возвращаем ответ клиенту
        response()->json($response);
    }
}
                </pre>
                </div>
            </section>
        </div>
    </div>

    <div class="paragraph">
        <div class="meta">
            <ul>
                <li>Добавление записи в журнал</li>
            </ul>
        </div>
        <div class="description">
            <section>
                <div class="header"><i class="fas fa-file-alt"></i>Описание</div>
                <div class="body">Запись в журнал происходит с помощью функции journal().
                </div>
            </section>
        </div>

        <div class="description">
            <section>
                <div class="header"><i class="fas fa-code"></i>Пример добавления записи в журнал</div>
                <div class="body">
                        <pre>
namespace Artamonov\Rest\Controllers\Example;

class Check
{
    public function read()
    {
        // Запишем информацию в Журнал: Запрос/Ответ
        journal()->add(\'request-response\', [\'request\' => request()->get(), \'response\' => $response]);
    }
}
                </pre>
                </div>
            </section>
        </div>
    </div>
</div>'
];