<?php

$MESS = [
    'ArtamonovRestPageTitle' => 'Журнал: ограничение запросов',
    'ArtamonovRestRequests' => 'Запросы',
    'ArtamonovRestId' => 'ID',
    'ArtamonovRestDateTime' => 'Время',
    'ArtamonovRestClientId' => 'ID клиента',
    'ArtamonovRestButtonDelete' => 'Удалить',
    'ArtamonovRestConfirmDelete' => 'Вы уверены, что хотите удалить запись?',
];