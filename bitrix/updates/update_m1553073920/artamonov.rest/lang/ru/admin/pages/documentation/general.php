<?php

$MESS = [
    'ArtamonovRestPageTitle' => 'Основные сведения',
    'ArtamonovRestTabMainTitle' => 'Основное',
    'ArtamonovRestTabMainDescription' => 'Основное описание',
    'ArtamonovRestTabFunctionsTitle' => 'Функции',
    'ArtamonovRestTabFunctionsDescription' => 'Функции и методы',
    'ArtamonovRestTabExamplesTitle' => 'Примеры',
    'ArtamonovRestTabExamplesDescription' => 'Примеры работы с модулем',
    'ArtamonovRestDescription' => 'Описание',
    'ArtamonovRestMethods' => 'Методы',
    'ArtamonovRestWarningExamplesHidden' => 'Внимание: показ подсказок и примеров отключен в <a href="rest-api-config.php?lang=#LANG#">настройках модуля</a>.',
];