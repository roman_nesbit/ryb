<?php

$MESS = [
    'ArtamonovRestPageTitle' => 'Карта роутов',
    'ArtamonovRestTabTitle' => '#METHOD_TYPE#',
    'ArtamonovRestTabGetTitle' => 'GET',
    'ArtamonovRestTabPostTitle' => 'POST',
    'ArtamonovRestTabPutTitle' => 'PUT',
    'ArtamonovRestTabDeleteTitle' => 'DELETE',
    'ArtamonovRestSecurityAuth' => 'Авторизация',
    'ArtamonovRestSecurityAuthHint' => 'Необходимо передать заголовок Authorization-Token с токеном авторизации',
    'ArtamonovRestDisabledHint' => 'Роут отключен',
    'ArtamonovRestEditHint' => 'Редактировать',
    'ArtamonovRestContentTypedHint' => 'Необходимо передать заголовок Content-Type с указанным типом',
    'ArtamonovRestDescription' => 'Описание',
    'ArtamonovRestParameters' => 'Параметры запроса',
    'ArtamonovRestResponseExample' => 'Пример ответа',
    'ArtamonovRestSecurityTokensWhitelist' => 'Список разрешенных токенов',
    'ArtamonovRestSecurityGroupsWhitelist' => 'Список разрешенных групп',
];