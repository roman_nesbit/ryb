<?php

$MESS = [
    'ArtamonovRestPageTitle' => 'Журнал',
    'ArtamonovRestTabMainTitle' => 'Основные сведения',
    'ArtamonovRestTabMainDescription' => 'Основная информация',
    'ArtamonovRestTabRequestTitle' => 'Запрос от клиента',
    'ArtamonovRestTabRequestDescription' => 'Параметры запроса со стороны клиента',
    'ArtamonovRestTabResponseTitle' => 'Ответ клиенту',
    'ArtamonovRestTabResponseDescription' => 'Данные ответа клиенту',
    'ArtamonovRestId' => 'ID запроса',
    'ArtamonovRestMethod' => 'Метод',
    'ArtamonovRestDateTime' => 'Время запроса',
    'ArtamonovRestIp' => 'IP адрес',
    'ArtamonovRestClientId' => 'ID клиента',
    'ArtamonovRestRequest' => 'Запрос',
    'ArtamonovRestButtonBackText' => 'Вернуться в общий список',
    'ArtamonovRestButtonBackTitle' => 'Вернуться в общий список журнала',
];