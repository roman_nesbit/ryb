<?php

$MESS = [
    'ArtamonovRestPageTitle' => 'Журнал: запрос/ответ',
    'ArtamonovRestRequests' => 'Запросы',
    'ArtamonovRestId' => 'ID',
    'ArtamonovRestDateTime' => 'Время',
    'ArtamonovRestIp' => 'IP адрес',
    'ArtamonovRestClientId' => 'ID клиента',
    'ArtamonovRestRequest' => 'Запрос',
    'ArtamonovRestResponse' => 'Ответ',
    'ArtamonovRestButtonDelete' => 'Удалить',
    'ArtamonovRestButtonView' => 'Просмотреть',
    'ArtamonovRestConfirmDelete' => 'Вы уверены, что хотите удалить запись?',
];