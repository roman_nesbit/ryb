<?php

$MESS = [
    'ArtamonovRestMenuItemDocumentation' => 'Документация',
    'ArtamonovRestMenuItemDocumentationGeneral' => 'Основные сведения',
    'ArtamonovRestMenuItemDocumentationRoutes' => 'Карта роутов',
    'ArtamonovRestMenuItemSecurity' => 'Безопасность',
    'ArtamonovRestMenuItemJournal' => 'Журналы',
    'ArtamonovRestMenuItemJournalRequestResponse' => 'Запрос/Ответ',
    'ArtamonovRestMenuItemJournalRequestLimit' => 'Ограничение запросов',
    'ArtamonovRestMenuItemSupport' => 'Поддержка',
    'ArtamonovRestMenuItemSettings' => 'Настройки',
];