<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;

return array(
	'parent' => 'store-chats-light',
	'code' => 'store-chats-light/footer',
	'name' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_LIGHT-FOOTER-NAME'),
	'description' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_LIGHT-FOOTER-DESC'),
	'active' => true,
	'preview' => '',
	'preview2x' => '',
	'preview3x' => '',
	'preview_url' => '',
	'show_in_list' => 'N',
	'type' => 'store',
	'version' => 2,
	'fields' => array(
		'TITLE' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_LIGHT-FOOTER-NAME'),
		'RULE' => null,
		'ADDITIONAL_FIELDS' => array(
			'VIEW_USE' => 'N',
			'VIEW_TYPE' => 'no',
			'THEME_CODE' => 'app',
			'THEME_CODE_TYPO' => 'app',
		),
	),
	'layout' => array(),
	'items' => array(
		'0' => array(
			'code' => '17.copyright',
			'cards' => array(),
			'nodes' => array(
				'.landing-block-node-text' => array(
					0 => '
				<p>&copy 2018 All rights reserved.</p>
			',
				),
			),
			'style' => array(
				'.landing-block-node-text' => array(
					0 => 'landing-block-node-text js-animation animation-none g-font-size-12 ',
				),
				'#wrapper' => array(
					0 => 'landing-block js-animation animation-none',
				),
			),
		),
	),
);