<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;

return array(
	'code' => 'store-chats-dark/webform',
	'name' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_DARK-WEBFORM-NAME'),
	'description' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_DARK-WEBFORM-DESC'),
	'type' => 'store',
	'version' => 3,
	'fields' => array(
		'RULE' => null,
		'ADDITIONAL_FIELDS' => array(
			'METAOG_IMAGE' => 'https://cdn.bitrix24.site/bitrix/images/demo/page/store-chats-dark/webform/preview.jpg',
			'VIEW_USE' => 'N',
			'VIEW_TYPE' => 'no',
			'THEME_CODE' => '3corporate',
			'THEME_CODE_TYPO' => '3corporate',
		),
	),
	'site_group_item' => 'Y',
	
	'items' => array(
		'0' => array(
			'code' => '33.4.form_1_transparent_black_no_text_simple',
			'nodes' => array(
				'.landing-block-node-bgimg' => array(
					0 => array(
						'src' => null,
					),
				),
			),
			'style' => array(
				'#wrapper' => array(
					0 => 'landing-block g-pos-rel g-pt-30 g-pb-30 landing-block-node-bgimg g-bg-img-hero g-bg-cover g-bg-none--after u-block-border u-block-border-margin-md g-rounded-6 g-theme-bitrix-bg-dark-v1',
				),
			),
		),
		
		'1' => array(
			'code' => '55.1.list_of_links',
			'access' => 'X',
			'cards' => array(
				'.landing-block-node-list-item' => array(
					'source' => array(
						0 => array(
							'value' => 0,
							'type' => 'card',
						),
						1 => array(
							'value' => 0,
							'type' => 'card',
						),
						2 => array(
							'value' => 0,
							'type' => 'card',
						),
					),
				),
			),
			'nodes' => array(
				'.landing-block-node-link' => array(
					0 => array(
						'href' => '#landing@landing[store-chats-dark/about]',
						'target' => '_self',
					),
					1 => array(
						'href' => '#landing@landing[store-chats-dark/contacts]',
						'target' => '_self',
					),
					2 => array(
						'href' => '#landing@landing[store-chats-dark/payinfo]',
						'target' => '_self',
					),
				),
				'.landing-block-node-link-text' => array(
					0 => 'About us',
					1 => 'Contacts',
					2 => 'Payment Information',
				),
			),
			'style' => array(
				'.landing-block-node-list-container' => array(
					0 => 'landing-block-node-list-container row no-gutters justify-content-center',
				),
				'.landing-block-node-list-item' => array(
					0 => 'landing-block-node-list-item g-brd-bottom g-brd-1 g-py-12 js-animation fadeInRight landing-card g-brd-white-opacity-0_2 g-font-size-18',
					1 => 'landing-block-node-list-item g-brd-bottom g-brd-1 g-py-12 js-animation fadeInRight landing-card g-brd-white-opacity-0_2 g-font-size-18',
					2 => 'landing-block-node-list-item g-brd-bottom g-brd-1 g-py-12 js-animation fadeInRight landing-card g-brd-white-opacity-0_2 g-font-size-18',
				),
				'.landing-block-node-link' => array(
					0 => 'landing-block-node-link row no-gutters justify-content-between align-items-center g-text-decoration-none--hover g-color-primary--hover font-weight-bold g-color-white',
					1 => 'landing-block-node-link row no-gutters justify-content-between align-items-center g-text-decoration-none--hover g-color-primary--hover font-weight-bold g-color-white',
					2 => 'landing-block-node-link row no-gutters justify-content-between align-items-center g-text-decoration-none--hover g-color-primary--hover font-weight-bold g-color-white',
				),
				'#wrapper' => array(
					0 => 'landing-block g-pt-10 g-pb-10 g-pl-15 g-pr-15 u-block-border u-block-border-margin-md g-rounded-6 g-theme-bitrix-bg-dark-v1',
				),
			),
		),
	),
);