<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;

return array(
	'parent' => 'store-chats-dark',
	'code' => 'store-chats-dark/header',
	'name' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_DARK-HEADER-NAME'),
	'description' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_DARK-HEADER-DESC'),
	'active' => true,
	'preview' => '',
	'preview2x' => '',
	'preview3x' => '',
	'preview_url' => '',
	'show_in_list' => 'N',
	'type' => 'store',
	'version' => 3,
	'fields' => array(
		'TITLE' => Loc::getMessage('LANDING_DEMO_STORE_CHATS_DARK-HEADER-NAME'),
		'RULE' => null,
		'ADDITIONAL_FIELDS' => array(
			'METAOG_IMAGE' => 'https://cdn.bitrix24.site/bitrix/images/demo/page/store-chats-dark/header/preview.jpg',
			'VIEW_USE' => 'N',
			'VIEW_TYPE' => 'no',
			'THEME_CODE' => '3corporate',
			'THEME_CODE_TYPO' => '3corporate',
		),
	),
	'layout' => array(),
	'items' => array(
		'0' => array(
			'code' => '35.8.header_logo_and_slogan_row',
			'nodes' => array(
				'.landing-block-node-logo' => array(
					0 => array(
						'alt' => 'Logo',
						'src' => 'https://cdn.bitrix24.site/bitrix/images/landing/logos/chats-store-dark-small.png',
						'url' => '{"text":"","href":"#system_mainpage","target":"_self","enabled":true}',
					),
				),
			),
			'style' => array(
				'.landing-block-node-text' => array(
					0 => 'landing-block-node-text h5 g-font-size-12 mb-0 g-color-white-opacity-0_7',
				),
			)
		),
	),
);