<?php

$appEnvironment = getenv('APP_ENV');
$isDebugMode = getenv('MARKETPLACE_DEBUG') == 1 && in_array($appEnvironment, ['dev', 'staging']);

return [
  'utf_mode' => [
    'value' => true,
    'readonly' => true,
  ],
  'cache_flags' => [
    'value' => [
      'config_options' => 3600.0,
      'site_domain' => 3600.0,
    ],
    'readonly' => false,
  ],
  'cookies' => [
    'value' => [
      'secure' => false,
      'http_only' => true,
    ],
    'readonly' => false,
  ],
  'exception_handling' => [
    'value' => [
      'debug' => $isDebugMode,
      'handled_errors_types' => 4437,
      'exception_errors_types' => 4437,
      'ignore_silence' => false,
      'assertion_throws_exception' => true,
      'assertion_error_type' => 256,
      'log' => array (
          'settings' =>
              array (
                  'file' => getenv('MARKETPLACE_LOGS_DIR') . '/app/bitrix/exceptions.log',
                  'log_size' => 1000000,
              ),
      ),
    ],
    'readonly' => false,
  ],
  'connections' => [
    'value' => [
      'default' => [
        'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
        'host' => getenv('MARKETPLACE_DB_HOST'),
        'database' => getenv('MARKETPLACE_DB_NAME'),
        'login' => getenv('MARKETPLACE_DB_USER'),
        'password' => getenv('MARKETPLACE_DB_PASSWORD'),
        'options' => 2.0,
      ],
    ],
    'readonly' => true,
  ],
  'crypto' => [
    'value' => [
      'crypto_key' => getenv('MARKETPLACE_BITRIX_CRYPTO_KEY'),
    ],
    'readonly' => true,
  ],
];
