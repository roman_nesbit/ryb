<?php

/**
 * This file is used by the build process in order to reset opcode cache after a deployment
 */

$result = opcache_reset();
echo $result
    ? "Opcode cache was successfully reset"
    : "Failed to reset opcode cache";
