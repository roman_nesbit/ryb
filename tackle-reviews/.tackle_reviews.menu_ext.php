<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$aMenuLinksExt = [];

$res = CIBlockSection::GetList(
	[
		'SORT' => 'DESC',
		'NAME' => 'ASC',
	],
	[
		'IBLOCK_TYPE' => 'content',
		'IBLOCK_ID' => TACKLE_REVIEWS_IBLOCK,
		'ACTIVE' => 'Y',
	],
	true,
	[
		'NAME',
		'SECTION_PAGE_URL',
		'UF_ICON'
	],
	false
);
while($section = $res->GetNext()) {
	if($section['ELEMENT_CNT'] > 0) {
		$aMenuLinksExt[] = [
			0 => $section['NAME'],
			1 => $section['SECTION_PAGE_URL'],
			2 => [],
			3 => [
				'ICON' => $section['UF_ICON'] ?? false,
			],
		];
	}
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>