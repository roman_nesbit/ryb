<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/page_templates/static.php', [
	'MENU_TYPES' => ['tackle_reviews'],
	'HIDE_RECOMMENDED' => 'Y',
	'HIDE_STATIC_PAGE_CLASS' => 'N',
]);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>