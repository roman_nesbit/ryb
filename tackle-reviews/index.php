<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Обзоры и тесты");

$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/page_templates/static.php', [
	'MENU_TYPES' => ['tackle_reviews'],
	'HIDE_RECOMMENDED' => 'Y',
	'HIDE_STATIC_PAGE_CLASS' => 'Y',
]);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>