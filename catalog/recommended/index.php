<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мы рекомендуем");

$APPLICATION->IncludeComponent(
	"bfs:catalog.filtered", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGE_COUNT" => "12",
		"FILTER" => "{\"!PROPERTY_RECOMMENDED\":false}",
		"SORT_FIELD" => "SHOW_COUNTER",
		"SORT_FIELD2" => "ID",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => DESC,
		"TITLE" => "Мы рекомендуем",
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>