<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акционные товары");

$APPLICATION->IncludeComponent(
	"bfs:catalog.filtered", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGE_COUNT" => "12",
		"FILTER" => "{\"PROPERTY_PROMOTIONAL\":\"Y\"}",
		"SORT_FIELD" => "SHOW_COUNTER",
		"SORT_FIELD2" => "ID",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => DESC,
		"TITLE" => "Акционные товары",
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>