<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новые товары");

$daysToAssumeAsNew = \Bitrix\Main\Config\Option::get('grain.customsettings', 'PRODUCT_NEW_PERIOD');
$lastTimestampForNew = AddToTimeStamp(['DD' => -$daysToAssumeAsNew], MakeTimeStamp(new DateTime));
$lastDayForNew = ConvertTimeStamp($lastTimestampForNew, 'SHORT');

$APPLICATION->IncludeComponent(
	"bfs:catalog.filtered", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGE_COUNT" => "12",
		"FILTER" => "{\">=DATE_CREATE\":\"" . $lastDayForNew . "\"}",
		"SORT_FIELD" => "DATE_CREATE",
		"SORT_FIELD2" => "SHOW_COUNTER",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => "DESC",
		"TITLE" => "Новые товары",
		"HIDE_NOT_AVAILABLE" => "Y",
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>