<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle('Страница не найдена');
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/page_templates/404.php');


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>