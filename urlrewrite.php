<?php
$arUrlRewrite=array (
  8 => 
  array (
    'CONDITION' => '#^/search/filter/((.*)\\-is\\-(.*)|price\\-(.*)|clear)/(\\\\?(.*))#',
    'RULE' => 'SMART_FILTER_PATH=$1',
    'ID' => '',
    'PATH' => '/search/index.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/personal/order/([A-Z-0-9]+)/cancel/(\\\\?(.*))#',
    'RULE' => 'order_id=$1',
    'ID' => '',
    'PATH' => '/personal/order/cancel.php',
    'SORT' => 100,
  ),
  10 => 
  array (
    'CONDITION' => '#^/tackle-reviews/([a-z-0-9]+)/([a-z-0-9]+)/#',
    'RULE' => 'section=$1&item=$2&COND=TACKLE_ITEM',
    'ID' => '',
    'PATH' => '/tackle-reviews/item/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/personal/order/([A-Z-0-9]+)/#',
    'RULE' => 'order_id=$1',
    'ID' => '',
    'PATH' => '/personal/order/details.php',
    'SORT' => 100,
  ),
  11 => 
  array (
    'CONDITION' => '#^/tackle-reviews/([a-z-0-9]+)/#',
    'RULE' => 'section=$1&COND=TACKLE_SECTION',
    'ID' => '',
    'PATH' => '/tackle-reviews/index.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/bitrix/services/ymarket/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/services/ymarket/index.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  3 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
  12 => 
  array (
    'CONDITION' => '#^/#',
    'RULE' => 'CATALOG=',
    'ID' => 'bitrix:catalog',
    'PATH' => '/catalog/index.php',
    'SORT' => 100,
  ),
);
