<?php

namespace Sale\Handlers\PaySystem;

use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\Payment;
use Bitrix\Main\Request;

// Loc::loadMessages(__FILE__);

/**
 * Class BankHandler
 */
class BankHandler extends PaySystem\ServiceHandler implements PaySystem\IRefundExtended, PaySystem\IHold, PaySystem\IPayable
{
	public function initiatePay(Payment $payment, Request $request = null)
	{
		return $this->showTemplate($payment, "template");
	}

	public function getPrice(Payment $payment)
	{}

	public function processRequest(Payment $payment, Request $request)
	{}

	public function getPaymentIdFromRequest(Request $request)
	{}

	public function getCurrencyList()
	{}

	public function isRefundableExtended()
	{}

	public function refund(Payment $payment, $refundableSum)
	{}

	public function cancel(Payment $payment)
	{}

	public function confirm(Payment $payment)
	{}

	public function getStructure()
	{}
}