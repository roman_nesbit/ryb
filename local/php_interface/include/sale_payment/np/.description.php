<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$data = [
	'NAME' => Loc::getMessage("SALE_HPS_NP"),
	'SORT' => 100,
	'CODES' => [
		'TARIF_FIX' => [
			'NAME' => 'Фиксированная часть (грн.)',
			'DESCRIPTION' => '',
			'SORT' => 100,
			'DEFAULT' => [
				'PROVIDER_KEY' => 'VALUE',
				'PROVIDER_VALUE' => 10,
			]
		],
		'TARIF_PERCENT' => [
			'NAME' => 'Процент от суммы (%)',
			'DESCRIPTION' => '',
			'SORT' => 200,
			'DEFAULT' => [
				'PROVIDER_KEY' => 'VALUE',
				'PROVIDER_VALUE' => 2,
			]
		],
	]
];