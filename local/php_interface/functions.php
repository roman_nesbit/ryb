<?php

use Bitrix\Main\Data\Cache;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08.12.2018
 * Time: 11:29
 */
//функция дебаггер
function dd($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
function exo($arr) {
    echo "<pre>"; print_r($arr); echo "</pre>";
}

//получения адреса сайта
function url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}
function generateToken($login, $name){
    $token = md5($login . '-' . $name . '=' . date('Y-m-d H:i:s'));
    $token = str_split($token, 8);
    $token = implode('-', $token);
    return $token;
}

// склонения
function pluralForm($n, $form1, $form2, $form5) {
	$n = abs($n) % 100;
	$n1 = $n % 10;
	if ($n > 10 && $n < 20) return $form5;
	if ($n1 > 1 && $n1 < 5) return $form2;
	if ($n1 == 1) return $form1;
	return $form5;
}

function getElementNameById($elementId) {
	if(!$elementId) return false;
	$arSelect = array("ID", "NAME");
	$arFilter = array("ID" => $elementId);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if($obj = $res->Fetch()) {
		return $obj["NAME"];
	} else {
		return "";
	}
}

function getElementIBlockById($elementId) {
	$arSelect = array("ID", "IBLOCK_ID");
	$arFilter = array("ID" => $elementId);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if($obj = $res->Fetch()) {
		return $obj["IBLOCK_ID"];
	} else {
		return false;
	}
}

function getElementPreviewPictureById($elementId) {
	$arSelect = array("ID", "PREVIEW_PICTURE");
	$arFilter = array("IBLOCK_ID" => getElementIBlockById($elementId), "ID" => $elementId);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if($obj = $res->Fetch()) {
		return CFile::GetPath($obj["PREVIEW_PICTURE"]);
	} else {
		return false;
	}
}

function getSectionDataById($iblockId, $sectionId) {
	$res = CIBlockSection::GetList(
		[],
		[
			'IBLOCK_ID' => $iblockId,
			'ID' => $sectionId,
		],
		false,
		[
			'NAME',
			'SECTION_PAGE_URL',
		],
		false
	);

	if($obj = $res->GetNext()) {
		return $obj;
	} else {
		return false;
	}
}

function getOrderStatusData($statusCode) {
	switch($statusCode) {
		case 'N':
			$statusColor = 'blue';
			$statusIcon = 'tagIcon';
			break;
		case 'P':
			$statusColor = 'orange';
			$statusIcon = 'reloadIcon';
			break;
		case 'D':
			$statusColor = 'orange';
			$statusIcon = 'trolleyIcon';
			break;
		case 'C':
			$statusColor = 'red';
			$statusIcon = 'closeRoundedIcon';
			break;
		case 'R':
			$statusColor = 'red';
			$statusIcon = 'packageIcon';
			break;
		case 'F':
			$statusColor = 'green';
			$statusIcon = 'correctIcon';
			break;
		case 'S':
			$statusColor = 'red';
			$statusIcon = 'returnIcon';
			break;
		default:
			$statusColor = false;
			$statusIcon = false;
	}
	return [
		'COLOR' => $statusColor,
		'ICON' => $statusIcon,
	];
}

// Описание способа доставки с суммой
function getDeliveryDescription(int $deliveryId, string $deliverySumFormatted)
{
	$_res = \Bitrix\Sale\Delivery\Services\Table::getList([
		'select' => ['*'],
		'filter' => [
			'ID' => $deliveryId,
		]
	]);
	if ($_obj = $_res->fetch()) {
		$_description = str_replace('#DELIVERY_PRICE#', $deliverySumFormatted, $_obj['DESCRIPTION']);
	}

	return $_description;
}

//сортировка товаров в корзине
function sortByNameAsc($arrA, $arrB){
    $a = $arrA['NAME'];
    $b = $arrB['NAME'];
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}
//сортировка товаров в API
function sortByNameAscAPI($arrA, $arrB){
    $a = $arrA['name'];
    $b = $arrB['name'];
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}
//сортировка товаров в коллекции
function sortByNameAscCollection($arrA, $arrB){
    $a = $arrA->getField('NAME');
    $b = $arrB->getField('NAME');
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}
function sortBasketItemsByNameCollection(&$items_array){
    uasort($items_array , 'sortByNameAscCollection');
}
function sortBasketItemsByName(&$items_array, $api=false){
    if($api)
        uasort($items_array , 'sortByNameAscAPI');
    else
        uasort($items_array , 'sortByNameAsc');
}