<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08.12.2018
 * Time: 11:36
 */

use Bitrix\Main;
use Bitrix\Main\Config\Option;
use Bitrix\Main\UserTable;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Delivery\Services\Manager;
use Bitrix\Sale\Order;
use Bitrix\Sale\PaySystem;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Model\Bitrix\MarketplaceConstants;
use Rybalka\Marketplace\Service\Event\Listener\Bitrix\EmailNotificationEventsListener;
use Rybalka\Marketplace\Service\Event\Listener\Bitrix\InfoBlockEventsListener;
use Rybalka\Marketplace\Service\Event\Listener\Bitrix\MessageServiceEventsListener;
use Rybalka\Marketplace\Service\Event\Listener\Bitrix\SaleEventsListener;
use Rybalka\Marketplace\Service\Notification\SmsService;
use Rybalka\Marketplace\Util\EventLog;
use Rybalka\Marketplace\Util\InvoicePdf;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\Phone\Normalizer;
use Rybalka\Marketplace\Util\Recaptcha;

$infoBlockEventsListener = new InfoBlockEventsListener();
$messageServiceEventsListener = new MessageServiceEventsListener();
$saleEventsListener = new SaleEventsListener();
$emailNotificationEventListener = new EmailNotificationEventsListener();

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("IblockHandlers", "CheckProcentes"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("IblockHandlers", "AddUniqueNumber"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("IblockHandlers", "CheckUniqueNumber"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("IblockHandlers", "CheckModeratorsShop"));
// AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("IblockHandlers", "CheckItemStatus"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("IblockHandlers", "CheckProcentes"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("IblockHandlers", "CheckModeratorsShop"));
// AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("IblockHandlers", "CheckItemStatus"));

AddEventHandler("main", "OnBeforeUserAdd", Array("UserHandlers", "AddDeleteToken"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("UserHandlers", "AddDeleteToken"));
AddEventHandler("main", "OnBeforeUserLogin", Array("UserHandlers", "OnBeforeUserLoginHandler"));
AddEventHandler("main", "OnBeforeEventAdd", ["EventHandlers", "OnBeforeEventAddHandler"]);
AddEventHandler("main", 'OnBeforeEventAdd', [$emailNotificationEventListener, 'onBefore']);

Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleOrderSaved', [$saleEventsListener, 'saleOrderSaved']);
Main\EventManager::getInstance()->addEventHandler('sale', 'OnSalePaymentEntitySaved', [$saleEventsListener, 'salePaymentSaved']);

// Пользовательские типы событий для журнала
AddEventHandler('main', 'OnEventLogGetAuditTypes', ['UserHandlers', 'TYPE_SEND_SMS_OnEventLogGetAuditTypes']);



AddEventHandler('sale', 'OnSaleComponentOrderOneStepComplete', Array('CSalehandlers', 'OnSaleComponentOrderOneStepCompleteHandler'));
AddEventHandler("sale", "OnSaleBeforeOrderCanceled", Array("CSalehandlers", "OnSaleBeforeOrderCanceledHandlers"));
AddEventHandler("sale", 'OnSaleStatusOrder', [$saleEventsListener, 'onOrderStatusChange']);

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", [$infoBlockEventsListener, 'onBeforeUpdate']);
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", [$infoBlockEventsListener, 'onBeforeAdd']);

AddEventHandler('form', 'onBeforeResultAdd', 'onBeforeResultAddHandler');
AddEventHandler('messageservice', 'onGetSmsSenders', [$messageServiceEventsListener, 'onGetSmsSenders']);

function onBeforeResultAddHandler($WEB_FORM_ID, &$arFields, &$arrVALUES)
{
	global $APPLICATION;

	if (isset($arrVALUES['g_recaptcha_token'])) {
		if (!Recaptcha::isHuman($arrVALUES['g_recaptcha_token'])) {
			$APPLICATION->throwException('You are a bot!');
		}
	}
}

class EventHandlers {
	function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$messageId, &$files, &$languageId) {
		if($event == 'SALE_NEW_ORDER') {

			try {
				$orderId = $arFields['ORDER_REAL_ID'];

				// Make invoice PDF
				$invoicePdf = new InvoicePdf($orderId);
				$order = $invoicePdf->getOrder();

				// Get order delivery (of the first shipment)
				$delivery = $order->getShipmentCollection()[0];
				$deliveryPriceFormatted = CurrencyFormat($delivery->getPrice(), 'UAH');
				$deliveryId = $delivery->getField('DELIVERY_ID');
				$arFields['DELIVERY_DESCRIPTION'] = getDeliveryDescription($deliveryId, $deliveryPriceFormatted);
			} catch(BadRequestException $e) {
				AddMessage2Log($e->getMessage());
			}
		}
	}
}

function check_unique_number_shop(){
	$unique_number = rand(100000, 999999);
	$arFilter = Array(
		"IBLOCK_ID" => SHOPS_IBLOCK,
		"PROPERTY_UNIQUE_NUMBER" => $unique_number,
	);
	$res = CIBlockElement::GetList([], $arFilter, false, [], ['ID']);
	while ($ar_result = $res->Fetch()){
		return false;
	}
	return $unique_number;
}
class IblockHandlers
{

	// проверка на пределы в проверяемых значениях и валидность комиссии магазина
	function CheckProcentes(&$arFields){
		if($arFields['IBLOCK_ID']==SHOPS_IBLOCK) {
			$arr_keys = array_keys($arFields['PROPERTY_VALUES'][148]);
			$str = $arFields['PROPERTY_VALUES'][148][$arr_keys[0]]['VALUE'];
			if (is_numeric($str)) {
				$float_percent = round(floatval($str), 2);
				if ($float_percent < 0.00 || $float_percent > 30.00) {
					global $APPLICATION;
					$APPLICATION->throwException("Значение комиссии магазина должно быть в диапазоне от 0.00% до 30.00%");
					return false;
				}
			} else {
				global $APPLICATION;
				$APPLICATION->throwException("Значение комиссии магазина содержит недопустимые символы");
				return false;
			}
		}
	}
	//Манипуляции с пользователями при смене модератора в магазине
	function CheckModeratorsShop(&$arFields){
		if($arFields['IBLOCK_ID']==SHOPS_IBLOCK) {
			$users = [];
			foreach ($arFields['PROPERTY_VALUES'][124] as $key=>$PROPERTY_VALUE) {
				if($PROPERTY_VALUE['VALUE']!=''){
					$rsUser = CUser::GetByID($PROPERTY_VALUE['VALUE']);
					$arUser = $rsUser->Fetch();
					if(in_array(GROUP_MODERATORS_ID, CUser::GetUserGroup(intval($PROPERTY_VALUE['VALUE'])))){
						if($arUser['UF_SHOP']&&($arUser['UF_SHOP']!=$arFields['ID'])){
							$message = 'Пользователю '.$arUser['NAME'].' с логином '.$arUser['LOGIN'].' уже привязан магазин. Установка прав модератора для данного магазина пользователю невозможна';
							global $APPLICATION;
							$APPLICATION->throwException($message);
							return false;
						}
						elseif(!$arUser['UF_SHOP']){
							$user = new CUser;
							$fields = array(
								"UF_SHOP" => $arFields['ID']
							);
							$user->Update($PROPERTY_VALUE['VALUE'], $fields);
							$users[] = (int)$PROPERTY_VALUE['VALUE'];
						}
						else{
							$users[] = (int)$PROPERTY_VALUE['VALUE'];
						}
					}else{
						$user = new CUser;
						$group_array = CUser::GetUserGroup(intval($PROPERTY_VALUE['VALUE']));
						$group_array[] = GROUP_MODERATORS_ID;
						CUser::SetUserGroup($PROPERTY_VALUE['VALUE'], []);
						CUser::SetUserGroup($PROPERTY_VALUE['VALUE'], $group_array);
						$fields = array(
							"UF_SHOP" => $arFields['ID'],
							"UF_REST_API_TOKEN" => generateToken($arUser['LOGIN'], $arUser['NAME']),
						);
						$user->Update($PROPERTY_VALUE['VALUE'], $fields);
						$users[] = (int)$PROPERTY_VALUE['VALUE'];
					}
				}
			}
			$filter = Array
			(
				"UF_SHOP" => $arFields['ID']
			);
			$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
			while ($arUser = $rsUsers->Fetch()) {
				if(!in_array($arUser['ID'], $users)){
					$oldGroups = CUser::GetUserGroup($arUser['ID']);
					if(in_array(GROUP_MODERATORS_ID, $oldGroups)){
						$key_group = array_search(GROUP_MODERATORS_ID, $oldGroups);
						if ($key_group !== false)
							unset($oldGroups[$key_group]);
						CUser::SetUserGroup($arUser['ID'], $oldGroups);
					}
					$user = new CUser;
					$fields = array(
						"UF_SHOP" => false
					);
					$user->Update($arUser['ID'], $fields);
				}
			}
		}
	}
	// активация/деактивация товара при смене статуса
	function CheckItemStatus(&$arFields){
		if($arFields['IBLOCK_ID']==ITEMS_IBLOCK) {
			if($arFields['PROPERTY_VALUES'][127][0]['VALUE']!=89){
				$arFields['ACTIVE'] = 'N';
			}else{
				$arFields['ACTIVE'] = 'Y';
			}
		}
	}
	// присвоение уникального номера магазину
	function CheckUniqueNumber(&$arFields){
		if($arFields['IBLOCK_ID']==SHOPS_IBLOCK) {
			do{
				$unique_number = check_unique_number_shop();
			}
			while (!$unique_number);
			$qwerty = $arFields['PROPERTY_VALUES'][175];
			$temp = array_shift($qwerty);
			$new_number = $temp['VALUE'];
			$db_props = CIBlockElement::GetProperty(SHOPS_IBLOCK, $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"UNIQUE_NUMBER"));
			if($ar_props = $db_props->Fetch()){
				$old_number = $ar_props['VALUE'];
			}
			if($new_number!=$old_number){
				global $APPLICATION;
				$APPLICATION->throwException('Нельзя изменять значение "Уникальный номер"');
				return false;
			}
		}
	}
	function AddUniqueNumber(&$arFields){
		if($arFields['IBLOCK_ID']==SHOPS_IBLOCK) {
			do{
				$unique_number = check_unique_number_shop();
			}
			while (!$unique_number);
			$PROPERTY_CODE = "UNIQUE_NUMBER";
			$PROPERTY_VALUE = $unique_number;
			CIBlockElement::SetPropertyValuesEx($arFields['ID'], SHOPS_IBLOCK, array($PROPERTY_CODE => $PROPERTY_VALUE));
		}
	}
}
class UserHandlers {
	function TYPE_SEND_SMS_OnEventLogGetAuditTypes() {
		return EventLog::getType(EventLog::TYPE_SEND_SMS);
	}

	function AddDeleteToken(&$arFields){
		foreach($arFields['GROUP_ID'] as $group){
			// if($group['GROUP_ID']==GROUP_MODERATORS_ID){
				if($arFields['UF_REST_API_TOKEN']==''){
					$arFields['UF_REST_API_TOKEN'] = generateToken($arFields['LOGIN'], $arFields['NAME']);
				}
			// }
		}
	}

	function OnBeforeUserLoginHandler(&$arFields) {
		global $APPLICATION;

		// AddMessage2Log($arFields);

		// Спробуємо нормалізувати логін, може то є номер телефона
		$normalizedPhoneNumber = Normalizer::normalizePhoneNumber($arFields['LOGIN']);
		if($normalizedPhoneNumber)
		{
			// Якщо вдалося нормалізовуати логін, то може бути номером телефона
			// Пошук за номером телефона або логіном
			$filter = [
				'LOGIC' => 'OR',
				['LOGIN' => $arFields['LOGIN']],
				['PERSONAL_PHONE' => $normalizedPhoneNumber],
			];

			// Шукаємо користувача
			$dbUser = UserTable::getList([
				'select' => ['ID', 'LOGIN'],
				'filter' => $filter,
			]);

			if(!$user = $dbUser->fetch())
			{
				$APPLICATION->throwException('Пользователь не найден');
				return false;
			}

			$arFields['LOGIN'] = $user['LOGIN'];
		}
	}
}

function setOrderValue($order, $code, $value){
	$propertyCollection = $order->getPropertyCollection();
	foreach ($propertyCollection as $propertyItem) {
		if($propertyItem->getField("CODE")==$code) {
			$propertyItem->setField("VALUE", $value);
		}
	}
}
function get_all_quantity($basket){
	$quantity = 0;
	foreach ($basket as $item) {
		$quantity += $item->getQuantity();
	}
	return $quantity;
}
function get_shop_products($basket){
	foreach ($basket as $item) {
		$res = CIBlockElement::GetByID($item->getProductId());
		$ar_res = $res->GetNext();
		$db_props = CIBlockElement::GetProperty($ar_res['IBLOCK_ID'], $ar_res['ID'], array("sort" => "asc"), Array("CODE" => "PARTNER"));
		if ($ar_props = $db_props->Fetch()) {
			$shops_array[$ar_props['VALUE']][] = [
				'PRODUCT_ID' => $item->getProductId(),
				'NAME' => $item->getField('NAME'),
				'PRICE' => $item->getPrice(),
				'CURRENCY' => $item->getField('CURRENCY'),
				'QUANTITY' => $item->getQuantity()
			];
		}
	}
	return $shops_array;
}
function setDefaultFields(Main\Event $event){
	CModule::IncludeModule('iblock');
	CModule::IncludeModule('sale');
	CModule::IncludeModule('catalog');
	$order = $event->getParameter("ENTITY");
	$main_order_id = $order->getId();
	$isNew = $event->getParameter("IS_NEW");
	if($isNew){
		$user = $order->getUserId();
		$site = $order->getSiteId();
		$person = $order->getPersonTypeId();
		$basket = $order->getBasket();
		$shops_array = get_shop_products($basket);
		$quantity = get_all_quantity($basket);
		if(count($shops_array)>1) {
			$paymentIds = $order->getPaymentSystemId(); // массив id способов оплат
			$deliveryIds = $order->getDeliverySystemId(); // массив id способов доставки
			$delivery = intval($deliveryIds[0]);
			foreach ($shops_array as $shop => $items) {
				$payment_id = intval($paymentIds[0]);
				$order = Order::create($site, $user);
				$order->setPersonTypeId($person);
				$quantity = 0;
				$basket = Basket::create($site);
				foreach ($items as $item_cart) {
					$quantity+=$item_cart['QUANTITY'];
					$item = $basket->createItem('catalog', $item_cart['PRODUCT_ID']);
					$item->setFields($item_cart);
				}
				setOrderValue($order, 'PARTNER', $shop);
				setOrderValue($order, 'COUNT_OF_ITEMS', $quantity);
				$order->setBasket($basket);
				/*Доставка*/
				$shipmentCollection = $order->getShipmentCollection();
				$shipment = $shipmentCollection->createItem(
					Manager::getObjectById($delivery)  // Курьером до подъезда
				);
				$shipmentItemCollection = $shipment->getShipmentItemCollection();
				$basket = $order->getBasket();
				foreach ($basket as $basketItem)
				{
					$item = $shipmentItemCollection->createItem($basketItem);
					$item->setQuantity($basketItem->getQuantity());
				}
				/*Оплата*/
				$paymentCollection = $order->getPaymentCollection();
				$payment = $paymentCollection->createItem(
					PaySystem\Manager::getObjectById($payment_id) // Наличными при получении
				);
				$payment->setField("SUM", $order->getPrice());
				$payment->setField("CURRENCY", $order->getCurrency());
				$order->doFinalAction(true);
				$order->save();
			}
			CSaleOrder::Delete($main_order_id);
		}elseif(count($shops_array)==1){
			$shop = 0;
			foreach ($shops_array as $key=>$val){
				$shop = $key;
			}
			setOrderValue($order, 'PARTNER', $shop);
			setOrderValue($order, 'COUNT_OF_ITEMS', $quantity);
			$order->save();
		}
	}
}

// Подія змінення полів замовлення
// \Bitrix\Main\EventManager::getInstance()->addEventHandler(
// 	'sale',
// 	'OnSaleOrderSetField',
// 	'handlerOnSaleOrderSetField'
// );
// \Bitrix\Main\EventManager::getInstance()->AddEventHandler(
// 	'sale',
// 	'OnSaleStatusOrder',
// 	'handlerOnSaleStatusOrder'
// );

function handlerOnSaleOrderSetField(\Bitrix\Main\Event $event) {
	$name = $event->getParameter('NAME');
	$value = $event->getParameter('VALUE');
	$entity = $event->getParameter('ENTITY');
	// exo($name);
	if($name == 'STATUS_ID') { // Змінили Статус
		addOrdersChangelogRecord($entity->getId(), $value, $_REQUEST[notes]);
	}

	// define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
	// AddMessage2Log('orderId=' . $orderId . ' status=' . $status);
}
function handlerOnSaleStatusOrder($orderId, $status) {
	addOrdersChangelogRecord($orderId, $status, $_REQUEST[notes]);

	// define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
	// AddMessage2Log('orderId=' . $orderId . ' status=' . $status);
}

class CSalehandlers {
	public static function OnSaleComponentOrderOneStepCompleteHandler($ID, $arFields) {
		if ($arFields['ID']>0 && $arFields['USER_ID'] > 0) {
			$arFI = array();
			$rsProp = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $arFields['ID']));
			while ($arProp = $rsProp->Fetch()) {
				if ($arProp['CODE']=='NAME')
					$arFI['NAME'] = $arProp['VALUE'];
				elseif ($arProp['CODE']=='LAST_NAME')
					$arFI['LAST_NAME'] = $arProp['VALUE'];
				elseif ($arProp['CODE']=='EMAIL')
					$arFI['EMAIL'] = $arProp['VALUE'];
			}

			$FI = trim($arFI[NAME] .' '. $arFI[LAST_NAME]);
			if ($FI != '') {
				if ($arProp = CSaleOrderProps::GetList(array(), array(
					'CODE' => 'FULL_NAME',
					'PERSON_TYPE_ID' => $arFields['PERSON_TYPE_ID']))->Fetch()) {
						CSaleOrderPropsValue::Add(array(
							'ORDER_ID' => $arFields['ID'],
							'ORDER_PROPS_ID' => $arProp['ID'],
							'NAME' => $arProp['NAME'],
							'CODE' => 'FULL_NAME',
							'VALUE' => $FI,
						));
				}
				// Update user data
				$user = new CUser;
				$rsUser = $user->GetById($arFields[USER_ID]);
				$arUser = $rsUser->Fetch();
				// define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log_order.txt");
				// AddMessage2Log($arUser);
				$fields = [];
				if($arUser[NAME] == '') $fields[NAME] = $arFI[NAME];
				if($arUser[LAST_NAME] == '') $fields[LAST_NAME] = $arFI[LAST_NAME];
				if($arUser[EMAIL] != $arFI[EMAIL]) $fields[EMAIL] = $arFI[EMAIL];
				// if($arUser[LOGIN] != $arFI[EMAIL]) $fields[LOGIN] = $arFI[EMAIL];
				// AddMessage2Log($fields);
				$user->Update($arFields[USER_ID], $fields);
				// AddMessage2Log($user->LAST_ERROR);
			}
	  	}
	}

	public static function OnSaleBeforeOrderCanceledHandlers(&$order){
		if ($order->isCanceled()){
			$order->setField("STATUS_ID", 'CC');
		}
	}

}
?>