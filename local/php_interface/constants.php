<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08.12.2018
 * Time: 11:30
 */
//ID группы модераторов магазинов
define ('GROUP_MODERATORS_ID', 5);
//ID инфоблока товаров
define ('ITEMS_IBLOCK', 4);
//ID инфоблока магазинов
define ('SHOPS_IBLOCK', 2);
// ID инфоблока протокола изменения заказов
if (!defined('ORDERS_CHANGELOG_IBLOCK')) define('ORDERS_CHANGELOG_IBLOCK', 10);
// ID инфоблока слайдеров
if (!defined('CAROUSEL_IBLOCK')) define ('CAROUSEL_IBLOCK', 11);
// ID инфоблока обзоров и тестов
if (!defined('TACKLE_REVIEWS_IBLOCK')) define('TACKLE_REVIEWS_IBLOCK', 12);

if (!defined('PHONE_VERIFICATION_TABLE_NAME')) define ('PHONE_VERIFICATION_TABLE_NAME', 'ryb_phone_verification');

?>