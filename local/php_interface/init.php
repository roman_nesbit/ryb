<?php

// adds PSR-4 autoloader to the project
require_once ($_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php");

//подключение файла констант
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/constants.php"))
    require_once ($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/constants.php");
//подключение файла функций
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/functions.php"))
    require_once ($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/functions.php");
//подключение обработчиков событий
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/event_handlers.php"))
    require_once ($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/event_handlers.php");

/**
 * Module REST API Business Edition
 *
 * @package  artamonov.rest
 * @author   Денис Артамонов <artamonov.ceo@gmail.com>
 * @website  http://artamonov.biz/
 */
if (Bitrix\Main\Loader::includeModule('artamonov.rest')) \Artamonov\Rest\Foundation\Core::getInstance()->run();
?>