<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => [
		"TITLE" => [
			"PARENT" => "BASE",
			"NAME" => "Заглавие",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"REFRESH" => "N",
		],
		"VIEW" => [
			"PARENT" => "BASE",
			"NAME" => "Вид",
			"TYPE" => "LIST",
			"VALUES" => [
				"COL2" => "2 колонки",
				"COL3" => "3 колонки",
			],
			"REFRESH" => "N"
		],
		"BANNER_OR_CAROUSEL" => [
			"PARENT" => "BASE",
			"NAME" => "Первый блок",
			"TYPE" => "LIST",
			"VALUES" => [
				"BANNER" => "Баннер",
				"CAROUSEL" => "Слайдер",
			],
			"REFRESH" => "Y"
		],
	],
);


if($arCurrentValues["BANNER_OR_CAROUSEL"] == "CAROUSEL") {
	$arCarouselSections = [];
	$arOrder = ["NAME" => "ASC"];
	$arFilter = ["IBLOCK_ID" => CAROUSEL_IBLOCK, "ACTIVE" => "Y"];
	$arSelect = ["ID", "NAME"];
	$resCarouselSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
	while($arSection = $resCarouselSections->GetNext()) {
		$arCarouselSections[$arSection['ID']] = $arSection['NAME'];
	}
	
	$arComponentParameters["PARAMETERS"]["SECTION_ID"] = [
		"PARENT" => "BASE",
		"NAME" => "Раздел со слайдами",
		"TYPE" => "LIST",
		"VALUES" => $arCarouselSections,
		"MULTIPLE" => "N",
		"DEFAULT" => "",
		"REFRESH" => "N",
	];
} else {
	$arComponentParameters["PARAMETERS"]["PICTURE"] = [
		"PARENT" => "BASE",
		"NAME" => "Изображение баннера",
		"TYPE" => "FILE",
		"FD_TARGET" => "F",
		"FD_EXT" => "jped,png",
		"FD_UPLOAD" => true,
		"FD_USE_MEDIALIB" => true,
		"FD_MEDIALIB_TYPES" => Array('image'),
		"MULTIPLE" => "N",
		"REFRESH" => "N",
	];
	$arComponentParameters["PARAMETERS"]["PICTURE_ALT"] = [
		"PARENT" => "BASE",
		"NAME" => "Alt текст изображения",
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"REFRESH" => "N",
	];
	$arComponentParameters["PARAMETERS"]["LINK"] = [
		"PARENT" => "BASE",
		"NAME" => "Ссылка баннера",
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"REFRESH" => "N",
	];
}

$arComponentParameters["PARAMETERS"]["PRODUCTS"] = [
	"PARENT" => "BASE",
	"NAME" => "ID Товаров",
	"TYPE" => "STRING",
	"MULTIPLE" => "Y",
	"REFRESH" => "N",
];

?>