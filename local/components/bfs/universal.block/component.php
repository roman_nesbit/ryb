<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams['PRODUCTS'] = array_filter($arParams['PRODUCTS'], function($a) {
	return $a > 0;
});

if(isset($arParams['PRODUCTS']) && count($arParams['PRODUCTS'])) {
	$arSelect = ["ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DETAIL_PICTURE"];
	$arFilter = [
		"IBLOCK_ID" => ITEMS_IBLOCK,
		"ACTIVE" => "Y",
		"ID" => $arParams['PRODUCTS'],
	];
	$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
	$arProducts = [];
	while($obj = $res->GetNext()) {
		$arProducts[] = [
			'ID' => $obj['ID'],
			'NAME' => $obj['NAME'],
			'LINK' => $obj['DETAIL_PAGE_URL'],
			'PICTURE' => ($obj['PREVIEW_PICTURE'] ? CFile::GetPath($obj['PREVIEW_PICTURE']) : ($obj['DETAIL_PICTURE'] ? CFile::GetPath($obj['DETAIL_PICTURE']) : false)),
		];
	}

	uasort($arProducts, function($a, $b) use ($arParams) {
		return array_search($a['ID'], $arParams['PRODUCTS']) > array_search($b['ID'], $arParams['PRODUCTS']);
	});
	$arResult['ITEMS'] = $arProducts;
}

$this->IncludeComponentTemplate();
?>