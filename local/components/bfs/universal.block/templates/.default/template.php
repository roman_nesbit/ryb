<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); 
?><section class="productsSection">
	<div class="titleLine">
		<h3><?=$arParams['TITLE']?></h3>
	</div>
	<div class="productsView2 fullWidthMobile <?=($arParams['VIEW'] == 'COL3' ? 'treeCellsView' : '')?>">
		<div class="leftCell"><?
			if($arParams['BANNER_OR_CAROUSEL'] == 'BANNER') {
				$APPLICATION->IncludeComponent(
					"bfs:single.banner.block", 
					".default", 
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"PICTURE" => $arParams['PICTURE'],
						"PICTURE_ALT" => $arParams['PICTURE_ALT'],
						"LINK" => $arParams['LINK'],
					),
					false
				);
			} else {
				$APPLICATION->IncludeComponent(
					"bfs:carousel.block", 
					".default", 
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"SECTION_ID" => $arParams['SECTION_ID'],
					),
					false
				);
			}
			?>
		</div>

		<div class="rightCell"><?
			switch(count($arResult['ITEMS'])) {
				case 1:
					?><div class="prodCell large">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][0]['LINK']?>"><?=$arResult['ITEMS'][0]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][0]['LINK']?>"><img src="<?=$arResult['ITEMS'][0]['PICTURE']?>" alt="<?=$arResult['ITEMS'][0]['NAME']?>"></a>
						</div>
					</div><?
					break;
				case 2:
					?><div class="prodCell large">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][0]['LINK']?>"><?=$arResult['ITEMS'][0]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][0]['LINK']?>"><img src="<?=$arResult['ITEMS'][0]['PICTURE']?>" alt="<?=$arResult['ITEMS'][0]['NAME']?>"></a>
						</div>
					</div>
					<div class="prodCell large">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][1]['LINK']?>"><?=$arResult['ITEMS'][1]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][1]['LINK']?>"><img src="<?=$arResult['ITEMS'][1]['PICTURE']?>" alt="<?=$arResult['ITEMS'][1]['NAME']?>"></a>
						</div>
					</div><?
					break;
				case 3:
					?><div class="prodCell large">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][0]['LINK']?>"><?=$arResult['ITEMS'][0]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][0]['LINK']?>"><img src="<?=$arResult['ITEMS'][0]['PICTURE']?>" alt="<?=$arResult['ITEMS'][0]['NAME']?>"></a>
						</div>
					</div>
					<div class="twoCells">
						<div class="prodCell">
							<h2 class="name"><a href="<?=$arResult['ITEMS'][1]['LINK']?>"><?=$arResult['ITEMS'][1]['NAME']?></a></h2>
							<div class="img">
								<a href="<?=$arResult['ITEMS'][1]['LINK']?>"><img src="<?=$arResult['ITEMS'][1]['PICTURE']?>" alt="<?=$arResult['ITEMS'][1]['NAME']?>"></a>
							</div>
						</div>
						<div class="prodCell">
							<h2 class="name"><a href="<?=$arResult['ITEMS'][2]['LINK']?>"><?=$arResult['ITEMS'][2]['NAME']?></a></h2>
							<div class="img">
								<a href="<?=$arResult['ITEMS'][2]['LINK']?>"><img src="<?=$arResult['ITEMS'][2]['PICTURE']?>" alt=""></a>
							</div>
						</div>
					</div><?
					break;
				case 4:
				?><div class="twoCells">
					<div class="prodCell">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][0]['LINK']?>"><?=$arResult['ITEMS'][0]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][0]['LINK']?>"><img src="<?=$arResult['ITEMS'][0]['PICTURE']?>" alt="<?=$arResult['ITEMS'][0]['NAME']?>"></a>
						</div>
					</div>
					<div class="prodCell">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][1]['LINK']?>"><?=$arResult['ITEMS'][1]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][1]['LINK']?>"><img src="<?=$arResult['ITEMS'][1]['PICTURE']?>" alt="<?=$arResult['ITEMS'][1]['NAME']?>"></a>
						</div>
					</div>
				</div>
				<div class="twoCells">
					<div class="prodCell">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][2]['LINK']?>"><?=$arResult['ITEMS'][2]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][2]['LINK']?>"><img src="<?=$arResult['ITEMS'][2]['PICTURE']?>" alt="<?=$arResult['ITEMS'][2]['NAME']?>"></a>
						</div>
					</div>
					<div class="prodCell">
						<h2 class="name"><a href="<?=$arResult['ITEMS'][3]['LINK']?>"><?=$arResult['ITEMS'][3]['NAME']?></a></h2>
						<div class="img">
							<a href="<?=$arResult['ITEMS'][3]['LINK']?>"><img src="<?=$arResult['ITEMS'][3]['PICTURE']?>" alt="<?=$arResult['ITEMS'][3]['NAME']?>"></a>
						</div>
					</div>
				</div><?
					break;
				default:
					?>Товаров должно быть от 2 до 4<?
					break;
			}?>
		</div>
	</div>
</section>
