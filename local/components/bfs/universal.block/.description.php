<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Универсальный блок",
	"DESCRIPTION" => "Выводит универсальный блок со слайдером или баннером и товарами",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "universal_block",
			"NAME" => "Блоки баннеров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>