<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок товаров с фильтром",
	"DESCRIPTION" => "Выводит блок товаров с заданным фильтром в кол-ве 4/8/12/16 и с тайтлом",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "catalog_filtered_block",
			"NAME" => "Блоки товаров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>