<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок обзоров",
	"DESCRIPTION" => "Выводит блок обзоров в кол-ве 4/8/12/16 и с тайтлом",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "tackle_reviews_block",
			"NAME" => "Блоки обзоров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>