<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Баннерный блок содной ссылкой",
	"DESCRIPTION" => "Выводит блок с одной ссылкой и изображением",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "single_banner_block",
			"NAME" => "Блоки баннеров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>