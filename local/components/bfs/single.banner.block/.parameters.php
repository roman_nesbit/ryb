<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$ext = 'jpeg,png';
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"PICTURE" => array(
			"PARENT" => "BASE",
			"NAME" => "Изображение",
			"TYPE" => "FILE",
			"FD_TARGET" => "F",
			"FD_EXT" => $ext,
			"FD_UPLOAD" => true,
			"FD_USE_MEDIALIB" => true,
			"FD_MEDIALIB_TYPES" => Array('image'),
			"MULTIPLE" => "N",
			"REFRESH" => "N",
		),
		"PICTURE_ALT" => array(
			"PARENT" => "BASE",
			"NAME" => "Alt текст для изображения",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"LINK" => array(
			"PARENT" => "BASE",
			"NAME" => "Линк",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>