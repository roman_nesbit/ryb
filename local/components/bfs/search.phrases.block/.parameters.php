<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тайтл",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => "Количество фраз",
			"TYPE" => "LIST",
			"VALUES" => array(10=>10, 15=>15, 20=>20),
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>