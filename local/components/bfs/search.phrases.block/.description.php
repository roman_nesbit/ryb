<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок поисковых фраз",
	"DESCRIPTION" => "Выводит блок с поисковыми фразами, которые имеют ненулевой результат",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "search_phrases_block",
			"NAME" => "Блоки поиска"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>