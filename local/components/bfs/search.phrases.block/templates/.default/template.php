<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
// exo($arResult);
?><section class="productsSection">
	<div class="titleLine">
		<h3><?=$arParams['TITLE']?></h3>
	</div>
	<nav class="list">
		<? foreach($arResult['ITEMS'] as $arColumn): ?>
			<ul>
				<? foreach($arColumn as $phrase): ?>
					<li><a href="/search/index.php?q=<?=$phrase?>"><?=$phrase?></a></li>
				<? endforeach; ?>
			</ul>
		<? endforeach; ?>
	</nav>
</section>