<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult['ITEMS'] = [1, 2, 3];
$connection = Bitrix\Main\Application::getConnection();
$sqlHelper = $connection->getSqlHelper();

$sql = "SELECT PHRASE FROM b_search_phrase WHERE RESULT_COUNT > 0 ORDER BY TIMESTAMP_X DESC LIMIT " . $arParams['COUNT'];

$recordset = $connection->query($sql);
$phrases = [];
while ($record = $recordset->fetch()) {
	$phrases[] = $record['PHRASE'];
}
$phrases = array_unique($phrases);
$arResult['ITEMS'] = array_chunk($phrases, ceil(count($phrases)/5));
$this->IncludeComponentTemplate();
?>