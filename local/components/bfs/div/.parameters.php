<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"DIV_CLASS" => array(
			"PARENT" => "BASE",
			"NAME" => "Класс контейнера",
			"TYPE" => "LIST",
			"VALUES" => array(
				"h2" => "h2",
				"h3" => "h3",
				"h4" => "h4",
				"h5" => "h5",
				"h6" => "h6", 
				"" => "без класса", 
			),
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"UPPERCASE" => array(
			"PARENT" => "BASE",
			"NAME" => "В верхнем регистре",
			"TYPE" => "CHECKBOX",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"ALIGNMENT" => array(
			"PARENT" => "BASE",
			"NAME" => "Выравнивание",
			"TYPE" => "LIST",
			"VALUES" => array(
				"left" => "left",
				"center" => "center",
				"right" => "right",
			),
			"MULTIPLE" => "N",
			"DEFAULT" => "center",
			"REFRESH" => "N",
		),
		"TEXT" => array(
			"PARENT" => "BASE",
			"NAME" => "Файл включаемой области",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>