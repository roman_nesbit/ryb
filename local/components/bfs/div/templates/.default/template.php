<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="<?=$arParams['DIV_CLASS']?> <?=($arParams['UPPERCASE'] == 'Y' ? 'text-uppercase' : '')?> text-<?=$arParams['ALIGNMENT']?>">
	<? $APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH.'/include/'.$arParams["~TEXT"].'.php',
		[],
		[
			"SHOW_BORDER" => true,
			"NAME" => "Текстовый блок",
			"MODE" => "html",
			"TEMPLATE" => "empty.php",
		]
	); ?>
</div>