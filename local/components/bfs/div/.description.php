<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок div",
	"DESCRIPTION" => "Выводит div блок с текстом внутри",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "div_blocks",
			"NAME" => "Div Блоки"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>