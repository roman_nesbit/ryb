<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок со слайдером",
	"DESCRIPTION" => "Выводит блок со слайдером из элементов указанного раздела инфоблока Слайдеры",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "carousel_block",
			"NAME" => "Блоки баннеров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>