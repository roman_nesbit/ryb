<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");
$arCarouselSections = [];
$arOrder = ["NAME" => "ASC"];
$arFilter = ["IBLOCK_ID" => CAROUSEL_IBLOCK, "ACTIVE" => "Y"];
$arSelect = ["ID", "NAME"];
$resCarouselSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
while($arSection = $resCarouselSections->GetNext()) {
	$arCarouselSections[$arSection['ID']] = $arSection['NAME'];
}
// exo($arCarouselSections);

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"SECTION_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "Раздел со слайдами",
			"TYPE" => "LIST",
			"VALUES" => $arCarouselSections,
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>