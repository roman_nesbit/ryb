<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок отфильтрованных товаров",
	"DESCRIPTION" => "Выводит блок товаров, отфильтрованных согласно фильтру",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "catalog_filtered",
			"NAME" => "Блоки товаров"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>