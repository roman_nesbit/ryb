<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if($arParams["~FILTER"]) {
	$GLOBALS['arrPrefilter'] = json_decode($arParams["~FILTER"], true);
}
// exo($GLOBALS['arrPrefilter']);
?>
<section class="productsPage"><? 
	if($arParams['TITLE']): 
?>
        <h1 class="pageTitle"><?=$arParams['TITLE']?></h1>
<?
	endif;

	if($_REQUEST['q'] || $arParams['~FILTER']):
		?><div class="productListContainer"><?
			// exo($arParams);
			if ($arParams['SHOW_FILTER'] == 'Y') {
				if(isset($_REQUEST['SMART_FILTER_PATH'])) {
					$APPLICATION->AddChainItem('Фильтр');
				}

				$arFieldsToShow = explode("\r\n", \Bitrix\Main\Config\Option::get("grain.customsettings", "FIELDS_TO_SHOW"));

				$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"catalog",
					array(
						"IBLOCK_TYPE" => "catalog",
						"IBLOCK_ID" => ITEMS_IBLOCK,
						"SECTION_ID" => "",
						"PREFILTER_NAME" => "arrPrefilter",
						"FILTER_NAME" => "arrFilter",
						"PRICE_CODE" => [
							0 => (in_array("UAH", $arFieldsToShow) ? "UAH" : false),
						],
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => 3600,
						"CACHE_GROUPS" => "Y",
						"SAVE_IN_SESSION" => "N",
						"FILTER_VIEW_MODE" => "VERTICAL",
						"XML_EXPORT" => "N",
						"SECTION_TITLE" => "NAME",
						"SECTION_DESCRIPTION" => "DESCRIPTION",
						'HIDE_NOT_AVAILABLE' => "L",
						"TEMPLATE_THEME" => "",
						'CONVERT_CURRENCY' => "Y",
						'CURRENCY_ID' => "UAH",
						"SEF_MODE" => "Y",
						"SEF_RULE" => "/search/filter/#SMART_FILTER_PATH#/?q=" . $_REQUEST[q] . '&s=',
						"SMART_FILTER_PATH" => $_REQUEST['SMART_FILTER_PATH'],
						"PAGER_PARAMS_NAME" => "",
						"INSTANT_RELOAD" => "Y",
						"FIELDS_TO_SHOW" => $arFieldsToShow,
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
			}

			if(!isset($GLOBALS['arrFilter'])) {
				$GLOBALS['arrFilter'] = [];
			}

			if(isset($GLOBALS['arrFilter']['FACET_OPTIONS'])) {
				unset($GLOBALS['arrFilter']['FACET_OPTIONS']);
			}

			$GLOBALS['arrFilter'] = array_merge(
				$GLOBALS['arrPrefilter'],
				$GLOBALS['arrFilter']
			);

			// exo($GLOBALS['arrFilter']);
		
			$APPLICATION->IncludeComponent(
				"bitrix:catalog.section", 
				"catalog", 
				array(
					"IBLOCK_TYPE_ID" => "catalog",
					"IBLOCK_ID" => ITEMS_IBLOCK,
					"BASKET_URL" => "/personal/cart/",
					"COMPONENT_TEMPLATE" => ".default",
					"IBLOCK_TYPE" => "catalog",
					"SECTION_ID" => "",
					"SECTION_CODE" => "",
					"SECTION_USER_FIELDS" => array(),
					"ELEMENT_SORT_FIELD" => $arParams[SORT_FIELD],
					"ELEMENT_SORT_ORDER" => $arParams[SORT_ORDER],
					"ELEMENT_SORT_FIELD2" => $arParams[SORT_FIELD2],
					"ELEMENT_SORT_ORDER2" => $arParams[SORT_ORDER2],
					"FILTER_NAME" => "arrFilter",
					"INCLUDE_SUBSECTIONS" => "Y",
					"SHOW_ALL_WO_SECTION" => "Y",
					"HIDE_NOT_AVAILABLE" => "L",
					"PAGE_ELEMENT_COUNT" => $arParams[PAGE_COUNT],
					"LINE_ELEMENT_COUNT" => "4",
					"PROPERTY_CODE" => array(),
					"OFFERS_FIELD_CODE" => array(),
					"OFFERS_PROPERTY_CODE" => array(),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_ORDER" => "desc",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER2" => "desc",
					"TEMPLATE_THEME" => "site",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => array(),
					"OFFER_ADD_PICT_PROP" => "-",
					"OFFER_TREE_PROPS" => array(),
					"PRODUCT_SUBSCRIPTION" => "Y",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "Y",
					"SHOW_CLOSE_POPUP" => "N",
					"MESS_BTN_BUY" => false,
					"MESS_BTN_ADD_TO_BASKET" => false,
					"MESS_BTN_SUBSCRIBE" => false,
					"MESS_BTN_DETAIL" => false,
					"MESS_NOT_AVAILABLE" => false,
					"SECTION_URL" => "",
					"DETAIL_URL" => "",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"SEF_MODE" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"BROWSER_TITLE" => "-",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "-",
					"SET_META_DESCRIPTION" => "N",
					"META_DESCRIPTION" => "-",
					"SET_LAST_MODIFIED" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_FILTER" => "N",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRICE_CODE" => array(
						0 => "UAH",
					),
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"CONVERT_CURRENCY" => "Y",
					"USE_PRODUCT_QUANTITY" => "N",
					"PRODUCT_QUANTITY_VARIABLE" => "",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRODUCT_PROPERTIES" => array(
					),
					"OFFERS_CART_PROPERTIES" => array(
						0 => "COLOR_REF",
						1 => "SIZES_SHOES",
						2 => "SIZES_CLOTHES",
					),
					"ADD_TO_BASKET_ACTION" => "ADD",
					"PAGER_TEMPLATE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SET_STATUS_404" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"COMPATIBLE_MODE" => "N",
					"CUSTOM_FILTER" => "",
					"HIDE_NOT_AVAILABLE_OFFERS" => "N",
					"PROPERTY_CODE_MOBILE" => array(
					),
					"BACKGROUND_IMAGE" => "-",
					"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
					"ENLARGE_PRODUCT" => "STRICT",
					"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
					"SHOW_SLIDER" => "N",
					"SLIDER_INTERVAL" => "3000",
					"SLIDER_PROGRESS" => "N",
					"SHOW_MAX_QUANTITY" => "N",
					"RCM_TYPE" => "personal",
					"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
					"SHOW_FROM_SECTION" => "N",
					"DISPLAY_COMPARE" => "N",
					"USE_ENHANCED_ECOMMERCE" => "N",
					"LAZY_LOAD" => "N",
					"LOAD_ON_SCROLL" => "N",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"CURRENCY_ID" => "UAH",
					"HIDE_SECTION_DESCRIPTION" => "Y",
				),
				false
			);
		?></div><?
	else:
		echo "Не задано условие отбора товаров";
	endif;
?></section>