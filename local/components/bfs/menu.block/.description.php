<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блок меню с тайтлом",
	"DESCRIPTION" => "Выводит блок меню с тайтлом",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "menu_block",
			"NAME" => "Блоки меню"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>