<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?><h4 class="title"><? 
	if($arParams['TITLE']) echo $arParams['TITLE'];
	if($arParams['SHOW_TOGGLES'] == "Y"): 
		?><span class="_toggleFooterNav"></span><svg><use href="#arrowIcon" /></svg><? 
	endif; 
?></h4><? 
$APPLICATION->IncludeComponent(
	"bitrix:menu",
	".default",
	Array(
		"ROOT_MENU_TYPE" => $arParams['MENU_TYPE'],
		"MAX_LEVEL" => "1", 
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "A", 
		"MENU_CACHE_TIME" => "36000000", 
		"MENU_CACHE_USE_GROUPS" => "N", 
		"MENU_CACHE_GET_VARS" => "" 
	)
);?>