<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тайтл",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"MENU_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип меню",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"MENU_TEMPLATE" => array(
			"PARENT" => "BASE",
			"NAME" => "Шаблон меню",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"SHOW_TOGGLES" => array(
			"PARENT" => "BASE",
			"NAME" => "Выводить переключатели на моб.",
			"TYPE" => "CHECKBOX",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>