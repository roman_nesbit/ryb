<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = [
	"NAME" => "Блоки меню для левой панели",
	"DESCRIPTION" => "Выводит блоки меню для левой панели на статических страницах",
	"PATH" => [
		"ID" => "bfs_components",
		"NAME" => "Компоненты BFS",
		"CHILD" => [
			"ID" => "menu_static_blocks",
			"NAME" => "Блоки меню"
		]
	],
	"ICON" => "/images/icon.gif",
];
?>