<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="categoriesNavBox">
	<ul class="nav">
		<?
		foreach ($arParams['MENU_TYPES'] as $menuType) {
			$APPLICATION->IncludeComponent(
				"bitrix:menu",
				$arParams['MENU_TEMPLATE'],
				array(
					"ROOT_MENU_TYPE" => $menuType,
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_CACHE_GET_VARS" => ""
				)
			);
		}
		?>
	</ul>
	<div class="showMoreBtn">
		<button class="_showMoreCategories">
			<span class="showMoreTxt">Смотреть все категории</span>
			<span class="showLessTxt">Свернуть</span>
			<span class="arrows">
				<svg>
					<use href="#arrowIcon" />
				</svg>
				<svg>
					<use href="#arrowIcon" />
				</svg>
			</span>
		</button>
	</div>
</div>