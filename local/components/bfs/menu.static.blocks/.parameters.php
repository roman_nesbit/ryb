<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"MENU_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип меню",
			"TYPE" => "STRING",
			"MULTIPLE" => "Y",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
		"MENU_TEMPLATE" => array(
			"PARENT" => "BASE",
			"NAME" => "Шаблон меню",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "",
			"REFRESH" => "N",
		),
	),
);
?>