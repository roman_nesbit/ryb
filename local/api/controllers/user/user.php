<?php

namespace Local\Api\Controllers\User;


use Bitrix\Main\Loader;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\Shipment;
use Bitrix\Sender\ContactTable;
use CUser;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\PostOfficeView;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\SettlementView;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Checkout\DetailsService;
use Rybalka\Marketplace\Service\Checkout\VirtualOrderBuilder;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;

class User
{
	use RestControllerTrait;

	private $detailsService;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    public function __construct()
	{
		Loader::includeModule('main');
		Loader::includeModule('sale');
		Loader::includeModule('iblock');
		Loader::includeModule('sender');

		$this->detailsService = new DetailsService();
		$this->settlementsService = new SettlementsService();
		$this->postOfficesService = new PostOfficesService();
	}

    public function getProfile()
    {
        $this->restResponse(function() {

           $this->assertAuthenticatedBySession();

            // Дані авторизованого користувача
            $arUser = $_SESSION['SESS_AUTH'];

            // Шукаємо, чи підписаний email на розсилки
            $senderContactTableData = ContactTable::getRow([
                'select' => [
                    'CODE'
                ],
                'filter' => [
                    'CODE' => $arUser['EMAIL']
                ],
            ]);

            // Шукаємо місто та відділення доставки
            $arFilter = ['ID' => $arUser['USER_ID']];
            $arParameters = [
                'SELECT' => ['UF_CITY_ID', 'UF_NP_OFFICE_ID'],
                'FIELDS' => ['ID', 'PERSONAL_PHONE'],
            ];
            $arRes = CUser::GetList($by, $order, $arFilter, $arParameters);
            $settlementView = null;
            $postOfficeView = null;

            if ($userData = $arRes->Fetch())
            {
                if($userData['UF_CITY_ID'])
                {
                    $settlement = $this->settlementsService->get($userData['UF_CITY_ID']);
                    $settlementView = $settlement
                        ? (new SettlementView($settlement))->render()
                        : null;
                }

                if($userData['UF_NP_OFFICE_ID'])
                {
                    $postOffice = $this->postOfficesService->get($userData['UF_NP_OFFICE_ID']);
                    $postOfficeView = $postOffice
                        ? (new PostOfficeView($postOffice))->render()
                        : null;
                }

                if($userData['PERSONAL_PHONE'])
                {
                    $arUser['PERSONAL_PHONE'] = $userData['PERSONAL_PHONE'];
                }
            }

            return [
                'profile' => [
                    'name' => $arUser['FIRST_NAME'],
                    'surname' => $arUser['LAST_NAME'],
                    'phoneNumber' => $arUser['PERSONAL_PHONE'],
                    'email' => $arUser['EMAIL'],
                    'maillistSubscriber' => ($senderContactTableData['CODE'] === $arUser['EMAIL']),
                ],
                'delivery' => [
                    'city' => $settlementView,
                    'npOffice' => $postOfficeView,
                ],
            ];

        });
    }

	public function getShoppingCart()
	{
	    $this->restResponse(function() {

	        $this->restoreSessionOrFail();

	        if (is_null($_SESSION['SALE_USER_ID'])) {
	            throw new BadRequestException('No shopping cart associated with the user session');
            }

            $virtualOrderBuilder = VirtualOrderBuilder::get(
                $_SESSION['SALE_USER_ID'],
                $this->detailsService->getUserId()
            );

            if (!is_null($this->detailsService->getPaymentMethodId())) {
                $virtualOrderBuilder->withPaymentService($this->detailsService->getPaymentMethodId());
            }

            if (!is_null($this->detailsService->getDeliveryMethodCode())) {
                $virtualOrderBuilder->withDeliveryMethod($this->detailsService->getDeliveryMethodCode());
            }

            $virtualOrder = $virtualOrderBuilder->build();

            $deliveryCosts = $this->extractDeliveryCosts($virtualOrder);
            $paymentCosts = $this->extractPaymentCosts($virtualOrder);

            $theSubtotal = $virtualOrder->getBasket()->getBasePrice();
            $theTotal = $virtualOrder->getBasket()->getPrice();
                // + ($deliveryCosts['cost'] ?? 0)
                // + ($paymentCosts['cost'] ?? 0);
            $theDiscountPrice = $theSubtotal - $theTotal;

			$basketItemsOrderable = $virtualOrder->getBasket()->getOrderableItems();
			$arProducts = [];

			foreach($basketItemsOrderable as $item)
			{

				// находим свойства товара
				$res = \CIBlockElement::GetList(false,
					[
						'ID' => $item->getProductId(),
					],
					false,
					false,
					[
						'ID',
						'IBLOCK_ID',
						'PREVIEW_PICTURE',
						'DETAIL_PICTURE',
						'DETAIL_PAGE_URL',
					]
				);
				$arProduct = $res->GetNext();
				
				$arProducts[] = [
					'id' => $item->getId(),
					'name' => $item->getField('NAME'),
					'image' => \CFile::GetPath($arProduct['PREVIEW_PICTURE'] ? $arProduct['PREVIEW_PICTURE'] : $arProduct['DETAIL_PICTURE']),
					'amount' => $item->getQuantity(),
					'originalPrice' => [
						'formatted' => CurrencyFormat($item->getBasePrice(), 'UAH'),
                        'value' => $item->getBasePrice(),
                        'currency' => 'UAH'
					],
					'price' => [
						'formatted' => CurrencyFormat($item->getPrice(), 'UAH'),
                        'value' => $item->getPrice(),
                        'currency' => 'UAH'
					],
					'total' => [
						'formatted' => CurrencyFormat($item->getFinalPrice(), 'UAH'),
                        'value' => $item->getFinalPrice(),
                        'currency' => 'UAH'
					],
				];
			}
			//Sort items in basket
            sortBasketItemsByName($arProducts, true);
            $arProducts = array_values($arProducts);
			return [
                'products' => $arProducts,
                'details' => [
                    'discount' => $theDiscountPrice ? [
                        'formatted' => CurrencyFormat($theDiscountPrice, 'UAH'),
                        'amount' => $theDiscountPrice,
                        'currency' => 'UAH'
                    ] : null,
                    'deliveryCost' => $deliveryCosts,
                    'paymentCost' => $paymentCosts,
                    'subtotal' => [
                        'formatted' => CurrencyFormat($theSubtotal, 'UAH'),
                        'amount' => $theSubtotal,
                        'currency' => 'UAH'
                    ],
                    'total' => [
                        'formatted' => CurrencyFormat($theTotal, 'UAH'),
                        'amount' => $theTotal,
                        'currency' => 'UAH'
                    ],
                ],
			];
        });
	}

	private function extractPaymentCosts(Order $virtualOrder)
    {
        foreach ($virtualOrder->getPaymentCollection() as $payment) {

            /**
             * @var Payment $payment
             */
            $paymentService = $payment->getPaySystem();
            $amount = $paymentService->getPaymentPrice($payment) ?? 0;
            $currency = 'UAH';

            return [
                'formatted' => CurrencyFormat($amount, $currency),
                'amount' => $amount,
                'currency' => $currency,
                'details' => $paymentService->getField('DESCRIPTION') ?? ''
            ];
        }

        return null;
    }

    private function extractDeliveryCosts(Order $virtualOrder)
    {
        foreach ($virtualOrder->getShipmentCollection() as $shipment) {

            if ($shipment->isSystem()) {
                continue;
            }

            $amount = $shipment->getPrice();
            $currency = 'UAH';

            /**
             * @var Shipment $shipment
             */
            return [
                'formatted' => CurrencyFormat($amount, $currency),
                'amount' => $amount,
                'currency' => $currency,
                'details' => getDeliveryDescription($shipment->getField('DELIVERY_ID'), $amount) ?? '',
            ];
        }

        return null;
    }
}