<?php

namespace Local\Api\Controllers\User;

use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;
use Bitrix\Main\UserGroupTable;
use CUser;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\ForbiddenException;
use Rybalka\Marketplace\Http\Exception\UnauthorizedException;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Util\Phone\Normalizer;
use Rybalka\Marketplace\Util\Phone\VerificationTable;

class Auth
{
	const LOGIN_PARAM_NAME = 'login';
	const PASSWORD_PARAM_NAME = 'password';
	const SCOPE_PARAM_NAME = 'scope';

	use RestControllerTrait;

	private $allowedScopes = ['user', 'moderator'];

	public function __construct()
	{
		Loader::includeModule('main');
	}

	public function login() {
		$this->restResponse(function () {
			return $this->_login();
		});
	}

	protected function _login()
	{
		global $USER;

		$USER = new CUser();
		$this->assertJsonParams(self::LOGIN_PARAM_NAME, self::PASSWORD_PARAM_NAME);

		$login = $this->json(self::LOGIN_PARAM_NAME);
		$password = $this->json(self::PASSWORD_PARAM_NAME);
		$scope = $this->json(self::SCOPE_PARAM_NAME) ?? 'moderator';

		if($scope && !in_array($scope, $this->allowedScopes))
		{
			throw new BadRequestException('Invalid value of the scope parameter');
		}

        if($USER->IsAuthorized() && $USER->GetLogin() != $login)
        {
            $USER->Logout();
        }

		// Спробуємо нормалізувати логін, може то є номер телефона
		$phoneNumberFromLogin = Normalizer::normalizePhoneNumber($login);

        $filter = [
            ['LOGIN' => $login],
        ];

        if ($phoneNumberFromLogin) {
            $filter = [
                'LOGIC' => 'OR',
                ['PERSONAL_PHONE' => $phoneNumberFromLogin],
            ];
        }

		// Шукаємо користувача
		$dbUser = UserTable::getList([
			'select' => ['ID', 'NAME', 'LAST_NAME', 'LOGIN', 'UF_REST_API_TOKEN', 'PASSWORD'],
			'filter' => $filter,
		]);

		if(!$user = $dbUser->fetch())
		{
			throw new UnauthorizedException('Login/password pair not found');
		}

        $loginResult = false;

        if ($phoneNumberFromLogin && VerificationTable::isValidVerificationToken($password)) {
            $phoneNumber = VerificationTable::getVerifiedPhoneNumber($password);

            if ($phoneNumber == $phoneNumberFromLogin) {
                $loginResult = $USER->Login($user['LOGIN'], $user['PASSWORD'], "N", "N");
            }
        } else {
            $loginResult = $USER->Login($user['LOGIN'], $password);
        }

		if ($loginResult !== true)
		{
			throw new UnauthorizedException('Login/password pair not found');
		}

        if($scope == 'moderator')
        {
            // Перевіримо, чи є наш користувач в групі модераторів
            if(!UserGroupTable::getCount([
                'USER_ID' => $user['ID'],
                'GROUP.STRING_ID' => $scope,
            ]))
            {
                throw new ForbiddenException('Scope verification failed');
            }
        }

		return [
			'token' => $user['UF_REST_API_TOKEN']
		];
	}
}