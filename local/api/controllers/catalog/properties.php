<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20.11.2018
 * Time: 16:59
 */

namespace Local\Api\Controllers\Catalog;


use Bitrix\Main\Loader;
use CFile;
use CIBlockPropertyEnum;
use CIBlockSectionPropertyLink;
use CIBlockProperty;

class Properties
{
    // Время хранения кэша
    private $cacheTtl = 604800;
    // Директория хранения кэша относительно /bitrix/cache
    private $cacheDir = 'catalog/properties';
    public function __construct()
    {
        Loader::includeModule('iblock');
    }
    public function show()
    {
        $types = [
            'S'=> 'string',
            'L'=> 'list',
            'E'=> 'element',
            'N'=> 'number',
            'F'=> 'file',
        ];
        $break_properties = [
            'SKU',
            'PARTNER',
            'STATUS',
            'MORE_PHOTO'
        ];
        cache()->clear($this->cacheDir);
        //Переводим все ключи в верхний регистр
        $arParams = array_change_key_case(request()->get(), CASE_UPPER);
        if($arParams['CATEGORY']) {
            $response['status'] = 200;
            $array = CIBlockSectionPropertyLink::GetArray(4, intval($arParams['CATEGORY']));
            //$response['param'] = intval($arParams['CATEGORY']);
            $response['items']['params'][] = [
                'label'=> 'Основное изображение',
                'name' => 'picture',
                'type' => 'image',
                'multiple' => false,
                'hidden' => false,
            ];
            $response['items']['params'][] = [
                'label'=> 'Дополнительные изображения',
                'name' => 'more_pictures',
                'type' => 'image',
                'multiple' => true,
                'hidden' => false,
            ];
            $response['items']['params'][] = [
                'label'=> 'Описание товара',
                'name' => 'description',
                'type' => 'string',
                'multiple' => false,
                'hidden' => false,
            ];
            $response['items']['params'][] = [
                'label'=> 'Цена',
                'name' => 'price',
                'type' => 'number',
                'multiple' => false,
                'hidden' => false,
            ];
            $response['items']['params'][] = [
                'label'=> 'Количество товара',
                'name' => 'quantity',
                'type' => 'number',
                'multiple' => false,
                'hidden' => false,
            ];
            foreach ($array as $key => $one) {
                $res = CIBlockProperty::GetByID($key);
                if ($ar_res = $res->GetNext()) {
                    if(in_array($ar_res['CODE'], $break_properties))
                        continue;
                    $prop_temp = [];
                    $prop_temp['label'] = $ar_res['NAME'];
                    $prop_temp['name'] = strtolower($ar_res['CODE']);
                    $prop_temp['type'] = $types[$ar_res['PROPERTY_TYPE']];
                    $prop_temp['multiple'] = $ar_res['MULTIPLE']=="Y"?true:false;
                    $prop_temp['hidden'] = false;
                    if ($ar_res['PROPERTY_TYPE'] == 'L') {
                        $property_enums = CIBlockPropertyEnum::GetList(Array("SORT" => "ASC", "ID" => "ASC"), Array("IBLOCK_ID" => 4, "CODE" => $ar_res['CODE']));
                        while ($enum_fields = $property_enums->GetNext()) {
                            $prop_temp['variants'][] = [
                                'id' => intval($enum_fields["ID"]),
                                'label' => $enum_fields["VALUE"]
                            ];
                        }
                    }
                }
                $response['items']['options'][] = $prop_temp;
            }
        }
        else{
            $response['status'] = 402;
            $response['message'] = "Неверные параметры";
        }
        // Запишем информацию в Журнал: Запрос/Ответ
        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        // Возвращаем результат клиенту
        response()->json($response, $response['status'], JSON_UNESCAPED_UNICODE);
    }
}