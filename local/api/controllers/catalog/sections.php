<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20.11.2018
 * Time: 16:59
 */

namespace Local\Api\Controllers\Catalog;


use Bitrix\Main\Loader;
use CFile;
use CCatalogProduct;
use CCatalog;
use CPrice;
use CIBlockElement;
use CIBlockSection;
use CIBlockPropertyEnum;

class Sections
{
    // Время хранения кэша
    private $cacheTtl = 604800;
    // Директория хранения кэша относительно /bitrix/cache
    private $cacheDir = 'catalog/categories';

    public function __construct()
    {
        Loader::includeModule('iblock');
    }
    public function show()
    {
        cache()->clear($this->cacheDir);
        //Переводим все ключи в верхний регистр
        $arParams = array_change_key_case(request()->get(), CASE_UPPER);
        if($arParams['ID']){
            $response['status'] = '200';
            $arFilter = [
                'IBLOCK_ID' => 4,
                'ACTIVE' => 'Y'
            ];
            if($arParams['ID'])
                $arFilter['ID'] = $arParams['ID'];
            $arSelect = [
                'ID',
                'NAME',
                'DEPTH_LEVEL',
                'IBLOCK_SECTION_ID'
            ];
            $array = [];
            $db_list = CIBlockSection::GetList(Array(), $arFilter, true, $arSelect);
            while($ar_result = $db_list->GetNext())
            {
                foreach ($ar_result as $id=>$value){
                    if($id[0]!='~'&&$value){
                        if($id=='ID'||$id=='IBLOCK_SECTION_ID')
                            $array[$ar_result['ID']][strtolower($id)] = intval($value);
                        else
                            $array[$ar_result['ID']][strtolower($id)] = $value;
                    }
                }
            }
            $parent_array = [];
            foreach ($array as $key => $item) {
                $parent_array[] = $item;
            }
            $response['items'] = $parent_array;
            // Запишем информацию в Журнал: Запрос/Ответ
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            // Возвращаем результат клиенту
            response()->json($response, 200, JSON_UNESCAPED_UNICODE);
        }

        else{
            $response['status'] = 200;
            $arFilter = [
                'IBLOCK_ID' => 4,
                'ACTIVE' => 'Y',
            ];
            $arSelect = [
                'ID',
                'NAME',
                'DEPTH_LEVEL',
                'IBLOCK_SECTION_ID'
            ];
            $array = [];
            $db_list = CIBlockSection::GetList(Array(), $arFilter, true, $arSelect);
            while($ar_result = $db_list->GetNext())
            {
                foreach ($ar_result as $id=>$value){
                    if($id[0]!='~'&&$value){
                        if($id=='ID'||$id=='IBLOCK_SECTION_ID')
                            $array[$ar_result['ID']][strtolower($id)] = intval($value);
                        else
                            $array[$ar_result['ID']][strtolower($id)] = $value;
                    }
                }
            }
            $parent_array = [];
            foreach ($array as $key => $item) {
                if($item['depth_level']==1)
                    $parent_array[] = $item;
            }
            foreach ($array as $key => $item) {
                if($item['depth_level']==2)
                    foreach ($parent_array as $key => $item2) {
                        if($item['iblock_section_id']==$item2['id'])
                            $parent_array[$key]['children'][] =  $item;
                    }
                //$parent_array[] = $item;
            }
            $response['items'] = $parent_array;
            // Запишем информацию в Журнал: Запрос/Ответ
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            // Возвращаем результат клиенту
            response()->json($response, 200, JSON_UNESCAPED_UNICODE);
        }

    }

}