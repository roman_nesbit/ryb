<?php

namespace Local\Api\Controllers\Catalog;


use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Catalog\CategoryService;

class Categories
{
    use RestControllerTrait;

    private $categoryService;

    public function __construct()
    {
        $this->categoryService = new CategoryService();
    }

    public function listCategories()
    {
        $this->legacyRestResponse(function() {

            $categories = $this->categoryService->getActiveCategories();
            return [
                'items' => $categories->toArray()
            ];
        });
    }

}