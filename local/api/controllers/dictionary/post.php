<?
namespace Local\Api\Controllers\Dictionary;

use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Model\Dictionary\Post\PostOffice;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\PostOfficeView;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;


class Post
{
    use RestControllerTrait;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    public function __construct()
    {
        $this->settlementsService = new SettlementsService();
        $this->postOfficesService = new PostOfficesService();
    }

    public function listOfficesAction()
    {
        $this->restResponse([$this, 'listOffices']);
    }

    public function legacyListOfficesAction()
    {
        $this->query()->set('settlementId', $this->query()->get('city_id'));
        $this->restResponse([$this, 'listOffices']);
    }

    private function listOffices()
    {
        $this->assertGetParams('settlementId');

        $settlementId = $this->query()->get('settlementId');
        $settlement = $this->settlementsService->get($settlementId);

        if (is_null($settlement)) {
            throw new BadRequestException('No settlement exists with such id');
        }

        $offices = $this->postOfficesService->getListFor($settlement);

        return array_map(
            function(PostOffice $postOffice) {
                return (new PostOfficeView($postOffice))->render();
            },
            $offices
        );
    }
}