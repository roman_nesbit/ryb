<?
namespace Local\Api\Controllers\Dictionary;

use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\SettlementView;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Address\StreetsService;


class Address
{
    use RestControllerTrait;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var StreetsService
     */
    private $streetsService;

    public function __construct()
    {
        $this->settlementsService = new SettlementsService();
        $this->streetsService = new StreetsService();
    }

    public function findSettlementAction()
    {
        $this->restResponse([$this, 'findSettlement']);
    }

    public function findStreetAction()
    {
        $this->restResponse([$this, 'findStreet']);
    }

    private function findStreet()
    {
        $this->assertGetParams('settlementId', 'query');

        $query = $this->query()->get('query');
        $settlementId = $this->query()->get('settlementId');

        return $this->streetsService->getSuggestions($settlementId, $query);
    }

    private function findSettlement()
    {
        $this->assertGetParams('query');
        $query = $this->query()->get('query');

        $searchResults = $this->settlementsService->searchByPartialName($query);

        $response = [];

        foreach ($searchResults as $searchResult) {
            $response[] = (new SettlementView($searchResult))->render();
        }

        return $response;
    }
}