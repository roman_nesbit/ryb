<?php

namespace Local\Api\Controllers;

use Rybalka\Marketplace\Util\Phone\VerificationTable;
use Rybalka\Marketplace\Util\Phone\Normalizer;
use Rybalka\Marketplace\Util\Recaptcha;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Http\Exception;
use Rybalka\Marketplace\Util\UserUtil;

class Phone_number
{
	use RestControllerTrait;

	public function SendVerificationCode()
	{		
		$this->restResponse(function()
		{
			$this->assertJsonParams('phoneNumber', 'captchaToken');

			// Перевірка каптчі
			if(!Recaptcha::isHuman($this->json('captchaToken')))
			{
				throw new Exception\UnauthorizedException('Captcha verification failure. Are you a bot?');
			}

			return VerificationTable::sendCode(
				$this->json('phoneNumber'),
				\Bitrix\Main\Service\GeoIp\Manager::getRealIp()
			);
		});
	}

	public function VerifyCode()
	{
		$this->restResponse(function()
		{
			$this->assertJsonParams('verificationId', 'code');

			return VerificationTable::verify(
				$this->json('verificationId'),
				$this->json('code')
			);
		});
	}

	public function CheckTokenValidity()
	{		
		$this->restResponse(function() {

			$this->assertJsonParams('verificationToken');
			$verificationToken = $this->json('verificationToken');

			if(!VerificationTable::isValidVerificationToken($verificationToken)) {
				throw new Exception\NotFoundException('Verification code not found');
			}

			return [];
		});
	}

	public function Status()
	{
		// перевіряє, чи номер телефону зареєстрований у системі

		$this->restResponse(function()
		{
			$this->assertGetParams('phoneNumber', 'captchaToken');

			// Перевірка каптчі
			if(!Recaptcha::isHuman($this->query()->get('captchaToken')))
			{
				throw new Exception\UnauthorizedException('Captcha verification failure. Are you a bot?');
			}

			$resultNormalizePhoneNumber = Normalizer::normalizePhoneNumber($this->query()->get('phoneNumber'));

			if(!$resultNormalizePhoneNumber)
			{
				throw new Exception\BadRequestException('Invalid phone number');
			}

			// спробуємо знайти юзера за номером телефона
			return ['registered' => UserUtil::isUserByPhone($resultNormalizePhoneNumber)];
		});
	}
}