<?
namespace Local\Api\Controllers;

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Sale\PaySystem\Manager;
use Rybalka\Marketplace\Exception\InvalidStateException;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Model\Checkout\DeliveryService;
use Rybalka\Marketplace\Model\Rest\View\Checkout\DeliveryServiceView;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\PostOfficeView;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\SettlementView;
use Rybalka\Marketplace\Model\Rest\View\Dictionary\Address\StreetView;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Checkout\DeliveryServices;
use Rybalka\Marketplace\Service\Checkout\DetailsService;
use Rybalka\Marketplace\Service\Checkout\DetailsValidationService;
use Rybalka\Marketplace\Service\Checkout\PaymentOptionsService;
use Rybalka\Marketplace\Service\Checkout\UserRegistrationService;
use Rybalka\Marketplace\Service\Checkout\VirtualOrderBuilder;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Address\StreetsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;
use Rybalka\Marketplace\Util\InvoicePdf;
use Bitrix\Main\Config\Option;

class Checkout
{
    use RestControllerTrait;

    /**
     * @var DetailsService
     */
    private $checkoutDetailsService;

    /**
     * @var PaymentOptionsService
     */
    private $paymentOptionService;

    /**
     * @var DetailsValidationService
     */
    private $detailsValidationService;

    /**
     * @var UserRegistrationService
     */
    private $userRegistrationService;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    /**
     * @var DeliveryServices
     */
    private $deliveryServices;

    /**
     * @var StreetsService
     */
    private $streetsService;

    public function __construct()
    {
        Loader::includeModule('sale');

        $this->checkoutDetailsService = new DetailsService();
        $this->paymentOptionService = new PaymentOptionsService();
        $this->detailsValidationService = new DetailsValidationService();
        $this->userRegistrationService = new UserRegistrationService();
        $this->settlementsService = new SettlementsService();
        $this->postOfficesService = new PostOfficesService();
        $this->deliveryServices = new DeliveryServices();
        $this->streetsService = new StreetsService();
    }

    public function getDetails()
    {
        $this->restResponse(function() {

            $this->restoreSessionOrFail();
            return $this->getCheckoutDetails();
        });
    }

    public function updateDetailsAction()
    {
        $this->restResponse([$this, 'updateDetails']);
    }

    public function updateDetails()
    {
        $this->restoreSessionOrFail();

        $saleUserId = $_SESSION['SALE_USER_ID'];

        if (empty($saleUserId)) {
            throw new InvalidStateException('No shopping cart may be loaded for the session');
        }

        $this->detailsValidationService->validate(
            $this->json()
        );

        $this->callIfFieldNotEmpty('name', [$this->checkoutDetailsService, 'setName']);
        $this->callIfFieldNotEmpty('surname', [$this->checkoutDetailsService, 'setSurname']);
        $this->callIfFieldNotEmpty(
            'subscribeForNewsletter',
            [$this->checkoutDetailsService, 'setIsMailListSubscriber']
        );

        $this->callIfFieldNotEmpty(
            'phone.verificationToken',
            [$this->checkoutDetailsService, 'setVerificationToken']
        );

        $this->callIfFieldNotEmpty(
            'email.value',
            [$this->checkoutDetailsService, 'setEmail']
        );

        $this->callIfFieldNotEmpty(
            'delivery',
            function ($data) use ($saleUserId)
            {
                $this->checkoutDetailsService->setDelivery($data, $saleUserId);
            }
        );

        $this->callIfFieldNotEmpty(
            'payment.id',
            [$this->checkoutDetailsService, 'setPaymentMethodId']
        );

        $this->callIfFieldNotEmpty(
            'note',
            [$this->checkoutDetailsService, 'setNote']
        );

        $response = $this->getCheckoutDetails();

        if (!is_null($this->json('placeOrder'))) {
            $this->detailsValidationService->assertDetailsComplete(
                $this->getCheckoutDetails()
            );

            $userId = $this->checkoutDetailsService->getUserId() ??
                $this->userRegistrationService->register($this->checkoutDetailsService);

            $orderBuilder = VirtualOrderBuilder::get($saleUserId, $userId)
                ->withDeliveryMethod($this->checkoutDetailsService->getDeliveryMethodCode())
                ->withDeliveryDetails($this->checkoutDetailsService->getDeliveryDetails())
                ->withPaymentService($this->checkoutDetailsService->getPaymentMethodId())
                ->withCustomerName($this->checkoutDetailsService->getName())
                ->withCustomerSurname($this->checkoutDetailsService->getSurname())
                ->withCustomerPhoneNumber($this->checkoutDetailsService->getPhoneNumber())
                ->withNote($this->checkoutDetailsService->getNote());

            if ($this->checkoutDetailsService->getEmail() !== null) {
                $orderBuilder->withCustomerEmail(
                    $this->checkoutDetailsService->getEmail()
                );
            };

            $order = $orderBuilder->build();

            $order->doFinalAction(true);
            $result = $order->save();

            if (!$result->isSuccess()) {
                throw new InternalServerErrorException('Failed to place order');
            }

            $orderId = $result->getId();
            $order = Sale\Order::load($result->getId());

            $response['orderId'] = $order->getField('ACCOUNT_NUMBER');

            if (isset($response['payment']['id']) && in_array(
                $response['payment']['id'],
                explode(',', Option::get("grain.customsettings", "INVOICE_PAYSYS_IDS"))
            )) {
                // Get invoice data
                $_invoice = new InvoicePdf($orderId);
                $response['payment']['invoice'] = $_invoice
                    ->prepare()
                    ->make()
                    ->getInvoiceUrl();
            }

            $this->checkoutDetailsService
                ->saveUserFields([
                    'UF_CITY_ID' => $response['delivery']['city']['id'],
                    'UF_NP_OFFICE_ID' => $response['delivery']['npOffice']['id']
                ])
                ->reset();

        }

        return $response;
    }

    public function getDeliveryOptionsAction()
    {
        return $this->restResponse([$this, 'getDeliveryOptions']);
    }

    public function getPaymentOptions()
    {
        $this->restResponse(function() {

            $this->restoreSessionOrFail();

            $data = [];

            $theSite = Context::getCurrent()->getSite();

            if (!$theFUser = $_SESSION['SALE_USER_ID']) {
                throw new InternalServerErrorException('No shopping cart can be loaded for a user');
            }

            Sale\Compatible\DiscountCompatibility::stopUsageCompatible();
            // перевіряємо кошик
            $basket = Sale\Basket::loadItemsForFUser($theFUser, $theSite)->getOrderableItems();

            if ($basket->isEmpty()) {
                throw new BadRequestException('Empty shopping cart');
            }

            $order = Sale\Order::create($theSite);
            $order->setBasket($basket);
            // Сума кошика
            $basketSum = $order->getPrice();

            $paymentCollection = $order->getPaymentCollection();

            $paymentOptions = Manager::getList([
                'filter' => [
                    'ACTIVE' => 'Y',
                ],
                'order' => [
                    'SORT' => 'ASC',
                ]
            ]);
            while ($paymentOption = $paymentOptions->fetch()) {
                $payment = $paymentCollection->createItem();
                $payment->setFields([
                    'PAY_SYSTEM_ID' => $paymentOption['ID'],
                    'PAY_SYSTEM_NAME' => $paymentOption['NAME'],
                    'SUM' => $basketSum,
                ]);
                $service = Manager::getObjectById($paymentOption['ID']);
                $paymentPrice = $service->getPaymentPrice($payment);

                $data[] = [
                    'id' => $paymentOption['ID'],
                    'title' => $paymentOption['NAME'],
                    'price' => [
                        'formatted' => ($paymentPrice ? CurrencyFormat($paymentPrice, 'UAH') : 'бесплатно')
                    ],
                    'comment' => $paymentOption['DESCRIPTION'] != '' ? $paymentOption['DESCRIPTION'] : null,
                ];
            }

            return [
                'paymentOptions' => $data
            ];
        });
    }

    private function getDeliveryOptions()
    {
        $this->restoreSessionOrFail();

        $saleUserId = $_SESSION['SALE_USER_ID'];

        if (empty($saleUserId)) {
            throw new BadRequestException('No shopping cart may be loaded for the session');
        }

        $userId = $this->checkoutDetailsService->getUserId() ?? 0;

        $order = VirtualOrderBuilder::get($saleUserId, $userId)->build();
        $mainShipment = $order->getShipmentCollection()[0] ?? null;

        if ($mainShipment == null) {
            throw new InvalidStateException('No shipments within a Virtual Order');
        }

        return array_map(
            function(DeliveryService $deliveryService) {
                return (new DeliveryServiceView($deliveryService))->render();
            },
            $this->deliveryServices->getAvailableFor($mainShipment)
        );
    }

    private function getCheckoutDetails()
    {
        $deliveryService = $this->deliveryServices->getBy(
            $this->checkoutDetailsService->getDeliveryMethodCode()
        );

        $deliveryInfo = [];

        if (!is_null($deliveryService)) {
            $deliveryInfo['method'] = (new DeliveryServiceView($deliveryService))->render();
        }

        $deliveryData = $this->checkoutDetailsService->getDeliveryDetails();

        if (isset($deliveryData['settlement']['id'])) {
            $settlement = $this->settlementsService->get($deliveryData['settlement']['id']);
            $deliveryData['settlement'] = (new SettlementView($settlement))->render();
        }

        if (isset($deliveryData['postOffice']['id'])) {
            $postOffice = $this->postOfficesService->get($deliveryData['postOffice']['id']);
            $deliveryData['postOffice'] = (new PostOfficeView($postOffice))->render();
        }

        if (isset($deliveryData['street']['id']) && isset($deliveryData['settlement']['id'])) {
            $street = $this->streetsService->get($deliveryData['settlement']['id'], $deliveryData['street']['id']);
            $deliveryData['street'] = (new StreetView($street))->render();
        }

        if (!empty($deliveryData)) {
            $deliveryInfo['data'] = $deliveryData;
        }

        $deliveryInfo = !empty($deliveryInfo) ? $deliveryInfo : null;

        return [
            'name' => $this->checkoutDetailsService->getName(),
            'surname' => $this->checkoutDetailsService->getSurname(),
            'phone' => [
                'number' => $this->checkoutDetailsService->getPhoneNumber(),
                'verificationToken' => $this->checkoutDetailsService->getVerificationToken()
            ],
            'email' => [
                'value' => $this->checkoutDetailsService->getEmail(),
                'readOnly' => !$this->checkoutDetailsService->isEmailEditable()
            ],
            'subscribeForNewsletter' => $this->checkoutDetailsService->isMailListSubscriber(),
            'delivery' => $deliveryInfo,
            'payment' => $this->paymentOptionService->getById(
                $this->checkoutDetailsService->getPaymentMethodId()
            ),
            'note' => $this->checkoutDetailsService->getNote(),
            'orderId' => null
        ];
    }

    private function callIfFieldNotEmpty(string $fieldName, callable $callback) {
        $value = $this->json($fieldName);

        if (is_null($value)) {
            return;
        }

        if (is_array($value)) {
            $callback($value);
            return;
        }

        $value = trim($value);

        if (empty($value)) {
            return;
        }

        $callback($value);
    }
}
