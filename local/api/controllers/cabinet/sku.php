<?php

namespace Local\Api\Controllers\Cabinet;


use Rybalka\Marketplace\Model\ShopModerator;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Catalog\ProductService;

class Sku
{
    use RestControllerTrait;

    const REQUEST_PARAM_SKU = 'sku';

    /**
     * @var ProductService
     */
    private $productService;

    public function __construct()
    {
        $this->productService = new ProductService();
    }

    public function get()
    {
        $this->legacyRestResponse(function() {
            $this->assertGetParams(self::REQUEST_PARAM_SKU);
            $sku = trim($this->query()->get(self::REQUEST_PARAM_SKU));

            if (empty($sku)) {
                return [
                    'valid' => true
                ];
            }

            $product = $this->productService->loadBySkuAndShopId($sku, $this->getModerator()->getShopId());

            return [
                'valid' => is_null($product)
            ];
        });
    }
}