<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20.11.2018
 * Time: 16:59
 */

namespace Local\Api\Controllers\Cabinet;


use Bitrix\Main\Loader;
use Bitrix\Sale\Internals\StatusLangTable;
use CFile;
use CCatalogProduct;
use CCatalog;
use CPrice;
use CIBlockElement;
use CIBlockPropertyEnum;

class Statuses
{
    // Время хранения кэша
    private $cacheTtl = 604800;
    // Директория хранения кэша относительно /bitrix/cache
    private $cacheDir = 'cabinet/item';

    public function __construct()
    {
        Loader::includeModule('sale');
    }
    public function show()
    {

        // Выборка данных
        $statusResult = StatusLangTable::getList(array(

            'order' => array('STATUS.SORT'=>'ASC'),

            'filter' => array('STATUS.TYPE'=>'O','LID'=>LANGUAGE_ID),

            'select' => array('STATUS_ID','NAME'),

        ));
        while($status=$statusResult->fetch())
        {
            $response['items'][] = [
                'id'    =>  $status['STATUS_ID'],
                'label' =>  $status['NAME']
            ];
        }

        response()->json($response, 200, JSON_UNESCAPED_UNICODE);
    }
}