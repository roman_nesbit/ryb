<?php

namespace Local\Api\Controllers\Cabinet;

use Bitrix\Main\Loader;
use DateTime;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Model\Rest\Request\Order\SetStatusRequest;
use Rybalka\Marketplace\Model\Rest\Request\OrdersFilter;
use Rybalka\Marketplace\Model\Rest\View\OrderCollectionView;
use Rybalka\Marketplace\Model\Rest\View\OrderView;
use Rybalka\Marketplace\Rest\RestControllerTrait;
use Rybalka\Marketplace\Service\Order\ChangeLogService;
use Rybalka\Marketplace\Service\OrderService;

class Orders
{
    use RestControllerTrait;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var ChangeLogService
     */
    private $changeLogService;

    /**
     * @var OrderView
     */
    private $orderView;

    /**
     * @var OrderCollectionView
     */
    private $orderCollectionView;

    public function __construct()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
        Loader::includeModule('sale');

        $this->orderService = new OrderService();
        $this->orderView = new OrderView();
        $this->orderCollectionView = new OrderCollectionView();

        $this->changeLogService = new ChangeLogService();
    }

    public function get()
    {
        $this->legacyRestResponse(function()
        {
            if ($this->query()->has('id')) {
                $order = $this->orderService->getOrFail(
                    $this->query()->get('id'),
                    $this->getModerator()->getShopId()
                );

                return $this->orderView->map($order);
            }

            $request = (new OrdersFilter())
                ->setShopId($this->getModerator()->getShopId())
                ->setPage($this->query()->getInt('page'))
                ->setItemsPerPage($this->query()->getInt('per_page'))
                ->setOrderBy($this->query()->get('sort'))
                ->setSortOrder($this->query()->getAlpha('order'))
                ->setStatus($this->query()->getAlpha('orders-status'))
                ->setDateFrom($this->getDateParam('orders-date-from'))
                ->setDateTo($this->getDateParam('orders-date-to'))
                ->setFreeTextFilter($this->query()->get('filter'));

            $ordersList = $this->orderService->getByFilter($request);

            return [
                'page' => $request->getPage(),
                'per_page' => $request->getItemsPerPage(),
                'total' => $ordersList->getTotalCount(),
                'items' => $this->orderCollectionView->map($ordersList)
            ];
        });
    }

    private function getDateParam(string $paramName): ?DateTime
    {
        if (!$this->query()->has($paramName)) {
            return null;
        }

        $timestamp = $this->query()->getInt($paramName) / 1000;
        if ($timestamp < 0) {
            return null;
        }

        return new DateTime('@' . $timestamp);
    }

    public function setStatus()
    {
        $this->legacyRestResponse(function()
        {
            if (!$this->post()->has('orderId')) {
                throw new BadRequestException('Missing parameter: orderId');
            }

            if (!$this->post()->has('statusId')) {
                throw new BadRequestException('Missing parameter: statusId');
            }

            $request = (new SetStatusRequest())
                ->setOrderPublicId($this->post()->get('orderId'))
                ->setStatusId($this->post()->get('statusId'))
                ->setNote(trim($this->post()->get('note', '')));

            $this->orderService->updateStatusOrFail($request, $this->getModerator());
        });
    }

    public function addNote()
    {

    }
}