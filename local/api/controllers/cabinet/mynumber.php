<?php

namespace Local\Api\Controllers\Cabinet;


use Bitrix\Main\Loader;
use CIBlockElement;
use Rybalka\Marketplace\Model\ShopModerator;
use Rybalka\Marketplace\Rest\RestControllerTrait;

class Mynumber
{
    use RestControllerTrait;

    public function __construct()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
    }

    public function show()
    {
        $arFilter = ['ID' => $this->getModerator()->getShopId()];
        $arSelect = ['PROPERTY_UNIQUE_NUMBER'];
        $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
        $response = [];
        while ($ar_result = $res->Fetch()){
            $response['value'] = intval($ar_result['PROPERTY_UNIQUE_NUMBER_VALUE']);
        }

        response()->json($response, 400, JSON_UNESCAPED_UNICODE);
    }
}