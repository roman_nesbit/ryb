<?php

namespace Local\Api\Controllers\Cabinet;

use Bitrix\Main\Loader;
use CFile;
use CCatalogProduct;
use CCatalog;
use CPrice;
use CIBlockElement;
use CIBlockSectionPropertyLink;
use CIBlockSection;
use CIBlockProperty;
use CIBlockPropertyEnum;
use Rybalka\Marketplace\Rest\RestControllerTrait;

class Products
{
    use RestControllerTrait;

	public function generateFile($img){
		//AddMessage2Log($img);
		$folderPath = $_SERVER["DOCUMENT_ROOT"]."/upload/products/";
		$image_parts = explode(";base64,", $img);
		$file = explode('data:', $image_parts[0]);
		$image_type_aux = explode("/", $file[1]);
		$image_type = $image_type_aux[1];
		$image_base64 = base64_decode($image_parts[1]);
		$file = $folderPath . uniqid() . '.'.$image_type;
		file_put_contents($file, $image_base64);
		return $file;
	}
	public function getCategoryName($id){
		$res = CIBlockSection::GetByID($id);
		if($ar_res = $res->GetNext()){
			return $ar_res['NAME'];
		}
	}
	public function load_files($id, $param, $property){
		$arFils = array();
		$paths_array = [];
		foreach ($param as $key=>$IMAGE){

			if($IMAGE['src']){
				$qwe = explode('https://marketplace.rybalka.ua', $IMAGE['src']);
				if(substr($IMAGE['src'], 0, 5)=='https') {
					$arTmpFil = CFile::MakeFileArray($qwe[1]);
					$arFils[]=array(
						'VALUE'         => $arTmpFil,
						'DESCRIPTION'   => '',
					);
					$paths_array[] = $arTmpFil['tmp_name'];
				}
				else{
					$extra_src = $this->generateFile($IMAGE['src']);
					$arTmpFile = CFile::MakeFileArray($extra_src);
					$arFils[]=array(
						'VALUE'=>$arTmpFile,
						'DESCRIPTION'=>'',
					);
				}
			}
		}
		$rsProperty =  \CIBlockElement::GetProperty(4, $id, 'sort', 'asc', ['CODE' => $property]);
		$del_images = [];
		while($arProperty = $rsProperty->Fetch()) {
			$str = '/var/www/html'.CFile::GetPath($arProperty['VALUE']);
			if(!in_array($str, $paths_array)) {
				$del_images[$arProperty['PROPERTY_VALUE_ID']] = ['VALUE' => ['del' => 'Y']];
			}else{
				foreach ($arFils as $key=>$arFil) {
					if($str==$arFil['VALUE']['tmp_name'])
						unset($arFils[$key]);
				}
			}
		}
		// Одной вилкой чистим множественной свойство картинок
		\CIBlockElement::SetPropertyValueCode(
			$id,
			$property,
			$del_images
		);
		if(count($arFils)>0) {
			foreach ($arFils as $arFil){
				$new_arFils[] = $arFil;
			}
		}
		return $new_arFils;
	}
	// Время хранения кэша
	private $cacheTtl = 604800;
	// Директория хранения кэша относительно /bitrix/cache
	private $cacheDir = 'cabinet/products';

	public function __construct()
	{
		Loader::includeModule('iblock');
		Loader::includeModule('catalog');
	}
	// Пример запроса получения данных из инфоблока
	// http://{domain}/{api}/example/check?iblock_id=1&active=Y&name=Шар&color=Белый&fields=preview_text,detail_text,form,preview_picture&sort=id:asc,name:asc&limit=5&page=1
	public function show()
	{
        //Переводим все ключи в верхний регистр
        $arParams = array_change_key_case(request()->get(), CASE_UPPER);
        if($arParams['ID']){
            //Поля по умолчанию для выборки
            $arSelect = [
                'IBLOCK_ID',
                'ID',
                'NAME',
                'IBLOCK_SECTION_ID',
                'PROPERTY_SKU',
                'PROPERTY_WEIGHT',
                'PROPERTY_STATUS',
                'DETAIL_PICTURE',
                'DETAIL_TEXT'
            ];
            // Фильтр по умолчанию для выборки
            $arFilter = [
                'PROPERTY_PARTNER' => $this->getModerator()->getShopId(),
                'ID' => $arParams['ID']
            ];

            // Дополнительные параметры фильтра из запроса
            $arFilter['IBLOCK_ID'] = $arParams['IBLOCK_ID'] ? $arParams['IBLOCK_ID'] : 4;
            // Параметры по умолчанию для постраничной навигации и ограничения количества выводимых элементов
            $arNavStartParams = [];
            // Сортировка по умолчанию для выборки
            if ($arParams['SORT']) {
                $arParams['ORDER'] = $arParams['ORDER'] ? $arParams['ORDER'] : 'ASC';
                switch ($arParams['SORT']) {
                    case 'quantity':
                        $arParams['SORT'] = 'CATALOG_QUANTITY';
                        break;
                    case 'sku':
                        $arParams['SORT'] = 'PROPERTY_SKU';
                        break;
                    case 'price':
                        $arParams['SORT'] = 'catalog_PRICE_1';
                        break;
                    case 'name':
                        $arParams['SORT'] = 'NAME';
                        break;
                    default:
                        $arParams['SORT'] = 'ID';
                        break;
                }
                $arOrder = [
                    $arParams['SORT'] => $arParams['ORDER']
                ];
            } else {
                $arOrder = [
                    'ID' => 'ASC'
                ];
            }
            // Уникальный ключ для кэша
            $cacheId = __CLASS__ . ':' . __FUNCTION__;
            if (is_array($arOrder)) {
                foreach ($arOrder as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arFilter)) {
                ksort($arFilter);
                foreach ($arFilter as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arNavStartParams)) {
                ksort($arNavStartParams);
                foreach ($arNavStartParams as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arSelect)) {
                sort($arSelect);
                $cacheId .= implode(':', $arSelect);
            }
            if (!$response = cache()->get($cacheId, $this->cacheTtl, $this->cacheDir)) {
                // Выборка данных
                if ($items = CIBlockElement::getList($arOrder, $arFilter, false, $arNavStartParams, $arSelect)) {
                    while ($item = $items->fetch()) {
                        $ar['id'] = (int)$item['ID'];
                        $ar['name'] = $item['NAME'];
                        //категория
                        $category['label'] = 'Категория';
                        $category['name'] = 'category';
                        $category['type'] = 'disabled';
                        $category['value'] = $this->getCategoryName(intval($item['IBLOCK_SECTION_ID']));
                        $ar['category'] = $item['IBLOCK_SECTION_ID'];
                        $ar['categoryLabel'] = $category['value'];

                        //sku
                        $sku['label'] = 'SKU';
                        $sku['name'] = 'sku';
                        $sku['type'] = 'disabled';
                        $sku['value'] = $item['PROPERTY_SKU_VALUE'];
                        $ar['sku'] = $sku['value'];

                        //weight
                        $weight['label'] = 'Вес';
                        $weight['name'] = 'weight';
                        $weight['type'] = 'number';
                        $weight['value'] = $item['PROPERTY_WEIGHT_VALUE'];
                        $weight['validation'][] = ['required'=>'number'];
                        $weight['min'] = 0.01;
                        $weight['step'] = 0.01;
                        $ar['main'][] = $weight;

                        //цена
                        $ar_res_price = CPrice::GetBasePrice((int)$item['ID']);
                        if ($ar_res_price['PRICE']) $pr = floatval($ar_res_price['PRICE']);
                        $price['label'] = 'Цена';
                        $price['name'] = 'price';
                        $price['type'] = 'number';
                        $price['value'] = $pr;
                        $price['validation'][] = ['required'=>'price'];
                        $price['min'] = 0.01;
                        $price['step'] = 0.01;
                        $ar['main'][] = $price;

                        //количество
                        if (CCatalog::GetByID($arFilter['IBLOCK_ID'])) {
                            $ar_res = CCatalogProduct::GetByID((int)$item['ID']);
                            if ($ar_res['QUANTITY']) $quan = intval($ar_res['QUANTITY']);
                        }
                        $quantity['label'] = 'Количество';
                        $quantity['name'] = 'quantity';
                        $quantity['type'] = 'number';
                        $quantity['value'] = $quan;
                        $quantity['validation'][] = ['required'=>'number'];
                        $quantity['min'] = 0;
                        $quantity['step'] = 0.01;
                        $ar['main'][] = $quantity;

                        //Описание
                        $desc['label'] = 'Описание';
                        $desc['name'] = 'desc';
                        $desc['type'] = 'reachtext';
                        $desc['value'] = $item['DETAIL_TEXT'];
                        $ar['main'][] = $desc;

                        //Главное изображение
                        $arFile = CFile::GetFileArray($item['DETAIL_PICTURE']);
                        $pict['label'] = 'Главное изображение';
                        $pict['name'] = 'main_image';
                        $pict['type'] = 'image';
                        $pict['allowedType'] = 'image/*';
                        $pict['value'] = [];
                        if($arFile['SRC']) {
                            $pict['value'][] = [
                                'src' => $arFile['SRC'],
                                'title' => $item['NAME'],
                            ];
                        }
                        $ar['main'][] = $pict;

                        //Дополнительние изображения
                        $photos_arr = [];
                        $more_photos = CIBlockElement::GetProperty(4, $item['ID'], "sort", "asc", array('CODE'=>'MORE_PHOTO'));
                        while($photo_props = $more_photos->Fetch()){
                            if($photo_props['VALUE']!=NULL) {
                                $arMorePict = CFile::GetFileArray($photo_props['VALUE']);
                                $photos_arr[] = [
                                    'src' => $arMorePict['SRC'],
                                    'title' => '',
                                ];
                            }
                        }
                        $extra_pict['label'] = 'Дополнительние изображения';
                        $extra_pict['name'] = 'extra_images';
                        $extra_pict['type'] = 'image';
                        $extra_pict['value'] = $photos_arr;
                        $extra_pict['allowedType'] = 'image/*';
                        $extra_pict['multiple'] = true;
                        $ar['main'][] = $extra_pict;

                        //Дополнительние файлы
                        /*$files_arr = [];
                        $more_files = CIBlockElement::GetProperty(4, $item['ID'], "sort", "asc", array('CODE'=>'FILES'));
                        while($files_props = $more_files->Fetch()){
                            if($files_props['VALUE']!=NULL) {
                                $arMoreFile = CFile::GetFileArray($files_props['VALUE']);
                                $files_arr[] = [
                                    'src' => 'https://marketplace.rybalka.ua' . $arMoreFile['SRC'],
                                    'title' => '',
                                    'allowedType' => $arMoreFile['CONTENT_TYPE']
                                ];
                            }
                        }
                        $extra_files['label'] = 'Файл';
                        $extra_files['name'] = 'file';
                        $extra_files['type'] = 'file';
                        $extra_files['multiple'] = true;
                        $extra_files['value'] = $files_arr;
                        $ar['main'][] = $extra_files;
                        */

                        //доп свойства

                        $types = [
                            'S'=> 'text',
                            'L'=> 'select',
                            'E'=> 'element',
                            'N'=> 'number',
                            'F'=> 'file',
                        ];
                        $break_properties = [
                            'SKU',
                            'PARTNER',
                            'STATUS',
                            'MORE_PHOTO',
                            'FILES',
                            'WEIGHT'
                        ];

                        $array = CIBlockSectionPropertyLink::GetArray(4, intval($item['IBLOCK_SECTION_ID']));
                        $new_array = [];

                        foreach ($array as $key => $one) {
                            $res = CIBlockProperty::GetByID($key);
                            if ($ar_res = $res->GetNext()) {
                                if(in_array($ar_res['CODE'], $break_properties))
                                    continue;
                                $new_array[] = $ar_res;
                                $prop_temp = [];
                                // $prop_temp['res'] = $ar_res;
                                $prop_temp['label'] = $ar_res['NAME'];
                                $prop_temp['name'] = strtolower($ar_res['CODE']);

                                if($ar_res[USER_TYPE] == 'Checkbox') {
                                    // If the field has User type Checkbox
                                    $prop_temp['type'] = 'radio';
                                    $prop_temp['choices'] = [
                                        ['id' => 'y', 'name' => 'Да'],
                                        ['id' => 'n', 'name' => 'Нет']
                                    ];
                                } elseif($ar_res['PROPERTY_TYPE'] == 'L' && $ar_res['LIST_TYPE'] == 'L') {
                                    $prop_temp['type'] = 'select';
                                } else {
                                    $prop_temp['type'] = $types[$ar_res['PROPERTY_TYPE']];
                                    $prop_temp['multiple'] = strtolower($ar_res['MULTIPLE']);
                                }

                                //$prop_temp['hidden'] = false;
                                $props = CIBlockElement::GetProperty(4, $item['ID'], "sort", "asc", array('CODE'=>$ar_res['CODE']));
                                while($arr_props = $props->Fetch()){
                                    if($ar_res['PROPERTY_TYPE'] == 'S' && $ar_res['USER_TYPE'])
                                        $prop_temp['value'] = strtolower($arr_props['VALUE']['TEXT']);
                                    elseif ($ar_res['PROPERTY_TYPE'] == 'L')
                                        $prop_temp['value'] = intval($arr_props['VALUE']);
                                    else {
                                        $prop_temp['value'] = strtolower($arr_props['VALUE']);
                                    }
                                }
                                if ($ar_res['PROPERTY_TYPE'] == 'L') {
                                    $property_enums = CIBlockPropertyEnum::GetList(Array("SORT" => "ASC", "ID" => "ASC"), Array("IBLOCK_ID" => 4, "CODE" => $ar_res['CODE']));
                                    while ($enum_fields = $property_enums->GetNext()) {
                                        $arr['id'] = intval($enum_fields["ID"]);
                                        $arr['name'] = $enum_fields["VALUE"];
                                        if(intval($prop_temp['value'])==$arr['id'])
                                            $arr['checked'] = true;
                                        $prop_temp['choices'][] = $arr;
                                        unset($arr);
                                    }
                                }
                                if ($ar_res['PROPERTY_TYPE'] == 'N') {
                                    $prop_temp['validation'][] = ['required'=>'number'];
                                    $prop_temp['min'] = 0;
                                    $prop_temp['step'] = 0.01;
                                }
                            }
                            $ar['options'][] = $prop_temp;
                        }
                        $response = $ar;
                    }
                }
                // Сохраняем данные в кэш чтобы при следующем запросе именно с такими входными параметрами уже не делать запросы в базу
                if ($response['items']) {
                    cache()->set($response);
                }
            }

            // Запишем информацию в Журнал: Запрос/Ответ
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            // Возвращаем результат клиенту
            response()->json($response, 200, JSON_UNESCAPED_UNICODE);
        }
        else {
            //Поля по умолчанию для выборки
            $arSelect = [
                'ID',
                'NAME',
                'PROPERTY_SKU',
                'PROPERTY_STATUS'
            ];
            // Дополнительные поля из запроса
            if ($arParams['FIELDS']) {
                $arParams['FIELDS'] = strtoupper($arParams['FIELDS']);
                $arParams['FIELDS'] = explode(',', $arParams['FIELDS']);
                foreach ($arParams['FIELDS'] as $field) {
                    if ($field === 'COLOR' || $field === 'FORM') {
                        $field = 'PROPERTY_' . $field;
                    }
                    if (!in_array($field, $arSelect)) {
                        $arSelect[] = $field;
                    }
                }
            }
            // Фильтр по умолчанию для выборки

            $arFilter = [
                //'ACTIVE' => 'Y',
                'PROPERTY_PARTNER' => $this->getModerator()->getShopId()
            ];
            if ($arParams['FILTER']){
                if(is_numeric($arParams['FILTER'])) {
                    $arFilter[] = [
                        'LOGIC' => 'OR',
                        ['NAME' => '%'.$arParams['FILTER'].'%'],
                        ['PROPERTY_SKU' => '%'.$arParams['FILTER'].'%'],
                        ['CATALOG_PRICE_SCALE_1' => $arParams['FILTER']],
                        ['CATALOG_QUANTITY' => $arParams['FILTER']],
                        ['ID' => $arParams['FILTER']],
                    ];
                } else {
                    $arFilter[] = [
                        'LOGIC' => 'OR',
                        ['NAME' => '%'.$arParams['FILTER'].'%'],
                        ['PROPERTY_SKU' => '%'.$arParams['FILTER'].'%'],
                    ];
                }
            }
            $arAllFilter = $arFilter;
            // Дополнительные параметры фильтра из запроса
            $arFilter['IBLOCK_ID'] = $arParams['IBLOCK_ID'] ? $arParams['IBLOCK_ID'] : 4;
            // Параметры по умолчанию для постраничной навигации и ограничения количества выводимых элементов
            $arNavStartParams = [
                'nPageSize' => 10,
                'iNumPage' => 1
            ];
            // Дополнительные параметры для постраничной навигации из запроса
            // Ограничиваем максимальное количество записей при выборке
            if ($arParams['PER_PAGE']) $arNavStartParams['nPageSize'] = $arParams['PER_PAGE'] < 150 ? $arParams['PER_PAGE'] : 150;
            if ($arParams['PAGE']) $arNavStartParams['iNumPage'] = &$arParams['PAGE'];
            // Сортировка по умолчанию для выборки
            if ($arParams['SORT']) {
                $arParams['ORDER'] = $arParams['ORDER'] ? $arParams['ORDER'] : 'ASC';
                switch ($arParams['SORT']) {
                    case 'quantity':
                        $arParams['SORT'] = 'CATALOG_QUANTITY';
                        break;
                    case 'sku':
                        $arParams['SORT'] = 'PROPERTY_SKU';
                        break;
                    case 'price':
                        $arParams['SORT'] = 'catalog_PRICE_1';
                        break;
                    case 'name':
                        $arParams['SORT'] = 'NAME';
                        break;
                    default:
                        $arParams['SORT'] = 'ID';
                        break;
                }
                $arOrder = [
                    $arParams['SORT'] => $arParams['ORDER']
                ];
            } else {
                $arOrder = [
                    'ID' => 'ASC'
                ];
            }
            // Уникальный ключ для кэша
            $cacheId = __CLASS__ . ':' . __FUNCTION__;
            if (is_array($arOrder)) {
                foreach ($arOrder as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arFilter)) {
                ksort($arFilter);
                foreach ($arFilter as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arNavStartParams)) {
                ksort($arNavStartParams);
                foreach ($arNavStartParams as $field => $value) {
                    $cacheId .= $field . ':' . $value . ':';
                }
            }
            if (is_array($arSelect)) {
                sort($arSelect);
                $cacheId .= implode(':', $arSelect);
            }
            if (!$response = cache()->get($cacheId, $this->cacheTtl, $this->cacheDir)) {
                $response = [
                    'page' => $arParams['PAGE'] ? (int)$arParams['PAGE'] : 1,
                    'per_page' => 0,
                    'items' => [],
                    // 'filter' => [],
                    // 'order' => []
                ];
                // Выборка данных
                if ($items = CIBlockElement::getList($arOrder, $arFilter, false, $arNavStartParams, $arSelect)) {
                    while ($item = $items->fetch()) {



                        $ar['id'] = (int)$item['ID'];
                        $ar['name'] = $item['NAME'];
                        $ar['sku'] = $item['PROPERTY_SKU_VALUE'];
                        if (CCatalog::GetByID($arFilter['IBLOCK_ID'])) {
                            $ar_res = CCatalogProduct::GetByID((int)$item['ID']);
                            if ($ar_res['QUANTITY']) $ar['quantity'] = intval($ar_res['QUANTITY']);
                        }
                        $ar_res_price = CPrice::GetBasePrice((int)$item['ID']);
                        if ($ar_res_price['PRICE']) $ar['price'] = floatval($ar_res_price['PRICE']);
                        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("CODE" => "STATUS", "VALUE" => $item['PROPERTY_STATUS_VALUE']));
                        while ($enum_fields = $property_enums->GetNext()) {
                            $ar['status']['id'] = $enum_fields['XML_ID'];
                            $ar['status']['label'] = $enum_fields['VALUE'];
                        }

                        $response['per_page']++;
                        $response['items'][] = $ar;
                    }
                }
                // BFS
                // $response['filter'] = $arFilter;
                // $response['order'] = $arOrder;

                // Сохраняем данные в кэш чтобы при следующем запросе именно с такими входными параметрами уже не делать запросы в базу
                if ($response['items']) {
                    cache()->set($response);
                }
            }

            $all_items = CIBlockElement::getList(false, $arAllFilter, array());
            $response['total'] = intval($all_items);
            // Запишем информацию в Журнал: Запрос/Ответ
            journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
            // Возвращаем результат клиенту
            response()->json($response, 200, JSON_UNESCAPED_UNICODE);
        }
	}

	public function update()
    {
		//Переводим все ключи в верхний регистр
		$arParams = array_change_key_case(request()->get(), CASE_UPPER);
        $arLoadProductArray = [];

        //Имя товара
        $arLoadProductArray['NAME'] = $arParams['NAME'];

        $price = $arParams['PRICE'] ? $arParams['PRICE'] : 0;
        $quantity = $arParams['QUANTITY'] ? $arParams['QUANTITY'] : 0;
        //Родительская категория, по умолчанию в корне
        $arLoadProductArray['IBLOCK_SECTION'] = $arParams['CATEGORY'] ? $arParams['CATEGORY'] : false;
        //Код инфоблока
        $arLoadProductArray['IBLOCK_ID'] = 4;
        $arLoadProductArray['ACTIVE'] = 'N';
        $arLoadProductArray['DETAIL_TEXT'] = $arParams['DESC'];

        $el = new CIBlockElement;

        $options = [];
        $options['PARTNER'] = $this->getModerator()->getShopId();

        foreach ($arParams as $key=>$param){
            if(!array_key_exists($key, $arLoadProductArray) && !in_array($key, ['CATEGORY'])) {
                // у випадку значення 'y' треба перевести в апперкейс
                if($param == 'y') {
                    $param = 'Y';
                }

                $options[$key] = $param;
            }
        }
        if($arParams['WEIGHT']) {
            $options['WEIGHT'] = $arParams['WEIGHT'];
        }

        // Головне забраження
        if($arParams[MAIN_IMAGE] === '') {
            // якщо не передали зображення, то значить поточне
            $arLoadProductArray[DETAIL_PICTURE] = ['del' => 'Y'];
        } elseif(strpos($arParams[MAIN_IMAGE][0][src], 'data:') !== false) {
            // якщо передали data:, то є нове зображення
            $theFileSrc = $this->generateFile($arParams[MAIN_IMAGE][0][src]);
            $arFile = CFile::MakeFileArray($theFileSrc);
            if($arFile[type] != 'inode/x-empty') {
                $arLoadProductArray['DETAIL_PICTURE'] = $arFile;
            }
        }

        $arCurrentMorePhotos = [];
        if($arParams[ID]) { // якщо товар вже існує
            // Додаткові зображення
            $resCurrentMorePhotos = $el->GetProperty(
                4,
                $arParams[ID],
                [],
                ['CODE' => 'MORE_PHOTO']
            );
            while($arCurrentMorePhoto = $resCurrentMorePhotos->Fetch()) {
                $arCurrentMorePhotos[$arCurrentMorePhoto[PROPERTY_VALUE_ID]] = CFile::GetPath($arCurrentMorePhoto[VALUE]);
            }
            // $response['current more photos'] = $arCurrentMorePhotos;
        }

        $arLoadExtraImages = [];
        // перевірка, які зображення потрібно видалити
        foreach($arCurrentMorePhotos as $key => $theCurrentMorePhotoSrc) {
            $isKeyFound = false;
            foreach($arParams[EXTRA_IMAGES] as $arExtraImage) {
                if($arExtraImage[src] === $theCurrentMorePhotoSrc) {
                    $isKeyFound = true;
                }
            }
            if(!$isKeyFound) {
                $arLoadExtraImages[$key] = ['del' => 'Y'];
            } else {
                $arLoadExtraImages[$key] = false;
            }
        }
        // перевірка на нові зображення
        foreach($arParams['EXTRA_IMAGES'] as $arExtraImage) {
            if(strpos($arExtraImage[src], 'data:') !== false) {
                // якщо передали data:, то э нове зображення
                $theFileSrc = $this->generateFile($arExtraImage[src]);
                $arFile = CFile::MakeFileArray($theFileSrc);
                if($arFile[type] != 'inode/x-empty') {
                    $arLoadExtraImages[] = $arFile;
                }
            }
        }
        $options[MORE_PHOTO] = $arLoadExtraImages;

        // змінюємо статус товару на Новий
        $options[STATUS] = 93;

        $arLoadProductArray['PROPERTY_VALUES'] = $options;

        if($arParams['ID']) {
            $PRODUCT_ID = $arParams['ID'];
            $el->Update($PRODUCT_ID, $arLoadProductArray, false, false, true);
        }else{
            $PRODUCT_ID = $el->Add($arLoadProductArray);
            CCatalogProduct::Add(['ID'=>$PRODUCT_ID]);
        }

        //обновление цены
        CPrice::SetBasePrice(intval($PRODUCT_ID), floatval($price), 'UAH');
        //обновление количества
        $arFields_quan = array('QUANTITY' => $quantity);
        CCatalogProduct::Update($PRODUCT_ID, $arFields_quan);

        // Запишем информацию в Журнал: Запрос/Ответ
        $response['id'] = $PRODUCT_ID;
        $response['name'] = $arParams['NAME'];
        $response['fields'] = $arLoadProductArray;

        // $response['params'] = $arParams; // just for testing, must be commented

        journal()->add('request-response', ['request' => request()->get(), 'response' => $response]);
        // Возвращаем результат клиенту
        response()->json($response, 200, JSON_UNESCAPED_UNICODE);
	}

	public function delete()
	{
        $arParams = array_change_key_case(request()->get(), CASE_UPPER);
        if($arParams['ID']) {
            $response['status'] = 200;
            $response['message'] = 'Не передан параметр ID';
            $id = intval($arParams['ID']);
            $db_props = CIBlockElement::GetProperty(4, $id, array(), Array("CODE" => "PARTNER"));
            if ($ar_props = $db_props->Fetch()) {
                if (intval($ar_props['VALUE']) == $this->getModerator()->getShopId()) {
                    if (CIBlockElement::Delete($id)) {
                        $response['status'] = 200;
                        $response['message'] = 'Товар c id ' . $id . ' успешно удален';
                    } else {
                        $response['status'] = 402;
                        $response['message'] = 'Ошибка удаления товара';
                    }
                } else {
                    $response['status'] = 402;
                    $response['message'] = 'У модератора недостаточно прав для удаления товара или товар отсутствует';
                }
            } else {
                $response['status'] = 402;
                $response['message'] = 'У модератора недостаточно прав для удаления товара или товар отсутствует';
            }
        }else{
            $response['status'] = 400;
            $response['message'] = 'Не передан параметр ID';
        }

        response()->json($response, $response['status'], JSON_UNESCAPED_UNICODE);
    }

}