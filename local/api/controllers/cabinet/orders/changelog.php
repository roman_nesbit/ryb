<?
namespace Local\Api\Controllers\Cabinet\Orders;

use CUser;
use CIBlockElement;
use \Bitrix\Iblock\ElementTable;
use Rybalka\Marketplace\Model\ShopModerator;
use Rybalka\Marketplace\Rest\RestControllerTrait;

class Changelog {
    use RestControllerTrait;
    private $cacheTtl = 7 * 24 * 3600;
	private $cacheDir = 'cabinet/orders/changelogs';

	public function __construct() {
		\Bitrix\Main\Loader::includeModule("iblock");
	}

	public function get() {

		if($this->getModerator()->getShopId() === $arParams['_USER']['UF_SHOP']) {
			//Переводим все ключи в верхний регистр
			$arParams = array_change_key_case(request()->get(), CASE_UPPER);

			if (!$response = cache()->get($arParams[ORDER_ID], $this->cacheTtl, $this->cacheDir)) {
				// Выбираем для заказа
				$arSelect = [
					"ID",
					"DATE_CREATE", 
					"CREATED_BY", 
					"PROPERTY_ORDER_STATUS", 
					"DETAIL_TEXT"
				];
				$arFilter = [
					"IBLOCK_ID" => 10,
					"ACTIVE"=>"Y",
					"PROPERTY_ORDER_ID" => $arParams[ORDER_ID],
				];
				$arOrder = ["DATE_CREATE" => "DESC"];
				$res = CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);
				$arItems = [];
				$arAuthor = [];
				while($obj = $res->GetNext()) {
					// имя создателя
					if($obj[CREATED_BY] !== $arAuthor[id]) {
						$rsUser = CUser::GetByID($obj[CREATED_BY]);
						$arUser = $rsUser->Fetch();
						$arAuthor = [
							'id' => $arUser[ID],
							'name' => $arUser[NAME] . ' ' . $arUser[LAST_NAME],
						];
					}

					$arItems[] = [
						'id' => $obj[ID],
						'author' => $arAuthor,
						'status' => $obj[PROPERTY_ORDER_STATUS_VALUE],
						'notes' => $obj[DETAIL_TEXT],
						'datetime' => date('c', MakeTimeStamp($obj[DATE_CREATE])),
					];
				}

				$response[items] = $arItems;

				// сохранием в кеш
				if ($response) {
					cache()->set($response);
				}
			}
			response()->json($response, 200, JSON_UNESCAPED_UNICODE);
		}
	}

	public function add() {
		if($this->getModerator()->getShopId() === $arParams['_USER']['UF_SHOP']) {
			//Переводим все ключи в верхний регистр
			$arParams = array_change_key_case(request()->get(), CASE_UPPER);

			// Додаємо новий елемент
			if($theElementId = addOrdersChangelogRecord($arParams[ORDER_ID], $arParams[STATUS], $arParams[NOTES], $arParams['_USER'][ID])) {
				$response[id] = $theElementId;
				// стираем кеш
				cache()->clear($this->cacheDir);
			} else {
				$response[error] = 'Ошибка протоколирования';
			}

			response()->json($response, 200, JSON_UNESCAPED_UNICODE);
		}	
	}
}