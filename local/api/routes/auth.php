<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

return [
    'POST' => [
        'auth/login' => [
            'description' => loc('GET:login'),
            'controller' => '\Local\Api\Controllers\User\Auth@login',
            'security' => [
            ]
        ]
    ]
];