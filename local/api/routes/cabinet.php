<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

$defaultSecurityRule = [
    'auth' => [
        'required' => true,
        'type' => 'token'
    ],
    'group' => [
        'whitelist' => [1, 5]
    ],
];

return [
	'GET' => [
		'cabinet/products' => [
			'description' => "Возращает список товаров или данные указанного товара",
			'controller' => '\Local\Api\Controllers\Cabinet\Products@show',
			'security' => $defaultSecurityRule,
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/cabinet/products/?order=DESC&page=1&per_page=10&sort=id',
					'response' => [
						'json' => '{"page":2,"per_page":4,"items":[{"id":4963,"name":"Безынерционная катушка Quantum Catalyst CT10PTI-B","sku":"CT10PTI-B","quantity":225,"price":3387.57,"status":{"id":"new","label":"новый"}},{"id":4962,"name":"Кастинговое удилище Element 21 Salty Carrot Stix Inshore CSIS761M-F-C","sku":"CSIS761M-F-C","quantity":125,"price":1404,"status":{"id":"review","label":"на модерации"}},{"id":4960,"name":"Спиннинг Encore Nemesis Viator SE NMSV-S764L (4-х частный, тубус)","sku":"NMSV-S764L","quantity":100,"price":3341.52,"status":{"id":"active","label":"активный"}},{"id":4959,"name":"Спиннинг Element 21 Bluewater Carrot Stix BWCS701ML-S","sku":"BWCS701ML-S","quantity":150,"price":1404,"status":{"id":"active","label":"активный"}}],"total":14}'
					]
				]
			],
			'documentation' => [
				'exclude' => [
					'admin' => false
				]
			]
		],
		'cabinet/sku' => [
			'description' => loc('GET:sku'),
			// Активность роутра
			// Необязательный параметр (по умолчанию роут активен)
			//'active' => false,
			// Контроллер обрабатывающий роут
			// Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
			// Где Check - класс, read - метод класса
			'controller' => '\Local\Api\Controllers\Cabinet\Sku@get',
			// Безопасность участвует при инициализации ядра интерфейса
			// Для работы параметра необходимо чтобы была включена Авторизация по токену в настройках модуля
			'security' => $defaultSecurityRule,
			'example' => [
				'request' => [
					'url' => 'http://#DOMAIN#/#API#/example/check/?iblock_id=1&sort=id:asc',
					'response' => [
						'json' => '{"page":1,"total":3,"items":[{"ID":1,"NAME":"item1"},{"ID":2,"NAME":"item2"},{"ID":3,"NAME":"item3"}]}'
					]
				]
			],
			// Настройки для поведения роута в документации
			'documentation' => [
				'exclude' => [
					// Исключить из документации в административной части сайта
					'admin' => false // true | false
				]
			]
		],
		'cabinet/orders' => [
			'description' => 'Возвращает список заказов или информацию о заказе',
			'controller' => '\Local\Api\Controllers\Cabinet\Orders@get',
			'security' => $defaultSecurityRule,
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/cabinet/orders/',
					'response' => [
						'json' => '{"page":1,"total":3,"items":[{"ID":1,"NAME":"item1"},{"ID":2,"NAME":"item2"},{"ID":3,"NAME":"item3"}]}'
					]
				]
			],
			'documentation' => [
				'exclude' => [
					'admin' => false
				]
			]
		],
		'cabinet/statuses' => [
			// Описание роута
			'description' => loc('GET:item'),
			// Активность роутра
			// Необязательный параметр (по умолчанию роут активен)
			//'active' => false,
			// Контроллер обрабатывающий роут
			// Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
			// Где Check - класс, read - метод класса
			'controller' => '\Local\Api\Controllers\Cabinet\Statuses@show',
			// Безопасность участвует при инициализации ядра интерфейса
			// Для работы параметра необходимо чтобы была включена Авторизация по токену в настройках модуля
			'security' => $defaultSecurityRule,
			'example' => [
				'request' => [
					'url' => 'http://#DOMAIN#/#API#/example/check/?iblock_id=1&sort=id:asc',
					'response' => [
						'json' => '{"page":1,"total":3,"items":[{"ID":1,"NAME":"item1"},{"ID":2,"NAME":"item2"},{"ID":3,"NAME":"item3"}]}'
					]
				]
			],
			// Настройки для поведения роута в документации
			'documentation' => [
				'exclude' => [
					// Исключить из документации в административной части сайта
					'admin' => false // true | false
				]
			]
		],
		'cabinet/mynumber' => [
			// Описание роута
			'description' => loc('GET:item'),
			// Активность роутра
			// Необязательный параметр (по умолчанию роут активен)
			//'active' => false,
			// Контроллер обрабатывающий роут
			// Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
			// Где Check - класс, read - метод класса
			'controller' => '\Local\Api\Controllers\Cabinet\Mynumber@show',
			// Безопасность участвует при инициализации ядра интерфейса
			// Для работы параметра необходимо чтобы была включена Авторизация по токену в настройках модуля
			'security' => $defaultSecurityRule,
			'example' => [
				'request' => [
					'url' => 'http://#DOMAIN#/#API#/example/check/?iblock_id=1&sort=id:asc',
					'response' => [
						'json' => '{"page":1,"total":3,"items":[{"ID":1,"NAME":"item1"},{"ID":2,"NAME":"item2"},{"ID":3,"NAME":"item3"}]}'
					]
				]
			],
			// Настройки для поведения роута в документации
			'documentation' => [
				'exclude' => [
					// Исключить из документации в административной части сайта
					'admin' => false // true | false
				]
			]
		],
		'cabinet/orders/changelog' => [
			'controller' => '\Local\Api\Controllers\Cabinet\Orders\Changelog@get',
			'parameters' => [
				'order_id' => [
					'required' => true,
					'type' => 'integer',
					'description' => 'ID заказа'
				]
			],
			'security' => $defaultSecurityRule,
		],
	],
	'POST' => [
		'cabinet/products' => [
			'description' => 'Сохранение данных товара',
			'controller' => '\Local\Api\Controllers\Cabinet\Products@update',
			'security' => $defaultSecurityRule,
		],
        'cabinet/orders/set-status' => [
            'controller' => '\Local\Api\Controllers\Cabinet\Orders@setStatus',
            'security' => $defaultSecurityRule,
        ],
        'cabinet/orders/add-note' => [
            'controller' => '\Local\Api\Controllers\Cabinet\Orders@addNote',
            'security' => $defaultSecurityRule,
        ],
        'cabinet/orders/changelog' => [
			'description' => 'Добавление записи в протокол изменений заказа',
			'controller' => '\Local\Api\Controllers\Cabinet\Orders\Changelog@add',
			'parameters' => [
				'order_id' => [
					'required' => true,
					'type' => 'integer',
					'description' => 'ID заказа'
				],
				'status' => [
					'required' => false,
					'type' => 'string',
					'description' => 'Статус заказа'
				],
				'notes' => [
					'required' => false,
					'type' => 'string',
					'description' => 'Заметки'
				],
			],
			'security' => $defaultSecurityRule,
		]
	],
	'PUT' => [
		'cabinet/item' => [
			// Описание роута
			'description' => loc('GET:item'),
			// Активность роутра
			// Необязательный параметр (по умолчанию роут активен)
			//'active' => false,
			// Контроллер обрабатывающий роут
			// Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
			// Где Check - класс, read - метод класса
			'controller' => '\Local\Api\Controllers\Cabinet\Item@update',
			'security' => $defaultSecurityRule,
		]
	],
	'DELETE' => [
		'cabinet/products' => [
			// Описание роута
			'description' => loc('GET:item'),
			// Активность роутра
			// Необязательный параметр (по умолчанию роут активен)
			//'active' => false,
			// Контроллер обрабатывающий роут
			// Контроллер может располагаться за пределами модуля, главное чтобы он был доступен через пространство имён
			// Где Check - класс, read - метод класса
			'controller' => '\Local\Api\Controllers\Cabinet\Products@delete',
			'security' => $defaultSecurityRule,
		],
	],
];