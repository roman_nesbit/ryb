<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

$defaultSecurityRule = [
	'auth' => [
		'required' => true,
		'type' => 'token'
	],
	'group' => [
		'whitelist' => [1, 5, 6]
	],
];

return [
	'GET' => [
	    'checkout/details' => [
            'controller' => '\Local\Api\Controllers\Checkout@getDetails',
            'security' => [],
        ],
        'checkout/payment-options' => [
            'controller' => '\Local\Api\Controllers\Checkout@getPaymentOptions',
        ],
        'checkout/delivery-options' => [
            'controller' => '\Local\Api\Controllers\Checkout@getDeliveryOptionsAction',
        ]
	],
    'PATCH' => [
        'checkout/details' => [
            'controller' => '\Local\Api\Controllers\Checkout@updateDetailsAction',
            'security' => [],
        ]
    ]
];