<?
Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

return [
    'GET' => [
        'dictionary/address/settlement' => [
            'controller' => '\Local\Api\Controllers\Dictionary\Address@findSettlementAction'
        ],
        // legacy API
        'locations' => [
            'controller' => '\Local\Api\Controllers\Dictionary\Address@findSettlementAction'
        ],
        'dictionary/address/street' => [
            'controller' => '\Local\Api\Controllers\Dictionary\Address@findStreetAction'
        ],

        'dictionary/post/office' => [
            'controller' => '\Local\Api\Controllers\Dictionary\Post@listOfficesAction'
        ],

        // legacy API
        'np-offices' => [
            'controller' => '\Local\Api\Controllers\Dictionary\Post@legacyListOfficesAction'
        ]
    ],
];