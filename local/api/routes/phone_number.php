<?
Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

$defaultSecurityRule = [
    'auth' => [
        'required' => true,
        'type' => 'token'
    ],
    'group' => [
        'whitelist' => [1, 5]
    ],
];

return [
	'POST' => [
		'phone-number/send-verification-code' => [
			'controller' => '\Local\Api\Controllers\Phone_number@SendVerificationCode',
			'security' => [
			],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/phone-number/send-verification-code/?phoneNumber=380449876543&captchaToken=db5c88d0-391c-11dd-90d9-001a92567626',
					'response' => [
						'json' => '{"meta":[{"status":200,"error":""}],"verificationId":"914803da-5169-4720-8af8-9c7e4d548041"}'
					]
				]
			],

		],
		'phone-number/verify-code' => [
			'controller' => '\Local\Api\Controllers\Phone_number@VerifyCode',
			'security' => [
			],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/phone-number/verify-code/?verificationId=123&code=1234',
					'response' => [
						'json' => '{"meta":[{"status":200,"error":""}],"verificationToken":"04a4cb08-9278-4007-9684-953bafdd2b8b"}'
					]
				]
			],

		],
		'phone-number/check-token-validity' => [
			'controller' => '\Local\Api\Controllers\Phone_number@CheckTokenValidity',
			'security' => [
			],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/phone-number/check-token-validity/?verificationToken=123',
					'response' => [
						'json' => '{"meta":[{"status":200,"error":""}]}'
					]
				]
			],

		],
	],
	'GET' => [
		'phone-number/status' => [
			'controller' => '\Local\Api\Controllers\Phone_number@Status',
			'parameters' => [
				'phoneNumber' => [
					'type' => 'string',
					'description' => 'Номер телефона для проверки',
				],
				'captchaToken' => [
					'type' => 'string',
					'description' => 'Токен каптчи',
				]
			],
			'security' => [
			],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/phone-number/status/?phoneNumber=380980980123&captchaToken=FSDJ...',
					'response' => [
						'json' => '{"meta":[{"status":200,"error":""}],"data":[{"registered":true}]}'
					]
				]
			]
		]
	]
];