<?php

Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);

$defaultSecurityRule = [
	'auth' => [
		'required' => true,
		'type' => 'token'
	],
	'group' => [
		'whitelist' => [1, 5, 6]
	],
];

return [
	'GET' => [
		'user/me' => [
			'description' => 'Получение данных текущего авторизованного пользователя',
			'controller' => '\Local\Api\Controllers\User\User@getProfile',
			'security' => [
            ],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/user/me/',
					'response' => [
						'json' => '{"meta":[{"status":200,"error":""}],"data":[{"profile":[{"name":"John","surname":"Snow","phone_number":"380963969812","email":"john@thewall.com","maillist_subscriber":true}]}]}'
					]
				]
			]
		],
		'user/shopping-cart' => [
			'description' => 'Корзина пользователя',
			'controller' => '\Local\Api\Controllers\User\User@getShoppingCart',
			'security' => [
			],
			'example' => [
				'request' => [
					'url' => 'https://#DOMAIN#/#API#/user/shopping-cart/',
					'response' => [
						'json' => '{
							"meta": {"status":200,"error":""},
							"data":[
								{
									"products":[
										{
											"id":1,
											"name":"Безынерционная катушка Pflueger Supreme XT 9035XT",
											"image":"/upload/iblock/ace/ace5830204e931273fb2d74393da59a3.jpg",
											"amount": 2,
											"original_price": {"formatted": "650.00 грн"},
											"price": {"formatted": "500.00 грн"},
											"total": {"formatted": "1000.00 грн"}
										}
									],
									"details": {
										"discount": {"formatted": "130.00 грн"},
										"delivery_cost": {
											"formatted": "35.00 грн",
											"details": "Доставка Нова Пошта"
										},
										"payment_cost": {
											"formatted": "95.00 грн", 
											"details": "Наложенный платеж Нова Пошта"
										},
										"subtotal": { "formatted": "750.00 грн"},
										"total": { "formatted": "1450.00 грн"}
									}
								}
							]
						}'
					]
				]
			]
		]
	],
];