<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	</div>
	<footer class="footer">
		<section class="container">
			<div class="inner">
				<div class="sectionNav">
					<nav class="section">
						<? $APPLICATION->IncludeComponent(
							"bfs:menu.block",
							".default",
							Array(
								"COMPONENT_TEMPLATE" => ".default",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO",
								"TITLE" => "Покупателям",
								"MENU_TYPE" => "footerforbuyers",
								"MENU_TEMPLATE" => ".default",
								"SHOW_TOGGLES" => "Y",
							)
						); ?>
					</nav>
					<nav class="section">
						<? $APPLICATION->IncludeComponent(
							"bfs:menu.block", 
							".default", 
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO",
								"TITLE" => "Продавцам",
								"MENU_TYPE" => "footerforsellers",
								"MENU_TEMPLATE" => ".default",
								"SHOW_TOGGLES" => "Y"
							),
							false
						); ?>
					</nav>
					<nav class="section">
						<? $APPLICATION->IncludeComponent(
							"bfs:menu.block", 
							".default", 
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO",
								"TITLE" => "О нас",
								"MENU_TYPE" => "footeraboutus",
								"MENU_TEMPLATE" => ".default",
								"SHOW_TOGGLES" => "Y"
							),
							false
						); ?>
					</nav>
				</div>
				<div class="section info">
					<? if($APPLICATION->GetCurPage(false) == SITE_DIR): ?>
						<span class="logo">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/logo-footer.svg" alt="" />
						</span>
					<? else: ?>
						<a href="/" class="logo">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/logo-footer.svg" alt="" />
						</a>
					<? endif; ?>

					<!-- <div class="langBar">
						<h4 class="title">Язык</h4>
						<div>
							<a href="&lang=ru" class="active">Ru</a>
							<a href="&lang=ua">Ua</a>
						</div>
					</div> -->

					<? $APPLICATION->IncludeComponent("bitrix:sender.subscribe",
						"footer", 
						Array(
							"COMPONENT_TEMPLATE" => "footer",
							"USE_PERSONALIZATION" => "N",
							"HIDE_MAILINGS" => "Y",
							"CONFIRMATION" => "N",
							"SHOW_HIDDEN" => "N",
							"AJAX_MODE" => "Y",
							"AJAX_OPTION_JUMP" => "Y",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "Y",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600",
							"SET_TITLE" => "N",
							"USER_CONSENT" => "N",
							"USER_CONSENT_ID" => "",
							"USER_CONSENT_IS_CHECKED" => "N",
							"USER_CONSENT_IS_LOADED" => "N",
						)
					); ?>

					<div class="phone"><? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include/phone.php'); ?></div>
					<div class="social">
						<? $APPLICATION->IncludeComponent(
							"bitrix:menu",
							"footer-social",
							Array(
								"ROOT_MENU_TYPE" => "footersocial",
								"MAX_LEVEL" => "1", 
								"CHILD_MENU_TYPE" => "",
								"USE_EXT" => "N",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"MENU_CACHE_TYPE" => "A", 
								"MENU_CACHE_TIME" => "36000000", 
								"MENU_CACHE_USE_GROUPS" => "N", 
								"MENU_CACHE_GET_VARS" => "" 
							)
						);?>
					</div>
					<div class="copyright"><?=GetMessage('FOOTER_COPYRIGHT')?></div>
				</div>
			</div>
		</section>
	</footer>
	<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/icons.php", ["SHOW_BORDER"=>false]); ?>
    <script>
        rmp.push('event.trigger', 'page.viewed');
    </script>
</body>
</html>