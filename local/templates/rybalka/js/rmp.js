window.rmp = window.rmp || [];
window.rmpData = Array.isArray(window.rmp) ? window.rmp : [];
rmp = function() {

    let handlers = [];
    let state = [];
    let eventHandlers = [];

    const registerHandlers = function (name, callback) {
        handlers[name] = callback;
    };

    const push = function() {
        const type = arguments[0] || '';

        for (let i in handlers) {
            if (i == type) {
                let handlerArgs = [].slice.call(arguments).slice(1);
                handlers[i].apply(this, handlerArgs);
                return;
            }
        }
    };

    const on = function(eventName, callback) {
        eventHandlers[eventName] = eventHandlers[eventName] || [];
        eventHandlers[eventName].push(callback);
    };

    const trigger = async function(eventName, params) {
        let subscribers = eventHandlers[eventName] || [];
        for (let i in subscribers) {
            subscribers[i](params);
        }
    };

    const addData = function(bucket, data) {
        state[bucket] = state[bucket] || [];
        state[bucket].push(data);
    };

    const init = function() {
        for (let i in rmpData) {
            const data = rmpData[i];

            if (data.type == 'event.trigger') {
                continue;
            }

            push(data);
        }

        for (let i in rmpData) {
            const data = rmpData[i];

            if (data.type != 'event.trigger') {
                continue;
            }

            push(data);
        }
    };

    let getState = function(containerName) {
        return state[containerName] || [];
    };

    registerHandlers('data.add', addData);

    registerHandlers('event.trigger', trigger);
    registerHandlers('event.on', on);

    return {
        push: push,
        registerHandler: registerHandlers,
        on: on,
        trigger: trigger,
        init: init,
        getState: getState,
    };
}();

window.onload = function() {
    rmp.init();
};

rmp.analytics = function(){

    // START: google tag init
    window.dataLayer = window.dataLayer || [];
    const gt = function() {dataLayer.push(arguments);};
    const googleAnalyticsCode = 'AW-1024981995';

    gt('js', new Date());

    gt('config', 'UA-159675076-1');
    gt('config', googleAnalyticsCode);
    // END: google tag init

    // START: Facebook pixel init
    const fbPixelId = '914502312376209';
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', fbPixelId);
    fbq('set', 'autoConfig', false, fbPixelId);

    // END: Facebook pixel init

    const getCatalogItemInfo = function(itemId) {

        if (!itemId) {
            return null;
        }

        const catalogItems = rmp.getState('catalog');

        for (let i in catalogItems) {
            const current = catalogItems[i];

            if (current.id != itemId) {
                continue
            };

            return current;
        }

        return null;
    };

    const postEventToGoogleAnalytics = function(googleEventName, itemId) {
        const catalogItem = getCatalogItemInfo(itemId);

        if (!catalogItem) {
            return;
        }

        gt('event', googleEventName, {
            'send_to': googleAnalyticsCode,
            'value': catalogItem.price || 0,
            'items': [{
                'id': catalogItem.id || 0,
                'google_business_vertical': 'retail'
            }]
        });
    };

    const postEventToFacebook = function(facebookEventName, itemId) {

        const catalogItem = getCatalogItemInfo(itemId);
        const data = catalogItem ?
            {
                value: catalogItem.price || 0,
                currency: 'UAH',
                content_ids: catalogItem.id || 0,
                content_type: 'product'
            } : null;

        fbq('track', facebookEventName, data);
    };


    rmp.push('event.on', 'catalog.item.viewed', function (params) {
        postEventToGoogleAnalytics('view_item', params);
        postEventToFacebook('ViewContent', params);
    });

    rmp.push('event.on', 'cart.item.added', function (params) {
        postEventToGoogleAnalytics('add_to_cart', params);
        postEventToFacebook('AddToCart', params);
    });

    rmp.push('event.on', 'page.viewed', function() {
        postEventToFacebook('PageView');
    });

    rmp.push('event.on', 'checkout.order.placed', function(orderInfo) {

        const total = orderInfo.total.amount;
        const currency = orderInfo.total.currency;
        const orderId = orderInfo.orderId;

        gt('event', 'conversion', {
            send_to: googleAnalyticsCode,
            value: total,
            currency: currency,
            transaction_id: orderId
        });

        let contents = orderInfo.items.map(function(item) {
            return {
                id: item.id,
                quantity: item.amount
            }
        });

        fbq('track', 'Purchase', {
            value: total,
            currency: currency,
            content_type: 'product',
            contents: contents
        });
    });
}();
