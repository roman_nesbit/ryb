<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<div class="page404">
    <div class="contentWrapper">
        <div class="infoSection">
            <h1 class="title">404. <span class="strike">Рыба</span> Страница не найдена</h1>
            <p>Мы пытались, как могли, искали здесь и там, но, у нас так и не получилось найти ее. Возможно, она ушла в небытие или же ее никогда не существовало.</p>
            <p>Если это ошибка, <a href="/contact-us/">напишите нам</a>!</p>
            <div class="btnBox"><a class="btn" href="/" class="button">На главную</a></div>
        </div>
        <div class="imgSection">
            <img src="/images/404.png" />
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>