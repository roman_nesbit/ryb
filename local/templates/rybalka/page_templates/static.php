<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>

<h1 class="pageTitle noBorder"><?= $APPLICATION->ShowTitle(false) ?></h1>
<div class="staticPageWrapper withCells">
	<asside class="leftCell">
		<? $APPLICATION->IncludeComponent(
			"bfs:menu.static.blocks",
			".default",
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "AUTO",
				"MENU_TYPES" => $arParams['MENU_TYPES'] ?? [
					"footerforbuyers",
					"footerforsellers",
					"footeraboutus",
				],
				"MENU_TEMPLATE" => "for.static.pages",
			)
		); ?>
	</asside>
	<div class="middleCell"><?
		if($arParams['HIDE_STATIC_PAGE_CLASS'] != 'Y'):
			?><div class="staticPage"><?
		endif;
			$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "before",
					"AREA_FILE_RECURSIVE" => "Y",
					"EDIT_TEMPLATE" => ""
				)
			);
			$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "inc",
					"AREA_FILE_RECURSIVE" => "Y",
					"EDIT_TEMPLATE" => ""
				)
			);
			$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "after",
					"AREA_FILE_RECURSIVE" => "Y",
					"EDIT_TEMPLATE" => ""
				)
			);
		if($arParams['HIDE_STATIC_PAGE_CLASS'] != 'Y'):
			?></div><?
		endif;
	?></div>
	<? if ($arParams['HIDE_RECOMMENDED'] !== 'Y') : ?>
		<div class="rightCell">
			<? $APPLICATION->IncludeComponent(
				"bfs:catalog.filtered.block",
				".default",
				array(
					"COMPONENT_TEMPLATE" => ".default",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"COUNT" => "4",
					"FILTER" => "{\"!PROPERTY_RECOMMENDED\":false}",
					"SORT_FIELD" => "PROPERTY_RECOMMENDED",
					"SORT_FIELD2" => "RAND",
					"SORT_ORDER" => "DESC",
					"SORT_ORDER2" => "ASC",
					"TITLE" => "Мы рекомендуем",
					"LINK_TO_ALL" => "/catalog/recommended/"
				),
				false
			); ?>
		</div>
	<? endif; ?>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>