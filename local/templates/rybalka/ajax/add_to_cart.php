<?
define("NO_KEEP_STATISTIC", true);
define('NO_AGENT_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Content-type: application/json');
$result = false;
$data = [];
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	if (($_REQUEST["action"] == "add2cart" || $action == "buy") && IntVal($_REQUEST["product_id"]) > 0) {
		$arParams = array();
		if ($_REQUEST["params"]) {
			$arParams = json_decode($_REQUEST[params], true);
		}
		if (count($arParams) == 0) $arParams = array();

		//количество
		$quantity = ($_REQUEST["quantity"] ?? 1);

		//ID товара (торговый каталог)
		$productId = $_REQUEST["product_id"];

		$arFields = [
			'PRODUCT_ID' => $productId,
			'QUANTITY' => $quantity,
			'RECOMMENDATION' => ($_REQUEST["recommendation"] ?? ""),
			'DELAY' => ($_REQUEST["delay"] ?? "N"),
			'PROPS' => $arParams,
		];

		$result = Bitrix\Catalog\Product\Basket::addProduct($arFields);
		$data = $result->getData();

		if ($_REQUEST["action"] == "buy") {
			LocalRedirect($_REQUEST["cart_link"] ?? "/order/cart");
		}

	} elseif ($_REQUEST["action"] == "unadd2cart" && IntVal($_REQUEST["basket_id"]) > 0) {

		$result = \CSaleBasket::Delete($_REQUEST["basket_id"]);
	
	}
}
if ($result) {
	// отправляем ответ
	echo json_encode([
		"result" => "ok",
		"message" => "Товар добавлен/удален",
		"data" => $data
	]);
} else {
	echo json_encode([
		"result" => "error",
		"message" => "Ошибка " . ($_REQUEST ? json_encode($_REQUEST) : false)
	]);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
