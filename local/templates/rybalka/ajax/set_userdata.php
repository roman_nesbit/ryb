<?
define("NO_KEEP_STATISTIC", true);
define('NO_AGENT_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Rybalka\Marketplace\Exception\RegistrationException;
use Rybalka\Marketplace\Http\Exception\OkException;
use Rybalka\Marketplace\Http\StatusCode;
use Rybalka\Marketplace\Util\UserUtil;

header('Content-Type: application/json');

try {
	global $USER;

	if(!$USER->IsAuthorized())
	{
		throw new RegistrationException('Not authorized', [
			'status' => StatusCode::UNAUTHORIZED,
			'field' => 'PHPSESSID'
		]);
	}

	if (isset($_POST['CURRENT_PASSWORD']) 
		&& isset($_POST['NEW_PASSWORD_REQ']) 
		&& isset($_POST['NEW_PASSWORD_CONFIRM'])
	 ) {
		// Change password

		if(!UserUtil::checkCurrentUserPassword($_POST['CURRENT_PASSWORD']))
		{
			throw new RegistrationException('Bad password', [
				'status' => StatusCode::NOT_FOUND,
				'field' => 'CURRENT_PASSWORD'
			]);
		}

		if($_POST['NEW_PASSWORD_REQ'] != $_POST['NEW_PASSWORD_CONFIRM'])
		{
			throw new RegistrationException('New password and confirm_password are different', [
				'status' => StatusCode::FORBIDDEN,
				'field' => 'NEW_PASSWORD_REQ'
			]);
		}

		$result = UserUtil::changeCurrentUserPassword(
			$_POST['NEW_PASSWORD_REQ'],
			$_POST['NEW_PASSWORD_CONFIRM']
		);
		if($result !== true)
		{
			throw new RegistrationException($result, [
				'status' => StatusCode::INTERNAL_SERVER_ERROR,
				'field' => 'PHPSESSID'
			]);
		}

		throw new OkException('Ok');

	}
	elseif(isset($_POST['NAME']) 
		&& isset($_POST['LAST_NAME'])
	) {
		// Change personal data

		$result = UserUtil::changeCurrentUserData([
			'NAME' => $_POST['NAME'],
			'LAST_NAME' => $_POST['LAST_NAME'],
		]);
		if ($result !== true)
		{
			throw new RegistrationException($result, [
				'status' => StatusCode::INTERNAL_SERVER_ERROR,
				'field' => 'PHPSESSID'
			]);
		}

		throw new OkException('Ok');
	}
	elseif(isset($_POST['UF_CITY_ID']) 
		&& isset($_POST['UF_NP_OFFICE_ID'])
	) {
		// Change delivery preferencies

		$result = UserUtil::changeCurrentUserData([
			'UF_CITY_ID' => $_POST['UF_CITY_ID'],
			'UF_NP_OFFICE_ID' => $_POST['UF_NP_OFFICE_ID'],
		]);

		if ($result !== true) {
			throw new RegistrationException($result, [
				'status' => StatusCode::INTERNAL_SERVER_ERROR,
				'field' => 'PHPSESSID'
			]);
		}

		throw new OkException('Ok');
	}
	else
	{
		throw new RegistrationException('Bad request', [
			'status' => StatusCode::BAD_REQUEST,
			'field' => 'PHPSESSID'
		]);
	}

}
catch (RegistrationException $e)
{
	echo json_encode([
		'meta' => $e->getData(),
	]);
}
catch (OkException $e)
{
	echo json_encode([
		'meta' => [
			'status' => $e->getCode()
		]
	]);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");