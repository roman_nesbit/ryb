<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(['fx']);
$curPage = $APPLICATION->GetCurPage(true);
use Bitrix\Main\Page\Asset;?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<link rel="shortcut icon" type="image/x-icon" href="<?=htmlspecialcharsbx(SITE_DIR)?>favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/safari-pinned-tab.svg" color="#001e41">
    <meta name="msapplication-TileColor" content="#001e41">
    <meta name="msapplication-config" content="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#001e41">
<?php
    $APPLICATION->ShowHead();
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/rmp.min.js');
?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159675076-1"></script>
    <script async src="https://www.google.com/recaptcha/api.js?render=<?=getenv('RECAPTCHA_SITE_KEY')?>"></script>
    <script src="//code.jivosite.com/widget/8Qu3enPXLG" async></script>
</head>

<?
	if(http_response_code() == "404") $thePageCode = "404";
	else $thePageCode = ($_REQUEST["PAGECODE"] ? $_REQUEST["PAGECODE"] : $APPLICATION->GetDirProperty("PAGECODE"));

	if($_REQUEST["BREADCRUMBS"] == "Y" || isset($_REQUEST["CATALOG"])) {
		$showBreadcrumbs = "Y";
	} elseif($APPLICATION->GetDirProperty("BREADCRUMBS") == "Y") {
		$showBreadcrumbs = "Y";
	} else {
		$showBreadcrumbs = "N";
	}

	// $GLOBALS[SIMPLE_TEMPLATE] = in_array($thePageCode, array("checkout")) && !$_REQUEST[ORDER_ID]; 
?>

<body>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=914502312376209&ev=PageView&noscript=1"/></noscript>
<script type="text/javascript" data-skip-moving="true">
    window.rmp = window.rmp || [];
</script>
<div id="panel"><?$APPLICATION->ShowPanel();?></div><?
$APPLICATION->IncludeComponent("h2o:favorites.add", "", []);
?><header id="header" class="header">
	<div class="headerMain">
		<div class="container inner">
			<div class="left">
				<button class="menuButton _showNavButton">
					<span></span>
					<span></span>
					<span></span>
				</button>

				<? if($APPLICATION->GetCurPage(false) == SITE_DIR): ?>
					<span class="logo">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="" />
					</span>
				<? else: ?>
					<a href="/" class="logo">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="" />
					</a>
				<? endif; ?>
			</div>
			<div class="right">
				<div class="mainBtnBox">
					<!--<a href="#" class="btn block">Подать объявление</a>-->
					<span class="btn btnGreen block _showNavButton"><?=GetMessage("CATALOG")?></span>
				</div>
				<?
				$APPLICATION->IncludeComponent(
					"bitrix:search.title",
					"header",
					Array(
						"SHOW_INPUT" => "Y",
						"INPUT_ID" => "title-search-input-header",
						"CONTAINER_ID" => "title-search-header",
						"PRICE_CODE" => array("UAH"),
						"PRICE_VAT_INCLUDE" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "150",
						"SHOW_PREVIEW" => "Y",
						"PREVIEW_WIDTH" => "50",
						"PREVIEW_HEIGHT" => "50",
						"CONVERT_CURRENCY" => "Y",
						"CURRENCY_ID" => "UAH",
						"PAGE" => "#SITE_DIR#search/",
						"NUM_CATEGORIES" => "1",
						"TOP_COUNT" => "10",
						"ORDER" => "date",
						"USE_LANGUAGE_GUESS" => "Y",
						"CHECK_DATES" => "Y",
						"SHOW_OTHERS" => "Y",
						"CATEGORY_0_TITLE" => "Товары",
						"CATEGORY_0" => array("iblock_catalog"),
						// "CATEGORY_0_iblock_news" => array("all"),
						// "CATEGORY_OTHERS_TITLE" => "Прочее",
					)
				);?>
				<button class="iconBtn closeNav _closeNavButton">
					<svg>
						<use href="#closeIcon" />
					</svg>
				</button>
				<div class="translation">
					<!-- <a class="active" href="#ru">Ru</a>
					<span></span>
					<a href="#ua">Ua</a> -->
				</div>
				<button class="iconBtn search _showSearchButton">
					<svg>
						<use href="#searchIcon" />
					</svg>
				</button><?
					$APPLICATION->IncludeComponent("h2o:favorites.line", "", array("URL_LIST" => "/personal/favorites/"));
					
					$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line",
						"",
						Array(
							"HIDE_ON_BASKET_PAGES" => "Y",
							"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
							"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
							"PATH_TO_PERSONAL" => SITE_DIR."personal/",
							"PATH_TO_PROFILE" => SITE_DIR."personal/",
							"PATH_TO_REGISTER" => SITE_DIR."login/",
							"POSITION_FIXED" => "N",
							"POSITION_HORIZONTAL" => "right",
							"POSITION_VERTICAL" => "top",
							"SHOW_AUTHOR" => "N",
							"SHOW_DELAY" => "N",
							"SHOW_EMPTY_VALUES" => "N",
							"SHOW_IMAGE" => "N",
							"SHOW_NOTAVAIL" => "N",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_PERSONAL_LINK" => "N",
							"SHOW_PRICE" => "N",
							"SHOW_PRODUCTS" => "Y",
							"SHOW_SUMMARY" => "N",
							"SHOW_TOTAL_PRICE" => "N"
						)
					);
				?><button class="iconBtn user _showUserMenu <?=($USER->isAuthorized() ? 'active' : '')?>">
					<svg>
                        <use href="#userIcon" />
                    </svg>
                    <svg>
                        <use href="#userIconFull" />
                    </svg>
				</button>
			</div>
		</div>
	</div>
</header>
<nav class="navigation _navigation">
	<div class="container inner">
		<!-- <div class="postAdds">
			<a href="#" class="btn block">Подать объявление</a>
		</div> -->
		<div class="navBox">
			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list",
				"nav",
				Array(
					"ADD_SECTIONS_CHAIN" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COUNT_ELEMENTS" => "Y",
					"FILTER_NAME" => "sectionsFilter",
					"IBLOCK_ID" => "4",
					"IBLOCK_TYPE" => "catalog",
					"SECTION_CODE" => "",
					"SECTION_FIELDS" => array("",""),
					"SECTION_ID" => "",
					"SECTION_URL" => "",
					"SECTION_USER_FIELDS" => array("UF_ICON_ID"),
					"SHOW_PARENT_NAME" => "Y",
					"TOP_DEPTH" => "3",
					"VIEW_MODE" => "LINE"
				)
			);?>
		</div>
		<div class="upToTop">
			<a href="#header">
				Вернуться к началу
				<span class="arrowUp">
					<svg>
						<use href="#arrowIcon" />
					</svg>
					<svg>
						<use href="#arrowIcon" />
					</svg>
				</span>
			</a>
		</div>
	</div>
</nav>
<div class="container userMenuWrapper">
	<div class="userMenu">
		<!--<div class="head small">-->
		<div class="head">
			<p>Вход</p>
			<button class="iconBtn close _closeUserMenu">
				<svg>
					<use href="#closeIcon" />
				</svg>
			</button>
		</div>
		<div class="body">
			<? $APPLICATION->IncludeComponent("bitrix:system.auth.form",
				"header",
				Array(
					"REGISTER_URL" => "/login/",
					"FORGOT_PASSWORD_URL" => "",
					"PROFILE_URL" => "/personal/",
					"SHOW_ERRORS" => "Y",
					"MERCHANT_APPLICATION_URL" => "/merchant-application/",
				)
			);?>
		</div>
	</div>
</div>
<div class="container content">
	<?
		if($showBreadcrumbs == "Y") {
			$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
					"START_FROM" => "0", 
					"PATH" => "", 
					"SITE_ID" => "s1" 
				)
			);
		}
	?>