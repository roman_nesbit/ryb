<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

// $dbBasketItems = CSaleBasket::GetList(
// 	[
// 		"NAME" => "ASC",
// 		"ID" => "ASC"
// 	],
// 	[
// 		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
// 		"LID" => SITE_ID,
// 		"PRODUCT_ID" => $arResult['ID'],
// 		"DELAY" => "N" //Исключая отложенные
// 	],
// 	false,
// 	false,
// 	['PRODUCT_ID']
// );
// if($arItemsBasket = $dbBasketItems->Fetch()) {
// 	$arResult['IN_BASKET'] = true;
// } else {
// 	$arResult['IN_BASKET'] = false;
// }

// exo($arResult['DETAIL_PICTURE']);