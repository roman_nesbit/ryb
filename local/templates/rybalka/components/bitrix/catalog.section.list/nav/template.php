<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); 

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<ul class="nav">
	<? foreach($arResult['SECTIONS'] as &$arSection) {
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
		?>
		<li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<a
				href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
				title="<? echo $arSection['PICTURE']['TITLE']; ?>">
				<span class="categoryIcon">
					<svg>
						<use href="#<?=$arSection['UF_ICON_ID']?>" />
					</svg>
				</span>
				<?=$arSection['NAME']?>
				<span class="arrowIcon">
					<svg>
						<use href="#arrowIcon" />
					</svg>
				</span>
			</a><? 
			if(count($arSection['SUBSECTIONS'])) {
				?><ul><?
					foreach($arSection['SUBSECTIONS'] as &$arSubsection) {
						$this->AddEditAction($arSubsection['ID'], $arSubsection['EDIT_LINK'], $strSectionEdit);
						$this->AddDeleteAction($arSubsection['ID'], $arSubsection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);				
						?><li id="<? echo $this->GetEditAreaId($arSubsection['ID']); ?>">
							<a href="<?=$arSubsection['SECTION_PAGE_URL']?>">
								<?=$arSubsection['NAME']?>
								<span class="arrowIcon">
									<svg>
										<use href="#arrowIcon" />
									</svg>
								</span>
							</a><?
							if(count($arSubsection['SUBSECTIONS'])) {
								?><ul><?
									foreach($arSubsection['SUBSECTIONS'] as &$arSubsubsection) {
										$this->AddEditAction($arSubsubsection['ID'], $arSubsubsection['EDIT_LINK'], $strSectionEdit);
										$this->AddDeleteAction($arSubsubsection['ID'], $arSubsubsection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
										?><li id="<? echo $this->GetEditAreaId($arSubsubsection['ID']); ?>">
											<a href="<?=$arSubsubsection['SECTION_PAGE_URL']?>">
												<?=$arSubsubsection['NAME']?>
											</a>
										</li><?
									}
								?></ul><?
							}
						?></li><?
					}
				?></ul><?
			}
		?></li><?
	}
?></ul>
<? if($arParams['SHOW_MORE'] == 'Y'): ?>
	<div class="showMoreBtn">
		<button class="_showMoreCategories">
			<span class="showMoreTxt"><?=GetMessage('CSL_NAV_SHOW_MORE_TEXT')?></span>
			<span class="showLessTxt"><?=GetMessage('CSL_NAV_SHOW_LESS_TEXT')?></span>
			<span class="arrows">
			<svg>
				<use href="#arrowIcon" />
			</svg>
			<svg>
				<use href="#arrowIcon" />
			</svg>
		</span>
		</button>
	</div>
<? endif; ?>