<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$theCurPage = $APPLICATION->GetCurPage(false);

$arNewSections = array();
$i1 = 0;
foreach ($arResult["SECTIONS"] as $key => $arSection1) {
	if($arSection1['ELEMENT_CNT'] == 0) continue;

	if($arSection1["DEPTH_LEVEL"] == 1) {
		$arNewSections[$i1]["ID"] = $arSection1["ID"];
		$arNewSections[$i1]["EDIT_LINK"] = $arSection1["EDIT_LINK"];
		$arNewSections[$i1]["DELETE_LINK"] = $arSection1["DELETE_LINK"];
		$arNewSections[$i1]["NAME"] = $arSection1["NAME"];
		$arNewSections[$i1]["CODE"] = $arSection1["CODE"];
		$arNewSections[$i1]["SECTION_PAGE_URL"] = $arSection1["SECTION_PAGE_URL"];
		$arNewSections[$i1]["SELECTED"] = ($arNewSections[$i1]["SECTION_PAGE_URL"] === $theCurPage);
		$arNewSections[$i1]["UF_ICON_ID"] = $arSection1["UF_ICON_ID"];
		if($arSection1[UF_SVG]) {
			$arNewSections[$i1][PICTURE][SRC] = CFile::GetPath($arSection1[UF_SVG]);
		}
		$i2 = 0;
		foreach ($arResult["SECTIONS"] as $key => $arSection2) {
			if($arSection2['ELEMENT_CNT'] == 0) continue;

			if($arSection2["DEPTH_LEVEL"] == 2 && $arSection2["LEFT_MARGIN"] > $arSection1["LEFT_MARGIN"] && $arSection2["RIGHT_MARGIN"] < $arSection1["RIGHT_MARGIN"]) {
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["ID"] = $arSection2["ID"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["EDIT_LINK"] = $arSection2["EDIT_LINK"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["DELETE_LINK"] = $arSection2["DELETE_LINK"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["NAME"] = $arSection2["NAME"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["CODE"] = $arSection2["CODE"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["SECTION_PAGE_URL"] = $arSection2["SECTION_PAGE_URL"];
				$arNewSections[$i1]["SUBSECTIONS"][$i2]["SELECTED"] = ($arNewSections[$i1]["SUBSECTIONS"][$i2]["SECTION_PAGE_URL"] === $theCurPage);
				$i3 = 0;
				foreach ($arResult["SECTIONS"] as $key => $arSection3) {
					if($arSection3['ELEMENT_CNT'] == 0) continue;

					if($arSection3["DEPTH_LEVEL"] == 3 && $arSection3["LEFT_MARGIN"] > $arSection2["LEFT_MARGIN"] && $arSection3["RIGHT_MARGIN"] < $arSection2["RIGHT_MARGIN"]) {
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["ID"] = $arSection3["ID"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["EDIT_LINK"] = $arSection3["EDIT_LINK"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["DELETE_LINK"] = $arSection3["DELETE_LINK"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["NAME"] = $arSection3["NAME"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["CODE"] = $arSection3["CODE"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["SECTION_PAGE_URL"] = $arSection3["SECTION_PAGE_URL"];
						$arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["SELECTED"] = ($arNewSections[$i1]["SUBSECTIONS"][$i2]["SUBSECTIONS"][$i3]["SECTION_PAGE_URL"] === $theCurPage);
						$i3++;
					}
				}
				$i2++;
			}
		}
		$i1++;
	}
}

$arResult['SECTIONS'] = $arNewSections;

// exo($arResult['SECTIONS']);
?>