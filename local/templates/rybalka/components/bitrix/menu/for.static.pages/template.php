<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
foreach ($arResult as $itemIdex => $arItem) : 
	?><li<?=($arItem['SELECTED'] ? ' class="active"' : '')?>>
		<a href="<?= $arItem['LINK'] ?>">
			<span class="categoryIcon">
				<svg>
					<use href="#<?=$arItem['PARAMS']['ICON']?>" />
				</svg>
			</span>
			<?= $arItem['TEXT'] ?>
			<span class="arrowIcon">
				<svg>
					<use href="#arrowIcon" />
				</svg>
			</span>
		</a>
	</li><? 
endforeach; ?>