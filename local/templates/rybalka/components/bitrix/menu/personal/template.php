<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<nav class="innerNav <?=($arParams['HIDE_BASE_CLASS'] == 'Y' ? '' : 'base')?>">
	<ul>
		<? foreach($arResult as $itemIdex => $arItem): ?>
			<li class="<?=($arItem['SELECTED'] ? 'active' : '')?>">
				<a href="<?=$arItem['LINK']?>">
					<svg>
						<use href="#<?=$arItem['PARAMS']['ICON']?>" />
					</svg>
					<span><?=$arItem['TEXT']?></span>
				</a>
			</li>
		<? endforeach; ?>
	</ul>
</nav>