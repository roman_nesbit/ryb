<?

use Rybalka\Marketplace\Util\UserUtil;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

?>
<h3 class="title"><?=GetMessage('sys_forgot_pass_sms_note')?></h3>
<div class="separator"></div>
<form method="post" action="<?= SITE_TEMPLATE_PATH ?>/ajax/get_resetform.php" id="resetform_step_5">
	<input type="hidden" name="STEP" value="5" />
	<input type="hidden" name="verificationId" value="<?= $arResult['verificationId'] ?>" />
	<div class="fieldRow">
		<div class="inputField">
			<label><?=GetMessage('sys_forgot_pass_phone')?></label>
			<input type="tel" name="phoneNumber" value="<?=$arResult['phoneNumber']?>" readonly />
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'code' ? 'withError' : '')?>">
			<label><?=GetMessage('sys_forgot_pass_sms_code')?></label>
			<input type="tel" name="code" value="<?=$arResult['code']?>" required />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'code') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'], [
                        '#WAITING#' => $arResult['ERROR']['DATA']['WAITING']
                    ])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('AUTH_PASSWORD_REQ')?></label>
			<span class="helperLabel"><?=UserUtil::getGroupPolicy()?></span>
			<input type="password" name="USER_PASSWORD" value="" required minlength="<?=$_SESSION['SESS_AUTH']['POLICY']['PASSWORD_LENGTH'] ?? 6?> autocomplete="new-password" />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('AUTH_CONFIRM')?></label>
			<input type="password" name="USER_CONFIRM_PASSWORD" value="" required minlength="<?=$_SESSION['SESS_AUTH']['POLICY']['PASSWORD_LENGTH'] ?? 6?>" autocomplete="new-password" />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<button class="btn fullWidth withIcon btnWithLoading">
			<svg class="loadingIcon">
				<use href="#loadingIcon" />
			</svg>
			<span><?=GetMessage('sys_forgot_pass_save')?></span>
		</button>
	</div>
	<div class="codeInfoBox">
		<span><?=GetMessage('sys_forgot_pass_no_sms_note')?></span>
		<button class="resendBtn" type="button" id="changePhoneNumber"><?=GetMessage('sys_forgot_pass_no_sms_action')?></button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#resetform_step_5");

	form.find("[required]").filter(function() {
		return !$(this).val() || $(this).parent(".inputField").hasClass("withError");
	}).first().focus();

	form.on("submit", function(event) {
		event.preventDefault();
		form.find("button").addClass("disabled").addClass("isLoading");
        form.find(".errorMsg").hide();
        form.find(".withError").removeClass("withError");
		var data = form.serializeArray();
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: data,
			success: function(data) {
				$("#resetFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});

	$("#changePhoneNumber").on("click", function() {
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: {
				'phoneNumberOrEmail': '<?=$arResult['phoneNumber']?>',
			},
			success: function(data) {
				$("#resetFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>