<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Rybalka\Marketplace\Exception;
use Rybalka\Marketplace\Util\Phone\Normalizer;
use Rybalka\Marketplace\Http\StatusCode;
use Rybalka\Marketplace\Util\Phone\VerificationTable;
use Rybalka\Marketplace\Util\Recaptcha;
use Rybalka\Marketplace\Util\UserUtil;

// exo($_POST);
// exo($_GET);

try {
    switch ($_POST['STEP']) {
        case 1:
			if (!$_POST['phoneNumberOrEmail'] || !$_POST['captchaToken']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'phoneNumberOrEmail']);
			}

			// Перевірка каптчі
			if(!Recaptcha::isHuman($_POST['captchaToken']))
			{
				throw new Rybalka\Marketplace\Http\Exception\UnauthorizedException(StatusCode::UNAUTHORIZED, ['FIELD' => 'phoneNumberOrEmail']);
			}

			// Що передали в phoneNumberOrEmail?
			if(filter_var($_POST['phoneNumberOrEmail'], FILTER_VALIDATE_EMAIL)) {
				$email = $_POST['phoneNumberOrEmail'];

				// Шукаємо користувача по email і отримуємо його id
				if(!$userData = UserUtil::getUserDataByEmail($email)) {
					$arResult['EMAIL'] = $email;
					$arResult['STEP'] = 2; // Go to Step 2 (Not found)
					unset($_POST);
				} else {
					// Знайшли і відправляємо йому код
					if (UserUtil::sendPassword($userData['LOGIN'], $userData['EMAIL'])) {
						$arResult['LOGIN'] = $userData['LOGIN'];
						$arResult['STEP'] = 3; // Go to Step 3 (The code has been sent)
					} else {
						throw new Exception\RegistrationException(StatusCode::NOT_FOUND, ['FIELD' => 'phoneNumberOrEmail']);
					}
				}
			} else {
				// Нормалізуємо номер телефона
				if (!$normalizedPhoneNumber = Normalizer::normalizePhoneNumber($_POST['phoneNumberOrEmail'])) {
					throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'phoneNumberOrEmail']);
				}

				// Шукаємо користувача по номеру
				if (!UserUtil::isUserByPhone($normalizedPhoneNumber)) {
					$arResult['phoneNumber'] = $normalizedPhoneNumber;
                    $arResult['STEP'] = 2; // Go to Step 2 (Not found)
                    unset($_POST);
				} else {
					// Номер зареєстрований в системі
					// Send code
					if ($response = VerificationTable::sendCode(
						$normalizedPhoneNumber,
						\Bitrix\Main\Service\GeoIp\Manager::getRealIp()
					)) {
						$arResult['verificationId'] = $response['verificationId'];
						$arResult['STEP'] = 5; // Go to Step 5 (SMS code input)
						$arResult['phoneNumber'] = $normalizedPhoneNumber;
						unset($_POST);
					} else {
						throw new Exception\RegistrationException($response['meta']['status'], [
							'FIELD' => 'phoneNumberOrEmail',
							'WAITING' => $response['data']['waitingTimeInSeconds']
						]);
					}
				}
			}
            break;
		
		case 3:
            if (!$_POST['LOGIN']) {
                throw new Exception\RegistrationException(StatusCode::UNAUTHORIZED, ['FIELD' => 'LOGIN']);
            }
			if(!$_POST['USER_CHECKWORD']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_CHECKWORD']);
			}
			if(!$_POST['USER_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_PASSWORD']);
			}
			if(!$_POST['USER_CONFIRM_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_CONFIRM_PASSWORD']);
			}

			// Check for password = confirm_password
			if ($_POST['USER_PASSWORD'] !== $_POST['USER_CONFIRM_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::FORBIDDEN, ['FIELD' => 'USER_PASSWORD']);
			}

			// Set new password
			$response = UserUtil::setNewPassword(
				$_POST['LOGIN'],
				$_POST['USER_CHECKWORD'],
				$_POST['USER_PASSWORD'],
				$_POST['USER_CONFIRM_PASSWORD']
			);
			if($response === true) {
				$arResult['STEP'] = 4; // Go to Step 4 (Thankyou page)
                unset($_POST);
			} else {
				throw new Exception\RegistrationException(StatusCode::INTERNAL_SERVER_ERROR, ['FIELD' => 'LOGIN']);
			}
			break;

		case 5:
			if (!$_POST['verificationId'] || !$_POST['code']) {
				throw new Exception\ RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'code']);
			}

			if(!$_POST['USER_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_PASSWORD']);
			}
			if(!$_POST['USER_CONFIRM_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_CONFIRM_PASSWORD']);
			}

			// Check for password = confirm_password
			if ($_POST['USER_PASSWORD'] !== $_POST['USER_CONFIRM_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::FORBIDDEN, ['FIELD' => 'USER_PASSWORD']);
			}

			// Verify code
			if ($response = VerificationTable::verify(
					$_POST['verificationId'],
					$_POST['code']
				)) {
				// Get verified phone number
				if (!$phoneNumber = VerificationTable::getVerifiedPhoneNumber($response['verificationToken'])) {
					throw new Exception\RegistrationException(StatusCode::UNAUTHORIZED, ['FIELD' => 'phoneNumber']);
				}

				// Set new password
				$response = UserUtil::setNewPasswordByPhone(
					$phoneNumber,
					$_POST['USER_PASSWORD'],
					$_POST['USER_CONFIRM_PASSWORD']
				);
				if ($response === true) {
					$arResult['STEP'] = 4; // Go to Step 4 (Thankyou page)
					unset($_POST);
				} else {
					throw new Exception\RegistrationException(StatusCode::INTERNAL_SERVER_ERROR, ['FIELD' => 'phoneNumber']);
				}
				unset($_POST);
			} else {
				throw new Exception\ RegistrationException($response['meta']['status'], [
					'FIELD' => 'code',
					'WAITING' => $response['data']['waitingTimeInSeconds']
				]);
			}
			break;

		default:
			if ($_GET['STEP'] == 3 && $_GET['USER_CHECKWORD'] && $_GET['USER_LOGIN'] && $_GET['forgot_password'] == 'yes') {
				$arResult['STEP'] = 3; // Get to Step 3 (The code has been sent)
				$arResult['USER_CHECKWORD'] = $_GET['USER_CHECKWORD'];
				$arResult['LOGIN'] = $_GET['USER_LOGIN'];
			} else {
				$arResult['STEP'] = 1; // Go to Step 1 (Phone/Email input)
			}
            break;

	}
}
catch (Exception\RegistrationException $e) {
	$arResult['ERROR'] = [
		'MESSAGE' => $e->getMessage(),
		'DATA' => $e->getData(),
	];
	foreach($_POST as $key => $value) {
		$arResult[$key] = $value;
	}
}
catch (Rybalka\Marketplace\Http\Exception\TooManyRequestsException $e) {
	switch($_POST['STEP']) {
		case 1:
			$errorField = 'phoneNumberOrEmail';
			break;
	}

	$arResult['ERROR'] = [
		'MESSAGE' => StatusCode::TOO_MANY_REQUESTS,
		'DATA' => [
			'FIELD' => $errorField,
			'WAITING' => $e->getResponseBody()['waitingTimeInSeconds'],
		],
	];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\BadRequestException $e) {
	switch ($_POST['STEP']) {
        case 1:
            $errorField = 'phoneNumberOrEmail';
			break;
		case 5:
			$errorField = 'code';
			break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::BAD_REQUEST,
        'DATA' => [
            'FIELD' => $errorField,
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\ForbiddenException $e) {
	switch ($_POST['STEP']) {
        case 1:
            $errorField = 'phoneNumberOrEmail';
			break;
		case 5:
			$errorField = 'code';
			break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::FORBIDDEN,
        'DATA' => [
            'FIELD' => $errorField,
            'WAITING' => $e->getResponseBody()['waitingTimeInSeconds'],
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\NotFoundException $e) {
	switch ($_POST['STEP']) {
        case 3:
            $errorField = 'code';
            break;
		case 5:
			$errorField = 'code';
			break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::NOT_FOUND,
        'DATA' => [
			'FIELD' => $errorField,
			'WAITING' => 0,
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\UnauthorizedException $e) {
	switch ($_POST['STEP']) {
        case 1:
            $errorField = 'phoneNumberOrEmail';
            break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::UNAUTHORIZED,
        'DATA' => [
            'FIELD' => $errorField,
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }

}