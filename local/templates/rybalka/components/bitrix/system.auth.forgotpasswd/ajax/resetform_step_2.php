<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<h3 class="title"><?=GetMessage('sys_forgot_pass_not_registered')?></h3>
<div class="separator"></div>
<form method="post" action="<?=SITE_TEMPLATE_PATH?>/ajax/get_resetform.php" id="resetform_step_2">
	<div class="fieldRow">
		<div class="inputField">
			<? if($arResult['EMAIL']): ?>
				<label><?=GetMessage('sys_forgot_pass_email') ?></label>
				<input type="text" name="phoneNumberOrEmail" value="<?=$arResult['EMAIL']?>" readonly />
			<? elseif($arResult['phoneNumber']): ?>
				<label><?=GetMessage('sys_forgot_pass_phone') ?></label>
				<input type="text" name="phoneNumberOrEmail" value="<?=$arResult['phoneNumber']?>" readonly />
			<? endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<a class="btn fullWidth" href="/login/?register=yes">
			<?=GetMessage('sys_forgot_pass_register')?>
		</a>
	</div>
	<div class="codeInfoBox">
		<span></span>
		<button class="resendBtn" type="button" id="changePhoneNumber"><?=GetMessage('sys_forgot_pass_change')?></button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#resetform_step_2");

	$("#changePhoneNumber").on("click", function() {
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: {
				'phoneNumberOrEmail': '<?=$arResult['EMAIL']?>',
			},
			success: function(data) {
				$("#resetFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>