<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<h3 class="title"><?=GetMessage('sys_forgot_pass_thankyou')?></h3>
<div class="buttonsRow">
	<a class="btn fullWidth" href="/login/?login=yes">
		<?=GetMessage('AUTH_AUTH')?>
	</a>
</div>