<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<form method="post" action="<?=SITE_TEMPLATE_PATH?>/ajax/get_resetform.php" id="resetform_step_1">
	<input type="hidden" name="STEP" value="1" />
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'phoneNumberOrEmail' ? 'withError' : '')?>">
			<label><?=GetMessage('sys_forgot_pass_phone_or_email') ?></label>
			<span class="helperLabel"><?=GetMessage('sys_forgot_pass_phone_or_email_sample')?></span>
			<input type="text" name="phoneNumberOrEmail" value="<?=$_REQUEST['phoneNumberOrEmail']?>" required />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'phoneNumberOrEmail') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'], [
						'#WAITING#' => $arResult['ERROR']['DATA']['WAITING']
					])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<button class="btn fullWidth withIcon btnWithLoading" type="submit">
			<svg class="loadingIcon">
				<use href="#loadingIcon" />
			</svg>
			<span><?=GetMessage('AUTH_SEND')?></span>
		</button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#resetform_step_1");

	form.find("[required]").filter(function() {
		return !$(this).val() || $(this).parent(".inputField").hasClass("withError");
	}).first().focus();

	form.on("submit", function(event) {
		event.preventDefault();
		var submitButton = form.find("button[type='submit']");
		submitButton.attr("disabled", "disabled").addClass("disabled").addClass("isLoading");
		form.find(".errorMsg").hide();
		form.find(".withError").removeClass("withError");
		grecaptcha.ready(function() {
			grecaptcha.execute('<?= getenv('RECAPTCHA_SITE_KEY') ?>', {
				action: 'registration'
			}).then(function(token) {
				var data = form.serializeArray();
				data.push({
					name: 'captchaToken',
					value: token
				});
				$.ajax({
					type: form.attr("method"),
					url: form.attr("action"),
					data: data,
					success: function(data) {
						$("#resetFormHolder").html(data);
					},
					error: function(xhr, ajaxOptions, thrownError) {
					},
					complete: function() {
						submitButton.removeAttr("disabled").removeClass("disabled").removeClass("isLoading");
					}
				});
			});;
		});
	});

	if(form.find("waiting").length) {
		var submitButton = form.find("button[type='submit']");

		submitButton.attr("disabled", "disabled").addClass("disabled");

		form.find("waiting").each(function() {
			var waiting = $(this);

			var wiatingTimer = setInterval(function() {
				var curSeconds = parseInt(waiting.html()) - 1;
				waiting.html(curSeconds);

				if(curSeconds == 0) {
                    clearInterval(wiatingTimer);
					submitButton.removeAttr("disabled").removeClass("disabled").removeClass("isLoading");
					waiting.parents(".errorMsg").hide().parents(".withError").removeClass("withError");
				}
			}, 1000);
		});
	}
</script>