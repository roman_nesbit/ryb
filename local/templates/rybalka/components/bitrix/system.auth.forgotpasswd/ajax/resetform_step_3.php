<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<h3 class="title"><?=GetMessage('sys_forgot_pass_note_email')?></h3>
<div class="separator"></div>
<form method="post" action="<?= SITE_TEMPLATE_PATH ?>/ajax/get_resetform.php" id="resetform_step_3">
	<input type="hidden" name="STEP" value="3" />
	<div class="fieldRow">
		<input type="hidden" name="LOGIN" value="<?=$arResult['LOGIN']?>" />
		<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'LOGIN') : ?>
			<div class="errorMsg">
				<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
			</div>
		<?php endif; ?>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_CHECKWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('sys_forgot_pass_checkword')?></label>
			<input type="text" name="USER_CHECKWORD" value="<?=$arResult['USER_CHECKWORD']?>" required />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_CHECKWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('sys_forgot_pass_password')?></label>
			<input type="password" name="USER_PASSWORD" value="<?=$arResult['USER_PASSWORD']?>" required autocomplete="new-password" />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('sys_forgot_pass_confirm_password')?></label>
			<input type="password" name="USER_CONFIRM_PASSWORD" value="<?=$arResult['USER_CONFIRM_PASSWORD']?>" required autocomplete="new-password" />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<button class="btn fullWidth withIcon btnWithLoading">
			<svg class="loadingIcon">
				<use href="#loadingIcon" />
			</svg>
			<span><?=GetMessage('sys_forgot_pass_save')?></span>
		</button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#resetform_step_3");

	form.find("[required]").filter(function() {
		return !$(this).val() || $(this).parent(".inputField").hasClass("withError");
	}).first().focus();

	form.on("submit", function(event) {
		event.preventDefault();
		form.find("button").addClass("disabled").addClass("isLoading");
        form.find(".errorMsg").hide();
        form.find(".withError").removeClass("withError");
		var data = form.serializeArray();
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: data,
			success: function(data) {
				$("#resetFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>