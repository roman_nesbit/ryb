<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->AddChainItem(GetMessage('AUTH_CAPTION'));
?>

<section class="middleContainer formsContainer">
	<h1 class="pageTitle noBorder"><?=GetMessage('AUTH_CAPTION')?></h1>
	<div class="middleInner">
		<div class="mainFormsHolder" id="resetFormHolder"><?php
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.forgotpasswd",
				"ajax",
				array()
			);
		?></div>
	</div>
</section>