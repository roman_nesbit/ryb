<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;

$APPLICATION->AddChainItem($_REQUEST['order_id'], "/personal/order/" . $_REQUEST['order_id'] . "/");
$APPLICATION->AddChainItem(Loc::getMessage('SALE_CANCEL'));
?>
<section class="middleContainer formsContainer">
	<h1 class="pageTitle noBorder"><?=Loc::getMessage('SALE_CANCEL')?></h1>
	<div class="middleInner">
		<div class="mainFormsHolder">
			<? if(strlen($arResult["ERROR_MESSAGE"])<=0): ?>
				<div class="title">
					<p><?=Loc::getMessage('SALE_CANCEL_ORDER1')?></p>
					<a class="blue" href="<?=$arResult['URL_TO_DETAIL']?>"><?=Loc::getMessage('SALE_CANCEL_ORDER2')?> № RM-2712160515</a> ?
					<p><?=Loc::getMessage('SALE_CANCEL_ORDER3')?></p>
				</div>
				<div class="separator"></div>
				<form method="post" action="<?=POST_FORM_ACTION_URI?>">
					<input type="hidden" name="CANCEL" value="Y">
					<?=bitrix_sessid_post()?>
					<input type="hidden" name="ID" value="<?=$arResult["ID"]?>">
					<div class="fieldRow">
						<div class="inputField">
							<label><?=Loc::getMessage('SALE_CANCEL_ORDER4')?></label>
							<textarea name="REASON_CANCELED" required></textarea>
						</div>
					</div>
					<div class="buttonsRow">
						<button class="btn fullWidth withIcon btnWithLoading"
							type="submit"
							name="action"
							value="<?=GetMessage("SALE_CANCEL_ORDER_BTN")?>">
							<svg class="loadingIcon">
								<use href="#loadingIcon" />
							</svg>
							<span><?=Loc::getMessage('SALE_CANCEL_ORDER_BTN')?></span>
						</button>
					</div>
				</form>
			<? else: ?>
				<div class="title">
					<?=ShowError($arResult["ERROR_MESSAGE"]);?>
				</div>
				<div class="buttonsRow">
					<a href="/personal/order/" class="btn fullWidth"><?=GetMessage("SALE_RECORDS_LIST")?></a>
				</div>
			<? endif; ?>
		</div>
	</div>
</section>