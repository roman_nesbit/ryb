<?
$MESS['CATALOG_PERSONAL_RECOM'] = 'Personal recommendations';
$MESS['CT_BCS_TPL_SORT_BY_POPULARITY'] = 'popularity';
$MESS['CT_BCS_TPL_SORT_BY_PRICE_ASC']  = 'price ascending';
$MESS['CT_BCS_TPL_SORT_BY_PRICE_DESC'] = 'price descending';
$MESS['CT_BCS_TPL_SORT_BY_DISCOUNT']   = 'discount rate';
?>