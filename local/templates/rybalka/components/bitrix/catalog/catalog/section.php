<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

// Subsections count
// $subsectionsCount = CIBlockSection::GetCount([
// 	'IBLOCK_ID' => 4,
// 	'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
// 	'ACTIVE' => 'Y'
// ]);

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');// && $subsectionsCount == 0);

if ($isFilter)
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
		$arCurSection = array();
}


?><section class="productsPage">
	<h1 class="pageTitle""><? $APPLICATION->ShowTitle(false); ?></h1><?

	\Bitrix\Main\Data\StaticHtmlCache::getInstance()->markNonCacheable();

	$sortOptions = [
		'popularity' => [
            'title' => GetMessage('CT_BCS_TPL_SORT_BY_POPULARITY'),
            'field' => 'SHOW_COUNTER',
            'order' => 'DESC'
        ],
        'price_asc' => [
            'title' => GetMessage('CT_BCS_TPL_SORT_BY_PRICE_ASC'),
            'field' => 'CATALOG_PRICE_1',
            'order' => 'ASC',
        ],
        'price_desc' => [
            'title' => GetMessage('CT_BCS_TPL_SORT_BY_PRICE_DESC'),
            'field' => 'CATALOG_PRICE_1',
            'order' => 'DESC',
        ],
        'discount' => [
            'title' => GetMessage('CT_BCS_TPL_SORT_BY_DISCOUNT'),
            'field' => 'PROPERTY_IMPORT_OFFER_DISCOUNT_RATE',
            'order' => 'DESC'
        ],
    ];

    $GLOBALS['arAvailableSort'] = $sortOptions;

    $defaultSortMethod = 'popularity';

    $sortField = $sortOptions[$defaultSortMethod]['field'];
    $sortOrder = $sortOptions[$defaultSortMethod]['order'];

    $selectedSortOption = strtolower($_REQUEST['sort'] ?? $_SESSION['sort'] ?? '');
    
    if (isset($sortOptions[$selectedSortOption])) {
        $sortField = $sortOptions[$selectedSortOption]['field'];
        $sortOrder = $sortOptions[$selectedSortOption]['order'];
    }

    include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_horizontal.php");

?></section>