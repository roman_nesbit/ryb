<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
// exo($arResult['CATEGORIES']['READY']);
if (!$arResult["DISABLE_USE_BASKET"]) {
	if($arResult['NUM_PRODUCTS']) {
		$theTotal = 0;
		foreach($arResult['CATEGORIES']['READY'] as $arProduct) {
			$theTotal += $arProduct['QUANTITY'];
		}
		?><span><?=($theTotal > 99 ? '99+' : $theTotal)?></span><?
	}
	?><svg><use href="#cartIcon" /></svg><?
} ?>