<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>
<section class="cartPage">
	<h1 class="pageTitle">корзина</h1>
	<div class="cartInner">
		<h2><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></h2>
		<? if (!empty($arParams['EMPTY_BASKET_HINT_PATH'])) {
			?><p>
				<?=Loc::getMessage(
					'SBB_EMPTY_BASKET_HINT',
					[
						'#A1#' => '<a href="'.$arParams['EMPTY_BASKET_HINT_PATH'].'">',
						'#A2#' => '</a>',
					]
				)?>
			</p><?
		} ?>
	</div>
</section>