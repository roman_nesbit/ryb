<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<script id="basket-item-template" type="text/html">
	<div class="cartRow basket-items-list-item-container{{#SHOW_RESTORE}} removedProduct basket-items-list-item-container-expend{{/SHOW_RESTORE}}" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}" data-not-available="{{NOT_AVAILABLE}}">
		{{#SHOW_RESTORE}}
			<div class="removedInfo">
				<?=Loc::getMessage('SBB_GOOD_CAP')?> <strong>{{NAME}}</strong> <?=Loc::getMessage('SBB_BASKET_ITEM_DELETED')?>
			</div>
			<div class="recoverBox">
				<button class="recoverBtn"
					data-entity="basket-item-restore-button">
					<?=Loc::getMessage('SBB_BASKET_ITEM_RESTORE')?>
				</button>
				<span class="basket-items-list-item-clear-btn" data-entity="basket-item-close-restore-button"></span>
			</div>
		{{/SHOW_RESTORE}}
		{{^SHOW_RESTORE}}
			<div class="imgCell">	
				<a href="{{DETAIL_PAGE_URL}}">
					<img alt="{{NAME}}"
						src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}">
				</a>
			</div>
			<div class="mainCell">
				<div class="prodInfoCell prodInfo">
					<a href="{{DETAIL_PAGE_URL}}">
						<h2 class="name">{{NAME}}</h2>
					</a>
					{{#PROPS}}
						<p>
							<span class="basket-item-property-name">
								{{{NAME}}}
							</span>:
							<span class="basket-item-property-value"
								data-entity="basket-item-property-value" data-property-code="{{CODE}}">
								{{{VALUE}}}
							</span>
						</p>
					{{/PROPS}}
				</div>
				<div class="priceCell">
					<div class="price">
						{{{PRICE_FORMATED}}}
						{{#SHOW_DISCOUNT_PRICE}}
							<div class="oldPrice">{{{FULL_PRICE_FORMATED}}}</div>
						{{/SHOW_DISCOUNT_PRICE}}
					</div>

					<div class="basket-item-price-title">
						<?=Loc::getMessage('SBB_BASKET_ITEM_PRICE_FOR')?> {{MEASURE_RATIO}} {{MEASURE_TEXT}}
					</div>
					{{#SHOW_LOADING}}
						<div class="basket-items-list-item-overlay"></div>
					{{/SHOW_LOADING}}
				</div>

				<div class="counterCell">
					<div class="countItems" data-entity="basket-item-quantity-block">
						<button data-entity="basket-item-quantity-minus">-</button>
						<input type="text" class="basket-item-amount-filed" value="{{QUANTITY}}"
							{{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
							data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
							id="basket-item-quantity-{{ID}}">
						<button data-entity="basket-item-quantity-plus">+</button>
						{{#SHOW_LOADING}}
							<div class="basket-items-list-item-overlay"></div>
						{{/SHOW_LOADING}}
					</div>
				</div>
				<div class="totalPriceCell">
					<div class="price">
						<span id="basket-item-sum-price-{{ID}}">{{{SUM_PRICE_FORMATED}}}</span>
						{{#SHOW_DISCOUNT_PRICE}}
							<div class="oldPrice" id="basket-item-sum-price-old-{{ID}}">{{{SUM_FULL_PRICE_FORMATED}}}</div>
						{{/SHOW_DISCOUNT_PRICE}}
						{{#SHOW_LOADING}}
							<div class="basket-items-list-item-overlay"></div>
						{{/SHOW_LOADING}}
					</div>
				</div>
				<div class="buttonCell">
					<button class="removeItem" data-entity="basket-item-delete">
						<svg><use href="#closeIcon" /></svg>
					</button>
					{{#SHOW_LOADING}}
						<div class="basket-items-list-item-overlay"></div>
					{{/SHOW_LOADING}}
				</div>
			</div>
		{{/SHOW_RESTORE}}
	</div>
</script>