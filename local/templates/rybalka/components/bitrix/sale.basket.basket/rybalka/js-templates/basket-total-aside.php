<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-aside-template" type="text/html">
 	<div class="inner">
		<h3 class="title"><?=Loc::getMessage('SBB_ORDER_CART')?></h3>
		<h4 class="subTitle"><?=Loc::getMessage('SBB_ORDER_CALC')?></h4>
		<div class="calcRow">
			<p><?=Loc::getMessage('SBB_ORDER_TOTAL_SUM')?></p>
			<span>{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}</span>
		</div>
		{{#DISCOUNT_PRICE_FORMATED}}
			<div class="calcRow">
				<p><?=Loc::getMessage('SBB_ORDER_DISCOUNT')?></p>
				<span>- {{{DISCOUNT_PRICE_FORMATED}}}</span>
			</div>
		{{/DISCOUNT_PRICE_FORMATED}}
		<div class="calcRow total">
			<p><?=Loc::getMessage('SBB_ORDER_TOTAL_TO_PAY')?></p>
			<span>{{{PRICE_FORMATED}}}</span>
		</div>
	</div>
	<div class="btnBox">
		<button class="btn{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
			data-entity="basket-checkout-button">
			<?=Loc::getMessage('SBB_ORDER')?>
		</button>
	</div>
</script>