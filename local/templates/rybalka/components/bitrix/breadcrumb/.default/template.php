<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';
$strReturn .= '<nav class="breadCrumbs" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<svg><use href="#arrowIcon" /></svg>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= $arrow . '
				<a href="'.$arResult[$index]["LINK"].'"
					title="'.$title.'"
					itemprop="url"
					id="bx_breadcrumb_'.$index.'"
					itemprop="itemListElement"
					itemscope
					itemtype="http://schema.org/ListItem">' . $title . '
					<meta itemprop="name" content="' . $title . '" />
					<meta itemprop="position" content="'.($index + 1).'" />
				</a>';
	}
	else
	{
		$strReturn .= $arrow . '
				<span
					itemprop="url"
					id="bx_breadcrumb_'.$index.'"
					itemprop="itemListElement"
					itemscope
					itemtype="http://schema.org/ListItem">' . $title . '
					<meta itemprop="name" content="' . $title . '" />
					<meta itemprop="position" content="'.($index + 1).'" />
				</span>';
	}
}

$strReturn .= '</nav>';

return $strReturn;
