<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>

<div class="bannersCarousel">
	<!-- items-container -->
	<?
		// exo($arResult['ITEMS']);
		if (!empty($arResult['ITEMS'])) {
			foreach ($arResult['ITEMS'] as $item) {
				$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
				$areaId = $this->GetEditAreaId($uniqueId);
				$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
				$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
				?><div id="<?=$areaId?>"><a href="<?=$item['PROPERTIES']['LINK']['VALUE']?>"><img src="<?=$item['DETAIL_PICTURE']['SRC']?>" alt="<?=$item['DETAIL_PICTURE']['ALT']?>" /></a></div><?
			}
			unset($generalParams, $rowItems);
		}
	?>
	<!-- items-container -->
</div>
<!-- component-end -->