<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<aside class="filterWrap">
	<div class="filterBox">		
		<form name="<?=$arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
				<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>

			<!-- selected filters -->
			<?
				$arFiltersSelected = [];
				foreach($arResult["ITEMS"] as $key=>$arItem) {

					if(empty($arItem["VALUES"]))
						continue;

					if(($arItem["PRICE"] == 1 || $arItem['DISPLAY_TYPE'] == 'A') && (isset($arItem['VALUES']['MIN']['HTML_VALUE']) || isset($arItem['VALUES']['MAX']['HTML_VALUE']))) {
						$arFiltersSelected[] = [
							'CONTROL_ID' => 'test',
							'VALUE' => $arItem['NAME'] . (isset($arItem['VALUES']['MIN']['HTML_VALUE']) ? ' от ' . $arItem['VALUES']['MIN']['HTML_VALUE'] : '') . (isset($arItem['VALUES']['MAX']['HTML_VALUE']) ? ' до ' . $arItem['VALUES']['MAX']['HTML_VALUE'] : ''),
							'ONCLICK' => '
								var el = document.getElementById(\'' . $arItem['VALUES']['MIN']['CONTROL_ID'] . '\');
								el.value = ' . floor($arItem['VALUES']['MIN']['VALUE']) . ';
								el.dispatchEvent(new Event(\'change\'));

								el = document.getElementById(\'' . $arItem['VALUES']['MAX']['CONTROL_ID'] . '\');
								el.value = ' . ceil($arItem['VALUES']['MAX']['VALUE']) . ';
								el.dispatchEvent(new Event(\'change\'));
							',
						];
					}

					$arCur = current($arItem['VALUES']);
					foreach($arItem["VALUES"] as $val => $ar) {
						if($ar['CHECKED']) {
							$ar['ALL_CONTROL_ID'] = $arCur['CONTROL_ID'];
							$arFiltersSelected[$arItem['DISPLAY_TYPE']] = $ar;
						}
					}
				}
				// exo($arFiltersSelected);
			?>
			
			<? if(count($arFiltersSelected)): ?>
				<div class="filterSection">
					<h3 class="sectionTitle"><?=GetMessage("CT_BCSF_FILTER_SELECTED")?></h3>
					<? foreach($arFiltersSelected as $type=>$ar): ?>
						<div class="selectedFilter">
							<div><?=$ar['VALUE']?></div>
							<? if(isset($ar['ONCLICK'])): ?>
								<svg onclick="<?=$ar['ONCLICK']?>">
									<use href="#closeIcon" />
								</svg>
							<? elseif($type == 'K'): // radiobutton ?>
								<svg onclick="document.getElementById('all_<?=$ar['ALL_CONTROL_ID']?>').click()">
									<use href="#closeIcon" />
								</svg>
							<? else: ?>
								<svg onclick="document.getElementById('<?=$ar['CONTROL_ID']?>').click()">
									<use href="#closeIcon" />
								</svg>
							<? endif; ?>
						</div>
					<? endforeach; ?>
				</div>
			<? endif; ?>

			<!-- main block -->
			<?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
			{
				$key = $arItem["ENCODED_ID"];
				if(isset($arItem["PRICE"])):
					if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
						continue;
					
					?><div class="filterSection bx-filter-parameters-box bx-active">
						<!-- <span class="bx-filter-container-modef"></span> -->
						<h4 class="sectionTitle _toggleButton open"><?=$arItem['NAME']?><svg><use href="#arrowIcon" /></svg></h4>
						<div class="sectionContent _toggleContent open">

							<?
								// exo($arItem['VALUES']['MIN']['VALUE']);
								$arItem['VALUES']['MIN']['VALUE'] = floor($arItem['VALUES']['MIN']['VALUE']);
								$arItem['VALUES']['MAX']['VALUE'] = ceil($arItem['VALUES']['MAX']['VALUE']);
								$step = floor(($arItem['VALUES']['MAX']['VALUE'] - $arItem['VALUES']['MIN']['VALUE']) / 20) ?? 1;
							?>
							<div class="rangeSliderBox">
								<div class="rangeSlider"
									data-min="<?=$arItem['VALUES']['MIN']['VALUE']?>" 
									data-max="<?=$arItem['VALUES']['MAX']['VALUE']?>" 
									data-step="<?=$step?>" 
									data-start="<?=$arItem['VALUES']['MIN']['HTML_VALUE'] ?? $arItem['VALUES']['MIN']['VALUE']?>" 
									data-end="<?=$arItem['VALUES']['MAX']['HTML_VALUE'] ?? $arItem['VALUES']['MAX']['VALUE']?>"></div>
								<div class="rangeFields">
									<input class="from"
										type="tel"
										placeholder="От"
										name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										min="<?=$arItem['VALUES']['MIN']['VALUE']?>"
										max="<?=$arItem['VALUES']['MAX']['VALUE']?>"
										value="<?=$arItem["VALUES"]["MIN"]["HTML_VALUE"] ?? $arItem['VALUES']['MIN']['VALUE']?>"
										onkeyup="smartFilter.keyup(this)"
										onchange="smartFilter.keyup(this)" />
									<input class="to"
										type="tel" 
										placeholder="До"
										name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										min="<?=$arItem['VALUES']['MIN']['VALUE']?>"
										max="<?=$arItem['VALUES']['MAX']['VALUE']?>"
										value="<?=$arItem['VALUES']['MAX']['HTML_VALUE'] ?? ceil($arItem["VALUES"]["MAX"]["VALUE"])?>"
										onkeyup="smartFilter.keyup(this)"
										onchange="smartFilter.keyup(this)" />
								</div>
							</div>

						</div>
					</div>
				<?endif;
			}

			//not prices
			foreach($arResult["ITEMS"] as $key=>$arItem)
			{
				if(
					empty($arItem["VALUES"])
					|| isset($arItem["PRICE"])
					|| (isset($arParams['FIELDS_TO_SHOW']) && !in_array($arItem['CODE'], $arParams['FIELDS_TO_SHOW']))
				)
					continue;

				if (
					$arItem["DISPLAY_TYPE"] == "A"
					&& (
						$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
					)
				)
					continue;
				?>
				<div class="filterSection">
					<!-- <span class="bx-filter-container-modef"></span> -->
					<h4 class="sectionTitle _toggleButton <?=($arItem["DISPLAY_EXPANDED"]== "Y" ? 'open' : '')?>"><?=$arItem["NAME"]?><svg><use href="#arrowIcon" /></svg></h4>

					<div class="sectionContent _toggleContent <?=($arItem["DISPLAY_EXPANDED"]== "Y" ? 'open' : '')?>" data-role="bx_filter_block"><?
						$arCur = current($arItem["VALUES"]);

						switch ($arItem["DISPLAY_TYPE"]) {
							case "A"://NUMBERS_WITH_SLIDER
								// exo($arItem);
								$arItem['VALUES']['MIN']['VALUE'] = floor($arItem['VALUES']['MIN']['VALUE']);
								$arItem['VALUES']['MAX']['VALUE'] = ceil($arItem['VALUES']['MAX']['VALUE']);
								$step = floor(($arItem['VALUES']['MAX']['VALUE'] - $arItem['VALUES']['MIN']['VALUE']) / 20) ?? 1;
								?><div class="rangeSliderBox">
									<div class="rangeSlider"
										data-min="<?=$arItem['VALUES']['MIN']['VALUE']?>" 
										data-max="<?=$arItem['VALUES']['MAX']['VALUE']?>" 
										data-step="<?=$step?>" 
										data-start="<?=$arItem['VALUES']['MIN']['HTML_VALUE'] ?? $arItem['VALUES']['MIN']['VALUE']?>" 
										data-end="<?=$arItem['VALUES']['MAX']['HTML_VALUE'] ?? $arItem['VALUES']['MAX']['VALUE']?>">
									</div>
									<div class="rangeFields">
										<input class="from"
											type="tel"
											placeholder="От"
											name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
											id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
											min="<?=$arItem['VALUES']['MIN']['VALUE']?>"
											max="<?=$arItem['VALUES']['MAX']['VALUE']?>"
											value=""
											onkeyup="smartFilter.keyup(this)"
											onchange="smartFilter.keyup(this)" />
										<input class="to"
											type="tel" 
											placeholder="До"
											name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
											id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
											min="<?=$arItem['VALUES']['MIN']['VALUE']?>"
											max="<?=$arItem['VALUES']['MAX']['VALUE']?>"
											value=""
											onkeyup="smartFilter.keyup(this)"
											onchange="smartFilter.keyup(this)" />
									</div>
								</div><?
								break;
							case "B"://NUMBERS
								?>
								<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
									<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
									<div class="bx-filter-input-container">
										<input
											class="min-price"
											type="text"
											name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
											id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
											value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
											size="5"
											onkeyup="smartFilter.keyup(this)"
											/>
									</div>
								</div>
								<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
									<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
									<div class="bx-filter-input-container">
										<input
											class="max-price"
											type="text"
											name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
											id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
											value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
											size="5"
											onkeyup="smartFilter.keyup(this)"
											/>
									</div>
								</div>
								<?
								break;
							case "G"://CHECKBOXES_WITH_PICTURES
								?>
								<div class="col-xs-12">
									<div class="bx-filter-param-btn-inline">
									<?foreach ($arItem["VALUES"] as $val => $ar):?>
										<input
											style="display: none"
											type="checkbox"
											name="<?=$ar["CONTROL_NAME"]?>"
											id="<?=$ar["CONTROL_ID"]?>"
											value="<?=$ar["HTML_VALUE"]?>"
											<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
										/>
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " bx-active";
										if ($ar["DISABLED"])
											$class.= " disabled";
										?>
										<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
											<span class="bx-filter-param-btn bx-color-sl">
												<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
												<?endif?>
											</span>
										</label>
									<?endforeach?>
									</div>
								</div>
								<?
								break;
							case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
								?>
								<div class="col-xs-12">
									<div class="bx-filter-param-btn-block">
									<?foreach ($arItem["VALUES"] as $val => $ar):?>
										<input
											style="display: none"
											type="checkbox"
											name="<?=$ar["CONTROL_NAME"]?>"
											id="<?=$ar["CONTROL_ID"]?>"
											value="<?=$ar["HTML_VALUE"]?>"
											<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
										/>
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " bx-active";
										if ($ar["DISABLED"])
											$class.= " disabled";
										?>
										<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
											<span class="bx-filter-param-btn bx-color-sl">
												<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
													<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
												<?endif?>
											</span>
											<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
											if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
											endif;?></span>
										</label>
									<?endforeach?>
									</div>
								</div>
								<?
								break;
							case "P"://DROPDOWN
								$checkedItemExist = false;
								?>
								<div class="col-xs-12">
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text" data-role="currentOption">
												<?
												foreach ($arItem["VALUES"] as $val => $ar)
												{
													if ($ar["CHECKED"])
													{
														echo $ar["VALUE"];
														$checkedItemExist = true;
													}
												}
												if (!$checkedItemExist)
												{
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<li>
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<?
								break;
							case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
								?>
								<div class="col-xs-12">
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text fix" data-role="currentOption">
												<?
												$checkedItemExist = false;
												foreach ($arItem["VALUES"] as $val => $ar):
													if ($ar["CHECKED"])
													{
													?>
														<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
														<?endif?>
														<span class="bx-filter-param-text">
															<?=$ar["VALUE"]?>
														</span>
													<?
														$checkedItemExist = true;
													}
												endforeach;
												if (!$checkedItemExist)
												{
													?><span class="bx-filter-btn-color-icon all"></span> <?
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<?=$ar["HTML_VALUE_ALT"]?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
												<ul>
													<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<span class="bx-filter-btn-color-icon all"></span>
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
															<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
															<?endif?>
															<span class="bx-filter-param-text">
																<?=$ar["VALUE"]?>
															</span>
														</label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<?
								break;
							case "K"://RADIO_BUTTONS
								?>
								<div class="fieldRow">
									<div class="boxField">
										<label for="<?="all_".$arCur["CONTROL_ID"]?>">
											<span class="radioBox">
												<input
													type="radio"
													value=""
													name="<?=$arCur["CONTROL_NAME_ALT"]?>"
													id="<?='all_'.$arCur['CONTROL_ID']?>"
													onclick="smartFilter.click(this)" />
												<span class="mark"></span>
											</span>
											<span class="label">
												<?=GetMessage("CT_BCSF_FILTER_ALL")?>
											</span>
										</label>
									</div>
								</div><?
								foreach($arItem["VALUES"] as $val => $ar):?>
									<div class="fieldRow">
                                        <div class="boxField">
											<label
												data-role="label_<?=$ar["CONTROL_ID"]?>"
												for="<?=$ar["CONTROL_ID"]?>"
												class="<?=($ar["DISABLED"] ? 'disabled' : '')?>">
												<span class="radioBox">
													<input
														type="radio"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
														<?=($ar["ELEMENT_COUNT"]==0 ? 'disabled="disabled"' : '')?>
														onclick="smartFilter.click(this)" />
													<span class="mark"></span>
												</span>
                                                <span class="label">
													<?=$ar["VALUE"]?>
													<? if($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><?=$ar["ELEMENT_COUNT"]?></span>)<?
													endif;?>
												</span>
                                            </label>
                                        </div>
                                    </div><?
								endforeach;
								break;
							case "U"://CALENDAR
								?>
								<div class="col-xs-12">
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>
									</div></div>
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>
									</div></div>
								</div>
								<?
								break;
							default://CHECKBOXES
								foreach($arItem["VALUES"] as $val => $ar):?>
									<div class="fieldRow">
										<div class="boxField">
											<label
												data-role="label_<?=$ar["CONTROL_ID"]?>"
												class="<?=($ar["DISABLED"] ? 'disabled': '')?>"
												for="<?=$ar["CONTROL_ID"]?>">
												<span class="checkBox">
													<input
														type="checkbox"
														value="<?=$ar["HTML_VALUE"]?>"
														name="<?=$ar["CONTROL_NAME"]?>"
														id="<?=$ar["CONTROL_ID"]?>"
														<?=($ar["CHECKED"] ? 'checked="checked"' : '')?>
														<?=($ar["ELEMENT_COUNT"]==0 ? 'disabled="disabled"' : '')?>
														onclick="smartFilter.click(this)" />
													<svg class="mark"><use href="#checkIcon" /></svg>
												</span>
												<span class="label">
													<?=$ar["VALUE"]?>
													<? if($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
														<span>(<?=$ar["ELEMENT_COUNT"]?>)</span>
													<? endif; ?>
												</span>
											</label>
										</div>
									</div><?
								endforeach;
						}
					?></div>
				</div><?
			} ?>
		</form>
	</div>
</aside>
<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>