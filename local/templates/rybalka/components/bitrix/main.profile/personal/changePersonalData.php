<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('NAME')?></label>
		<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" required />
		<div class="errorMsg"></div>
	</div>
</div>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('LAST_NAME')?></label>
		<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" required />
		<div class="errorMsg"></div>
	</div>
</div>