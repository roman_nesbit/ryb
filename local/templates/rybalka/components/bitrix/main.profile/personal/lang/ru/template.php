<?
$MESS ['TITLE_changePersonalData'] = "Персональные данные";
$MESS ['TITLE_changePassword'] = "Пароль";
$MESS ['TITLE_changeDelivery'] = "Доставка";

$MESS ['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS ['NAME'] = "Имя";
$MESS ['LAST_NAME'] = "Фамилия";
$MESS ['SECOND_NAME'] = "Отчество";
$MESS ['NEW_PASSWORD_CONFIRM'] = "Подтверждение пароля";
$MESS ['NEW_PASSWORD_REQ'] = "Новый пароль";
$MESS ['MAIN_SAVE'] = "Сохранить";
$MESS ['MAIN_SAVED'] = "Сохранено";
$MESS ['MP_PASSWORD_MAIN_PSWD'] = "Текущий пароль";

$MESS['PHPSESSID_400'] = "Ошибочная операция";
$MESS['PHPSESSID_401'] = "Неавторизованный пользователь";
$MESS['PHPSESSID_500'] = "Ошибка записи изменений. Повторите попытку позже.";

$MESS['CURRENT_PASSWORD_404'] = "Неверный пароль";

$MESS['NEW_PASSWORD_REQ_403'] = "Пароль и подтверждение пароля не совпадают";

$MESS['CITY'] = "Населенный пункт";
$MESS['NP_OFFICE'] = "Отделение &laquo;Нова пошта&raquo;";
?>