<?
$MESS['TITLE_changePersonalData'] = "Персональні дані";
$MESS['TITLE_changePassword'] = "Пароль";
$MESS['TITLE_changeDelivery'] = "Доставка";

$MESS['PROFILE_DATA_SAVED'] = "Зміни збережені";
$MESS['NAME'] = "Ім'я";
$MESS['LAST_NAME'] = "Прізвище";
$MESS['SECOND_NAME'] = "По батькові";
$MESS['NEW_PASSWORD_CONFIRM'] = "Підтвердження пароля";
$MESS['NEW_PASSWORD_REQ'] = "Новий пароль";
$MESS['MAIN_SAVE'] = "Зберегти";
$MESS['MAIN_SAVED'] = "Збережено";
$MESS['MP_PASSWORD_MAIN_PSWD'] = "Поточний пароль";

$MESS['PHPSESSID_400'] = "Помилкова операція";
$MESS['PHPSESSID_401'] = "Неавторизований користувач";
$MESS['PHPSESSID_500'] = "Не можу записати змін. Будь ласка, спробуйте пізніше.";

$MESS['CURRENT_PASSWORD_404'] = "Неправильний пароль";

$MESS['NEW_PASSWORD_REQ_403'] = "Пароль і підтвердження пароля не збігаються";

$MESS['CITY'] = "Населений пункт";
$MESS['NP_OFFICE'] = "Відділення &laquo;Нова пошта&raquo;";
?>