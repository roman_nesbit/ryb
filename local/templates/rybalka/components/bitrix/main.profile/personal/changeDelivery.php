<?

use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$settlement = !empty($arResult['arUser']['UF_CITY_ID'])
    ? (new SettlementsService())->get($arResult['arUser']['UF_CITY_ID'])
    : null;
$postOffice = !empty($arResult["arUser"]["UF_NP_OFFICE_ID"])
    ? (new PostOfficesService())->get($arResult["arUser"]["UF_NP_OFFICE_ID"])
    : null;

$settlementName = (string)($settlement ?? '');
$postOfficeName = (string)($postOffice ?? '');
?>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('CITY')?></label>
		<input type="hidden"
			name="UF_CITY_ID"
			value="<?=$arResult['arUser']['UF_CITY_ID']?>"
			readonly />
		<input type="text"
			id="settlement"
			value="<?=$settlementName?>"
			list="settlementsList"
			autocomplete="off" />
		<datalist id="settlementsList"></datalist>
	</div>
</div>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('NP_OFFICE')?></label>
		<input type="hidden"
			name="UF_NP_OFFICE_ID"
			value="<?=$arResult["arUser"]["UF_NP_OFFICE_ID"]?>"
			readonly />
		<input type="text"
			id="office"
			value="<?=htmlspecialchars($postOfficeName)?>"
			list="officesList"
			autocomplete="off" />
		<datalist id="officesList"></datalist>
	</div>
</div>

<script>
	var settlementTimer;

	$(document).ready(function() {
		$("#settlement").on("input", function(e) {
			if(settlementTimer) {
				clearTimeout(settlementTimer);
			}
			settlementTimer = setTimeout(function() {
				var field = $("#settlement");
				var fieldVal = $.trim(field.val());
				var list = field.next("datalist");
				var listOptions = list.children("option");
				var dataField = field.prev("input");

				list.html('');

				// console.log('settlement', fieldVal);

				if(fieldVal == '') {
					dataField.val('');
				}
				
				if(listOptions.length) {
					var optionsFound = listOptions.filter(function() {
						return this.value === fieldVal;        
					});
					if(optionsFound.length) {
						dataField.val(optionsFound.data("id")).trigger("ryb:changed");
					}
				}

				if(fieldVal.length >= 2) {
					$.ajax({
						type: "GET",
						url: "/api/dictionary/address/settlement/",
						data: {
							'query': fieldVal
						},
						dataType: "json",
						success: function(response) {
							if(response.meta.status == 200) {
								response.data.map(function(city) {
									list.append($("<option/>", {
										'data-id': city.id,
										'value': city.name
									}));
								});
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
						},
						complete: function() {
						}
					});
				}
			}, 300);
		});

		$("[name='UF_CITY_ID']").on("ryb:changed", function() {
			var cityId = $.trim($(this).val());

			var field = $("#office");
			var fieldVal = field.val();
			var list = field.next("datalist");
			var listOptions = list.children("option");
			var dataField = field.prev("input");

			field.val('');
			dataField.val('');
			list.html('');

			if(cityId != '') {
				$.ajax({
					type: "GET",
					url: "/api/dictionary/post/office/",
					data: {
						'settlementId': cityId
					},
					dataType: "json",
					success: function(response) {
						if(response.meta.status == 200) {
							response.data.map(function(office) {
								list.append($("<option/>", {
									'data-id': office.id,
									'html': office.name
								}));
							});
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
					},
					complete: function() {
					}
				});
			}

		});

		$("#office").on("input", function(e) {
			var field = $(e.target);
			var fieldVal = field.val();
			var list = field.next("datalist");
			var listOptions = list.children("option");
			var dataField = field.prev("input");

			// console.log('office', fieldVal);
			if(fieldVal == '') {
				dataField.val('');
				$("[name='UF_CITY_ID']").trigger("ryb:changed");
			}
			
			if(listOptions.length) {
				var optionsFound = listOptions.filter(function() {
					return this.value === fieldVal;        
				});
				if(optionsFound.length) {
					dataField.val(optionsFound.data("id"));
				}
			}

		});

		if($("[name='UF_NP_OFFICE_ID']").val() == '') {
			$("[name='UF_CITY_ID']").trigger("ryb:changed");
		}
	});
</script>