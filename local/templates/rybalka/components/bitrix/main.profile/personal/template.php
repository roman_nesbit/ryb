<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2 class="title"><?=GetMessage('TITLE_' . $arParams['FORM_ID'])?></h2>
<div class="formContainer">
	<?ShowError($arResult["strProfileError"]);?>
	<form method="post"
		id="<?=$arParams['FORM_ID']?>"
		name="<?=$arParams['FORM_ID']?>"
		action="<?=SITE_TEMPLATE_PATH?>/ajax/set_userdata.php"
		enctype="multipart/form-data">
		<div class="fieldRow">
			<div class="inputField">
				<?=bitrix_sessid_post('PHPSESSID')?>
				<div class="errorMsg">
					<div data-error="400" style="display: none"><?=GetMessage('PHPSESSID_400')?></div>
					<div data-error="401" style="display: none"><?=GetMessage('PHPSESSID_401')?></div>
					<div data-error="500" style="display: none"><?=GetMessage('PHPSESSID_500')?></div>
				</div>
			</div>
		</div>

		<? include(\Bitrix\Main\Application::getDocumentRoot() . $templateFolder . '/' . $arParams['FORM_ID'] . '.php'); ?>

		<div class="buttonsRow">
			<button class="btn fullWidth withIcon btnWithLoading <?=($arResult['DATA_SAVED'] == 'Y' ? 'isComplete' : '')?>"
				type="submit"
				name="save"
				value="<?=GetMessage("MAIN_SAVE")?>">
				<svg class="loadingIcon">
					<use href="#loadingIcon" />
				</svg>
				<svg class="correctIcon">
					<use href="#correctIcon" />
				</svg>
				<span class="primary"><?=GetMessage("MAIN_SAVE")?></span>
				<span class="secondary"><?=GetMessage("MAIN_SAVED")?></span>
			</button>
		</div>
	</form>
</div>

<script type="text/javascript">
	var form_<?=$arParams['FORM_ID']?> = $("#<?=$arParams['FORM_ID']?>");

	form_<?=$arParams['FORM_ID']?>.find("input").on("change", function() {
		var submitButton = form_<?=$arParams['FORM_ID']?>.find("button[type='submit']");
		submitButton.removeClass("isComplete");
	});

	form_<?=$arParams['FORM_ID']?>.on("submit", function(e) {
		e.preventDefault();

		var submitButton = form_<?=$arParams['FORM_ID']?>.find("button[type='submit']");
		submitButton.removeClass("isComplete").attr("disabled", "disabled").addClass("disabled").addClass("isLoading");
		form_<?=$arParams['FORM_ID']?>.find(".errorMsg [data-error]").hide();
		form_<?=$arParams['FORM_ID']?>.find(".withError").removeClass("withError");

		$.ajax({
			type: form_<?=$arParams['FORM_ID']?>.attr("method"),
			url: form_<?=$arParams['FORM_ID']?>.attr("action"),
			data: form_<?=$arParams['FORM_ID']?>.serialize(),
			success: function(data) {
				switch(data.meta.status) {
					case 200:
						<? if($arParams['FORM_RESET'] == 'Y'): ?>
							form_<?=$arParams['FORM_ID']?>.trigger("reset");
						<? endif; ?>
						submitButton.addClass("isComplete");
						break;
					default:
						form_<?=$arParams['FORM_ID']?>.find("[name='" + data.meta.field + "']").next(".errorMsg").find("[data-error='" + data.meta.status + "']").show().parents(".inputField").addClass("withError");
						break;
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				submitButton.removeAttr("disabled").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>