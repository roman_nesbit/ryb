<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('MP_PASSWORD_MAIN_PSWD')?></label>
		<input type="password"
			name="CURRENT_PASSWORD"
			maxlength="50"
			value=""
			required
			autocomplete="new-password" />
		<div class="errorMsg">
			<div data-error="404" style="display:none"><?=GetMessage('CURRENT_PASSWORD_404')?></div>
		</div>
	</div>
</div>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('NEW_PASSWORD_REQ')?></label>
		<span class="helperLabel"><?=$arResult['GROUP_POLICY']['PASSWORD_REQUIREMENTS']?></span>
		<input type="password"
			name="NEW_PASSWORD_REQ"
			maxlength="50"
			minlength="<?=$arResult['GROUP_POLICY']['PASSWORD_LENGTH']?>"
			value=""
			required 
			autocomplete="new-password" />
		<div class="errorMsg">
			<div data-error="403" style="display:none"><?=GetMessage('NEW_PASSWORD_REQ_403')?></div>
		</div>
	</div>
</div>
<div class="fieldRow">
	<div class="inputField">
		<label><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
		<input type="password"
			name="NEW_PASSWORD_CONFIRM"
			maxlength="50"
			minlength="<?=$arResult['GROUP_POLICY']['PASSWORD_LENGTH']?>"
			value=""
			required
			autocomplete="new-password" />
	</div>
</div>