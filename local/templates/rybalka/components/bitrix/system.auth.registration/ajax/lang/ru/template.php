<?
use Rybalka\Marketplace\Http\StatusCode;

$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_REGISTER_ACTION"] = "Зарегистрироваться";
$MESS["AUTH_NAME"] = "Имя";
$MESS["AUTH_LAST_NAME"] = "Фамилия";
$MESS["AUTH_LOGIN_MIN"] = "Логин (мин. 3 символа):";
$MESS["AUTH_CONFIRM"] = "Подтверждение пароля";
$MESS["CAPTCHA_REGF_TITLE"] = "Защита от автоматической регистрации";
$MESS["CAPTCHA_REGF_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REQ"] = "Обязательные поля";
$MESS["AUTH_AUTH"] = "Авторизация";
$MESS["AUTH_AUTH_ACTION"] = "Авторизоваться";
$MESS["AUTH_PASSWORD_REQ"] = "Пароль";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "На указанный в форме e-mail придет запрос на подтверждение регистрации.";
$MESS["AUTH_EMAIL_SENT"] = "На указанный в форме e-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["AUTH_EMAIL"] = "E-mail (опционально)";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["main_register_sms_code"] = "Код из СМС";
$MESS["main_register_sms_send"] = "Дальше";
$MESS["main_register_phone_number"] = "Телефон";
$MESS["main_register_phone_number_change"] = "Изменить номер телефона";
$MESS["main_register_phone_number_sample"] = "Например: 380963334455";
$MESS["main_register_phone_number_error"] = "Введите номер телефона";
$MESS["main_register_sms_note"] = "На ваш телефон отправлено СМС с кодом подтверждения";
$MESS["main_register_no_sms_note"] = "Не получили сообщение?";
$MESS["main_register_no_sms_action"] = "Изменить номер или отправить еще раз";
$MESS["main_register_phone_already_note"] = "Ваш номер телефона зарегистрирован в системе, пожалуйста, авторизуйтесь с помощью этого номера и пароля.";
$MESS["main_register_thankyou_note"] = "Вы успешно зарегистрировались!";
$MESS["phoneNumber_" . StatusCode::BAD_REQUEST] = "Введите правильный номер телефона";
$MESS["phoneNumber_" . StatusCode::UNAUTHORIZED] = "Каптча не верифицирована. Вы бот?";
$MESS["phoneNumber_" . StatusCode::TOO_MANY_REQUESTS] = "Вам следует подождать перед новой отправкой кода <waiting>#WAITING#</waiting> сек.";
$MESS["code_" . StatusCode::BAD_REQUEST] = "Что-то пошло не так, попробуйте еще раз";
$MESS["code_" . StatusCode::FORBIDDEN] = "Ошибка проверки кода, отправьте код повторно";
$MESS["code_" . StatusCode::NOT_FOUND] = "Неверный код из СМС";

$MESS["verificationToken_" . StatusCode::INTERNAL_SERVER_ERROR] = "Не удалось зарегистрировать пользователя. #LAST_ERROR#.";
$MESS["verificationToken_" . StatusCode::UNAUTHORIZED] = "Ваш номер телефона не подтвержден";
$MESS["verificationToken_" . StatusCode::BAD_REQUEST] = "Что-то пошло не так";
$MESS["USER_NAME_" . StatusCode::BAD_REQUEST] = "Введите Ваше имя";
$MESS["USER_LAST_NAME_" . StatusCode::BAD_REQUEST] = "Введите Вашу фамилию";
$MESS["USER_PASSWORD_" . StatusCode::BAD_REQUEST] = "Введите пароль";
$MESS["USER_CONFIRM_PASSWORD_" . StatusCode::BAD_REQUEST] = "Введите подтверждение пароля";
$MESS["USER_PASSWORD_" . StatusCode::FORBIDDEN] = "Пароль и подверждение пароля не совпадают";
$MESS["USER_EMAIL_" . StatusCode::BAD_REQUEST] = "Введите корректный email";
$MESS["USER_EMAIL_" . StatusCode::UNAUTHORIZED] = "Пользователь с таким email зарегистрирован";

?>