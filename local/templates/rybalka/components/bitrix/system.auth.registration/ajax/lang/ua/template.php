<?
use Rybalka\Marketplace\Http\StatusCode;

$MESS["AUTH_AUTH"] = "Авторизація";
$MESS["AUTH_AUTH_ACTION"] = "Авторизуватись";
$MESS["AUTH_CONFIRM"] = "Підтвердження пароля:";
$MESS["AUTH_EMAIL"] = "E-mail:";
$MESS["AUTH_EMAIL_SENT"] = "На зазначений у формі e-mail було надіслано лист з інформацією про підтвердження реєстрації.";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "На зазначений у формі e-mail прийде запит на підтвердження реєстрації.";
$MESS["AUTH_LAST_NAME"] = "Прізвище:";
$MESS["AUTH_LOGIN_MIN"] = "Логін (мін. 3 символи):";
$MESS["AUTH_NAME"] = "Ім'я:";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль буде відправлений у відкритому вигляді. Увімкніть JavaScript в браузері, щоб зашифрувати пароль перед відправкою.";
$MESS["AUTH_PASSWORD_REQ"] = "Пароль:";
$MESS["AUTH_REGISTER"] = "Реєстрація";
$MESS["AUTH_REGISTER_ACTION"] = "Зареєструватись";
$MESS["AUTH_REQ"] = "Обов'язкові поля";
$MESS["AUTH_SECURE_NOTE"] = "Перед відправкою форми пароль буде зашифрований в браузері. Це дозволить уникнути передачі пароля у відкритому вигляді.";
$MESS["CAPTCHA_REGF_PROMT"] = "Введіть слово на картинці";
$MESS["CAPTCHA_REGF_TITLE"] = "Захист від автоматичної реєстрації";
$MESS["main_register_phone_number"] = "Телефон";
$MESS["main_register_phone_number_change"] = "Змінити номер телефона";
$MESS["main_register_sms_code"] = "Код із СМС";
$MESS["main_register_sms_send"] = "Далі";
$MESS["main_register_phone_number_sample"] = "Наприклад: +380963334455";
$MESS["main_register_phone_number_error"] = "Введіть номер телефона";
$MESS["main_register_sms_note"] = "На ваш телефон відправлено СМС з кодом підтвердження";
$MESS["main_register_no_sms_note"] = "Не отримали повідомлення?";
$MESS["main_register_no_sms_action"] = "Змінити номер або відправити ще раз";
$MESS["main_register_phone_already_note"] = "Ваш номер телефону зареєстрований в системі, будь ласка, авторизуйтесь за допомогою цього номера і пароля.";
$MESS["main_register_thankyou_note"] = "Ви успішно зереєструвались!";
$MESS["phoneNumber_" . StatusCode::BAD_REQUEST] = "Введіть правильний номер телефону";
$MESS["phoneNumber_" . StatusCode::TOO_MANY_REQUESTS] = "Вам слід почекати перед новою відправкою коду #WAITING# сек.";
$MESS["phoneNumber_" . StatusCode::UNAUTHORIZED] = "Каптча не веріфікована. Ви бот?";
$MESS["code_" . StatusCode::BAD_REQUEST] = "Щось пішло не так, спробуйте ще раз";
$MESS["code_" . StatusCode::BAD_REQUEST] = "Щось пішло не так, спробуйте ще раз";
$MESS["code_" . StatusCode::FORBIDDEN] = "Помилка перевірки коду, надішліть код повторно";
$MESS["code_" . StatusCode::NOT_FOUND] = "Невірний код з СМС";

$MESS["verificationToken_" . StatusCode::INTERNAL_SERVER_ERROR] = "Не вдалося зареєструвати користувача. #LAST_ERROR#.";
$MESS["verificationToken_" . StatusCode::UNAUTHORIZED] = "Ваш номер телефону не підтверджений";
$MESS["verificationToken_" . StatusCode::BAD_REQUEST] = "Щось пішло не так";
$MESS["USER_NAME_" . StatusCode::BAD_REQUEST] = "Введіть ваше ім'я";
$MESS["USER_LAST_NAME_" . StatusCode::BAD_REQUEST] = "Введіть Ваше прізвище";
$MESS["USER_PASSWORD_" . StatusCode::BAD_REQUEST] = "Введіть пароль";
$MESS["USER_CONFIRM_PASSWORD_" . StatusCode::BAD_REQUEST] = "Введіть підтвердження пароля";
$MESS["USER_PASSWORD_" . StatusCode::FORBIDDEN] = "Пароль і підтвердження пароля не збігаються";
$MESS["USER_EMAIL_" . StatusCode::BAD_REQUEST] = "Введіть коректний email";
$MESS["USER_EMAIL_" . StatusCode::UNAUTHORIZED] = "Користувач з таким email зареєстрований";

?>