<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}?>
<form method="post" action="<?=SITE_TEMPLATE_PATH?>/ajax/get_regform.php" id="regform_step_4">
	<input type="hidden" name="STEP" value="4" />
	<div class="fieldRow">
		<input type="hidden" name="verificationToken" value="<?= $arResult['verificationToken'] ?>" />
		<? if ($arResult['ERROR']['DATA']['FIELD'] == 'verificationToken') : ?>
			<div class="errorMsg">
				<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'], [
						'#LAST_ERROR#' => $arResult['ERROR']['DATA']['LAST_ERROR']
					])?>
			</div>
		<? endif; ?>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['USER_NAME'] ? 'withError' : '')?>">
			<label><?=GetMessage('AUTH_NAME')?></label>
			<input type="text" name="USER_NAME" value="<?= $arResult['USER_NAME'] ?>" required />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_NAME') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_LAST_NAME' ? 'withError' : '')?>">
			<label><?= GetMessage('AUTH_LAST_NAME') ?></label>
			<input type="text" name="USER_LAST_NAME" value="<?= $arResult['USER_LAST_NAME'] ?>" required />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_LAST_NAME') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD' ? 'withError' : '')?>">
			<label><?= GetMessage('AUTH_PASSWORD_REQ') ?></label>
			<span class="helperLabel"><?= $arResult['GROUP_POLICY']['PASSWORD_REQUIREMENTS'] ?></span>
			<input type="password" name="USER_PASSWORD" value="" required autocomplete="new-password" />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD' ? 'withError' : '')?>">
			<label><?=GetMessage('AUTH_CONFIRM')?></label>
			<input type="password" name="USER_CONFIRM_PASSWORD" value="" required autocomplete="new-password" />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_CONFIRM_PASSWORD') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'USER_EMAIL' ? 'withError' : '')?>">
			<label><?=GetMessage('AUTH_EMAIL') ?></label>
			<span class="helperLabel"><?= GetMessage('AUTH_EMAIL_TEXT') ?></span>
			<input type="email" name="USER_EMAIL" value="<?= $arResult['USER_EMAIL'] ?>" />
			<? if ($arResult['ERROR']['DATA']['FIELD'] == 'USER_EMAIL') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'])?>
				</div>
			<? endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<button class="btn fullWidth withIcon btnWithLoading" type="submit">
			<svg class="loadingIcon">
				<use href="#loadingIcon" />
			</svg>
			<span><?=GetMessage('AUTH_REGISTER_ACTION')?></span>
		</button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#regform_step_4");

	form.find("[required]").filter(function() {
		return !$(this).val() || $(this).parent(".inputField").hasClass("withError");
	}).first().focus();

	form.on("submit", function(event) {
		event.preventDefault();
		form.find("button").addClass("disabled").addClass("isLoading");
        form.find(".errorMsg").hide();
        form.find(".withError").removeClass("withError");
		var data = form.serializeArray();
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: data,
			success: function(data) {
				$("#regFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>