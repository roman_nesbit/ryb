<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<h3 class="title"><?=GetMessage('main_register_thankyou_note')?></h3>
<div class="buttonsRow">
	<a class="btn fullWidth" href="/login/?login=yes">
		<?=GetMessage('AUTH_AUTH_ACTION')?>
	</a>
</div>