<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Rybalka\Marketplace\Exception;
use Rybalka\Marketplace\Util\Phone\Normalizer;
use Rybalka\Marketplace\Http\StatusCode;
use Rybalka\Marketplace\Util\Phone\VerificationTable;
use Rybalka\Marketplace\Util\Recaptcha;
use Rybalka\Marketplace\Util\UserUtil;

// exo($_POST);

try {
    switch ($_POST['STEP']) {
        case 1:
			if (!$_POST['phoneNumber'] || !$_POST['captchaToken']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'phoneNumber']);
			}

			// Перевірка каптчі
			if(!Recaptcha::isHuman($_POST['captchaToken']))
			{
				throw new Exception\UnauthorizedException(StatusCode::UNAUTHORIZED, ['FIELD' => 'phoneNumber']);
			}

			// Нормалізуємо номер телефона
			if (!$normalizedPhoneNumber = Normalizer::normalizePhoneNumber($_POST['phoneNumber'])) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'phoneNumber']);
			}

			// Шукаємо користувача по номеру
			if (UserUtil::isUserByPhone($normalizedPhoneNumber)) {
				// The phone is already registered
				$arResult['STEP'] = 2; // Go to Step 2 (Already registered)
			} else {
				// The phone is not registered
				// Send code
				if ($response = VerificationTable::sendCode(
					$normalizedPhoneNumber,
					\Bitrix\Main\Service\GeoIp\Manager::getRealIp()
				)) {
					$arResult['verificationId'] = $response['verificationId'];
					$arResult['STEP'] = 3; // Go to Step 3 (SMS code input)
					$arResult['phoneNumber'] = $normalizedPhoneNumber;
					unset($_POST);
				} else {
					throw new Exception\RegistrationException($response['meta']['status'], [
						'FIELD' => 'phoneNumber',
						'WAITING' => $response['data']['waitingTimeInSeconds']
					]);
				}
			}
            break;
		
		case 3:
			if(!$_POST['verificationId'] || !$_POST['code']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'code']);
			}

			// Verify code
			if($response = VerificationTable::verify(
				$_POST['verificationId'],
				$_POST['code']
			)) {
				$arResult['verificationToken'] = $response['verificationToken'];
				$arResult['STEP'] = 4; // Go to Step 4 (Personal)
                unset($_POST);
			} else {
				throw new Exception\RegistrationException($response['meta']['status'], [
					'FIELD' => 'code',
					'WAITING' => $response['data']['waitingTimeInSeconds']
				]);
			}
			break;

		case 4:
			if(!$_POST['verificationToken']) {
				throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'verificationToken']);
			}

			// Check for required fields
			$arFieldsRequired = [
				'USER_NAME',
				'USER_LAST_NAME',
				'USER_PASSWORD',
				'USER_CONFIRM_PASSWORD',
			];
			foreach ($arFieldsRequired as $fieldRequired) {
				if (!$_POST[$fieldRequired]) {
					throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => $fieldRequired]);
				}
			}

			// Check for password = confirm_password
			if ($_POST['USER_PASSWORD'] !== $_POST['USER_CONFIRM_PASSWORD']) {
				throw new Exception\RegistrationException(StatusCode::FORBIDDEN, ['FIELD' => 'USER_PASSWORD']);
			}

			// Check for email validity and uniqueness
			if($_POST['USER_EMAIL']) {
				if (!filter_var($_POST['USER_EMAIL'], FILTER_VALIDATE_EMAIL)) {
		            throw new Exception\RegistrationException(StatusCode::BAD_REQUEST, ['FIELD' => 'USER_EMAIL']);
				}
				
				if(UserUtil::isUserByEmail($_POST['USER_EMAIL'])) {
					throw new Exception\RegistrationException(StatusCode::UNAUTHORIZED, ['FIELD' => 'USER_EMAIL']);
				}
    		}

			// Check for validity of verificationToken
			if(VerificationTable::isValidVerificationToken(
				$_POST['verificationToken']
			)) {
				// Get verified phone number
				if (!$phoneNumber = VerificationTable::getVerifiedPhoneNumber($_POST['verificationToken'])) {
					throw new Exception\RegistrationException(StatusCode::UNAUTHORIZED, ['FIELD' => 'verificationToken']);
				}

				// Register new user
				$user = new CUser;
				$arFields = array(
					'NAME'              => $_POST['USER_NAME'],
					'LAST_NAME'         => $_POST['USER_LAST_NAME'],
					'EMAIL'             => $_POST['USER_EMAIL'],
					'LOGIN'             => $phoneNumber,
					'ACTIVE'            => 'Y',
					'GROUP_ID'          => array(6),
					'PASSWORD'          => $_POST['USER_PASSWORD'],
					'CONFIRM_PASSWORD'  => $_POST['USER_CONFIRM_PASSWORD'],
					'PERSONAL_PHONE'    => $phoneNumber
				);
				$ID = $user->Add($arFields);
				if (intval($ID) > 0) {
					// echo 'Пользователь успешно добавлен.';
					$arResult['STEP'] = 5; // Go to Step 5 (Thankyou page)
					unset($_POST);
				} else {
					throw new Exception\RegistrationException(StatusCode::INTERNAL_SERVER_ERROR, ['FIELD' => 'verificationToken', 'LAST_ERROR' => $user->LAST_ERROR]);
				}
			}

			break;

		default:
			$arResult['STEP'] = 1; // Go to Step 1 (Phone input)
            break;

	}
}
catch (Exception\RegistrationException $e) {
	$arResult['ERROR'] = [
		'MESSAGE' => $e->getMessage(),
		'DATA' => $e->getData(),
	];
	foreach($_POST as $key => $value) {
		$arResult[$key] = $value;
	}
}
catch (Rybalka\Marketplace\Http\Exception\TooManyRequestsException $e) {
	switch($_POST['STEP']) {
		case 1:
			$errorField = 'phoneNumber';
			break;
	}

	$arResult['ERROR'] = [
		'MESSAGE' => StatusCode::TOO_MANY_REQUESTS,
		'DATA' => [
			'FIELD' => $errorField,
			'WAITING' => $e->getResponseBody()['waitingTimeInSeconds'],
		],
	];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\BadRequestException $e) {
	switch ($_POST['STEP']) {
        case 3:
            $errorField = 'code';
            break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::BAD_REQUEST,
        'DATA' => [
            'FIELD' => $errorField,
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\ForbiddenException $e) {
	switch ($_POST['STEP']) {
        case 3:
            $errorField = 'code';
            break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::FORBIDDEN,
        'DATA' => [
            'FIELD' => $errorField,
            'WAITING' => $e->getResponseBody()['waitingTimeInSeconds'],
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}
catch (Rybalka\Marketplace\Http\Exception\NotFoundException $e) {
	switch ($_POST['STEP']) {
        case 3:
            $errorField = 'code';
            break;
    }

    $arResult['ERROR'] = [
        'MESSAGE' => StatusCode::NOT_FOUND,
        'DATA' => [
            'FIELD' => $errorField,
        ],
    ];
    foreach ($_POST as $key => $value) {
        $arResult[$key] = $value;
    }
}