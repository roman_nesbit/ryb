<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<h3 class="title"><?=GetMessage('main_register_sms_note')?></h3>
<div class="separator"></div>
<form method="post" action="<?= SITE_TEMPLATE_PATH ?>/ajax/get_regform.php" id="regform_step_3">
	<input type="hidden" name="STEP" value="3" />
	<input type="hidden" name="verificationId" value="<?= $arResult['verificationId'] ?>" />
	<div class="fieldRow">
		<div class="inputField">
			<label><?=GetMessage('main_register_phone_number')?></label>
			<input type="tel" name="phoneNumber" value="<?=$arResult['phoneNumber']?>" readonly />
		</div>
	</div>
	<div class="fieldRow">
		<div class="inputField <?=($arResult['ERROR']['DATA']['FIELD'] == 'code' ? 'withError' : '')?>">
			<label><?=GetMessage('main_register_sms_code')?></label>
			<input type="tel" name="code" value="<?=$arResult['code']?>" required />
			<?php if ($arResult['ERROR']['DATA']['FIELD'] == 'code') : ?>
				<div class="errorMsg">
					<?=GetMessage($arResult['ERROR']['DATA']['FIELD'].'_'.$arResult['ERROR']['MESSAGE'], [
                        '#WAITING#' => $arResult['ERROR']['DATA']['WAITING']
                    ])?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="buttonsRow">
		<button class="btn fullWidth withIcon btnWithLoading">
			<svg class="loadingIcon">
				<use href="#loadingIcon" />
			</svg>
			<span><?=GetMessage('main_register_sms_send')?></span>
		</button>
	</div>
	<div class="codeInfoBox">
		<span><?=GetMessage('main_register_no_sms_note')?></span>
		<button class="resendBtn" type="button" id="changePhoneNumber"><?=GetMessage('main_register_no_sms_action')?></button>
	</div>
</form>
<script type="text/javascript">
	var form = $("#regform_step_3");

	form.find("[required]").filter(function() {
		return !$(this).val() || $(this).parent(".inputField").hasClass("withError");
	}).first().focus();

	form.on("submit", function(event) {
		event.preventDefault();
		form.find("button").addClass("disabled").addClass("isLoading");
        form.find(".errorMsg").hide();
        form.find(".withError").removeClass("withError");
		var data = form.serializeArray();
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: data,
			success: function(data) {
				$("#regFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});

	$("#changePhoneNumber").on("click", function() {
		$.ajax({
			type: form.attr("method"),
			url: form.attr("action"),
			data: {
				'phoneNumber': '<?=$arResult['phoneNumber']?>',
			},
			success: function(data) {
				$("#regFormHolder").html(data);
			},
			error: function(xhr, ajaxOptions, thrownError) {
			},
			complete: function() {
				form.find("button").removeClass("disabled").removeClass("isLoading");
			}
		});
	});
</script>