<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
	die();
}
$APPLICATION->AddChainItem(GetMessage('AUTH_REGISTER'));
?><section class="middleContainer formsContainer">
	<h1 class="pageTitle noBorder"><?=GetMessage('AUTH_REGISTER')?></h1>
	<div class="middleInner">
		<div class="mainFormsHolder" id="regFormHolder"><?php
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.registration",
				"ajax",
				array()
			);
		?></div>
	</div>
</section>