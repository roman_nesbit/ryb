<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;
use Rybalka\Marketplace\Util\InvoicePdf;

CJSCore::Init(array('clipboard', 'fx'));
?>
<section class="orderDetailsPage">
	<?
	if (!empty($arResult['ERRORS']['FATAL']))
	{
		foreach ($arResult['ERRORS']['FATAL'] as $errorCode => $error)
		{
			if($errorCode == 10004) continue;

			ShowError($error);
		}

		$component = $this->__component;

		if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
		{
			$APPLICATION->AuthForm('', false, false, 'N', false);
		}
	}
	else
	{
		if (!empty($arResult['ERRORS']['NONFATAL']))
		{
			foreach ($arResult['ERRORS']['NONFATAL'] as $error)
			{
				ShowError($error);
			}
		}
		?>
		<h1 class="pageTitle noBorder"><?=Loc::getMessage('SPOD_LIST_MY_ORDER', ['#ACCOUNT_NUMBER#' => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])])?></h1>
		<div class="innerPage threeCells">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"personal",
				Array(
					"ROOT_MENU_TYPE" => "personal",
					"MAX_LEVEL" => "1", 
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "A", 
					"MENU_CACHE_TIME" => "36000000", 
					"MENU_CACHE_USE_GROUPS" => "N", 
					"MENU_CACHE_GET_VARS" => "", 
					"HIDE_BASE_CLASS" => "Y",
				)
			);?>

			<div class="innerSide">
				<div class="cartInfoContainer">
					<div class="summaryHead _toggleCartInfoBox">
						<p>
							<svg class="basket">
								<use href="#cartIcon" />
							</svg>
							<?=Loc::getMessage('SPOD_ORDER')?> <?=Loc::getMessage('SPOD_TPL_SUMOF')?>
							<svg class="arrow">
								<use href="#arrowIcon" />
							</svg>
						</p>
						<div class="totalPrice"><?=$arResult['PRICE_FORMATED']?></div>
					</div>
					<div class="inner">
						<div class="products">
							<? foreach ($arResult['BASKET'] as $basketItem): ?>
								<? //exo($basketItem); ?>
								<div class="prodItem">
									<div class="prod">
										<div class="imgBox">
											<?
											if (strlen($basketItem['PICTURE']['SRC']))
											{
												$imageSrc = $basketItem['PICTURE']['SRC'];
											}
											else
											{
												$imageSrc = $this->GetFolder().'/images/no_photo.png';
											}
											?>
											<img src="<?=$imageSrc?>" alt="<?=$basketItem['NAME']?>" />
										</div>
										<div class="info">
											<h4 class="name"><?=$basketItem['NAME']?></h4>
											<div class="priceInfo">
												<div><?=$basketItem['PRICE_FORMATED']?></div>
												<div><?=$basketItem['QUANTITY']?> <?=$basketItem['MEASURE_NAME']?></div>
												<div><?=$basketItem['FORMATED_SUM']?></div>
											</div>
										</div>
									</div>
									<div class="priceInfo">
										<div><?=$basketItem['PRICE_FORMATED']?></div>
										<div><?=$basketItem['QUANTITY']?> <?=$basketItem['MEASURE_NAME']?></div>
										<div><?=$basketItem['FORMATED_SUM']?></div>
									</div>
								</div>
							<? endforeach; ?>
						</div>
						<div class="infoRow">
							<p><?=Loc::getMessage('SPOD_DELIVERY')?></p>
							<span><?=$arResult["PRICE_DELIVERY_FORMATED"]?></span>
						</div>
						<div class="infoRow totalBox">
							<p><?=Loc::getMessage('SPOD_SUMMARY')?></p>
							<span class="price"><?=$arResult['PRICE_FORMATED']?></span>
						</div>
						<div class="buttonsContainer">
							<div>
								<a class="btn fullWidth withIcon" href="<?=$arResult["URL_TO_COPY"]?>">
									<svg>
										<use href="#plusIcon"></use>
									</svg> <?=Loc::getMessage('SPOD_ORDER_REPEAT')?>
								</a>
							</div>
							<? if ($arResult["CAN_CANCEL"] === "Y"): ?>
							<div>
								<a class="btn fullWidth withIcon btnRed" href="<?=$arResult["URL_TO_CANCEL"]?>">
									<svg>
										<use href="#closeIcon"></use>
									</svg> <?=Loc::getMessage('SPOD_ORDER_CANCEL')?>
								</a>
							</div>
							<? endif; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="buttonsContainer">
				<div>
					<a class="btn fullWidth withIcon" href="<?=$arResult["URL_TO_COPY"]?>">
						<svg>
							<use href="#plusIcon"></use>
						</svg> <?=Loc::getMessage('SPOD_ORDER_REPEAT')?>
					</a>
				</div>
				<? if ($arResult["CAN_CANCEL"] === "Y"): ?>
				<div>
					<a class="btn fullWidth withIcon btnRed" href="<?=$arResult["URL_TO_CANCEL"]?>">
						<svg>
							<use href="#closeIcon"></use>
						</svg> <?=Loc::getMessage('SPOD_ORDER_CANCEL')?>
					</a>
				</div>
				<? endif; ?>
			</div>

			<div class="innerWrap">
				<div class="innerContent">
					<div class="orderInfoBox">
						<h4 class="title"><?=Loc::getMessage('SPOD_LIST_ORDER_INFO')?></h4>
						<div class="orderInfoDetails">
							<div class="orderInfoRow main">
								<label><?=Loc::getMessage('SPOD_ORDER_STATUS')?></label>
								<div class="desc">
									<? $statusData = getOrderStatusData($arResult['STATUS']['ID'][0]); ?>
									<div class="orderStatusLabel <?=$statusData['COLOR']?>">
										<svg>
											<use href="#<?=$statusData['ICON']?>" />
										</svg>
										<?=$arResult['STATUS']['NAME']?>
									</div>
								</div>
							</div>
							<div class="orderInfoRow">
								<label><?=Loc::getMessage('SPOD_ORDER_DATE')?></label>
								<div class="desc"><?=$arResult['DATE_INSERT']?></div>
							</div>
							<? if($arResult['USER_DESCRIPTION']): ?>
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_ORDER_DESC')?></label>
									<div class="desc"><?=$arResult['USER_DESCRIPTION']?></div>
								</div>
							<? endif; ?>
							<? foreach($arResult['ORDER_PROPS'] as $arProp): ?>
								<div class="orderInfoRow">
									<label><?=$arProp['NAME']?></label>
									<div class="desc">
										<? if($arProp['CODE'] == 'INVOICE_PDF') {
											$_invoicePdf = new InvoicePdf($arResult['ID']);
											$_invoiceUrl = $_invoicePdf->setFileName($arProp['VALUE'])->getInvoiceUrl(); ?>
											<a href="<?=$_invoiceUrl?>" target="_blank"><?=Loc::getMessage('SPOD_TPL_BILL_SHOW')?></a>
										<? } else {
												echo $arProp['VALUE'];
										} ?>
									</div>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					
					<? if($arResult['PARTNER'] != false): ?>
						<div class="orderInfoBox">
							<h4 class="title"><?=Loc::getMessage('SPOD_PARTNER')?></h4>
							<div class="orderInfoDetails">
								<? if($arResult['PARTNER']['PARTNER_LOGO']): ?>
									<div class="orderInfoRow">
										<div class="desc cartInfoContainer">
											<div class="prodItem">
												<div class="imgBox">
													<img src="<?=$arResult['PARTNER']['PARTNER_LOGO']?>" alt="<?=$arResult['PARTNER']['NAME']?>" />
												</div>
											</div>
										</div>
									</div>
								<? endif; ?>
								<? if($arResult['PARTNER']['PARTNER_NAME']): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_PARTNER_NAME')?></label>
										<div class="desc"><?=$arResult['PARTNER']['PARTNER_NAME']?></div>
									</div>
								<? endif; ?>
								<? if($arResult['PARTNER']['PARTNER_URL']): ?>
									<div class="orderInfoRow">
										<label></label>
										<div class="desc"><a href="<?=$arResult['PARTNER']['PARTNER_URL']?>"><?=Loc::getMessage('SPOD_PARTNER_URL')?></a></div>
									</div>
								<? endif; ?>
								<? if($arResult['PARTNER']['PARTNER_PHONE']): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_PARTNER_PHONE')?></label>
										<div class="desc"><?=$arResult['PARTNER']['PARTNER_PHONE']?></div>
									</div>
								<? endif; ?>
								<? if($arResult['PARTNER']['PARTNER_SETTLEMENT']): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_PARTNER_SETTLEMENT')?></label>
										<div class="desc"><?=$arResult['PARTNER']['PARTNER_SETTLEMENT']?></div>
									</div>
								<? endif; ?>
							</div>
						</div>
					<? endif; ?>

					<div class="orderInfoBox">
						<h4 class="title"><?=Loc::getMessage('SPOD_ORDER_PAYMENT')?></h4>
						<? foreach($arResult['PAYMENT'] as $arPayment): ?>
							<div class="orderInfoDetails">
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_PAY_SYSTEM')?></label>
									<div class="desc"><?=$arPayment['PAY_SYSTEM_NAME']?></div>
								</div>
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_ORDER_PAYMENT_STATUS')?></label>
									<div class="desc"><?=($arPayment['PAID'] == 'Y' ? Loc::getMessage('SPOD_PAYMENT_PAID') : Loc::getMessage('SPOD_PAYMENT_UNPAID'))?></div>
								</div>
								<? if($arPayment['PAY_VOUCHER_NUM'] || $arPayment['PAY_VOUCHER_DATE']): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_ORDER_DETAILS')?></label>
										<div class="desc"><?=$arPayment['PAY_VOUCHER_NUM']?> <?=$arPayment['PAY_VOUCHER_DATE']->toString()?></div>
									</div>
								<? endif; ?>
								<? if(count($arResult['PAYMENT']) > 1): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_TPL_BILL')?></label>
										<div class="desc"><?=$arPayment['ACCOUNT_NUMBER']?></div>
									</div>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_ORDER_PRICE')?></label>
										<div class="desc"><?=$arPayment['PRICE_FORMATED']?></div>
									</div>
								<? endif; ?>
								<?
								if ($arPayment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y' && $arResult["IS_ALLOW_PAY"] !== "N")
								{
									?>
									<a class="btn"
										target="_blank"
										href="<?=htmlspecialcharsbx($arPayment['PAY_SYSTEM']['PSA_ACTION_FILE'])?>">
										<?=Loc::getMessage('SPOD_ORDER_PAY')?>
									</a>
									<?
								}
								
								?>
							</div>
						<? endforeach; ?>
					</div>
					<div class="orderInfoBox">
						<h4 class="title"><?=Loc::getMessage('SPOD_ORDER_SHIPMENT')?></h4>
						<? foreach($arResult['SHIPMENT'] as $arShipment): ?>
							<div class="orderInfoDetails">
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_ORDER_DELIVERY')?></label>
									<div class="desc"><?=$arShipment['DELIVERY_NAME']?></div>
								</div>
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_DELIVERY')?></label>
									<div class="desc"><?=$arShipment['PRICE_DELIVERY_FORMATED']?></div>
								</div>
								<? if($arShipment['TRACKING_NUMBER']): ?>
									<div class="orderInfoRow">
										<label><?=Loc::getMessage('SPOD_ORDER_TRACKING_NUMBER')?></label>
										<div class="desc">
											<a href="https://novaposhta.ua/tracking/?cargo_number=<?=$arShipment['TRACKING_NUMBER']?>" target="_blank"><?=$arShipment['TRACKING_NUMBER']?></a>
										</div>
									</div>
								<? endif; ?>
								<div class="orderInfoRow">
									<label><?=Loc::getMessage('SPOD_ORDER_SHIPMENT_STATUS')?></label>
									<div class="desc"><?=$arShipment['STATUS_NAME']?></div>
								</div>
							</div>
						<? endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		
		<?
			$javascriptParams = array(
				"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
				"templateFolder" => CUtil::JSEscape($templateFolder),
				"templateName" => $this->__component->GetTemplateName(),
				"paymentList" => $paymentData
			);
			$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
		</script>
	<?
	}
	?>
</section>