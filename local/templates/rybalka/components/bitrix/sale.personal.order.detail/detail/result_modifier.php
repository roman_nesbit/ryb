<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Sale\Internals\OrderPropsValueTable;
use Rybalka\Marketplace\Repository\Dictionary\Address\SettlementsTable;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;

$settlementsService = new SettlementsService();

// Get order's partner
$dbRes = OrderPropsValueTable::getList([
	'select' => [
		'PARTNER_ID' => 'VALUE',
		'PARTNER_NAME' => 'PARTNER_DATA.NAME',
		'PARTNER_DETAIL_PICTURE' => 'PARTNER_DATA.DETAIL_PICTURE',
		'PARTNER_PREVIEW_PICTURE' => 'PARTNER_DATA.PREVIEW_PICTURE',
		'PARTNER_URL' => 'PARTNER_DATA.IBLOCK.DETAIL_PAGE_URL',
		'PARTNER_CODE' => 'PARTNER_DATA.CODE',
	],
	'filter' => [
		'ORDER_ID' => $arResult['ID'],
		'CODE' => 'PARTNER',
	],
	'runtime' => [
		new Bitrix\Main\Entity\ReferenceField(
			'PARTNER_DATA',
			'\Bitrix\Iblock\ElementTable',
			[
				'=this.VALUE' => 'ref.ID'
			]
		),
	],
]);

if ($data = $dbRes->fetch()) {
	if ($data['PARTNER_DETAIL_PICTURE'] || $data['PARTNER_PREVIEW_PICTURE']) {
		$data['PARTNER_LOGO'] = CFile::GetPath($data['PARTNER_PREVIEW_PICTURE'] ?? $data['PARTNER_DETAIL_PICTURE']);
	}
	unset($data['PARTNER_PREVIEW_PICTURE']);
	unset($data['PARTNER_DETAIL_PICTURE']);

	if ($data['PARTNER_CODE']) {
		$data['CODE'] = $data['PARTNER_CODE'];
	}

	if ($data['PARTNER_URL']) {
		$data['PARTNER_URL'] = CIBlock::ReplaceDetailUrl($data['PARTNER_URL'], $data, false);
	}

	$res = CIBlockElement::GetProperty(SHOPS_IBLOCK, $data['PARTNER_ID'], [], ['CODE' => 'SHOP_PHONE']);
	if ($ob = $res->GetNext()) {
		$data['PARTNER_PHONE'] = $ob['VALUE'];
	}

	$res = CIBlockElement::GetProperty(SHOPS_IBLOCK, $data['PARTNER_ID'], [], ['CODE' => 'SETTLEMENT']);
	if ($ob = $res->GetNext()) {
		$settlement = $settlementsService->get($ob['VALUE']);
	    $data['PARTNER_SETTLEMENT'] = (string)($settlement ?? '');
	}

	unset($data['CODE']);

	$arResult['PARTNER'] = $data;
} else {
	$data = false;
}
//sorting items in list by Name asc
sortBasketItemsByName($arResult['BASKET']);
