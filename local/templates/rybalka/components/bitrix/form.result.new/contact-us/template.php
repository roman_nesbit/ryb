<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/validate/jquery.validate.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/validate/additional-methods.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/validate/jquery.maskedinput.js');
?>

<section class="middleContainer formsContainer">
	<h1 class="pageTitle noBorder"><?= $arResult["FORM_TITLE"] ?></h1>

	<div class="middleInner">
		<div class="mainFormsHolder" id="regFormHolder">
			<?= $arResult["FORM_NOTE"] ?>

			<? if ($arResult["isFormNote"] != "Y") {

				echo $arResult["FORM_HEADER"];
				foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
					// exo($arQuestion);

					switch ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']):
						case 'hidden':
							echo $arQuestion["HTML_CODE"];
							break;

						case 'radio':
							?><div class="fieldRow" id="row_<?= $FIELD_SID ?>" <?= $arQuestion['CAPTION'] === 'Статус' ? 'style="display:none"' : '' ?>>
								<div class="optsField <?= $arResult['FORM_ERRORS'][$FIELD_SID] ? 'withError' : '' ?>">
									<label>
										<?= $arQuestion["CAPTION"] ?>
										<? if ($arQuestion["REQUIRED"] == "Y") : ?>
											<font color="red"><span class="form-required starrequired">*</span></font>
										<? endif; ?>
									</label>
									<div class="optsGroup"><?
										foreach ($arQuestion['STRUCTURE'] as $arField) :
											// exo($arField);
											if ($arField['FIELD_PARAM'] === 'checked') {
												$isChecked = true;
											} elseif ($arField['FIELD_TYPE'] == 'checkbox') {
												$isChecked = in_array($arField['ID'], $_REQUEST['form_' . $arField['FIELD_TYPE'] . '_' . $FIELD_SID]);
											} else {
												$isChecked = $arField['ID'] === $_REQUEST['form_' . $arField['FIELD_TYPE'] . '_' . $FIELD_SID];
											}
											?><div class="boxField">
												<label for="<?= $arField['VALUE'] ?>">
													<span class="<?= str_replace('box', '', $arField['FIELD_TYPE']) ?>Box">
														<input type="radio"
															id="<?= $arField['VALUE'] ?>" 
															name="form_radio_<?= $FIELD_SID ?><?= ($arField['FIELD_TYPE'] == 'checkbox' ? '[]' : '') ?>" 
															value="<?= $arField['ID'] ?>" 
															<?= $isChecked ? 'checked' : '' ?> />
														<span class="mark"></span>
													</span>
													<span class="label"><?= $arField['MESSAGE'] ?></span>
												</label>
												<? //exo($arField); 
												?>
											</div><?
										endforeach;
									?></div>
									<div class="errorMsg"><?= $arResult['FORM_ERRORS'][$FIELD_SID] ?? '' ?></div>
								</div>
							</div><?
							break;

						case 'textarea':
							?><div class="fieldRow" id="row_<?= $FIELD_SID ?>" <?= (in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE']) ? 'style="display:none"' : '') ?>>
								<div class="inputField <?= isset($arResult['FORM_ERRORS'][$FIELD_SID]) ? 'withError' : '' ?>">
									<label>
										<?= $arQuestion["CAPTION"] ?>
										<? if ($arQuestion["REQUIRED"] == "Y" || in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE'])) : ?>
											<font color="red"><span class="form-required starrequired">*</span></font>
										<? endif; ?>
									</label>
									<? foreach ($arQuestion['STRUCTURE'] as $arField) : ?>
										<textarea class="inputtext" 
											name="form_textarea_<?=$arField['ID']?>" 
											aria-invalid="false"
											id="<?=$FIELD_SID?>"><?=$_REQUEST['form_textarea_' . $arField['ID']]?></textarea>
										<div class="errorMsg"><?= $arResult['FORM_ERRORS'][$FIELD_SID] ?? '' ?></div>
									<? endforeach; ?>
								</div>
							</div><?
							break;

						case 'dropdown':
							?><div class="fieldRow" id="row_<?= $FIELD_SID ?>" <?= (in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE']) ? 'style="display:none"' : '') ?>>
								<div class="selectField <?= isset($arResult['FORM_ERRORS'][$FIELD_SID]) ? 'withError' : '' ?>">
									<label>
										<?= $arQuestion["CAPTION"] ?>
										<? if ($arQuestion["REQUIRED"] == "Y" || in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE'])) : ?>
											<font color="red"><span class="form-required starrequired">*</span></font>
										<? endif; ?>
									</label>
									<div class="selectBox">
										<select name="form_dropdown_<?=$FIELD_SID?>" 
											value="" 
											aria-invalid="false"
											id="<?=$FIELD_SID?>">
											<option value="">Выберите тему</option>
											<? foreach ($arQuestion['STRUCTURE'] as $arField) : ?>
												<option value="<?=$arField['ID']?>"><?=$arField['MESSAGE']?></option>
											<? endforeach; ?>
										</select>
										<svg>
                                            <use href="#arrowIcon"></use>
										</svg>
										<div class="errorMsg"><?= $arResult['FORM_ERRORS'][$FIELD_SID] ?? '' ?></div>
									</div>
								</div>
							</div><?
							break;

						default:
							?><div class="fieldRow" id="row_<?= $FIELD_SID ?>" <?= (in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE']) ? 'style="display:none"' : '') ?>>
								<div class="inputField <?= isset($arResult['FORM_ERRORS'][$FIELD_SID]) ? 'withError' : '' ?>">
									<label>
										<?= $arQuestion["CAPTION"] ?>
										<? if ($arQuestion["REQUIRED"] == "Y" || in_array($FIELD_SID, ['FEEDBACK_FORM_EMAIL', 'FEEDBACK_FORM_PHONE'])) : ?>
											<font color="red"><span class="form-required starrequired">*</span></font>
										<? endif; ?>
									</label>
									<? //exo($arQuestion) ?>
									<? foreach ($arQuestion['STRUCTURE'] as $arField) :
										//exo($arField); ?>
										<input type="text" 
											class="inputtext" 
											name="form_<?=$arField['FIELD_TYPE']?>_<?=$arField['ID']?>" 
											value="<?=$_REQUEST['form_' . $arField['FIELD_TYPE'] . '_' . $arField['ID']]?>" 
											aria-invalid="false"
											id="<?=$FIELD_SID?>" />
										<div class="errorMsg"><?= $arResult['FORM_ERRORS'][$FIELD_SID] ?? '' ?></div>
									<? endforeach; ?>
								</div>
							</div><?
							break;

					endswitch;
				} //endforeach

				if ($arResult["isFormErrors"] == "Y") {
					echo '<div class="errorMsg">' . $arResult["FORM_ERRORS_TEXT"] . '</div>';
				}

				?>

			<div class="buttonsRow">
				<button class="btn fullWidth withIcon btnWithLoading" type="button" onclick="checkFormAndSubmit($(this))">
					<svg class="loadingIcon">
						<use href="#loadingIcon" />
					</svg>
					<span><?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?></span>
				</button>
				<button type="submit" style="display:none">Submit</button>
			</div>

			<input type="hidden" name="web_form_submit" value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>" />

			<input type="hidden" name="g_recaptcha_token" value="" />

			<?
				echo $arResult["FORM_FOOTER"];
			} //endif (isFormNote)
			?>

		</div>
	</div>
</section>

<script>
	var theForm = $('form[name="<?= $arResult['WEB_FORM_NAME'] ?>"]');

	function checkFormAndSubmit(button) {
		if (theForm.valid()) {
			button.addClass('isLoading');
            grecaptcha.execute('<?= getenv('RECAPTCHA_SITE_KEY') ?>', {
                action: 'homepage'
            }).then(function(token) {
                $('input[name="g_recaptcha_token"]').val(token);
                button.next('button').trigger('click');
            });

		}
	}

	$(document).ready(function() {

		$('#FEEDBACK_FORM_PHONE').mask('+38(999) 999-99-99');
		
		theForm.validate({
			errorPlacement: function(error, element) {
				// console.log(element);
				var field = element.parents('.inputField, .optsField, .selectField');
				var errorMsg = field.find('.errorMsg');
				errorMsg.html(error[0].innerText);
				field.addClass('withError');
			},
			success: function(label, element) {
				// console.log('success', element);
				var field = $(element).parents('.inputField, .optsField, .selectField');
				field.removeClass('withError');
			},
			submitHandler: function(form) {
				// console.log('submitHandler');

				return true;
			}
		});

		$('#FEEDBACK_FORM_NAME').rules('add', {
			required: true,
			messages: {
				required: 'Введите имя'
			}
		});
		$('[name=form_radio_FEEDBACK_FORM_CONNECT_BY]').rules('add', {
			required: true,
			messages: {
				required: 'Выберите, как с вами связаться'
			}
		});
		$('#FEEDBACK_FORM_THEME').rules('add', {
			required: true,
			messages: {
				required: 'Выберите тему сообщения'
			}
		});
		$('#FEEDBACK_FORM_MESSAGE').rules('add', {
			required: true,
			minlength: 10,
			messages: {
				required: 'Введите текст сообщения',
				minlength: 'Сообщение не может такиим коротким'
			}
		});
		$('#FEEDBACK_FORM_PHONE').rules('add', {
			messages: {
				required: 'Введите телефон'
			}
		});
		$('#FEEDBACK_FORM_EMAIL').rules('add', {
			messages: {
				required: 'Введите email',
				email: 'Введите корректный email'
			}
		});

		<? if ($_REQUEST['form_radio_FEEDBACK_FORM_CONNECT_BY'] === '19') : ?>
			$('#row_FEEDBACK_FORM_EMAIL').show().find('input').rules('add', {
				required: true
			});
		<? endif; ?>
		<? if ($_REQUEST['form_radio_new_field_62059'] === '3') : ?>
			$('#row_SIMPLE_QUESTION_786').hide().find('input').rules('add', {
				required: true
			});
		<? endif; ?>

		$('[type="radio"]').change(function(e) {
			$('#row_FEEDBACK_FORM_EMAIL').fadeOut().find('input').rules('remove');
			$('#row_FEEDBACK_FORM_PHONE').fadeOut().find('input').rules('remove');
			switch (e.target.id) {
				case 'EMAIL':
					$('#row_FEEDBACK_FORM_EMAIL').fadeIn().find('input').rules('add', {
						required: true, 
						email: true
					});
					break;
				case 'PHONE':
					$('#row_FEEDBACK_FORM_PHONE').fadeIn().find('input').rules('add', {
						required: true
					});
					break;
				case 'EMAIL_OR_PHONE':
					$('#row_FEEDBACK_FORM_EMAIL').fadeIn().find('input').rules('add', {
						required: true, 
						email: true
					});
					$('#row_FEEDBACK_FORM_PHONE').fadeIn().find('input').rules('add', {
						required: true
					});
					break;
			}
		});

	});
</script>