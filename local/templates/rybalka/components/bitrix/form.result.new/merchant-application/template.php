<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/validate/jquery.validate.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/validate/jquery.maskedinput.js');
?>

<section class="middleContainer formsContainer">
	<h1 class="pageTitle noBorder"><?= $arResult["FORM_TITLE"] ?></h1>

	<div class="middleInner">
		<div class="mainFormsHolder" id="regFormHolder">
			<?
			// exo($arResult['FORM_ERRORS']);
			// exo($_REQUEST);
			?>
			<?= $arResult["FORM_NOTE"] ?>

			<? if ($arResult["isFormNote"] != "Y") {
	
				echo $arResult["FORM_HEADER"];
				foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
					// exo($arQuestion);

					if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
						echo $arQuestion["HTML_CODE"];
					} elseif(count($arQuestion['STRUCTURE']) > 1) {
					?>
						<div class="fieldRow"
							id="row_<?=$FIELD_SID?>"
							<?=$arQuestion['CAPTION'] === 'Статус' ? 'style="display:none"' : ''?>>
							<div class="optsField <?=$arResult['FORM_ERRORS'][$FIELD_SID] ? 'withError' : ''?>">
								<label>
									<?= $arQuestion["CAPTION"] ?>
									<? if ($arQuestion["REQUIRED"] == "Y") : ?>
										<font color="red"><span class="form-required starrequired">*</span></font>
									<? endif; ?>
								</label>
								<div class="optsGroup"><?
									foreach($arQuestion['STRUCTURE'] as $arField):
										if($arField['FIELD_PARAM'] === 'checked') {
											$isChecked = true;
										} elseif($arField['FIELD_TYPE'] == 'checkbox') {
											$isChecked = in_array($arField['ID'], $_REQUEST['form_'.$arField['FIELD_TYPE'].'_'.$FIELD_SID]);
										} else {
											$isChecked = $arField['ID'] === $_REQUEST['form_'.$arField['FIELD_TYPE'].'_'.$FIELD_SID];
										}
									?><div class="boxField">
										<label for="<?=$arField['ID']?>">
											<span class="<?=str_replace('box', '', $arField['FIELD_TYPE'])?>Box">
												<input id="<?=$arField['ID']?>"
													type="<?=$arField['FIELD_TYPE']?>"
													name="form_<?=$arField['FIELD_TYPE']?>_<?=$FIELD_SID?><?=($arField['FIELD_TYPE'] == 'checkbox' ? '[]' : '')?>"
													value="<?=$arField['ID']?>"
													<?= $isChecked ? 'checked' : ''?> />
												<span class="mark"></span>
											</span>
											<span class="label"><?=$arField['MESSAGE']?></span>
										</label>
										<? //exo($arField); ?>
									</div><?
									endforeach;
								?></div>
								<div class="errorMsg"><?=$arResult['FORM_ERRORS'][$FIELD_SID] ?? ''?></div>
							</div>
							<?//= $arQuestion["HTML_CODE"] ?>
						</div><?
					} else {
					?><div class="fieldRow"
						id="row_<?=$FIELD_SID?>"
						<?=(in_array($FIELD_SID, ['SIMPLE_QUESTION_362', 'SIMPLE_QUESTION_786']) ? 'style="display:none"' : '')?>>
						<div class="inputField <?=isset($arResult['FORM_ERRORS'][$FIELD_SID]) ? 'withError' : ''?>">
							<label>
								<?= $arQuestion["CAPTION"] ?>
								<? if ($arQuestion["REQUIRED"] == "Y" || in_array($FIELD_SID, ['SIMPLE_QUESTION_362', 'SIMPLE_QUESTION_786'])) : ?>
									<font color="red"><span class="form-required starrequired">*</span></font>
								<? endif; ?>
							</label>
							<?= $arQuestion["HTML_CODE"] ?>
							<div class="errorMsg"><?=$arResult['FORM_ERRORS'][$FIELD_SID] ?? ''?></div>
						</div>
					</div><?
					}
				} //endforeach

				if ($arResult["isFormErrors"] == "Y") {
					echo '<div class="errorMsg">' . $arResult["FORM_ERRORS_TEXT"] . '</div>';
				}

				?>

				<div class="buttonsRow">
					<button class="btn fullWidth withIcon btnWithLoading"
						type="button"
						onclick="checkFormAndSubmit($(this))">
						<svg class="loadingIcon">
							<use href="#loadingIcon" />
						</svg>
						<span><?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?></span>
					</button>
					<button type="submit" style="display:none">Submit</button>
				</div>

				<input type="hidden" name="web_form_submit" value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>" />
				
				<input type="hidden" name="g_recaptcha_token" value="" />

				<?
				echo $arResult["FORM_FOOTER"];
			} //endif (isFormNote)
			?>

		</div>
	</div>
</section>

<script>
	var theForm = $('form[name="<?=$arResult['WEB_FORM_NAME']?>"]');

	function checkFormAndSubmit(button) {
		if(theForm.valid()) {
			button.addClass('isLoading');

            grecaptcha.execute('<?=getenv('RECAPTCHA_SITE_KEY')?>', {action: 'homepage'})
                .then(function(token) {
                    $('input[name="g_recaptcha_token"]').val(token);
                    button.next('button').trigger('click');
                });
		}
	}

	$(document).ready(function() {

		$('input[name="form_text_8"]').mask('+38(999) 999-99-99');

		theForm.validate({
			rules: {
				form_radio_new_field_62059: {
					required: true
				},
				form_text_4: { 
					required: true
				},
				form_text_6: {
					required: true
				},
				form_text_7: {
					required: true
				},
				form_text_8: {
					required: true
				},
				form_email_9: {
					email: true
				},
			},
			messages: {
				form_radio_new_field_62059: {
					required: 'Выберите тип деятельности'
				},
				form_text_4: {
					required: 'Введите название ФОП'
				},
				form_text_5: {
					required: 'Введите название компании'
				},
				form_text_6: {
					required: 'Введите имя'
				},
				form_text_7: {
					required: 'Введите фамилию'
				},
				form_text_8: {
					required: 'Введите телефон'
				},
				form_email_9: {
					email: 'Введите корректный email'
				},
			},
			errorPlacement: function(error, element) {
				// console.log(element);
				var field = element.parents('.inputField, .optsField');
				var errorMsg = field.find('.errorMsg');
				errorMsg.html(error[0].innerText);
				field.addClass('withError');
			},
			success: function(label, element) {
				// console.log('success', element);
				var field = $(element).parents('.inputField, .optsField');
				field.removeClass('withError');
			},
			submitHandler: function(form) {
				// console.log('submitHandler');

				return true;
			}
		});

		<? if($_REQUEST['form_radio_new_field_62059'] === '2'): ?>
			$('#row_SIMPLE_QUESTION_362').show().find('input').rules('add', {required: true});
		<? endif; ?>
		<? if($_REQUEST['form_radio_new_field_62059'] === '3'): ?>
			$('#row_SIMPLE_QUESTION_786').show().find('input').rules('add', {required: true});
		<? endif; ?>

		$('[type="radio"]').change(function(e) {
			$('#row_SIMPLE_QUESTION_362').fadeOut().find('input').rules('remove');
			$('#row_SIMPLE_QUESTION_786').fadeOut().find('input').rules('remove');
			switch(e.target.value) {
				case '2':
					$('#row_SIMPLE_QUESTION_362').fadeIn().find('input').rules('add', {required: true});
					break;
				case '3':
					$('#row_SIMPLE_QUESTION_786').fadeIn().find('input').rules('add', {required: true});
					break;
			}
		});

	});
</script>