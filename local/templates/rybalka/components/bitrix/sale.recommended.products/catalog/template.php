<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

$arIds = $arResult['ID'];

if(count($arIds)) {
	$arFilter = [
		'ID' => $arIds,
	];

	$APPLICATION->IncludeComponent(
		"bfs:catalog.filtered.carousel", 
		".default", 
		array(
			"COMPONENT_TEMPLATE" => ".default",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"COUNT" => "20",
			"FILTER" => json_encode($arFilter),
			"SORT_FIELD" => "",
			"SORT_FIELD2" => "",
			"SORT_ORDER" => "",
			"SORT_ORDER2" => "",
			"TITLE" => GetMessage('SRP_HREF_TITLE'),
			"LINK_TO_ALL" => "",
			"HIDE_NOT_AVAILABLE" => "N",
		),
		$component
	);
}