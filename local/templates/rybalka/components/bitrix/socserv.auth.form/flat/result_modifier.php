<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach($arParams["~AUTH_SERVICES"] as $key => $service) {
	// exo($service);
	switch($arParams["~AUTH_SERVICES"][$key]['ICON']) {
		case 'facebook':
			$arParams["~AUTH_SERVICES"][$key]['CLASS'] = 'btnFacebook';
			$arParams["~AUTH_SERVICES"][$key]['ICON'] = 'facebookIcon';
			break;
		case 'google':
		case 'google-plus':
			$arParams["~AUTH_SERVICES"][$key]['CLASS'] = 'btnGooglePlus';
			$arParams["~AUTH_SERVICES"][$key]['ICON'] = 'googlePlusIcon';
			break;
	}
}