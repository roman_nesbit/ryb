<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>

<div class="postBox">
	<div class="postTitle"><a href="<?= $arResult['ITEM']['DETAIL_PAGE_URL'] ?>"><?= $arResult['ITEM']['NAME'] ?></a></div>
	<div class="img"><img src="<?= $arResult['ITEM']['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arResult['ITEM']['PREVIEW_PICTURE']['ALT'] ?>" /></div>
	<div class="desc">
		<p><?=$arResult['ITEM']['~PREVIEW_TEXT']?></p>
	</div>
	<div class="link"><a href="<?= $arResult['ITEM']['DETAIL_PAGE_URL'] ?>">Далее</a></div>
</div>