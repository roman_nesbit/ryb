<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$text = mb_substr($arResult['ITEM']['~PREVIEW_TEXT'], 0, 200);
$text = mb_substr($text, 0, mb_strrpos($text, ' '));
if (strlen($arResult['ITEM']['~PREVIEW_TEXT']) > strlen($text)) {
	$text .= '&#8230';
}

$arResult['ITEM']['~PREVIEW_TEXT'] = $text;