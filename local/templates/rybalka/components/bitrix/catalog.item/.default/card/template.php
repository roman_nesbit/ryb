<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

$dateItemCreate = new DateTime($item['DATE_CREATE']);
$now = new DateTime();
$daysFromItemCreate = $dateItemCreate->diff($now)->format('%a');

?>

<div class="rightInfo">
	<? if ($daysFromItemCreate <= \Bitrix\Main\Config\Option::get('grain.customsettings', 'PRODUCT_NEW_PERIOD')) : ?>
		<div class="newProdLabel">
			<span class="triangle"></span>
			<p><?= Loc::getMessage('CT_BCI_TPL_MESS_NEW') ?></p>
		</div>
	<? endif; ?>
	<button class="addToFavorite h2o_add_favor" data-id="<?= $item['ID'] ?>">
		<svg>
			<use href="#starIcon" />
		</svg>
		<svg>
			<use href="#starIconFull" />
		</svg>
	</button>
</div>
<? if ($price['PERCENT']) : ?>
	<div class="sale"><span>- <?=$price['PERCENT']?>%</span></div>
<? endif; ?>
<div class="img"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><img src="<?= ($item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE']['SRC'] : $item['DETAIL_PICTURE']['SRC']) ?>" alt="<?= $item['PREVIEW_PICTURE']['ALT'] ?>"></a></div>
<span id="<?= $itemIds['PICT'] ?>"></span>
<span id="<?= $itemIds['SECOND_PICT'] ?>"></span>
<span id="<?= $itemIds['PICT_SLIDER'] ?>"></span>
<div class="availability">
	<? if ($item['PROPERTIES']['RATING']['VALUE']) : ?>
		<div class="rating">
			<? for ($i = 1; $i <= 5; $i++) : ?>
				<? if ($i <= $item['PROPERTIES']['RATING']['VALUE']) : ?>
					<span class="star active">
						<svg>
							<use href="#starFullIcon" />
						</svg>
					</span>
				<? else : ?>
					<span class="star">
						<svg>
							<use href="#starFullIcon" />
						</svg>
					</span>
				<? endif; ?>
			<? endfor; ?>
			<span class="star" style="visibility:hidden">
				<svg>
					<use href="#starFullIcon" />
				</svg>
			</span>
		</div>
	<? endif; ?>
	<? if ($actualItem['CAN_BUY']) : ?>
		<span class="txt available">В наличии</span>
	<? else : ?>
		<span class="txt"><?= $arParams['MESS_NOT_AVAILABLE'] ?></span>
	<? endif; ?>
</div>
<div class="priceBox product-item-info-container product-item-price-container" data-entity="price-block">
	<? if ($arParams['SHOW_OLD_PRICE'] === 'Y') : ?>
		<span class="old product-item-price-old" id="<?= $itemIds['PRICE_OLD'] ?>" <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>><?= $price['PRINT_RATIO_BASE_PRICE'] ?></span>
	<? endif; ?>
	<div class="price">
		<span class="current product-item-price-current" id="<?= $itemIds['PRICE'] ?>"><?= $price['PRINT_RATIO_PRICE'] ?></span>
	</div>
</div>
<div class="name">
	<h2><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a></h2>
</div>
<input id="<?= $itemIds['QUANTITY'] ?>" type="hidden" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="1" />
<span id="<?= $itemIds['QUANTITY_DOWN'] ?>"></span>
<span id="<?= $itemIds['QUANTITY_UP'] ?>"></span>
<div class="btnBox">
	<?
	if (!$haveOffers) {
		if ($actualItem['CAN_BUY']) {
			// exo($arParams);
	?>
			<div class="product-item-button-container" id="<?= $itemIds['BASKET_ACTIONS'] ?>">
				<a class="btn withIcon main btnWithLoading <?= ($item['IN_BASKET'] ? 'isComplete' : '') ?>" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0)" rel="nofollow">
					<svg class="cartIcon">
						<use href="#cartIcon" />
					</svg>
					<svg class="loadingIcon">
						<use href="#loadingIcon" />
					</svg>
					<svg class="correctIcon">
						<use href="#correctIcon" />
					</svg>
					<span class="primary"><?
											echo ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']);
											?></span>
					<span class="secondary"><?= Loc::getMessage('CT_PRODUCT_ADDED_TO_CART') ?></span>
				</a>
			</div>
		<?
		} else {
		?>
			<div class="product-item-button-container">
				<?
				if ($showSubscribe) {
					$APPLICATION->IncludeComponent(
						'bitrix:catalog.product.subscribe',
						'',
						array(
							'PRODUCT_ID' => $actualItem['ID'],
							'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
							'BUTTON_CLASS' => 'btn withIcon main',
							'DEFAULT_DISPLAY' => true,
							'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}
				?>
				<!-- <a class="btn withIcon"
						id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>"
						href="javascript:void(0)"
						rel="nofollow"><?= $arParams['MESS_NOT_AVAILABLE'] ?></a> -->
			</div>
		<?
		}
	} else {
		if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
		?>
			<div class="product-item-button-container">
				<?
				if ($showSubscribe) {
					$APPLICATION->IncludeComponent(
						'bitrix:catalog.product.subscribe',
						'',
						array(
							'PRODUCT_ID' => $item['ID'],
							'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
							'BUTTON_CLASS' => 'btn btn-default ' . $buttonSizeClass,
							'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
							'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}
				?>
				<a class="btn btn-link <?= $buttonSizeClass ?>" id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)" rel="nofollow" <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>><?= $arParams['MESS_NOT_AVAILABLE'] ?></a>
				<div id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
					<a class="btn btn-default <?= $buttonSizeClass ?>" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0)" rel="nofollow">
						<?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
					</a>
				</div>
			</div>
		<?
		} else {
		?>
			<div class="product-item-button-container">
				<a class="btn btn-default <?= $buttonSizeClass ?>" href="<?= $item['DETAIL_PAGE_URL'] ?>">
					<?= $arParams['MESS_BTN_DETAIL'] ?>
				</a>
			</div>
	<?
		}
	}
	?>
</div>