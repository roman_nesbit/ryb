<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>
<div class="postItem">
	<div class="img"><a href="<?= $arResult['ITEM']['DETAIL_PAGE_URL'] ?>"><img src="<?= $arResult['ITEM']['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arResult['ITEM']['PREVIEW_PICTURE']['ALT'] ?>" /></a></div>
	<div class="details">
		<div class="category"><a href="<?= $arResult['SECTION']['URL']?>" class="category"><?=$arResult['SECTION']['NAME']?></a></div>
		<div class="name"><a href="<?= $arResult['ITEM']['DETAIL_PAGE_URL'] ?>"><?= $arResult['ITEM']['NAME'] ?></a></div>
		<div class="hint">
			<!-- <span class="tag">Статті</span> -->
			<span class="time"><?=$arResult['ITEM']['DATE_CREATE']?></span>
		</div>
	</div>
</div>