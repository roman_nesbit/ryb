<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$section = getSectionDataById(TACKLE_REVIEWS_IBLOCK, $arResult['ITEM']['IBLOCK_SECTION_ID']);

$arResult['SECTION'] = [
	'NAME' => $section['NAME'],
	'URL' => $section['SECTION_PAGE_URL'],
];

$arResult['ITEM']['DATE_CREATE'] = FormatDate('X', MakeTimeStamp($arResult['ITEM']['DATE_CREATE']));