<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<section class="middleContainer">
	<h1 class="pageTitle noBorder"><?=GetMessage('AUTH_TITLE')?></h1>
	<div class="middleInner">
		<div class="signInBody authorizationBox">
			<?
			// exo($arParams["~AUTH_RESULT"]);
			// exo($arResult['ERROR_MESSAGE']);
			?>
			<? if($arParams['~AUTH_RESULT']['ERROR_TYPE'] != 'LOGIN'): ?>
				<h3 class="infoTitle"><?=$arParams['~AUTH_RESULT']['MESSAGE']?></h3>
			<? endif; ?>
			<? ShowMessage($arResult['ERROR_MESSAGE']); ?>

			<div class="bx-auth">
				<form class="signInForm"
					name="form_auth"
					method="post"
					target="_top"
					action="<?=$arResult["AUTH_URL"]?>">

					<input type="hidden" name="AUTH_FORM" value="Y" />
					<input type="hidden" name="TYPE" value="AUTH" />
					<?if (strlen($arResult["BACKURL"]) > 0):?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
					<?endif?>
					<?foreach ($arResult["POST"] as $key => $value):?>
						<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
					<?endforeach?>

					<div class="form-group <?=($arParams['~AUTH_RESULT']['ERROR_TYPE'] == 'LOGIN' ? 'withError' : '')?>">
						<label><?=GetMessage("AUTH_LOGIN")?></label>
						<input type="text"
							name="USER_LOGIN"
							maxlength="255"
							value="<?=($_REQUEST['USER_LOGIN'] ? $_REQUEST['USER_LOGIN'] : $arResult['LAST_LOGIN'])?>"
							required="required" />
						<? if($arParams['~AUTH_RESULT']['ERROR_TYPE'] == 'LOGIN'): ?>
							<div class="errorMsg"><?=$arParams['~AUTH_RESULT']['MESSAGE']?></div>
						<? endif; ?>
					</div>
					<div class="form-group">
						<label><?=GetMessage("AUTH_PASSWORD")?></label>
						<input type="password"
							name="USER_PASSWORD"
							maxlength="255"
							autocomplete="off"
							required="required" />
						<?if($arResult["SECURE_AUTH"]):?>
							<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
								<div class="bx-auth-secure-icon"></div>
							</span>
							<noscript>
								<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
									<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
								</span>
							</noscript>
							<script type="text/javascript">
								document.getElementById('bx_auth_secure').style.display = 'inline-block';
							</script>
						<?endif?>
					</div>

					<?if($arResult["CAPTCHA_CODE"]):?>
						<tr>
							<td></td>
							<td><input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></td>
						</tr>
						<tr>
							<td class="bx-auth-label"><?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:</td>
							<td><input class="bx-auth-input form-control" type="text" name="captcha_word" maxlength="50" value="" size="15" /></td>
						</tr>
					<?endif;?>

					<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
						<tr>
							<td></td>
							<td><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" /><label for="USER_REMEMBER">&nbsp;<?=GetMessage("AUTH_REMEMBER_ME")?></label></td>
						</tr>
					<?endif?>

					<div class="form-footer">
						<button type="submit" class="btn btnSignIn" name="Login">
							<?=GetMessage("AUTH_AUTHORIZE")?>
						</button>
						<?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
							<noindex>
								<a class="forgotPassword" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
							</noindex>
						<?endif?>
					</div>
				</form>

				<?if($arParams["NOT_SHOW_LINKS"] != "Y"
					&& $arResult["NEW_USER_REGISTRATION"] == "Y" 
					&& $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
					<noindex>
						<div class="authRegisterInfo">
							<?=GetMessage('AUTH_FIRST_ONE')?> <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="dottedTxt"><?=GetMessage('AUTH_REGISTER')?></a>.
						</div>
					</noindex>
				<?endif?>
			</div>

			<script type="text/javascript">
				try{document.form_auth.USER_LOGIN.focus();}catch(e){}
			</script>

			<?if($arResult["AUTH_SERVICES"]):?>
				<div class="signInBySocial">
					<p>Или войти через</p>
					<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
						"flat",
						array(
							"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
							"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
							"AUTH_URL" => $arResult["AUTH_URL"],
							"POST" => $arResult["POST"],
							"SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
							"FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
							"AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
						),
						$component,
						array("HIDE_ICONS"=>"Y")
					); ?>
				</div>
			<?endif?>
		</div>
	</div>
</section>