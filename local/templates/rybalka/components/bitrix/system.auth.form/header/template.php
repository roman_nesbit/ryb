<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init();
?>

<?if($arResult["FORM_TYPE"] == "login"):?>
	<div class="signInBody">
		<? if($arResult['ERROR_MESSAGE']['TYPE'] !== 'OK'): ?>
			<form class="signInForm"
				name="system_auth_form<?=$arResult["RND"]?>"
				method="post"
				target="_top"
				action="<?=$arResult["AUTH_URL"]?>">

				<?if($arResult["BACKURL"] <> ''):?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />

				<div class="form-group">
					<label for="login"><?=GetMessage("AUTH_LOGIN")?></label>
					<input type="text" id="login" name="USER_LOGIN" maxlength="50" value="" size="17" required />
					<script>
						BX.ready(function() {
							var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
							if (loginCookie)
							{
								var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
								var loginInput = form.elements["USER_LOGIN"];
								loginInput.value = loginCookie;
							}
						});
					</script>
				</div>
				<div class="form-group">
					<label for="password"><?=GetMessage("AUTH_PASSWORD")?></label>
					<input type="password" id="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" required />
					<?if($arResult["SECURE_AUTH"]):?>
						<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
							<div class="bx-auth-secure-icon"></div>
						</span>
						<noscript>
							<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
								<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
							</span>
						</noscript>
						<script type="text/javascript">
							document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
						</script>
					<?endif?>
				</div>

				<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
					<div class="form-group">
						<label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> <?echo GetMessage("AUTH_REMEMBER_SHORT")?></label>
					</div>
				<?endif?>
				<?if ($arResult["CAPTCHA_CODE"]):?>
					<div class="form-group">
						<label><?echo GetMessage("AUTH_CAPTCHA_PROMT")?></label>
						<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br />
						<input type="text" name="captcha_word" maxlength="50" value="" />
					</div>
				<?endif?>
				<div class="form-footer">
					<button type="submit" name="Login" class="btn btnSignIn _btnSignIn">
						<?=GetMessage("AUTH_LOGIN_BUTTON")?>
					</button>
					<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="forgotPassword" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
				</div>
			</form>
		<? endif; ?>
		<?
			if($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
				if($arResult['ERROR_MESSAGE']['TYPE'] == 'OK') {
					echo '<p class="successMsg">' . $arResult['ERROR_MESSAGE']['MESSAGE'] . '</p>';
				} elseif($arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR') {
					echo '<p class="errorMsg">' . $arResult['ERROR_MESSAGE']['MESSAGE'] . '</p>';
				} else {
					echo '<p>' . $arResult['ERROR_MESSAGE']['MESSAGE'] . '</p>';
				}
			}
		?>

		<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
			<div class="singUpWays">
				<p><?=GetMessage('AUTH_REGISTER_AS')?></p>
				<div class="singUpLinks">
					<a href="<?=$arResult["AUTH_REGISTER_URL"]?>">
						<?=GetMessage('AUTH_REGISTER_AS_BUYER')?>
					</a>
					<div class=" verticalLine"></div>
					<a href="<?=$arParams["MERCHANT_APPLICATION_URL"]?>">
						<?=GetMessage('AUTH_REGISTER_AS_SELLER')?>
					</a>
				</div>
			</div>
		<?endif?>
		<?if($arResult["AUTH_SERVICES"]):?>
			<div class="signInBySocial">
				<p><?=GetMessage("socserv_as_user_form")?></p>
				<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat", 
						array(
							"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
							"SUFFIX"=>"form",
						), 
						$component, 
						array("HIDE_ICONS"=>"Y")
					);
				?>
			</div>
		<?endif; ?>
	</div>

<? elseif($arResult["FORM_TYPE"] == "otp"): ?>

	<div class="signInBody">
		<form class="signInForm" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<?if($arResult["BACKURL"] <> ''):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="OTP" />
			<table width="95%">
				<tr>
					<td colspan="2">
					<?echo GetMessage("auth_form_comp_otp")?><br />
					<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
				</tr>
		<?if ($arResult["CAPTCHA_CODE"]):?>
				<tr>
					<td colspan="2">
					<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
					<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
					<input type="text" name="captcha_word" maxlength="50" value="" /></td>
				</tr>
		<?endif?>
		<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
				<tr>
					<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
					<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
				</tr>
		<?endif?>
				<tr>
					<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
				</tr>
				<tr>
					<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
				</tr>
			</table>
		</form>
	</div>
<? else: ?>
	<div class="profileBlock">
		<form action="<?=$arResult["AUTH_URL"]?>">
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"account",
				Array(
					"ROOT_MENU_TYPE" => "personal",
					"MAX_LEVEL" => "1", 
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "A", 
					"MENU_CACHE_TIME" => "36000000", 
					"MENU_CACHE_USE_GROUPS" => "N", 
					"MENU_CACHE_GET_VARS" => "" 
				)
			);?>
			<div class="exit">
				<button class="_btnSignOut" type="submit" name="logout_butt"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></button>
			</div>
		</form>
	</div>
<?endif?>