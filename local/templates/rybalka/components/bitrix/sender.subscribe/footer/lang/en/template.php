<?
$MESS["subscr_form_title_desc"] = "Select Subscription";
$MESS["subscr_form_email_title"] = "Email";
$MESS["subscr_form_button"] = "Subscribe";
$MESS ['subscr_form_subtext'] = 'Want to get discounts and recommendations?';
$MESS["subscr_form_response_ERROR"] = "Something's gone wrong.";
$MESS["subscr_form_response_NOTE"] = "Congratulations!";
$MESS["subscr_form_button_sent"] = "DONE";
?>