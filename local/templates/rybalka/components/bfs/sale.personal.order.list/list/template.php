<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section class="ordersPage">
	<?
	if (!empty($arResult['ERRORS']['FATAL']))
	{
		foreach($arResult['ERRORS']['FATAL'] as $errorCode => $error)
		{
			if($errorCode == 10004) continue;

			ShowError($error);
		}
		$component = $this->__component;
		if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
		{
			$APPLICATION->AuthForm('', false, false, 'N', false);
		}

	}
	else
	{
		if (!empty($arResult['ERRORS']['NONFATAL']))
		{
			foreach($arResult['ERRORS']['NONFATAL'] as $error)
			{
				ShowError($error);
			}
		}?>

		<div class="pageTitleWithSearch">
			<h1 class="pageTitle"><?=$APPLICATION->GetTitle()?></h1>
			<form class="searchBar" action="">
				<div class="showSearchBtn _showSearchFieldBtn">
					<svg>
						<use href="#searchIcon" />
					</svg>
				</div>
				<div class="searchField">
					<input type="text" placeholder="<?=GetMessage('STPOL_SEARCH_TEXT')?>" name="qo" value="<?=($_REQUEST['qo'] ? $_REQUEST['qo'] : '')?>" />
					<button class="search" type="submit">
						<svg>
							<use href="#searchIcon" />
						</svg>
					</button>
					<button class="close _hideSearchField">
						<svg>
							<use href="#closeIcon" />
						</svg>
					</button>
				</div>
			</form>
		</div>

		<div class="innerPage twoCells">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"personal",
				Array(
					"ROOT_MENU_TYPE" => "personal",
					"MAX_LEVEL" => "1", 
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "A", 
					"MENU_CACHE_TIME" => "36000000", 
					"MENU_CACHE_USE_GROUPS" => "N", 
					"MENU_CACHE_GET_VARS" => "" 
				)
			);?>

			<div class="innerWrap">
				<div class="innerContent">
					<div class="infoTable ordersTable">
						<div class="infoRow headRow">
							<div class="imgCell"><?=GetMessage('STPOL_ORDER')?></div>
							<div class="infoCell">
								<div class="cell mainIfo"></div>
								<div class="cell"><?=GetMessage('STPOL_STATUS_T')?></div>
								<div class="cell"><?=GetMessage('STPOL_SUM')?></div>
							</div>
						</div>
						<? foreach($arResult['ORDERS'] as $arOrder): ?>
							<div class="infoRow">
								<div class="imgCell">
									<div class="imgBox"><a href="<?=$arOrder['URL_TO_DETAIL']?>"><img src="<?=$arOrder['PHOTO']?>" alt="<?=$arOrder['ACCOUNT_NUMBER']?>"></a></div>
								</div>
								<div class="infoCell">
									<div class="cell mainIfo">
										<h3 class="title"><a href="<?=$arOrder['URL_TO_DETAIL']?>"><?=GetMessage('STPOL_ORDER_NO')?> <?=$arOrder['ACCOUNT_NUMBER']?></a></h3>
										<div class="date"><?=$arOrder['DATE_INSERT']?></div>
										<div class="link ordersAmount"><a href="<?=$arOrder['URL_TO_DETAIL']?>">
											<?=($arOrder['PRODUCT_NAME'] ? $arOrder['PRODUCT_NAME'] : GetMessage('STPOL_IN_ORDER') . ' ' . $arOrder['QUANTITY_TOTAL'])?>
										</a></div>
										<!-- <div class="ordersAmount"><?=GetMessage('STPOL_SHOP')?><?=$arOrder['PARTNER']?></div> -->
									</div>
									<div class="cell statusCell">
										<div class="orderInfoBox">
											<div class="orderStatusLabel <?=$arOrder['STATUS']['DATA']['COLOR']?>">
												<svg>
													<use href="#<?=$arOrder['STATUS']['DATA']['ICON']?>" />
												</svg>
												<?=$arOrder['STATUS']['NAME']?>
											</div>
										</div>
									</div>
									<div class="cell priceCell">
										<div class="price"><?=$arOrder['FORMATED_PRICE']?></div>
										<div class="status orderInfoBox">
											<div class="orderStatusLabel <?=$arOrder['STATUS']['DATA']['COLOR']?>">
												<svg>
													<use href="#<?=$arOrder['STATUS']['DATA']['ICON']?>" />
												</svg>
												<?=$arOrder['STATUS']['NAME']?>
											</div>
										</div>
										<div class="link ordersAmount"><a href="#"><?=GetMessage('STPOL_IN_ORDER')?> <?=$arOrder['QUANTITY_TOTAL']?></a></div>
										<!-- <div class="ordersAmount"><?=GetMessage('STPOL_SHOP')?><?=$arOrder['PARTNER']?></div> -->
									</div>
								</div>
							</div>
							<? //exo($arOrder); ?>
						<? endforeach; ?>
					</div>
				</div>

				<?=$arResult["NAV_STRING"]?>
			</div>
		</div>
	<?
	}
	?>
</section>