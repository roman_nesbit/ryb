<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['ORDERS'] as $key => $arOrder) {
	// exo($arOrder['BASKET_ITEMS']);
	// exo($arOrder['ORDER']);

	// загальна кількість товарів
	$quantityTotal = 0;
	// індекс найдорожчого товара
	$mostExpensiveProductKey = 0;
	foreach($arOrder['BASKET_ITEMS'] as $basketKey => $arBasketItem) {
		$quantityTotal += $arBasketItem['QUANTITY'];

		if($mostExpensiveProductKey) {
			if($arOrder['BASKET_ITEMS'][$basketKey]['PRICE'] > $arOrder['BASKET_ITEMS'][$mostExpensiveProductKey]['PRICE']) {
				$mostExpensiveProductKey = $basketKey;
			}
		} else {
			$mostExpensiveProductKey = $basketKey;
		}
	}

	// статус
	$arStatus = CSaleStatus::GetByID($arOrder['ORDER']['STATUS_ID']);

	// властивості замовлення
	$resProps = CSaleOrderPropsValue::GetOrderProps($arOrder['ORDER']['ID']);
	while($arProp = $resProps->Fetch()) {
		if($arProp['CODE'] == 'PARTNER') {
			$partnerId = $arProp['VALUE'];
		}
		// exo($arProp);
	}

	$arResult['ORDERS'][$key] = [
		'ID' => $arOrder['ORDER']['ID'],
		'URL_TO_DETAIL' => $arOrder['ORDER']['URL_TO_DETAIL'],
		'ACCOUNT_NUMBER' => $arOrder['ORDER']['ACCOUNT_NUMBER'],
		'DATE_INSERT' => $arOrder['ORDER']['DATE_INSERT']->toString(),
		'FORMATED_PRICE' => $arOrder['ORDER']['FORMATED_PRICE'],
		'QUANTITY_TOTAL' => $quantityTotal . ' ' . pluralForm($quantityTotal, GetMessage('STPOL_GOODS_1'), GetMessage('STPOL_GOODS_2'), GetMessage('STPOL_GOODS_3')),
		'STATUS' => [
			'DATA' => getOrderStatusData($arStatus['ID'][0]),
			'NAME' => $arStatus['NAME'],
		],
		'PARTNER' => getElementNameById($partnerId),
		'PHOTO' => getElementPreviewPictureById($arOrder['BASKET_ITEMS'][$mostExpensiveProductKey]['PRODUCT_ID']),
		'PRODUCT_ID' => $arOrder['BASKET_ITEMS'][$mostExpensiveProductKey]['PRODUCT_ID'],
		'PRODUCT_NAME' => (count($arOrder['BASKET_ITEMS']) == 1 ? $arOrder['BASKET_ITEMS'][key($arOrder['BASKET_ITEMS'])]['NAME'] : false),
	];
}
?>