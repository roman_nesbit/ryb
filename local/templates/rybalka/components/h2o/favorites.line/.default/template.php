<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->createFrame()->begin("");
?>
<a href="<?=$arParams['URL_LIST']?>" class="favor-list-wrap iconBtn favorites <?=($arResult['COUNT'] ? 'active' : '')?>"><?
	if($arResult['COUNT']):
		?><span class="count-favor-item"><?=$arResult['COUNT']?></span><? 
	endif;
	?><svg><use href="#starIcon" /></svg>
</a>