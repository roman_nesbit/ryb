<?
$MESS ["H2O_FAVOR_BUTTON_IN_FAVOR"] = "<svg><use href='#starIconFull' /></svg><span>В обраному</span>";
$MESS ['H2O_FAVOR_BUTTON_NOT_IN_FAVOR'] = "<svg><use href='#starIcon' /></svg><span>Додати в обране</span>";
$MESS ['H2O_FAVORITES_FATAL_ERROR_TITLE'] = "Що щось пішло не так.";
$MESS ['H2O_FAVORITES_FATAL_ERROR_TEXT'] = "Будь ласка обновіть сторінку і спробуйте ще раз.";
$MESS ['H2O_FAVORITES_SUCCESS_TITLE'] = "Товар успішно доданий до вибраних";
$MESS ['H2O_FAVORITES_SUCCESS_TEXT'] = "Ми отримали Ваш запит. <br /> Як тільки товар надійде на склад, ми повідомимо Вам про це.";
$MESS ['H2O_FAVORITES_ALREADY_TITLE'] = "Ви вже підписувалися на цей товар!";
$MESS ['H2O_FAVORITES_ALREADY_TEXT'] = "Але у нас його все ще немає :(";
$MESS ['H2O_FAVORITES_ZAKAZAT'] = "Замовити";
$MESS ['H2O_FAVORITES_EMPTY_VALUE'] = "Пусте значення";
$MESS ['H2O_FAVORITES_WRONG_EMAIL'] = "Невірний email";
$MESS ['H2O_FAVORITES_NAME'] = "Ваше ім'я";
$MESS ['H2O_FAVORITES_COMMENT'] = "Коментар";
$MESS ['H2O_FAVORITES_SUBMIT'] = "Відправити";
?>