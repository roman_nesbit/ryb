<?
$MESS["H2O_FAVORITES"] = "Вибране";
$MESS['H2O_FAVORITES_DELETE_ITEM'] = "Видалити";
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Буде видалена вся інформація, пов'язана з цим записом. Продовжити?";
$MESS["H2O_FAVORITES_FIELD_DATE_INSERT"] = 'Дата створення';
$MESS["H2O_FAVORITES_FIELD_ELEMENT"] = 'Товар';
$MESS["H2O_FAVORITES_FIELD_NOTIFIED"] = "Товар прийшов";
$MESS["H2O_FAVORITES_FIELD_DELETE"] = "";
$MESS["H2O_FAVORITES_EMPTY_LIST"] = "Список заявок порожній";
$MESS["H2O_FAVORITES_AVAILABLE"] = "Є";
$MESS["H2O_FAVORITES_NOT_AVAILABLE"] = "Ні";
$MESS["H2O_FAVORITES_BUY"] = "В кошик";
$MESS["H2O_DELETED_NOTE"] = "Товар <strong> #PRODUCT # </ strong> був видалений з обраних";
$MESS["H2O_DELETED_RECOVER"] = "Відновити товар";
$MESS["H2O_FAVORITES_PRODUCT"] = "Товар";
$MESS["H2O_FAVORITES_AVAILABILITY"] = "Наявність";
$MESS["H2O_FAVORITES_PRICE"] = "Ціна";
$MESS["H2O_CART_NOTE"] = "Товар <strong> #PRODUCT # </ strong> доданий в кошик";
$MESS["H2O_CART_RECOVER"] = "Повернути в обране";
?>