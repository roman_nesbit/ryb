<?
$MESS["H2O_FAVORITES"] = "Избранное";
$MESS['H2O_FAVORITES_DELETE_ITEM'] = "Удалить";
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["H2O_FAVORITES_FIELD_DATE_INSERT"] = 'Дата создания';
$MESS["H2O_FAVORITES_FIELD_ELEMENT"] = 'Товар';
$MESS["H2O_FAVORITES_FIELD_NOTIFIED"] = "Товар пришел";
$MESS["H2O_FAVORITES_FIELD_DELETE"] = "";
$MESS["H2O_FAVORITES_EMPTY_LIST"] = "Список заявок пуст";
$MESS["H2O_FAVORITES_AVAILABLE"] = "Есть";
$MESS["H2O_FAVORITES_NOT_AVAILABLE"] = "Нет";
$MESS["H2O_FAVORITES_BUY"] = "В корзину";
$MESS["H2O_DELETED_NOTE"] = "Товар <strong>#PRODUCT#</strong> был удален из избранных";
$MESS["H2O_DELETED_RECOVER"] = "Восстановить товар";
$MESS["H2O_FAVORITES_PRODUCT"] = "Товар";
$MESS["H2O_FAVORITES_AVAILABILITY"] = "Наличие";
$MESS["H2O_FAVORITES_PRICE"] = "Цена";
$MESS["H2O_CART_NOTE"] = "Товар <strong>#PRODUCT#</strong> добавлен в корзину";
$MESS["H2O_CART_RECOVER"] = "Вернуть в избранное";
?>