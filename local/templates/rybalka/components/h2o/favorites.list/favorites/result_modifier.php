<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['FAVORITES'] as $key => $item) {
	$product = \Bitrix\Catalog\ProductTable::getRow([
		'filter' => [
			'ID' => $item['ELEMENT']['ID'],
		]
	]);
	$product['QUANTITY_TRACE'] = 'Y';
	$product['CAN_BUY_ZERO'] = 'N';
	$arResult['FAVORITES'][$key]['ELEMENT']['CAN_BUY'] = \Bitrix\Catalog\ProductTable::calculateAvailable($product);

	$arPrice = CCatalogProduct::GetOptimalPrice($item['ELEMENT']['ID']);
	$arResult['FAVORITES'][$key]['ELEMENT']['PRICE'] = [
		'BASE_PRICE' => ($arPrice['RESULT_PRICE']['BASE_PRICE'] != $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'] ? $arPrice['RESULT_PRICE']['BASE_PRICE'] : false),
		'DISCOUNT_PRICE' => $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'],
		'CURRENCY' => $arPrice['RESULT_PRICE']['CURRENCY'],
	];
}