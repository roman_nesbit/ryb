function favorites_list() {
	jQuery(document).on('click', '.deleteBtn', function (e) {
		e.preventDefault();
		var button = jQuery(this);
		var id = button.data('id');
		var productRow = jQuery('#productRow' + id);
		var deletedRow = jQuery('#deletedRow' + id);
		var link = '?AJAX_CALL_FAVORITES_LIST=Y';

		button.attr('disabled', 'disabled');
		productRow.css('opacity', '.5');

		var postArray = {
			'DELETE_FAVOR': 'Y',
			'ID': productRow.attr('data-favorites-id')
		};
		jQuery.ajax(link, {
			type: "POST",
			data: postArray,
			dataType: "html",
			success: function (data) {
				productRow.hide();
				deletedRow.show();
			},
			complete: function () {
				if (typeof updateFavoritesLine == 'function') {
					updateFavoritesLine();
				}
				button.removeAttr('disabled');
				productRow.css('opacity', '1');				
			}
		});
	}).on('click', '.restoreToFavorites', function (e) {
		e.preventDefault();
		var button = jQuery(this);
		var id = button.data('id');
		var deletedRow = jQuery('#deletedRow' + id);
		var productRow = jQuery('#productRow' + id);
		var link = '?AJAX_CALL_FAVORITES_ADD=Y';

		button.attr('disabled', 'disabled');
		deletedRow.css('opacity', '.5');

		var postArray = {
			'H2O_FAVORITES_ELEMENT_ID': productRow.data('product-id'),
			'h2o_add_favorites': 'Y'
		};
		jQuery.ajax(link, {
			type: "POST",
			data: postArray,
			dataType: "json",
			success: function (data) {
				if (data.ADD > 0) {
					productRow.attr('data-favorites-id', data.ADD);
				}
				deletedRow.hide();
				productRow.show();
				if (typeof updateFavoritesLine == 'function') {
					updateFavoritesLine();
				}
			},
			complete: function () {
				button.removeAttr('disabled');
				deletedRow.css('opacity', '1');
			}
		});

	}).on('click', '.add2cart', function (e) {
		e.preventDefault();
		var button = jQuery(this);
		var id = button.data('id');
		var productRow = jQuery('#productRow' + id);
		var movedRow = jQuery('#movedRow' + id);
		var link = button.data('link');

		button.attr('disabled', 'disabled');
		productRow.css('opacity', '.5');

		var postArray = {
			'product_id': productRow.data('product-id'),
			'action': 'add2cart'
		};

		jQuery.ajax(link, {
			type: "POST",
			data: postArray,
			dataType: "json",
			success: function (data) {
				if (data.result == 'ok') {
					productRow.hide();
					movedRow.show();
					movedRow.attr('data-basket-id', data.data.ID);

					// romeve from favorites
					link = '?AJAX_CALL_FAVORITES_LIST=Y';
					postArray = {
						'DELETE_FAVOR': 'Y',
						'ID': productRow.attr('data-favorites-id')
					};
					jQuery.ajax(link, {
						type: "POST",
						data: postArray,
						dataType: "html",
						success: function () {
							if (typeof updateFavoritesLine == 'function') {
								updateFavoritesLine();
							}
							BX.onCustomEvent('OnBasketChange');
						},
					});
				}
			},
			complete: function () {
				button.removeAttr('disabled');
				productRow.css('opacity', '1');
			}
		});
	}).on('click', '.moveFromCart', function (e) {
		e.preventDefault();
		var button = jQuery(this);
		var id = button.data('id');
		var movedRow = jQuery('#movedRow' + id);
		var productRow = jQuery('#productRow' + id);
		var link = button.data('link');

		button.attr('disabled', 'disabled');
		movedRow.css('opacity', '.5');

		var postArray = {
			'basket_id': movedRow.attr('data-basket-id'),
			'action': 'unadd2cart'
		};

		jQuery.ajax(link, {
			type: "POST",
			data: postArray,
			dataType: "json",
			success: function (data) {
				if (data.result == 'ok') {
					movedRow.hide();
					productRow.show();

					// add to favorites
					link = '?AJAX_CALL_FAVORITES_ADD=Y';
					postArray = {
						'H2O_FAVORITES_ELEMENT_ID': productRow.data('product-id'),
						'h2o_add_favorites': 'Y'
					};
					jQuery.ajax(link, {
						type: "POST",
						data: postArray,
						dataType: "json",
						success: function (data) {
							if (data.ADD > 0) {
								productRow.attr('data-favorites-id', data.ADD);
							}
							if (typeof updateFavoritesLine == 'function') {
								updateFavoritesLine();
							}
							BX.onCustomEvent('OnBasketChange');
						},
					});
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus);
			},
			complete: function () {
				button.removeAttr('disabled');
				movedRow.css('opacity', '1');
			}
		});

	});
   
}

if (window.frameCacheVars !== undefined) {
	BX.addCustomEvent("onFrameDataReceived" , function(json) {
		favorites_list();
	});
} else {
	jQuery(function() {
		favorites_list();
	});
}