<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->createFrame()->begin("");
?><section class="favoritesPage"><?
	if (is_array($arResult['ERRORS']['FATAL']) && !empty($arResult['ERRORS']['FATAL'])) {
		foreach ($arResult['ERRORS']['FATAL'] as $error) {
			ShowError($error);
		}
	} else {
		if (is_array($arResult['ERRORS']['NONFATAL']) && !empty($arResult['ERRORS']['NONFATAL'])) {
			foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
				ShowError($error);
			}
		}
		?><h1 class="pageTitle noBorder"><?= Loc::getMessage('H2O_FAVORITES') ?></h1>
		<div class="innerPage twoCells"><?
			$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"personal",
				array(
					"ROOT_MENU_TYPE" => "personal",
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_CACHE_GET_VARS" => ""
				)
			);
			?><div class="innerWrap">
				<div class="innerContent">
					<div class="infoTable">
						<div class="infoRow headRow">
							<div class="imgCell"><?= Loc::getMessage('H2O_FAVORITES_PRODUCT') ?></div>
							<div class="infoCell">
								<div class="cell mainIfo"></div>
								<div class="cell statusCell"><?= Loc::getMessage('H2O_FAVORITES_AVAILABILITY') ?></div>
								<div class="cell priceCell"><?= Loc::getMessage('H2O_FAVORITES_PRICE') ?></div>
								<div class="cell buttonsCell"></div>
							</div>
						</div>
						<? foreach ($arResult['FAVORITES'] as $item) : ?>
							<div class="infoRow removedInfoRow"
								id="deletedRow<?=$item['ID']?>"
								style="display:none">
								<div class="removedInfo">
									<?= Loc::getMessage('H2O_DELETED_NOTE', ['#PRODUCT#' => $item['ELEMENT']['NAME']]) ?>
								</div>
								<div class="recoverBox">
									<a class="recoverBtn restoreToFavorites"
										data-entity="basket-item-restore-button"
										href="javascript:void(0)"
										data-id="<?=$item['ID']?>">
										<?= Loc::getMessage('H2O_DELETED_RECOVER') ?>
									</a>
								</div>
							</div>
							<div class="infoRow removedInfoRow"
								id="movedRow<?=$item['ID']?>"
								data-basket-id=""
								style="display:none">
								<div class="removedInfo">
									<?= Loc::getMessage('H2O_CART_NOTE', ['#PRODUCT#' => $item['ELEMENT']['NAME']]) ?>
								</div>
								<div class="recoverBox">
									<a class="recoverBtn moveFromCart"
										data-entity="basket-item-restore-button"
										href="javascript:void(0)"
										data-id="<?=$item['ID']?>"
										data-link="<?= SITE_TEMPLATE_PATH ?>/ajax/add_to_cart.php">
										<?= Loc::getMessage('H2O_CART_RECOVER') ?>
									</a>
								</div>
							</div>
							<div class="infoRow"
								id="productRow<?=$item['ID']?>"
								data-favorites-id="<?=$item['ID']?>"
								data-product-id="<?=$item['ELEMENT']['ID']?>">
								<div class="imgCell">
									<div class="imgBox"><a href="<?= $item['ELEMENT']['DETAIL_PAGE_URL'] ?>"><img src="<?= CFile::GetPath($item['ELEMENT']['PREVIEW_PICTURE'] ?? $item['ELEMENT']['DETAIL_PICTURE']) ?>" alt="<?= $item['ELEMENT']['NAME'] ?>"></a></div>
								</div>
								<div class="infoCell">
									<div class="cell mainIfo">
										<h3 class="title"><a href="<?= $item['ELEMENT']['DETAIL_PAGE_URL'] ?>"><?= $item['ELEMENT']['NAME'] ?></a></h3>
										<div class="date"><?= $item['DATE_INSERT'] ?></div>

										<? if ($item['ELEMENT']['CAN_BUY'] == 'Y') : ?>
											<button class="btn add2cart"
												data-id="<?=$item['ID']?>" 
												data-link="<?= SITE_TEMPLATE_PATH ?>/ajax/add_to_cart.php"><?= Loc::getMessage('H2O_FAVORITES_BUY') ?></button>
										<? endif; ?>
									</div>
									<div class="cell statusCell">
										<div class="orderInfoBox">
											<? if ($item['ELEMENT']['CAN_BUY'] == 'Y') : ?>
												<div class="orderStatusLabel green">
													<?= Loc::getMessage('H2O_FAVORITES_AVAILABLE') ?>
												</div>
											<? else : ?>
												<div class="orderStatusLabel">
													<?= Loc::getMessage('H2O_FAVORITES_NOT_AVAILABLE') ?>
												</div>
											<? endif; ?>
										</div>
									</div>
									<div class="cell priceCell"><?
										if ($item['ELEMENT']['PRICE']['BASE_PRICE']) :
										?><div class="oldPrice"><?
											echo CurrencyFormat($item['ELEMENT']['PRICE']['BASE_PRICE'], $item['ELEMENT']['PRICE']['CURRENCY']);
										?></div><?
										endif;
										?><div class="price"><?= CurrencyFormat($item['ELEMENT']['PRICE']['DISCOUNT_PRICE'], $item['ELEMENT']['PRICE']['CURRENCY']) ?></div>
										<div class="orderInfoBox">
											<? if ($item['ELEMENT']['CAN_BUY'] == 'Y') : ?>
												<div class="orderStatusLabel green">
													<?= Loc::getMessage('H2O_FAVORITES_AVAILABLE') ?>
												</div>
											<? else : ?>
												<div class="orderStatusLabel">
													<?= Loc::getMessage('H2O_FAVORITES_NOT_AVAILABLE') ?>
												</div>
											<? endif; ?>
										</div>
									</div>
									<div class="cell buttonsCell">
										<? if ($item['ELEMENT']['CAN_BUY'] == 'Y') : ?>
											<button class="btn add2cart"
												data-id="<?=$item['ID']?>"
												data-link="<?= SITE_TEMPLATE_PATH ?>/ajax/add_to_cart.php"><?= Loc::getMessage('H2O_FAVORITES_BUY') ?></button>
											<button class="baseBtn add2cart"
												data-id="<?=$item['ID']?>"
												data-link="<?= SITE_TEMPLATE_PATH ?>/ajax/add_to_cart.php"><?= Loc::getMessage('H2O_FAVORITES_BUY') ?></button>
										<? endif; ?>
										<button class="deleteBtn"
											data-id="<?= $item['ID'] ?>">
											<svg><use href="#binIcon" /></svg>
										</button>
									</div>
								</div>
							</div>
						<? endforeach; ?>
					</div>
				</div>
			</div>
		</div><?
	}
?></section>