<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Rybalka\Marketplace\Service\Checkout\DeliveryServices;
use Rybalka\Marketplace\Service\StoreFrontView\CatalogItem\FreeDeliveryView;

$freeDeliveryMethods = (new DeliveryServices())->getFree();
 ?>
<p>Доставка по Украине соответствует тарифам “Нова Пошта”.</p>
<?php
    foreach ($freeDeliveryMethods as $deliveryMethod) {
        $rendered = (new FreeDeliveryView($deliveryMethod))->render();

        if (!empty($rendered)) {
            echo '<p class="green">' . $rendered . '</p>';
        }
    }
?>