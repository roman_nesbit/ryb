<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$SNIPPETS = [];
$SNIPPETS['h1_pagetitle.snp'] = ['title' => 'Заголовок H1.pageTitle'];
$SNIPPETS['h1.snp'] = ['title' => 'Заголовок H1'];
$SNIPPETS['h2.snp'] = ['title' => 'Заголовок H2'];
$SNIPPETS['h3.snp'] = ['title' => 'Заголовок H3'];
$SNIPPETS['h4.snp'] = ['title' => 'Заголовок H4'];
$SNIPPETS['h5.snp'] = ['title' => 'Заголовок H5'];
$SNIPPETS['h6.snp'] = ['title' => 'Заголовок H6'];
$SNIPPETS['p.snp'] = ['title' => 'Параграф'];
$SNIPPETS['p_right.snp'] = ['title' => 'Параграф по правому краю'];
$SNIPPETS['p_center.snp'] = ['title' => 'Параграф по центру'];
$SNIPPETS['p_justify.snp'] = ['title' => 'Параграф по ширине'];
$SNIPPETS['list_ol.snp'] = ['title' => 'Нумерованный список'];
$SNIPPETS['list_ul.snp'] = ['title' => 'Ненумерованный список'];
$SNIPPETS['table.snp'] = ['title' => 'Таблица'];
$SNIPPETS['blockquote.snp'] = ['title' => 'Цитата'];
?>