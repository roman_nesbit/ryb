<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(['fx']);
use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="<?=htmlspecialcharsbx(SITE_DIR)?>favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/safari-pinned-tab.svg" color="#001e41">
    <meta name="msapplication-TileColor" content="#001e41">
    <meta name="msapplication-config" content="<?=htmlspecialcharsbx(SITE_DIR)?>images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#001e41">
	<?php
	    $APPLICATION->ShowHead();
	    $APPLICATION->SetAdditionalCSS("/local/templates/rybalka/template_styles.css");
        Asset::getInstance()->addJs('/local/templates/rybalka/js/rmp.min.js');
    ?>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159675076-1"></script>
	<script async src="https://www.google.com/recaptcha/api.js?render=<?=getenv('RECAPTCHA_SITE_KEY')?>"></script>
</head>

<?
	if(http_response_code() == "404") $thePageCode = "404";
	else $thePageCode = ($_REQUEST["PAGECODE"] ? $_REQUEST["PAGECODE"] : $APPLICATION->GetDirProperty("PAGECODE"));

?>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div><?
// $APPLICATION->IncludeComponent("h2o:favorites.add", "", []);
?>