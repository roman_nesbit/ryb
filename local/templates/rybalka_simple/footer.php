<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<? $APPLICATION->IncludeFile("/local/templates/rybalka/include/icons.php", ["SHOW_BORDER"=>false]); ?>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-159675076-1');
        gtag('config', 'AW-1024981995');
        window.rmp = window.rmp || [];
        rmp.push('event.trigger', 'page.viewed');
    </script>
</body>
</html>