<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">
    <meta name="color-scheme" content="light">
    <meta name="supported-color-schemes" content="light">
    <title></title>

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

    <!--[if mso]>
    <style>
        * {
            font-family: Roboto, sans-serif !important;
        }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet" />
    <!--<![endif]-->

    <!-- CSS Reset : BEGIN -->
    <style>
        :root {
            color-scheme: light;
            supported-color-schemes: light;
        }

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        #MessageViewBody, #MessageWebViewDiv{
            width: 100% !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        img {
            -ms-interpolation-mode:bicubic;
        }

        a {
            text-decoration: none;
            color: #5fb34e;
        }

        a[x-apple-data-detectors],  /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        .im {
            color: inherit !important;
        }

        img.g-img + div {
            display: none !important;
        }

        body,
        p,
        a,
        td {
            font-family: "Roboto", sans-serif !important;
            font-size: 14px;
            line-height: 20px;
        }

        p {
            margin-bottom: 20px;
        }

        h1, h2, h3, h4 {
            color: #001e41;
            font-family: "Roboto", sans-serif !important;
        }

        h1 {
            font-size: 25px;
            font-weight: normal;
        }

        h2 {
            font-size: 18px;
            font-weight: bold;
        }

        h3 {
            font-size: 18px;
            font-weight: normal;
        }

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            u ~ div .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            u ~ div .email-container {
                min-width: 375px !important;
            }
        }

        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            u ~ div .email-container {
                min-width: 414px !important;
            }
        }

    </style>
    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container p, .email-container a {
                font-size: 16px !important;
            }

        }

    </style>
    <!-- Progressive Enhancements : END -->
</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #ffffff;">
<center role="article" aria-roledescription="email" lang="ru" style="width: 100%; background-color: #ffffff;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #222222;">
    <tr>
    <td>
    <![endif]-->

    <div style="max-width: 600px; margin: 0 auto;" class="email-container">

    <!--[if mso]>
    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600">
    <tr>
    <td>
    <![endif]-->

    <!-- Email Body : BEGIN -->
    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
        <!-- Email Header : BEGIN -->
        <tr>
            <td style="padding:20px;background-color: #001e41;">
                <a href="https://rybalka.ua/">
                    <img src="https://rybalka.ua/local/templates/rybalka/images/logo.png" class="g-img" width="175" height="32" border="0" alt="" />
                </a>
            </td>
        </tr>
        <!-- Email Header : END -->

        <!-- Clear Spacer : BEGIN -->
        <tr>
            <td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px;">
                &nbsp;
            </td>
        </tr>
        <!-- Clear Spacer : END -->

        <!-- Email body : BEGIN -->
        <tr>
            <td style="background-color: #ffffff; padding-left: 20px;padding-right:20px;">