<?php


namespace Rybalka\Marketplace\Service\StoreFrontView\CatalogItem;


use Rybalka\Marketplace\Model\Checkout\DeliveryService;
use Rybalka\Marketplace\Model\Rest\View\MoneyView;
use Rybalka\Marketplace\Util\StringsUtils;

class FreeDeliveryView
{
    /**
     * @var MoneyView
     */
    private $moneyView;

    /**
     * @var DeliveryService
     */
    private $data;

    /**
     * @var StringsUtils
     */
    private $stringsUtils;

    public function __construct(DeliveryService $data)
    {
        $this->data = $data;
        $this->moneyView = new MoneyView();
        $this->stringsUtils = new StringsUtils();
    }

    public function render(string $template = ''): string
    {
        $result = '';

        if (!$this->data->getRestrictions()->orderAmount()->hasMin()) {
            return $result;
        }

        $min = $this->data->getRestrictions()->orderAmount()->getMin();

        return sprintf(
            'Бесплатная доставка <b>%s</b> для заказов от <b>%s</b>',
            $this->stringsUtils->lcFirst($this->data->getName()),
            $this->moneyView->map($min)
        );
    }
}