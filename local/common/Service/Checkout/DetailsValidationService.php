<?php


namespace Rybalka\Marketplace\Service\Checkout;


use Respect\Validation\Exceptions\ValidationException;
use Respect\Validation\Validator as V;
use Rybalka\Marketplace\Exception\ValidationException as HttpValidationException;
use Rybalka\Marketplace\Http\Exception\ConflictException;

class DetailsValidationService
{
    private const UUID_REGEX = '/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/';
    private $validationRules = [];
    private $completenessRules = [];

    public function __construct()
    {
        $this->validationRules = [

            V::key('name', V::stringType()->length(2), false)
                ->setTemplate('Имя должно быть строкой длиной не меньше двух символов'),
            V::key('surname', V::stringType()->length(2), false)
                ->setTemplate('Фамилия должна быть строкой длиной не меньше двух символов'),

            V::keyNested(
                'phone.verificationToken',
                V::regex(self::UUID_REGEX),
                false
            )
            ->setTemplate('Некоректное значение {{name}}')
            ->setName('phone.verificationToken'),

            V::keyNested(
                'email.value',
                V::email(),
                false)
            ->setTemplate('Некорректный email адрес'),

            V::key(
        'subscribeForNewsletter',
                V::boolType(),
        false
            )->setTemplate('Некорректное значение'),

            V::keyNested(
                'delivery.city.id',
                V::regex(self::UUID_REGEX),
                false
            )
            ->setTemplate('Некорректный идентификатор населенного пункта')
            ->setName('delivery.city.id'),

            V::keyNested(
                'delivery.npOffice.id',
                V::regex(self::UUID_REGEX),
                false)
            ->setTemplate('Некорректный идентификатор почтового отделения')
            ->setName('delivery.npOffice.id'),

             V::keyNested(
                'payment.id',
                V::intType()->min(0),
                false)
            ->setName('payment.id')
            ->setTemplate('Некорректный идентификатор платежного метода')
        ];

        $this->completenessRules = [
            V::key('name', V::notEmpty(), true),
            V::key('surname', V::notEmpty(), true),
            V::keyNested('phone.number', V::notEmpty(), true),
            V::keyNested('delivery.method.code', V::notEmpty(), true),
            V::keyNested('payment.id', V::notEmpty(), true)
        ];
    }
    
    public function assertDetailsComplete(array $checkoutDetails)
    {
        try {
            V::allOf($this->completenessRules)->check($checkoutDetails);
        } catch (ValidationException $validationException) {
            $message = sprintf(
                'Checkout data is incomplete. The "%s" field is not set.',
                $validationException->getName()
            );
            throw (new ConflictException($message));
        }
    }

    public function validate(array $data)
    {
        try {
            V::allOf($this->validationRules)->check($data);
        } catch (ValidationException $exception) {
            throw new HttpValidationException($exception->getName(), $exception->getMessage());
        }
    }
}