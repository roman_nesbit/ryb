<?php


namespace Rybalka\Marketplace\Service\Checkout;


use Bitrix\Main\Context;
use Bitrix\Sale\Basket;
use Bitrix\Sale\BasketBase;
use Bitrix\Sale\Compatible\DiscountCompatibility;
use Bitrix\Sale\Delivery\Restrictions\Manager as RestrictionsManager;
use Bitrix\Sale\Delivery\Services\Manager as DeliveryServicesManager;
use Bitrix\Sale\Discount;
use Bitrix\Sale\Discount\Context\Fuser;
use Bitrix\Sale\Order;
use Bitrix\Sale\PaySystem\Manager as PaymentServicesManager;
use InvalidArgumentException;
use Rybalka\Marketplace\Exception\InvalidStateException;
use Rybalka\Marketplace\Service\Checkout\Exception\EmptyShoppingCart;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Address\StreetsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;

class VirtualOrderBuilder
{
    const CUSTOMER_TYPE_PRIVATE_ENTITY = 1;
    const CUSTOMER_TYPE_LEGAL_ENTITY = 2;

    private $order;

    private $propertyCodeToIdMap = [];

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    /**
     * @var DeliveryServices
     */
    private $deliveryServices;

    /**
     * @var StreetsService
     */
    private $streetsService;

    private function __construct(int $saleUserId, ?int $userId)
    {
        $siteId = Context::getCurrent()->getSite();
        $this->order = Order::create($siteId, $userId);
        $this->order->setPersonTypeId(self::CUSTOMER_TYPE_PRIVATE_ENTITY);

        $this->order->setBasket(
            $this->createBasket($saleUserId, $siteId)->getOrderableItems()
        );

        $this->settlementsService = new SettlementsService();
        $this->postOfficesService = new PostOfficesService();
        $this->streetsService = new StreetsService();
        $this->deliveryServices = new DeliveryServices();
    }

    public static function get(int $saleUserId, ?int $userId)
    {
        return (new VirtualOrderBuilder($saleUserId, $userId));
    }

    public function withPaymentService(int $serviceId)
    {
        $payment = $this->order->getPaymentCollection()->createItem();
        $payment->setField('SUM', $this->order->getPrice());

        $service = PaymentServicesManager::getObjectById($serviceId);

        if (is_null($service)) {
            throw new InvalidArgumentException("Payment service with $serviceId does not exist");
        }

        $payment->setFields([
            'PAY_SYSTEM_ID'   => $service->getField('PAY_SYSTEM_ID'),
            'PAY_SYSTEM_NAME' => $service->getField('NAME'),
        ]);

        return $this;
    }

    public function withDeliveryMethod(string $code)
    {
        $deliveryService = $this->deliveryServices->getBy($code);
        $shipment = $this->createShipment();

        if (empty($deliveryService)) {
            throw new InvalidStateException('No shipping methods available for an order');
        }

        $deliveryPriceCalculation = DeliveryServicesManager::calculateDeliveryPrice(
            $shipment,
            $deliveryService->getId()
        );

        $shipment->setFields([
            'DELIVERY_ID'   => $deliveryService->getId(),
            'DELIVERY_NAME' => $deliveryService->getName(),
        ]);

        $shipment->setBasePriceDelivery($deliveryPriceCalculation->getDeliveryPrice() ?? 0);

        return $this;
    }

    public function withNote(?string $comment)
    {
        if (is_null($comment)) {
            return $this;
        }

        $this->order->setField('USER_DESCRIPTION', $comment);

        return $this;
    }

    public function withCustomerName(string $name)
    {
        $this->setOrderProperty('NAME', $name);

        $surname = $this->getOrderProperty('LAST_NAME');
        $this->setOrderProperty('FULL_NAME', $name . ' ' . $surname);

        return $this;
    }

    public function withCustomerSurname(string $surname)
    {
        $this->setOrderProperty('LAST_NAME', $surname);

        $name = $this->getOrderProperty('NAME');
        $this->setOrderProperty('FULL_NAME', $name . ' ' . $surname);

        return $this;
    }

    public function withCustomerPhoneNumber(string $phoneNumber)
    {
        $this->setOrderProperty('PHONE', $phoneNumber);
        return $this;
    }

    public function withCustomerEmail(string $email)
    {
        $this->setOrderProperty('EMAIL', $email);
        return $this;
    }

    public function withDeliveryDetails(array $details)
    {
        if (isset($details['postOffice']['id'])){
            $postOffice = $this->postOfficesService->get($details['postOffice']['id']);

            if (!$postOffice) {
                return $this;
            }

            $this->setOrderProperty('DELIVERY_POST_OFFICE_ID', $postOffice->getId());
            $this->setOrderProperty('DELIVERY_POST_OFFICE_NAME', (string)$postOffice);
            $this->setOrderProperty('DELIVERY_SETTLEMENT_ID', $postOffice->getSettlementId());

            $settlement = $this->settlementsService->get($postOffice->getSettlementId());

            if (!is_null($settlement)) {
                $this->setOrderProperty('DELIVERY_SETTLEMENT_NAME', (string)$settlement);
            }

            return $this;
        }

        if (isset($details['street']['id']) && isset($details['settlement']['id'])) {
            $settlement = $this->settlementsService->get($details['settlement']['id']);
            $street = $this->streetsService->get($details['settlement']['id'], $details['street']['id']);

            if (!$settlement || !$street) {
                return $this;
            }

            $this->setOrderProperty('DELIVERY_SETTLEMENT_ID', $settlement->getDeliverySettlementId());
            $this->setOrderProperty('DELIVERY_SETTLEMENT_NAME', (string)$settlement);

            $this->setOrderProperty('DELIVERY_STREET_ID', $street->getId());

            $addressComponents = [];

            if (!empty($details['apartmentNumber'])) {
                $addressComponents[] = 'кв. ' . $details['apartmentNumber'];
            }

            $addressComponents[] = (string)$street . ' ' . ($details['buildingNumber'] ?? '');
            $addressComponents[] = (string)$settlement;

            $this->setOrderProperty('ADDRESS', implode(', ', $addressComponents));

            if (!empty($details['note'])) {
                $this->setOrderProperty('DELIVERY_NOTE', $details['note']);
            }
        }

        return $this;
    }

    public function build()
    {
        return $this->order;
    }

    private function getPropertyCodeToIdMap()
    {
        if (empty($this->propertyCodeToIdMap)) {
            foreach ($this->order->getPropertyCollection() as $property) {
                $this->propertyCodeToIdMap[$property->getField('CODE')] = $property->getField('ORDER_PROPS_ID');
            }
        }

        return $this->propertyCodeToIdMap;
    }

    private function getOrderProperty(string $propertyId)
    {
        $map = $this->getPropertyCodeToIdMap();
        if (!isset($map[$propertyId])) {
            return null;
        }

        return $this->order
            ->getPropertyCollection()
            ->getItemByOrderPropertyId($map[$propertyId])
            ->getValue();
    }

    private function setOrderProperty(string $propertyId, string $propertyValue)
    {
        $map = $this->getPropertyCodeToIdMap();
        if (!isset($map[$propertyId])) {
            throw new InvalidArgumentException("Order property with the name \"$propertyId\" does not exist");
        }

        $this->order
            ->getPropertyCollection()
            ->getItemByOrderPropertyId($map[$propertyId])
            ->setValue($propertyValue);
    }

    private function createBasket(int $saleUserId, string $siteId)
    {
        $basket = Basket::loadItemsForFUser($saleUserId, $siteId);

        if ($basket->isEmpty()) {
            throw new EmptyShoppingCart();
        }

        $this->applyDiscountsToBasket($basket);

        return $basket;
    }

    private function createShipment()
    {
        $shipmentCollection = $this->order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipment->setField('CURRENCY', $this->order->getCurrency());
        $shipment->allowDelivery();

        $shipmentItems = $shipment->getShipmentItemCollection();

        foreach ($this->order->getBasket() as $basketItem) {

            if (!$basketItem->canBuy()) {
                continue;
            }

            $item = $shipmentItems->createItem($basketItem);
            $item->setQuantity($basketItem->getQuantity());
        }

        return $shipment;
    }

    private function applyDiscountsToBasket(BasketBase $basket)
    {
        if ($basket->isEmpty()) {
            return;
        }

        DiscountCompatibility::stopUsageCompatible();

        $userId = $basket->getFUserId();

        $saleContext = new Fuser($userId);

        $discounts = Discount::buildFromBasket($basket, $saleContext);
        $basket->refreshData(['PRICE', 'COUPONS']);

        $discounts->calculate();
        $result = $discounts->getApplyResult(true);
        $prices = $result['PRICES']['BASKET'];

        $basket->applyDiscount($prices);
        $basket->save();
    }
}