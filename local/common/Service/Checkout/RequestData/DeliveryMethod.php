<?php

namespace Rybalka\Marketplace\Service\Checkout\RequestData;

use Rybalka\Marketplace\Exception\ValidationException;
use Rybalka\Marketplace\Partners\ImportXml\SanitizationUtil;
use Rybalka\Marketplace\Service\Checkout\DeliveryServices;
use Rybalka\Marketplace\Service\Checkout\VirtualOrderBuilder;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Address\StreetsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;
use Rybalka\Marketplace\Util\ArrayUtil;

class DeliveryMethod
{
    /**
     * @var DeliveryServices
     */
    private $deliveryServices;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    /**
     * @var StreetsService
     */
    private $streetsService;

    /**
     * @var ArrayUtil
     */
    private $arrayUtil;

    /**
     * @var SanitizationUtil
     */
    private $sanitizationUtil;

    public function __construct()
    {
        $this->deliveryServices = new DeliveryServices();
        $this->settlementsService = new SettlementsService();
        $this->postOfficesService = new PostOfficesService();
        $this->streetsService = new StreetsService();
        $this->arrayUtil = new ArrayUtil();
        $this->sanitizationUtil = new SanitizationUtil();
    }

    public function assertAcceptableMethod(string $methodCode, int $userId, int $saleUserId)
    {
        if (empty($methodCode)) {
            throw new ValidationException('delivery.method.code', 'Не указан код метода доставки');
        }

        $order = VirtualOrderBuilder::get($saleUserId, $userId)->build();
        $mainShipment = $order->getShipmentCollection()[0] ?? null;

        $deliveryService = $this->deliveryServices->getIfAvailable($methodCode, $mainShipment);

        if (is_null($deliveryService)) {
            throw new ValidationException('delivery.method.code', 'Служба доставки не доступна для этого заказа');
        }
    }

    public function assertValidDataFor(string $methodCode, array $methodData)
    {
        $deliveryMethod = $this->deliveryServices->getBy($methodCode);

        if (is_null($deliveryMethod)) {
            throw new InvalidArgumentException('No delivery method exists with such code: ' . $deliveryCode);
        }

        foreach ($deliveryMethod->getRequires() as $requiredFieldName) {
            $value = $this->arrayUtil->get($methodData, $requiredFieldName) ?? '';

            if (!is_string($value)) {
                throw new ValidationException(
                    'delivery.data.' . $requiredFieldName,
                    'Неправильный формат значения'
                );
            }

            if (empty($value)) {
                throw new ValidationException(
                    'delivery.data.' . $requiredFieldName,
                    'Это поле обязательно к заполнению'
                );
            }

            switch ($requiredFieldName) {
                case 'settlement.id':
                    $settlement = $this->settlementsService->get($value);

                    if (empty($settlement)) {
                        throw new ValidationException(
                            'delivery.data.' . $requiredFieldName,
                            'Неверный идентификатор населенного пункта'
                        );
                    }

                    break;
                case 'street.id':
                    $settlementId = $this->arrayUtil->get($methodData, 'settlement.id') ?? '';
                    $street = $this->streetsService->get($settlementId, $value);

                    if (empty($street)) {
                        throw new ValidationException(
                            'delivery.data.' . $requiredFieldName,
                            'Неверный идентификатор улицы'
                        );
                    }

                    break;
                case 'postOffice.id':
                    $postOffice = $this->postOfficesService->get($value);

                    if (empty($postOffice)) {
                        throw new ValidationException(
                            'delivery.data.' . $requiredFieldName,
                            'Неверный идентификатор почтового отделения'
                        );
                    }

                    break;
                default:
                    continue;
            }
        }
    }

    public function extractDetailsFor(string $methodCode, array $methodData)
    {
        $deliveryMethod = $this->deliveryServices->getBy($methodCode);
        $result = [];

        if (is_null($deliveryMethod)) {
            throw new InvalidArgumentException('No delivery method exists with such code: ' . $deliveryCode);
        }

        $acceptedFields = array_merge(
            $deliveryMethod->getRequires(),
            $deliveryMethod->getAccepts()
        );

        foreach ($acceptedFields as $fieldName) {
            $value = $this->arrayUtil->get($methodData, $fieldName);

            if (is_null($value)) {
                continue;
            }

            $value = $this->sanitizationUtil->parameterValue($value);

            $this->arrayUtil->set($result, $fieldName, $value);
        }

        return $result;
    }
}