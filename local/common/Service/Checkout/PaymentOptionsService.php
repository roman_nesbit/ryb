<?php


namespace Rybalka\Marketplace\Service\Checkout;


use Bitrix\Sale\PaySystem\Manager;

class PaymentOptionsService
{
    public function getById(?int $paymentOptionId)
    {
        if (is_null($paymentOptionId)) {
            return null;
        }

        $paymentOptions = Manager::getList([
            'filter' => [
                'ACTIVE' => 'Y',
                'ID' => $paymentOptionId
            ],
            'order' => [
                'SORT' => 'ASC',
            ]
        ]);

        if ($paymentOption = $paymentOptions->fetch()) {
            return [
                'id' => $paymentOption['ID'],
                'title' => $paymentOption['NAME'],
                'comment' => empty($paymentOption['DESCRIPTION']) ? null : $paymentOption['DESCRIPTION']
            ];
        }

        return null;
    }
}