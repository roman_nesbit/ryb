<?php


namespace Rybalka\Marketplace\Service\Checkout\Exception;


use Rybalka\Marketplace\Http\Exception\BadRequestException;

class EmptyShoppingCart extends BadRequestException
{
    public function __construct()
    {
        parent::__construct('An empty shopping cart can not be checked out');
    }
}