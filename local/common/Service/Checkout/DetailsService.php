<?php

namespace Rybalka\Marketplace\Service\Checkout;


use Rybalka\Marketplace\Exception\ValidationException;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Service\AuthService;
use Rybalka\Marketplace\Service\Checkout\RequestData\DeliveryMethod;
use Rybalka\Marketplace\Service\Dictionary\Address\SettlementsService;
use Rybalka\Marketplace\Service\Dictionary\Post\PostOfficesService;
use Rybalka\Marketplace\Service\ProfileService;
use Rybalka\Marketplace\Util\Phone\VerificationTable;
use Rybalka\Marketplace\Util\SessionParametersBag;
use Rybalka\Marketplace\Util\UserUtil;

class DetailsService
{
    private const SESSION_BAG_NAME = 'checkoutData';
    private const CHECKOUT_DATA_EXPIRATION_IN_SECONDS = 900;

    /**
     * @var ProfileService
     */
    private $profileService;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var PostOfficesService
     */
    private $postOfficesService;

    /**
     * @var PaymentOptionsService
     */
    private $paymentOptionsService;

    /**
     * @var AuthService
     */
    private $authService;

    private $accountDetailsGenerationService;

    private $checkoutDataBag;

    /**
     * @var DeliveryServices
     */
    private $deliveryServices;

    /**
     * @var DeliveryMethod
     */
    private $deliveryMethodDetailsService;

    public function __construct()
    {
        $this->profileService = new ProfileService();
        $this->settlementsService = new SettlementsService();
        $this->postOfficesService = new PostOfficesService();
        $this->paymentOptionsService = new PaymentOptionsService();
        $this->authService = new AuthService();
        $this->accountDetailsGenerationService = new AccountDetailsGenerationService();
        $this->deliveryServices = new DeliveryServices();
        $this->deliveryMethodDetailsService = new DeliveryMethod();
        $this->checkoutDataBag = new SessionParametersBag(
            self::SESSION_BAG_NAME,
            self::CHECKOUT_DATA_EXPIRATION_IN_SECONDS
        );
    }

    public function getUserId(): ?int
    {
        return $this->profileService->getId();
    }

    public function getName(): ?string
    {
        return $this->getFieldValue('name', function() {
            return $this->profileService->getName();
        });
    }

    public function setName(string $name)
    {
        if (is_null($name)) {
            return;
        }

        $name = trim($name);

        if (empty($name)) {
            throw new ValidationException('name', 'Поле не может быть пустым');
        }

        $this->setFieldValue('name', $name);
    }

    public function getSurname(): ?string
    {
        return $this->getFieldValue('surname', function() {
            return $this->profileService->getSurname();
        });
    }

    public function setSurname(string $surname)
    {
        $this->setFieldValue('surname', $surname);
    }

    public function getPhoneNumber(): ?string
    {
        return $this->getFieldValue('phoneNumber', function() {
            return $this->profileService->getPhoneNumber();
        });
    }

    public function getEmail(): ?string
    {
        $email = $this->profileService->getEmail();

        if (!is_null($email) && !$this->accountDetailsGenerationService->isGeneratedEmail($email)) {
            return $email;
        }

        return $this->getFieldValue('email');
    }

    public function isEmailEditable(): bool
    {
        $emailFromProfile = $this->profileService->getEmail();

        return is_null($emailFromProfile) ||
               $this->accountDetailsGenerationService->isGeneratedEmail($emailFromProfile);
    }

    public function setEmail(string $email)
    {
        if (!$this->isEmailEditable()) {
            throw new ValidationException(
                'email.value',
                'Модификация email запрещена'
            );
        }

        if ($email == $this->getEmail()) {
            return;
        }

        $userDataByEmail = UserUtil::getUserDataByEmail($email);

        if ($userDataByEmail !== false && $userDataByEmail['ID'] != $this->profileService->getId()) {
            throw (new ValidationException(
                'email.value',
                'Пользователь с таким email уже зарегистрирован в системе')
            );
        }

        $this->setFieldValue('email', $email);
    }

    public function getVerificationToken(): ?string
    {
        return $this->getFieldValue('phoneNumberVerificationToken');
    }

    public function setVerificationToken(string $verificationToken)
    {
        $phoneNumber = VerificationTable::getVerifiedPhoneNumber($verificationToken);

        if (!$phoneNumber) {
            throw new BadRequestException('Invalid value of the phone number verification token');
        }

        $userData = UserUtil::getUserDataByPhoneNumber($phoneNumber);

        if (!empty($userData)) {
            $this->authService->loginWithHashedPassword($userData['LOGIN'], $userData['PASSWORD']);
        } else {
            $this->authService->logout();
        }

        $this->setFieldValue('phoneNumber', $phoneNumber);
        $this->setFieldValue('phoneNumberVerificationToken', $verificationToken);
    }

    public function isMailListSubscriber(): ?bool
    {
        return $this->getFieldValue('isMailListSubscriber', function() {
            return $this->profileService->isMaillistSubscriber();
        });
    }

    public function setIsMailListSubscriber(bool $flag)
    {
        $this->setFieldValue('isMailListSubscriber', $flag);
    }

    public function setDelivery($data, string $saleUserId)
    {
        $deliveryCode = $data['method']['code'] ?? null;
        $userId = $this->getUserId() ?? 0;

        $this->deliveryMethodDetailsService->assertAcceptableMethod($deliveryCode, $userId, $saleUserId);
        $this->deliveryMethodDetailsService->assertValidDataFor($deliveryCode, $data['data'] ?? []);

        $this->setFieldValue('deliveryMethodCode', $deliveryCode);

        $deliveryDetails = $this->deliveryMethodDetailsService->extractDetailsFor($deliveryCode, $data['data']);
        $this->setFieldValue('deliveryDetails', $deliveryDetails);
    }

    public function getDeliveryMethodCode(): ?string
    {
        return $this->getFieldValue('deliveryMethodCode');
    }

    public function getDeliveryDetails(): ?array
    {
        return $this->getFieldValue('deliveryDetails');
    }

    public function getPaymentMethodId(): ?int
    {
        return $this->getFieldValue('paymentMethodId');
    }

    public function setPaymentMethodId(int $paymentMethodId)
    {
        $data = $this->paymentOptionsService->getById($paymentMethodId);

        if (is_null($data)) {
            throw new ValidationException('payment.id', 'Неверный идентификатор платежного метода');
        }

        $this->setFieldValue('paymentMethodId', $paymentMethodId);
    }

    public function getNote(): ?string
    {
        return $this->getFieldValue('note');
    }

    public function setNote(string $note)
    {
        $this->setFieldValue('note', $note);
    }

    public function saveUserFields($fields)
    {
        $this->profileService->setProfileFields($fields);

        return $this;
    }

    public function reset()
    {
        $this->checkoutDataBag->reset();
    }

    private function getFieldValue(string $fieldName, callable $fallback = null)
    {
        return $this->checkoutDataBag->getOrCallback($fieldName, $fallback);
    }

    private function setFieldValue($fieldName, $value)
    {
        $this->checkoutDataBag->set($fieldName, $value);
    }
}