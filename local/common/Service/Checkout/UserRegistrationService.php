<?php


namespace Rybalka\Marketplace\Service\Checkout;


use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use CUser;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;

class UserRegistrationService
{
    /**
     * @var AccountDetailsGenerationService
     */
    private $detailsGenerationService;

    public function __construct()
    {
        $this->detailsGenerationService = new AccountDetailsGenerationService();
    }

    public function register(DetailsService $detailsService)
    {
        $loginName = $this->detailsGenerationService->generateLogin();
        $password = $this->detailsGenerationService->generatePassword();

        $email = $detailsService->getEmail() ?? $this->detailsGenerationService->generateEmail($loginName);

        $groupIds = [];
        $defaultGroups = Option::get('main', 'new_user_registration_def_group');

        if (!empty($defaultGroups)) {
            $groupIds = explode(',', $defaultGroups);
        }

        $fields = [
            'LOGIN' => $loginName,
            'NAME' => $detailsService->getName(),
            'LAST_NAME' => $detailsService->getSurname(),
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $password,
            'EMAIL' => $email,
            'GROUP_ID' => $groupIds,
            'ACTIVE' => 'Y',
            'LID' => Context::getCurrent()->getSite(),
            'PERSONAL_PHONE' => $detailsService->getPhoneNumber()
        ];

        $user = new CUser;
        $userId = $user->Add($fields);

        if ($userId <= 0) {
            throw new InternalServerErrorException('Failed to register a user. User data: ' . json_encode($fields));
        }

        return $userId;
    }
}