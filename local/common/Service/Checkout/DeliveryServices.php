<?php


namespace Rybalka\Marketplace\Service\Checkout;


use Bitrix\Sale\Delivery\ExtraServices\Manager as ExtraServicesManager;
use Bitrix\Sale\Delivery\Restrictions\Manager as RestrictionsManager;
use Bitrix\Sale\Delivery\Services\Manager as DeliveryServicesManager;
use Bitrix\Sale\Internals\ServiceRestrictionTable;
use Bitrix\Sale\Shipment;
use Rybalka\Marketplace\Model\Checkout\DeliveryService;
use Rybalka\Marketplace\Util\StringsUtils;

class DeliveryServices
{
    private $deliveryRestrictionsRepository;

    public function __construct()
    {
        $this->deliveryRestrictionsRepository = new ServiceRestrictionTable();
    }

    public function getAvailableFor(Shipment $shipment)
    {
        $deliveryServices = DeliveryServicesManager::getRestrictedList(
            $shipment,
            RestrictionsManager::MODE_CLIENT
        );

        $results = [];

        foreach ($deliveryServices as $service) {
            $service = array_merge(
                $service, $this->parseExtraParametersFor((int)$service['ID'])
            );
            $results[] = DeliveryService::fromRaw($service);
        }

        return $results;
    }

    public function getIfAvailable(string $deliveryServiceCode, Shipment $shipment): ?DeliveryService
    {
        foreach ($this->getAvailableFor($shipment) as $service) {
            if ($service->getCode() == $deliveryServiceCode) {
                return $service;
            }
        }

        return null;
    }

    public function getFree(): array
    {
        $methods = [];

        $activeDeliveryMethods = DeliveryServicesManager::getActiveList();

        foreach ($activeDeliveryMethods as $method) {
            if (!preg_match('/\-free$/', $method['XML_ID'])) {
                continue;
            }

            $methods[] = $this->asDeliveryService($method);
        }

        return $methods;
    }

    public function getBy(?string $deliveryServiceCode): ?DeliveryService
    {
        if (empty($deliveryServiceCode)) {
            return null;
        }

        $deliveryServices = DeliveryServicesManager::getActiveList();

        foreach ($deliveryServices as $service) {

            if ($service['XML_ID'] == $deliveryServiceCode) {
                return $this->asDeliveryService($service);
            }
        }

        return null;
    }

    private function asDeliveryService(array $serviceData)
    {
        $serviceData = array_merge(
            $serviceData,
            $this->parseExtraParametersFor((int)$serviceData['ID']),
            [
                'RESTRICTIONS' => $this->parseRestrictionsFor((int)$serviceData['ID'])
            ]
        );

        return DeliveryService::fromRaw($serviceData);
    }

    private function parseRestrictionsFor(int $deliveryServiceId): array
    {
        $restrictions = [];
        $queryResult = $this->deliveryRestrictionsRepository->getList([
            'select' => ['*'],
            'filter' => [
                'SERVICE_ID' => $deliveryServiceId,
                'CLASS_NAME' => '%ByPrice'
            ],
        ]);

        if ($record = $queryResult->fetch()) {
            $restrictions['ORDER_AMOUNT'] = [
                'MIN' => !empty($record['PARAMS']['MIN_PRICE']) ? $record['PARAMS']['MIN_PRICE'] : null,
                'MAX' => !empty($record['PARAMS']['MAX_PRICE']) ? $record['PARAMS']['MAX_PRICE'] : null
            ];
        }

        return $restrictions;
    }

    private function parseExtraParametersFor(int $deliveryServiceId): array
    {
        $manager = new ExtraServicesManager($deliveryServiceId);
        $parameters = $manager->getItems();

        $parsed = [];

        foreach ($parameters as $parameter) {
            switch ($parameter->getCode()) {
                case 'SYS_ROUGH_PRICE':
                    $parsed['ROUGH_PRICE'] = [
                        'value' => $parameter->getParams()['PRICE'],
                        'currency' => $parameter->getOperatingCurrency()
                    ];
                    break;
                case 'SYS_REQUIRES':
                    $parsed['REQUIRES'] = StringsUtils::instance()->explode(';', $parameter->getDescription());
                    break;
                case 'SYS_ACCEPTS':
                    $parsed['ACCEPTS'] = StringsUtils::instance()->explode(';', $parameter->getDescription());
                    break;
                default:
                    continue;
            }
        }

        return $parsed;
    }
}