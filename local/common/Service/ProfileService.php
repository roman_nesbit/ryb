<?php


namespace Rybalka\Marketplace\Service;


use Bitrix\Sender\ContactTable;
use CUser;

class ProfileService
{
    public function getId(): ?int
    {
        return $this->getProfileFieldFromSession('USER_ID');
    }

    public function getName(): ?string
    {
        return $this->getProfileFieldFromSession('FIRST_NAME');
    }

    public function getSurname(): ?string
    {
        return $this->getProfileFieldFromSession('LAST_NAME');
    }

    public function getPhoneNumber(): ?string
    {
        return $this->getProfileFieldFromRepository('PERSONAL_PHONE');
    }

    public function getEmail(): ?string
    {
        return $this->getProfileFieldFromSession('EMAIL');
    }

    public function getDeliveryCityId(): ?string
    {
        return $this->getProfileFieldFromRepository('UF_CITY_ID');
    }

    public function getDeliveryNpOfficeId(): ?string
    {
        return $this->getProfileFieldFromRepository('UF_NP_OFFICE_ID');
    }

    public function isMaillistSubscriber(): bool
    {

        if ($this->getEmail() == null) {
            return false;
        }

        $result = ContactTable::getRow([
            'select' => [
                'CODE'
            ],
            'filter' => [
                'CODE' => $this->getEmail()
            ],
        ]);

        return $result['CODE'] === $this->getEmail();
    }

    private function getProfileFieldFromSession(string $fieldName): ?string
    {
        return $_SESSION['SESS_AUTH'][$fieldName] ?? null;
    }

    private function getProfileFieldFromRepository(string $fieldName): ?string
    {
        if ($this->getId() == null) {
            return null;
        }

        $filter = [
            'ID' => $this->getId()
        ];

        if (preg_match('/^UF_/', $fieldName)) {
            $parameters = [
                'SELECT' => [$fieldName]
            ];
        } else {
            $parameters = [
                'FIELDS' => [$fieldName]
            ];
        }

        $result = CUser::GetList($by, $order, $filter, $parameters);

        if ($data = $result->Fetch()) {

            return $data[$fieldName] ?? null;
        }

        return null;
    }

    public function setProfileFields($fields): bool
    {
        if(!$userId = $this->getId()) {
            return false;
        }

        $user = new CUser;
        return $user->Update($userId, $fields);
    } 

}