<?php


namespace Rybalka\Marketplace\Service;


use Rybalka\Marketplace\Model\Rest\Request\OrdersFilter;
use Rybalka\Marketplace\Util\ArrayUtil;

class RequestMapperService
{

    private $arrayUtil;

    public function __construct()
    {
        $this->arrayUtil = new ArrayUtil();
    }

    public function mapOrdersListRequest(array $rawRequestData): OrdersFilter
    {
        $result = new OrdersFilter();

        $result
            ->setPage(
                $this->arrayUtil->getIntOrNull($rawRequestData, 'page'))
            ->setItemsPerPage(
                $this->arrayUtil->getIntOrNull($rawRequestData, 'per_page')
            )
            ->setSortOrder(
                $this->arrayUtil->getOrNull($rawRequestData, 'order')
            )
            ->setOrderBy(
                $this->arrayUtil
            );

        return $result;
    }
}