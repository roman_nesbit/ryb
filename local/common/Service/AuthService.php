<?php


namespace Rybalka\Marketplace\Service;


use CUser;

class AuthService
{
    public function loginWithHashedPassword(string $username, string $password): bool
    {
        $user = $this->getUser();

        if($user->IsAuthorized() && $user->GetLogin() != $username)
        {
            $user->Logout();
        }

        $loginResult = $user->Login($username, $password, 'N', 'N');
        return $loginResult === true;
    }

    public function logout()
    {
        $user = $this->getUser();

        if ($user->IsAuthorized()) {
            $user->Logout();
        }
    }

    private function getUser(): CUser
    {
        global $USER;

        if (empty($USER)) {
            $USER = new CUser();
        }

        return $USER;
    }
}