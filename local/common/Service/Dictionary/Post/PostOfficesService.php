<?php


namespace Rybalka\Marketplace\Service\Dictionary\Post;


use Rybalka\Marketplace\Model\Dictionary\Post\PostOffice;
use Rybalka\Marketplace\Model\Dictionary\Address\Settlement;
use Rybalka\Marketplace\Repository\Dictionary\Address\PostOfficesTable;

class PostOfficesService
{
    /**
     * @var PostOfficesTable
     */
    private $repository;

    public function __construct()
    {
        $this->repository = new PostOfficesTable();
    }

    public function get(?string $postOfficeId): ?PostOffice
    {
        if (empty($postOfficeId)) {
            return null;
        }

        $queryResult = $this->repository->getByPrimary($postOfficeId);
        $record = $queryResult->fetch();

        return $record
            ? PostOffice::fromRaw($record)
            : null;
    }

    public function getListFor(Settlement $settlement): array
    {
        $offices = $settlement->getNumberOfWarehouses() > 0
            ? $this->repository->getListBy($settlement->getDeliverySettlementId())
            : $this->repository->getClosestTo($settlement->getLongitude(), $settlement->getLatitude());

        return array_map(function($entry) {
            return PostOffice::fromRaw($entry);
        }, $offices);
    }
}