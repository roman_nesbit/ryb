<?php

namespace Rybalka\Marketplace\Service\Dictionary\Address;

use InvalidArgumentException;
use Rybalka\Marketplace\Model\Dictionary\Address\Street;
use Rybalka\Marketplace\Repository\Dictionary\Address\StreetsCacheTable;
use Rybalka\Marketplace\Service\Dictionary\Import\Address\GenericNpApiClient;

class StreetsService
{
    /**
     * @var GenericNpApiClient
     */
    private $apiClient;

    /**
     * @var SettlementsService
     */
    private $settlementsService;

    /**
     * @var StreetsCacheTable
     */
    private $repository;

    public function __construct()
    {
        $this->apiClient = (new GenericNpApiClient())
            ->withHttpMethod('POST')
            ->withMethodName('getStreet')
            ->withModelName('Address');

        $this->settlementsService = new SettlementsService();
        $this->repository = new StreetsCacheTable();
    }

    public function getSuggestions(string $settlementId, string $partialName): array
    {
        $settlement = $this->settlementsService->get($settlementId);

        if (is_null($settlement)) {
            throw new InvalidArgumentException('There is no settlement with such id');
        }

        $partialName = trim($partialName);

        if (empty($partialName)) {
            return [];
        }

        if (empty($settlement->getDeliverySettlementId())) {
            return [];
        }

        $this->apiClient
            ->withProperty('CityRef', $settlement->getDeliverySettlementId())
            ->withProperty('FindByString', $partialName);

        $responseBody = $this->apiClient->loadPagedResource();

        $data = $responseBody['data'] ?? [];
        $response = [];
        $streetsForCaching = [];

        foreach ($data as $record) {
            $response[] = [
                'id' => $record['Ref'],
                'name' => $record['StreetsType']. ' ' . $record['Description']
            ];

            $forCache = $this->streetDataFromApiStructure($record);
            $forCache['delivery_settlement_id'] = $settlement->getDeliverySettlementId();

            $streetsForCaching[] = $forCache;
        }

        $this->repository->saveIfAbsent($streetsForCaching);

        return $response;
    }

    public function get(string $settlementId, string $streetId): ?Street
    {
        return $this->getFromCache($streetId) ?? $this->getFromApi($settlementId, $streetId);
    }

    private function getFromApi(string $settlementId, string $streetId): ?Street
    {
        $settlement = $this->settlementsService->get($settlementId);

        if (is_null($settlement)) {
            throw new InvalidArgumentException('There is no settlement with such id');
        }

        $this->apiClient
            ->withProperty('CityRef', $settlement->getDeliverySettlementId())
            ->withProperty('Ref', $streetId);

        $responseBody = $this->apiClient->loadPagedResource();

        $data = $responseBody['data'] ?? [];

        if (!isset($data[0])) {
            return null;
        }

        $remappedData = $this->streetDataFromApiStructure($data[0]);
        $remappedData['delivery_settlement_id'] = $settlement->getDeliverySettlementId();

        return Street::fromRaw($remappedData);
    }

    private function getFromCache(string $streetId): ?Street
    {
        if (empty($streetId)) {
            return null;
        }

        $queryResult = $this->repository->getByPrimary($streetId);
        $record = $queryResult->fetch();

        return $record
            ? Street::fromRaw($record)
            : null;
    }

    private function streetDataFromApiStructure(array $streetDataFromApi)
    {
        return [
            'id' => $streetDataFromApi['Ref'],
            'name' => $streetDataFromApi['Description'],
            'type' => $streetDataFromApi['StreetsType'],
        ];
    }
}