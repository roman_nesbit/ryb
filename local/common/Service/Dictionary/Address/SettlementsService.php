<?php

namespace Rybalka\Marketplace\Service\Dictionary\Address;

use Rybalka\Marketplace\Model\Dictionary\Address\Settlement;
use Rybalka\Marketplace\Repository\Dictionary\Address\SettlementsTable;

class SettlementsService
{
    /**
     * @var SettlementsTable
     */
    private $repository;

    public function __construct()
    {
        $this->repository = new SettlementsTable();
    }

    public function searchByPartialName(string $partialName): array
    {
        $partialName = trim($partialName);

        $results = [];
        $rawResults = empty($partialName)
            ? $this->repository->loadDefault()
            : $this->repository->loadStartingWith($partialName);

        foreach ($rawResults as $settlementData) {
            $results[] = Settlement::fromRaw($settlementData);
        }

        return $results;
    }

    public function get(?string $settlementId): ?Settlement
    {
        if (empty($settlementId)) {
            return null;
        }

        $queryResult = $this->repository->getByPrimary($settlementId);
        $record = $queryResult->fetch();


        return $record
            ? Settlement::fromRaw($record)
            : null;
    }
}