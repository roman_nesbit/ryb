<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import\Post;


use Bitrix\Main\Entity\DataManager;
use Monolog\Logger;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Repository\Dictionary\Address\PostOfficesTable;
use Rybalka\Marketplace\Repository\Dictionary\Address\PostOfficesTemporaryTable;
use Rybalka\Marketplace\Service\Dictionary\Import\Address\GenericNpApiClient;
use Rybalka\Marketplace\Service\Dictionary\Import\DictionaryImportService;
use Rybalka\Marketplace\Util\System\Timer;
use Symfony\Component\Lock\Lock;

class PostOfficesImportService implements DictionaryImportService
{
    private const RESOURCE_LOAD_MAX_ATTEMPTS = 3;
    private const BACK_OFF_PERIOD_IN_SECONDS = 2;

    /**
     * @var PostOfficesTemporaryTable
     */
    private $repository;

    /**
     * @var PostOfficesTable
     */
    private $postOfficesRepository;

    public function __construct()
    {
        $this->repository = new PostOfficesTemporaryTable();
        $this->postOfficesRepository = new PostOfficesTable();
    }

    public function getName(): string
    {
        return 'Post offices import';
    }

    public function getTemporaryTable(): DataManager
    {
        return $this->repository;
    }

    public function getPermanentTable(): DataManager
    {
        return $this->postOfficesRepository;
    }

    public function importAndPersistData(Lock $importProcessLock, Logger $logger)
    {
        $apiClient = $this->createApiClient();

        $newEntriesLoaded = true;
        $recordsAdded = 0;

        while ($newEntriesLoaded) {

            $timer = Timer::create();
            $importProcessLock->refresh();

            $page = ($page ?? 0) + 1;
            $responseBody = $apiClient->loadPagedResource($page);
            $newEntriesLoaded = !empty($responseBody['data']);

            if (!$newEntriesLoaded) {
                $logger->info('It seems like all records have been fetched', [
                    'page' => $page,
                    'totalRecords' => $recordsAdded
                ]);
                continue;
            }

            $rowsForInsert = [];

            foreach ($responseBody['data'] as $data) {

                $rowsForInsert[] = [
                    'id' => $data['Ref'],
                    'number' => intval($data['Number']),
                    'name_ru' => $data['DescriptionRu'],
                    'name_ua' => $data['Description'],
                    'address_ru' => $data['ShortAddressRu'],
                    'address_ua' => $data['ShortAddress'],
                    'phone_number' => $data['Phone'],
                    'type_id' => $data['TypeOfWarehouse'],
                    'settlement_id' => $data['SettlementRef'],
                    'delivery_settlement_id' => $data['CityRef'],
                    'latitude' => $data['Latitude'],
                    'longitude' => $data['Longitude']
                ];
            }

            $result = $this->repository->addMulti($rowsForInsert);

            if (!$result->isSuccess()) {
                throw new InternalServerErrorException('Failed to insert offices records to database');
            }

            $recordsAdded = ($recordsAdded ?? 0) + count($rowsForInsert);

            $logger->info('Imported new batch of the offices', [
                'totalRecords' => $recordsAdded,
                'elapsedTime' => $timer->elapsedTime()
            ]);
        }
    }

    private function createApiClient(): GenericNpApiClient
    {
        return (new GenericNpApiClient())
            ->withMaxNumberOfAttempts(self::RESOURCE_LOAD_MAX_ATTEMPTS)
            ->withBackOffBaseInSeconds(self::BACK_OFF_PERIOD_IN_SECONDS)
            ->withHttpMethod('POST')
            ->withMethodName('getWarehouses')
            ->withModelName('AddressGeneral');
    }
}