<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import;


use Bitrix\Main\Entity\DataManager;
use Monolog\Logger;
use Symfony\Component\Lock\Lock;

interface DictionaryImportService
{
    public function getName(): string;
    public function getTemporaryTable(): DataManager;
    public function getPermanentTable(): DataManager;

    public function importAndPersistData(Lock $importProcessLock, Logger $logger);
}