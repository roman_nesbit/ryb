<?php

namespace Rybalka\Marketplace\Service\Dictionary\Import\Address;

use Bitrix\Main\Entity\DataManager;
use Monolog\Logger;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Repository\Dictionary\Address\SettlementsTable;
use Rybalka\Marketplace\Repository\Dictionary\Address\SettlementsTemporaryTable;
use Rybalka\Marketplace\Service\Dictionary\Import\DictionaryImportService;
use Rybalka\Marketplace\Util\System\Timer;
use Symfony\Component\Lock\Lock;

class SettlementsImportService implements DictionaryImportService
{
    private const RESOURCE_LOAD_MAX_ATTEMPTS = 3;
    private const BACK_OFF_PERIOD_IN_SECONDS = 2;

    /**
     * @var SettlementsTemporaryTable
     */
    private $repository;

    /**
     * @var SettlementsTable
     */
    private $settlementsRepository;

    /**
     * @var DeliveryIdLookupService
     */
    private $deliverySettlementIdService;

    public function __construct()
    {
        $this->repository = new SettlementsTemporaryTable();
        $this->settlementsRepository = new SettlementsTable();
        $this->deliverySettlementIdService = new DeliveryIdLookupService();
    }

    public function getName(): string
    {
        return 'Settlements import';
    }

    public function getTemporaryTable(): DataManager
    {
        return $this->repository;
    }

    public function getPermanentTable(): DataManager
    {
        return $this->settlementsRepository;
    }

    public function importAndPersistData(Lock $importProcessLock, Logger $logger)
    {
        $this->loadAndPersistSettlements($importProcessLock, $logger);
        $this->copyPermanentDataFromPrimaryTable($importProcessLock, $logger);
        $this->loadAndPersistDeliverySettlementIds($importProcessLock, $logger);
    }

    private function loadAndPersistSettlements(Lock $importProcessLock, Logger $logger)
    {
        $apiClient = $this->createApiClient();
        $recordsAdded = 0;

        do {
            $importProcessLock->refresh();
            $timer = Timer::create();

            $page = ($page ?? 0) + 1;
            $responseBody = $apiClient->loadPagedResource($page);

            $responseContainsRecords = !empty($responseBody['data']);

            if (!$responseContainsRecords) {
                $logger->info('It seems like all records have been fetched', [
                    'page' => $page,
                    'totalRecords' => $recordsAdded
                ]);
                continue;
            }

            $rowsForInsert = [];

            foreach ($responseBody['data'] as $data) {

                $settlementId = $data['Ref'];

                $rowsForInsert[] = [
                    'id' => $settlementId,
                    'name_ru' => $data['DescriptionRu'],
                    'name_ua' => $data['Description'],
                    'type_id' => $data['SettlementType'],
                    'type_name_ru' => $data['SettlementTypeDescriptionRu'],
                    'type_name_ua' => $data['SettlementTypeDescription'],
                    'region_id' => $data['Region'],
                    'region_name_ru' => $data['RegionsDescriptionRu'],
                    'region_name_ua' => $data['RegionsDescription'],
                    'area_id' => $data['Area'],
                    'area_name_ru' => $data['AreaDescriptionRu'],
                    'area_name_ua' => $data['AreaDescription'],
                    'latitude' => $data['Latitude'],
                    'longitude' => $data['Longitude'],
                    'postcode_start' => trim($data['Index1']),
                    'postcode_end' => trim($data['Index2']),
                    'settlement_relative_size' => abs(intval($data['Index2']) - intval($data['Index1'])),
                    'warehouse' => $data['Warehouse'],
                ];
            }

            $result = $this->repository->addMulti($rowsForInsert);

            if (!$result->isSuccess()) {
                throw new InternalServerErrorException('Failed to insert settlements to database');
            }

            $recordsAdded = ($recordsAdded ?? 0) + count($rowsForInsert);

            $logger->info('Imported new settlements', [
                'totalRecords' => $recordsAdded,
                'elapsedTime' => $timer->elapsedTime()
            ]);

        } while ($responseContainsRecords);
    }

    private function copyPermanentDataFromPrimaryTable(Lock $importProcessLock, Logger $logger)
    {
        $timer = Timer::create();

        $stepName = 'Copy existing permanent data from primary table';
        $logger->info('Start: ' . $stepName);

        $filter = [
            'LOGIC' => 'OR',
            '!=delivery_settlement_id' => [null, ''],
            '!=sort_order' => [null]
        ];

        $queryResult = $this->settlementsRepository->getList([
            'filter' => $filter,
            'select' => [
                'id',
                'delivery_settlement_id',
                'sort_order'
            ]
        ]);

        $counter = 0;

        while($record = $queryResult->fetch()) {

            $counter++;

            if ($counter % 100 == 0) {
                $logger->info('Moved data about ' . $counter . ' settlements');
            }

            if ($importProcessLock->getRemainingLifetime() < 5) {
                $importProcessLock->refresh();
            }

            $this->repository->update($record['id'], [
                'delivery_settlement_id' => $record['delivery_settlement_id'],
                'sort_order' => $record['sort_order']
            ]);
        }

        $logger->info('Finish: ' . $stepName, [
            'elapsedTime' => $timer->elapsedTime()
        ]);
    }

    private function loadAndPersistDeliverySettlementIds(Lock $importProcessLock, Logger $logger)
    {
        $timer = Timer::create();

        $stepName = 'Load delivery settlement ids';
        $logger->info('Start: ' . $stepName);

        $filter = [
            'delivery_settlement_id' => [null, ''],
        ];

        $queryResult = $this->settlementsRepository->getList([
            'filter' => $filter,
            'select' => [
                'id',
                'postcode_start'
            ]
        ]);

        while($record = $queryResult->fetch()) {

            if ($importProcessLock->getRemainingLifetime() < 5) {
                $importProcessLock->refresh();
            }

            $deliverySettlementId = $this->fetchDeliverySettlementIdFor(
                $record['id'],
                $record['postcode_start'],
                $logger
            );

            if (!empty($deliverySettlementId)) {
                $this->repository->update($record['id'], [
                    'delivery_settlement_id' => $deliverySettlementId,
                ]);
            }
        }

        $logger->info('Finish: ' . $stepName, [
            'elapsedTime' => $timer->elapsedTime()
        ]);
    }

    private function fetchDeliverySettlementIdFor(string $settlementId, string $postalCode, Logger $logger): ?string
    {
        $logger->info('No delivery_settlement_id looked up in the past, loading from NP', [
            'settlementId' => $settlementId,
            'postalCode' => $postalCode
        ]);

        $deliveryId = $this->deliverySettlementIdService->lookupBy($settlementId, $postalCode);

        if (!empty($deliveryId) && !empty($settlement['id'])) {
            $this->settlementsRepository->update(
                $settlement['id'],
                [
                    'delivery_settlement_id'
                ]
            );
        }

        return $deliveryId;
    }

    private function createApiClient(): GenericNpApiClient
    {
        return (new GenericNpApiClient())
            ->withMaxNumberOfAttempts(self::RESOURCE_LOAD_MAX_ATTEMPTS)
            ->withBackOffBaseInSeconds(self::BACK_OFF_PERIOD_IN_SECONDS)
            ->withHttpMethod('POST')
            ->withMethodName('getSettlements')
            ->withModelName('AddressGeneral');
    }
}
