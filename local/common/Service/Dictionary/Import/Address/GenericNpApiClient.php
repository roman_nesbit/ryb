<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import\Address;


use Bitrix\Main\Config\Option;
use Rybalka\Marketplace\Exception\InvalidStateException;

class GenericNpApiClient
{
    const RESOURCES_PER_PAGE = 100;

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var int
     */
    private $maxNumberOfAttempts = 1;

    /**
     * @var int
     */
    private $backOffBaseInSeconds = 2;

    /**
     * @var string
     */
    private $modelName;

    /**
     * @var string
     */
    private $methodName;

    /**
     * @var string
     */
    private $httpMethod = 'GET';

    /**
     * @var array
     */
    private $properties = [];

    public function __construct()
    {
        $this->apiKey = trim(Option::get('grain.customsettings', 'NP_API_KEY'));
        $this->apiUrl = trim(Option::get('grain.customsettings', 'NP_API_URL'));

        if (empty($this->apiUrl)) {
            throw new InvalidStateException('API URL must be specified');
        }

        if (empty($this->apiKey)) {
            throw new InvalidStateException('API key must be specified');
        }
    }

    public function withModelName(string $modelName)
    {
        $this->modelName = $modelName;
        return $this;
    }

    public function withMethodName(string $methodName)
    {
        $this->methodName = $methodName;
        return $this;
    }

    public function withHttpMethod(string $httpMethod)
    {
        $this->httpMethod = strtoupper($httpMethod);
        return $this;
    }

    public function withMaxNumberOfAttempts(int $maxNumberOfAttempts)
    {
        $maxNumberOfAttempts = $maxNumberOfAttempts >= 1 ? $maxNumberOfAttempts : 1;
        $this->maxNumberOfAttempts = $maxNumberOfAttempts;

        return $this;
    }

    public function withBackOffBaseInSeconds(int $backOffBaseInSeconds)
    {
        $backOffBaseInSeconds = $backOffBaseInSeconds > 0 ? $backOffBaseInSeconds : 2;
        $this->backOffBaseInSeconds = $backOffBaseInSeconds;

        return $this;
    }

    public function withProperty(string $propertyName, string $propertyValue)
    {
        $this->properties[$propertyName] = $propertyValue;

        return $this;
    }

    public function loadPagedResource(int $page = 1): array
    {
        $page = $page >= 1 ? $page : 1;
        $doNewLoadAttempt = true;

        if (empty($this->methodName)) {
            throw new InvalidStateException('Method name must be specified');
        }

        if (empty($this->modelName)) {
            throw new InvalidStateException('Model name must be specified');
        }

        $methodProperties = array_merge(
            $this->properties,
            [
                'Page' => $page,
                'Limit' => self::RESOURCES_PER_PAGE
            ]
        );

        $requestBody = [
            'apiKey' => $this->apiKey,
            'modelName' => $this->modelName,
            'calledMethod' => $this->methodName,
            'methodProperties' => $methodProperties
        ];

        $curlOptions = [
            CURLOPT_URL => $this->apiUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->httpMethod,
            CURLOPT_POSTFIELDS => json_encode($requestBody),
            CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
        ];

        $responseBody = [];

        while ($doNewLoadAttempt) {

            $attempt = ($attempt ?? 0) + 1;

            if ($attempt > $this->maxNumberOfAttempts) {
                $errorMessage = 'The maximum number of attempts to load the resource is reached. Giving up.';
                $this->log($errorMessage);

                throw new ApiConsumptionException($errorMessage);
            }

            if ($attempt - 1 > 0) {
                $sleepTime = pow($this->backOffBaseInSeconds, $attempt - 1);
                $this->log("Will sleep for $sleepTime seconds before attempting to load the resource once again");

                sleep($sleepTime);
            }

            $requestHandler = curl_init();
            curl_setopt_array($requestHandler, $curlOptions);
            $response = curl_exec($requestHandler);
            $errorMessage = curl_error($requestHandler);
            curl_close($requestHandler);

            if ($response === false) {
                $this->log('Failed to load the resource. Error message from cURL: ' . $errorMessage);
                continue;
            }

            $responseBody = json_decode($response, true);

            if ($responseBody == null) {
                $this->log('Failed to decode response: ' . $response);
                continue;
            }

            if (!$responseBody['success']) {
                $errorMessage = sprintf(
                    'API endpoint responded with a failure. Errors: %s',
                    implode('; ', $responseBody['errors'])
                );
                $this->log($errorMessage);
                continue;
            }

            $doNewLoadAttempt = false;
        }

        return $responseBody;
    }

    public function __toString()
    {
        return $this->modelName . '::' . $this->methodName . '::' . $this->httpMethod;
    }

    private function log(string $message)
    {
        AddMessage2Log(sprintf('%s: %s', $this, $message));
    }
}