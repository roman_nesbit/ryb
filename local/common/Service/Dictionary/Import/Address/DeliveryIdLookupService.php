<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import\Address;


class DeliveryIdLookupService
{
    /**
     * @var GenericNpApiClient
     */
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = (new GenericNpApiClient())
            ->withHttpMethod('POST')
            ->withMethodName('searchSettlements')
            ->withModelName('Address');
    }

    public function lookupBy(string $settlementId, string $postalCode): ?string
    {
        $this->apiClient->withProperty(
            'CityName',
            $postalCode
        );

        $responseBody = $this->apiClient->loadPagedResource();
        $addresses = $responseBody['data'][0]['Addresses'] ?? [];

        foreach ($addresses as $record) {
            if ($record['Ref'] == $settlementId) {
                return $record['DeliveryCity'];
            }
        }

        return null;
    }
}