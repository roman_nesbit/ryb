<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import;


use Monolog\Logger;
use Rybalka\Marketplace\Repository\Util\TableManipulationUtil;
use Rybalka\Marketplace\Service\System\ProcessLockingService;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\System\Timer;

class FacilitationService
{
    /**
     * @var TableManipulationUtil
     */
    private $tableUtil;

    /**
     * @var ProcessLockingService
     */
    private $lockingService;

    public function __construct()
    {
        $this->tableUtil = new TableManipulationUtil();
        $this->lockingService = new ProcessLockingService();
    }

    public function fetch(DictionaryImportService $importService)
    {
        $lockName = $this->generateLockNameFrom($importService->getName());
        $logger = (new LoggerFactory())->getDictionaryImportLogger($lockName);

        try {

            $importTimer = Timer::create();

            $logger->info('Start: ' . $importService->getName());
            $lock = $this->lockingService->acquireLock($lockName);

            if (is_null($lock) || !$lock->isAcquired()) {
                $logger->warning('Failed to acquire lock. Terminating.');
                return;
            }

            $this->tableUtil->recreate($importService->getTemporaryTable());

            $importService->importAndPersistData($lock, $logger);

            $this->tableUtil->move(
                $importService->getTemporaryTable()->getTableName(),
                $importService->getPermanentTable()->getTableName()
            );

            $logger->info('Finish: ' . $importService->getName(), [
                'elapsedTime' => $importTimer->elapsedTime()
            ]);

        } catch (Exception $exception) {
            $logger->error(
                'Import has failed',
                [
                    'reason' => $exception->getMessage()
                ]
            );
        }
    }

    private function generateLockNameFrom(string $name)
    {
        $lockName = strtolower($name);
        $lockName = preg_replace('/\W/', '-', $lockName);

        return $lockName;
    }
}