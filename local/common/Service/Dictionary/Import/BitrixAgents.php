<?php


namespace Rybalka\Marketplace\Service\Dictionary\Import;


use Rybalka\Marketplace\Service\Dictionary\Import\Post\PostOfficesImportService;
use Rybalka\Marketplace\Service\Dictionary\Import\Address\SettlementsImportService;

class BitrixAgents
{
    public static function importSettlements(): string
    {
        $facilitator = new FacilitationService();
        $facilitator->fetch(new SettlementsImportService());

        return SettlementsImportService::class . '::importSettlements();';
    }

    public static function importPostOffices(): string
    {
        $facilitator = new FacilitationService();
        $facilitator->fetch(new PostOfficesImportService());

        return PostOfficesImportService::class . '::importPostOffices();';
    }
}