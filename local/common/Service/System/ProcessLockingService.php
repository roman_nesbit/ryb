<?php

namespace Rybalka\Marketplace\Service\System;

use Bitrix\Main\Application;
use Symfony\Component\Lock\Exception\LockAcquiringException;
use Symfony\Component\Lock\Lock;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;
use Symfony\Component\Lock\Store\PdoStore;
use Symfony\Component\Lock\Store\SemaphoreStore;

class ProcessLockingService
{
    /**
     * @var LockFactory
     */
    private $factory;

    public function __construct()
    {
        $dbConfig = Application::getConnection()->getConfiguration();

        $dsn = sprintf('mysql:host=%s;dbname=%s', $dbConfig['host'], $dbConfig['database']);
        $connectionSettings = [
            'db_username' => $dbConfig['login'],
            'db_password' => $dbConfig['password'],
            'db_table' => 'marketplace_locks'
        ];
        $store = new PdoStore($dsn, $connectionSettings);

        $this->factory = new LockFactory($store);
    }

    public function acquireLock(string $lockId, float $ttl = 300): ?Lock
    {
        try {
            $lock = $this->factory->createLock($lockId, $ttl);
            $isAcquired = $lock->acquire();
            return $isAcquired ? $lock : null;
        } catch (LockAcquiringException $exception) {
            return null;
        }
    }
}