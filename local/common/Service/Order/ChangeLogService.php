<?php

namespace Rybalka\Marketplace\Service\Order;

use Rybalka\Marketplace\Model\Order\ChangeLogEntry;
use Rybalka\Marketplace\Model\Order\Status;
use Rybalka\Marketplace\Model\ShopModerator;
use Rybalka\Marketplace\Repository\Order\ChangeLogRepository;

class ChangeLogService
{
    /**
     * @var ChangeLogRepository
     */
    private $changeLogRepository;

    private $statusService;

    public function __construct()
    {
        $this->changeLogRepository = new ChangeLogRepository();
        $this->statusService = new StatusService();
    }

    public function addStatusChange(string $orderId, ShopModerator $moderator, Status $status, string $note)
    {
        $data = [
            'statusId' => $status->getId(),
            'statusName' => $status->getName(),
            'note' => $note
        ];

        $this->addNewEntry(ChangeLogEntry::TYPE_STATUS_CHANGE, $orderId, $moderator, $data);
    }

    public function addNote(string $orderId, ShopModerator $moderator, string $note)
    {
        $data = [
            'note' => $note,
        ];

        $this->addNewEntry(ChangeLogEntry::TYPE_NOTE, $orderId, $moderator, $data);
    }

    private function addNewEntry(string $logEntryType, string $orderId, ShopModerator $moderator, array $data)
    {
        $entry = (new ChangeLogEntry())
            ->setType($logEntryType)
            ->setOrderId($orderId)
            ->setAuthorId($moderator->getUserId())
            ->setAuthorName($moderator->getFirstName() . ' ' . $moderator->getLastName())
            ->setData($data);

        $this->changeLogRepository->addEntry($entry);
    }
}