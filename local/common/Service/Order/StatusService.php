<?php

namespace Rybalka\Marketplace\Service\Order;

use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\NotFoundException;
use Rybalka\Marketplace\Model\Order\Status;
use Rybalka\Marketplace\Model\Order\StatusCollection;
use Rybalka\Marketplace\Repository\Order\StatusRepository;
use Rybalka\Marketplace\Service\Order\Status\TransitionService;

class StatusService
{
    private $statusRepository;

    private $statusTransitionService;

    public function __construct()
    {
        $this->statusRepository = new StatusRepository();
        $this->statusTransitionService = new TransitionService();
    }

    public function getAllowedStatuses(Status $currentStatus): StatusCollection
    {
        $allowedStatuses = new StatusCollection();
        $allStatuses = $this->statusRepository->loadAllByLanguageId(LANGUAGE_ID);

        foreach ($allStatuses as $toStatus) {

            /** @var Status $toStatus */
            if ($this->statusTransitionService->isTransitionAllowed($currentStatus, $toStatus)) {
                $allowedStatuses->add($toStatus);
            }
        }

        return $allowedStatuses;
    }

    /**
     * @param string $id
     * @return Status
     * @throws BadRequestException
     */
    public function getOrFail(string $id): Status
    {
        $status = $this->statusRepository->loadByIdAndLanguage($id, LANGUAGE_ID);

        if ($status == null) {
            throw new BadRequestException('A status with such id does not exist');
        }

        return $status;
    }
}