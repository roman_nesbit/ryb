<?php


namespace Rybalka\Marketplace\Service\Order;


use Bitrix\Sale\PropertyValueCollection;
use Rybalka\Marketplace\Repository\Order\PropertyRepository;

class PropertyService
{
    public function getPropertyValueById(string $propertyCode, PropertyValueCollection $collection)
    {
        foreach ($collection as $property) {
            if ($property->getField(PropertyRepository::FIELD_PROPERTY_NAME) == $propertyCode) {
                return $property->getField(PropertyRepository::FIELD_PROPERTY_VALUE);
            }
        }

        return null;
    }
}