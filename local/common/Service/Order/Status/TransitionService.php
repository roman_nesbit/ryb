<?php

namespace Rybalka\Marketplace\Service\Order\Status;

use Rybalka\Marketplace\Exception\ConfigurationException;
use Rybalka\Marketplace\Model\Order\Status;
use Rybalka\Marketplace\Repository\SettingsRepository;

class TransitionService
{
    const TEST_STATUS_ID = 'T';
    private $settingsRepository;

    public function __construct()
    {
        $this->settingsRepository = new SettingsRepository();
    }

    /**
     * @param Status $from
     * @param Status $to
     *
     * @return bool
     * @throws ConfigurationException
     */
    public function isTransitionAllowed(Status $from, Status $to): bool
    {
        $transitions = $this->settingsRepository->getOrderStatusTransitions();

        // the first symbol of the status id encodes it's "stage"
        // transitions within the same stage are always allowed
        $sameStage = $from->getStageId() == $to->getStageId();

        if ($sameStage) {
            return true;
        }

        if (empty($transitions)) {
            throw new ConfigurationException('Order status transitions are configured incorrectly');
        }

        preg_match_all('/' . $from->getStageId() . '-([A-Z])/', $transitions, $matches);

        $allowedStages = array_unique($matches[1]);

        if ($from->getId() != self::TEST_STATUS_ID) {
            $allowedStages[] = self::TEST_STATUS_ID;
        }

        return in_array($to->getStageId(), $allowedStages);
    }
}