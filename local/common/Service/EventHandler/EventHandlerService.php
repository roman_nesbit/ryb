<?php


namespace Rybalka\Marketplace\Service\EventHandler;


interface EventHandlerService
{
    public function eventTriggered($eventData);
}