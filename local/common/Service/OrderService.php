<?php

namespace Rybalka\Marketplace\Service;

use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\NotFoundException;
use Rybalka\Marketplace\Model\Order;
use Rybalka\Marketplace\Model\OrderCollection;
use Rybalka\Marketplace\Model\Rest\Request\Order\SetStatusRequest;
use Rybalka\Marketplace\Model\Rest\Request\OrdersFilter;
use Rybalka\Marketplace\Model\ShopModerator;
use Rybalka\Marketplace\Repository\OrderRepository;
use Rybalka\Marketplace\Service\Order\ChangeLogService;
use Rybalka\Marketplace\Service\Order\Status\TransitionService;
use Rybalka\Marketplace\Service\Order\StatusService;

class OrderService
{
    /**
     * @var OrderRepository $orderRepository;
     */
    private $orderRepository;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * @var TransitionService
     */
    private $statusTransitionService;

    /**
     * @var ChangeLogService
     */
    private $changeLogService;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->statusService = new StatusService();
        $this->statusTransitionService = new TransitionService();
        $this->changeLogService = new ChangeLogService();
    }

    public function getByFilter(OrdersFilter $filter): OrderCollection
    {
        return $this->orderRepository->loadByFilter($filter);
    }

    public function getOrFail(string $publicId, int $shopId): Order
    {
        $order = $this->orderRepository->loadByPublicId($publicId);

        if ($order == null || $order->getShopId() != $shopId) {
            throw new NotFoundException('Order with such id does not exist!');
        }

        return $order;
    }

    public function updateStatusOrFail(SetStatusRequest $request, ShopModerator $moderator)
    {
        $order = $this->orderRepository->loadByPublicId($request->getOrderPublicId());

        if ($order == null || $order->getShopId() != $moderator->getShopId()) {
            throw new NotFoundException('Order with such id does not exist!');
        }

        $newStatus = $this->statusService->getOrFail($request->getStatusId());

        if ($order->getStatus() == $newStatus) {
            return;
        }

        if (!$this->statusTransitionService->isTransitionAllowed($order->getStatus(), $newStatus)) {
            throw new BadRequestException('Status of the order may not be transitioned to the requested status');
        }

        $this->orderRepository->setStatus($order, $newStatus);

        // TODO: adding a record to a changelog should be detached from the status change
        // TODO: also, it should not fail it
        $this->changeLogService->addStatusChange($order->getPublicId(), $moderator, $newStatus, $request->getNote());
    }
}