<?php


namespace Rybalka\Marketplace\Service\External;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Rybalka\Marketplace\Util\FeatureToggle;
use Rybalka\Marketplace\Util\LoggerFactory;
use function GuzzleHttp\Promise\settle;

class UrlShorteningService
{
    private const API_ENDPOINT_SHORT_URL = '/rest/v2/short-urls';
    const REQUEST_FORMAT_JSON = 'application/json';
    const DEFAULT_REQUEST_TIMEOUT = 500;

    /**
     * @var string|null
     */
    private $serviceUrl;

    /**
     * @var string|null
     */
    private $serviceApiKey;

    /**
     * @var FeatureToggle
     */
    private $featureToggleService;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct($timeoutInMilliseconds = self::DEFAULT_REQUEST_TIMEOUT)
    {
        $this->featureToggleService = FeatureToggle::getInstance();

        $this->serviceUrl = getenv('URL_SHORTENING_SERVICE_URL') ?: null;
        $this->serviceApiKey = getenv('URL_SHORTENING_SERVICE_API_KEY') ?: null;

        $this->httpClient = new Client([
            'timeout' => $timeoutInMilliseconds / 1000,
            'base_uri' => $this->serviceUrl
        ]);

        $this->logger = (new LoggerFactory())->getLogger('short-url');
    }

    public function shortenAll(array $longUrls, $tags = []): array
    {
        $longUrls = array_unique($longUrls);
        $promises = [];

        foreach ($longUrls as $longUrl) {
            $promises[$longUrl] = $this->shortenAsync($longUrl, $tags[$longUrl] ?? []);
        }

        $responses = settle($promises)->wait();
        $result = [];

        foreach ($responses as $longUrl => $promiseResult) {

            if ($promiseResult['state'] == PromiseInterface::FULFILLED) {
                $result[$longUrl] = $this->getShortUrlFromResponseOrDefault(
                    $promiseResult['value'],
                    $longUrl);
                continue;
            }

            $this->logger->error('Failed to shorten url', [
                'message' => $promiseResult['reason']->getMessage(),
                'url' => $longUrl
            ]);

            $result[$longUrl] = $longUrl;
        }

        return $result;
    }

    public function shorten(string $longUrl, $tags = [], ?string $customName = null)
    {
        try {
            return $this->doShorten($longUrl, $tags, $customName);
        } catch (Exception $exception) {

            $this->logger->error('Failed to shorten url', [
                'message' => $exception->getMessage(),
                'url' => $longUrl
            ]);
            return $longUrl;
        }
    }

    private function shortenAsync(string $longUrl, $tags = [], ?string $customName = null): Promise
    {
        if (!$this->isEnabled()) {
            return (new Promise())->resolve($longUrl);
        }

        return $this->httpClient->postAsync(self::API_ENDPOINT_SHORT_URL, [
            'headers' => $this->buildHeaders(),
            'json' => $this->buildUrlShorteningRequest($longUrl, $tags, $customName)
        ]);
    }

    private function doShorten(string $longUrl, $tags = [], ?string $customName = null)
    {
        $response = $this->shortenAsync($longUrl, $tags, $customName)->wait();
        return $this->getShortUrlFromResponseOrDefault($response, $longUrl);
    }

    private function getShortUrlFromResponseOrDefault(ResponseInterface $response, string $defaultValue)
    {
        if ($response->getStatusCode() != 200) {
            return $defaultValue;
        }

        $content = $response->getBody()->getContents();

        $jsonResponse = json_decode($content, true);

        if (is_null($jsonResponse) || !isset($jsonResponse['shortUrl'])) {
            return $defaultValue;
        }

        return $jsonResponse['shortUrl'];
    }

    private function buildUrlShorteningRequest(string $longUrl, $tags = [], ?string $customName = null)
    {
        $result = [
            'longUrl' => $longUrl,
            'findIfExists' => true
        ];

        if (!empty($tags)) {

            $tags = is_array($tags) ? $tags : [$tags];
            $result['tags'] = $tags;
        }

        if (!empty($customName)) {
            $result['customSlug'] = $customName;
        }

        return $result;
    }

    private function buildHeaders()
    {
        return [
            'X-Api-Key' => $this->serviceApiKey,
            'Content-Type' => self::REQUEST_FORMAT_JSON
        ];
    }

    private function isEnabled()
    {
        return $this->featureToggleService->isUrlsShorteningEnabled()
            && !empty($this->serviceUrl)
            && !empty($this->serviceApiKey);
    }
}