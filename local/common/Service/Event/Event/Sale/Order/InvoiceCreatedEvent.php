<?php


namespace Rybalka\Marketplace\Service\Event\Event\Sale\Order;


use Bitrix\Sale\Order;

class InvoiceCreatedEvent
{
    public const NAME = 'order.invoice.created';

    /**
     * @var Order
     */
    private $order;

    /**
     * @var string
     */
    private $invoiceUrl;

    public function __construct(Order $order, string $publicInvoiceUrl)
    {
        $this->order = $order;
        $this->invoiceUrl = $publicInvoiceUrl;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getInvoiceUrl(): string
    {
        return $this->invoiceUrl;
    }
}