<?php


namespace Rybalka\Marketplace\Service\Event\Event\Sale\Order;


use Bitrix\Main\Event;

class PaymentCreatedEvent
{
    public const NAME = 'order.payment.created';

    /**
     * @var Event
     */
    private $event;

    public function __construct(Event $bitrixEvent)
    {
        $this->event = $bitrixEvent;
    }

    public function getEntity()
    {
        return $this->event->getParameter('ENTITY');
    }
}