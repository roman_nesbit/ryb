<?php


namespace Rybalka\Marketplace\Service\Event\Event\Sale\Order;


class StatusChangeEvent
{
    public const NAME = 'order.status-change';

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var string
     */
    private $statusId;

    public function __construct(int $orderId, string $statusId)
    {
        $this->orderId = $orderId;
        $this->statusId = $statusId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getStatusId(): string
    {
        return $this->statusId;
    }
}