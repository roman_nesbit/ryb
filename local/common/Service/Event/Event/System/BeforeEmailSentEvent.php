<?php

namespace Rybalka\Marketplace\Service\Event\Event\System;

use Symfony\Contracts\EventDispatcher\Event;

class BeforeEmailSentEvent extends Event
{
    public const NAME = 'system.email.before-sent';

    /**
     * @var string
     */
    private $eventName;

    /**
     * @var array
     */
    private $data;

    public function __construct(string $eventName, array &$data)
    {
        $this->eventName = $eventName;
        $this->data = &$data;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return $this->eventName;
    }

    /**
     * @return array
     */
    public function &getData(): array
    {
        return $this->data;
    }
}