<?php

namespace Rybalka\Marketplace\Service\Event\Event;

use Symfony\Contracts\EventDispatcher\Event;

abstract class AbstractReferencedDataEvent extends Event
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array &$data)
    {
        $this->data = &$data;
    }

    public function &getData(): array
    {
        return $this->data;
    }
}