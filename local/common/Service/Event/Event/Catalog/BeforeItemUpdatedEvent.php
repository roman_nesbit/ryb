<?php

namespace Rybalka\Marketplace\Service\Event\Event\Catalog;

use Rybalka\Marketplace\Service\Event\Event\AbstractReferencedDataEvent;

class BeforeItemUpdatedEvent extends AbstractReferencedDataEvent
{
    public const NAME = 'catalog.item.before-updated';
}