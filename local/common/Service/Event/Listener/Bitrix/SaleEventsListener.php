<?php


namespace Rybalka\Marketplace\Service\Event\Listener\Bitrix;


use Bitrix\Main\Event;
use Bitrix\Sale\Payment;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\OrderCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\PaymentCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\PaymentUpdatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\StatusChangeEvent;
use Rybalka\Marketplace\Service\Event\Listener\AbstractListener;

class SaleEventsListener extends AbstractListener
{
    public function onOrderStatusChange(int $orderId, string $statusId)
    {
        $event = new StatusChangeEvent($orderId, $statusId);
        $this->getDispatcher()->dispatch($event, $event::NAME);
    }

    public function saleOrderSaved(Event $bitrixEvent)
    {
        if ($bitrixEvent->getParameter('IS_NEW')) {
            $event = new OrderCreatedEvent($bitrixEvent);
            $this->getDispatcher()->dispatch($event, $event::NAME);
        }
    }

    public function salePaymentSaved(Event $bitrixEvent)
    {
        /**
         * @var Payment $payment
         */
        $payment = $bitrixEvent->getParameter('ENTITY');
        $isNew = isset($payment->getFields()->getChangedValues()['ID']);

        $event = $isNew
            ? new PaymentCreatedEvent($bitrixEvent)
            : new PaymentUpdatedEvent($bitrixEvent);

        $this->getDispatcher()->dispatch($event, $event::NAME);
    }
}