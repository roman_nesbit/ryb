<?php


namespace Rybalka\Marketplace\Service\Event\Listener\Bitrix;


use Rybalka\Marketplace\Service\Event\Event\System\BeforeEmailSentEvent;
use Rybalka\Marketplace\Service\Event\Listener\AbstractListener;

class EmailNotificationEventsListener extends AbstractListener
{
    public function onBefore(&$eventName, &$lid, &$eventData, &$messageId, &$files, &$languageId)
    {
        $event = new BeforeEmailSentEvent($eventName, $eventData);
        $this->getDispatcher()->dispatch($event, $event::NAME);
    }
}