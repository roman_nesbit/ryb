<?php

namespace Rybalka\Marketplace\Service\Event\Listener\Bitrix;

use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Service\Event\Event\Catalog\BeforeItemAddedEvent;
use Rybalka\Marketplace\Service\Event\Event\Catalog\BeforeItemUpdatedEvent;
use Rybalka\Marketplace\Service\Event\Listener\AbstractListener;

class InfoBlockEventsListener extends AbstractListener
{
    public function onBeforeUpdate(array &$data)
    {
        $infoBlockId = $data['IBLOCK_ID'] ?? null;

        switch ($infoBlockId) {
            case InfoBlock::CATALOG:
                $event = new BeforeItemUpdatedEvent($data);
                $this->getDispatcher()->dispatch($event, $event::NAME);
                break;
            default:
                break;
        }
    }

    public function onBeforeAdd(array &$data)
    {
        $infoBlockId = $data['IBLOCK_ID'] ?? null;

        switch ($infoBlockId) {
            case InfoBlock::CATALOG:
                $event = new BeforeItemAddedEvent($data);
                $this->getDispatcher()->dispatch($event, $event::NAME);
                break;
            default:
                break;
        }
    }
}