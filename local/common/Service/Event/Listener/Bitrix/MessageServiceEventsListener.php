<?php


namespace Rybalka\Marketplace\Service\Event\Listener\Bitrix;


use Rybalka\Marketplace\Service\Notification\Sms\Gateway\SmscUa;

class MessageServiceEventsListener
{
    public function onGetSmsSenders()
    {
        $this->services = $this->services ?? [new SmscUa()];
        return $this->services;
    }
}