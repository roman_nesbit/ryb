<?php

namespace Rybalka\Marketplace\Service\Event\Listener;

use Rybalka\Marketplace\Service\Event\EventsDispatcherFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;

abstract class AbstractListener
{
    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    public function __construct()
    {
        $this->dispatcher = (new EventsDispatcherFactory())->get();
    }

    /**
     * @return EventDispatcher
     */
    protected function getDispatcher(): EventDispatcher
    {
        return $this->dispatcher;
    }
}