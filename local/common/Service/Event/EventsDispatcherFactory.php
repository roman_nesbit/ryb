<?php


namespace Rybalka\Marketplace\Service\Event;

use Rybalka\Marketplace\Service\Event\Subscriber\Catalog\ItemEventsSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\Notification\EmailNotificationSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\Notification\SmsNotificationSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\Sale\CreateInvoiceForBankTransferSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\Sale\EnrichSaleEmailsSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\Sale\ResetInvoiceIdSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\System\EnrichEmailsWithContactsDataSubscriber;
use Rybalka\Marketplace\Service\Event\Subscriber\SmsOnOrderStatusChangeSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;

class EventsDispatcherFactory
{
    private static $instance = null;

    public function get(): EventDispatcher
    {
        if (!is_null(self::$instance)) {
            return self::$instance;
        }

        $dispatcher = new EventDispatcher();

        $dispatcher->addSubscriber(new ItemEventsSubscriber());
        $dispatcher->addSubscriber(new SmsNotificationSubscriber());
        $dispatcher->addSubscriber(new EmailNotificationSubscriber());
        $dispatcher->addSubscriber(new CreateInvoiceForBankTransferSubscriber());
        $dispatcher->addSubscriber(new EnrichSaleEmailsSubscriber());
        $dispatcher->addSubscriber(new EnrichEmailsWithContactsDataSubscriber());
        $dispatcher->addSubscriber(new ResetInvoiceIdSubscriber());

        self::$instance = $dispatcher;

        return $dispatcher;
    }
}