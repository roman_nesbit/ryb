<?php

namespace Rybalka\Marketplace\Service\Event\Subscriber\System;

use Bitrix\Main\Config\Option;
use Bitrix\Sale\PropertyValue;
use Rybalka\Marketplace\Repository\OrderRepository;
use Rybalka\Marketplace\Service\Event\Event\System\BeforeEmailSentEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Util\StringsUtils;

class EnrichEmailsWithContactsDataSubscriber extends AbstractSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            BeforeEmailSentEvent::NAME => [
                ['enrichWithContactsData'],
            ]
        ];
    }

    public function enrichWithContactsData(BeforeEmailSentEvent $event)
    {
        $event->getData()['SUPPORT_EMAIL'] = Option::get('grain.customsettings', 'SMS_SUPPORT_EMAIL');
        $event->getData()['SUPPORT_PHONE'] = Option::get('grain.customsettings', 'SMS_SUPPORT_PHONE');
    }
}