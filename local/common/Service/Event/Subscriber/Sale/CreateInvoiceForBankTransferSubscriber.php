<?php


namespace Rybalka\Marketplace\Service\Event\Subscriber\Sale;


use Bitrix\Main\Config\Option;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Exception;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\InvoiceCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\OrderCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\PaymentCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\PaymentUpdatedEvent;
use Rybalka\Marketplace\Service\Event\EventsDispatcherFactory;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Util\InvoicePdf;
use Sale\Handlers\Delivery\Additional\DeliveryRequests\RusPost\Requests\OrderCreate;

class CreateInvoiceForBankTransferSubscriber extends AbstractSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            PaymentCreatedEvent::NAME => 'onPaymentCreated',
            PaymentUpdatedEvent::NAME => 'onPaymentUpdated',
            OrderCreatedEvent::NAME => 'onOrderCreated'
        ];
    }

    public function onOrderCreated(OrderCreatedEvent $event)
    {
        /**
         * @var Order $order
         */
        $order = $event->getEntity();

        foreach ($order->getPaymentCollection() as $payment) {
            $this->generateInvoiceIfNeededFor($payment);
        }
    }

    public function onPaymentCreated(PaymentCreatedEvent $event)
    {
        /**
         * @var Payment $payment
         */
        $payment = $event->getEntity();

        if ($payment->getOrder()->isNew()) {
            // if the order is in process of creation, the 'onOrderCreated' method
            // has to take care of creation of the invoice. At this point, not all
            // data is available (like the chosen shipping method)
            return;
        }

        $this->generateInvoiceIfNeededFor($payment);
    }

    public function onPaymentUpdated(PaymentUpdatedEvent $event)
    {
        /**
         * @var Payment $payment
         */
        $payment = $event->getEntity();
        $paymentSystemChanged = !empty($payment->getFields()->getChangedValues()['PAY_SYSTEM_ID']);

        if (!$paymentSystemChanged) {
            return;
        }

        $this->generateInvoiceIfNeededFor($payment);
    }

    private function generateInvoiceIfNeededFor(Payment $payment)
    {
        try {
            $paymentServiceId = $payment->getPaySystem()->getField('ID');

            if (!$this->isInvoiceNeededFor($paymentServiceId)) {

                return;
            }

            $orderId = $payment->getOrder()->getId();
            $invoiceUrl = (new InvoicePdf($orderId))
                ->prepare()
                ->make()
                ->getInvoiceUrl();

            $invoiceCreatedEvent = new InvoiceCreatedEvent($payment->getOrder(), $invoiceUrl);
            (new EventsDispatcherFactory())->get()->dispatch($invoiceCreatedEvent, $invoiceCreatedEvent::NAME);
        } catch (Exception $exception) {
            $this->logger()->error('Failed to generate a PDF invoice', [
                'exception' => $exception
            ]);
        }
    }

    private function isInvoiceNeededFor(int $paymentServiceId)
    {
        $servicesWithInvoices = explode(',', Option::get('grain.customsettings', 'INVOICE_PAYSYS_IDS'));
        return in_array($paymentServiceId, $servicesWithInvoices);
    }
}