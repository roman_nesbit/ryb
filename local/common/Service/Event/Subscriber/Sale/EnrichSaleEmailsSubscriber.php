<?php

namespace Rybalka\Marketplace\Service\Event\Subscriber\Sale;

use Bitrix\Sale\PropertyValue;
use Rybalka\Marketplace\Repository\OrderRepository;
use Rybalka\Marketplace\Service\Event\Event\System\BeforeEmailSentEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Util\StringsUtils;

class EnrichSaleEmailsSubscriber extends AbstractSubscriber
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEmailSentEvent::NAME => [
                ['enrichWithBuyerName']
            ]
        ];
    }

    public function enrichWithBuyerName(BeforeEmailSentEvent $event)
    {
        if (!$this->isSaleEvent($event)) {
            return;
        }

        $orderId = $event->getData()['ORDER_REAL_ID'] ?? null;

        if (!$orderId) {
            return;
        }

        $order = $this->orderRepository->loadById($orderId);

        if (is_null($order)) {
            return;
        }

        foreach ($order->getPropertyCollection() as $property) {
            /**
             * @var PropertyValue $property
             */

            $code = $property->getField('CODE');

            switch ($code) {
                case 'NAME':
                    $event->getData()['CUSTOMER_FIRST_NAME'] = StringsUtils::instance()->ucFirst(
                        $property->getValue()
                    );
                    break;
                case 'LAST_NAME':
                    $event->getData()['CUSTOMER_LAST_NAME'] = StringsUtils::instance()->ucFirst(
                        $property->getValue()
                    );
                    break;
                default:
                    break;
            }
        }
    }

    private function isSaleEvent(BeforeEmailSentEvent $event)
    {
        return StringsUtils::instance()->startsWith(
            'SALE_',
            $event->getEventName()
        );
    }
}