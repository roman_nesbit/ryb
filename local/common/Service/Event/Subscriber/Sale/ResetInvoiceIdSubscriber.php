<?php


namespace Rybalka\Marketplace\Service\Event\Subscriber\Sale;


use Bitrix\Main\Config\Option;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\PaymentUpdatedEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Util\Bitrix\Model\OrderDataExtractor;

class ResetInvoiceIdSubscriber extends AbstractSubscriber
{

    /**
     * @var OrderDataExtractor
     */
    private $dataExtractor;

    public function __construct()
    {
        $this->dataExtractor = new OrderDataExtractor();
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            PaymentUpdatedEvent::NAME => 'onPaymentUpdated'
        ];
    }

    public function onPaymentUpdated(PaymentUpdatedEvent $event)
    {
        /**
         * @var Payment $payment
         */
        $payment = $event->getEntity();
        $paymentServiceId = $payment->getPaySystem()->getField('ID');

        if (!$this->isInvoiceNeededFor($paymentServiceId)) {

            $order = $payment->getOrder();
            $property = $this->dataExtractor->getProperty($order, 'INVOICE_PDF');

            if (empty($property) || empty($property->getValue())) {
                return;
            }

            $property->setValue(null);
            $order->save();
        }
    }

    private function isInvoiceNeededFor(int $paymentServiceId)
    {
        $servicesWithInvoices = explode(',', Option::get('grain.customsettings', 'INVOICE_PAYSYS_IDS'));
        return in_array($paymentServiceId, $servicesWithInvoices);
    }
}