<?php


namespace Rybalka\Marketplace\Service\Event\Subscriber\Catalog;


use Rybalka\Marketplace\Service\Catalog\Item\UtilService;
use Rybalka\Marketplace\Service\Event\Event\Catalog\BeforeItemAddedEvent;
use Rybalka\Marketplace\Service\Event\Event\Catalog\BeforeItemUpdatedEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Util\ArrayUtil;

class ItemEventsSubscriber extends AbstractSubscriber
{
    /**
     * @var UtilService
     */
    private $utilService;

    /**
     * @var ArrayUtil
     */
    private $arrayUtil;

    public function __construct()
    {
        $this->utilService = new UtilService();
        $this->arrayUtil = new ArrayUtil();
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeItemAddedEvent::NAME => 'onBeforeItemAdded',
            BeforeItemUpdatedEvent::NAME => 'onBeforeItemUpdated'
        ];
    }

    public function onBeforeItemAdded(BeforeItemAddedEvent $event)
    {
        $data = &$event->getData();
        $data['CODE'] = $this->utilService->generateCodeFor(
            (int)$data['ID'],
            $data['NAME'] ?? ''
        );
    }

    public function onBeforeItemUpdated(BeforeItemUpdatedEvent $event)
    {
        $data = &$event->getData();

        if (!$this->arrayUtil->allKeysSet($data, 'CODE', 'NAME', 'ID')) {
            return;
        }

        if (!empty($data['CODE'])) {
            return;
        }

        $data['CODE'] = $this->utilService->generateCodeFor((int)$data['ID'], $data['NAME']);

        $this->logger()->warning('A new code had to be generated for a catalog item', [
            'id' => $data['ID'],
            'code' => $data['CODE']
        ]);
    }
}