<?php

namespace Rybalka\Marketplace\Service\Event\Subscriber;

use Monolog\Logger;
use Rybalka\Marketplace\Util\LoggerFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

abstract class AbstractSubscriber implements EventSubscriberInterface
{
    private $logger;

    public function logger(): Logger
    {
        $this->logger = $this->logger ?? (new LoggerFactory())->getLogger($this->getLoggerId());
        return $this->logger;
    }

    private function getLoggerId(): string
    {
        $className = get_class($this);

        if (preg_match('/\w+$/', $className, $matches)) {
            return 'event.' . $matches[0];
        }

        return 'event.' . $className;
    }
}