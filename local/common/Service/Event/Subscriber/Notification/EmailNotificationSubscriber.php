<?php


namespace Rybalka\Marketplace\Service\Event\Subscriber\Notification;


use Bitrix\Main\Mail\Event;
use CEvent;
use COption;
use Rybalka\Marketplace\Model\Bitrix\MarketplaceConstants;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\InvoiceCreatedEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;

class EmailNotificationSubscriber extends AbstractSubscriber
{
    public static function getSubscribedEvents()
    {
        return [
            InvoiceCreatedEvent::NAME => 'sendInvoicePerEmail'
        ];
    }

    public function sendInvoicePerEmail(InvoiceCreatedEvent $event)
    {
        $invoiceUrl = $event->getInvoiceUrl();
        $order = $event->getOrder();

        $templateData = [
            'INVOICE_LINK' => $invoiceUrl,
            'ORDER_REAL_ID' => $order->getId(),
            'EMAIL' => $order->getPropertyCollection()->getUserEmail()->getValue(),
            'ORDER_ID' => $order->getField('ACCOUNT_NUMBER'),
            'BCC' => COption::GetOptionString('sale', 'order_email'),
            'SALE_EMAIL' => COption::GetOptionString('sale', 'order_email'),
        ];

        CEvent::Send(
            'SALE_NEW_INVOICE',
            MarketplaceConstants::SITE_ID,
            $templateData
        );
    }
}