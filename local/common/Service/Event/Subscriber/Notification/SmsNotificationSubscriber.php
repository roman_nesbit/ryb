<?php


namespace Rybalka\Marketplace\Service\Event\Subscriber\Notification;


use Bitrix\Sale\Notify;
use Bitrix\Sale\Order;
use Rybalka\Marketplace\Repository\OrderRepository;
use Rybalka\Marketplace\Service\Checkout\AccountDetailsGenerationService;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\InvoiceCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\OrderCreatedEvent;
use Rybalka\Marketplace\Service\Event\Event\Sale\Order\StatusChangeEvent;
use Rybalka\Marketplace\Service\Event\Event\System\BeforeEmailSentEvent;
use Rybalka\Marketplace\Service\Event\Subscriber\AbstractSubscriber;
use Rybalka\Marketplace\Service\Notification\SmsService;

class SmsNotificationSubscriber extends AbstractSubscriber
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var AccountDetailsGenerationService
     */
    private $accountsDetailsGenerationService;

    /**
     * @var SmsService
     */
    private $smsService;

    public static function getSubscribedEvents()
    {
        return [
            StatusChangeEvent::NAME => 'onOrderStatusChange',
            OrderCreatedEvent::NAME => 'onOrderCreated',
            InvoiceCreatedEvent::NAME => 'onInvoiceCreated',
            BeforeEmailSentEvent::NAME => 'onTrackingNumberEmailEvent'
        ];
    }

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->accountsDetailsGenerationService = new AccountDetailsGenerationService();
        $this->smsService = new SmsService();
    }

    public function onOrderCreated(OrderCreatedEvent $event)
    {
        /**
         * @var Order $order
         */
        $order = $event->getEntity();

        if (is_null($order)) {
            $this->logger()->error('Order creation event arrived without data');
            return;
        }

        $phoneNumber = $order->getPropertyCollection()->getPhone()->getValue();

        if (empty($phoneNumber)) {
            $this->logger()->error('Order has no phone number attached to it', [
                'orderId' => $order->getId()
            ]);
            return;
        }

        $this->smsService->delayedDispatch(
            'SMS_NEW_ORDER',
            [
                'USER_PHONE' => $phoneNumber,
                'ORDER_ID' => $order->getField('ACCOUNT_NUMBER')
            ]
        );
    }

    public function onInvoiceCreated(InvoiceCreatedEvent $event)
    {
        if (empty($event->getInvoiceUrl())) {
            $this->logger()->error('Invoice creation event arrived without invoice url');
            return;
        }

        $order = $event->getOrder();

        if (is_null($order)) {
            $this->logger()->error('Invoice creation event arrived without order data');
            return;
        }

        $phoneNumber = $order->getPropertyCollection()->getPhone()->getValue();

        if (empty($phoneNumber)) {
            $this->logger()->error('Order has no phone number attached to it', [
                'orderId' => $order->getId()
            ]);
            return;
        }

        $this->smsService->delayedDispatch(
            'SMS_INVOICE_LINK',
            [
                'USER_PHONE' => $phoneNumber,
                'ORDER_ID' => $order->getField('ACCOUNT_NUMBER'),
                'INVOICE_URL' => $event->getInvoiceUrl()
            ]
        );
    }

    public function onOrderStatusChange(StatusChangeEvent $event)
    {
        $order = $this->orderRepository->loadById($event->getOrderId());

        if (empty($order)) {
            $this->logger()->error('Could not load order', [
                'orderId' => $event->getOrderId(),
                'statusId' => $event->getStatusId()
            ]);

            return;
        }

        $email = $order->getPropertyCollection()->getUserEmail()->getValue();

        if (!empty($email) && !$this->accountsDetailsGenerationService->isGeneratedEmail($email)) {
            // do not send sms notification if it may be sent via email
            return;
        }

        $eventName = 'SMS_SALE_STATUS_CHANGED_' . $event->getStatusId();
        $this->smsService->delayedDispatch(
            $eventName,
            [
                'ORDER_ID' => $order->getField('ACCOUNT_NUMBER'),
                'USER_PHONE' => $order->getPropertyCollection()->getPhone()->getValue()
            ]
        );
    }

    public function onTrackingNumberEmailEvent(BeforeEmailSentEvent $event)
    {
        if (!$this->isTrackingNumberChangeEvent($event)) {
            return;
        }

        $trackingId = $event->getData()['ORDER_TRACKING_NUMBER'] ?? null;

        if (empty($trackingId)) {
            return;
        }

        $email = $event->getData()['EMAIL'] ?? null;

        if (!empty($email) && !$this->accountsDetailsGenerationService->isGeneratedEmail($email)) {
            // do not send sms notification if it may be sent via email
            return;
        }

        $orderId = $event->getData()['ORDER_REAL_ID'] ?? null;

        if (empty($orderId)) {
            $this->logger()->error('Shipment tracking change event came broken', $event->getData());
            return;
        }

        $order = $this->orderRepository->loadById($orderId);

        if (empty($order)) {
            $this->logger()->error('Could not load order by order id', [
                'orderId' => $orderId,
            ]);

            return;
        }

        $eventName = 'SMS_SALE_ORDER_TRACKING_NUMBER';
        $this->smsService->delayedDispatch(
            $eventName,
            [
                'ORDER_ID' => $order->getField('ACCOUNT_NUMBER'),
                'USER_PHONE' => $order->getPropertyCollection()->getPhone()->getValue(),
                'TRACKING_NUMBER' => $trackingId
            ]
        );
    }

    private function isTrackingNumberChangeEvent(BeforeEmailSentEvent $event)
    {
        return $event->getEventName() == Notify::EVENT_SHIPMENT_TRACKING_NUMBER_SEND_EMAIL_EVENT_NAME;
    }
}