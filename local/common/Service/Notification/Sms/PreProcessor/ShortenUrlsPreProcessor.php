<?php


namespace Rybalka\Marketplace\Service\Notification\Sms\PreProcessor;


use Rybalka\Marketplace\Service\External\UrlShorteningService;
use Rybalka\Marketplace\Service\Notification\Sms\SmsPreProcessor;
use Rybalka\Marketplace\Util\UrlUtil;

class ShortenUrlsPreProcessor implements SmsPreProcessor
{
    /**
     * @var UrlShorteningService
     */
    private $service;

    public function __construct()
    {
        $this->service = new UrlShorteningService();
    }

    public function process(array $smsData): array
    {
        $text = $smsData['MESSAGE_BODY'];
        if (!preg_match_all(UrlUtil::URL_PATTERN, $text, $matches)) {
            return $smsData;
        }

        $shortenedUrls = $this->service->shortenAll($matches[0]);

        foreach ($shortenedUrls as $longUrl => $shortUrl) {
            $text = str_replace($longUrl, $shortUrl, $text);
        }

        $smsData['MESSAGE_BODY'] = $text;
        return $smsData;
    }
}