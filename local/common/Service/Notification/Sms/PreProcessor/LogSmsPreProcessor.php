<?php


namespace Rybalka\Marketplace\Service\Notification\Sms\PreProcessor;


use Rybalka\Marketplace\Service\Notification\Sms\SmsPreProcessor;
use Rybalka\Marketplace\Util\EventLog;

class LogSmsPreProcessor implements SmsPreProcessor
{
    public function process(array $smsData): array
    {
        EventLog::add(
            EventLog::SEVERITY_INFO,
            EventLog::TYPE_SEND_SMS,
            'Phone: ' . $smsData['MESSAGE_TO'] . ' Сообщение: ' . $smsData['MESSAGE_BODY']
        );

        return $smsData;
    }
}