<?php


namespace Rybalka\Marketplace\Service\Notification\Sms\Gateway;


use Bitrix\Main\Error;
use Bitrix\MessageService\Sender\Base;
use Bitrix\MessageService\Sender\Result\SendMessage;
use Monolog\Logger;
use Rybalka\Marketplace\Service\Notification\Sms\PreProcessor\LogSmsPreProcessor;
use Rybalka\Marketplace\Service\Notification\Sms\PreProcessor\SetDefaultDataPreProcessor;
use Rybalka\Marketplace\Service\Notification\Sms\PreProcessor\ShortenUrlsPreProcessor;
use Rybalka\Marketplace\Service\Notification\Sms\SmsPreProcessor;
use Rybalka\Marketplace\Util\FeatureToggle;
use Rybalka\Marketplace\Util\LoggerFactory;

class SmscUa extends Base
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var array
     */
    private $preprocessors;

    public function __construct()
    {
        $this->logger = (new LoggerFactory())->getLogger('sms');
        $this->preprocessors = [
            new ShortenUrlsPreProcessor(),
            new LogSmsPreProcessor()
        ];
    }

    public function getId()
    {
        return 'smsc';
    }

    public function getName()
    {
        return 'smsc';
    }

    /**
     * @inheritDoc
     */
    public function getShortName()
    {
        return 'smsc';
    }

    /**
     * @inheritDoc
     */
    public function canUse()
    {
        return true;
    }

    public function getFromList()
    {
        return [
            [
                'id' => getenv('SMSC_SENDER_ID'),
                'name' => getenv('SMSC_SENDER_ID'),
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function sendMessage(array $fields)
    {
        $fields = $this->preProcessSmsData($fields);
        $result = new SendMessage();

        if (FeatureToggle::getInstance()->isSmsTestModeOn()) {
            $this->logger->info('Sms is not sent as the test mode is enabled');

            $result->setAccepted();
            return $result;
        }

        $sendResult = self::doSend(
            $fields['MESSAGE_FROM'],
            $fields['MESSAGE_TO'],
            $fields['MESSAGE_BODY']
        );

        if($sendResult !== true) {
            $this->logger->error('Failed to send sms message', [
                'from' => $fields['MESSAGE_FROM'],
                'to' => $fields['MESSAGE_TO'],
                'message' => $fields['MESSAGE_BODY'],
                'error' => $sendResult
            ]);

            $result->addError(new Error($sendResult));
        } else {
            $result->setAccepted();
        }

        return $result;
    }

    private function preProcessSmsData(array $data): array
    {
        foreach ($this->preprocessors as $preprocessor) {
            /**
             * @var SmsPreProcessor $preprocessor
             */
            $data = $preprocessor->process($data);
        }

        return $data;
    }

    private function doSend(string $sender, string $phone, string $message)
    {
        $urlQuery = self::prepareUrlQuery($sender, $phone, $message);
        $smsResultStr = file_get_contents('https://smsc.ua/sys/send.php?' . $urlQuery);

        $isSuccess = strpos($smsResultStr, 'OK') !== false;

        if (!$isSuccess) {
            return $smsResultStr;
        }

        return true;
    }

    private function prepareUrlQuery(string $sender, string $phone, string $message): string
    {
        $pairedParams = [];

        foreach ($this->prepareUrlQueryParams($sender, $phone, $message) as $key => $value) {
            $pairedParams[] = $key . '=' . urlencode($value);
        }

        return implode('&', $pairedParams);
    }

    private function prepareUrlQueryParams(string $sender, string $phone, string $message): array
    {
        return [
            'login' => getenv('SMSC_LOGIN'),
            'psw' => getenv('SMSC_PASSWORD'),
            'phones' => $phone,
            'mes' => $message,
            'sender' => $sender
        ];
    }
}