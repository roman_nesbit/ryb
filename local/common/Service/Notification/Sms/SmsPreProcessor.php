<?php


namespace Rybalka\Marketplace\Service\Notification\Sms;


interface SmsPreProcessor
{
    public function process(array $smsData): array;
}