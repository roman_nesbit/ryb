<?php


namespace Rybalka\Marketplace\Service\Notification;


use Bitrix\Main\Config\Option;
use Bitrix\Main\Result;
use Bitrix\Main\Sms\Event;
use Rybalka\Marketplace\Model\Bitrix\MarketplaceConstants;

class SmsService
{
    public function dispatchNow(string $eventName, array $data = []): Result
    {
        return $this->dispatch($eventName, $data, true);
    }

    public function delayedDispatch(string $eventName, array $data = []): Result
    {
        return $this->dispatch($eventName, $data, false);
    }

    private function dispatch(string $eventName, array $data, bool $dispatchNow): Result
    {
        $data = $this->enrichWithDefaultData($data);

        return (new Event($eventName, $data))
            ->setSite(MarketplaceConstants::SITE_ID)
            ->send($dispatchNow);
    }

    private function enrichWithDefaultData(array $data): array
    {
        $defaultData = [
            'SUPPORT_PHONE' => Option::get('grain.customsettings', 'SMS_SUPPORT_PHONE'),
            'SUPPORT_EMAIL' => Option::get('grain.customsettings', 'SMS_SUPPORT_EMAIL')
        ];

        return array_merge($defaultData, $data);
    }
}