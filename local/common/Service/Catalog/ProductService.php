<?php


namespace Rybalka\Marketplace\Service\Catalog;


use Bitrix\Main\Loader;
use CIBlockElement;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock\CommonField;
use Rybalka\Marketplace\Model\Catalog\Product;

class ProductService
{
    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    public function loadBySkuAndShopId(string $sku, int $shopId) {
        $filter = [
            CommonField::INFO_BLOCK_ID => InfoBlock::CATALOG,
            CommonField::SHOP_ID => $shopId,
            CommonField::SKU => $sku
        ];

        $result = CIBlockElement::GetList([], $filter);
        if ($rawData = $result->GetNextElement(true, false)) {
            return Product::fromRawData($rawData->GetFields());
        }

        return null;
    }
}