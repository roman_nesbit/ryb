<?php


namespace Rybalka\Marketplace\Service\Catalog\Item;


use CIBlockElement;
use CUtil;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;

class UtilService
{
    public function generateCodeFor(int $catalogItemId, string $itemName)
    {
        $base = Cutil::translit($itemName, 'ru', [
                'change_case' => 'L',
                'replace_space' => '-',
                'replace_other' => '-'
            ]);

        $code = $base;

        do {

            $filter = [
                'IBLOCK_ID' => InfoBlock::CATALOG,
                '!=ID' => $catalogItemId,
                'CODE' => $code,
            ];

            $result = CIBlockElement::GetList([], $filter, [], false, []);

            if (!$result) {
                break;
            }

            $code = implode('-', [
                $base,
                randString(6, ['abcdefghijklnmopqrstuvwxyz'])
            ]);
        } while (true);

        return $code;
    }
}