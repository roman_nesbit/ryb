<?php

namespace Rybalka\Marketplace\Service\Catalog;

use Bitrix\Main\Loader;
use CIBlockSection;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock\CommonField;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock\CommonValue;
use Rybalka\Marketplace\Model\Catalog\Category;
use Rybalka\Marketplace\Model\Catalog\CategoryCollection;

class CategoryService
{

    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    public function getActiveCategories(): CategoryCollection {
        return $this->buildTree(
            $this->loadAllActive()
        );
    }

    private function loadAllActive(): array {
        $categories = [];

        $filter = [
            CommonField::INFO_BLOCK_ID => InfoBlock::CATALOG,
            CommonField::ACTIVE => CommonValue::YES
        ];

        $result = CIBlockSection::GetList([], $filter);

        while ($rawCategoryData = $result->GetNext(true, false)) {
            $category = Category::fromRawData($rawCategoryData);
            $categories[$category->getId()] = $category;
        }

        return $categories;
    }

    private function buildTree(array $categories): CategoryCollection {

        $categoryCollection = new CategoryCollection();

        foreach ($categories as $category) {
            if ($category->getParentId() == 0) {
                $categoryCollection->add($category);
                continue;
            }

            if (!isset($categories[$category->getParentId()])) {
                // TODO: LOG-WARN: No parent category found
                continue;
            }

            $categories[$category->getParentId()]->addChild($category);
        }

        return $categoryCollection;
    }
}