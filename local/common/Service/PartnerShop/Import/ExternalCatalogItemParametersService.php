<?php


namespace Rybalka\Marketplace\Service\PartnerShop\Import;


use Rybalka\Marketplace\Model\PartnerShop;
use Rybalka\Marketplace\Model\PartnerShop\Import\ExternalCatalogItemParameter;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCatalogItemParametersTable;
use Rybalka\Marketplace\Util\PartnerShop\Import\NormalizationUtil;

class ExternalCatalogItemParametersService
{
    /**
     * @var PartnerShop
     */
    private $partnerShop;

    /**
     * @var NormalizationService
     */
    private $normalizer;

    /**
     * @var ExternalCatalogItemParameter
     */
    private $repository;

    public function __construct(PartnerShop $partnerShop)
    {
        $this->partnerShop = $partnerShop;
        $this->normalizer = new NormalizationUtil();
        $this->repository = new ExternalCatalogItemParametersTable();
    }

    public function addAllIfNew(array $parameters)
    {
        foreach ($parameters as $name) {
            $this->addAndMapParameter($name);
        }
    }

    public function getAllMappings(): array
    {
        return $this->repository->loadMappedParametersFor($this->partnerShop->getId());
    }

    public function getWithMappingBy(string $parameterName, int $categoryId)
    {
        return $this->getMappedParameterByNameAndCategory($parameterName, $categoryId)
            ?? $this->getMappedParameterByNameAndCategory($parameterName, 0);
    }

    private function addAndMapParameter(ExternalCatalogItemParameter $parameterName)
    {
        $parameter = $this->repository->addIfNotExists($parameterName);

        if (!$this->isAutomaticMappingNeeded($parameter)) {
            return;
        }

        $globalMapping = $this->repository->getGlobalMappingByCode($parameter->getCode());

        if (!is_null($globalMapping)) {
            $this->repository->copySettingsFrom($parameter->getId(), $globalMapping);
        }
    }

    private function isAutomaticMappingNeeded(ExternalCatalogItemParameter $parameter)
    {
        return is_null($parameter->getInternalCode()) || $parameter->isAutomaticallyMapped();
    }

    private function getMappedParameterByNameAndCategory(string $name, int $categoryId)
    {
        $externalParameter = ExternalCatalogItemParameter::fromRaw([
            'EXTERNAL_PARAMETER_NAME' => $name,
            'EXTERNAL_CATEGORY_ID' => $categoryId,
            'PARTNER_ID' => $this->partnerShop->getId()
        ]);

        $result = $this->repository->findOneBy($externalParameter);

        if (!is_null($result) && !is_null($result->getInternalCode())) {
            return $result;
        }

        return null;
    }
}