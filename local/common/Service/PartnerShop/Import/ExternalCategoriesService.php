<?php

namespace Rybalka\Marketplace\Service\PartnerShop\Import;

use InvalidArgumentException;
use Rybalka\Marketplace\Model\PartnerShop;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCategoriesTable;
use SimpleXMLElement;

class ExternalCategoriesService
{
    /**
     * @var PartnerShop
     */
    private $partnerShop;

    /**
     * @var ExternalCategoriesTable
     */
    private $repository;
    private $mappingsCache = null;

    public function __construct(PartnerShop $partnerShop)
    {
        $this->repository = new ExternalCategoriesTable();
        $this->partnerShop = $partnerShop;
    }

    public function import(SimpleXMLElement $categoriesNode)
    {
        if (!isset($categoriesNode->category)) {
            throw new InvalidArgumentException('No category node found in xml');
        }

        foreach ($categoriesNode->category as $currentCategory) {
            $categoryId = (int)($currentCategory->attributes()->id ?? null);
            $parentId = (int)($currentCategory->attributes()->parentId ?? null);

            if (empty($categoryId)) {
                $message = sprintf(
                    'Bad category descriptor. Shop id: %s, Descriptor: %s',
                    $this->partnerShop->getId(),
                    $currentCategory->asXML()
                );

                AddMessage2Log($message);
                continue;
            }

            $categoryName = trim((string)$currentCategory);

            if (empty($categoryName)) {
                $message = sprintf('Empty category name. Shop id: %s, Descriptor', $currentCategory->asXML());

                AddMessage2Log($message);
                continue;
            }

            $parentId = empty($parentId) ? null : $parentId;

            $this->addOrUpdate($categoryId, $categoryName, $parentId);
        }
    }

    public function getInternalCategoryFor(int $categoryId): ?int
    {
        $this->assertMappingsLoaded();
        return $this->mappingsCache[$categoryId] ?? null;
    }

    public function getAllMappings(): array
    {
        $this->assertMappingsLoaded();
        return $this->mappingsCache;
    }

    public function addOrUpdate(int $categoryId, string $name, ?int $parentId)
    {
        $existingRecord = $this->loadByExternalCategoryId($categoryId);

        if (empty($existingRecord)) {
            $this->add($categoryId, $name, $parentId);
            return;
        }

        $this->update($existingRecord['ID'], $name, $parentId);
    }

    public function add(int $categoryId, string $name, ?int $parentId)
    {
        $data = [
            'PARTNER_ID' => $this->partnerShop->getId(),
            'EXTERNAL_CATEGORY_ID' => $categoryId,
            'EXTERNAL_CATEGORY_NAME' => $name
        ];

        if (!is_null($parentId)) {
            $data['EXTERNAL_CATEGORY_PARENT_ID'] = $parentId;
        }

        $this->repository->add($data);
    }

    public function update(int $categoryId, string $name, ?int $parentId)
    {
        $dataToUpdate = [
            'EXTERNAL_CATEGORY_NAME' => $name,
            'EXTERNAL_CATEGORY_PARENT_ID' => $parentId
        ];

        $this->repository->update($categoryId, $dataToUpdate);
    }

    public function mapTo(int $categoryId, ?int $internalCategoryId, ?int $userId)
    {
        $dataToUpdate = [
            'INTERNAL_CATEGORY_ID' => $internalCategoryId,
            'UPDATED_BY' => $userId
        ];

        $this->repository->update($categoryId, $dataToUpdate);
    }

    private function assertMappingsLoaded()
    {
        if (is_null($this->mappingsCache)) {
            $this->mappingsCache = $this->loadAll();
        }
    }

    private function loadByExternalCategoryId(int $categoryId)
    {
        $query = [
            'filter' => [
                'PARTNER_ID' => $this->partnerShop->getId(),
                'EXTERNAL_CATEGORY_ID' => $categoryId
            ]
        ];

        return $this->repository->getRow($query);
    }

    private function loadAll()
    {
        $fields = [
            'EXTERNAL_CATEGORY_ID',
            'INTERNAL_CATEGORY_ID'
        ];

        $filter = [
            'PARTNER_ID' => $this->partnerShop->getId(),
            '!INTERNAL_CATEGORY_ID' => null,
        ];

        $queryResult = $this->repository->getList([
            'select' => $fields,
            'filter' => $filter
        ]);
        $mappings = [];

        while ($row = $queryResult->fetch()) {
            $mappings[$row['EXTERNAL_CATEGORY_ID']] = $row['INTERNAL_CATEGORY_ID'];
        }

        return $mappings;
    }
}