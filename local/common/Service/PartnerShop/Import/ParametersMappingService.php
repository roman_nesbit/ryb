<?php


namespace Rybalka\Marketplace\Service\PartnerShop\Import;


use Monolog\Logger;
use Rybalka\Marketplace\Model\PartnerShop\Import\ExternalCatalogItemParameter;
use Rybalka\Marketplace\Model\PartnerShop\Import\InternalCatalogItemParameter;
use Rybalka\Marketplace\Repository\PartnerShop\Import\InternalCatalogItemParametersRepository;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\PartnerShop\Import\MeasurementTypeUtil;
use Rybalka\Marketplace\Util\PartnerShop\Import\NormalizationUtil;
use Rybalka\Marketplace\Util\PartnerShop\Import\PropertiesUtil;

class ParametersMappingService
{
    /**
     * @var InternalCatalogItemParametersRepository
     */
    private $internalParamsRepository;

    /**
     * @var MeasurementTypeUtil
     */
    private $measurementTypeUtil;

    /**
     * @var PropertiesUtil
     */
    private $propertiesUtil;

    /**
     * @var NormalizationUtil
     */
    private $normalizationUtil;

    private $propertiesValuesCache = [];

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?? (new LoggerFactory())->getLogger('parameters-mapping');

        $this->internalParamsRepository = new InternalCatalogItemParametersRepository();
        $this->measurementTypeUtil = new MeasurementTypeUtil();
        $this->propertiesUtil = new PropertiesUtil();
        $this->normalizationUtil = new NormalizationUtil();
    }

    public function getInternalValueFor(ExternalCatalogItemParameter $externalParameter, string $originalValue)
    {
        if (is_null($externalParameter->getInternalCode())) {
            return null;
        }

        $internalParameter = $this->internalParamsRepository->getByCode(
            $externalParameter->getInternalCode()
        );

        if (is_null($internalParameter)) {
            return null;
        }

        switch ($internalParameter->getType()) {
            case 'S': // string value
                return $originalValue;
            case 'N':
                return $this->convertNumberValue($externalParameter, $internalParameter, $originalValue);
            case 'L':
                return $this->getListValue($internalParameter, $originalValue);
            default:
                return null;
        }
    }

    private function getListValue(
        InternalCatalogItemParameter $internal,
        string $originalValue
    ) {
        $internalValues = $this->getPropertyValuesFromCacheOrLoad($internal);
        $value = $this->normalizationUtil->listPropertyValue($originalValue);

        if (!$internal->isMultiValue()) {
            return $internalValues[$value] ?? null;
        }


        $values = explode('<br/>', $originalValue);
        $results = [];

        foreach ($values as $currentValue) {
            $normalizedValue = $this->normalizationUtil->listPropertyValue($currentValue);

            if (!isset($internalValues[$normalizedValue])) {
                $this->logger->warning('Unknown value for multi-value parameter', [
                    'code' => $internal->getCode(),
                    'name' => $internal->getName(),
                    'value' => $currentValue
                ]);

                continue;
            }

            $results[] = $internalValues[$normalizedValue];
        }

        return $results;
    }

    private function getPropertyValuesFromCacheOrLoad(InternalCatalogItemParameter $internal)
    {
        $key = $internal->getCode();

        if (!isset($this->propertiesValuesCache[$key])) {
            $values = $this->propertiesUtil->getListPropertyValues($internal->getId());
            $valuesForCache = [];

            foreach ($values as $propertyValueId => $propertyValue) {
                $normalizedName = $this->normalizationUtil->listPropertyValue($propertyValue['NAME']);
                $valuesForCache[$normalizedName] = $propertyValueId;
            }

            $this->propertiesValuesCache[$key] = $valuesForCache;
        }

        return $this->propertiesValuesCache[$key];
    }

    private function convertNumberValue(
        ExternalCatalogItemParameter $externalParameter,
        InternalCatalogItemParameter $internalParameter,
        string $originalValue
    ) {
        $internalValueUnit = $this->extractUnitsFrom($internalParameter->getName());
        $value = $this->extractNumberValueFrom($originalValue);

        if (is_null($internalValueUnit) || is_null($value)) {
            return $value;
        }

        $externalValueUnit = $this->extractUnitsFrom($externalParameter->getName())
            ?? $this->extractUnitsFrom($originalValue);

        if (is_null($externalValueUnit)) {
            return $value;
        }

        if (!$this->measurementTypeUtil->isTheSameType($internalValueUnit, $externalValueUnit)) {
            return null;
        }

        $internalValueUnit = $this->measurementTypeUtil->getInternationalNotationFor($internalValueUnit);
        $externalValue = $this->measurementTypeUtil->create(
            $value,
            $externalValueUnit
        );

        return $externalValue->toUnit($internalValueUnit);
    }

    private function extractNumberValueFrom(string $stringValue)
    {
        $values = explode(' ', trim($stringValue));

        if (empty($values[0])) {
            return 0;
        }

        $rawValue = $values[0];

        $lastCommaPosition = strrpos($rawValue, ',');
        $lastDotPosition = strrpos($rawValue, '.');

        if ($lastDotPosition === false && $lastCommaPosition === false) {
            return floatval($rawValue);
        }

        if ($lastCommaPosition === 0 && $lastDotPosition === false) {
            $rawValue = str_replace(',', '.', $rawValue);
            return floatval($rawValue);
        }

        $decimalChar = ($lastCommaPosition > $lastDotPosition ? ',' : '.');
        $thousandsChar = ($decimalChar === '.' ? ',' : '.');

        if (substr_count($rawValue, ',') > 1) {
            $decimalChar = '.';
            $thousandsChar = ',';
        } else if (substr_count($rawValue, '.') > 1) {
            $decimalChar = ',';
            $thousandsChar = '.';
        }

        $rawValue = str_replace($thousandsChar, '', $rawValue);
        $rawValue = str_replace($decimalChar, '.', $rawValue);

        return floatval($rawValue);
    }

    private function extractUnitsFrom(string $name): ?string
    {
        $name = trim($name);
        if (preg_match('/\s+(.{1,4})$/', $name, $matches)) {
            return $matches[1];
        }

        return null;
    }
}