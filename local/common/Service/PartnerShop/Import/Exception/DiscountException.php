<?php


namespace Rybalka\Marketplace\Service\PartnerShop\Import\Exception;


use Exception;

class DiscountException extends Exception
{
    /**
     * @var array
     */
    private $params;

    public function __construct($message = "", array $params = [])
    {
        parent::__construct($message);

        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}