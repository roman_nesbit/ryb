<?php


namespace Rybalka\Marketplace\Service\PartnerShop\Import;


use Bitrix\Main\Loader;
use CSaleDiscount;
use Exception;
use Monolog\Logger;
use Rybalka\Marketplace\Model\Bitrix\MarketplaceConstants;
use Rybalka\Marketplace\Service\PartnerShop\Import\Exception\DiscountException;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\PartnerShop\Import\DiscountsUtil;

class DiscountsService
{
    const SORT_START = 1000;
    const IMPORT_DISCOUNT_PRIORITY = 10;

    const MAX_DISCOUNT_RATE = 95;
    const MIN_DISCOUNT_RATE = 0;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var CSaleDiscount
     */
    private $discountsRepository;

    /**
     * @var DiscountsUtil
     */
    private $discountsUtil;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?? (new LoggerFactory())->getLogger('discounts');
        Loader::includeModule('sale');

        $this->discountsRepository = new CSaleDiscount();
        $this->discountsUtil = new DiscountsUtil();
    }

    public function addDiscountFor(int $offerId, int $discountRate): bool
    {
        return $this->runAndHandleExceptionsIfAny(function() use ($offerId, $discountRate) {

            $this->assertValidDiscountRate($discountRate);
            $discount = $this->loadCatalogDiscountWith($discountRate);

            if (empty($discount)) {
                $this->createCatalogDiscountWith($discountRate, $offerId);
                return true;
            }

            $this->addOfferToDiscount($offerId, $discount);
            return true;

        }, false);
    }

    public function removeDiscountFor(int $offerId, int $discountRate): bool
    {
        return $this->runAndHandleExceptionsIfAny(function () use ($offerId, $discountRate) {

            $this->assertValidDiscountRate($discountRate);

            $discountName = $this->getRuleNameFor($discountRate);
            $discount = $this->loadDiscountByName($discountName);

            if (empty($discount)) {
                $this->logger->warning('Offer was not removed from the discount as discount does not exist', [
                    'name' => $discountName,
                    'offerId' => $offerId,
                ]);

                return true;
            }

            $this->removeOfferFromDiscount($offerId, $discount);

            return true;
        }, false);
    }

    private function addOfferToDiscount(int $offerId, array $discount): bool
    {
        $discountId = $discount['ID'] ?? null;
        $discountName = $discount['NAME'] ?? null;

        $offerIds = $this->extractOfferIdsFromDiscount($discount) ?? [];

        if (empty($offerIds)) {
            $this->logger->error('Discount does not contain any product', [
                'id' => $discountId,
                'name' => $discountName,
            ]);
        }

        $offerIds[] = $offerId;
        $offerIds = array_unique($offerIds);

        $discountRate = $this->extractDiscountRateFromDiscount($discount);

        if (is_null($discountRate)) {
            throw new DiscountException('Could not extract discount rate from the discount', [
                'operation' => 'addOfferToDiscount',
                'id' => $discountId,
                'name' => $discountName,
                'actions' => $discount['ACTIONS'] ?? null,
            ]);
        }

        $updateResult = $this->discountsRepository->Update(
            $discountId,
            [
                'ACTIONS' => $this->createDiscountActionFor($offerIds, $discountRate)
            ]
        );

        if (empty($updateResult)) {
            throw new DiscountException('Failed to add offerId to the discount', [
                'id' => $discountId,
                'name' => $discountName,
                'offerId' => $offerId
            ]);
        }

        return $discount['ID'];
    }

    private function removeOfferFromDiscount(int $offerId, array $discount)
    {
        $offerIds = $this->extractOfferIdsFromDiscount($discount) ?? [];
        $discountRate = $this->extractDiscountRateFromDiscount($discount);

        $discountId = $discount['ID'];
        $discountName = $discount['NAME'];

        if (is_null($discountRate)) {

            throw new DiscountException('Could not extract discount rate from the discount', [
                'operation' => 'removeOfferFromDiscount',
                'id' => $discountId,
                'name' => $discountName,
                'actions' => $discount['ACTIONS'] ?? null
            ]);
        }

        $offerIds = array_filter($offerIds, function($currentValue) use ($offerId) {
            return $currentValue != $offerId;
        });

        if (empty($offerIds)) {
            if (!$this->discountsRepository->Delete($discount['ID'])) {
                throw new DiscountException('Failed to delete discount', [
                    'id' => $discountId,
                    'name' => $discountName,
                    'offerId' => $offerId
                ]);
            }

            return;
        }

        $updateResult = $this->discountsRepository->Update(
            $discountId,
            [
                'ACTIONS' => $this->createDiscountActionFor($offerIds, $discountRate)
            ]
        );

        if (!$updateResult) {
            throw new DiscountException('Failed to remove offerId from discount', [
                'id' => $discountId,
                'name' => $discountName,
                'offerId' => $offerId,
                'offerIdsInDiscount' => $offerIds
            ]);
        }
    }

    private function getRuleNameFor(int $discountRate): string
    {
        return 'Импорт товаров: ' . $discountRate . '% скидка';
    }

    private function loadCatalogDiscountWith(int $discountRate): ?array
    {
        $ruleName = $this->getRuleNameFor($discountRate);
        return $this->loadDiscountByName($ruleName);
    }

    private function createCatalogDiscountWith(int $discountRate, int $offerId): ?int
    {
        $ruleName = $this->getRuleNameFor($discountRate);

        $discountSettings = [
            'LID' => MarketplaceConstants::SITE_ID,
            'NAME' => $ruleName,
            'LAST_DISCOUNT' => 'N',
            'ACTIVE' => 'Y',
            'SORT' => self::SORT_START + $discountRate,
            'CURRENCY' => MarketplaceConstants::DEFAULT_CURRENCY,
            'PRIORITY' => self::IMPORT_DISCOUNT_PRIORITY,
            'USER_GROUPS' => [MarketplaceConstants::USER_GROUP_ALL_USERS],
            'CONDITIONS' => $this->createBlankDiscountConditions(),
            'ACTIONS' => $this->createDiscountActionFor([$offerId], $discountRate),
        ];

        $result = $this->discountsRepository->Add($discountSettings);

        if ($result == false) {
            throw new DiscountException('Failed to create discount rate', [
                'name' => $ruleName,
                'rate' => $discountRate
            ]);
        }

        return $result;
    }

    private function loadDiscountByName(string $name)
    {
        $filter = [
            'NAME' => $name
        ];

        $fieldsToSelect = [
            'ID',
            'NAME',
            'ACTIVE',
            'CURRENCY',
            'CONDITIONS',
            'ACTIONS',
            'DISCOUNT_VALUE',
            'DISCOUNT_TYPE'
        ];

        $queryResult = $this->discountsRepository->GetList([], $filter, false, false, $fieldsToSelect);
        $recordsCount = 0;

        $result = null;

        while ($record = $queryResult->Fetch()) {
            $result = $record;
            $recordsCount++;
        }

        if ($recordsCount > 1) {
            $this->logger->error('There are multiple discounts with the same name', [
                'name' => $name
            ]);
        }

        return $result;
    }

    private function extractDiscountRateFromDiscount(array $discount): ?float
    {
        $actions = unserialize($discount['ACTIONS']);

        $saleBasketGroupNode = $this->findChildByClassId(
            'ActSaleBsktGrp',
            $actions['CHILDREN'] ?? []
        );

        if (is_null($saleBasketGroupNode)) {
            return null;
        }

        return !is_null($saleBasketGroupNode['DATA']['Value'])
            ? (float)$saleBasketGroupNode['DATA']['Value']
            : null;
    }

    private function extractOfferIdsFromDiscount(array $discount): ?array
    {
        $actions = unserialize($discount['ACTIONS']);

        $saleBasketGroupNode = $this->findChildByClassId(
            'ActSaleBsktGrp',
            $actions['CHILDREN'] ?? []
        );

        if (is_null($saleBasketGroupNode)) {
            return null;
        }

        $conditionNode = $this->findChildByClassId(
            'CondIBElement',
            $saleBasketGroupNode['CHILDREN'] ?? []
        );

        return $conditionNode['DATA']['value'] ?? null;
    }

    private function findChildByClassId(string $classId, array $children): ?array
    {
        foreach ($children as $child) {
            $currentClassId = $child['CLASS_ID'] ?? null;

            if ($classId != $currentClassId) {
                continue;
            }

            return $child;
        }

        return null;
    }

    private function createDiscountActionFor(array $offerIds, float $discount)
    {
        return [
            'CLASS_ID' => 'CondGroup',
            'DATA' => [
                'All' => 'AND',
            ],
            'CHILDREN' => [
                [
                    'CLASS_ID' => 'ActSaleBsktGrp',
                    'DATA' => [
                        'Type' => 'Discount',
                        'Value' => $discount,
                        'Unit' => 'Perc',
                        'Max' => 0,
                        'All' => 'AND',
                        'True' => 'True',
                    ],
                    'CHILDREN' => [
                        [
                            'CLASS_ID' => 'CondIBElement',
                            'DATA' => [
                                'logic' => 'Equal',
                                'value' => $offerIds
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    private function createBlankDiscountConditions()
    {
        return [
            'CLASS_ID' => 'CondGroup',
            'DATA' => [
                'All' => 'AND',
                'True' => 'True',
            ],
            'CHILDREN' => []
        ];
    }

    private function assertValidDiscountRate(int $discountRate)
    {
        if ($discountRate < self::MIN_DISCOUNT_RATE || $discountRate > self::MAX_DISCOUNT_RATE) {
            throw new DiscountException('Bad discount rate', [
                'rate' => $discountRate
            ]);
        }
    }

    private function runAndHandleExceptionsIfAny(callable $code, $returnValueOnFailure = null)
    {
        try {
            return $code();
        } catch (DiscountException $exception) {
            $this->logger->error($exception->getMessage(), $exception->getParams());
            return $returnValueOnFailure;
        } catch (Exception $exception) {
            $this->logger->error(
                'Unexpected exception when working with discounts: ' . $exception->getMessage(),
                [
                    'exception' => $exception
                ]
            );
            return $returnValueOnFailure;
        }
    }
}