<?php


namespace Rybalka\Marketplace\Service;


use InvalidArgumentException;
use Rybalka\Marketplace\Model\PartnerShop;
use Rybalka\Marketplace\Repository\PartnerShopRepository;

class PartnerShopService
{
    /**
     * @var PartnerShopRepository
     */
    private $repository;

    public function __construct()
    {
        $this->repository = new PartnerShopRepository();
    }

    public function loadByIdOrFail(int $partnerId): PartnerShop
    {
        $rawData = $this->repository->loadById($partnerId);

        if ($rawData == null) {
            throw new InvalidArgumentException('Unable to find a partner shop with id: ' . $partnerId);
        }

        return PartnerShop::fromRawData($rawData);
    }
}