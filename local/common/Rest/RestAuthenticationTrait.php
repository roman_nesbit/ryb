<?php


namespace Rybalka\Marketplace\Rest;


use Rybalka\Marketplace\Model\ShopModerator;

trait RestAuthenticationTrait
{
    private $moderator;

    public function getModerator(): ?ShopModerator
    {
        if (is_null($this->moderator)) {
            $this->moderator = ShopModerator::fromRestSession(
                request()->get('_user')
            );
        }

        return $this->moderator;
    }
}