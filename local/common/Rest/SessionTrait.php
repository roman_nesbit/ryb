<?php


namespace Rybalka\Marketplace\Rest;


use Bitrix\Main\Loader;
use CSecuritySessionDB;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Http\Exception\UnauthorizedException;

trait SessionTrait
{
    public function restoreSessionOrFail() {

        $sessionCookieName = session_name();

        if (!$this->cookies()->has($sessionCookieName)) {
            throw new BadRequestException('Session cookie must be passed');
        }

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        Loader::includeModule('security');
        $serializedSessionData = CSecuritySessionDB::read($this->cookies()->get($sessionCookieName));

        if (empty($serializedSessionData)) {
            throw new BadRequestException('Bad session identifier');
        }

        $isSuccess = session_decode($serializedSessionData);

        if (!$isSuccess) {
            throw new InternalServerErrorException();
        }
    }

    public function assertAuthenticatedBySession()
    {
        $this->restoreSessionOrFail();
        $bitrixUserData = $_SESSION['SESS_AUTH'] ?? [];

        if(!isset($bitrixUserData['AUTHORIZED']) || $bitrixUserData['AUTHORIZED'] == 'N')
        {
            throw new UnauthorizedException();
        }
    }
}