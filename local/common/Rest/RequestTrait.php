<?php

namespace Rybalka\Marketplace\Rest;

use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Rest\Exception\MalformedJsonBodyException;
use Rybalka\Marketplace\Util\ArrayUtil;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

trait RequestTrait
{
    private $requestObject;
    private $jsonBody;

    public function query(): ParameterBag
    {
        return $this->request()->query;
    }

    public function post(): ParameterBag
    {
        return $this->request()->request;
    }

    public function cookies(): ParameterBag
    {
        return $this->request()->cookies;
    }

    public function headers(): HeaderBag
    {
        return $this->request()->headers;
    }

    public function json($path = '')
    {
        $this->parseJsonRequestIfNotParsed();
        $jsonData = $this->jsonBody;

        return (new ArrayUtil())->get($jsonData, $path);
    }

    public function assertJsonParams(...$paths) {

        foreach ($paths as $paramPath) {
            $value = $this->json($paramPath);

            if ($value == null) {
                throw new BadRequestException("Expected json body field $paramPath is not passed");
            }
        }
    }

    public function assertPostParams(...$requiredParams)
    {
        foreach ($requiredParams as $paramName) {
            if (!$this->post()->has($paramName)) {
                throw new BadRequestException("Expected POST parameter $paramName is not passed");
            }
        }
    }

    public function assertGetParams(...$requiredParams)
    {
        foreach ($requiredParams as $paramName) {
            if (!$this->query()->has($paramName)) {
                throw new BadRequestException("Expected GET parameter $paramName is not passed");
            }
        }
    }

    private function request(): Request
    {
        if ($this->requestObject == null) {
            $this->requestObject = Request::createFromGlobals();
        }

        return $this->requestObject;
    }

    private function parseJsonRequestIfNotParsed()
    {
        if ($this->jsonBody != null) {
            return;
        }

        $contentType = trim($this->headers()->get('CONTENT_TYPE'));

        if ($contentType == 'application/json') {
            $requestBody = $this->requestObject->getContent();

            $decodedJson = json_decode($requestBody, true);
            if (is_null($decodedJson)) {
                throw new MalformedJsonBodyException();
            }

            $this->jsonBody = $decodedJson;
        }
    }
}