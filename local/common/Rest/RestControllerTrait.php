<?php

namespace Rybalka\Marketplace\Rest;

use Exception;
use Rybalka\Marketplace\Exception\IllegalFormatException;
use Rybalka\Marketplace\Exception\ValidationException;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Http\Exception\HttpException;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Http\Exception\NotFoundException;
use Rybalka\Marketplace\Http\StatusCode;
use Rybalka\Marketplace\Util\StringsUtils;
use Throwable;

trait RestControllerTrait
{
    use RequestTrait;
    use RestAuthenticationTrait;
    use SessionTrait;

    public function legacyRestResponse(callable $controller) {
        try {
            $responseData = $controller() ?? [];

            if (!is_array($responseData)) {
                throw new IllegalFormatException('Controller is expected to return array with response data');
            }

            $responseData['status'] = StatusCode::OK;
            response()->json($responseData, StatusCode::OK, JSON_UNESCAPED_UNICODE);

        } catch (Exception $exception) {
            $httpException = $this->toHttpException($exception);

            $response = [
                'status' => $httpException->getHttpCode(),
                'message' => $httpException->getMessage()
            ];

            response()->json($response, $httpException->getHttpCode(), JSON_UNESCAPED_UNICODE);
        }
    }

    public function restResponse(callable $controller) {

        $httpCode = StatusCode::OK;

        try {
            $data = $controller() ?? [];

            if (!is_array($data)) {
                throw new IllegalFormatException('Controller is expected to return array with response data');
            }

            $response = [
                'meta' => [
                    'status' => StatusCode::OK,
                ]
            ];

            $response['data'] = !empty($data)
                ? $data
                : [];

        } catch (Exception $exception) {
            $httpException = $this->toHttpException($exception);

            $meta = [
                'status'  => $httpException->getHttpCode(),
                'error' => $httpException->getMessage()
            ];

            $userError = $this->buildUserErrorFromException($httpException);

            if (!empty($userError)) {
                $meta['userError'] = $userError;
            }

            $response = [
                'meta' => $meta
            ];

            $data = $httpException->getResponseBody();

            if(!empty($data)) {
                $response['data'] = $data;
            }

            $httpCode = $httpException->getHttpCode();
        }

        response()->json($response ?? [], $httpCode, JSON_UNESCAPED_UNICODE);
    }

    private function toHttpException(Throwable $exception) {
        if ($exception instanceof HttpException) {
            return $exception;
        } elseif ($exception instanceof ValidationException) {
            return (new BadRequestException(
                $exception->getMessage()
            ))->withFieldError(
                $exception->getFieldName(),
                $exception->getUserErrorMessage()
            )->withUserErrorMessage('Ошибка валидации данных');
        }

        return new InternalServerErrorException();
    }

    private function buildUserErrorFromException(HttpException $httpException): array
    {
        $result = [];

        $userError = $httpException->getUserErrorMessage();

        if (!empty($userError)) {
            $result['general'] = $userError;
        }

        if ($httpException->getFieldErrors()) {

            $result['fields'] = [];

            foreach ($httpException->getFieldErrors() as $key => $value) {
                $result['fields'][] = [
                    'name' => $key,
                    'message' => $value
                ];
            }
        }

        return $result;
    }
}