<?php

namespace Rybalka\Marketplace\Agent;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AgentTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> MODULE_ID string(50) optional
 * <li> SORT int optional default 100
 * <li> NAME string optional
 * <li> ACTIVE bool optional default 'Y'
 * <li> LAST_EXEC datetime optional
 * <li> NEXT_EXEC datetime mandatory
 * <li> DATE_CHECK datetime optional
 * <li> AGENT_INTERVAL int optional default 86400
 * <li> IS_PERIOD bool optional default 'Y'
 * <li> USER_ID int optional
 * <li> RUNNING bool optional default 'N'
 * </ul>
 *
 * @package Bitrix\Agent
 **/

class AgentTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_agent';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('AGENT_ENTITY_ID_FIELD'),
			),
			'MODULE_ID' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateModuleId'),
				'title' => Loc::getMessage('AGENT_ENTITY_MODULE_ID_FIELD'),
			),
			'SORT' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('AGENT_ENTITY_SORT_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('AGENT_ENTITY_NAME_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('AGENT_ENTITY_ACTIVE_FIELD'),
			),
			'LAST_EXEC' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('AGENT_ENTITY_LAST_EXEC_FIELD'),
			),
			'NEXT_EXEC' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('AGENT_ENTITY_NEXT_EXEC_FIELD'),
			),
			'DATE_CHECK' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('AGENT_ENTITY_DATE_CHECK_FIELD'),
			),
			'AGENT_INTERVAL' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('AGENT_ENTITY_AGENT_INTERVAL_FIELD'),
			),
			'IS_PERIOD' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('AGENT_ENTITY_IS_PERIOD_FIELD'),
			),
			'USER_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('AGENT_ENTITY_USER_ID_FIELD'),
			),
			'RUNNING' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('AGENT_ENTITY_RUNNING_FIELD'),
			),
		);
	}
	/**
	 * Returns validators for MODULE_ID field.
	 *
	 * @return array
	 */
	public static function validateModuleId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}
}
