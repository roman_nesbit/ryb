<?php


namespace Rybalka\Marketplace\Util;


interface Sanitizer
{
    public function sanitize($value);
}