<?php

namespace Rybalka\Marketplace\Util;

use \Bitrix\Main\UserTable;
use CUser;

class UserUtil
{
	public static function isUserByPhone($phoneNumber)
	{
		return isset(self::getUserDataByPhoneNumber($phoneNumber)['ID']);
	}

	public static function isUserByEmail($email)
	{
		$result = UserTable::getList([
			'select' => ['ID'],
			'filter' => ['EMAIL' => $email]
		]);
		$record = $result->fetch();

		return isset($record['ID']);
	}

	public static function getUserDataByEmail($email)
	{
		$result = UserTable::getList([
            'select' => ['ID', 'LOGIN', 'EMAIL'],
            'filter' => ['EMAIL' => $email]
        ]);
        $record = $result->fetch();

        return isset($record['ID']) ? $record : false;
	}

	public static function getUserDataByPhoneNumber(string $phoneNumber)
    {
        $result = UserTable::getList([
            'select' => ['ID', 'LOGIN', 'PASSWORD'],
            'filter' => ['PERSONAL_PHONE' => $phoneNumber]
        ]);
        $record = $result->fetch();

        return $record;
    }

	public static function sendPassword($login, $email)
	{
		global $USER;
		$res = $USER->SendPassword($login, $email);
		return $res["TYPE"] == "OK";
	}

	public static function setNewPassword($login, $checkWord, $password, $confirmPassword)
	{
		global $USER;
		$arResult = $USER->ChangePassword($login, $checkWord, $password, $confirmPassword);
		return $arResult["TYPE"] != "OK" ? ShowMessage($arResult) : true;
	}

	public static function setNewPasswordByPhone($phone, $newPassword, $confirmNewPassword)
	{
		$result = UserTable::getList([
			'select' => ['ID'],
			'filter' => ['PERSONAL_PHONE' => $phone]
		]);
		if($record = $result->fetch())
		{
			return static::changeUserPassword($record['ID'], $newPassword, $confirmNewPassword);
		} 
		else
		{
			return false;
		}
	}

	public static function getGroupPolicy()
	{
		return \CUser::GetGroupPolicy([2])['PASSWORD_REQUIREMENTS'];
	}

	public static function checkCurrentUserPassword($password)
	{
		$login = CUser::GetLogin();
		$user = new \CUser;
		return $user->Login($login, $password, 'N', 'Y') == 'Y';
	}

	public static function changeUserPassword($userId, $newPassword, $confirmNewPassword)
	{
		return static::changeUserData($userId, [
			'PASSWORD' => $newPassword,
			'CONFIRM_PASSWORD' => $confirmNewPassword
		]);
	}

	public static function changeCurrentUserPassword($newPassword, $confirmNewPassword)
	{
		$userId = CUser::GetId();

		return static::changeUserPassword($userId, $newPassword, $confirmNewPassword);
	}

	public static function changeCurrentUserData($arData)
	{
		$userId = CUser::GetId();

		return static::changeUserData($userId, $arData);
	}

	private static function changeUserData($userId, $arData)
	{
		$user = new \CUser;
		if($user->Update($userId, $arData))
		{
			return true;
		}
		else
		{
			return $user->LAST_ERROR;
		}
	}
}