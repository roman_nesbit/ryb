<?php


namespace Rybalka\Marketplace\Util\System;


class Timer
{
    /**
     * @var float|string
     */
    private $beginning;

    private function __construct()
    {
        $this->beginning = microtime(true);
    }

    public static function create()
    {
        return new Timer();
    }

    public function elapsedTime(int $precision = 2): float
    {
        return round(microtime(true) - $this->beginning, $precision);
    }
}