<?php


namespace Rybalka\Marketplace\Util;


use Monolog\Logger;

class FeatureToggle
{
    const TEST_MODE_PHONE_VERIFICATION = 'TEST_MODE_PHONE_VERIFICATION';
    const TEST_MODE_RECAPTCHA = 'TEST_MODE_RECAPTCHA';
    const TEST_MODE_TEST_MODE_SMS = 'TEST_MODE_SMS';

    const CATALOG_IMPORT = 'CATALOG_IMPORT';
    const CATALOG_IMPORT_NO_ITEM_CHANGE_CHECK = 'CATALOG_IMPORT_NO_ITEM_CHANGE_CHECK';

    const TOGGLE_PREFIX = 'APP_TOGGLE_';

    private static $instance;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct()
    {
        $this->logger = (new LoggerFactory())->getLogger('toggles');
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new FeatureToggle();
        }

        return self::$instance;
    }

    public function isPhoneVerificationTestModeOn()
    {
        return $this->isOn(self::TEST_MODE_PHONE_VERIFICATION);
    }

    public function isRecapchaTestModeOn()
    {
        return $this->isOn(self::TEST_MODE_RECAPTCHA);
    }

    public function isSmsTestModeOn()
    {
        return $this->isOn(self::TEST_MODE_TEST_MODE_SMS);
    }

    public function isCatalogImportOn()
    {
        return $this->isOn(self::CATALOG_IMPORT);
    }

    public function isCatalogImportItemChangeCheckDisabled()
    {
        return $this->isOn(self::CATALOG_IMPORT_NO_ITEM_CHANGE_CHECK);
    }

    public function isUrlsShorteningEnabled(): bool
    {
        return $this->isOn('USE_SHORT_URLS');
    }

    public function isOn(string $featureName): bool
    {
        $toggleName = self::TOGGLE_PREFIX . $featureName;
        $envValue = getenv($toggleName);

        if ($envValue === false) {
            $this->logger->warning('No environment variable with such name', [
                'name' => $toggleName
            ]);

            return false;
        }

        return $envValue == '1' || $envValue == 'true';
    }
}