<?php

namespace Rybalka\Marketplace\Util;

use \Bitrix\Main\Config\Option;

class Recaptcha
{
	public static function isHuman($recaptchaToken)
	{
	    if (FeatureToggle::getInstance()->isRecapchaTestModeOn())
		{
			return true;
		}

		$result = false;

		$recaptchaUrl = Option::get("grain.customsettings", "RECAPTCHA_URL");
		$recaptchaSecretKey = getenv('RECAPTCHA_SECRET_KEY');

		$recaptcha = file_get_contents($recaptchaUrl . '?secret=' . $recaptchaSecretKey . '&response=' . $recaptchaToken);
		$recaptcha = json_decode($recaptcha);

		// exo($recaptcha);
		if($recaptcha->success)
		{
			$maxHumanScore = (float)Option::get("grain.customsettings", "RECAPTCHA_MAX_HUMAN_SCORE");
			$result = $recaptcha->score >= $maxHumanScore;
		}

		return $result;
	}
}