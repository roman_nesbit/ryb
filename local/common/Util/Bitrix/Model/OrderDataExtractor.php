<?php

namespace Rybalka\Marketplace\Util\Bitrix\Model;

use Bitrix\Sale\Order;
use Bitrix\Sale\Property;
use Bitrix\Sale\PropertyValue;
use Bitrix\Sale\Shipment;
use Throwable;

class OrderDataExtractor
{
    const FIELD_PROPERTY_NAME  = 'CODE';
    const FIELD_PROPERTY_VALUE = 'VALUE';

    public function getProperty(Order $order, string $propertyCode): ?PropertyValue
    {
        foreach ($order->getPropertyCollection() as $property) {
            /**
             * @var Property $property
             */
            if ($property->getField(self::FIELD_PROPERTY_NAME) == $propertyCode) {
                return $property;
            }
        }

        return null;
    }

    public function getPropertyValueOrFail(Order $order, string $propertyCode, Throwable $throwable)
    {
        $property = $this->getProperty($order, $propertyCode);

        if (is_null($property)) {
            throw $throwable;
        }

        return $property->getField(self::FIELD_PROPERTY_VALUE);
    }

    public function getDeliveryMethodName(Order $order)
    {
        $shipments = $order->getShipmentCollection();
        foreach ($shipments as $shipment) {

            /**
             * @var Shipment $shipment
             */
            if (!$shipment->isSystem()) {
                return $shipment->getField('DELIVERY_NAME');
            }
        }

        return null;
    }
}