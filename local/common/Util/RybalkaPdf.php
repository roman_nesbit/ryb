<?

namespace Rybalka\Marketplace\Util;

use TCPDF;

class RybalkaPdf extends TCPDF
{
	const PDF_PAGE_FORMAT = 'A4';
	const PDF_PAGE_WIDTH = 210;
	const LOGO_FILE = '/local/templates/rybalka/images/logo.png';

	public function Header()
	{
		$this->Rect(0, 0, self::PDF_PAGE_WIDTH, 23, 'F', [], [0, 30, 65]);
		$logoPath = $_SERVER['DOCUMENT_ROOT'] . self::LOGO_FILE;
		$this->Image($logoPath, PDF_MARGIN_LEFT, 8, 50, 9, 'PNG', '', 'T', true);
	}
}