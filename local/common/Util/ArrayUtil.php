<?php

namespace Rybalka\Marketplace\Util;

use Throwable;

class ArrayUtil
{
    public function allKeysSet(array $data, ...$keys): bool
    {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $data)) {
                return false;
            }
        }

        return true;
    }

    public function set(array &$data, string $key, $value)
    {
        $pathParts = explode('.', $key);

        while (!empty($pathParts)) {
            $nextPathPart = array_shift($pathParts);

            if (!is_array($data)) {
                $data = [];
            }

            $isLastElement = empty($pathParts);

            if ($isLastElement) {
                $data[$nextPathPart] = $value;
                continue;
            }

            if (!is_array($data[$nextPathPart])) {
                $data[$nextPathPart] = [];

            }

            $data = &$data[$nextPathPart];
        }
    }

    public function get(array $data, string $key)
    {
        $pathParts = explode('.', $key);

        while (!empty($pathParts)) {
            $nextPathPart = array_shift($pathParts);

            if (empty($nextPathPart) && empty($pathParts)) {
                return $data;
            }

            if (!isset($data[$nextPathPart])) {
                return null;
            }

            $data = $data[$nextPathPart];
        }

        return $data;
    }
    public function getIntOrDefault($array, $key, int $defaultValue) {
        if (!is_array($array)) {
            return $defaultValue;
        }

        return (int)($array[$key] ?? $defaultValue);
    }

    public function getIntOrNull($array, $key): ?int
    {
        return isset($array[$key]) ? (int)($array[$key]) : null;
    }

    public function getIntOrException($array, $key, Throwable $exception) {
        if (!is_array($array)) {
            throw $exception;
        }

        if (isset($array[$key])) {
            return (int)($array[$key]);
        }

        throw $exception;
    }

    public function getOrNull($array, $key)
    {
        return $array[$key] ?? null;
    }
}