<?php


namespace Rybalka\Marketplace\Util;


class StringsUtils
{
    const DEFAULT_ENCODING = 'utf-8';

    private static $instance;

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new StringsUtils();
        }

        return self::$instance;
    }

    public function startsWith(string $what, string $where): bool
    {
        return strpos($where, $what) === 0;
    }

    public function endsWith(string $what, string $where): bool
    {
        $length = strlen($what);

        if ($length == 0) {
            return true;
        }

        return substr($where, -$length) === $what;
    }

    public function explode(string $delimiter, string $value): array
    {
        $values = explode($delimiter, $value);
        $values = array_map(function($value) {
            return trim($value);
        }, $values);

        return array_filter($values, function ($value) {
            return !empty($value);
        });
    }

    public function lcFirst(string $string)
    {
        $length = mb_strlen($string, self::DEFAULT_ENCODING);

        $firstChar = mb_substr($string, 0, 1, self::DEFAULT_ENCODING);
        $restOfTheString = mb_substr($string, 1, $length - 1, self::DEFAULT_ENCODING);

        return mb_strtolower($firstChar, self::DEFAULT_ENCODING) . $restOfTheString;
    }

    public function ucFirst(string $string)
    {
        $length = mb_strlen($string, self::DEFAULT_ENCODING);

        $firstChar = mb_substr($string, 0, 1, self::DEFAULT_ENCODING);
        $restOfTheString = mb_substr($string, 1, $length - 1, self::DEFAULT_ENCODING);

        return mb_strtoupper($firstChar, self::DEFAULT_ENCODING) . $restOfTheString;
    }
}