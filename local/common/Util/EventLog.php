<?

namespace Rybalka\Marketplace\Util;

class EventLog
{
	const SEVERITY_INFO = 'INFO';

	const TYPE_SEND_SMS = 'TYPE_SEND_SMS';

	// In case of new TYPE_... don't forget to register new handler for event OnEventLogGetAuditTypes in /local/php_interface/event_handlers.php

	public static function add($severity, $type, $description)
	{
		return \CEventLog::Add([
			'SEVERITY' => $severity,
			'AUDIT_TYPE_ID' => $type,
			'MODULE_ID' => 'main',
			'ITEM_ID' => __CLASS__,
			'DESCRIPTION' => $description,
		]);
	}

	public static function getType($type)
	{
		$_descriptions = [
			self::TYPE_SEND_SMS => 'Отправка СМС',
		];

		return [
			$type => '[' . $type . '] ' . $_descriptions[$type]
		];
	}
}