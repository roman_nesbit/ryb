<?php

namespace Rybalka\Marketplace\Util\DataStructure;


use ArrayIterator;
use Countable;
use IteratorAggregate;

class GenericCollection implements IteratorAggregate, Countable
{
    private $items = [];

    public function add($item)
    {
        $this->items[] = $item;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }
}