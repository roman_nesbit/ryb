<?php


namespace Rybalka\Marketplace\Util\DataStructure\Map;


use Countable;
use IteratorAggregate;
use Serializable;
use Throwable;

interface Map extends Serializable, IteratorAggregate, Countable
{
    public function get(string $key);
    public function getOrDefault(string $key, $defaultValue);
    public function getOrThrowException(string $key, Throwable $exception);

    public function add(string $key, $value): bool;
    public function remove(string $key, $value): bool;
    public function exist(string $key): bool;

    public function asArray(): array;
}