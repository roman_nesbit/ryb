<?php


namespace Rybalka\Marketplace\Util\DataStructure\Map\Decorator;


use Rybalka\Marketplace\Util\DataStructure\Map\Map;
use Rybalka\Marketplace\Util\Sanitizer;
use Throwable;

class SanitizationDecorator implements Map
{
    private $actualMap;

    /**
     * @var Sanitizer
     */
    private $keySanitizer;

    /**
     * @var Sanitizer
     */
    private $valueSanitizer;

    public function __construct(Map $map)
    {
        $this->actualMap = $map;
    }

    public function setKeySanitizer(Sanitizer $sanitizer): SanitizationDecorator
    {
        $this->keySanitizer = $sanitizer;
        return $this;
    }

    public function setValueSanitizer(Sanitizer $sanitizer): SanitizationDecorator
    {
        $this->valueSanitizer = $sanitizer;
        return $this;
    }

    public function getIterator()
    {
        return $this->actualMap->getIterator();
    }

    public function serialize()
    {
        return $this->actualMap->serialize();
    }

    public function unserialize($serialized)
    {
        return $this->actualMap->unserialize($serialized);
    }

    public function count()
    {
        return $this->actualMap->count();
    }

    public function get(string $key)
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        return $this->actualMap->get($key);
    }

    public function getOrDefault(string $key, $defaultValue)
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        return $this->actualMap->getOrDefault($key, $defaultValue);
    }

    public function getOrThrowException(string $key, Throwable $exception)
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        return $this->actualMap->getOrThrowException($key, $exception);
    }

    public function add(string $key, $value): bool
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        if (!is_null($this->valueSanitizer)) {
            $value = $this->valueSanitizer->sanitize($value);
        }

        return $this->actualMap->add($key, $value);
    }

    public function remove(string $key, $value): bool
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        $this->actualMap->remove($key);
    }

    public function exist(string $key): bool
    {
        if (!is_null($this->keySanitizer)) {
            $key = $this->keySanitizer->sanitize($key);
        }

        return $this->actualMap->exist($key);
    }

    public function asArray(): array
    {
        return $this->actualMap->asArray();
    }
}