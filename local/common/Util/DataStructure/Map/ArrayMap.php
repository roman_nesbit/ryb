<?php


namespace Rybalka\Marketplace\Util\DataStructure\Map;


use ArrayIterator;
use Throwable;

class ArrayMap implements Map
{
    private $data = [];

    public function get(string $key)
    {
        if ($this->exist($key)) {
            return $this->data[$key];
        }

        return null;
    }

    public function getOrDefault(string $key, $defaultValue)
    {
        if ($this->exist($key)) {
            return $this->data[$key];
        }

        return $defaultValue;
    }

    public function getOrThrowException(string $key, Throwable $exception)
    {
        if ($this->exist($key)) {
            return $this->data[$key];
        }

        throw $exception;
    }

    public function add(string $key, $value): bool
    {
        $elementExists = array_key_exists($key, $this->data);
        $this->data[$key] = $value;

        return !$elementExists;
    }

    public function remove(string $key, $value): bool
    {
        $elementExists = array_key_exists($key, $this->data);

        if ($elementExists) {
            unset($this->data[$key]);
        }

        return $elementExists;
    }

    public function exist(string $key): bool
    {
        return array_key_exists($key, $this->data);
    }

    public function asArray(): array
    {
        return $this->data;
    }

    public function serialize()
    {
        return serialize($this->data);
    }

    public function unserialize($serialized)
    {
        $result = unserialize($serialized);

        if ($result === false) {
            return false;
        }

        $this->data = $result;
        return true;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    public function count()
    {
        return count($this->data);
    }
}