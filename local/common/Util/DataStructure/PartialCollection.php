<?php


namespace Rybalka\Marketplace\Util\DataStructure;


class PartialCollection extends GenericCollection
{
    private $totalCount = 0;

    public function setTotalCount(int $totalCount)
    {
        $this->totalCount = $totalCount;
    }

    public function getTotalCount(): int
    {
        if (!$this->totalCount) {
            return $this->count();
        }

        return $this->totalCount;
    }
}