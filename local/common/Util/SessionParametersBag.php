<?php


namespace Rybalka\Marketplace\Util;


use DateTime;

class SessionParametersBag
{
    private $bagName;

    private $expirationTime;

    public function __construct(string $bagName, ?int $expirationTimeInSeconds = null)
    {
        $this->bagName = $bagName;
        $this->expirationTime = $expirationTimeInSeconds;
    }

    public function getOrDefault(string $fieldName, $defaultValue = null)
    {
        $this->initBagIfDoesNotExist();
        $this->invalidateDataIfStale();

        $_SESSION[$this->bagName]['lastAccessed'] = new DateTime();
        return $_SESSION[$this->bagName]['data'][$fieldName] ?? $defaultValue;
    }

    public function getOrCallback(string $fieldName, ?callable $callback)
    {
        $result = $this->getOrDefault($fieldName);

        if (is_null($result) && !is_null($callback)) {
            return $callback($fieldName);
        }

        return $result;
    }

    public function set(string $fieldName, $fieldValue)
    {
        $this->initBagIfDoesNotExist();

        $_SESSION[$this->bagName]['data'][$fieldName] = $fieldValue;
        $_SESSION[$this->bagName]['lastAccessed'] = new DateTime();
    }

    public function reset()
    {
        $_SESSION[$this->bagName] = [
            'data' => [],
            'lastAccessed' => new DateTime()
        ];
    }

    private function invalidateDataIfStale()
    {
        if (is_null($this->expirationTime)) {
            return;
        }

        $lastAccessed = $_SESSION[$this->bagName]['lastAccessed'];
        $now = new DateTime();

        $elapsedTime = $now->getTimestamp() - $lastAccessed->getTimestamp();
        if ($elapsedTime > $this->expirationTime) {
            $this->reset();
        }
    }

    private function initBagIfDoesNotExist()
    {
        if (!isset($_SESSION[$this->bagName])) {
            $this->reset();
        }
    }
}