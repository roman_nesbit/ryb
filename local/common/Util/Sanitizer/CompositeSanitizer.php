<?php


namespace Rybalka\Marketplace\Util\Sanitizer;


use InvalidArgumentException;
use Rybalka\Marketplace\Util\Sanitizer;

class CompositeSanitizer implements Sanitizer
{
    private $sanitizer = [];

    public function __construct(...$sanitizers)
    {
        foreach ($sanitizers as $sanitizer) {
            if (!$sanitizer instanceof Sanitizer) {
                throw new InvalidArgumentException('Argument must be of type Sanitizer');
            }

            $this->sanitizer[] = $sanitizer;
        }
    }

    public function sanitize($value)
    {
        foreach ($this->sanitizer as $sanitizer) {
            /**
             * @var Sanitizer $sanitizer
             */
            $value = $sanitizer->sanitize($value);
        }

        return $value;
    }
}