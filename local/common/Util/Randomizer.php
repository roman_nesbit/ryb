<?php

namespace Rybalka\Marketplace\Util;

class Randomizer
{
	public static function code()
	{
		return sprintf('%04u',
			mt_rand(1001, 9999)
		);
	}

	public static function uuid() 
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),
			mt_rand(0, 0xffff),
			mt_rand(0, 0x0fff) | 0x4000,
			mt_rand(0, 0x3fff) | 0x8000,
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

	public static function fileName(int $length = 6)
	{
		$_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$_string = '';

		for ($i = 0; $i < $length; $i++)
		{
			$_string .= $_chars[mt_rand(0, strlen($_chars) - 1)];
		}

		return $_string;
	}
}