<?php


namespace Rybalka\Marketplace\Util;


use Money\Currency;
use Money\Money;

class MoneyUtil
{
    const DEFAULT_NUMBER_OF_FRACTIONS = 100;
    const DEFAULT_CURRENCY = 'UAH';

    public function floatToMoney(string $currencyCode, float $amount): Money
    {
        $amount = (int)($amount * self::DEFAULT_NUMBER_OF_FRACTIONS);
        return new Money($amount, new Currency($currencyCode));
    }
}