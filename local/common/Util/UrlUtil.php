<?php


namespace Rybalka\Marketplace\Util;


class UrlUtil
{
    public const URL_PATTERN = '#(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\\\'".,<>?«»“”‘’]))#';
    public function getFilenameFrom(string $url): ?string
    {
        $path = $this->getFilePathFrom($url);

        if (!$path) {
            return null;
        }

        return bx_basename($path);
    }

    public function getFilePathFrom(string $url): ?string
    {
        $urlComponents = parse_url($url);

        if (!$urlComponents) {
            return null;
        }

        return $urlComponents['path'] ?? null;
    }
}