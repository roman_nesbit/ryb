<?php

namespace Rybalka\Marketplace\Util\Phone;

interface SmsLimit
{
    public function getBlockingTime(string $phoneNumber, string $clientIpAddress): int;
}