<?php

namespace Rybalka\Marketplace\Util\Phone;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DateTimeField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Service\GeoIp\Manager as GeoIpManager;
use Bitrix\Main\Sms\Event;
use Bitrix\Main\Type\DateTime;
use Rybalka\Marketplace\Http\Exception;
use Rybalka\Marketplace\Http\Exception\ForbiddenException;
use Rybalka\Marketplace\Http\Exception\InternalServerErrorException;
use Rybalka\Marketplace\Http\Exception\TooManyRequestsException;
use Rybalka\Marketplace\Service\Notification\SmsService;
use Rybalka\Marketplace\Util\FeatureToggle;
use Rybalka\Marketplace\Util\Phone\SmsLimit\MaxCompositeLimit;
use Rybalka\Marketplace\Util\Randomizer;

class VerificationTable extends Entity\DataManager
{
    const DEFAULT_VERIFICATION_CODE = '1234';

    public static function getTableName()
	{
		return PHONE_VERIFICATION_TABLE_NAME;
	}

	public static function getMap()
	{
		return [
			new StringField('verification_id', [
				'primary' => true,
				'required' => true,
				'validation' => function () {
					return [
						new Validator\Length(null, 36),
					];
				}
			]),
			new StringField('phone_number',[
				'required' => true,
				'validation' => function () {
					return [
						new Validator\Length(null, 12),
					];
				},
				'default_value' => '',
			]),
			new StringField('verification_code',[
				'required' => true,
				'validation' => function () {
					return [
						new Validator\Length(null, 4),
					];
				}
			]),
			new StringField('verification_token',[
				'validation' => function () {
					return [
						new Validator\Length(null, 36),
					];
				}
			]),
			new IntegerField('verification_attempts', [
				'default_value' => 0,
			]),
			new StringField('remote_ip',[
				'required' => true,
				'validation' => function () {
					return [
						new Validator\Length(null, 15),
					];
				}
			]),
			new DateTimeField('created_at'),
			new DateTimeField('verified_at'),
			new DateTimeField('used_at'),
		];
	}

	public static function isTableExists() {
		$connection = Application::getInstance()->getConnection();

		return $connection->isTableExists(static::getTableName());
	}

	public static function createTable()
	{
		$connection = Application::getInstance()->getConnection();

		// check is table exists
		if (!$connection->isTableExists(static::getTableName()))
		{
			// if no, create table
			static::getEntity()->createDbTable();
		}
		return true;
	}

	public static function dropTable()
	{
		$connection = Application::getInstance()->getConnection();

		// check if table exists
		if ($connection->isTableExists(static::getTableName()))
		{
			// drop the table
			$connection->dropTable(static::getTableName());
			return true;
		}
		else
		{
			return false;
		}
	}

	private static function addNewRecord($data)
	{
		$connection = Application::getInstance()->getConnection();
	
		if (!$connection->isTableExists(static::getTableName()))
		{
			static::createTable();
		}

		$result = static::add($data);

		if ($result->isSuccess()) {
			return $result->getId();
		}
		else
		{
			return [
				'error' => true,
				'message' => $result->getErrorMessages()
			];
		}
	}

	public static function getRecordData($verificationId)
	{
		$result = static::getList([
			'select' => ['phone_number', 'verification_code'],
			'filter' => ['verification_id' => $verificationId],
		]);
		$record = $result->fetch();
		return $record;
	}

	public static function sendCode($phoneNumber, $clientIpAddress)
	{
		$normalizedPhoneNumber = Normalizer::normalizePhoneNumber($phoneNumber);

		// перевірка коректності номера телефона
		if(!$normalizedPhoneNumber)
		{
			throw new Exception\BadRequestException('Invalid phone number');
		}

		$smsLimits = new MaxCompositeLimit();
		$blockingTime = $smsLimits->getBlockingTime($normalizedPhoneNumber, $clientIpAddress);

		if($blockingTime > 0)
		{
			throw (new TooManyRequestsException(
				'You have to wait before a new verification code can be sent'
			))->withResponseBody([
                'waitingTimeInSeconds' => $blockingTime
            ]);
		}

		$verificationCode = FeatureToggle::getInstance()->isPhoneVerificationTestModeOn()
            ? self::DEFAULT_VERIFICATION_CODE
            : Randomizer::code();

		// додаємо новий запис
		$resultAddNewRecord = static::addNewRecord([
			'phone_number' => $normalizedPhoneNumber,
			'remote_ip' => $clientIpAddress,
            'verification_id' => Randomizer::uuid(),
            'verification_code' => $verificationCode,
            'verification_token' => Randomizer::uuid(),
		]);

		// якщо є помилка
		if(isset($resultAddNewRecord['error']) && $resultAddNewRecord['error'])
		{
		    throw (new ForbiddenException())
                ->withResponseBody(['waitingTimeInSeconds' => 0]);
		}
		else
		{
			$arNewRecordData = static::getRecordData($resultAddNewRecord);

            $smsDispatchResult = (new SmsService())
                ->dispatchNow('SMS_USER_CONFIRM_NUMBER', [
                    'USER_PHONE' => $arNewRecordData['phone_number'],
                    'CODE' => $arNewRecordData['verification_code']
                ]);

            if (!$smsDispatchResult->isSuccess()) {
                throw new InternalServerErrorException('Failed to send sms confirmation');
            }

			static::update($resultAddNewRecord, [
				'created_at' => new DateTime()
			]);

			$data = [
			    'verificationId' => $resultAddNewRecord,
                'waitingTimeInSeconds' => $smsLimits
                    ->getBlockingTime($normalizedPhoneNumber, $clientIpAddress)
            ];
		}

		return $data;
	}

	public static function verify($verificationId, $code)
	{
		// метод для перевірки відповідності коду з смс конкретній верифікації
		// читаємо дані з таблиці
		$result = static::getList([
			'select' => ['verification_token', 'verification_code', 'verification_attempts', 'created_at'],
			'filter' => [
				'verification_id' => $verificationId, 
				'!verification_code' => false, 
			],
			'order' => ['created_at' => 'desc'],
			'limit' => 1,
		]);
		$record = $result->fetch();
		// exo($record);

		// Якщо немає запису
		if(!$record)
		{
			throw new Exception\BadRequestException('Bad request');
		}

		// Якщо СМС не відправлявся
		if(!$record['created_at'])
		{
			throw new Exception\BadRequestException('Bad request');
		}

		// Якщо перевищена кількість спроб - кидає помилку.
		$maxAttempts = (int)Option::get("grain.customsettings", "PV_MAX_ATTEMPTS");
		if((int)$record['verification_attempts'] >= $maxAttempts)
		{
		    throw (new ForbiddenException('Code verification failed. New attempt has to be made'))
                ->withResponseBody(['waitingTimeInSeconds' => 0]);
		}

		// Якщо перевищено час "життя" коду - кидає помилку.
		$codeLifetime = (int)Option::get("grain.customsettings", "PV_CODE_LIFETIME");
		$to = new DateTime();
		if($to->getTimestamp() - $record['created_at']->getTimestamp() > $codeLifetime)
		{
			throw (new ForbiddenException('Code verification failed. New attempt has to be made'))
                ->withResponseBody(['waitingTimeInSeconds' => 0]);
		}

		// Інкрементуємо кількість спроб
		static::update($verificationId, [
			'verification_attempts' => $record['verification_attempts'] + 1
		]);

		// Якщо неправильний код - кидає помилку.
		if($record['verification_code'] !== $code)
		{
			throw new Exception\NotFoundException('Incorrect verification code');
		}

		// Записуємо час вдалої перевірки
		static::update($verificationId, [
			'verified_at' => new DateTime()
		]);

		$data = ['verificationToken' => $record['verification_token']];

		return $data;
	}

	public static function isValidVerificationToken($verificationToken)
	{
		// просто перевіряє, чи це правильний токен верифікації та чи він не застарів (він має застарівати за 10 хв)

		// читаємо дані з таблиці
		$result = static::getList([
			'select' => ['created_at', 'used_at'],
			'filter' => [
				'verification_token' => $verificationToken, 
			],
		]);
		$record = $result->fetch();

		if($record['used_at'])
		{
			// вже використаний
			return false;
		}

		if($record['created_at'])
		{
			// Якщо перевищено час "життя" токена - кидає помилку.
			$tokenLifetime = (int)Option::get("grain.customsettings", "PV_TOKEN_LIFETIME");
			$to = new DateTime();
			if($to->getTimestamp() - $record['created_at']->getTimestamp() > $tokenLifetime)
			{
				return false;
			}

			return true;
		}

		return false;
	}

	public static function getVerifiedPhoneNumber($verificationToken)
	{
		// повертає номер телефону по токену і відмічає цей токен, як використаний

		// читаємо дані з таблиці
		$result = static::getList([
			'select' => ['phone_number', 'used_at', 'verification_id'],
			'filter' => [
				'verification_token' => $verificationToken,
			],
		]);
		$record = $result->fetch();

		if($record['phone_number'] && !$record['used_at'])
		{
			// Записуємо час використання
			static::update($record['verification_id'], [
				'used_at' => new DateTime()
			]);

			return $record['phone_number'];
		}

		return false;
	}

	public static function cleanUpAgent()
	{
		$connection = Application::getConnection();

		$sql = 'DELETE FROM ' . static::getTableName() . ' WHERE created_at IS NULL OR created_at < NOW() - INTERVAL ' . Option::get("grain.customsettings", "PV_TOKEN_LIFETIME") . ' SECOND';

		$connection->query($sql);

		return 'Rybalka\Marketplace\Util\Phone\VerificationTable::cleanUpAgent();';
	}
}