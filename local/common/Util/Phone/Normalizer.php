<?php

namespace Rybalka\Marketplace\Util\Phone;

use Exception;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

class Normalizer
{
	public static function normalizePhoneNumber($phoneNumber)
	{
		try
		{
			$phoneUtil = PhoneNumberUtil::getInstance();

			$numberProto = $phoneUtil->parse($phoneNumber, "UA");
			if(!$phoneUtil->isValidNumberForRegion($numberProto, "UA"))
			{
				throw new Exception('Invalid phone number', 400);
			}

			$phoneNumberNormalized = str_replace('+', '', $phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::E164));

			// Check the number length
			if(strlen($phoneNumberNormalized) != 12)
			{
				throw new Exception('Invalid phone number', 400);
			}

			return $phoneNumberNormalized;
		}
		catch (NumberParseException $e)
		{
			return false;
		}
		catch (Exception $e)
		{
			return false;
		}
	}
}