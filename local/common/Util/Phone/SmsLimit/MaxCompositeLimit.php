<?php


namespace Rybalka\Marketplace\Util\Phone\SmsLimit;


use Rybalka\Marketplace\Util\Phone\SmsLimit;

class MaxCompositeLimit implements SmsLimit
{
    /**
     * @var array
     */
    private $limits = [];

    public function __construct()
    {
        $this->limits = [
            new IpAddressLimit(),
            new PhoneNumberLimit(),
            new TimeSinceLastMessageLimit()
        ];
    }

    public function getBlockingTime(string $phoneNumber, string $clientIpAddress): int
    {
        $blockingTime = 0;

        foreach ($this->limits as $limit) {
            /**
             * @var SmsLimit $limit
             */
            $blockingTime = max($blockingTime, $limit->getBlockingTime($phoneNumber, $clientIpAddress));
        }

        return $blockingTime;
    }
}