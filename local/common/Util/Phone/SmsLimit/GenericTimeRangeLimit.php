<?php


namespace Rybalka\Marketplace\Util\Phone\SmsLimit;


use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;
use Rybalka\Marketplace\Util\Phone\SmsLimit;
use Rybalka\Marketplace\Util\Phone\VerificationTable;

abstract class GenericTimeRangeLimit implements SmsLimit
{
    abstract protected function getFilter(string $phoneNumber, string $clientIpAddress): array;
    abstract protected function getMaxMessagesPerTimeConstraint(): int;
    abstract protected function getTimeConstraintInSeconds(): int;

    public function getBlockingTime(string $phoneNumber, string $clientIpAddress): int
    {
        if (!$this->isLimitExceeded($phoneNumber, $clientIpAddress)) {
            return 0;
        }

        $now = new DateTime();

        $result = VerificationTable::getList([
            'select' => ['created_at'],
            'filter' => $this->getFilter($phoneNumber, $clientIpAddress),
            'order' => [
                'created_at' => 'desc'
            ],
            'limit' => 1,
        ]);


        if($record = $result->fetch())
        {
            $timeSinceLastMessage = $now->getTimestamp() - $record['created_at']->getTimestamp();
            $blockingTime = $this->getTimeConstraintInSeconds() - $timeSinceLastMessage;
            return max($blockingTime, 0);
        }

        return 0;
    }

    private function isLimitExceeded(string $phoneNumber, string $clientIpAddress): bool
    {
        $now = new DateTime();

        $filter = $this->getFilter($phoneNumber, $clientIpAddress);
        $filter['>=created_at'] = $now
            ->add('-' . $this->getTimeConstraintInSeconds() . ' seconds')
            ->toString();


        $result = VerificationTable::getList([
            'select' => ['CNT'],
            'filter' => $filter,
            'runtime' => [
                new Entity\ExpressionField('CNT', 'COUNT(*)')
            ],
        ]);
        $record = $result->fetch();

        $messagesSent = (int)$record['CNT'];
        return $messagesSent >= $this->getMaxMessagesPerTimeConstraint();
    }
}