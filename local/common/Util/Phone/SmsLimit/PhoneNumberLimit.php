<?php

namespace Rybalka\Marketplace\Util\Phone\SmsLimit;


use Bitrix\Main\Config\Option;

class PhoneNumberLimit extends GenericTimeRangeLimit
{
    protected function getFilter(string $phoneNumber, string $clientIpAddress): array
    {
        return [
            'phone_number' => $phoneNumber
        ];
    }

    protected function getMaxMessagesPerTimeConstraint(): int
    {
        return (int)Option::get('grain.customsettings', 'PV_MAX_SMS_FOR_NUMBER');
    }

    protected function getTimeConstraintInSeconds(): int
    {
        return 3600;
    }
}