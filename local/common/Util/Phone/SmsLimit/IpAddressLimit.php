<?php

namespace Rybalka\Marketplace\Util\Phone\SmsLimit;

use Bitrix\Main\Config\Option;

class IpAddressLimit extends GenericTimeRangeLimit
{
    protected function getFilter(string $phoneNumber, string $clientIpAddress): array
    {
        return [
            'remote_ip' => $clientIpAddress,
        ];
    }

    protected function getMaxMessagesPerTimeConstraint(): int
    {
        return (int)Option::get(
            'grain.customsettings',
            'PV_MAX_SMS_FROM_IP'
        );
    }

    protected function getTimeConstraintInSeconds(): int
    {
        return 3600;
    }
}