<?php

namespace Rybalka\Marketplace\Util\Phone\SmsLimit;


use Bitrix\Main\Config\Option;

class TimeSinceLastMessageLimit extends GenericTimeRangeLimit
{
    protected function getFilter(string $phoneNumber, string $clientIpAddress): array
    {
        return [
            'phone_number' => $phoneNumber
        ];
    }

    protected function getMaxMessagesPerTimeConstraint(): int
    {
        return 1;
    }

    protected function getTimeConstraintInSeconds(): int
    {
        return (int)Option::get('grain.customsettings', 'PV_PERIOD_BETWEEN_SMS');
    }
}