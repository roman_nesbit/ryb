<?php


namespace Rybalka\Marketplace\Util\PartnerShop\Import;


class NormalizationUtil
{
    const DEFAULT_ENCODING = 'utf-8';
    const MAX_EXTERNAL_ID_LENGTH = 20;

    public function externalOfferId(string $offerId)
    {
        $offerId = trim($offerId);
        $offerId = preg_replace('/[^\w_\-]/', '', $offerId);

        if (mb_strlen($offerId, self::DEFAULT_ENCODING) > self::MAX_EXTERNAL_ID_LENGTH) {
            return mb_substr($offerId, 0, self::MAX_EXTERNAL_ID_LENGTH, self::DEFAULT_ENCODING);
        }

        return $offerId;
    }

    public function catalogItemParameterName(string $name)
    {
        return $this->defaultNormalization($name);
    }

    public function measurement(string $measurement)
    {
        return $this->defaultNormalization($measurement);
    }

    public function measurementUnit(string $unit)
    {
        return trim(
            mb_strtolower($unit, self::DEFAULT_ENCODING)
        );
    }

    public function listPropertyValue(string $property)
    {
        return $this->defaultNormalization($property);
    }

    private function defaultNormalization(string $parameter): string
    {
        $parameter = mb_strtolower($parameter, self::DEFAULT_ENCODING);
        $parameter = preg_replace( '/[^\p{L}|\p{N}]+/u', '', $parameter);

        return $parameter;
    }
}