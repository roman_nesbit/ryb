<?php


namespace Rybalka\Marketplace\Util\PartnerShop\Import;


class DiscountsUtil
{
    const PRICE_PRECISION = 2;

    public function calculateDiscountRate(float $price, float $originalPrice): int
    {
        if (round($originalPrice, self::PRICE_PRECISION) <= round($price, self::PRICE_PRECISION)) {
            return 0;
        }

        return round(($originalPrice - $price) /  $originalPrice * 100);
    }

    public function calculateOriginalPrice(float $discountedPrice, float $discountRate): float
    {
        return round($discountedPrice / (100 - $discountRate) * 100, self::PRICE_PRECISION);
    }
}