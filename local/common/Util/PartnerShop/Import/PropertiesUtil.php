<?php

namespace Rybalka\Marketplace\Util\PartnerShop\Import;

use Bitrix\Iblock\PropertyEnumerationTable;

class PropertiesUtil
{
    public function getListPropertyValues(int $propertyId)
    {
        $data = [];

        $resValue = PropertyEnumerationTable::getList([
            'select' => [
                'ID',
                'VALUE'
            ],
            'filter' => [
                'PROPERTY_ID' => $propertyId,
            ],
            'order' => [
                'SORT' => 'ASC',
                'VALUE' => 'ASC',
            ]
        ]);
        while($value = $resValue->fetch())
        {
            $data[$value['ID']] = [
                'NAME' => $value['VALUE']
            ];
        }

        return $data;
    }
}