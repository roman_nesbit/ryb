<?php


namespace Rybalka\Marketplace\Util\PartnerShop\Import;


use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Volume;

class MeasurementTypeUtil
{
    public const TYPE_LENGTH = 'length';
    public const TYPE_WEIGHT = 'weight';
    public const TYPE_VOLUME = 'volume';

    private const MEASUREMENTS = [

        self::TYPE_LENGTH => [
            'мм' => 'mm',
            'см' => 'cm',
            'м'  => 'm',
            'км' => 'km'
        ],

        self::TYPE_WEIGHT => [
            'мг' => 'mg',
            'г' => 'g',
            'кг' => 'kg',
            'т' => 't',
        ],

        self::TYPE_VOLUME => [
            'мл' => 'ml',
            'л' => 'l'
        ],
    ];

    /**
     * @var NormalizationUtil
     */
    private $normalizationUtil;

    public function __construct()
    {
        $this->normalizationUtil = new NormalizationUtil();
    }

    public function create(float $value, string $unit)
    {
        $type = $this->getTypeByUnit($unit);

        if (is_null($type)) {
            return null;
        }

        $internationalUnit = $this->getInternationalNotationFor($unit);

        $result = null;

        switch ($type) {
            case self::TYPE_LENGTH:
                $result = new Length($value, $internationalUnit);
                break;
            case self::TYPE_WEIGHT:
                $result = new Mass($value, $internationalUnit);
                break;
            case self::TYPE_VOLUME:
                $result = new Volume($value, $internationalUnit);
                break;
        }

        return $result;
    }

    public function isTheSameType(string $unit1, string $unit2): bool
    {
        $type1 = $this->getTypeByUnit($unit1);
        $type2 = $this->getTypeByUnit($unit2);

        return !is_null($type1) && !is_null($type2) && $type1 == $type2;
    }


    public function getInternationalNotationFor(string $unit): ?string
    {
        $unit = $this->normalizationUtil->measurementUnit($unit);

        if (empty($unit)) {
            return null;
        }

        $type = $this->getTypeByUnit($unit);

        if (is_null($type)) {
            return null;
        }

        return self::MEASUREMENTS[$type][$unit] ?? null;
    }

    private function getTypeByUnit(string $unit): ?string
    {
        $unit = $this->normalizationUtil->measurementUnit($unit);

        if (empty($unit)) {
            return null;
        }

        foreach (self::MEASUREMENTS as $type => $units) {
            if (isset($units[$unit])) {
                return $type;
            }
        }

        return null;
    }
}