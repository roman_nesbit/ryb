<?

namespace Rybalka\Marketplace\Util;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Sale;
use CUser;
use Rybalka\Marketplace\Http\Exception\BadRequestException;
use Rybalka\Marketplace\Util\RybalkaPdf;
use NumberToWords\NumberToWords;

class InvoicePdf
{
	const INVOICES_FOLDER = '/i/';

	private $pdf;
	private $orderId;
	private $order;
	private $siteData;
	private $userData;
	private $fileName;
	private $invoiceData;

	public function __construct($orderId)
	{
		if(!Loader::includeModule('sale'))
		{
			throw new BadRequestException('Sale module not loaded');
		}
		
		$this->orderId = $orderId;
	}

	public function setOrder($order)
	{
		$this->order = $order;

		return $this;
	}

	public function prepare()
	{
		if (!$this->orderId)
		{
			throw new BadRequestException('No order id' . $this);
		}

		$this->order = $this->getOrder();

		return $this;
	}

	public function make()
	{
		// check if order already has invoice
		if(!$this->hasOrderInvoice())
		{
			$this->invoiceData = $this->getInvoiceData();

			$this->siteData = $this->getSiteData();

			$this->userData = $this->getUserData();

			$this
				->createInvoiceFile()
				->startPdf()
				->completePdf()
				->save()
				->setOrderInvoice();
		}
		else
		{
			$this->getOrderInvoice();
		}

		return $this;
	}

	public function display()
	{
		$this->pdf->Output($this->fileName, 'I');
	}

	public function save()
	{
		$_filePath = $this->getInvoicePath();
		$_string = $this->pdf->Output($_filePath, 'S');
		file_put_contents($_filePath, $_string);

		return $this;
	}

	public function getFileName()
	{
		return $this->fileName;
	}

	public function setFileName($fileName)
	{
		$this->fileName = $fileName;

		return $this;
	}

	public function getOrder()
	{
		return Sale\Order::load($this->orderId);
	}

	private function getSiteData($siteId = SITE_ID)
	{
		if(empty($siteId))
		{
			$siteId = 's1';
		}

		return \CSite::GetByID($siteId)->Fetch();
	}

	private function getUserData()
	{
		$userId = (int)$this->order->getUserId();
		$rsUser = CUser::GetByID($userId);
		return $rsUser->Fetch();
	}

	private function createInvoiceFile()
	{
		do
		{
			$this->fileName = Randomizer::fileName() . '.pdf';
		} 
		while(file_exists($this->getInvoicePath()));

		return $this;
	}

	public function getInvoicePath()
	{
		return $_SERVER['DOCUMENT_ROOT'] 
			. self::INVOICES_FOLDER 
			. $this->fileName;
	}

	public function getInvoiceUrl()
	{
		$_server = Application::getInstance()->getContext()->getServer();

		return (\CMain::IsHTTPS() ? 'https://' : 'http://') 
			. $_server->getServerName()
			. self::INVOICES_FOLDER 
			. $this->fileName;
	}

	private function startPdf()
	{
		$this->pdf = new RybalkaPdf(
			PDF_PAGE_ORIENTATION,
			PDF_UNIT,
			RybalkaPdf::PDF_PAGE_FORMAT,
			true,
			'UTF-8',
			false, 
			false
		);

		// set document information
		$this->pdf->SetCreator(PDF_CREATOR);
		if (is_array($this->siteData) && strlen($this->siteData['NAME']) > 0)
		{
			$this->pdf->SetAuthor($this->siteData['NAME']);
		}
		$this->pdf->SetTitle('Rybalka.ua - Счет на оплату заказа №' . $this->order->getField('ACCOUNT_NUMBER'));
		// $this->pdf->SetSubject('TCPDF Tutorial');
		// $this->pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// remove default footer
		$this->pdf->setPrintFooter(false);

		// font
		$this->pdf->SetFont('freesans', '', 12);

		// set margins
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		return $this;
	}

	private function completePdf()
	{
		// add a page
		$this->pdf->AddPage();

		$this
			->printSeparator()
			->printBlock('<h1>Счет № ' . $this->invoiceData['number'] . ' от ' . $this->invoiceData['date'] . '</h1>')
			->printSeparator()
			->printColumns(
				'<b>Реквизиты для банковского перевода</b><br>' . Option::get('grain.customsettings', 'INVOICE_BANK_DATA'),
				'<b>Реквизиты для перевода на карту</b><br>' . Option::get('grain.customsettings', 'INVOICE_CARD_DATA')
			)
			->printColumns(
				'<b>Назначение платежа</b><br>Заказ №' . $this->order->getField('ACCOUNT_NUMBER'),
				'<b>Покупатель</b><br>' . $this->userData['NAME'] . ' ' . $this->userData['LAST_NAME']
			)
			->printProducts()
			->printSeparator();

		// reset pointer to the last page
		$this->pdf->lastPage();

		return $this;
	}

	private function printBlock($html)
	{
		$this->pdf->writeHTML($html, true, false, true, false, '');

		return $this;
	}

	private function printColumns($leftHtml, $rightHtml)
	{
		$_html = '<table><tr><td>' . $leftHtml . '</td><td>' . $rightHtml . '</td></tr></table>';
		return $this->printBlock($_html);
	}

	private function printProducts()
	{
		$basket = $this->order->getBasket();
		$currency = $this->order->getCurrency();
		$price = $this->order->getPrice(); // Сума до сплати
		$deliverySumFormatted =  CurrencyFormat($this->order->getDeliveryPrice(), $currency); // Сумма доставка
		$deliveryId = $this->order->getDeliveryIdList()[0];
		$deliveryDescription = getDeliveryDescription($deliveryId, $deliverySumFormatted);
		
		$priceWithoutDiscount = $basket->getBasePrice(); // Сума без знижок 
		$discount = $priceWithoutDiscount - $price; // Знижка

		$products = $basket->getBasketItems();
		$_html = '<table border="1" cellpadding="5">';
		$_html .= '<thead><tr>';
		$_html .= '<td align="center" width="5%">№ п/п</td>';
		$_html .= '<td align="center" width="50%">Наименование товара</td>';
		$_html .= '<td align="center" width="10%">Кол-во</td>';
		$_html .= '<td align="center" width="5%">Ед</td>';
		$_html .= '<td align="center" width="15%">Цена (грн.)</td>';
		$_html .= '<td align="center" width="15%">Сумма (грн.)</td>';
		$_html .= '</tr></thead>';
		//sort items by name
        sortBasketItemsByNameCollection($products);
        $key = 0;
		foreach($products as $product)
		{
			$_html .= '<tr>';
			$_html .= '<td align="center" width="5%">' . ($key + 1) . '</td>';
			$_html .= '<td width="50%">' . $product->getField('NAME') .'</td>';
			$_html .= '<td align="right" width="10%">' . $product->getQuantity() . '</td>';
			$_html .= '<td align="center" width="5%">' . $product->getField('MEASURE_NAME') . '</td>';
			$_html .= '<td align="right" width="15%">' . CurrencyFormatNumber($product->getPrice(), $currency) . '</td>';
			$_html .= '<td align="right" width="15%">' . CurrencyFormatNumber($product->getFinalPrice(), $currency) . '</td>';
			$_html .= '</tr>';
			$key++;
		}

		$_html .= '</table><br><br>';

		$_html .= '<div style="text-align:right">';
		if($discount)
		{
			$_html .= 'Итого без скидки: ' . CurrencyFormat($priceWithoutDiscount, $currency) . '<br>';
			$_html .= 'Скидка: ' . CurrencyFormat($discount, $currency) . '<br>';
		}

		$_html .= 'Итого: ' . CurrencyFormat($price, $currency) . '<br><br>';
		$_html .= $deliveryDescription . '<br><br>';

		$_html .= '<b>Всего к оплате: ' . CurrencyFormat($price, $currency) . '</b><br>';
		$_html .= '(' . $this->getPriceInWords($price, $currency)  . ')';
		$_html .= '</div>';

		return $this->printBlock($_html);
	}

	private function getPriceInWords($price, $currency)
	{
		$numberToWords = new NumberToWords();
		$currencyTransformer = $numberToWords->getCurrencyTransformer(LANGUAGE_ID);

		return $currencyTransformer->toWords($price*100, $currency);;
	}

	private function printSeparator()
	{
		return $this->printBlock('<br>');
	}

	private function getInvoiceData()
	{
		$paymentCollection = $this->order->getPaymentCollection();
		$payment = $paymentCollection[0];

		return [
			'paysystem' => $payment->getField('PAY_SYSTEM_ID'),
			'number' => $payment->getField('ID'),
			'date' => ConvertDateTime($payment->getField('DATE_BILL'), 'DD.MM.YYYY'),
		];
	}

	private function hasOrderInvoice()
	{
		$_orderPropertyId = $this->getOrderPropertyIdByCode('INVOICE_PDF');

		if(!$_orderPropertyId)
		{
			return false;
		}

		$this->fileName = $this->order->getPropertyCollection()->getItemByOrderPropertyId($_orderPropertyId)->getValue();

		return $this->fileName && file_exists($this->getInvoicePath());
	}

	private function getOrderPropertyIdByCode($code)
	{
		if(!$code)
		{
			return false;
		}

		$_properties = $this->order->getPropertyCollection()->getArray()['properties'];
		

		foreach($_properties as $_property)
		{
			if($_property['CODE'] == $code)
			{
				return $_property['ID'];
			}
		}

		return false;
	}

	private function setOrderInvoice()
	{
		if(!$this->fileName)
		{
			return false;
		}

		$_orderPropertyId = $this->getOrderPropertyIdByCode('INVOICE_PDF');

		if(!$_orderPropertyId)
		{
			return false;
		}

		$this->order->getPropertyCollection()->getItemByOrderPropertyId($_orderPropertyId)->setValue($this->getFileName());

		$this->order->save();

		return $this;
	}

	private function getOrderInvoice()
	{
		$_orderPropertyId = $this->getOrderPropertyIdByCode('INVOICE_PDF');

		if (!$_orderPropertyId) 
		{
			return false;
		}

		$this->fileName = $this->order->getPropertyCollection()->getItemByOrderPropertyId($_orderPropertyId)->getValue();

		return $this;
	}

}