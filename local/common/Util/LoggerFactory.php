<?php


namespace Rybalka\Marketplace\Util;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerFactory
{
    const UNIQ_ID_LENGTH = 6;

    /**
     * @var string
     */
    private $logsDir;

    /**
     * @var string
     */
    private $importLogsDir;

    public function __construct()
    {
        $this->logsDir = getenv('MARKETPLACE_LOGS_DIR') . '/app';
        $this->importLogsDir = $this->logsDir . '/import';
    }

    public function getLogger(string $loggerId, string $loggerName = 'default'): Logger
    {
        $infoStream = new StreamHandler($this->logsDir . '/' . $loggerName . '.info.log', Logger::INFO);
        $errorStream = new StreamHandler($this->logsDir . '/' . $loggerName . '.error.log', Logger::ERROR);

        $logger = new Logger($loggerId);
        $logger->pushHandler($infoStream);
        $logger->pushHandler($errorStream);

        return $logger;
    }

    public function getDictionaryImportLogger(string $dataName): Logger
    {
        return $this->getImportLogger('dictionary', $dataName);
    }

    public function getCatalogImportLogger(string $partnerId): Logger
    {
        return $this->getImportLogger('catalog-items', $partnerId);
    }

    private function getImportLogger(string $filePrefix, string $id): Logger
    {
        $infoStream = new StreamHandler($this->importLogsDir . '/' . $filePrefix . '.info.log', Logger::INFO);
        $errorStream = new StreamHandler($this->importLogsDir . '/' . $filePrefix . '.error.log', Logger::ERROR);

        $uniqId = randString(self::UNIQ_ID_LENGTH);

        $logger = new Logger('import.' . $id . '.' . $uniqId);
        $logger->pushHandler($infoStream);
        $logger->pushHandler($errorStream);

        return $logger;
    }
}