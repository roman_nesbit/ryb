<?php


namespace Rybalka\Marketplace\Partners\ImportXml;


use Monolog\Logger;
use Rybalka\Marketplace\Util\LoggerFactory;
use function array_key_first;

class PicturesImportQueueService
{
    private $main = [];
    private $extra = [];

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var PicturesUpdateService
     */
    private $picturesUpdateService;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?? (new LoggerFactory())->getLogger('picturesImportQueue');
        $this->picturesUpdateService = new PicturesUpdateService($this->logger);
    }

    public function enqueueMain(int $catalogItem, string $pictureUrl)
    {
        $this->main[$catalogItem] = $pictureUrl;
    }

    public function enqueueExtra(int $catalogItem, array $picturesUrls)
    {
        $this->extra[$catalogItem] = $picturesUrls;
    }

    public function resolveQueue()
    {
        while (!empty($this->main)) {
            $catalogItemId = array_keys($this->main)[0];
            $url = $this->main[$catalogItemId];

            $this->main = array_slice($this->main, 1, null, true);

            $this->picturesUpdateService->setMainPictureFor($catalogItemId, $url);
        }

        while (!empty($this->extra)) {
            $catalogItemId = array_keys($this->extra)[0];
            $urls = $this->extra[$catalogItemId];

            $this->extra = array_slice($this->extra, 1, null, true);

            $this->picturesUpdateService->updatePicturesFor($catalogItemId, $urls);
        }
    }
}