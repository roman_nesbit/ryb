<?php


namespace Rybalka\Marketplace\Partners\ImportXml;


use HTMLPurifier;
use HTMLPurifier_Config;
use HTMLPurifier_TagTransform_Simple;

class PurifierFactory
{
    public function plaintextPurifier(): HTMLPurifier
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
        $config->set('HTML.Allowed', '');

        return $this->createPurifierWithConfig(
            $config
        );
    }

    public function limitedTagsPurifier(string $allowedTags): HTMLPurifier
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
        $config->set('HTML.Allowed', $allowedTags);

        return $this->createPurifierWithConfig($config);
    }

    private function createPurifierWithConfig(HTMLPurifier_Config $config): HTMLPurifier
    {
        return new HTMLPurifier($config);
    }
}