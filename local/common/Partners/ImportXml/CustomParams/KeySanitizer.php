<?php


namespace Rybalka\Marketplace\Partners\ImportXml\CustomParams;


use Rybalka\Marketplace\Partners\ImportXml\SanitizationUtil;
use Rybalka\Marketplace\Util\Sanitizer;

class KeySanitizer implements Sanitizer
{
    /**
     * @var SanitizationUtil
     */
    private $sanitizationUtil;

    public function __construct()
    {
        $this->sanitizationUtil = new SanitizationUtil();
    }

    public function sanitize($value)
    {
        return $this->sanitizationUtil->parameterName($value);
    }
}