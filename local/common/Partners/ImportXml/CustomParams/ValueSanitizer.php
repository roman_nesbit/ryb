<?php


namespace Rybalka\Marketplace\Partners\ImportXml\CustomParams;


use Rybalka\Marketplace\Partners\ImportXml\SanitizationUtil;
use Rybalka\Marketplace\Util\Sanitizer;

class ValueSanitizer implements Sanitizer
{
    /**
     * @var SanitizationUtil
     */
    private $sanitizationUtil;

    public function __construct()
    {
        $this->sanitizationUtil = new SanitizationUtil();
    }

    public function sanitize($value)
    {
        $values = $this->sanitizationUtil->multiValueParameterValue($value);
        return implode(' / ', $values);
    }
}