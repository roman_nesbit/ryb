<?php


namespace Rybalka\Marketplace\Partners\ImportXml;


use CFile;
use CIBlockElement;
use Monolog\Logger;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\UrlUtil;

class PicturesUpdateService
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var UrlUtil
     */
    private $urlUtil;

    /**
     * @var CIBlockElement
     */
    private $repository;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?? (new LoggerFactory())->getLogger('pictureUpdateService');
        $this->urlUtil = new UrlUtil();
        $this->repository = new CIBlockElement();
    }

    public function setMainPictureFor(int $catalogItemId, string $newPictureUrl)
    {
        $this->logger->info('Setting main picture for the catalog item', [
            'itemId' => $catalogItemId,
            'url' => $newPictureUrl
        ]);

        $mainPicture = $this->getMainFor($catalogItemId);
        $newPictureName = $this->urlUtil->getFilenameFrom($newPictureUrl);

        if (!empty($mainPicture) && $mainPicture['~ORIGINAL_NAME'] == $newPictureName) {
            return;
        }

        $this->logger->info('Main picture will be replaced for the catalog item', [
            'itemId' => $catalogItemId,
            'url' => $newPictureUrl
        ]);

        $imageFileDescriptor = CFile::MakeFileArray($newPictureUrl);

        if (empty($imageFileDescriptor)) {
            $this->logger->error('Failed to download an image for the catalog item', [
                'itemId' => $catalogItemId,
                'url' => $newPictureUrl
            ]);

            return;
        }

        $isUpdateSuccessful = $this->repository->Update(
            $catalogItemId,
            [
                'DETAIL_PICTURE' => $imageFileDescriptor
            ],
            false,
            false,
            false
        );

        if (!$isUpdateSuccessful) {
            $this->logger->error('Failed to set a new main picture for the catalog item', [
                'itemId' => $catalogItemId,
                'descriptor' => $imageFileDescriptor
            ]);
        }
    }

    public function updatePicturesFor(int $catalogItemId, array $newPicturesUrls)
    {
        $this->logger->info('Setting extra pictures for the catalog item', [
            'itemId' => $catalogItemId
        ]);

        $existingPictures = $this->loadExistingFor($catalogItemId);

        $images = [];
        $hasNewImages = false;

        foreach ($newPicturesUrls as $pictureUrl) {
            $filename = $this->urlUtil->getFilenameFrom($pictureUrl);

            if (isset($existingPictures[$filename])) {
                $existingPicture = $existingPictures[$filename];

                $images[$existingPicture['PROPERTY_VALUE_ID']] = [
                    'VALUE' => [
                        'name' => $existingPicture['FILE_NAME'],
                        'type' => $existingPicture['CONTENT_TYPE'],
                    ],
                    'DESCRIPTION' => $existingPicture['~DESCRIPTION']
                ];

                continue;
            }

            $imageFileDescriptor = CFile::MakeFileArray($pictureUrl);

            if (empty($imageFileDescriptor)) {
                $this->logger->error('Failed to download an image for the catalog item', [
                    'itemId' => $catalogItemId,
                    'url' => $pictureUrl
                ]);

                continue;
            }

            $this->logger->info('A new image will be added to catalog item', [
                'itemId' => $catalogItemId,
                'url' => $pictureUrl
            ]);

            $images[] = $imageFileDescriptor;
            $hasNewImages = true;
        }

        $properties = [
            'MORE_PHOTO' => $images
        ];

        $imagesHaveChanged = $hasNewImages || count($existingPictures) != count($images);

        if (!$imagesHaveChanged) {
            return;
        }

        if (empty($images)) {
            $this->logger->warning('Catalog item has no extra images', [
                'itemId' => $catalogItemId
            ]);

            $properties['MORE_PHOTO'] = [
                'VALUE' => [
                    'del' => 'Y'
                ]
            ];
        }

        if (!empty($images)) {
            $properties['MORE_PHOTO'] = $images;
        }

        $this->repository->SetPropertyValuesEx(
            $catalogItemId,
            InfoBlock::CATALOG,
            $properties
        );
    }

    private function getFilesDetails(array $filesIds): array
    {
        $queryResult = CFile::GetList([], [ '@ID' => $filesIds ]);

        $result = [];

        while ($record = $queryResult->GetNext()) {
            $result[$record['ID']] = $record;
        }

        return $result;
    }

    private function getMainFor(int $catalogItemId): ?array
    {
        $data = $this->fetchCatalogItemData($catalogItemId, ['ID', 'DETAIL_PICTURE']);

        if (empty($data['DETAIL_PICTURE'])) {
            return null;
        }

        $files = $this->getFilesDetails([$data['DETAIL_PICTURE']]);

        if (empty($files)) {
            return null;
        }

        return array_values($files)[0];
    }

    private function loadExistingFor(int $catalogItemId): array
    {
        $result = [];

        $data = $this->fetchCatalogItemData($catalogItemId, ['ID', 'PROPERTY_MORE_PHOTO']);

        if (empty($data)) {
            return $result;
        }

        $fileIdToValueIdMap = [];

        foreach ($data['PROPERTY_MORE_PHOTO_PROPERTY_VALUE_ID'] as $index => $valueId) {
            $fileId = $data['PROPERTY_MORE_PHOTO_VALUE'][$index];
            $fileIdToValueIdMap[$fileId] = $valueId;
        }

        $files = !empty($fileIdToValueIdMap)
            ? $this->getFilesDetails(array_keys($fileIdToValueIdMap))
            : [];

        foreach ($files as $fileId => $fileData) {
            $fileData['PROPERTY_VALUE_ID'] = $fileIdToValueIdMap[$fileId];
            $result[$fileData['~ORIGINAL_NAME']] = $fileData;
        }

        return $result;
    }

    private function fetchCatalogItemData(int $catalogItemId, array $fields): array
    {
        $result = $this->repository->GetList(
            [],
            [
                'IBLOCK_ID' => InfoBlock::CATALOG,
                'ID' => $catalogItemId
            ],
            false,
            false,
            $fields
        )->Fetch();

        return $result ?: [];
    }
}