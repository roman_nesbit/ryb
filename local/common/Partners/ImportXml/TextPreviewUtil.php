<?php


namespace Rybalka\Marketplace\Partners\ImportXml;


use HTMLPurifier;

class TextPreviewUtil
{
    /**
     * @var HTMLPurifier
     */
    private $plaintextPurifier;

    public function __construct()
    {
        $this->plaintextPurifier = (new PurifierFactory())->plaintextPurifier();
    }

    public function create(string $text, $maxLength = 300)
    {
        $text = trim($this->plaintextPurifier->purify($text));

        if (preg_match('/^(.+)\R/', $text, $matches)) {
            if (mb_strlen($matches[1], $this->getCharset()) <= $maxLength) {
                return $matches[1];
            }
        }

        if (mb_strlen($text, $this->getCharset()) <= $maxLength) {
            return $text;
        }

        $maxAllowedText = mb_substr($text, 0, $maxLength + 1, $this->getCharset());
        $lastSpacePosition = mb_strrpos($maxAllowedText, ' ', 0, $this->getCharset());

        return mb_substr($maxAllowedText, 0, $lastSpacePosition, $this->getCharset()) . ' [...]';
    }

    private function getCharset()
    {
        if (defined('LANG_CHARSET')) {
            return LANG_CHARSET;
        }

        return 'UTF-8';
    }
}