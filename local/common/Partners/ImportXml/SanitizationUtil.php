<?php

namespace Rybalka\Marketplace\Partners\ImportXml;

use HTMLPurifier;

class SanitizationUtil
{
    private const ALLOWED_DESCRIPTION_TAGS = 'p,div,span,ul,li,ol,br';

    /**
     * @var HTMLPurifier
     */
    private $plaintextPurifier;

    /**
     * @var HTMLPurifier
     */
    private $descriptionPurifier;

    /**
     * @var HTMLPurifier
     */
    private $brAllowedPurifier;

    public function __construct()
    {
        $purifierFactory = new PurifierFactory();

        $this->plaintextPurifier = $purifierFactory->plaintextPurifier();
        $this->descriptionPurifier = $purifierFactory->limitedTagsPurifier(
            self::ALLOWED_DESCRIPTION_TAGS
        );
        $this->brAllowedPurifier = $purifierFactory->limitedTagsPurifier('br');
    }

    public function description(string $description): string
    {
        $description = trim($this->descriptionPurifier->purify($description));
        $description = preg_replace('/\s+/', ' ', $description);

        return $description;
    }

    public function title(string $title): string
    {
        return $this->oneLineString($title);
    }

    public function parameterName(string $name): string
    {
        return $this->oneLineString($name);
    }

    public function parameterValue(string $va): string
    {
        return $this->oneLineString($va);
    }

    public function multiValueParameterValue(string $value): array
    {
        $purified = $this->brAllowedPurifier->purify($value);

        $values = explode('<br />', $purified);
        $resultValues = [];

        foreach ($values as $key => $value) {
            $value = $this->removeRedundantSpaces($value);

            if (!empty($value)) {
                $resultValues[] = $value;
            }
        }

        return $resultValues;
    }

    private function oneLineString(string $value): string
    {
        $value = $this->plaintextPurifier->purify($value);

        return $this->removeRedundantSpaces($value);
    }

    private function removeRedundantSpaces(string $value): string
    {
        $value = trim($value);
        return preg_replace('/\s+/', ' ', $value);
    }
}