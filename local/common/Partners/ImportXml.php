<?php

namespace Rybalka\Marketplace\Partners;

use Bitrix\Catalog\Model\Price;
use Bitrix\Catalog\PriceTable;
use Bitrix\Catalog\ProductTable;
use CCatalogProduct;
use CFile;
use CIBlockElement;
use Monolog\Logger;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\PartnerShop\Import\ExternalCatalogItemParameter;
use Rybalka\Marketplace\Model\PartnerShop\Import\ImportConfiguration;
use Rybalka\Marketplace\Partners\Import\ExternalCategory;
use Rybalka\Marketplace\Partners\ImportXml\CustomParams\KeySanitizer;
use Rybalka\Marketplace\Partners\ImportXml\CustomParams\ValueSanitizer;
use Rybalka\Marketplace\Partners\ImportXml\PicturesImportQueueService;
use Rybalka\Marketplace\Partners\ImportXml\SanitizationUtil;
use Rybalka\Marketplace\Partners\ImportXml\TextPreviewUtil;
use Rybalka\Marketplace\Service\PartnerShop\Import\DiscountsService;
use Rybalka\Marketplace\Service\PartnerShop\Import\ExternalCatalogItemParametersService;
use Rybalka\Marketplace\Service\PartnerShop\Import\ExternalCategoriesService;
use Rybalka\Marketplace\Service\PartnerShop\Import\ParametersMappingService;
use Rybalka\Marketplace\Service\PartnerShopService;
use Rybalka\Marketplace\Service\System\ProcessLockingService;
use Rybalka\Marketplace\Util\DataStructure\Map\ArrayMap;
use Rybalka\Marketplace\Util\DataStructure\Map\Decorator\SanitizationDecorator;
use Rybalka\Marketplace\Util\DataStructure\Map\Map;
use Rybalka\Marketplace\Util\FeatureToggle;
use Rybalka\Marketplace\Util\LoggerFactory;
use Rybalka\Marketplace\Util\PartnerShop\Import\DiscountsUtil;
use Rybalka\Marketplace\Util\PartnerShop\Import\NormalizationUtil;
use Rybalka\Marketplace\Util\PropUtil;
use Rybalka\Marketplace\Util\StringUtil;
use Rybalka\Marketplace\Util\System\Timer;
use SimpleXMLElement;

class ImportXml
{
    const DEFAULT_CURRENCY = 'UAH';

    const LOG_TYPE_ERROR = 'errors';
    const LOG_TYPE_WARNING = 'warnings';
    const LOG_TYPE_INFO = 'info';

    const PREVIEW_MAXLENGTH = 600;
    const PARAM_NAME_VENDOR = 'Производитель';
    const PARAM_NAME_URL = 'URL';
    const CACHE_MAPPED_PROPERTIES = 'MAPPED_PROPERTIES';

    private $_log = [
		'successes' => [],
		self::LOG_TYPE_WARNING => [],
		self::LOG_TYPE_ERROR => [],
	];
	private $_results = [
		'categories' => [
			'total' => 0,
		],
		'offers' => [
			'total' => 0,
			'new' => 0,
			'updated' => 0,
		],
		'params' => [
			'total' => 0
		],
	];
	private $_xml;

	private $cache = [];

    /**
     * @var ImportConfiguration
     */
    private $importConfiguration;

    private $externalCategoriesService;

    /**
     * @var ExternalCatalogItemParametersService
     */
    private $externalParamsService;

    /**
     * @var SanitizationUtil
     */
    private $sanitizationUtil;

    /**
     * @var TextPreviewUtil
     */
    private $textPreviewUtil;

    /**
     * @var ParametersMappingService
     */
    private $paramsMappingService;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var NormalizationUtil
     */
    private $normalizationUtil;

    /**
     * @var ProcessLockingService
     */
    private $lockService;

    /**
     * @var DiscountsService
     */
    private $discountsService;

    /**
     * @var DiscountsUtil
     */
    private $discountsUtil;

    /**
     * @var PicturesImportQueueService
     */
    private $picturesImportQueueService;

    public function __construct(ImportConfiguration $config)
	{
        $this->logger = (new LoggerFactory())->getCatalogImportLogger($config->getPartnerId());

	    $this->importConfiguration = $config;

	    $partnerShopService = new PartnerShopService();

	    $partnerShop = $partnerShopService->loadByIdOrFail($config->getPartnerId());

	    $this->externalCategoriesService = new ExternalCategoriesService($partnerShop);
	    $this->sanitizationUtil = new SanitizationUtil();
	    $this->textPreviewUtil = new TextPreviewUtil();

        $this->externalParamsService = new ExternalCatalogItemParametersService($partnerShop);
        $this->paramsMappingService = new ParametersMappingService($this->logger);

        $this->discountsService = new DiscountsService($this->logger);
        $this->discountsUtil = new DiscountsUtil();

        $this->picturesImportQueueService = new PicturesImportQueueService($this->logger);

        $this->normalizationUtil = new NormalizationUtil();

        $this->lockService = new ProcessLockingService();
    }

	public function getLog($part = false)
	{
		if($part)
		{
			return $this->_log[$part];
		}

		return $this->_log;
	}

	public function noErrors()
	{
		return count($this->_log[self::LOG_TYPE_ERROR]) == 0;
	}

	public function getParamValues($paramName)
	{
		$xmlOffers = $this->_xml->shop->offers->offer;

		$data = [];
		foreach ($xmlOffers as $xmlOffer) {
			foreach ($xmlOffer->param as $param) {
				if($paramName == (string) $param->attributes()->name) {
					$value = (string) $param;
					if (!in_array($value, $data)) {
						$data[md5($value)] = $value;
					}
				}
			}
		}

		return $data;
	}

	private function add2log(string $type, string $message, array $context = [])
	{
	    switch ($type) {
            case self::LOG_TYPE_INFO:
                $this->logger->info($message, $context);
                break;
            case self::LOG_TYPE_WARNING:
                $this->logger->warning($message, $context);
                break;
            case self::LOG_TYPE_ERROR:
                $this->logger->error($message, $context);
                break;
            default:
                $this->logger->debug($message, $context);
                break;
        }
	}

	private function add2results($type, $subtype, $val = 1)
	{
		return ($this->_results[$type][$subtype] += $val);
	}

	public function getResults($type = false, $subtype = false)
	{
		if($type && $subtype)
		{
			return $this->_results[$type][$subtype];
		}
		elseif($type)
		{
			return $this->_results[$type];
		}
		else
		{
			return $this->_results;
		}
	}

    public function processShopOffers(): bool
    {
        if (!FeatureToggle::getInstance()->isCatalogImportOn()) {
            $this->add2log(self::LOG_TYPE_ERROR, 'Catalog import is not enabled, stopping import process');
            return false;
        }

        $this->add2log(self::LOG_TYPE_INFO,'Start: Catalog import', [
            'importConfigurationId' => $this->importConfiguration->getId(),
            'partnerId' => $this->importConfiguration->getPartnerId(),
            'url' => $this->importConfiguration->getUrl()
        ]);

        $timerForImport = Timer::create();

        $lock = $this->lockService->acquireLock('catalog-import-' . $this->importConfiguration->getPartnerId());

        if (!$lock) {
            $this->add2log(
                self::LOG_TYPE_ERROR,
                'Failed to acquire lock, no import will be performed'
            );

            return false;
        }

        $xml = $this->loadXmlFile($this->importConfiguration);

        if (empty($xml)) {
            $this->add2log(
                self::LOG_TYPE_ERROR,
                'Failed to load xml with catalog items',
                [
                    'url' => $this->importConfiguration->getUrl()
                ]
            );

            return false;
        }

        $this->add2log(self::LOG_TYPE_INFO, 'Start: Categories import');
        $this->externalCategoriesService->import($xml->shop->categories);
        $xmlOffers = $xml->shop->offers->offer;
        $this->add2log(self::LOG_TYPE_INFO, 'End: Categories import');


        $this->add2results('categories', 'total', count($xml->shop->categories->category));
        $this->add2results('offers', 'total', count($xmlOffers));

        $this->deactivateMissingOffers($xml);

        $this->importAllDefinedParameters($xml);

        if (!count($xmlOffers)) {
		    $this->add2log(self::LOG_TYPE_ERROR, 'No offers found');
		    return true;
        }

        $counter = 0;
        $total = $xmlOffers->count();

        foreach ($xmlOffers as $xmlOffer) {

            $lock->refresh();
            $timer = Timer::create();

            $counter++;

            $this->add2log(self::LOG_TYPE_INFO, 'Start: Offer processing', [
                'offerNumber' => $counter,
                'total' => $total
            ]);

            $offerId = (string) ($xmlOffer->attributes()->id ?? '');
            $offerId = $this->normalizationUtil->externalOfferId($offerId);

            if (empty($offerId)) {
                $this->add2log(self::LOG_TYPE_ERROR, 'Offer doesn\'t have an id');
                continue;
            }

            $checksum = $this->calculateOfferChecksum($xmlOffer);

            if (!$this->isOfferNewOrHasChanges($offerId, $checksum)) {
                $this->add2log(self::LOG_TYPE_INFO, 'The offer hasn\'t changed since last import, skipping', [
                    'offerId' => $offerId
                ]);
                continue;
            }

            // Params
            $params = [
                'IMPORT_OFFER_CHECKSUM' => $checksum
            ];

            // Currency
            $currencyId = (string)$xmlOffer->currencyId;

            if(empty($currencyId)) {
                $currencyId = self::DEFAULT_CURRENCY;
                $this->add2log(
                    self::LOG_TYPE_WARNING,
                    'No currency defined for the offer, default is set',
                    [
                        'offerId' => $offerId
                    ]
                );
            }

            if (!empty($currencyId) && $currencyId != self::DEFAULT_CURRENCY) {
                $this->add2log(self::LOG_TYPE_ERROR,
                    'Currency of the offer is different from UAH',
                    [
                        'currency' => $currencyId,
                        'offerId' => $offerId
                    ]
                );
                continue;
            }

            $categoryId = (int)$xmlOffer->categoryId;

            if (empty($categoryId)) {
                $this->add2log(
                    self::LOG_TYPE_ERROR,
                    'No category specified for the offer',
                    [
                        'offerId' => $offerId
                    ]);
                continue;
            }

            $internalCategoryId = $this->externalCategoriesService->getInternalCategoryFor($categoryId);

            if (is_null($internalCategoryId)) {
                $this->add2log(
                    self::LOG_TYPE_WARNING,
                    'No mapping defined for a category',
                    [
                        'offerId' => $offerId,
                        'categoryId' => $categoryId
                    ]
                );
                continue;
            }

            $customOfferParams = $this->createMapForCustomParams();

            if (!$this->addVendorToParamsIfSpecified($xmlOffer)) {
                $this->add2log(
                    self::LOG_TYPE_WARNING,
                    'No vendor name specified for an offer',
                    [
                        'offerId' => $offerId
                    ]
                );
            }

            if (!$this->addUrlToParamsIfSpecified($xmlOffer)) {
                $this->add2log(
                    self::LOG_TYPE_WARNING,
                    'No offer URL specified for an offer',
                    [
                        'offerId' => $offerId
                    ]
                );
            }

            foreach ($xmlOffer->param as $param) {
                $paramName = trim((string) $param->attributes()->name);
                $paramValue = trim((string) $param);

                if (empty($paramName) || empty($paramValue)) {
                    $this->add2log(
                        self::LOG_TYPE_WARNING,
                        'Empty name or value of a parameter for an offer',
                        [
                            'offerId' => $offerId,
                            'paramName' => $paramName,
                            'paramValue' => $paramValue
                        ]);
                    continue;
                }

                $externalParam = $this->externalParamsService->getWithMappingBy($paramName, $categoryId);

                if (is_null($externalParam)) {
                    $customOfferParams->add($paramName, $paramValue);
                    $this->add2log(
                        self::LOG_TYPE_INFO,
                        'No mapping defined for a parameter',
                        [
                            'offerId' => $offerId,
                            'paramName' => $paramName
                        ]
                    );
                    continue;
                }

                if ($externalParam->isIgnored()) {
                    $this->add2log(
                        self::LOG_TYPE_INFO,
                        'Parameter marked as ignored',
                        [
                            'offerId' => $offerId,
                            'paramName' => $paramName
                        ]
                    );
                    continue;
                }

                $value = $this->paramsMappingService->getInternalValueFor($externalParam, $paramValue);

                if (is_null($value)) {
                    $customOfferParams->add($paramName, $paramValue);
                    $this->add2log(
                        self::LOG_TYPE_WARNING,
                        'No mapping for a param found',
                        [
                            'offerId' => $offerId,
                            'paramName' => $paramName,
                            'paramValue' => $paramValue
                        ]
                    );

                    continue;
                }

                $params[$externalParam->getInternalCode()] = $value;
            }

            $offerData = [
                'available' => (string) $xmlOffer->attributes()->available == 'true',
                'currency' => $currencyId,
                'category' => $internalCategoryId,
                'pictures' => (array) $xmlOffer->picture,
                'params' => $params,
                'name' => (string) $xmlOffer->name,
                'prices' => [
                    'current' => (float) $xmlOffer->price
                ],
                'quantity' => (float) $xmlOffer->stock_quantity,
                'description' => (string) $xmlOffer->description,
                'customParams' => $customOfferParams
            ];

            if (!empty($xmlOffer->price_old)) {
                $offerData['prices']['old'] = (float)$xmlOffer->price_old;
            }

            // IMPORT
            $this->saveOffer(
                $offerId,
                $offerData
            );

            $this->add2log(self::LOG_TYPE_INFO, 'Finish: Offer processing', [
                'elapsedTime' => $timer->elapsedTime()
            ]);
        }

        $this->picturesImportQueueService->resolveQueue();

        $lock->release();

        $this->add2log(self::LOG_TYPE_INFO,'Finish: Catalog import', [
            'importConfigurationId' => $this->importConfiguration->getId(),
            'partnerId' => $this->importConfiguration->getPartnerId(),
            'elapsedTime' => $timerForImport->elapsedTime()
        ]);

        return true;
	}

	private function saveOffer($offerId, $offerData)
	{
	    $partnerId = $this->importConfiguration->getPartnerId();

		// Check does appropriate product exist
		$productId = false;
		$res = CIBlockElement::GetList(
			[],
			[
				'IBLOCK_ID' => ITEMS_IBLOCK,
				'PROPERTY_IMPORT_OFFER_ID' => $offerId,
				'PROPERTY_PARTNER' => $partnerId,
			],
			false,
			false,
			['ID']
		);
		if(($product = $res->Fetch()) && $product['ID'])
		{
			$productId = $product['ID'];
		}

		// Add or update product
		if($productId)
		{
			if($this->updateProduct($partnerId, $productId, $offerId, $offerData))
			{
				$this->add2results('offers', 'updated');
			}
		}
		else
		{
			if($this->addProduct($partnerId, $offerId, $offerData))
			{
				$this->add2results('offers', 'new');
			}
		}
	}

	private function addProduct($partnerId, $offerId, $offerData)
	{
		$el = new CIBlockElement;

		$PROP = $offerData['params'];
		$PROP['IMPORT_OFFER_ID'] = $offerId;
        $PROP['IMPORT_OFFER_CUSTOM_PROPERTIES'] = $offerData['customParams']->serialize();

		$PROP['PARTNER'] = $partnerId;

		$discountRate = 0;

		if (!empty($offerData['prices']['old'])) {
		    $discountRate = $this->discountsUtil->calculateDiscountRate(
		        $offerData['prices']['current'],
                $offerData['prices']['old']
            );

		    if (!empty($discountRate)) {
                $PROP['IMPORT_OFFER_DISCOUNT_RATE'] = $discountRate;
                $PROP['PROMOTIONAL'] = 'Y';
            }
        }

		$elementData = [
			'IBLOCK_SECTION_ID' => $offerData['category'],
			'IBLOCK_ID' => ITEMS_IBLOCK,
			'PROPERTY_VALUES' => $PROP,
			'NAME' => $this->sanitizationUtil->title($offerData['name']),
			'ACTIVE' => $offerData['available'] ? 'Y' : 'N',
            'PREVIEW_TEXT' => $this->textPreviewUtil
                ->create($offerData['description'], self::PREVIEW_MAXLENGTH),
			'DETAIL_TEXT' => $this->buildDescription($offerData['description'])
		];

        if (count($offerData['pictures']) > 0) {
            $elementData['DETAIL_PICTURE'] = CFile::MakeFileArray($offerData['pictures'][0]);
        } else {
            $this->add2log(self::LOG_TYPE_WARNING, 'An offer does not have images', [
                'offerId' => $offerId
            ]);
        }

        $elementId = $el->Add($elementData, false, true, true);

        if (empty($elementId)) {
            $this->add2log(self::LOG_TYPE_ERROR, 'Failed to add offer to the catalog', [
                'offerId' => $offerId,
                'error' => $el->LAST_ERROR
            ]);
            return false;
        }

        $this->picturesImportQueueService->enqueueExtra(
            $elementId,
            array_slice($offerData['pictures'], 1)
        );

        $result = CCatalogProduct::Add(
            [
                'ID' => $elementId,
                'QUANTITY' => $offerData['quantity'],
                'TYPE' => ProductTable::TYPE_PRODUCT,
            ]
        );

        if (!$result) {
            $this->add2log(self::LOG_TYPE_ERROR, 'Failed to initialize offer in catalog', [
                'offerId' => $offerId,
                'catalogOfferId' => $elementId,
            ]);

            return false;
        }

        $priceForCatalog = $discountRate
            ? $this->discountsUtil->calculateOriginalPrice($offerData['prices']['current'], $discountRate)
            : $offerData['prices']['current'];


        $isSuccessful = Price::add([
            'CURRENCY' => $offerData['currency'],
            'PRICE' => $priceForCatalog,
            'CATALOG_GROUP_ID' => 1,
            'PRODUCT_ID' => $elementId,
        ]);

        if (!$isSuccessful) {
            $this->add2log(
                self::LOG_TYPE_ERROR,
                'Failed to insert price for an offer',
                [
                    'offerId' => $offerId,
                    'catalogOfferId' => $elementId,
                    'error' => $GLOBALS['APPLICATION']->GetException()
                ]
            );

            return false;
        }

        if ($discountRate) {
            $this->discountsService->addDiscountFor($elementId, $discountRate);
        }

        $this->add2log(self::LOG_TYPE_INFO, 'Offer has been successfully imported', [
            'offerId' => $offerId,
            'catalogOfferId' => $elementId
        ]);

		return true;
	}

	private function updateProduct($partnerId, $productId, $offerId, $offerData)
	{
		$el = new CIBlockElement;

		// Current data
		$productData = $el->GetList(
			[],
			[
				'IBLOCK_ID' => ITEMS_IBLOCK,
				'ID' => $productId
			],
			false,
			false,
			[
				'ID',
				'CODE',
                'PROPERTY_IMPORT_OFFER_DISCOUNT_RATE'
			]
		)->Fetch();

		$PROP = $offerData['params'];
		$PROP['IMPORT_OFFER_ID'] = $offerId;
        $PROP['IMPORT_OFFER_CUSTOM_PROPERTIES'] = $offerData['customParams']->serialize();

        $discountRate = 0;
        $PROP['PARTNER'] = $partnerId;

        if (!empty($offerData['prices']['old'])) {
            $discountRate = $this->discountsUtil->calculateDiscountRate(
                $offerData['prices']['current'],
                $offerData['prices']['old']
            );

            $PROP['IMPORT_OFFER_DISCOUNT_RATE'] = $discountRate;
            $PROP['PROMOTIONAL'] = $discountRate ? 'Y' : 'N';
        }

        $currentDiscountRate = (int)$productData['PROPERTY_IMPORT_OFFER_DISCOUNT_RATE_VALUE'];

        if ($currentDiscountRate > 0 && $currentDiscountRate != $discountRate) {
            $this->discountsService->removeDiscountFor($productId, $currentDiscountRate);
        }

		if(count($offerData['pictures']) >= 1)
		{
		    $pictures = array_slice($offerData['pictures'], 1);
		    $this->picturesImportQueueService->enqueueMain($productId, $offerData['pictures'][0]);
		    $this->picturesImportQueueService->enqueueExtra($productId, $pictures ?? []);
		} else {
            $this->picturesImportQueueService->enqueueExtra($productId, []);
        }

        $elementData = [
            'NAME' => $this->sanitizationUtil->title($offerData['name']),
            'CODE' => $productData['CODE'],
            'ACTIVE' => $offerData['available'] ? 'Y' : 'N',
            'PREVIEW_TEXT' => $this->textPreviewUtil
                ->create($offerData['description'], self::PREVIEW_MAXLENGTH),
            'DETAIL_TEXT' => $this->buildDescription($offerData['description']),
		];

		$isUpdateSuccessful = $el->Update($productId, $elementData, false, true, true);

		if (!$isUpdateSuccessful) {

		    $this->add2log(self::LOG_TYPE_ERROR, 'Failed to update offer in the catalog',[
                'offerId' => $offerId,
		        'catalogOfferId' => $productId,
                'error' => $el->LAST_ERROR
            ]);

		    return false;
        }

        $PROP = $this->resetValuesOfMissingProperties($PROP);
        $el->SetPropertyValuesEx($productId, InfoBlock::CATALOG, $PROP);

        $isQuantityUpdateSuccessful = CCatalogProduct::Update($productId, [
            'QUANTITY' => $offerData['quantity']
        ]);

        if (!$isQuantityUpdateSuccessful) {
            $this->add2log(self::LOG_TYPE_ERROR, 'Failed to update items quantity in the catalog', [
                'offerId' => $offerId,
                'catalogOfferId' => $productId,
                'quantity' => $offerData['quantity']
            ]);

            return false;
        }

        $price = PriceTable::getList([
            'select' => ['ID'],
            'filter' => ['PRODUCT_ID' => $productId]
        ])->fetch();

        if (empty($price)) {
            $this->add2log(self::LOG_TYPE_ERROR, 'Failed to load price object for the catalog item', [
                'offerId' => $offerId,
                'offerCatalogId' => $productId
            ]);

            return false;
        }

        $priceForCatalog = $discountRate
            ? $this->discountsUtil->calculateOriginalPrice($offerData['prices']['current'], $discountRate)
            : $offerData['prices']['current'];


        $priceUpdateResult = PriceTable::update($price['ID'], [
            'CURRENCY' => $offerData['currency'],
            'PRICE' => $priceForCatalog,
        ]);


        if(!$priceUpdateResult->isSuccess())
        {
            $this->add2log(self::LOG_TYPE_ERROR, 'Failed to update price for the catalog item', [
                'offerId' => $offerId,
                'offerCatalogId' => $productId
            ]);

            return false;
        }

        if ($discountRate) {
            $this->discountsService->addDiscountFor($productId, $discountRate);
        }

        return true;
	}

	private function buildDescription(string $offerDescription): string
    {
        return $this->sanitizationUtil->description($offerDescription);
    }

    private function importAllDefinedParameters(SimpleXMLElement $xml)
    {
        $offers = $xml->xpath('//shop/offers/offer');
        $paramsByCategories = [];

        foreach ($offers as $offer) {
            $categoryId = $categoryId = (int)$offer->categoryId;

            if (empty($categoryId) || is_null($offer->param)) {
                continue;
            }

            $paramNames = [];

            foreach ($offer->param as $node) {
                if (is_null($node->attributes()->name)) {
                    continue;
                }

                $paramNames[] = (string)$node->attributes()->name;
            }

            $paramsByCategories[$categoryId] = array_unique(array_merge(
                $paramsByCategories[$categoryId] ?? [],
                $paramNames
            ));

            $paramsByCategories[0] = array_unique(array_merge(
                $paramsByCategories[0] ?? [],
                $paramNames
            ));
        }



        // creating a fake parameters ('vendor', 'URL') in order to avoid creation of
        // custom code to deal with those. With this little hack those will be
        // processed as any other parameter
        $paramsByCategories[0][] = self::PARAM_NAME_VENDOR;
        $paramsByCategories[0][] = self::PARAM_NAME_URL;

        $paramsToInsert = [];

        foreach ($paramsByCategories as $categoryId => $paramsInCategory) {

            foreach ($paramsInCategory as $paramName) {

                $paramsToInsert[] = ExternalCatalogItemParameter::fromRaw([
                    'EXTERNAL_PARAMETER_NAME' => $paramName,
                    'PARTNER_ID' => $this->importConfiguration->getPartnerId(),
                    'EXTERNAL_CATEGORY_ID' => $categoryId
                ]);
            }
        }

        $this->externalParamsService->addAllIfNew($paramsToInsert);
    }

    private function deactivateMissingOffers(SimpleXMLElement $xml): void
    {
        $this->add2log(self::LOG_TYPE_INFO, 'Start: Missing offers deactivation');
        $nodes = $xml->xpath('//shop/offers/offer/@id');

        $allOfferIds = [];

        foreach ($nodes as $node) {
            $allOfferIds[] = $this->normalizationUtil->externalOfferId((string)$node[0]);
        }

        $allOfferIds = array_unique($allOfferIds);

        $result = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => InfoBlock::CATALOG,
                '!PROPERTY_IMPORT_OFFER_ID' => $allOfferIds,
                'PROPERTY_PARTNER' => $this->importConfiguration->getPartnerId(),
                'ACTIVE' => 'Y',
                '>CATALOG_QUANTITY' => 0
            ],
            false,
            false,
            ['ID']
        );

        while ($catalogItem = $result->Fetch()) {

            CCatalogProduct::Update(
                $catalogItem['ID'],
                [
                    'QUANTITY' => 0
                ]
            );
        }

        $this->add2log(self::LOG_TYPE_INFO, 'Finish: Missing offers deactivation');
    }

    private function addUrlToParamsIfSpecified(SimpleXMLElement $xmlOffer): bool
    {
        return $this->addFieldToParamsIfSpecified($xmlOffer, 'url', self::PARAM_NAME_URL);
    }

    private function addVendorToParamsIfSpecified(SimpleXMLElement $xmlOffer): bool
    {
        return $this->addFieldToParamsIfSpecified($xmlOffer, 'vendor', self::PARAM_NAME_VENDOR);
    }

    private function addFieldToParamsIfSpecified(SimpleXMLElement $xmlOffer, string $fieldName, string $paramName): bool
    {
        if (is_null($xmlOffer->$fieldName)) {
            return false;
        }

        $fieldValue = trim((string)$xmlOffer->$fieldName);

        if (empty($fieldValue)) {
            return false;
        }

        $newParam = $vendor = $xmlOffer->addChild(
            'param', $fieldValue
        );
        $newParam->addAttribute('name', $paramName);

        return true;
    }

    private function resetValuesOfMissingProperties(array $properties): array
    {
        if (!isset($this->cache[self::CACHE_MAPPED_PROPERTIES])) {
            $this->cache[self::CACHE_MAPPED_PROPERTIES] = $this->externalParamsService->getAllMappings();
        }

        $allMappedProperties = $this->cache[self::CACHE_MAPPED_PROPERTIES];

        foreach ($allMappedProperties as $property) {
            /**
             * @var ExternalCatalogItemParameter $property
             */
            if (!isset($properties[$property->getInternalCode()])) {
                $properties[$property->getInternalCode()] = null;
            }
        }

        return $properties;
    }

    private function isOfferNewOrHasChanges(string $offerId, string $checksum): bool
    {
        if (FeatureToggle::getInstance()->isCatalogImportItemChangeCheckDisabled()) {
            return true;
        }

        $result = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => InfoBlock::CATALOG,
                'PROPERTY_IMPORT_OFFER_ID' => $offerId,
                'PROPERTY_IMPORT_OFFER_CHECKSUM' => $checksum,
                'PROPERTY_PARTNER' => $this->importConfiguration->getPartnerId(),
            ],
            false,
            false,
            ['ID']
        );

        if ($result->Fetch()) {
            return false;
        }

        return true;
    }

    private function calculateOfferChecksum(SimpleXMLElement $xmlOffer): string
    {
        return crc32($xmlOffer->asXML());
    }

    private function loadXmlFile(ImportConfiguration $config): SimpleXMLElement
    {
        $result = null;

        libxml_use_internal_errors(true);

        try {
            $result = new SimpleXMLElement($config->getUrl(), LIBXML_DTDVALID, true);
        } catch (\Exception $e) {
            foreach (libxml_get_errors() as $error) {
                $this->add2log(self::LOG_TYPE_ERROR, $error->message);
            }
            $this->add2log(self::LOG_TYPE_ERROR, $e->getMessage());
        }

        return $result;
    }

    private function createMapForCustomParams(): Map
    {
        return (new SanitizationDecorator(new ArrayMap()))
            ->setKeySanitizer(new KeySanitizer())
            ->setValueSanitizer(new ValueSanitizer());
    }
}