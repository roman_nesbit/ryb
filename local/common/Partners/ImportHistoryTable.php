<?php

namespace Rybalka\Marketplace\Partners;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\Type\DateTime;

class ImportHistoryTable extends Entity\DataManager
{
	public function __construct()
	{
		Loader::includeModule('iblock');
	}

	public static function getTableName()
	{
		return 'ryb_partners_import_history';
	}

	public static function getMap()
	{
		return [
			new Entity\IntegerField('ID', [
				'primary' => true,
				'autocomplete' => true,
				'title' => 'ID',
			]),
			new Entity\DatetimeField('DATE_EXEC', [
				'default_value' => function() {
					return new DateTime();
				},
				'title' => 'Дата',
			]),
			new Entity\BooleanField('EXEC_SUCCESS', [
				'title' => 'Операция успешна',
				'values' => ['N', 'Y'],
				'default_value' => 'N',
			]),
			new Entity\IntegerField('PARTNER_ID', [
				'title' => 'Партнер',
				'required' => true,
			]),
			new Entity\IntegerField('AGENT_ID', [
				'title' => 'Агент',
				'required' => true,
			]),
			new Entity\IntegerField('CATEGORIES_NEW', [
				'title' => 'Категорий новых',
			]),
			new Entity\IntegerField('CATEGORIES_TOTAL', [
				'title' => 'Категорий всего',
			]),
			new Entity\IntegerField('PRODUCTS_NEW', [
				'title' => 'Товаров новых',
			]),
			new Entity\IntegerField('PRODUCTS_DEACTIVATED', [
				'title' => 'Товаров деактивировано',
			]),
			new Entity\IntegerField('PRODUCTS_TOTAL', [
				'title' => 'Товаров всего',
			]),
			new Entity\IntegerField('PROPERTIES_NEW', [
				'title' => 'Параметров новых',
			]),
			new Entity\IntegerField('PROPERTIES_TOTAL', [
				'title' => 'Параметров всего',
			]),
			new Entity\StringField('LOG', [
				'title' => 'Лог файл',
			]),

			new Reference(
				'PARTNER',
				'\Bitrix\Iblock\Element',
				['=this.PARTNER_ID' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
			new Reference(
				'AGENT',
				'\Rybalka\Marketplace\Agent\AgentTable',
				['=this.AGENT_ID' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
		];
	}

	private function getLog($id)
	{
		$res = $this->getList([
			'select' => ['LOG'],
			'filter' => [
				'ID' => $id
			]
		]);
		$data = $res->fetch();
		return $data['LOG'];
	}

	public function save($id, $log, $results)
	{
		$result = $this->update($id, [
			'CATEGORIES_TOTAL' => $results['categories']['total'],
			'PRODUCTS_TOTAL' => $results['offers']['total'],
			'PRODUCTS_NEW' => $results['offers']['new'],
		]);

		if($result->isSuccess())
		{
			$logFile = $_SERVER["DOCUMENT_ROOT"] . $this->getLog($id);
			foreach($log as $type => $sublog)
			{
				error_log(strtoupper($type) . PHP_EOL, 3, $logFile);
				foreach($sublog as $logText)
				{
					error_log(" - " . $logText . PHP_EOL, 3, $logFile);
				}
				error_log(PHP_EOL, 3, $logFile);
			}
		}
	}
}