<?php

namespace Rybalka\Marketplace\Partners;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\Type\DateTime;
use CAgent;
use CUser;
use Rybalka\Marketplace\Model\PartnerShop\Import\ImportConfiguration;
use \Rybalka\Marketplace\Partners\ImportHistoryTable;
use \Rybalka\Marketplace\Agent\AgentTable;
use \Rybalka\Marketplace\Partners\ImportXml;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCategoriesTable;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCatalogItemParametersTable;

class ImportTable extends Entity\DataManager
{
	public function __construct()
	{
		Loader::includeModule('iblock');
	}

	public static function getTableName()
	{
		return 'ryb_partners_import';
	}

	public static function getMap()
	{
		return [
			new Entity\IntegerField('ID', [
				'primary' => true,
				'autocomplete' => true,
				'title' => 'ID',
			]),
			new Entity\DatetimeField('TIMESTAMP_X', [
				'default_value' => new DateTime(),
				'title' => 'Дата изменения',
			]),
			new Entity\IntegerField('MODIFIED_BY', [
				'title' => 'Кто изменил',
				'default_value' => function() {
					return CUser::getId();
				}
			]),
			new Entity\DatetimeField('DATE_CREATE', [
				'default_value' => function() {
					return new DateTime();
				},
				'title' => 'Дата создания',
			]),
			new Entity\IntegerField('CREATED_BY', [
				'title' => 'Кем создан',
				'default_value' => function() {
					return CUser::getId();
				}
			]),
			new Entity\BooleanField('ACTIVE', [
				'values' => ['N', 'Y'],
				'default_value' => 'Y',
				'title' => 'Активно',
			]),
			new Entity\IntegerField('SORT', [
				'default_value' => 500,
				'title' => 'Индекс сортировки',
			]),
			new Entity\IntegerField('PARTNER_ID', [
				'title' => 'Партнер',
				'required' => true,
			]),
			new Entity\StringField('URL', [
				'title' => 'Ссылка на файл',
				'required' => true,
			]),
			new Entity\IntegerField('PERIOD', [
				'title' => 'Периодичность',
				'required' => true,
				'values' => [24, 12, 6, 3],
				'default_value' => 24,
			]),
			new Entity\BooleanField('LAST_EXEC_SUCCESS', [
				'title' => 'Последняя операция успешна',
				'values' => ['N', 'Y'],
				'default_value' => 'N',
			]),
			new Entity\IntegerField('AGENT_ID', [
				'title' => 'Агент',
			]),

			new Reference(
				'MODIFIED_BY_USER',
				'\Bitrix\Main\User',
				['=this.MODIFIED_BY' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
			new Reference(
				'CREATED_BY_USER',
				'\Bitrix\Main\User',
				['=this.CREATED_BY' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
			new Reference(
				'PARTNER',
				'\Bitrix\Iblock\Element',
				['=this.PARTNER_ID' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
			new Reference(
				'AGENT',
				'\Rybalka\Marketplace\Agent\AgentTable',
				['=this.AGENT_ID' => 'ref.ID'],
				['join_type' => 'LEFT']
			),
			// new Reference(
			// 	'MAPPING',
			// 	'\Rybalka\Marketplace\Partners\ImportMappingTable',
			// 	['=this.ID' => 'ref.IMPORT_ID'],
			// 	['join_type' => 'LEFT']
			// )
		];
	}

	public static function save($id, $fields, $mapping)
	{
		$resultId = false;

		if ($id > 0)
		{
			$res = static::update($id, $fields);
			if ($res->isSuccess()) {
				$resultId = $id;
			}
		} 
		else 
		{
			$res = static::add($fields);
			if ($res->isSuccess()) {
				$resultId = $res->getId();
			}
		}

		if($resultId)
		{
			// exo('save mapping');

			$partnerId = $fields['PARTNER_ID'];

			// Save all categories mappings
			$repositoryCategories = new ExternalCategoriesTable();

			foreach($mapping['CATEGORIES'] as $externalCategoryId => $categogoryData) {
				$repositoryCategories->updateRecord($partnerId, $externalCategoryId, $categogoryData);
			}

			// Save all params mappings
			$repositoryParams = new ExternalCatalogItemParametersTable();

			foreach ($mapping['PARAMS'] as $externalParameterCode => $parameterData) {
				$repositoryParams->updateRecord($externalParameterCode, $parameterData);
			}
		}

		return $resultId;
	}

	public static function onBeforeAdd(Entity\Event $event)
	{
		$result = new Entity\EventResult;
		$result->modifyFields([
			'TIMESTAMP_X' => new DateTime(),
			'CREATED_BY' => CUser::GetId(),
		]);
		return $result;
	}

	public static function onAfterAdd(Entity\Event $event)
	{
		$arPrimary = $event->getParameter('primary');
		$arFields = $event->getParameter('fields');

		$id = $arPrimary['ID'];

		// Add Agent
		$agentId = CAgent::AddAgent(
			'\Rybalka\Marketplace\Partners\ImportTable::doExec(' . $id . ');',
			'ryb.import',
			'N',
			$arFields['PERIOD'] * 3600
		);

		// Agent ID storing
		if($agentId) {
			$result = static::update($id, [
				'AGENT_ID' => $agentId
			]);

			return $result->isSuccess();
		} else {
			return false;
		}
	}

	public static function onBeforeUpdate(Entity\Event $event)
	{
		$result = new Entity\EventResult;
		$result->modifyFields([
			'TIMESTAMP_X' => new DateTime(),
			'MODIFIED_BY' => CUser::GetId(),
		]);
		return $result;
	}

	public static function onAfterUpdate(Entity\Event $event)
	{
		$arPrimary = $event->getParameter('primary');
		$arFields = $event->getParameter('fields');

		if($agentId = static::getAgentId($arPrimary['ID']))
		{
			// Modify Agent's period
			AgentTable::update($agentId, [
				'AGENT_INTERVAL' => static::getPeriod($arPrimary['ID']) * 3600,
				'ACTIVE' => $arFields['ACTIVE'],
			]);
		}
	}

	private static function getAgentId($id) 
	{
		$res = static::getList([
			'select' => ['AGENT_ID'],
			'filter' => [
				'ID' => $id
			]
		]);
		$data = $res->fetch();
		return $data['AGENT_ID'];
	}

	public static function getPartnerId($id)
	{
		$res = static::getList([
			'select' => ['PARTNER_ID'],
			'filter' => [
				'ID' => $id
			]
		]);
		$data = $res->fetch();
		return $data['PARTNER_ID'];
	}

	private static function getPeriod($id)
	{
		$res = static::getList([
			'select' => ['PERIOD'],
			'filter' => [
				'ID' => $id
			]
		]);
		$data = $res->fetch();
		return $data['PERIOD'];
	}

	public static function execAgent($id) 
	{
		if($id && $agentId = static::getAgentId($id))
		{
			// Set agent's next execition time = now
			AgentTable::update($agentId, [
				'NEXT_EXEC' => new DateTime(),
			]);
		}
	}

	public static function onBeforeDelete(Entity\Event $event)
	{
		// Delete Agent
		$arPrimary = $event->getParameter('primary');
		
		$id = $arPrimary['ID'];
		$agentId = static::getAgentId($id);
		
		if($agentId)
		{
			CAgent::Delete($agentId);
		}
	}

	public static function activate($id)
	{
		$result = static::update($id, [
			'ACTIVE' => 'Y'
		]);

		return $result->isSuccess();
	}

	public static function deactivate($id)
	{
		$result = static::update($id, [
			'ACTIVE' => 'N'
		]);

		return $result->isSuccess();
	}

	public static function getPartners()
	{
		$arIds = [];

		$res = static::getList([
			'select' => ['PARTNER_ID'],
			'group' => ['PARTNER_ID'],
		]);
		while($partner = $res->fetch())
		{
			$arIds[] = $partner['PARTNER_ID'];
		}

		return $arIds;
	}

	public static function checkAgents()
	{
		$agentsTotal = 0;
		$agentsCreated = 0;

		// get all records
		$res = static::getList([
			'select' => [
				'ID',
				'AGENT_ID',
				'PERIOD',
			],
		]);

		while ($data = $res->fetch()) {
			if ($data['AGENT_ID']) {
				// Check for agent by ID
				$resAgent = AgentTable::getList(
					[
						'select' => ['ID'],
						'filter' => [
							'ID' => $data['AGENT_ID'],
						]
					]
				);
				if (!$resAgent->fetch()) {
					$agentId = CAgent::AddAgent(
						'\Rybalka\Marketplace\Partners\ImportTable::doExec(' . $data['ID'] . ');',
						'ryb.import',
						'N',
						$data['PERIOD'] * 3600
					);

					// Agent ID storing
					if ($agentId) {
						$result = ImportTable::update($data['ID'], [
							'AGENT_ID' => $agentId
						]);

						$agentsCreated++;
					}
				}
			}

			$agentsTotal++;
		}

		return [
			'total' => $agentsTotal,
			'created' => $agentsCreated,
		];
	}

	public static function doExec($id)
	{
		// Find data for current import
		$resImport = static::getList([
			'filter' => [
				'ID' => $id
			]
		]);

		if($import = $resImport->fetch())
		{
			// DO EXEC
            $importConfiguration = ImportConfiguration::fromRawData($import);

			$importService = new ImportXml($importConfiguration);

			$isCompleted = $importService->processShopOffers($id);

			if (!$isCompleted) {
			    return '\Rybalka\Marketplace\Partners\ImportTable::doExec(' . $id . ');';
            }

            // Add new history record
            $iht = new ImportHistoryTable();
            $result = $iht->add([
                'DATE_EXEC' => new DateTime(),
                'EXEC_SUCCESS' => $importService->noErrors() ? 'Y' : 'N',
                'PARTNER_ID' => $import['PARTNER_ID'],
                'AGENT_ID' => $import['AGENT_ID'],
                'CATEGORIES_NEW' => 0,
                'CATEGORIES_TOTAL' => 0,
                'PRODUCTS_NEW' => 0,
                'PRODUCTS_DEACTIVATED' => 0,
                'PRODUCTS_TOTAL' => 0,
                'PROPERTIES_NEW' => 0,
                'PROPERTIES_TOTAL' => 0,
                'LOG' => '/logs/import/' . $import['PARTNER_ID'] . '_' . $import['AGENT_ID'] . '_' . time() . '.log',
            ]);

            if($result->isSuccess())
            {
                $iht->save($result->getId(), $importService->getLog(), $importService->getResults());
            }
		}

		return '\Rybalka\Marketplace\Partners\ImportTable::doExec(' . $id . ');';
	}
}