<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;

class OkException extends HttpException {
    public function __construct($message)
    {
        parent::__construct(StatusCode::OK, $message);
    }
}