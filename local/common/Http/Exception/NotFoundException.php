<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;

class NotFoundException extends HttpException
{
    public function __construct($message)
    {
        parent::__construct(StatusCode::NOT_FOUND, $message);
    }
}