<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;

class UnauthorizedException extends HttpException
{
    public function __construct($message = null)
    {
        parent::__construct(StatusCode::UNAUTHORIZED, $message ?? 'Unauthorized');
    }
}