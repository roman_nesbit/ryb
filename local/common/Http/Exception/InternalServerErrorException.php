<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;
use Throwable;

class InternalServerErrorException extends HttpException
{
    public function __construct($message = '', Throwable $originalException = null) {
        if (empty($message)) {
            $message = 'Failed to process a request due to a technical issue';
        }

        parent::__construct(StatusCode::INTERNAL_SERVER_ERROR, $message, $originalException);
    }
}