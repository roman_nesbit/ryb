<?php

namespace Rybalka\Marketplace\Http\Exception;

use Exception;
use Throwable;

class HttpException extends Exception
{
    private $httpCode;

    private $responseBody = [];
    private $userErrorMessage;
    private $fieldErrors = [];

    public function __construct(int $httpCode, string $message, Throwable $originalException = null) {
        $this->httpCode = $httpCode;
        parent::__construct($message, $httpCode, $originalException);
    }

    public function withUserErrorMessage(string $userErrorMessage)
    {
        $this->userErrorMessage = $userErrorMessage;
        return $this;
    }

    public function withResponseBody(array $responseBody)
    {
        $this->responseBody = $responseBody;
        return $this;
    }

    public function withFieldError(string $fieldId, string $errorMessage)
    {
        $this->fieldErrors[$fieldId] = $errorMessage;
        return $this;
    }

    public function getUserErrorMessage(): ?string
    {
        return $this->userErrorMessage;
    }

    public function getResponseBody(): array
    {
        return $this->responseBody;
    }

    public function getFieldErrors(): array
    {
        return $this->fieldErrors;
    }

    public function getHttpCode() {
        return $this->httpCode;
    }
}