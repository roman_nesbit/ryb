<?php


namespace Rybalka\Marketplace\Http\Exception;


use Rybalka\Marketplace\Http\StatusCode;
use Throwable;

class ConflictException extends HttpException
{
    public function __construct(string $message)
    {
        parent::__construct(StatusCode::CONFLICT, $message);
    }
}