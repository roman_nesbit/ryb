<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;

class TooManyRequestsException extends HttpException
{
    public function __construct($message)
    {
        parent::__construct(StatusCode::TOO_MANY_REQUESTS, $message);
    }
}