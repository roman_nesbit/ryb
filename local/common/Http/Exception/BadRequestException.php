<?php

namespace Rybalka\Marketplace\Http\Exception;

use Rybalka\Marketplace\Http\StatusCode;

class BadRequestException extends HttpException {
    public function __construct($message)
    {
        parent::__construct(StatusCode::BAD_REQUEST, $message);
    }
}