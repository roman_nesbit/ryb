<?php


namespace Rybalka\Marketplace\Repository;


use Bitrix\Main\Config\Option;

class SettingsRepository
{
    const MODULE_NAME = "grain.customsettings";

    const SETTING_ORDER_STATUS_CHAINS = 'ORDER_STATUS_CHAINS';

    private $cache = [];

    public function getOrderStatusTransitions()
    {
        if (!isset($this->cache[self::SETTING_ORDER_STATUS_CHAINS])) {
            $this->cache[self::SETTING_ORDER_STATUS_CHAINS] = Option::get(self::MODULE_NAME, self::SETTING_ORDER_STATUS_CHAINS);
        }

        return $this->cache[self::SETTING_ORDER_STATUS_CHAINS];
    }
}