<?php


namespace Rybalka\Marketplace\Repository;


use Bitrix\Sale\BasketItemCollection;
use Bitrix\Sale\Order as BitrixOrder;
use Bitrix\Sale\OrderBase;
use DateTime;
use Rybalka\Marketplace\Exception\InvalidStateException;
use Rybalka\Marketplace\Model\Order;
use Rybalka\Marketplace\Model\Order\Status;
use Rybalka\Marketplace\Model\OrderCollection;
use Rybalka\Marketplace\Model\Rest\Request\OrdersFilter;
use Rybalka\Marketplace\Repository\Order\ItemRepository;
use Rybalka\Marketplace\Repository\Order\PropertyRepository;
use Rybalka\Marketplace\Repository\Order\Query\OrdersByFilterQuery;
use Rybalka\Marketplace\Repository\Order\StatusRepository;
use Rybalka\Marketplace\Util\Bitrix\Model\OrderDataExtractor;
use Rybalka\Marketplace\Util\MoneyUtil;

class OrderRepository
{
    const FIELD_STATUS_ID = 'STATUS_ID';
    const FIELD_PUBLIC_ID = 'ACCOUNT_NUMBER';
    const FIELD_DATE_INSERT = 'DATE_INSERT';

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * @var StatusRepository
     */
    private $statusRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OrderDataExtractor
     */
    private $orderDataExtractor;

    /**
     * @var MoneyUtil
     */
    private $moneyUtil;

    /**
     * @var OrdersByFilterQuery
     */
    private $ordersByFilterQuery;

    public function __construct()
    {
        $this->propertyRepository = new PropertyRepository();
        $this->statusRepository = new StatusRepository();
        $this->itemRepository = new ItemRepository();

        $this->moneyUtil = new MoneyUtil();
        $this->orderDataExtractor = new OrderDataExtractor();

        $this->ordersByFilterQuery = new OrdersByFilterQuery();
    }

    public function loadByFilter(OrdersFilter $filter): OrderCollection
    {
        $orderCollection = new OrderCollection();

        $query = $this->ordersByFilterQuery->get($filter);
        $result = BitrixOrder::getList($query);

        $orderCollection->setTotalCount($result->getCount());

        while ($record = $result->fetch()) {

            $customer = (new Order\Customer())
                ->setLastName($record['CUSTOMER_LAST_NAME'])
                ->setFirstName($record['CUSTOMER_FIRST_NAME']);

            $status = (new Order\Status())
                ->setId($record['STATUS_ID'])
                ->setName($record['STATUS_NAME'])
                ->setDescription($record['STATUS_DESCRIPTION']);

            $creationDateTime = new DateTime('@' . $record['DATE_INSERT']->getTimestamp());
            $totalPrice = $this->moneyUtil->floatToMoney(
                $record['CURRENCY'],
                $record['PRICE']
            );

            $order = (new Order())
                ->setId($record['ID'])
                ->setPublicId($record['ACCOUNT_NUMBER'])
                ->setStatus($status)
                ->setItemsQuantity($record['ITEMS_QUANTITY'])
                ->setCreationDateTime($creationDateTime)
                ->setTotalPrice($totalPrice)
                ->setCustomer($customer);

            $orderCollection->add($order);
        }

        return $orderCollection;
    }

    public function loadById(int $orderId)
    {
        return BitrixOrder::load($orderId);
    }

    public function loadByPublicId(string $publicId): ?Order
    {
        /**
         * @var BitrixOrder $order
         */
        $order = BitrixOrder::loadByAccountNumber($publicId);

        if (is_null($order)) {
            return null;
        }

        $orderModel = (new Order())
            ->setId($order->getId())
            ->setPublicId($order->getField(self::FIELD_PUBLIC_ID))
            ->setTotalPrice(
                $this->moneyUtil->floatToMoney(
                    $order->getCurrency(),
                    $order->getPrice()
                )
            )
            ->setItemsQuantity(
                $this->orderDataExtractor->getPropertyValueOrFail(
                    $order,
                    PropertyRepository::PROPERTY_ITEMS_QUANTITY,
                    new InvalidStateException('No items quantity found for the order: ' . $order->getId())
                )
            )
            ->setDeliveryMethodName(
                $this->orderDataExtractor->getDeliveryMethodName($order)
            )
            ->setShopId(
                $this->propertyRepository->getShopIdByOrderId($order->getId())
            );

        $this->addStatus($orderModel, $order->getField(self::FIELD_STATUS_ID));
        $this->addCreationDateTime($order, $orderModel);

        $this->addOrderedItems($order->getBasket()->getBasket(), $orderModel);

        return $orderModel;
    }

    public function setStatus(Order $order, Status $status)
    {
        $order = BitrixOrder::load($order->getId());

        if (is_null($order)) {
            throw new InvalidStateException('Failed to load the order by id: ' . $order->getId());
        }

        $order->setField('STATUS_ID', $status->getId());
        $order->save();
    }

    private function addStatus(Order $order, string $statusId)
    {
        // TODO: Somehow get rid of the language hack
        $status = $this->statusRepository->loadByIdAndLanguage($statusId, LANGUAGE_ID);

        if ($status == null) {
            throw new InvalidStateException('No status information available for the order: ' . $order->getId());
        }

        $order->setStatus($status);
    }

    private function addCreationDateTime(BitrixOrder $bitrixOrder, Order $order)
    {
        $timestamp = '@' . MakeTimeStamp($bitrixOrder->getField(self::FIELD_DATE_INSERT));
        $order->setCreationDateTime(new DateTime($timestamp));
    }

    private function addOrderedItems(BasketItemCollection $basketItemCollection, Order $order)
    {
        $collection = new Order\ItemsCollection();

        foreach ($basketItemCollection as $basketItem) {
            $collection->add(
                $this->itemRepository->loadItemFromBitrixBasketItem($basketItem)
            );
        }

        $order->setItems($collection);
    }
}