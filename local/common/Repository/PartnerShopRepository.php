<?php


namespace Rybalka\Marketplace\Repository;


use Bitrix\Main\Loader;
use CIBlock;
use CIBlockElement;
use CIBlockSection;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock\CommonField;

class PartnerShopRepository
{
    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    public function loadById(int $partnerId)
    {
        $filter = [
            CommonField::INFO_BLOCK_ID => InfoBlock::SHOPS,
            'ID' => $partnerId
        ];


        $result = CIBlockElement::GetList([], $filter);

        if ($rawResult = $result->GetNext()) {
            return $rawResult;
        }

        return null;
    }
}