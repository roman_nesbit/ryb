<?php

namespace Rybalka\Marketplace\Repository\Order;

use Bitrix\Main\ArgumentNullException;
use Bitrix\Sale\BasketItem;
use Rybalka\Marketplace\Model\Order\Item;
use Rybalka\Marketplace\Repository\Product\PropertyRepository as ProductPropertyRepository;
use Rybalka\Marketplace\Repository\ProductRepository;
use Rybalka\Marketplace\Util\MoneyUtil;

class ItemRepository
{
    /**
     * @var ProductPropertyRepository
     */
    private $productPropertyRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var MoneyUtil
     */
    private $moneyUtil;

    public function __construct()
    {
        $this->productPropertyRepository = new ProductPropertyRepository();
        $this->productRepository = new ProductRepository();
        $this->moneyUtil = new MoneyUtil();
    }

    /**
     * @param BasketItem $basketItem
     * @return Item
     * @throws ArgumentNullException
     */
    public function loadItemFromBitrixBasketItem(BasketItem $basketItem): Item
    {
        $result = new Item();

        $productId = (int)$basketItem->getField('PRODUCT_ID');

        $result->setProductId($productId)
            ->setName($basketItem->getField('NAME'))
            ->setQuantity((int)$basketItem->getQuantity())
            ->setProductPrice(
                $this->moneyUtil->floatToMoney(
                    $basketItem->getCurrency(),
                    $basketItem->getPrice()
                )
            )
            ->setTotalPrice(
                $this->moneyUtil->floatToMoney(
                    $basketItem->getCurrency(),
                    $basketItem->getFinalPrice()
                )
            )
            ->setProductMainImage(
                $this->productRepository->getMainPictureById($productId)
            )
            ->setProductSku(
                $this->productPropertyRepository->getSkuByProductId($productId)
            );

        return $result;
    }
}