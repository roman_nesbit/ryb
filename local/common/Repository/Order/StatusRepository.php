<?php


namespace Rybalka\Marketplace\Repository\Order;


use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Sale\Internals\StatusTable;
use Rybalka\Marketplace\Model\Order\Status;
use Rybalka\Marketplace\Model\Order\StatusCollection;

class StatusRepository
{
    public function loadByIdAndLanguage(string $statusId, string $languageId): ?Status
    {
        $query = $this->genericLoadQuery($languageId);
        $query['filter']['ID'] = $statusId;

        $result = StatusTable::getList($query);

        if ($statusRawData = $result->fetch()) {
            return Status::fromRawData($statusRawData);
        }

        return null;
    }

    public function loadAllByLanguageId(string $languageId): StatusCollection
    {
        $statusCollection = new StatusCollection();

        $result = StatusTable::getList($this->genericLoadQuery($languageId));

        while ($rawData = $result->fetch()) {
            $statusCollection->add(
                Status::fromRawData($rawData)
            );
        }

        return $statusCollection;
    }

    private function genericLoadQuery(string $languageId)
    {
        return [
            'select' => [
                'ID',
                'NAME' => 'STATUS_LANG.NAME',
                'DESCRIPTION' => 'STATUS_LANG.DESCRIPTION',
            ],
            'filter' => [
                'STATUS_LANG.LID' => $languageId,
            ],
            'runtime' => [
                new ReferenceField(
                    'STATUS_LANG',
                    '\Bitrix\Sale\Internals\StatusLangTable',
                    [
                        '=ref.STATUS_ID' => "this.ID"
                    ],
                    ['join_type' => 'left']
                )
            ],
            'order' => [
                'SORT' => 'ASC'
            ]
        ];
    }

}