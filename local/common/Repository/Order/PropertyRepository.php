<?php

namespace Rybalka\Marketplace\Repository\Order;

use Bitrix\Sale\Internals\OrderPropsValueTable;

class PropertyRepository
{
    const PROPERTY_ITEMS_QUANTITY = 'COUNT_OF_ITEMS';
    const PROPERTY_SHOP_ID = 'PARTNER';
    const PROPERTY_NOTE_FROM_MODERATOR = 'NOTE_FROM_MODERATOR';

    const FIELD_PROPERTY_NAME  = 'CODE';
    const FIELD_PROPERTY_VALUE = 'VALUE';
    const FIELD_ORDER_ID = 'ORDER_ID';

    public function getShopIdByOrderId(int $orderId)
    {
        $filter = [
            self::FIELD_ORDER_ID => $orderId,
            self::FIELD_PROPERTY_NAME => self::PROPERTY_SHOP_ID
        ];

        return $this->loadPropertyByFilter($filter, self::FIELD_PROPERTY_VALUE);
    }

    private function loadPropertyByFilter(array $filter, string $propertyName)
    {
        $result = OrderPropsValueTable::getList([
            'filter' => $filter
        ]);

        if ($record = $result->Fetch()) {
            return $record[$propertyName];
        }

        return null;
    }
}