<?php

namespace Rybalka\Marketplace\Repository\Order\Query;

use Bitrix\Main\UserTable;
use Bitrix\Sale\Internals\OrderPropsValueTable;
use Bitrix\Sale\StatusLangTable;
use Rybalka\Marketplace\Exception\InvalidStateException;
use Rybalka\Marketplace\Model\Rest\Request\OrdersFilter;
use Rybalka\Marketplace\Repository\OrderRepository;
use Rybalka\Marketplace\Repository\QueryBuilder;

class OrdersByFilterQuery
{
    const FREE_TEXT_FILTER_FIELDS = [
        'CUSTOMER_FIRST_NAME',
        'CUSTOMER_LAST_NAME',
        OrderRepository::FIELD_PUBLIC_ID
    ];

    public function get(OrdersFilter $ordersFilter): array
    {
        $queryBuilder = QueryBuilder::create()
            ->countTotal()
            ->offset($ordersFilter->getOffset())
            ->limit($ordersFilter->getItemsPerPage());

        $this->addJoins($queryBuilder);
        $this->addSelectedFields($queryBuilder);
        $this->addFilters($queryBuilder, $ordersFilter);
        $this->addOrdering($queryBuilder, $ordersFilter);

        return $queryBuilder->build();
    }

    private function addJoins(QueryBuilder $builder)
    {
        $builder
            ->join(
                'PROPERTY_PARTNER',
                OrderPropsValueTable::class,
                ['=ref.ORDER_ID' => 'this.ID']
            )
            ->join(
                'PROPERTY_COUNT_OF_ITEMS',
                OrderPropsValueTable::class,
                ['=ref.ORDER_ID' => 'this.ID']
            )
            ->join(
                'USER',
                UserTable::class,
                ['=ref.ID' => 'this.USER_ID']
            )
            ->leftJoin(
                'STATUS_LANG',
                StatusLangTable::class,
                ['=ref.STATUS_ID' => 'this.STATUS_ID']
            );
    }

    private function addSelectedFields(QueryBuilder $builder)
    {
        $builder
            ->select('ID', 'STATUS_ID', 'DATE_INSERT', 'PRICE', 'CURRENCY', OrderRepository::FIELD_PUBLIC_ID)
            ->selectAlias('STATUS_NAME', 'STATUS_LANG.NAME')
            ->selectAlias('STATUS_DESCRIPTION', 'STATUS_LANG.DESCRIPTION')
            ->selectAlias('CUSTOMER_LAST_NAME', 'USER.LAST_NAME')
            ->selectAlias('CUSTOMER_FIRST_NAME', 'USER.NAME')
            ->selectAlias('ITEMS_QUANTITY', 'PROPERTY_COUNT_OF_ITEMS.VALUE');
    }

    private function addFilters(QueryBuilder $builder, OrdersFilter $ordersFilter)
    {
        if (empty($ordersFilter->getShopId())) {
            throw new InvalidStateException('No shopId provided');
        }

        $builder
            ->withField('STATUS_LANG.LID', LANGUAGE_ID)
            ->withField('PROPERTY_PARTNER.CODE', 'PARTNER')
            ->withField('PROPERTY_PARTNER.VALUE', $ordersFilter->getShopId())
            ->withField('PROPERTY_COUNT_OF_ITEMS.CODE', 'COUNT_OF_ITEMS');

        if ($ordersFilter->getDateFrom() != null) {
            $builder->withFieldGreaterThan(
                'DATE_INSERT',
                $ordersFilter->getDateFrom()->format('d.m.Y 00:00:00')
            );
        }

        if ($ordersFilter->getDateTo() != null) {
            $builder->withFieldLowerThan(
                'DATE_INSERT',
                $ordersFilter->getDateTo()->format('d.m.Y 23:59:59')
            );
        }

        if ($ordersFilter->getStatus() != null) {
            $builder->withField('STATUS_ID', $ordersFilter->getStatus());
        }

        $builder->withFilter($this->composeFreeTextSearch($ordersFilter));
    }

    private function composeFreeTextSearch(OrdersFilter $ordersFilter): array
    {
        $result = [];

        $freeTextFilter = $ordersFilter->getFreeTextFilter();

        if (empty($freeTextFilter)) {
            return $result;
        }

        $parts = preg_split('/\s+/', $freeTextFilter);

        foreach ($parts as $searchText) {
            $condition = [
                'LOGIC' => 'OR',
            ];

            foreach (self::FREE_TEXT_FILTER_FIELDS as $field) {
                $condition['%' . $field] = $searchText;
            }

            $result[] = $condition;
        }

        return $result;
    }

    private function addOrdering(QueryBuilder $builder, OrdersFilter $ordersFilter)
    {
        if ($ordersFilter->getOrderBy() == 'customer') {
            $builder
                ->orderBy('CUSTOMER_FIRST_NAME', $ordersFilter->getSortOrder())
                ->orderBy('CUSTOMER_LAST_NAME', $ordersFilter->getSortOrder());
            return;
        }

        $mapping = [
            'quantity' => 'ITEMS_QUANTITY',
            'date' => 'DATE_INSERT',
            'price' => 'PRICE',
            'id' => 'ID',
            'status.label' => 'STATUS_NAME'
        ];

        $builder->orderBy($mapping[$ordersFilter->getOrderBy()], $ordersFilter->getSortOrder());
    }
}