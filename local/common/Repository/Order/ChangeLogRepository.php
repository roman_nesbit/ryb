<?php

namespace Rybalka\Marketplace\Repository\Order;

use CIBlockElement;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\Order\ChangeLogEntriesCollection;
use Rybalka\Marketplace\Model\Order\ChangeLogEntry;

class ChangeLogRepository
{
    public function addEntry(ChangeLogEntry $changeLogEntry)
    {
        $entryName = sprintf(
            'Order #: %s (type: %s)',
            $changeLogEntry->getOrderId(),
            $changeLogEntry->getType()
        );

        $data = json_encode($changeLogEntry->getData(), JSON_UNESCAPED_UNICODE);

        $data = [
            'IBLOCK_ID' => InfoBlock::ORDERS_CHANGELOG,
            'NAME' => $entryName,
            'DETAIL_TEXT' => $data,
            'CREATED_BY' => $changeLogEntry->getAuthorId(),
            'PROPERTY_VALUES' => [
                'ORDER_ID' => $changeLogEntry->getOrderId(),
                'AUTHOR_NAME' => $changeLogEntry->getAuthorName(),
                'TYPE' => $changeLogEntry->getType()
            ]
        ];

        (new CIBlockElement())->Add($data);
    }

    public function getAllFor(string $orderId): ChangeLogEntriesCollection
    {
        $result = new ChangeLogEntriesCollection();

        return $result;
    }
}