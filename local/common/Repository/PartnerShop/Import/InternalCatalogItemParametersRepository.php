<?php


namespace Rybalka\Marketplace\Repository\PartnerShop\Import;


use CIBlockProperty;
use CIBlockPropertyResult;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;
use Rybalka\Marketplace\Model\PartnerShop\Import\InternalCatalogItemParameter;

class InternalCatalogItemParametersRepository
{
    public function getByCode(string $code): ?InternalCatalogItemParameter
    {
        $filter = [
            'IBLOCK_ID' => InfoBlock::CATALOG,
            'CODE' => $code
        ];

        /**
         * @var CIBlockPropertyResult $queryResult
         */
        $queryResult = CIBlockProperty::GetList([], $filter);

        if ($record = $queryResult->GetNext()) {
            return InternalCatalogItemParameter::fromRaw($record);
        }

        return null;
    }
}