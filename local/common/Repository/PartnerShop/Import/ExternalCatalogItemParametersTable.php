<?php


namespace Rybalka\Marketplace\Repository\PartnerShop\Import;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Rybalka\Marketplace\Model\PartnerShop\Import\ExternalCatalogItemParameter;
use Rybalka\Marketplace\Util\PartnerShop\Import\NormalizationUtil;

class ExternalCatalogItemParametersTable extends Entity\DataManager
{
    public const TABLE_NAME = 'marketplace_mapping_external_parameters';

    /**
     * @var NormalizationUtil
     */
    private $normalizer;

    public function __construct()
    {
        $this->normalizer = new NormalizationUtil();
    }

    public static function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function getByNameAndPartnerId(string $name, int $partnerId): ?ExternalCatalogItemParameter
    {
        $result = $this->findOneByNameAndPartnerId($name, $partnerId);

        if (empty($result)) {
            return null;
        }

        return ExternalCatalogItemParameter::fromRaw($result);
    }

    public function findOneBy(ExternalCatalogItemParameter $itemParameter): ?ExternalCatalogItemParameter
    {
        $result = $this->findOneByAllParams($itemParameter);

        if (!$result) {
            return null;
        }

        return ExternalCatalogItemParameter::fromRaw($result);
    }

    public function addIfNotExists(ExternalCatalogItemParameter $itemParameter): ExternalCatalogItemParameter
    {
        $record = $this->findOneByAllParams($itemParameter);

        if (!empty($record)) {
            return ExternalCatalogItemParameter::fromRaw($record);
        }

        $data = $itemParameter->nonNullStateAsArray();

        /**
         * @var AddResult $result
         */
        $result = $this->add($data);

        return $this->get($result->getId());
    }

    public function get(int $id): ?ExternalCatalogItemParameter
    {
        $result = $this->getRowById($id);

        if (is_null($result)) {
            return null;
        }

        return ExternalCatalogItemParameter::fromRaw($result);
    }

    public function getGlobalMappingByCode(string $code): ?ExternalCatalogItemParameter
    {
        $query = [
            'filter' => [
                'PARTNER_ID' => null,
                'EXTERNAL_PARAMETER_CODE' => $code
            ]
        ];

        $result = $this->getRow($query);

        if (empty($result)) {
            return null;
        }

        return ExternalCatalogItemParameter::fromRaw($result);
    }

    public function loadMappedParametersFor(int $partnerId): array
    {
        $results = [];

        $query = [
            'filter' => [
                'PARTNER_ID' => $partnerId,
                '!=INTERNAL_PARAMETER_CODE' => null
            ]
        ];

        $queryResult = $this->getList($query);

        while ($row = $queryResult->fetch()) {
            $results[] = ExternalCatalogItemParameter::fromRaw(
                $row
            );
        }

        return $results;
    }

    public function copySettingsFrom(int $id, ExternalCatalogItemParameter $templateParameter)
    {
        $data = [
            'IS_AUTOMATICALLY_MAPPED' => true,
            'INTERNAL_PARAMETER_CODE' => $templateParameter->getInternalCode(),
            'IS_IGNORED' => $templateParameter->isIgnored()
        ];

        $this->update($id, $data);
    }

    private function findOneByNameAndPartnerId(string $name, int $partnerId)
    {
        $code = $this->normalizer->catalogItemParameterName($name);

        $query = [
            'filter' => [
                'PARTNER_ID' => $partnerId,
                'EXTERNAL_PARAMETER_CODE' => $code
            ]
        ];

        return $this->getRow($query);
    }

    private function findOneByAllParams(ExternalCatalogItemParameter $itemParameter)
    {
        $searchFields = [
            'ID',
            'EXTERNAL_PARAMETER_CODE',
            'EXTERNAL_CATEGORY_ID',
            'PARTNER_ID',
            'INTERNAL_PARAMETER_CODE',
        ];

        $query = [
            'filter' => $itemParameter->nonNullStateAsArray($searchFields)
        ];

        return $this->getRow($query);
    }

    public static function getMap()
    {
        return [

            new Entity\IntegerField('ID', [
                'primary' => true,
                'title' => 'ID',
            ]),

            new Entity\IntegerField('PARTNER_ID', [
                'required' => false,
                'title' => 'ID магазина-партнера',
            ]),

            new Entity\StringField('EXTERNAL_PARAMETER_NAME', [
                'required' => true,
                'title' => 'Внешнее имя параметра',
            ]),

            new Entity\StringField('EXTERNAL_PARAMETER_CODE', [
                'required' => true,
                'title' => 'Сгенирированный внешний код',
            ]),

            new Entity\IntegerField('EXTERNAL_CATEGORY_ID', [
                'required' => false
            ]),

            new Entity\StringField('INTERNAL_PARAMETER_CODE', [
                'title' => 'Внутренний код параметра',
            ]),

            new Entity\BooleanField('IS_AUTOMATICALLY_MAPPED'),
            new Entity\BooleanField('IS_IGNORED'),

            new Entity\IntegerField('UPDATED_BY'),

            new Entity\DatetimeField('CREATED_AT'),
            new Entity\DatetimeField('UPDATED_AT'),

            new Reference(
                'CATEGORY',
                ExternalCategoriesTable::class,
                ['=this.EXTERNAL_CATEGORY_ID' => 'ref.EXTERNAL_CATEGORY_ID'],
                ['join_type' => 'LEFT']
            ),

            new Reference(
                'UPDATED_BY_USER',
                '\Bitrix\Main\User',
                ['=this.UPDATED_BY' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),

            new Reference(
                'PARTNER',
                '\Bitrix\Iblock\Element',
                ['=this.PARTNER_ID' => 'ref.ID'],
                [
                    'join_type' => 'LEFT',
                ]
            ),
        ];
    }

    public function getAllMappings(int $partnerId): array
    {
        $res = $this->getList([
            'select' => [
                'ID',
                'EXTERNAL_PARAMETER_CODE',
                'EXTERNAL_PARAMETER_NAME',
                'INTERNAL_PARAMETER_CODE',
                'CATEGORY_NAME' => 'CATEGORY.EXTERNAL_CATEGORY_NAME',
                'IS_IGNORED',
            ],
            'filter' => [
                'PARTNER_ID' => $partnerId,
            ],
            'order' => [
                'EXTERNAL_PARAMETER_NAME' => 'ASC',
                'EXTERNAL_CATEGORY_ID' => 'ASC'
            ]
        ]);

        $mappings = [];
        while ($obj = $res->fetch()) {
            $mappings[$obj['ID']] = [
                'parent_id' => true,
                'name' => $obj['EXTERNAL_PARAMETER_NAME'],
                'ref_id' => $obj['INTERNAL_PARAMETER_CODE'],
                'category_name' => $obj['CATEGORY_NAME'],
                'ignored' => $obj['IS_IGNORED'],
            ];
        }

        return $mappings;
    }

    public function updateRecord(string $parameterId, array $parameterData): bool
    {
        $res = $this->getById($parameterId);

        if($obj = $res->fetch())
        {
            if(!isset($parameterData['ignored']))
            {
                $parameterData['ignored'] = 0;
            }

            if($obj['INTERNAL_PARAMETER_CODE'] != $parameterData['ref_id'] || $obj['IS_IGNORED'] != $parameterData['ignored'])
            {
                global $USER;
                
                $result = $this->update($obj['ID'], [
                    'INTERNAL_PARAMETER_CODE' => $parameterData['ref_id'],
                    'IS_IGNORED' => $parameterData['ignored'],
                    'UPDATED_BY' => $USER->getId(),
                ]);
                
                return $result->isSuccess();
            }
        }

        return false;
    }
}