<?php

namespace Rybalka\Marketplace\Repository\PartnerShop\Import;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\Relations\Reference;

class ExternalCategoriesTable extends Entity\DataManager
{
    public const TABLE_NAME = 'marketplace_mapping_external_categories';

    public static function getTableName()
    {
        return self::TABLE_NAME;
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'title' => 'ID',
            ]),

            new Entity\IntegerField('PARTNER_ID', [
                'required' => true,
                'title' => 'ID магазина-партнера',
            ]),

            new Entity\IntegerField('EXTERNAL_CATEGORY_ID', [
                'required' => true,
                'title' => 'ID категории',
            ]),
            new Entity\StringField('EXTERNAL_CATEGORY_NAME', [
                'required' => true,
                'title' => 'Имя категории',
            ]),
            new Entity\IntegerField('EXTERNAL_CATEGORY_PARENT_ID', [
                'title' => 'ID родительской категории',
            ]),

            new Entity\IntegerField('INTERNAL_CATEGORY_ID', [
                'title' => 'ID категории маркеплейса',
            ]),

            new Entity\IntegerField('UPDATED_BY'),

            new Entity\DatetimeField('CREATED_AT'),
            new Entity\DatetimeField('UPDATED_AT'),

            new Reference(
                'UPDATED_BY_USER',
                '\Bitrix\Main\User',
                ['=this.UPDATED_BY' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),

            new Reference(
                'CATEGORY',
                '\Bitrix\Iblock\Element',
                ['=this.INTERNAL_CATEGORY_ID' => 'ref.ID'],
                [
                    'join_type' => 'LEFT',
                ]
            ),

            new Reference(
                'PARTNER',
                '\Bitrix\Iblock\Element',
                ['=this.PARTNER_ID' => 'ref.ID'],
                [
                    'join_type' => 'LEFT',
                ]
            ),
        ];
    }

    public function updateRecord(int $partnerId, int $externalCategoryId, array $categoryData): bool
    {
        $res = $this->getList([
            'select' => [
                'ID',
                'INTERNAL_CATEGORY_ID',
            ],
            'filter' => [
                'PARTNER_ID' => $partnerId,
                'EXTERNAL_CATEGORY_ID' => $externalCategoryId,
            ]
        ]);
        
        if($obj = $res->fetch()) 
        {
            if($obj['INTERNAL_CATEGORY_ID'] != $categoryData['ref_id'])
            {
                global $USER;
                
                $result = $this->update($obj['ID'], [
                    'INTERNAL_CATEGORY_ID' => $categoryData['ref_id'],
                    'UPDATED_BY' => $USER->getId(),
                ]);
                
                return $result->isSuccess();
            }
        }

        return false;
    }

    public function getAllMappingData(int $partnerId): array
    {
        $fields = [
            'EXTERNAL_CATEGORY_ID',
            'EXTERNAL_CATEGORY_PARENT_ID',
            'EXTERNAL_CATEGORY_NAME',
            'INTERNAL_CATEGORY_ID',
        ];

        $filter = [
            'PARTNER_ID' => $partnerId,
        ];

        $queryResult = $this->getList([
            'select' => $fields,
            'filter' => $filter
        ]);

        $mappings = [];
        while ($row = $queryResult->fetch()) {
            $mappings[$row['EXTERNAL_CATEGORY_ID']] = [
                'parent_id' => $row['EXTERNAL_CATEGORY_PARENT_ID'],
                'name' => $row['EXTERNAL_CATEGORY_NAME'],
                'ref_id' => $row['INTERNAL_CATEGORY_ID'],
            ];
        }

        return $mappings;
    }
}