<?php


namespace Rybalka\Marketplace\Repository;

use Bitrix\Main\Entity\ReferenceField;

class QueryBuilder
{
    private $query = [];

    private function __construct()
    {
    }

    public static function create()
    {
        return new QueryBuilder();
    }

    public function select(...$fields): QueryBuilder
    {
        foreach ($fields as $field) {
            $this->query['select'][] = $field;
            continue;
        }

        return $this;
    }

    public function selectAlias(string $alias, string $fieldName): QueryBuilder
    {
        $this->query['select'][$alias] = $fieldName;
        return $this;
    }

    /**
     * @param int $offset
     * @return QueryBuilder
     */
    public function offset(int $offset): QueryBuilder
    {
        $this->query['offset'] = $offset;
        return $this;
    }

    /**
     * @param int $limit
     * @return QueryBuilder
     */
    public function limit(int $limit): QueryBuilder
    {
        $this->query['limit'] = $limit;
        return $this;
    }

    /**
     * @return QueryBuilder
     */
    public function countTotal(): QueryBuilder
    {
        $this->query['count_total'] = true;
        return $this;
    }

    public function orderBy(string $fieldName, string $order): QueryBuilder
    {
        strtoupper($order) == 'ASC'
            ? $this->orderAscBy($fieldName)
            : $this->orderDescBy($fieldName);

        return $this;
    }

    public function orderAscBy(string $fieldName): QueryBuilder
    {
        $this->query['order'][$fieldName] = 'ASC';
        return $this;
    }

    public function orderDescBy(string $fieldName): QueryBuilder
    {
        $this->query['order'][$fieldName] = 'DESC';
        return $this;
    }

    public function join(string $alias, string $classReference, array $joinCondition): QueryBuilder
    {
        $this->query['runtime'][] = new ReferenceField(
            $alias, $classReference, $joinCondition, ['join_type' => 'inner']
        );

        return $this;
    }

    public function leftJoin(string $alias, string $classReference, array $joinCondition): QueryBuilder
    {
        $this->query['runtime'][] = new ReferenceField(
            $alias, $classReference, $joinCondition, ['join_type' => 'left']
        );

        return $this;
    }

    public function withField(string $fieldName, string $fieldValue): QueryBuilder
    {
        $this->query['filter']['=' . $fieldName] = $fieldValue;
        return $this;
    }

    public function withFieldGreaterThan(string $fieldName, string $fieldValue): QueryBuilder
    {
        $this->query['filter']['>=' . $fieldName] = $fieldValue;
        return $this;
    }

    public function withFieldLowerThan(string $fieldName, string $fieldValue): QueryBuilder
    {
        $this->query['filter']['<=' . $fieldName] = $fieldValue;
        return $this;
    }

    public function withFieldLike(string $fieldName, string $fieldValue): QueryBuilder
    {
        $this->query['filter']['%' . $fieldName] = $fieldValue;
        return $this;
    }

    public function withFilter(array $filter): QueryBuilder
    {
        if (!empty($filter)) {
            $this->query['filter'] = array_merge($this->query['filter'] ?? [], $filter);
        }

        return $this;
    }

    public function build(): array
    {
        $result = $this->query;
        $this->query = [];

        return $result;
    }
}