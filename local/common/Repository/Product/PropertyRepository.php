<?php

namespace Rybalka\Marketplace\Repository\Product;


use CIBlockElement;
use Rybalka\Marketplace\Model\Bitrix\InfoBlock;

class PropertyRepository
{
    const PROPERTY_SKU = 'SKU';

    const FIELD_PROPERTY_NAME  = 'CODE';
    const FIELD_PROPERTY_VALUE = 'VALUE';

    public function getSkuByProductId(int $productId)
    {
        return $this->loadProperty($productId, self::PROPERTY_SKU);
    }

    private function loadProperty(int $productId, string $propertyCode)
    {
        $result = CIBlockElement::GetProperty(InfoBlock::CATALOG, $productId,
            'sort',
            'ask',
            [self::FIELD_PROPERTY_NAME => $propertyCode]
        );

        if ($property = $result->Fetch()) {
            return $property[self::FIELD_PROPERTY_VALUE];
        }

        return null;
    }
}