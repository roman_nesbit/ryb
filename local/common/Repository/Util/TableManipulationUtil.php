<?php

namespace Rybalka\Marketplace\Repository\Util;

use Bitrix\Main\Application;
use Bitrix\Main\DB\Connection;
use Bitrix\Main\Entity;
use Rybalka\Marketplace\Repository\Util\Exception\TableManipulationException;

class TableManipulationUtil
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct()
    {
        $this->connection = Application::getInstance()->getConnection();
    }

    public function createIfDoesNotExist(Entity\DataManager $tableDefinition)
    {
        $tableName = $tableDefinition->getTableName();

        if ($this->connection->isTableExists($tableName)) {
            return;
        }

        $tableDefinition->getEntity()->createDbTable();
    }

    public function recreate(Entity\DataManager $tableDefinition)
    {
        $tableName = $tableDefinition->getTableName();
        $this->drop($tableName);
        $this->createIfDoesNotExist($tableDefinition);
    }

    public function move(string $oldTableName, string $newTableName)
    {
        if ($this->connection->isTableExists($newTableName)) {
            $this->drop($newTableName);
        }

        $this->rename($oldTableName, $newTableName);
    }

    public function rename(string $oldTableName, string $newTableName)
    {
        if (!$this->connection->isTableExists($oldTableName)) {
            throw new TableManipulationException('Can not rename the table as it does not exist: ', $oldTableName);
        }

        if ($this->connection->isTableExists($newTableName)) {
            throw new TableManipulationException(sprintf(
                'Can not rename table as destination table (%s) already exists',
                $newTableName
            ));
        }

        $this->connection->renameTable($oldTableName, $newTableName);
    }

    public function drop(string $tableName)
    {
        if (!$this->connection->isTableExists($tableName)) {
            return;
        }

        $this->connection->dropTable($tableName);
    }
}