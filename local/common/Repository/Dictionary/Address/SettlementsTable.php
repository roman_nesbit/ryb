<?php

namespace Rybalka\Marketplace\Repository\Dictionary\Address;

use Bitrix\Main\DB\SqlHelper;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;

class SettlementsTable extends Entity\DataManager
{
    const ENCODING_UTF8 = 'utf-8';

    public static function getTableName()
    {
        return 'mp_dictionary_settlements';
    }

    public static function getMap()
    {
        return [
            new StringField('id', [
                'primary' => true,
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('delivery_settlement_id', [
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('name_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('name_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('type_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('type_name_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('type_name_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('region_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('region_name_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('region_name_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('area_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('area_name_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('area_name_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('latitude',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 20),
                    ];
                },
            ]),
            new StringField('longitude',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 20),
                    ];
                },
            ]),
            new StringField('postcode_start',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 5),
                    ];
                },
            ]),
            new StringField('postcode_end',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 5),
                    ];
                },
            ]),
            new IntegerField('sort_order'),
            new IntegerField('settlement_relative_size'),
            new IntegerField('warehouse'),
        ];
    }

    public function get(string $settlementId): ?array
    {
        $queryResult = $this->getByPrimary($settlementId);
        $record = $queryResult->fetch();

        return $record
            ? $record
            : null;
    }

    public function loadDefault(int $limit = 10): ?array
    {
        $queryResult = $this->getList([
            'filter' => [
                '!=sort_order' => [null, 0]
            ],
            'order' => [
                'sort_order' => 'ASC',
                'name_ua' => 'ASC'
            ],
            'limit' => $limit,
        ]);

        $results = [];

        while ($record = $queryResult->fetch()) {
            $results[] = $record;
        }

        return $results;
    }

    public function loadStartingWith(string $query, int $limit = 10): array
    {
        $results = [];

        $filter = [
            'LOGIC' => 'OR',
            '%name_ru' => $query,
            '%name_ua' => $query,
        ];

        $queryResult = $this->getList([
            'filter' => $filter,
            'runtime' => [
                new ExpressionField(
                    'first_order',
                    $this->getStartsWithExpression($query)
                ),
            ],
            'order' => [
                'first_order' => 'asc',
                'settlement_relative_size' => 'desc',
                'name_ua' => 'asc'
            ],
            'limit' => $limit,
        ]);

        while ($record = $queryResult->fetch()) {
            $results[] = $record;
        }

        return $results;
    }

    private function getSqlHelper(): SqlHelper
    {
        return $this->getEntity()->getConnection()->getSqlHelper();
    }

    private function getStartsWithExpression(string $query): string
    {
        $sqlHelper = $this->getSqlHelper();

        $length = mb_strlen($query, self::ENCODING_UTF8);
        $query = $sqlHelper->forSql(mb_strtoupper($query, self::ENCODING_UTF8));

        return sprintf('(CASE '
            . 'WHEN LEFT(UPPER(name_ru), %d)=\'%s\' OR LEFT(UPPER(name_ua), %d)=\'%s\' THEN 1 '
            . 'ELSE 2 '
            . 'END)',
            $length, $query, $length, $query
        );
    }
}