<?php


namespace Rybalka\Marketplace\Repository\Dictionary\Address;

use Bitrix\Main\Entity\DataManager;

class SettlementsTemporaryTable extends DataManager
{
    public static function getTableName()
    {
        return 'mp_dictionary_settlements_temp';
    }

    public static function getMap()
    {
        return SettlementsTable::getMap();
    }
}