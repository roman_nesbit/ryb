<?php

namespace Rybalka\Marketplace\Repository\Dictionary\Address;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;

class StreetsCacheTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'mp_dictionary_streets_cache';
    }

    public static function getMap()
    {
        return [
            new StringField('id', [
                'primary' => true,
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('name',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
            ]),
            new StringField('type',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('delivery_settlement_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
        ];
    }

    public function saveIfAbsent(array $streets)
    {
        $ids = array_map(function($data) {
            return $data['id'];
        }, $streets);

        $queryResult = $this->getList([
            'filter' => [
                'id' => $ids,
            ],
            'select' => [
                'id'
            ]
        ]);

        $existingStreetsIds = [];

        while ($record = $queryResult->fetch()) {
            $existingStreetsIds[] = $record['id'];
        }

        $nonExistingStreets = array_filter($streets, function ($current) use ($existingStreetsIds) {
            return !in_array($current['id'], $existingStreetsIds);
        });

        if (!empty($nonExistingStreets)) {
            $this->addMulti($nonExistingStreets);
        }
    }
}