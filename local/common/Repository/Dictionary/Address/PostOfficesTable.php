<?php

namespace Rybalka\Marketplace\Repository\Dictionary\Address;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;

class PostOfficesTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'mp_dictionary_post_offices';
    }

    public static function getMap()
    {
        return [
            new StringField('id', [
                'primary' => true,
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new IntegerField('number'),
            new StringField('name_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
            ]),
            new StringField('name_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
            ]),
            new StringField('address_ru',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
            ]),
            new StringField('address_ua',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
            ]),
            new StringField('phone_number',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 15),
                    ];
                },
            ]),
            new StringField('type_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('settlement_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('delivery_settlement_id',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 36),
                    ];
                },
            ]),
            new StringField('latitude',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
            new StringField('longitude',[
                'validation' => function () {
                    return [
                        new Validator\Length(null, 50),
                    ];
                },
            ]),
        ];
    }

    public function getListBy(string $deliverySettlementId): array
    {
        $filter = [
            'delivery_settlement_id' => $deliverySettlementId,
        ];

        $queryResult = static::getList([
            'filter' => $filter,
            'order' => ['number' => 'asc'],
        ]);

        $results = [];
        while ($record = $queryResult->fetch()) {
            $results[] = $record;
        }

        return $results;
    }

    public function getClosestTo(string $longitude, string $latitude): array
    {
        $result = $this->getList([
            'order' => [
                'settlement_office_distance' => 'asc',
            ],
            'limit' => 10,
            'runtime' => [
                new Entity\ExpressionField(
                    'settlement_office_distance',
                    'ST_Distance(Point(' . $longitude . ',' . $latitude . '),Point(longitude,latitude))'),
            ],
        ]);

        $results = [];

        while($record = $result->fetch()) {
            $results[] = $record;
        }

        return $results;
    }
}