<?php


namespace Rybalka\Marketplace\Repository\Dictionary\Address;


use Bitrix\Main\Entity\DataManager;

class PostOfficesTemporaryTable extends DataManager
{
    public static function getTableName()
    {
        return 'mp_dictionary_post_offices_temp';
    }

    public static function getMap()
    {
        return PostOfficesTable::getMap();
    }
}