<?php


namespace Rybalka\Marketplace\Repository;


use CFile;
use CIBlockElement;

class ProductRepository
{
    public function getMainPictureById(int $productId)
    {
        $result = CIBlockElement::GetByID($productId);

        if ($product = $result->GetNext()) {
            return CFile::GetPath($product['DETAIL_PICTURE']);
        }

        return null;
    }
}