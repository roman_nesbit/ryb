<?php
namespace Rybalka\Marketplace\Exception;

use Exception;

class RegistrationException extends Exception
{
	private $_data;

	public function __construct($message, $data) 
	{
		$this->_data = $data;
		parent::__construct($message);
	}

	public function getData()
	{
		return $this->_data;
	}
}