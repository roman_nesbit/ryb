<?php


namespace Rybalka\Marketplace\Exception;


use Exception;

class ValidationException extends Exception
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var string
     */
    private $userErrorMessage;

    public function __construct(string $fieldName, string $userErrorMessage)
    {
        $this->fieldName = $fieldName;
        $this->userErrorMessage = $userErrorMessage;
        parent::__construct(sprintf('Failed to validate the "%s" field', $fieldName));
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return string
     */
    public function getUserErrorMessage(): string
    {
        return $this->userErrorMessage;
    }
}