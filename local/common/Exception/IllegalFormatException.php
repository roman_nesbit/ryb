<?php

namespace Rybalka\Marketplace\Exception;

use Exception;

class IllegalFormatException extends Exception
{
}