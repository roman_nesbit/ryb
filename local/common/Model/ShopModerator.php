<?php

namespace Rybalka\Marketplace\Model;


use Rybalka\Marketplace\Exception\AccessViolationException;
use Rybalka\Marketplace\Exception\InvalidStateException;
use Rybalka\Marketplace\Util\ArrayUtil;

class ShopModerator {

    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    private $rawData = [];

    public static function fromRestSession(array $userData) {
        $arrayUtil = new ArrayUtil();
        $shopModerator = new ShopModerator();

        $shopModerator->userId = $arrayUtil->getIntOrException(
            $userData, 'ID',
            new InvalidStateException('No user id provided')
        );

        $shopModerator->shopId = $arrayUtil->getIntOrException(
            $userData, 'UF_SHOP', new AccessViolationException('Moderator is not assigned to any shop')
        );

        $shopModerator->username = $arrayUtil->getOrNull($userData, 'LOGIN');
        $shopModerator->lastName = $arrayUtil->getOrNull($userData, 'LAST_NAME');
        $shopModerator->firstName = $arrayUtil->getOrNull($userData, 'NAME');

        $shopModerator->rawData = $userData;

        return $shopModerator;
    }

    private function __construct() {
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }
}