<?php

namespace Rybalka\Marketplace\Model\PartnerShop\Import;

class ImportConfiguration
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $partnerId;

    /**
     * @var string
     */
    private $url;

    public static function fromRawData(array $rawData): ImportConfiguration
    {
        $result = new ImportConfiguration();

        $result->id = $rawData['ID'] ?? null;
        $result->partnerId = $rawData['PARTNER_ID'] ?? null;
        $result->url = $rawData['URL'] ?? null;

        return  $result;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getPartnerId(): int
    {
        return $this->partnerId;
    }
}