<?php


namespace Rybalka\Marketplace\Model\PartnerShop\Import;


class InternalCatalogItemParameter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string code
     */
    private $code;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $isMultiValue;

    public static function fromRaw(array $data): InternalCatalogItemParameter
    {
        $instance = new InternalCatalogItemParameter();

        $instance->id = $data['ID'] ?? null;
        $instance->name = $data['NAME'] ?? null;
        $instance->code = $data['CODE'] ?? null;
        $instance->type = $data['PROPERTY_TYPE'] ?? 'S';
        $instance->isMultiValue = isset($data['MULTIPLE']) && $data['MULTIPLE'] == 'Y';

        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isMultiValue(): bool
    {
        return $this->isMultiValue;
    }
}