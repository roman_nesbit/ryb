<?php


namespace Rybalka\Marketplace\Model\PartnerShop\Import;


use Rybalka\Marketplace\Model\Common\NonEmptyStateTrait;
use Rybalka\Marketplace\Model\Common\StateAsArray;
use Rybalka\Marketplace\Util\PartnerShop\Import\NormalizationUtil;

class ExternalCatalogItemParameter implements StateAsArray
{
    use NonEmptyStateTrait;

    /**
     * @var ?int
     */
    private $id;

    /**
     * @var ?int
     */
    private $partnerId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $internalCode;

    /**
     * @var ?bool
     */
    private $isAutomaticallyMapped;

    /**
     * @var ?bool
     */
    private $isIgnored;

    /**
     * @var ?int
     */
    private $externalCategoryId;

    /**
     * @var NormalizationUtil
     */
    private $normalizer;

    private function __construct()
    {
        $this->normalizer = new NormalizationUtil();
    }

    public static function fromRaw(array $data)
    {
        $instance = new ExternalCatalogItemParameter();

        $instance->id = $data['ID'] ?? null;
        $instance->partnerId = intval ($data['PARTNER_ID'] ?? null);
        $instance->name = $data['EXTERNAL_PARAMETER_NAME'] ?? null;
        $instance->code = $data['EXTERNAL_PARAMETER_CODE'] ?? null;

        if (is_null($instance->code) && !is_null($instance->name)) {
            $instance->code = $instance->normalizer->catalogItemParameterName($instance->name);
        }

        $instance->externalCategoryId = (int)$data['EXTERNAL_CATEGORY_ID'] ?? null;

        $instance->internalCode = $data['INTERNAL_PARAMETER_CODE'] ?? null;

        $instance->isIgnored = $data['IS_IGNORED'] ?? null;
        $instance->isAutomaticallyMapped = $data['IS_AUTOMATICALLY_MAPPED'] ?? null;

        return $instance;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function isGlobal(): bool
    {
        return empty($this->partnerId);
    }

    public function isIgnored(): bool
    {
        return $this->isIgnored ?? false;
    }

    /**
     * @return int
     */
    public function getPartnerId(): ?int
    {
        return $this->partnerId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getInternalCode(): ?string
    {
        return $this->internalCode;
    }

    /**
     * @return bool
     */
    public function isAutomaticallyMapped(): bool
    {
        return $this->isAutomaticallyMapped ?? false;
    }

    /**
     * @return int
     */
    public function getExternalCategoryId(): int
    {
        return $this->externalCategoryId ?? 0;
    }

    public function asArray(array $fieldsFilter = []): array
    {
        $all = [
            'ID' => $this->id,
            'PARTNER_ID' => $this->partnerId,
            'EXTERNAL_PARAMETER_NAME' => $this->name,
            'EXTERNAL_PARAMETER_CODE' => $this->code,
            'EXTERNAL_CATEGORY_ID' => $this->externalCategoryId,
            'INTERNAL_PARAMETER_CODE' => $this->internalCode,
            'IS_IGNORED' => $this->isIgnored,
            'IS_AUTOMATICALLY_MAPPED' => $this->isAutomaticallyMapped
        ];

        if (empty($fieldsFilter)) {
            return $all;
        }

        $result = [];

        foreach ($all as $key => $value) {
            if (in_array($key, $fieldsFilter)) {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}