<?php


namespace Rybalka\Marketplace\Model;


class PartnerShop
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    public static function fromRawData($rawData): PartnerShop
    {
        $result = new PartnerShop();

        $result->id   = $rawData['ID'] ?? null;
        $result->name = $rawData['NAME'] ?? null;

        return $result;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}