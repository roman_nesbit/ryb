<?php


namespace Rybalka\Marketplace\Model;

use Rybalka\Marketplace\Util\DataStructure\PartialCollection;

class OrderCollection extends PartialCollection
{
    public function add(Order $item)
    {
        parent::add($item);
    }
}