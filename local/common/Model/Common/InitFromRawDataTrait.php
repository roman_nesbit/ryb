<?php


namespace Rybalka\Marketplace\Model\Common;


trait InitFromRawDataTrait
{
    public static function fromRaw(array $data)
    {
        $className = self::class;
        $instance = new $className;

        $instance->initFromRawData($data);

        if (method_exists($instance, 'postInit')) {
            $instance->postInit($data);
        }

        return $instance;
    }

    private function initFromRawData(array $data)
    {
        foreach ($data as $key => $value) {
            $normalizedKey = $this->normalizeKey($key);

            if (property_exists($this, $normalizedKey)) {
                $this->$normalizedKey = $value;
            }
        }

        $this->initNestedModels($data);
    }

    private function initNestedModels(array $data)
    {
        if (!method_exists($this, 'getNestedModels')) {
            return;
        }

        $nestedModels = $this->getNestedModels();

        foreach ($nestedModels as $propertyName => $className) {
            $properties = $this->extractPropertiesWithPrefix($propertyName, $data);

            $this->$propertyName = $className::fromRaw($properties);
        }
    }

    private function extractPropertiesWithPrefix(string $prefix, array $data): array
    {
        $result = [];

        foreach ($data as $key => $value) {

            $normalizedKey = $this->normalizeKey($key);

            if ($prefix == $normalizedKey && is_array($data[$key])) {
                return $data[$key];
            }

            if (strpos($normalizedKey, $prefix) === 0) {
                $newKey = substr(
                    $key,
                    strlen($prefix)
                );

                $result[$newKey] = $value;
            }
        }

        return $result;
    }

    private function normalizeKey(string $key): string
    {
        $key = str_replace('~', 'HTML_', $key);
        $parts = explode('_', $key);

        $parts = array_map(function($part) {
            return ucfirst(
                strtolower(
                    preg_replace('/\W/', '', $part)
                )
            );
        }, $parts);

        return lcfirst(implode('', $parts));
    }
}