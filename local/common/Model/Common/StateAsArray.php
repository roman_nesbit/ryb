<?php


namespace Rybalka\Marketplace\Model\Common;


interface StateAsArray
{
    public function asArray(): array;
}