<?php


namespace Rybalka\Marketplace\Model\Common;


trait NonEmptyStateTrait
{
    public function nonNullStateAsArray(array $fieldsFilter = []): array
    {
        $state = $this->asArray($fieldsFilter);
        $filteredState = [];

        foreach ($state as $key => $value) {
            if (!is_null($value)) {
                $filteredState[$key] = $value;
            }
        }

        return $filteredState;
    }
}