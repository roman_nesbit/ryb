<?php


namespace Rybalka\Marketplace\Model;

use Bitrix\Sale\Order as BitrixModel;
use DateTime;
use Money\Money;
use Rybalka\Marketplace\Model\Order\Customer;
use Rybalka\Marketplace\Model\Order\ItemsCollection;
use Rybalka\Marketplace\Model\Order\Property;
use Rybalka\Marketplace\Model\Order\Status;

class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $publicId;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var int
     */
    private $itemsQuantity;

    /**
     * @var Money
     */
    private $totalPrice;

    /**
     * @var string
     */
    private $deliveryMethodName = '';

    /**
     * @var ItemsCollection
     */
    private $items;

    /**
     * @var DateTime
     */
    private $creationDateTime;

    /**
     * @var int $shopId
     */
    private $shopId;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var string
     */
    private $noteFromModerator = '';

    public function __construct()
    {
        $this->status = new Status();
        $this->totalPrice = Money::UAH(0);
        $this->items = new ItemsCollection();
        $this->customer = new Customer();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublicId(): string
    {
        return $this->publicId;
    }

    /**
     * @param string $publicId
     * @return Order
     */
    public function setPublicId(string $publicId): Order
    {
        $this->publicId = $publicId;
        return $this;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @param Status $status
     * @return Order
     */
    public function setStatus(Status $status): Order
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Money
     */
    public function getTotalPrice(): Money
    {
        return $this->totalPrice;
    }

    /**
     * @param Money $totalPrice
     * @return Order
     */
    public function setTotalPrice(Money $totalPrice): Order
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getItemsQuantity(): int
    {
        return $this->itemsQuantity;
    }

    /**
     * @param int $itemsQuantity
     * @return Order
     */
    public function setItemsQuantity(int $itemsQuantity): Order
    {
        $this->itemsQuantity = $itemsQuantity;
        return $this;
    }

    /**
     * @param DateTime $creationDateTime
     * @return Order
     */
    public function setCreationDateTime(DateTime $creationDateTime): Order
    {
        $this->creationDateTime = $creationDateTime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDateTime(): DateTime
    {
        return $this->creationDateTime;
    }

    /**
     * @return ItemsCollection
     */
    public function getItems(): ItemsCollection
    {
        return $this->items;
    }

    /**
     * @param ItemsCollection $items
     * @return Order
     */
    public function setItems(ItemsCollection $items): Order
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryMethodName(): string
    {
        return $this->deliveryMethodName;
    }

    /**
     * @param string $deliveryMethodName
     * @return Order
     */
    public function setDeliveryMethodName(string $deliveryMethodName): Order
    {
        $this->deliveryMethodName = $deliveryMethodName;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @param int $shopId
     * @return Order
     */
    public function setShopId(int $shopId): Order
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return Order
     */
    public function setCustomer(Customer $customer): Order
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoteFromModerator(): string
    {
        return $this->noteFromModerator;
    }

    /**
     * @param string $noteFromModerator
     * @return Order
     */
    public function setNoteFromModerator(string $noteFromModerator): Order
    {
        $this->noteFromModerator = $noteFromModerator;
        return $this;
    }
}