<?php

namespace Rybalka\Marketplace\Model\Checkout;

use Money\Currency;
use Money\Money;
use Rybalka\Marketplace\Model\Checkout\DeliveryService\Restrictions;
use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;
use Rybalka\Marketplace\Util\MoneyUtil;

class DeliveryService
{
    use InitFromRawDataTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $xmlId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var Money
     */
    private $roughPrice;

    /**
     * @var array
     */
    private $requires = [];

    /**
     * @var array
     */
    private $accepts = [];

    /**
     * @var Restrictions
     */
    private $restrictions;

    public function getNestedModels()
    {
        return [
            'restrictions' => Restrictions::class
        ];
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }

    public function getRoughPrice(): ?Money
    {
        return $this->roughPrice;
    }

    public function getRequires(): array
    {
        return $this->requires;
    }

    public function getAccepts(): array
    {
        return $this->accepts;
    }

    /**
     * @return Restrictions
     */
    public function getRestrictions(): Restrictions
    {
        return $this->restrictions;
    }

    private function postInit(array $data) {
        if (isset($data['ROUGH_PRICE'])) {
            $this->roughPrice = (new MoneyUtil())->floatToMoney(
                $data['ROUGH_PRICE']['currency'] ?? MoneyUtil::DEFAULT_CURRENCY,
                (float)($data['ROUGH_PRICE']['value'] ?? 0)
            );
        }

        $priceData = $data['CONFIG']['MAIN'] ?? [];

        $this->price = (new MoneyUtil())->floatToMoney(
            $priceData['CURRENCY'] ?? MoneyUtil::DEFAULT_CURRENCY,
            (float)($data['PRICE'] ?? 0)
        );
    }
}