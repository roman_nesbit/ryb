<?php


namespace Rybalka\Marketplace\Model\Checkout\DeliveryService;


use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;

class Restrictions
{
    use InitFromRawDataTrait;

    /**
     * @var OrderAmountRestriction
     */
    private $orderAmount;

    public function getNestedModels()
    {
        return [
            'orderAmount' => OrderAmountRestriction::class
        ];
    }

    public function orderAmount(): OrderAmountRestriction
    {
        return $this->orderAmount;
    }
}