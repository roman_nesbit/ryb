<?php


namespace Rybalka\Marketplace\Model\Checkout\DeliveryService;


use Money\Money;
use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;
use Rybalka\Marketplace\Util\MoneyUtil;

class OrderAmountRestriction
{
    use InitFromRawDataTrait;

    /**
     * @var Money
     */
    private $min;

    /**
     * @var Money
     */
    private $max;

    public function hasMin():bool
    {
        return !is_null($this->min);
    }

    public function getMin(): ?Money
    {
        return $this->min;
    }

    public function getMax(): ?Money
    {
        return $this->max;
    }

    private function postInit(array $data)
    {
        if (isset($data['MIN'])) {
            $this->min = (new MoneyUtil())->floatToMoney(
                MoneyUtil::DEFAULT_CURRENCY,
                (float)($data['MIN'] ?? 0)
            );
        }

        if (isset($data['MAX'])) {
            $this->max = (new MoneyUtil())->floatToMoney(
                MoneyUtil::DEFAULT_CURRENCY,
                (float)($data['MAX'] ?? 0)
            );
        }
    }
}