<?php

namespace Rybalka\Marketplace\Model\Bitrix\InfoBlock;

class CommonField
{
    const INFO_BLOCK_ID = 'IBLOCK_ID';
    const ACTIVE = 'ACTIVE';

    const SHOP_ID = 'PROPERTY_PARTNER';
    const SKU = 'PROPERTY_SKU';
}