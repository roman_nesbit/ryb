<?php


namespace Rybalka\Marketplace\Model\Bitrix;


class MarketplaceConstants
{
    public const SITE_ID = 's1';
    public const DEFAULT_CURRENCY = 'UAH';

    public const USER_GROUP_ALL_USERS = 2;
}