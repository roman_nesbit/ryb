<?php


namespace Rybalka\Marketplace\Model\Bitrix;


class InfoBlock
{
    const CATALOG = 4;
    const SHOPS = 2;

    const ORDERS_CHANGELOG = 10;
}