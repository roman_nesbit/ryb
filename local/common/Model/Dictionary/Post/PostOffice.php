<?php


namespace Rybalka\Marketplace\Model\Dictionary\Post;


use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;

class PostOffice
{
    use InitFromRawDataTrait;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $nameRu;

    /**
     * @var string
     */
    private $nameUa;

    /**
     * @var string
     */
    private $addressUa;

    /**
     * @var string
     */
    private $addressRu;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var string
     */
    private $settlementId;

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNameRu(): ?string
    {
        return $this->nameRu;
    }

    /**
     * @return string
     */
    public function getNameUa(): ?string
    {
        return $this->nameUa;
    }

    /**
     * @return string
     */
    public function getAddressUa(): ?string
    {
        return $this->addressUa;
    }

    /**
     * @return string
     */
    public function getAddressRu(): ?string
    {
        return $this->addressRu;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getSettlementId(): string
    {
        return $this->settlementId;
    }

    public function __toString(): string
    {
        $parts = explode(':', $this->getNameUa());

        $officeName = count($parts) == 2 ? $parts[0] : $this->getNameUa();
        $address = $this->getAddressUa();

        return $officeName . ': ' . $address;
    }
}