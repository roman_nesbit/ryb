<?php


namespace Rybalka\Marketplace\Model\Dictionary\Address;


use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;

class SettlementType
{
    use InitFromRawDataTrait;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $nameUa;

    /**
     * @var string
     */
    private $nameRu;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNameUa(): ?string
    {
        return $this->nameUa;
    }

    /**
     * @return string
     */
    public function getNameRu(): ?string
    {
        return $this->nameRu;
    }

    public function __toString()
    {
        return $this->nameUa ?? '';
    }
}