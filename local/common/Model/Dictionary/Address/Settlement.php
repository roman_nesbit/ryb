<?php


namespace Rybalka\Marketplace\Model\Dictionary\Address;


use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;

class Settlement
{
    use InitFromRawDataTrait;

    const DEFAULT_ENCODING = 'utf-8';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $deliverySettlementId;

    /**
     * @var string
     */
    private $nameUa;

    /**
     * @var string
     */
    private $nameRu;

    /**
     * @var SettlementType
     */
    private $type;

    /**
     * @var Area
     */
    private $area;

    /**
     * @var Area
     */
    private $region;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var string
     */
    private $postcodeStart;

    /**
     * @var int
     */
    private $warehouse;

    public function getNestedModels()
    {
        return [
            'type' => SettlementType::class,
            'area' => Area::class,
            'region' => Area::class,
        ];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDeliverySettlementId(): ?string
    {
        return $this->deliverySettlementId;
    }

    public function getNameUa(): ?string
    {
        return $this->nameUa;
    }

    public function getNameRu(): ?string
    {
        return $this->nameRu;
    }

    public function getType(): SettlementType
    {
        return $this->type;
    }

    public function getArea(): Area
    {
        return $this->area;
    }

    public function getRegion(): Area
    {
        return $this->region;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getPostcode(): ?string
    {
        return $this->postcodeStart;
    }

    public function getNumberOfWarehouses(): int
    {
        return $this->warehouse;
    }

    public function __toString(): string
    {
        $type = (string)$this->getType();

        $nameParts = [
            $this->nameUa ?? '',
            $this->getRegion(),
            $this->getArea()
        ];

        $nameParts = array_filter($nameParts, function ($value) {
            return !empty(trim($value));
        });

        $type = !empty($type)
            ? mb_substr($type, 0, 1, self::DEFAULT_ENCODING) . '. '
            : '';

        return $type . implode(', ', $nameParts);
    }
}