<?php


namespace Rybalka\Marketplace\Model\Dictionary\Address;


use Rybalka\Marketplace\Model\Common\InitFromRawDataTrait;

class Street
{
    use InitFromRawDataTrait;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->getType() . ' ' . $this->getName();
    }
}