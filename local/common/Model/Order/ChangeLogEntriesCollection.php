<?php

namespace Rybalka\Marketplace\Model\Order;

use Rybalka\Marketplace\Util\DataStructure\GenericCollection;

class ChangeLogEntriesCollection extends GenericCollection
{
    public function add(ChangeLogEntry $changeLogEntry)
    {
        parent::add($changeLogEntry);
    }
}