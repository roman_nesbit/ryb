<?php


namespace Rybalka\Marketplace\Model\Order;


use DateTime;
use InvalidArgumentException;

class ChangeLogEntry
{
    const TYPE_STATUS_CHANGE = 'status-change';
    const TYPE_NOTE = 'note';
    const ALL_TYPES = [self::TYPE_STATUS_CHANGE, self::TYPE_NOTE];

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $authorId;

    /**
     * @var string
     */
    private $authorName;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var DateTime
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ChangeLogEntry
     */
    public function setType(string $type): ChangeLogEntry
    {
        if (!in_array($type, self::ALL_TYPES)) {
            throw new InvalidArgumentException('Unknown changelog type: ' . $type);
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return ChangeLogEntry
     */
    public function setOrderId(int $orderId): ChangeLogEntry
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     * @return ChangeLogEntry
     */
    public function setAuthorId(int $authorId): ChangeLogEntry
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorName(): string
    {
        return $this->authorName;
    }

    /**
     * @param string $authorName
     * @return ChangeLogEntry
     */
    public function setAuthorName(string $authorName): ChangeLogEntry
    {
        $this->authorName = $authorName;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return ChangeLogEntry
     */
    public function setCreatedAt(DateTime $createdAt): ChangeLogEntry
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return ChangeLogEntry
     */
    public function setData(array $data): ChangeLogEntry
    {
        $this->data = $data;
        return $this;
    }
}