<?php

namespace Rybalka\Marketplace\Model\Order;

class Status
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    public static function fromRawData(array $rawData): Status {
        $rawData = array_change_key_case($rawData, CASE_LOWER);

        $status = new Status();

        $status->id   = $rawData['id'] ?? '';
        $status->name = $rawData['name'] ?? '';
        $status->description = $rawData['description'] ?? '';

        return $status;
    }

    public function getStageId(): string
    {
        return $this->getId()[0];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $id
     * @return Status
     */
    public function setId(string $id): Status
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Status
     */
    public function setName(string $name): Status
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $description
     * @return Status
     */
    public function setDescription(string $description): Status
    {
        $this->description = $description;
        return $this;
    }
}