<?php


namespace Rybalka\Marketplace\Model\Order;


use Rybalka\Marketplace\Util\DataStructure\GenericCollection;

class StatusCollection extends GenericCollection
{
    public function add(Status $item)
    {
        parent::add($item);
    }
}