<?php


namespace Rybalka\Marketplace\Model\Order;


use Rybalka\Marketplace\Util\DataStructure\GenericCollection;

class ItemsCollection extends GenericCollection
{
    public function add(Item $item)
    {
        parent::add($item);
    }
}