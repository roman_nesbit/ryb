<?php

namespace Rybalka\Marketplace\Model\Order;

use Money\Money;

class Item
{
    /**
     * @var string
     */
    private $productSku;

    /**
     * @var string
     */
    private $productMainImage;

    /**
     * @var int
     */
    private $productId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var Money
     */
    private $productPrice;

    /**
     * @var Money
     */
    private $totalPrice;

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return Item
     */
    public function setProductId(int $productId): Item
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Item
     */
    public function setQuantity(int $quantity): Item
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Money
     */
    public function getProductPrice(): Money
    {
        return $this->productPrice;
    }

    /**
     * @param Money $productPrice
     * @return Item
     */
    public function setProductPrice(Money $productPrice): Item
    {
        $this->productPrice = $productPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductSku(): string
    {
        return $this->productSku;
    }

    /**
     * @param string $productSku
     * @return Item
     */
    public function setProductSku(string $productSku): Item
    {
        $this->productSku = $productSku;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductMainImage(): ?string
    {
        return $this->productMainImage;
    }

    /**
     * @param string $productMainImage
     * @return Item
     */
    public function setProductMainImage(?string $productMainImage): Item
    {
        $this->productMainImage = $productMainImage;
        return $this;
    }

    public function getTotalPrice(): Money
    {
        return $this->totalPrice;
    }

    /**
     * @param Money $totalPrice
     * @return Item
     */
    public function setTotalPrice(Money $totalPrice): Item
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }
}