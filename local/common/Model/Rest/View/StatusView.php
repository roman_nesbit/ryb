<?php

namespace Rybalka\Marketplace\Model\Rest\View;

use Rybalka\Marketplace\Model\Order\Status;

class StatusView
{
    public function map(Status $status) {
        return [
            'id' => $status->getId(),
            'name' => $status->getName(),
            'description' => $status->getDescription()
        ];
    }
}