<?php

namespace Rybalka\Marketplace\Model\Rest\View;

use Rybalka\Marketplace\Model\Order;
use Rybalka\Marketplace\Model\Order\OrderRepository;
use Rybalka\Marketplace\Model\Rest\View\Order\ItemView;
use Rybalka\Marketplace\Service\Order\StatusService;

class OrderView
{
    /**
     * @var MoneyView
     */
    private $moneyView;

    /**
     * @var StatusView
     */
    private $statusView;

    /**
     * @var ItemView
     */
    private $orderItemView;

    /**
     * @var StatusService
     */
    private $statusService;

    public function __construct()
    {
        $this->moneyView = new MoneyView();
        $this->statusView = new StatusView();
        $this->orderItemView = new ItemView();
        $this->statusService = new StatusService();
    }

    public function map(Order $order)
    {
        $result = [
            'id' => $order->getPublicId(),
            'number' => $order->getItemsQuantity(),
            'totalPrice' => $this->moneyView->map($order->getTotalPrice()),
            'delivery' => $order->getDeliveryMethodName(),
            'date' => $order->getCreationDateTime()->format('c'),
            'currentStatus' => $this->statusView->map($order->getStatus()),
            'products' => []
        ];

        $result['currentStatus']['options'] = [];
        $allowedStatuses = $this->statusService->getAllowedStatuses($order->getStatus());

        foreach ($allowedStatuses as $allowedStatus) {
            $result['currentStatus']['options'][] = $this->statusView->map($allowedStatus);
        }

        foreach ($order->getItems() as $item) {
            $result['products'][] = $this->orderItemView->map($item);
        }

        return $result;
    }
}