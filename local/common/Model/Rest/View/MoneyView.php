<?php

namespace Rybalka\Marketplace\Model\Rest\View;

use Money\Currencies\ISOCurrencies;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use NumberFormatter;

class MoneyView
{
    private $numberFormatter;

    private $moneyFormatter;

    public function __construct()
    {
        $this->numberFormatter = new NumberFormatter('ru_RU', NumberFormatter::CURRENCY);
        $this->moneyFormatter = new IntlMoneyFormatter($this->numberFormatter, new ISOCurrencies());
    }

    public function map(Money $money) {
        return $this->moneyFormatter->format($money);
    }
}