<?php

namespace Rybalka\Marketplace\Model\Rest\View;

use Rybalka\Marketplace\Model\Order;
use Rybalka\Marketplace\Model\OrderCollection;

class OrderCollectionView
{
    /**
     * @var MoneyView
     */
    private $moneyView;

    public function __construct()
    {
        $this->moneyView = new MoneyView();
    }

    public function map(OrderCollection $orderCollection): array
    {
        $orders = [];

        foreach ($orderCollection as $item) {
            /**
             * @var Order $item
             */
            $orders[] = [
                'id' => $item->getPublicId(),
                'customer' => $item->getCustomer()->getFirstName() . ' ' . $item->getCustomer()->getLastName(),
                'date' => $item->getCreationDateTime()->format('c'),
                'quantity' => $item->getItemsQuantity(),
                'price' => $this->moneyView->map($item->getTotalPrice()),
                'status' => [
                    'id' => $item->getStatus()->getId(),
                    'label' => $item->getStatus()->getName()
                ]
            ];
        }

        return $orders;
    }
}