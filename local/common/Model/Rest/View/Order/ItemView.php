<?php

namespace Rybalka\Marketplace\Model\Rest\View\Order;

use Rybalka\Marketplace\Model\Order\Item;
use Rybalka\Marketplace\Model\Rest\View\MoneyView;

class ItemView
{
    private $moneyView;

    public function __construct()
    {
        $this->moneyView = new MoneyView();
    }

    public function map(Item $basketItem)
    {
        return [
            'name' => $basketItem->getName(),
            'product_id' => $basketItem->getProductId(),
            'image' => $basketItem->getProductMainImage(),
            'sku' => $basketItem->getProductSku(),
            'items' => $basketItem->getQuantity(),
            'pricePerItem' => $this->moneyView->map($basketItem->getProductPrice()),
            'totalPrice' => $this->moneyView->map($basketItem->getTotalPrice())
        ];
    }
}