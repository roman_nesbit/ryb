<?php


namespace Rybalka\Marketplace\Model\Rest\View\Checkout;


use Money\Currencies\ISOCurrencies;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Rybalka\Marketplace\Model\Checkout\DeliveryService;

class ServicePriceView
{
    /**
     * @var DeliveryService
     */
    private $data;

    public function __construct(Money $money)
    {
        $this->data = $money;
    }

    public function render()
    {
        $currencies = new ISOCurrencies();
        $moneyFormatter = new DecimalMoneyFormatter($currencies);

        $formattedValue = $moneyFormatter->format($this->data);

        $formatted = $this->data->isZero()
            ? 'бесплатно'
            : $formattedValue . ' грн.';

        return [
            'formatted' => $formatted,
            'canonical' => [
                'value' => $formattedValue,
                'currency' => $this->data->getCurrency()
            ]
        ];
    }
}