<?php


namespace Rybalka\Marketplace\Model\Rest\View\Checkout;


use Rybalka\Marketplace\Model\Checkout\DeliveryService;

class DeliveryServiceView
{
    /**
     * @var DeliveryService
     */
    private $data;

    public function __construct(DeliveryService $postOffice)
    {
        $this->data = $postOffice;
    }

    public function render(): array
    {
        $result = [
            'code' => $this->data->getCode(),
            'name' => $this->data->getName(),
            'description' => $this->data->getDescription()
        ];

        $price = !empty($this->data->getRoughPrice())
            ? $this->data->getRoughPrice()
            : $this->data->getPrice();

        $result['price'] = (new ServicePriceView($price))->render();

        if (!empty($this->data->getRequires())) {
            $result['requires'] = $this->data->getRequires();
        }

        if (!empty($this->data->getAccepts())) {
            $result['accepts'] = $this->data->getAccepts();
        }

        return $result;
    }
}