<?php


namespace Rybalka\Marketplace\Model\Rest\View\Dictionary\Address;


use Rybalka\Marketplace\Model\Dictionary\Address\Street;
use Rybalka\Marketplace\Model\Dictionary\Post\PostOffice;

class StreetView
{
    /**
     * @var Street
     */
    private $data;

    public function __construct(?Street $postOffice)
    {
        $this->data = $postOffice ?? Street::fromRaw([]);
    }

    public function render(): array
    {
        return [
            'id' => $this->data->getId(),
            'name' => (string)$this->data,
        ];
    }
}