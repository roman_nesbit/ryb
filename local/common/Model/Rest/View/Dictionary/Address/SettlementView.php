<?php

namespace Rybalka\Marketplace\Model\Rest\View\Dictionary\Address;

use Rybalka\Marketplace\Model\Dictionary\Address\Settlement;

class SettlementView
{
    /**
     * @var Settlement
     */
    private $data;

    public function __construct(?Settlement $data)
    {
        $this->data = $data ?? Settlement::fromRaw([]);
    }

    public function render(): array
    {
        return [
            'id' => $this->data->getId() ?? '',
            'name' => (string)$this->data,
            'doorstepDelivery' => $this->data->getDeliverySettlementId() != null
        ];
    }
}