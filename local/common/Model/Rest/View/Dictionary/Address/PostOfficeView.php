<?php


namespace Rybalka\Marketplace\Model\Rest\View\Dictionary\Address;


use Rybalka\Marketplace\Model\Dictionary\Post\PostOffice;

class PostOfficeView
{
    /**
     * @var PostOffice
     */
    private $data;

    public function __construct(?PostOffice $postOffice)
    {
        $this->data = $postOffice ?? PostOffice::fromRaw([]);
    }

    public function render(): array
    {
        return [
            'id' => $this->data->getId(),
            'name' => (string)$this->data,
            'address' => $this->data->getAddressRu(),
            'phoneNumber' => $this->data->getPhoneNumber()
        ];
    }
}