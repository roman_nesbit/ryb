<?php


namespace Rybalka\Marketplace\Model\Rest\Request\Order;


class SetStatusRequest
{
    /**
     * @var string
     */
    private $orderPublicId;

    /**
     * @var string
     */
    private $statusId;

    /**
     * @var string
     */
    private $note;

    /**
     * @return string
     */
    public function getOrderPublicId(): string
    {
        return $this->orderPublicId;
    }

    /**
     * @param string $orderPublicId
     * @return SetStatusRequest
     */
    public function setOrderPublicId(string $orderPublicId): SetStatusRequest
    {
        $this->orderPublicId = $orderPublicId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusId(): ?string
    {
        return $this->statusId;
    }

    /**
     * @param string $statusId
     * @return SetStatusRequest
     */
    public function setStatusId(?string $statusId): SetStatusRequest
    {
        $this->statusId = $statusId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return SetStatusRequest
     */
    public function setNote(?string $note): SetStatusRequest
    {
        $this->note = $note;
        return $this;
    }
}