<?php


namespace Rybalka\Marketplace\Model\Rest\Request;


use DateTime;

class OrdersFilter
{
    const DEFAULT_ITEMS_PER_PAGE = 10;
    const DEFAULT_PAGE = 1;

    const DEFAULT_SORT_FIELD = 'date';
    const DEFAULT_SORT_ORDER = self::SORT_ORDER_DESC;

    const SORT_ORDER_ASC  = 'ASC';
    const SORT_ORDER_DESC = 'DESC';

    const VALID_SORT_ORDERS = [self::SORT_ORDER_ASC, self::SORT_ORDER_DESC];

    const VALID_ITEMS_PER_PAGE = [5, 10, 25];

    const VALID_SORT_FIELDS = [
        'customer',
        'quantity',
        'date',
        'price',
        'status.label',
        'id'
    ];

    /**
     * @var int
     */
    private $shopId;

    /**
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    private $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE;

    /**
     * @var string
     */
    private $status;

    private $orderBy = self::DEFAULT_SORT_FIELD;

    private $sortOrder = self::DEFAULT_SORT_ORDER;

    /**
     * @var DateTime
     */
    private $dateFrom;

    /**
     * @var DateTime
     */
    private $dateTo;

    /**
     * @var string
     */
    private $freeTextFilter;

    /**
     * @param int $shopId
     * @return OrdersFilter
     */
    public function setShopId(int $shopId): OrdersFilter
    {
        $this->shopId = $shopId;
        return $this;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return OrdersFilter
     */
    public function setPage(?int $page): OrdersFilter
    {
        if ($page == null || $page < 1) {
            $page = self::DEFAULT_PAGE;
        }

        $this->page = $page;
        return $this;
    }

    public function getOffset(): int
    {
        return $this->itemsPerPage * ($this->page - 1);
    }

    /**
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     * @return OrdersFilter
     */
    public function setItemsPerPage(?int $itemsPerPage): OrdersFilter
    {
        if (!in_array($itemsPerPage, self::VALID_ITEMS_PER_PAGE)) {
            $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE;
        }
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return OrdersFilter
     */
    public function setStatus(?string $status): OrdersFilter
    {
        $this->status = $status;
        return $this;
    }

    public function setOrderBy(?string $fieldName): OrdersFilter
    {
        $fieldName = strtolower($fieldName);

        if (in_array($fieldName, self::VALID_SORT_FIELDS)) {
            $this->orderBy = $fieldName;
        }

        return $this;
    }

    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    /**
     * @return string
     */
    public function getSortOrder(): string
    {
        return $this->sortOrder;
    }

    /**
     * @param string $sortOrder
     * @return OrdersFilter
     */
    public function setSortOrder(?string $sortOrder): OrdersFilter
    {
        if ($sortOrder == null) {
            $sortOrder = self::DEFAULT_SORT_ORDER;
        }

        $sortOrder = strtoupper($sortOrder);

        if (!in_array($sortOrder, self::VALID_SORT_ORDERS)) {
            $sortOrder = self::DEFAULT_SORT_ORDER;
        }

        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom(): ?DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTime $dateFrom
     * @return OrdersFilter
     */
    public function setDateFrom(?DateTime $dateFrom): OrdersFilter
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTo(): ?DateTime
    {
        return $this->dateTo;
    }

    /**
     * @param DateTime $dateTo
     * @return OrdersFilter
     */
    public function setDateTo(?DateTime $dateTo): OrdersFilter
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreeTextFilter(): ?string
    {
        return $this->freeTextFilter;
    }

    /**
     * @param string $freeTextFilter
     * @return OrdersFilter
     */
    public function setFreeTextFilter(?string $freeTextFilter): OrdersFilter
    {
        if ($freeTextFilter != null) {
            $freeTextFilter = trim($freeTextFilter);
        }

        $this->freeTextFilter = $freeTextFilter;
        return $this;
    }
}