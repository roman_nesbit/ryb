<?php


namespace Rybalka\Marketplace\Model\Catalog;


class Product {

    private $id;

    public function __construct() {
    }

    public static function fromRawData(array $productRawData) {

        $product = new Product();

        foreach ($productRawData as $key => $value) {
            $key = strtolower($key);

            switch ($key) {
                case 'id':
                    $product->id = (int)$value;
                    break;
            }
        }

        return new Product();
    }
}