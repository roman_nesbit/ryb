<?php


namespace Rybalka\Marketplace\Model\Catalog;


use InvalidArgumentException;

class Category {

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $parentId = 0;

    /**
     * @var string
     */
    private $name = '';

    private $children;

    public function __construct() {
        $this->children = new CategoryCollection();
    }

    public static function fromRawData($data): Category {

        if (!is_array($data)) {
            throw new InvalidArgumentException('An array is expected to be passed as a source of data for a category');
        }

        $category = new Category();

        foreach ($data as $key => $value) {
            $key = strtolower($key);

            switch ($key) {
                case 'id':
                    $category->id = (int)$value;
                    break;
                case 'name':
                    $category->name = $value;
                    break;
                case 'iblock_section_id':
                    $category->parentId = (int)$value;
                    break;
            }
        }

        return $category;
    }

    public function addChild(Category $category) {
        $this->children->add($category);
    }

    public function toArray() {

        $result = [
            'id' => $this->id,
            'name' => $this->name
        ];

        $children = $this->children->toArray();

        if (!empty($children)) {
            $result['children'] = $children;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }
}