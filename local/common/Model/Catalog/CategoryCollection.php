<?php


namespace Rybalka\Marketplace\Model\Catalog;


class CategoryCollection
{
    private $elements = [];

    public function add(Category $category) {
        $this->elements[] = $category;
    }

    public function toArray() {
        $categories = [];

        foreach ($this->elements as $category) {
            $categories[] = $category->toArray();
        }

        return $categories;
    }
}