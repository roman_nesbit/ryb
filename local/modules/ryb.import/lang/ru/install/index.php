﻿<?
$MESS["RYB_IMPORT_MODULE_NAME"] = "Модуль импорта товаров";
$MESS["RYB_IMPORT_MODULE_DESC"] = "Модуль импорта товаров магазинов-партнеров";
$MESS["RYB_IMPORT_PARTNER_NAME"] = "Rybalka";
$MESS["RYB_IMPORT_PARTNER_URI"] = "rybalka.ua";
$MESS["RYB_IMPORT_INSTALL_ERROR_VERSION"] = "Ошибка";
$MESS["RYB_IMPORT_INSTALL_TITLE"] = "Установка Модуля импорта товаров";
$MESS["RYB_IMPORT_UNINSTALL_TITLE"] = "Удаление Модуля импорта товаров";
?>