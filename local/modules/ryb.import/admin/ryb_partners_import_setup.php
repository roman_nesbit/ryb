<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Rybalka\Marketplace\Partners\ImportTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

Loader::includeModule('iblock');

global $USER;
$canImportCatalog = $USER->CanDoOperation('catalog_import_edit');

if (!$canImportCatalog) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$sTableID = 'tblRybPartnersImportSetup';
$oSort = new CAdminSorting($sTableID, 'ID', 'desc');
$lAdmin = new CAdminUiList($sTableID, $oSort);

$arFilter = [];

// ФИЛЬТР
$filterFields = [
	[
		'id' => 'ID',
		'name' => 'ID импорта',
		'type' => 'number',
		'filterable' => '',
		'default' => true,
	],
	[
		'id' => 'PARTNER_NAME',
		'name' => 'Название партнера',
		'filterable' => '?',
		'quickSearch' => '?',
		'default' => true,
	],
	[
		'id' => 'PARTNER_ID',
		'name' => 'ID партнера',
		'type' => 'number',
		'filterable' => '',
		'default' => true,
	],
];
$lAdmin->AddFilter($filterFields, $arFilter);

// ******************************************************************** //
//                ОБРАБОТКА ДЕЙСТВИЙ НАД ЭЛЕМЕНТАМИ СПИСКА              //
// ******************************************************************** //

// сохранение отредактированных элементов
if ($lAdmin->EditAction()) {
	// пройдем по списку переданных элементов
	foreach ($FIELDS as $ID => $arFields) {
		if (!$lAdmin->IsUpdated($ID)) {
			continue;
		}

		$arData = [];
		foreach ($arFields as $key => $val) {
			$arData[$key] = $val;
		}

		// сохраним изменения каждого элемента
		$ID = IntVal($ID);
		if (!($result = ImportTable::update($ID, $arData))) {
			$lAdmin->AddGroupError(
				Loc::getMessage('RPIS_SAVE_ERROR') . ' ' . $result->LAST_ERROR,
				$ID
			);
		}
	}
}

// обработка одиночных и групповых действий
if (($arID = $lAdmin->GroupAction())) {
	// если выбрано "Для всех элементов"
	if ($_REQUEST['action_target'] == 'selected') {
		$rsData = ImportTable::getList([$by => $order], $arFilter);
		while ($arRes = $rsData->fetch()) {
			$arID[] = $arRes['ID'];
		}
	}

	// пройдем по списку элементов
	foreach ($arID as $ID) {
		if (strlen($ID) <= 0) {
			continue;
		}
		$ID = IntVal($ID);

		// для каждого элемента совершим требуемое действие
		switch ($_REQUEST['action']) {
			case "exec_agent":
				ImportTable::execAgent($ID);
				break;
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!ImportTable::delete($ID)) {
					$DB->Rollback();
					$lAdmin->AddGroupError(Loc::getMessage('RPIS_DEL_ERROR'), $ID);
				}
				$DB->Commit();
				break;

			case "activate":
				if (!ImportTable::activate($ID)) {
					$lAdmin->AddGroupError(Loc::getMessage('RPIS_SAVE_ERROR'));
				}
				break;
			case "deactivate":
				if (!ImportTable::deactivate($ID)) {
					$lAdmin->AddGroupError(Loc::getMessage('RPIS_SAVE_ERROR'));
				}
				break;
		}
	}
}


// ******************************************************************** //
//                ВЫБОРКА ЭЛЕМЕНТОВ СПИСКА                              //
// ******************************************************************** //
// exo($arFilter);

$arOrder = (strtoupper($by) === "ID"
	? [strtoupper($by) => $order]
	: [strtoupper($by) => $order, 'ID' => 'ASC']);

$cData = new ImportTable;
$rsData = $cData->getList([
	'select' => [
		'ID',
		'ACTIVE',
		'SORT',
		'PARTNER_ID',
		'PARTNER_NAME' => 'PARTNER.NAME',
		'DATE_CREATE',
		'CREATED_BY',
		'CREATED_BY_NAME' => 'CREATED_BY_USER.NAME',
		'CREATED_BY_LAST_NAME' => 'CREATED_BY_USER.LAST_NAME',
		'TIMESTAMP_X',
		'MODIFIED_BY',
		'MODIFIED_BY_NAME' => 'MODIFIED_BY_USER.NAME',
		'MODIFIED_BY_LAST_NAME' => 'MODIFIED_BY_USER.LAST_NAME',
		'URL',
		'PERIOD',
		'LAST_EXEC_SUCCESS',
		'DATE_LAST_EXEC' => 'AGENT.LAST_EXEC',
		'DATE_NEXT_EXEC' => 'AGENT.NEXT_EXEC',
		'AGENT_ID',
	],
	'filter' => $arFilter,
	'order' => $arOrder,
]);

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminUiResult($rsData, $sTableID);

// инициализируем постраничную навигацию.
$rsData->NavStart();
$lAdmin->SetNavigationParams($rsData);


// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders([
	[
		"id" => "ID",
		"content" => "ID",
		"sort" => "id",
		"default" => true,
	],
	[
		"id" => "SORT",
		"content" => Loc::getMessage('RPIS_FIELD_SORT'),
		"sort" => "sort",
		"default" => false,
	],
	[
		"id" => "ACTIVE",
		"content" => Loc::getMessage('RPIS_FIELD_ACTIVE'),
		"sort" => "active",
		"default" => true,
	],
	[
		"id" => "CREATED_BY",
		"content" => Loc::getMessage('RPIS_FIELD_CREATED_BY'),
		"sort" => "created_by",
		"default" => true,
	],
	[
		"id" => "DATE_CREATE",
		"content" => Loc::getMessage('RPIS_FIELD_DATE_CREATE'),
		"sort" => "date_create",
		"default" => false,
	],
	[
		"id" => "MODIFIED_BY",
		"content" => Loc::getMessage('RPIS_FIELD_MODIFIED_BY'),
		"sort" => "modified_by",
		"default" => true,
	],
	[
		"id" => "TIMESTAMP_X",
		"content" => Loc::getMessage('RPIS_FIELD_TIMESTAMP_X'),
		"sort" => "timestamp_x",
		"default" => false,
	],
	[
		"id" => "PARTNER",
		"content" => Loc::getMessage('RPIS_FIELD_PARTNER'),
		"sort" => "partner_name",
		"default" => true,
	],
	[
		'id' => 'URL',
		'content' => Loc::getMessage('RPIS_FIELD_URL'),
		'sort' => 'url',
		'default' => true,
	],
	[
		'id' => 'PERIOD',
		'content' => Loc::getMessage('RPIS_FIELD_PERIOD'),
		'sort' => 'period',
		'default' => false,
	],
	[
		'id' => 'AGENT_ID',
		'content' => Loc::getMessage('RPIS_FIELD_AGENT'),
		'sort' => 'agent_id',
		'default' => false,
	],
	[
		'id' => 'LAST_EXEC_SUCCESS',
		'content' => Loc::getMessage('RPIS_FIELD_LAST_EXEC_SUCCESS'),
		'sort' => 'last_exec_success',
		'default' => false,
	],
	[
		'id' => 'DATE_LAST_EXEC',
		'content' => Loc::getMessage('RPIS_FIELD_DATE_LAST_EXEC'),
		'sort' => 'date_last_exec',
		'default' => true,
	],
	[
		'id' => 'DATE_NEXT_EXEC',
		'content' => Loc::getMessage('RPIS_FIELD_DATE_NEXT_EXEC'),
		'sort' => 'date_next_exec',
		'default' => true,
	],
]);

//This is all parameters needed for proper navigation
$sThisSectionUrl = '&lang=' . LANGUAGE_ID;

// ЦИКЛ ПО ЗАПИСЯМ
while ($arRes = $rsData->NavNext(true, 'f_')) {
	// exo($arRes);

	// создаем строку. результат - экземпляр класса CAdminListRow
	$row = &$lAdmin->AddRow(
		$f_ID,
		$arRes,
		'ryb_partners_import_setup_edit.php?ID=' . $f_ID . '&lang=' . LANGUAGE_ID,
		Loc::getMessage('RPIS_DOUBLE_CLICK_ACTION')
	);

	$row->AddViewField('ID', '<a href="ryb_partners_import_setup_edit.php?ID=' . $f_ID . '&lang=' . LANGUAGE_ID . '">' . $f_ID . '</a>');
	if ($f_CREATED_BY > 0) {
		$row->AddViewField('CREATED_BY', $f_CREATED_BY_NAME . ' ' . $f_CREATED_BY_LAST_NAME . ' [<a href="">' . $f_CREATED_BY . '</a>]');
	} else {
		$row->AddViewField('CREATED_BY', '&mdash;');
	}
	if ($f_MODIFIED_BY > 0) {
		$row->AddViewField('MODIFIED_BY', $f_MODIFIED_BY_NAME . ' ' . $f_MODIFIED_BY_LAST_NAME . ' [<a href="">' . $f_MODIFIED_BY . '</a>]');
	} else {
		$row->AddViewField('MODIFIED_BY', '&mdash;');
	}
	if ($f_PARTNER_ID) {
		$row->AddViewField('PARTNER', $f_PARTNER_NAME . ' [<a href="iblock_element_edit.php?IBLOCK_ID=' . SHOPS_IBLOCK . '&ID=' . $f_PARTNER_ID . '&type=shops&lang=' . LANGUAGE_ID . '&find_section_section=0">' . $f_PARTNER_ID . '</a>]');
	} else {
		$row->AddViewField('PARTNER', '&mdash;');
	}
	$row->AddViewField('LAST_OPERATION_SUCCESS', Loc::getMessage('RPIS_' . $f_LAST_OPERATION_SUCCESS));
	$row->AddViewField('AGENT_ID', '<a href="agent_edit.php?ID=' . $f_AGENT_ID . '&lang=' . LANGUAGE_ID . '">' . $f_AGENT_ID . '</a>');

	$row->AddCheckField('ACTIVE');
	$row->AddInputField('SORT', ['size' => 20]);
	$row->AddInputField('URL', ['size' => 220]);
	$row->AddSelectField('PERIOD', [
		3 => '3',
		6 => '6',
		9 => '9',
		12 => '12',
		24 => '24'
	]);

	// сформируем контекстное меню
	$arActions = [];

	$arActions[] = [
		"TEXT" => Loc::getMessage('RPIS_EXEC_ACTION'),
		"ACTION" => $lAdmin->ActionDoGroup($f_ID, "exec_agent", $sThisSectionUrl),
		"ONCLICK" => "",
	];

	if ($arRes['ACTIVE'] == "Y") {
		$arActions[] = [
			"TEXT" => Loc::getMessage('RPIS_DEACTIVATE_ACTION'),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "deactivate", $sThisSectionUrl),
			"ONCLICK" => "",
		];
	} else {
		$arActions[] = [
			"TEXT" => Loc::getMessage('RPIS_ACTIVATE_ACTION'),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "activate", $sThisSectionUrl),
			"ONCLICK" => "",
		];
	}

	// редактирование элемента
	$arActions[] = [
		"ICON" => "edit",
		"DEFAULT" => true,
		"TEXT" => Loc::getMessage('RPIS_EDIT_ACTION'),
		"ACTION" => $lAdmin->ActionRedirect("ryb_partners_import_setup_edit.php?ID=" . $f_ID)
	];

	// удаление элемента
    $arActions[] = [
        "ICON" => "delete",
        "TEXT" => Loc::getMessage('RPIS_DELETE_ACTION'),
        "ACTION" => "if(confirm('" . Loc::getMessage('RPIS_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
    ];

	// применим контекстное меню к строке
	$row->AddActions($arActions);
}

// резюме таблицы
$lAdmin->AddFooter([
	[
		"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
		"value" => $rsData->SelectedRowsCount()
	], // кол-во элементов
	[
		"counter" => true,
		"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
		"value" => "0"
	], // счетчик выбранных элементов
]);

// групповые действия
$lAdmin->AddGroupActionTable([
	"edit" => GetMessage("MAIN_ADMIN_LIST_EDIT"), // изменить выбранные элементы
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
	"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // активировать выбранные элементы
	"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // деактивировать выбранные элементы
]);


// ******************************************************************** //
//                АДМИНИСТРАТИВНОЕ МЕНЮ                                 //
// ******************************************************************** //

$aContext = [
	[
		"TEXT" => Loc::getMessage('RPIS_CREATE_ACTION'),
		"LINK" => "ryb_partners_import_setup_edit.php?lang=" . LANG,
		"TITLE" => Loc::getMessage('RPIS_CREATE_TITLE'),
		"ICON" => "btn_new",
	],
];
// и прикрепим его к списку
$lAdmin->AddAdminContextMenu($aContext);


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle(Loc::getMessage('RPIS_TITLE'));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// выведем фильтр
$lAdmin->DisplayFilter($filterFields);

// выведем таблицу списка элементов
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
