<?
global $USER;

$canImportCatalog = $USER->CanDoOperation('catalog_import_edit');

if (!$canImportCatalog) {
    return [];
}

$aMenu = [    
	'parent_menu' => 'global_menu_store',
	'sort' => 450,
	'text' => 'Партнеры',
	'title' => 'Управление магазинами-партнерами',
	'icon' => 'smile_menu_icon',
	'page_icon' => 'sale_page_icon_bigdata',
	'items_id' => 'ryb_partners',
	'items' => [
		[
			'text' => 'Импорт товаров',
			'title' => 'Управление импортом товаров партнеров',
			'items_id' => 'ryb_partners_import',
			'items' => [
				[
					'text' => 'Настройки',
					'title' => 'Настройки импорта',
					'url' => 'ryb_partners_import_setup.php?lang=' . LANGUAGE_ID,
				],
				[
					'text' => 'История',
					'title' => 'История выполнения импорта',
					'url' => 'ryb_partners_import_history.php?lang=' . LANGUAGE_ID,
				],
				[
					'text' => 'Проверка агентов',
					'title' => 'Восстановление удаленных агентов',
					'url' => 'ryb_partners_import_check_agents.php?lang=' . LANGUAGE_ID
				],
			],
		],        
	]
];

return (!empty($aMenu) ? $aMenu : false);