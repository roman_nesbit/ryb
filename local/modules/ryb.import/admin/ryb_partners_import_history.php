<?

use Bitrix\Main\Localization\Loc;
use Rybalka\Marketplace\Partners\ImportHistoryTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

global $USER;
$canImportCatalog = $USER->CanDoOperation('catalog_import_edit');

if (!$canImportCatalog) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$sTableID = 'tblRybPartnersImportHistory';
$oSort = new CAdminSorting($sTableID, 'ID', 'desc');
$lAdmin = new CAdminUiList($sTableID, $oSort);

// ФИЛЬТР
$filterFields = [
	[
		'id' => 'ID',
		'name' => 'ID',
		'type' => 'number',
		'filterable' => '',
		'default' => false,
	],
	[
		'id' => 'PARTNER_NAME',
		'name' => 'Название партнера',
		'filterable' => '?',
		'quickSearch' => '?',
		'default' => true,
	],
	[
		'id' => 'PARTNER_ID',
		'name' => 'ID партнера',
		'type' => 'number',
		'filterable' => '',
		'default' => false,
	],
	[
		'id' => 'EXEC_SUCCESS',
		'name' => 'Успешные операции',
		'type' => 'list',
		'items' => [
			'Y' => Loc::getMessage('RPIH_Y'),
			'N' => Loc::getMessage('RPIH_N'),
		],
		'filterable' => '',
		'default' => true,
	],
	[
		'id' => 'DATE_EXEC',
		'name' => 'Дата операции',
		'type' => 'date',
		'filterable' => '',
		'default' => true,
	]
];
$arFilter = [];

$lAdmin->AddFilter($filterFields, $arFilter);

// ******************************************************************** //
//                ОБРАБОТКА ДЕЙСТВИЙ НАД ЭЛЕМЕНТАМИ СПИСКА              //
// ******************************************************************** //

// сохранение отредактированных элементов
if ($lAdmin->EditAction()) {
	// пройдем по списку переданных элементов
	foreach ($FIELDS as $ID => $arFields) {
		if (!$lAdmin->IsUpdated($ID)) {
			continue;
		}

		$arData = [];
		foreach ($arFields as $key => $val) {
			$arData[$key] = $val;
		}

		// сохраним изменения каждого элемента
		$ID = IntVal($ID);
		if (!($result = ImportTable::update($ID, $arData))) {
			$lAdmin->AddGroupError(
				Loc::getMessage('RPIH_SAVE_ERROR') . ' ' . $result->LAST_ERROR,
				$ID
			);
		}
	}
}

// обработка одиночных и групповых действий
if (($arID = $lAdmin->GroupAction())) {
	// если выбрано "Для всех элементов"
	if ($_REQUEST['action_target'] == 'selected') {
		$rsData = ImportTable::getList([$by => $order], $arFilter);
		while ($arRes = $rsData->fetch()) {
			$arID[] = $arRes['ID'];
		}
	}

	// пройдем по списку элементов
	foreach ($arID as $ID) {
		if (strlen($ID) <= 0) {
			continue;
		}
		$ID = IntVal($ID);

		// для каждого элемента совершим требуемое действие
		switch ($_REQUEST['action']) {
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!ImportHistoryTable::delete($ID)) {
					$DB->Rollback();
					$lAdmin->AddGroupError(Loc::getMessage('RPIH_DEL_ERROR'), $ID);
				}
				$DB->Commit();
				break;
		}
	}
}


// ******************************************************************** //
//                ВЫБОРКА ЭЛЕМЕНТОВ СПИСКА                              //
// ******************************************************************** //
// exo($arFilter);

$arOrder = (strtoupper($by) === "ID"
	? [strtoupper($by) => $order]
	: [strtoupper($by) => $order, 'ID' => 'ASC']);

$cData = new ImportHistoryTable;
$rsData = $cData->getList([
	'select' => [
		'ID',
		'DATE_EXEC',
		'EXEC_SUCCESS',
		'PARTNER_ID',
		'PARTNER_NAME' => 'PARTNER.NAME',
		'AGENT_ID',
		'CATEGORIES_NEW',
		'CATEGORIES_TOTAL',
		'PRODUCTS_NEW',
		'PRODUCTS_DEACTIVATED',
		'PRODUCTS_TOTAL',
		'PROPERTIES_NEW',
		'PROPERTIES_TOTAL',
		'LOG',
	],
	'filter' => $arFilter,
	'order' => $arOrder,
]);

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminUiResult($rsData, $sTableID);

// инициализируем постраничную навигацию.
$rsData->NavStart();
$lAdmin->SetNavigationParams($rsData);


// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders([
	[
		"id" => "ID",
		"content" => "ID",
		"sort" => "id",
		"default" => true,
	],
	[
		"id" => "DATE_EXEC",
		"content" => Loc::getMessage('RPIH_FIELD_DATE_EXEC'),
		"sort" => "date_exec",
		"default" => false,
	],
	[
		'id' => 'EXEC_SUCCESS',
		'content' => Loc::getMessage('RPIH_FIELD_EXEC_SUCCESS'),
		'sort' => 'exec_success',
		'default' => false,
	],
	[
		"id" => "PARTNER",
		"content" => Loc::getMessage('RPIH_FIELD_PARTNER'),
		"sort" => "partner_name",
		"default" => true,
	],
	[
		'id' => 'AGENT_ID',
		'content' => Loc::getMessage('RPIH_FIELD_AGENT'),
		'sort' => 'agent_id',
		'default' => false,
	],
	[
		'id' => 'CATEGORIES',
		'content' => Loc::getMessage('RPIH_FIELD_CATEGORIES'),
		'sort' => false,
		'default' => true,
	],
	[
		'id' => 'PRODUCTS',
		'content' => Loc::getMessage('RPIH_FIELD_PRODUCTS'),
		'sort' => false,
		'default' => true,
	],
	[
		'id' => 'PROPERTIES',
		'content' => Loc::getMessage('RPIH_FIELD_PROPERTIES'),
		'sort' => false,
		'default' => true,
	],
	[
		'id' => 'LOG',
		'content' => Loc::getMessage('RPIH_FIELD_LOG'),
		'sort' => 'log',
		'default' => true,
	],
]);

//This is all parameters needed for proper navigation
$sThisSectionUrl = '&lang=' . LANGUAGE_ID;

// ЦИКЛ ПО ЗАПИСЯМ
while ($arRes = $rsData->NavNext(true, 'f_')) {
	// exo($arRes);

	// создаем строку. результат - экземпляр класса CAdminListRow
	$row = &$lAdmin->AddRow(
		$f_ID,
		$arRes,
		'',// 'ryb_partners_import_setup_edit.php?ID=' . $f_ID . '&lang=' . LANGUAGE_ID,
		''// Loc::getMessage('RPIH_DOUBLE_CLICK_ACTION')
	);

	// $row->AddViewField('ID', '<a href="ryb_partners_import_setup_edit.php?ID=' . $f_ID . '&lang=' . LANGUAGE_ID . '">' . $f_ID . '</a>');
	if ($f_PARTNER_ID) {
		$row->AddViewField('PARTNER', $f_PARTNER_NAME . ' [<a href="iblock_element_edit.php?IBLOCK_ID=' . SHOPS_IBLOCK . '&ID=' . $f_PARTNER_ID . '&type=shops&lang=' . LANGUAGE_ID . '&find_section_section=0">' . $f_PARTNER_ID . '</a>]');
	} else {
		$row->AddViewField('PARTNER', '&mdash;');
	}
	$row->AddViewField('OPERATION_SUCCESS', Loc::getMessage('RPIH_' . $f_OPERATION_SUCCESS));
	$row->AddViewField('AGENT_ID', '<a href="agent_edit.php?ID=' . $f_AGENT_ID . '&lang=' . LANGUAGE_ID . '">' . $f_AGENT_ID . '</a>');
	$row->AddViewField('CATEGORIES', $f_CATEGORIES_NEW . '/' . $f_CATEGORIES_TOTAL);
	$row->AddViewField('PRODUCTS', $f_PRODUCTS_NEW . '/' . $f_PRODUCTS_DEACTIVATED . '/' . $f_PRODUCTS_TOTAL);
	$row->AddViewField('PROPERTIES', $f_PROPERTIES_NEW . '/' . $f_PROPERTIES_TOTAL);

	// сформируем контекстное меню
	$arActions = [];

	// удаление элемента
    $arActions[] = [
        "ICON" => "delete",
        "TEXT" => Loc::getMessage('RPIH_DELETE_ACTION'),
        "ACTION" => "if(confirm('" . Loc::getMessage('RPIH_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
    ];

	// применим контекстное меню к строке
	$row->AddActions($arActions);
}

// резюме таблицы
$lAdmin->AddFooter([
	[
		"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
		"value" => $rsData->SelectedRowsCount()
	], // кол-во элементов
	[
		"counter" => true,
		"title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"),
		"value" => "0"
	], // счетчик выбранных элементов
]);

// групповые действия
$lAdmin->AddGroupActionTable([
	// "edit" => GetMessage("MAIN_ADMIN_LIST_EDIT"), // изменить выбранные элементы
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
	// "activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // активировать выбранные элементы
	// "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // деактивировать выбранные элементы
]);


// ******************************************************************** //
//                АДМИНИСТРАТИВНОЕ МЕНЮ                                 //
// ******************************************************************** //

// $aContext = [
// 	[
// 		"TEXT" => Loc::getMessage('RPIH_CREATE_ACTION'),
// 		"LINK" => "ryb_partners_import_setup_edit.php?lang=" . LANG,
// 		"TITLE" => Loc::getMessage('RPIH_CREATE_TITLE'),
// 		"ICON" => "btn_new",
// 	],
// ];
// // и прикрепим его к списку
// $lAdmin->AddAdminContextMenu($aContext);


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle(Loc::getMessage('RPIH_TITLE'));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// выведем фильтр
$lAdmin->DisplayFilter($filterFields);

// выведем таблицу списка элементов
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
