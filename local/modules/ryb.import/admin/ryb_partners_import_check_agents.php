<?
	use Bitrix\Main\Localization\Loc;
use Rybalka\Marketplace\Partners\ImportTable;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

global $USER;
$canImportCatalog = $USER->CanDoOperation('catalog_import_edit');

if (!$canImportCatalog) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$message = null;

// установим заголовок страницы
$APPLICATION->SetTitle(Loc::getMessage('RPICA_TITLE'));

$arResult = ImportTable::checkAgents();

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// конфигурация административного меню
$aMenu = [
	[
		"TEXT" => Loc::getMessage('RPICA_LIST_ACTION'),
		"TITLE" => Loc::getMessage('RPICA_LIST_TITLE'),
		"LINK" => "ryb_partners_import_setup.php?lang=" . LANG,
		"ICON" => "btn_list",
	]
];

// создание экземпляра класса административного меню
$context = new CAdminContextMenu($aMenu);

// вывод административного меню
$context->Show();

echo Loc::getMessage('RPICA_CHECKED', [
	'#TOTAL#' => $arResult['total'],
	'#CREATED#' => $arResult['created'],
]);

// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if ($_REQUEST["mess"] == "ok" && $ID > 0) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => Loc::getMessage('RPICA_SAVED'),
		"TYPE" => "OK"
	]);
}

if ($message) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => $message,
		"TYPE" => "ERROR"
	]);
} elseif ($res && !$res->isSuccess()) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => $res->getErrorMessage(),
		"TYPE" => "ERROR"
	]);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");