<?

use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Rybalka\Marketplace\Model\PartnerShop\Import\ImportConfiguration;
use Rybalka\Marketplace\Partners\ImportTable;
use Rybalka\Marketplace\Partners\ImportXml;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCatalogItemParametersTable;
use Rybalka\Marketplace\Repository\PartnerShop\Import\ExternalCategoriesTable;
use Rybalka\Marketplace\Util\PropUtil;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

if (!Loader::includeModule("iblock")) return;

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

$parametersRepository = new ExternalCatalogItemParametersTable();
$categoriesRepository = new ExternalCategoriesTable();

global $USER;
$canImportCatalog = $USER->CanDoOperation('catalog_import_edit');

if (!$canImportCatalog) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

// сформируем список закладок
$aTabs = [
	[
		"DIV" => "edit1",
		"TAB" => Loc::getMessage('RPISE_TAB1'),
		"ICON" => "main_user_edit",
		"TITLE" => Loc::getMessage("RPISE_TAB1_TITLE")
	],
	[
		"DIV" => "edit2",
		"TAB" => Loc::getMessage('RPISE_TAB2'),
		"ICON" => "main_user_edit",
		"TITLE" => Loc::getMessage("RPISE_TAB2_TITLE")
	],
	[
		"DIV" => "edit3",
		"TAB" => Loc::getMessage('RPISE_TAB3'),
		"ICON" => "main_user_edit",
		"TITLE" => Loc::getMessage("RPISE_TAB3_TITLE")
	]
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$ID = intval($ID);
$message = null;
$bVarsFromForm = false;


// ******************************************************************** //
//                ОБРАБОТКА ИЗМЕНЕНИЙ ФОРМЫ                             //
// ******************************************************************** //
if (
	$REQUEST_METHOD == "POST" // проверка метода вызова страницы
	&& ($save != "" || $apply != "") // проверка нажатия кнопок "Сохранить" и "Применить"
	&& check_bitrix_sessid()     // проверка идентификатора сессии
) {
	// обработка данных формы
	$arFields = [
		'ACTIVE' => $ACTIVE == 'Y' ?? 'N',
		'SORT' => $SORT,
		'PARTNER_ID' => $PARTNER_ID,
		'URL' => $URL,
		'PERIOD' => $PERIOD,
	];

	// сохранение данных
	if (ImportTable::save($ID, $arFields, $MAPPING)) {
		// если сохранение прошло удачно - перенаправим на новую страницу
		// (в целях защиты от повторной отправки формы нажатием кнопки "Обновить" в браузере)
		if ($apply != "") {
			// если была нажата кнопка "Применить" - отправляем обратно на форму.
			LocalRedirect("/bitrix/admin/ryb_partners_import_setup_edit.php?ID=" . $ID . "&mess=ok&lang=" . LANG . "&" . $tabControl->ActiveTabParam());
		} else {
			// если была нажата кнопка "Сохранить" - отправляем к списку элементов.
			LocalRedirect("/bitrix/admin/ryb_partners_import_setup.php?lang=" . LANG);
		}
	} else {
		// если в процессе сохранения возникли ошибки - получаем текст ошибки и меняем вышеопределённые переменные
		// if ($e = $APPLICATION->GetException()) {
		$message = Loc::getMessage('RPISE_SAVE_ERROR');
		// }
		// $lAdmin->AddGroupError(Loc::getMessage('RPISE_SAVE_ERROR'));
		$bVarsFromForm = true;
	}
}


// ******************************************************************** //
//                ВЫБОРКА И ПОДГОТОВКА ДАННЫХ ФОРМЫ                     //
// ******************************************************************** //

// значения по умолчанию
$str_ACTIVE = "Y";
$str_SORT = 500;
$str_PARTNER_ID = null;
$str_URL = "http://";
$str_PERIOD = 24;

// если пришли данные из формы
if ($MAPPING) {
	$arFormMapping = $MAPPING;
}

// выборка данных
if ($ID > 0) {
	$cData = new ImportTable;
	$resData = $cData->getList([
		'select' => [
			'ID',
			'ACTIVE',
			'SORT',
			'PARTNER_ID',
			'PARTNER_NAME' => 'PARTNER.NAME',
			'DATE_CREATE',
			'CREATED_BY',
			'CREATED_BY_NAME' => 'CREATED_BY_USER.NAME',
			'CREATED_BY_LAST_NAME' => 'CREATED_BY_USER.LAST_NAME',
			'TIMESTAMP_X',
			'MODIFIED_BY',
			'MODIFIED_BY_NAME' => 'MODIFIED_BY_USER.NAME',
			'MODIFIED_BY_LAST_NAME' => 'MODIFIED_BY_USER.LAST_NAME',
			'URL',
			'PERIOD',
			'LAST_EXEC_SUCCESS',
			'DATE_LAST_EXEC' => 'AGENT.LAST_EXEC',
			'DATE_NEXT_EXEC' => 'AGENT.NEXT_EXEC',
		],
		'filter' => [
			'ID' => $ID,
		],
	]);
	if (!$arData = $resData->fetch()) {
		$ID = 0;
	} else {
		foreach ($arData as $key => $val) {
			$GLOBALS['str_' . $key] = $val;
		}
	}

	// MAPPING
    $importConfiguration = ImportConfiguration::fromRawData($arData);

	$xmlData = new ImportXml($importConfiguration);
	if ($xmlData->noErrors()) {
		$MAPPING = [
			'CATEGORIES' => $categoriesRepository->getAllMappingData($importConfiguration->getPartnerId()),
			'PARAMS' => $parametersRepository->getAllMappings($importConfiguration->getPartnerId()),
		];
	}

	// Заполняем данными из формы
	if ($arFormMapping) {
		// exo($arFormMapping);
		foreach ($MAPPING as $mappingType => $mappingValues) {
			foreach ($mappingValues as $mappingKey => $mappingValue) {
				if(isset($arFormMapping[$mappingType][$mappingKey]['ref_id'])) {
					$MAPPING[$mappingType][$mappingKey]['ref_id'] = $arFormMapping[$mappingType][$mappingKey]['ref_id'];
				}
				if (isset($arFormMapping[$mappingType][$mappingKey]['ignored'])) {
					$MAPPING[$mappingType][$mappingKey]['ignored'] = $arFormMapping[$mappingType][$mappingKey]['ignored'];
				}
			}
		}
	}
	//exo($MAPPING); // Check mapping array here
}

// если данные переданы из формы, инициализируем их
if ($bVarsFromForm) {
	$DB->InitTableVarsForEdit("ryb_partners_import", "", "str_");
}


// List of partners without export + this partner to reselect
$resPartners = ElementTable::getList([
	'select' => [
		'ID',
		'NAME',
	],
	'filter' => [
		'IBLOCK_ID' => SHOPS_IBLOCK,
		'ACTIVE' => 'Y',
		[
			'LOGIC' => 'OR',
			'!ID' => ImportTable::getPartners(),
			'ID' => $str_PARTNER_ID,
		],
	],
	'order' => [
		'NAME' => 'ASC',
	],
]);
$arPartners = [];
while ($arPartner = $resPartners->fetch()) {
	$arPartners[$arPartner['ID']] = [
		'NAME' => $arPartner['NAME'],
		'CATEGORY_NAME' => false,
	];
}

// List of categories for mapping
$resSections = SectionTable::getList([
	'select' => [
		'ID',
		'NAME',
		'DEPTH_LEVEL',
		'IBLOCK_SECTION_ID',
	],
	'filter' => [
		'IBLOCK_ID' => ITEMS_IBLOCK,
		'ACTIVE' => 'Y',
	],
	'order' => [
		'LEFT_MARGIN' => 'ASC',
	],
]);
$arCategories = [];
while ($arCategory = $resSections->fetch()) {
	$categoryLevel{
		$arCategory['DEPTH_LEVEL']} = $arCategory['NAME'];
	$categoryName = ($arCategory['DEPTH_LEVEL'] > 2 ? ' > ' . $categoryLevel{
		2} : '') . ($arCategory['DEPTH_LEVEL'] > 1 ? ' > ' . $categoryLevel{
		1} : '');
	$categorySelected = (int) $arCategory['ID'] == (int) $MAPPING[$mappingType][$mappingKey]['ref_id'];

	$arCategories[$arCategory['ID']] = [
		'NAME' => $arCategory['NAME'] . ' (' . $arCategory['ID'] . ')',
		'CATEGORY_NAME' => $categoryName,
	];
}

// List of product parameters for mapping
$resParams = PropertyTable::getList([
	'select' => [
		'ID',
		'NAME',
		'CODE',
	],
	'filter' => [
		'IBLOCK_ID' => ITEMS_IBLOCK,
		'ACTIVE' => 'Y',
	],
	'order' => [
		'NAME' => 'ASC',
	]
]);
$arParams = [];
while ($arParam = $resParams->fetch()) {
	$arParams[$arParam['CODE']] = [
		'NAME' => $arParam['NAME'] . ' (' . $arParam['CODE'] . ')',
	];
}

// ******************************************************************** //
//                ВЫВОД ФОРМЫ                                           //
// ******************************************************************** //

// установим заголовок страницы
$APPLICATION->SetTitle(($ID > 0 ? Loc::getMessage('RPISE_EDIT') . ' ' . $arPartners[$str_PARTNER_ID]['NAME'] : Loc::getMessage('RPISE_ADD')));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// конфигурация административного меню
$aMenu = [
	[
		"TEXT" => Loc::getMessage('RPISE_LIST_ACTION'),
		"TITLE" => Loc::getMessage('RPISE_LIST_TITLE'),
		"LINK" => "ryb_partners_import_setup.php?lang=" . LANG,
		"ICON" => "btn_list",
	]
];

if ($ID > 0) {
	$aMenu[] = ["SEPARATOR" => "Y"];
	$aMenu[] = [
		"TEXT" => Loc::getMessage('RPISE_ADD_ACTION'),
		"TITLE" => Loc::getMessage('RPISE_ADD_TITLE'),
		"LINK" => "ryb_partners_import_setup_edit.php?lang=" . LANG,
		"ICON" => "btn_new",
	];
	$aMenu[] = [
		"TEXT" => Loc::getMessage('RPISE_DELETE_ACTION'),
		"TITLE" => Loc::getMessage('RPISE_DELETE_TITLE'),
		"LINK" => "javascript:if(confirm('" . Loc::getMessage('RPISE_DELETE_CONFIRM') . "'))window.location='ryb_partners_import_setup.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "';",
		"ICON" => "btn_delete",
	];
}

// создание экземпляра класса административного меню
$context = new CAdminContextMenu($aMenu);

// вывод административного меню
$context->Show();


// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if ($_REQUEST["mess"] == "ok" && $ID > 0) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => Loc::getMessage('RPISE_SAVED'),
		"TYPE" => "OK"
	]);
}

if ($message) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => $message,
		"TYPE" => "ERROR"
	]);
} elseif ($res && !$res->isSuccess()) {
	CAdminMessage::ShowMessage([
		"MESSAGE" => $res->getErrorMessage(),
		"TYPE" => "ERROR"
	]);
}
?>
<div class="adm-info-message-wrap">
    <div class="adm-info-message">
        <b>Важно!</b> Настройка сопоставления категорий действует только на новые импортируемые товары. Если товар уже в каталоге, его категорию возможно изменить только ручным редактированием <a href="/bitrix/admin/cat_product_list.php?lang=ru&IBLOCK_ID=4&type=catalog&find_section_section=-1">здесь</a>.
    </div>
</div>
<script>
	function setDrawlistValue(theInput) {
		var theInputId = theInput.getAttribute('id');
		var theListId = theInputId + '_list';
		var theValueId = theInputId + '_value';

		if (theInput.value) {
			var theListOption = document.querySelector("#" + theListId + " option[value='" + theInput.value + "']");
			if (theListOption) {
				theValueToSet = theListOption.dataset.id;
				if (theValueToSet) {
					document.getElementById(theValueId).value = theValueToSet;
				}
			}
		} else {
			document.getElementById(theValueId).value = null;
		}
	}

	function toggleMainBlock(theInput) {
		document.getElementById(theInput.dataset.id + '_mainblock').style.display = (theInput.checked ? 'none' : 'block');
	}
</script>
<?
function drawDatalist($arItems, $curId, $id, $name, $readonly = false, $ignored = NULL)
{
	if(isset($ignored)):
		?><div style="margin-bottom:2px;">
			<label>
				<input type="checkbox"
					name="<?=str_replace('ref_id', 'ignored', $name)?>" 
					<?=($ignored == 1 ? 'checked' : '')?> 
					value="1" 
					onchange="toggleMainBlock(this)" 
					data-id="<?=$id?>" /> Не импортировать
			</label>
    	</div><?
	endif;
	
	?><div style="display:<?=($ignored ? 'none' : 'block')?>;"
		id="<?=$id?>_mainblock">
		<input type="hidden" id="<?= $id ?>_value" name="<?= $name ?>" value="<?= $curId ?>" />
		<input type="text" id="<?= $id ?>" list="<?= $id ?>_list" value="<?= $arItems[$curId]['NAME'] ?>" autocomplete="off" oninput="setDrawlistValue(this)" style="width:90%" <?=($readonly ? 'readonly="readonly"' : '')?> /><?
		if(!$readonly):
			?><datalist id="<?= $id ?>_list">
				<? foreach ($arItems as $itemId => $arItem) : ?>
					<option value="<?= $arItem['NAME'] ?>" data-id="<?= $itemId ?>" <?= ((int) $itemId === (int) $curId ? 'selected' : '') ?>>
						<?= $arItem['CATEGORY_NAME'] ?>
					</option>
				<? endforeach;
			?></datalist><?
		endif;
	?></div><?
}
?>

<form method="POST" Action="<? echo $APPLICATION->GetCurPage() ?>" ENCTYPE="multipart/form-data" name="post_form">
	<? // проверка идентификатора сессии 
	?>
	<?= bitrix_sessid_post() ?>
	<?
	// отобразим заголовки закладок
	$tabControl->Begin();
	?>
	<?
	//********************
	// первая закладка
	//********************
	$tabControl->BeginNextTab();
	$xmlErrors = $xmlData ? $xmlData->getLog('errors') : [];
	if (count($xmlErrors)) {
		foreach($xmlErrors as $xmlError) {
			CAdminMessage::ShowMessage($xmlError);
		}
	} ?>
	<tr>
		<td width="40%"><?= Loc::getMessage('RPISE_FIELD_ID') ?></td>
		<td width="60%"><?= $str_ID ?></td>
	</tr>
	<tr>
		<td width="40%"><?= Loc::getMessage('RPISE_FIELD_CREATED') ?></td>
		<td width="60%">
			<? if ($str_DATE_CREATE || $str_CREATED_BY) : ?>
				<?= $str_DATE_CREATE ?>
				&mdash;
				<?= $str_CREATED_BY_NAME ?>&nbsp;<?= $str_CREATED_BY_LAST_NAME ?>&nbsp;[<a href="user_edit.php?lang=<?= $LANG ?>&ID=<?= $str_CREATED_BY ?>"><?= $str_CREATED_BY ?></a>]
			<? endif; ?>
		</td>
	</tr>
	<tr>
		<td width="40%"><?= Loc::getMessage('RPISE_FIELD_MODIFIED') ?></td>
		<td width="60%">
			<? if ($str_TIMESTAMP_X || $str_MODIFIED_BY) : ?>
				<?= $str_TIMESTAMP_X ?>
				&mdash;
				<?= $str_MODIFIED_BY_NAME ?>&nbsp;<?= $str_MODIFIED_BY_LAST_NAME ?>&nbsp;[<a href="user_edit.php?lang=<?= $LANG ?>&ID=<?= $str_MODIFIED_BY ?>"><?= $str_MODIFIED_BY ?></a>]
			<? endif; ?>
		</td>
	</tr>
	<tr>
		<td width="40%"><?= Loc::getMessage("RPISE_FIELD_ACTIVE") ?></td>
		<td width="60%"><input type="checkbox" name="ACTIVE" value="Y" <?= ($str_ACTIVE == 'Y' ? "checked" : '') ?>></td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_SORT") ?></td>
		<td><input type="text" name="SORT" value="<?= $str_SORT ?>" size="10"></td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_PARTNER") ?></td>
		<td>
			<? drawDatalist($arPartners, $str_PARTNER_ID, 'rybPartner', 'PARTNER_ID', isset($str_PARTNER_ID)); ?>
		</td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_URL") ?></td>
		<td><input type="text" name="URL" value="<?= $str_URL ?>" size="80"></td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_PERIOD") ?></td>
		<td>
			<? $arPeriodValues = [
				3 => '3 часа',
				6 => '6 часов',
				9 => '9 часов',
				12 => '12 часов',
				24 => '24 часа',
			]; ?>
			<select type="text" name="PERIOD">
				<? foreach ($arPeriodValues as $val => $title) : ?>
					<option value="<?= $val ?>" <?= ($val == $str_PERIOD ? 'selected' : '') ?>><?= $title ?></option>
				<? endforeach; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_DATE_LAST_EXEC") ?></td>
		<td><?= $str_DATE_LAST_EXEC ?></td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_LAST_EXEC_SUCCESS") ?></td>
		<td><?= Loc::getMessage("RPISE_" . $str_LAST_EXEC_SUCCESS) ?></td>
	</tr>
	<tr>
		<td><?= Loc::getMessage("RPISE_FIELD_DATE_NEXT_EXEC") ?></td>
		<td><?= $str_DATE_NEXT_EXEC ?></td>
	</tr>

	<?
	//********************
	// вторая и пр. закладки - Меппинг...
	//********************

	foreach ($MAPPING as $mappingType => $mappingValues) :
		$tabControl->BeginNextTab();
		foreach ($mappingValues as $mappingKey => $mappingValue) : ?>
			<tr>
				<? if (!$mappingValue['parent_id']) : ?>
					<td class="adm-required-field" colspan="2" align="center"><?= $mappingValue['name'] ?></td>
				<? else : ?>
					<td width="40%" valign="top"><?php
                        $displayName = $mappingValue['name'];

                        if (array_key_exists('category_name', $mappingValue)) {
                            $categoryName = $mappingValue['category_name'] ?? 'Все категории';
                            $displayName .= ' (' . $categoryName . ')';
                        }

                        echo $displayName;
                        ?>
                    </td>
					<td><?
						switch ($mappingType):

							case 'CATEGORIES':
								drawDatalist(
									$arCategories,
									$MAPPING[$mappingType][$mappingKey]['ref_id'],
									'rybMapping_' . $mappingType . '_' . $mappingKey,
									'MAPPING[' . $mappingType . '][' . $mappingKey . '][ref_id]'
								);
								break;

							case 'PARAMS':
								drawDatalist(
									$arParams,
									$MAPPING[$mappingType][$mappingKey]['ref_id'],
									'rybMapping_' . $mappingType . '_' . $mappingKey,
									'MAPPING[' . $mappingType . '][' . $mappingKey . '][ref_id]',
									$MAPPING[$mappingType][$mappingKey]['automapped'] == 1,
									$MAPPING[$mappingType][$mappingKey]['ignored']
								);
								break;

							default: ?>
								<input type="text" name="MAPPING[<?= $mappingType ?>][<?= $mappingKey ?>]" value="<?= $mappingValue['ref_id'] ?>">
							<? break;
						endswitch; ?>
					</td>
				<? endif; ?>
			</tr>
	<? endforeach;
	endforeach;


	// завершение формы - вывод кнопок сохранения изменений
	$tabControl->Buttons([
		"back_url" => "ryb_partners_import_setup.php?lang=" . LANG,
	]);
	?>
	<input type="hidden" name="lang" value="<?= LANG ?>">
	<? if ($ID > 0 && !$bCopy) : ?>
		<input type="hidden" name="ID" value="<?= $ID ?>">
	<? endif; ?>
	<?
	// завершаем интерфейс закладок
	$tabControl->End();

	// дополнительное уведомление об ошибках - вывод иконки около поля, в котором возникла ошибка
	$tabControl->ShowWarnings("post_form", $message);

	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
