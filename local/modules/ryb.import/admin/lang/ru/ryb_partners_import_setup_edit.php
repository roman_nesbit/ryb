<?php
$MESS['RPISE_TAB1'] = 'Основные данные';
$MESS['RPISE_TAB1_TITLE'] = 'Основные данные конфигурации импорта товаров';
$MESS['RPISE_TAB2'] = 'Сопоставление категорий';
$MESS['RPISE_TAB2_TITLE'] = 'Настройка соответствий категорий магазина-партнера категориям маркетплейса';
$MESS['RPISE_TAB3'] = 'Сопоставление свойств';
$MESS['RPISE_TAB3_TITLE'] = 'Настройка соответствий свойств товаров агазина-партнера свойствам товаров маркетплейса';
$MESS['RPISE_EDIT'] = 'Конфигурация импорта товаров для ';
$MESS['RPISE_ADD'] = 'Добавить конфигурацию импорта';
$MESS['RPISE_ADD_ACTION'] = 'Добавить';
$MESS['RPISE_ADD_TITLE'] = 'Добавить конфигурацию импорта';
$MESS['RPISE_DELETE_ACTION'] = 'Удалить';
$MESS['RPISE_DELETE_TITLE'] = 'Удалить конфигурацию импорта';
$MESS['RPISE_DELETE_CONFIRM'] = 'Удалить конфигурацию?';
$MESS['RPISE_SAVE_ERROR'] = 'Ошибка сохранения';
$MESS['RPISE_LIST_ACTION'] = 'Вернуться к списку';
$MESS['RPISE_LIST_TITLE'] = 'Список конфигураций импорта';
$MESS['RPISE_SAVED'] = 'Сохранено';
$MESS['RPISE_FIELD_ID'] = 'ID';
$MESS['RPISE_FIELD_CREATED'] = 'Создан';
$MESS['RPISE_FIELD_MODIFIED'] = 'Изменен';
$MESS['RPISE_FIELD_ACTIVE'] = 'Активно';
$MESS['RPISE_FIELD_SORT'] = 'Индекс сортировки';
$MESS['RPISE_FIELD_PARTNER'] = 'Партнер';
$MESS['RPISE_FIELD_URL'] = 'Ссылка на файл с данными';
$MESS['RPISE_FIELD_PERIOD'] = 'Периодичность (час.)';
$MESS['RPISE_FIELD_DATE_LAST_EXEC'] = 'Последний запуск';
$MESS['RPISE_FIELD_DATE_NEXT_EXEC'] = 'Следующий запуск';
$MESS['RPISE_FIELD_LAST_EXEC_SUCCESS'] = 'Успешно';
$MESS['RPISE_FIELD_CURRENCIES'] = 'Валюты';
$MESS['RPISE_FIELD_CATEGORIES'] = 'Категории';
$MESS['RPISE_FIELD_PARAMS'] = 'Параметры товаров';
$MESS['RPISE_FIELD_VENDORS'] = 'Производители';
$MESS['RPISE_Y'] = 'Да';
$MESS['RPISE_N'] = 'Нет';