<?php
$MESS['RPIH_TITLE'] = 'История импорта';
$MESS['RPIH_DELETE_ACTION'] = 'Удалить';
$MESS['RPIH_DELETE_CONFIRM'] = 'Удалить запись?';
$MESS['RPIH_FIELD_DATE_EXEC'] = 'Время импорта';
$MESS['RPIH_FIELD_PARTNER'] = 'Партнер';
$MESS['RPIH_FIELD_LOG'] = 'Лог';
$MESS['RPIH_FIELD_EXEC_SUCCESS'] = 'Успешно';
$MESS['RPIH_FIELD_AGENT'] = 'Агент';
$MESS['RPIH_Y'] = 'Да';
$MESS['RPIH_N'] = 'Нет';
$MESS['RPIH_FIELD_CATEGORIES'] = 'Категории';
$MESS['RPIH_FIELD_PRODUCTS'] = 'Товары';
$MESS['RPIH_FIELD_PROPERTIES'] = 'Параметры';