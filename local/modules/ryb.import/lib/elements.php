<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("iblock");
use \Bitrix\Iblock\ElementTable;

class Elements {
	function getElementWhatByWhat($elementWhat, $what, $byWhat) {
		if(!$elementWhat) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "element_what_by_what_" . sha1(json_encode($elementWhat)) . sha1(json_encode($what)) . sha1(json_encode($byWhat));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elements")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementTable::getList(array(
				"select" => array($what),
				"filter" => array($byWhat => $elementWhat),
			));
			if($obj = $result->fetch()) $data = $obj[$what];
			else $data = false;

//			$obCache->endDataCache($data);
//		}
		
		return $data;
	}

	function getElementsWhatByFilter($what, $filter) {
//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "elements_what_by_filter_" . sha1(json_encode($what)) . sha1(json_encode($filter));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elements")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementTable::getList(array(
				"select" => (is_array($what) ? $what : array($what)),
				"filter" => $filter,
			));
			$data = $result->fetchAll();

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;		
	}

	function getElementsWhatByFilterByOrder($what, $filter, $order) {
//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "elements_what_by_filter_" . sha1(json_encode($what)) . sha1(json_encode($filter)) . sha1(json_encode($order));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elements")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementTable::getList(array(
				"select" => (is_array($what) ? $what : array($what)),
				"filter" => $filter,
				"order" => $order
			));
			$data = $result->fetchAll();

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;		
	}

	function getElementsWhatByFilterByOrderByPage($what, $filter, $order, $page) {
		$result = ElementTable::getList(array(
			"select" => (is_array($what) ? $what : array($what)),
			"filter" => $filter,
			"order" => $order,
			"limit" => $page[limit],
			"offset" => $page[limit] * ($page[number] - 1)
		));
		$data = $result->fetchAll();
		return $data;		
	}

	function getElementPropertyByWhat($elementWhat, $propertyCode, $byWhat) {
		if(!$elementWhat) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "element_property_by_what_" . sha1(json_encode($elementWhat)) . sha1(json_encode($propertyCode)) . sha1(json_encode($byWhat));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elements")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementTable::getList(array(
				"select" => array("*"), //array(""$propertyCode),
				"filter" => array($byWhat => $elementWhat),
			));
			if($obj = $result->fetch()) $data = $obj["PROPERTY_" . $propertyCode . "_VALUE"];
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}

	public function getElementNameById($elementId) {
		return Elements::getElementWhatByWhat($elementId, "NAME", "ID");
	}

	public function getElementCodeById($elementId) {
		return Elements::getElementWhatByWhat($elementId, "CODE", "ID");
	}

	function getRusUa($russianTerm) {
		if(!$russianTerm) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "rus_us_" . sha1($russianTerm);
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/rus_ua")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementTable::getList(array(
				"select" => array("PREVIEW_TEXT"),
				"filter" => array("IBLOCK_ID" => 23, "NAME" => $russianTerm),
			));
			if($obj = $result->fetch()) $data = $obj["PREVIEW_TEXT"];
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}
		
		return $data;
	}
}
?>