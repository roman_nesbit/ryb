<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("catalog");
use \Bitrix\Catalog\ProductTable;

class CatalogProducts {
	function getCatalogTableWhatByWhat($productWhat, $what, $byWhat) {
		if(!$productWhat) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "catalog_table_what_by_what_" . sha1(json_encode($what)) . sha1(json_encode($byWhat));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/catalogproducts")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ProductTable::getList(array(
				"select" => array($what),
				"filter" => array($byWhat => $productWhat),
			));
			if($obj = $result->fetch()) $data = $obj[$what];
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}
		
		return $data;
	}
}
?>