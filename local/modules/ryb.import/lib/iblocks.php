<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("iblock");
use \Bitrix\Iblock\IblockTable;

class Iblocks {
	function getIblockWhatByWhat($iblockWhat, $what, $byWhat) {
		if(!$iblockWhat) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "iblock_what_by_what_" . sha1(json_encode($iblockWhat)) . sha1(json_encode($what)) . sha1(json_encode($byWhat));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/iblocks")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = IblockTable::getList(array(
				"select" => array($what),
				"filter" => array($byWhat => $iblockWhat),
			));
			if($obj = $result->fetch()) $data = $obj[$what];
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}
}
?>