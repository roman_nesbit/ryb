<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("main");
use \Bitrix\Main\UserTable;

class Users {
	function getUsersWhatByFilter($what, $filter) {
		$result = UserTable::getList(array(
			"select" => (is_array($what) ? $what : array($what)),
			"filter" => $filter,
		));
		$data = $result->fetchAll();

		return $data;
	}
}
?>