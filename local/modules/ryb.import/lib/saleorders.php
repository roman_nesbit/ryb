<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("sale");
use \Bitrix\Sale\Order;
use \Bitrix\Sale\Internals\StatusTable;
use \Bitrix\Sale\Internals\StatusLangTable;
use \Bitrix\Sale\Internals\ShipmentTable;
use \Bitrix\Sale\Internals\PaymentTable;
use \Bitrix\Sale\Internals\BasketTable;

class SaleOrders {
	function getSaleOrders($userId) {
		if(!$userId) return false;

		$result = Order::getList(array(
			"select" => array("ID", "PAYED", "STATUS_ID", "ALLOW_DELIVERY", "PRICE", "CURRENCY", "PAY_SYSTEM_ID", "DELIVERY_ID", "DATE_INSERT", "USER_DESCRIPTION"),
			"filter" => array("USER_ID" => $userId),
			"order" => array("ID" => "DESC"),
		));

		$data = $result->fetchAll();

		return $data;
	}

	function getSaleOrderStatusData($statusId) {
//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "sale_order_status_" . sha1($statusId . LANGUAGE_ID);
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/sale/orderstatus")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			// color
			$result = StatusTable::getList(array(
				"select" => array("COLOR"),
				"filter" => array("ID" => $statusId),
			));
			if($obj = $result->fetch()) $data[COLOR] = $obj[COLOR];
			// name
			$result = StatusLangTable::getList(array(
				"select" => array("NAME"),
				"filter" => array("STATUS_ID" => $statusId, "LID" => LANGUAGE_ID),
			));
			if($obj = $result->fetch()) $data[NAME] = $obj[NAME];

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}

	function getSaleOrder($orderId) {
		if(!$orderId) return false;

		$result = Order::getList(array(
			"select" => array("ID", "PAYED", "STATUS_ID", "ALLOW_DELIVERY", "PRICE", "CURRENCY", "SUM_PAID", "PAY_SYSTEM_ID", "DEDUCTED", "DATE_DEDUCTED", "DATE_ALLOW_DELIVERY", "DATE_INSERT", "USER_DESCRIPTION", "STATUS_ID"),
			"filter" => array("ID" => $orderId),
		));

		$data = $result->fetch();

		return $data;
	}

	function setSaleOrderData($orderId, $data) {
		if(!$orderId) return false;

		if(Order::isLocked($orderId)) return false;

		$order = Order::load($orderId);
		$order->lock($orderId);
		foreach($data as $key=>$value) {
			$order->setField($key, $value);
		}
		$result = $order->save();
		$order->unlock($orderId);
		return $result->isSuccess();
	}

	function getSaleOrderDeliveryData($orderId) {
		if(!$orderId) return false;

		$result = ShipmentTable::getList(array(
			"select" => array("ID", "DATE_INSERT", "DELIVERY_ID", "DELIVERY_NAME", "ALLOW_DELIVERY", "DATE_ALLOW_DELIVERY", "DELIVERY_DOC_NUM", "DELIVERY_DOC_DATE", "TRACKING_STATUS", "DEDUCTED", "DATE_DEDUCTED", "STATUS_ID"),
			"filter" => array("ORDER_ID" => $orderId, "SYSTEM" => "N"),
		));
		$data = $result->fetchAll();

		return $data;
	}

	function setSaleOrderDeliveryData($deliveryId, $data) {
		if(!$deliveryId) return false;

		$result = ShipmentTable::update($deliveryId, $data);
		return $result->isSuccess();
	}

	function getSaleOrderPaymentData($orderId) {
		if(!$orderId) return false;

		$result = PaymentTable::getList(array(
			"select" => array("ID", "DATE_BILL", "SUM", "CURRENCY", "PAID", "DATE_PAID", "PAY_SYSTEM_ID", "PAY_SYSTEM_NAME"),
			"filter" => array("ORDER_ID" => $orderId),
		));
		$data = $result->fetchAll();

		return $data;
	}

	function getSaleOrderBasketData($orderId) {
		if(!$orderId) return false;

		$result = BasketTable::getList(array(
			"select" => array("ID", "PRODUCT_ID", "PRICE", "DISCOUNT_PRICE", "BASE_PRICE", "CURRENCY", "QUANTITY", "NAME", "DETAIL_PAGE_URL"),
			"filter" => array("ORDER_ID" => $orderId, "DELAY" => "N", "CAN_BUY" => "Y"),
			"order" => array("SORT" => "ASC"),
		));
		$data = $result->fetchAll();

		return $data;
	}

	function getSaleOrderNewPostData($deliveryDocNum) {
		if(!$deliveryDocNum) return false;

		// Инфо по ЕН
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => NP_API_URL,
			CURLOPT_RETURNTRANSFER => True,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{
				"apiKey": "' . NP_API_KEY . '",
				"modelName": "TrackingDocument",
				"calledMethod": "getStatusDocuments",
				"methodProperties": {
					"Documents": [
						{
							"DocumentNumber": "' . $deliveryDocNum . '",
							"Phone": "' . NP_API_SENDER_PHONE . '"
						}
					]
				}
			}',
			CURLOPT_HTTPHEADER => array("content-type: application/json",),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			return false; // "cURL Error #:" . $err;
		} else {
			$arResponse = json_decode($response, true);
			if($arResponse[success]) {
				return $arResponse[data][0];
			} else {
				return false;//$arResponse[errors]);
			}
		}
	}
}
?>