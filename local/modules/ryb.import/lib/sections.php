<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("iblock");
use \Bitrix\Iblock\SectionTable;

class Sections {
	function getSectionWhatByWhat($sectionId, $what, $byWhat) {
		if(!$sectionId) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "section_what_by_what_" . sha1($sectionId) . sha1(json_encode($what)) . sha1(json_encode($byWhat));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/sections")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = SectionTable::getList(array(
				"select" => (is_array($what) ? $what : array($what)),
				"filter" => array($byWhat => $sectionId),
			));
			if($obj = $result->fetch()) $data = (is_array($what) ? $obj : $obj[$what]);
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}
		
		return $data;
	}

	function getChildrenSections($sectionId) {
		if(!$sectionId) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "children_sections_" . sha1($sectionId);
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/children_sections")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = SectionTable::getList(array(
				"select" => array("ID"),
				"filter" => array("IBLOCK_SECTION_ID" => $sectionId),
			));
			$data = array();
			while($obj = $result->fetch()) {
				$data[] = $obj["ID"];
				$arChildren = Sections::getChildrenSections($obj["ID"]);
				if(count($arChildren)) $data = array_merge($data, $arChildren);
			}

//			$obCache->endDataCache($data);
//		}
		
		return $data;
	}

	function getSectionWhatById($sectionId, $what) {
		if(!$sectionId) return false;
		return Sections::getSectionWhatByWhat($sectionId, $what, "ID");
	}

	public function getSectionNameById($sectionId) {
		return Sections::getSectionWhatById($sectionId, "NAME");
	}
}
?>