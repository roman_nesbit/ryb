<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("iblock");
use \Bitrix\Iblock\ElementPropertyTable;

class ElementProperties {
	function getElementPropertyWhatByFilter($what, $byFilter) {
//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "element_property_what_by_filter_" . sha1(json_encode($what)) . sha1(json_encode($byFilter));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elementproperties")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementPropertyTable::getList(array(
				"select" => (is_array($what) ? $what : array($what)),
				"filter" => $byFilter,
			));
			if($obj = $result->fetch()) $data = (is_array($what) ? $obj : $obj[$what]);
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}

	function getAllElementPropertyByFilter($what, $byFilter) {
//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "all_elements_property_by_filter_" . sha1(json_encode($what)) . sha1(json_encode($byFilter));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/elementproperties")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = ElementPropertyTable::getList(array(
				"select" => (is_array($what) ? $what : array($what)),
				"filter" => $byFilter,
			));
			if($obj = $result->fetchAll()) $data = $obj;
			else $data = false;

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}
}
?>