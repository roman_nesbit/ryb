<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

class Funcs {
	public function exo($arVar, $show = false) {
		echo "<pre class='" . ($show ? '' : 'hide') . "'>"; print_r($arVar); echo "</pre>";
	}

	public function exor($paramName) {
		Funcs::exo($_REQUEST);
	}
}
?>