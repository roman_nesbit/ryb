<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

namespace Bfs\Tools;

\Bitrix\Main\Loader::includeModule("iblock");
use \Bitrix\Iblock\SectionElementTable;

class SectionElements {
	function getSectionElements($sectionId) {
		if(!$sectionId) return false;

//		$obCache = \Bitrix\Main\Data\Cache::createInstance();
//		$cache_id = "section_elements_" . sha1(json_encode($sectionId));
//		if($obCache->initCache(CACHE_DEFAULT_TIME, $cache_id, "bfs/sectionelements")) {
//			$data = $obCache->GetVars();
//		} elseif($obCache->startDataCache()) {
			$result = SectionElementTable::getList(array(
				"select" => array("IBLOCK_ELEMENT_ID"),
				"filter" => array("IBLOCK_SECTION_ID" => $sectionId),
			));
			$data = array();
			while($row = $result->fetch()) {
				$data[] = $row["IBLOCK_ELEMENT_ID"];
			}

//			if($data) $obCache->endDataCache($data);
//			else $obCache->abortDataCache();
//		}

		return $data;
	}
}
?>