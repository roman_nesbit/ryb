create table if not exists ryb_partners_import
(
	ID int(11) not null auto_increment,
	TIMESTAMP_X timestamp not null default current_timestamp on update current_timestamp,
	MODIFIED_BY int(18) references b_user(ID),
	DATE_CREATE datetime,
	CREATED_BY int(18) references b_user(ID),
	ACTIVE char(1) not null default 'Y',
	SORT int(11) not null default 500,
	PARTNER_ID int(11) not null references b_iblock_element(ID),
	URL char(255) not null,
	PERIOD int(2),
	LAST_EXEC_SUCCESS char(1),
	AGENT_ID int(11) references b_agent(ID),
	PRIMARY KEY (ID)
);

create table if not exists ryb_partners_import_history
(
	ID int(11) not null auto_increment,
	DATE_EXEC datetime,
	EXEC_SUCCESS char(1),
	PARTNER_ID int(11) not null references b_iblock_element(ID),
	AGENT_ID int(11) references b_agent(ID),
	CATEGORIES_NEW int,
	CATEGORIES_TOTAL int,
	PRODUCTS_NEW int,
	PRODUCTS_DEACTIVATED int,
	PRODUCTS_TOTAL int,
	PROPERTIES_NEW int,
	PROPERTIES_TOTAL int,
	LOG char(255),
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS marketplace_mapping_external_categories
(
    ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PARTNER_ID INT UNSIGNED NOT NULL,

    EXTERNAL_CATEGORY_ID INT UNSIGNED NOT NULL,
    EXTERNAL_CATEGORY_NAME VARCHAR(100) NOT NULL,

    EXTERNAL_CATEGORY_PARENT_ID INT UNSIGNED DEFAULT NULL,

    INTERNAL_CATEGORY_ID INT UNSIGNED DEFAULT NULL REFERENCES b_iblock_section(ID),
    UPDATED_BY INT UNSIGNED REFERENCES b_user(ID),

    CREATED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UPDATED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(ID),
    UNIQUE KEY(PARTNER_ID,EXTERNAL_CATEGORY_ID)
)

COMMENT 'A table to store associations between external categories provided by partners and categories of the marketplace'
CHARACTER SET utf8
COLLATE utf8_general_ci;


CREATE TABLE IF NOT EXISTS marketplace_mapping_external_parameters
(
    ID INT UNSIGNED NOT NULL AUTO_INCREMENT,

    PARTNER_ID INT UNSIGNED COMMENT 'If set to NULL, the mapping perceived as global, for all partners',

    EXTERNAL_PARAMETER_NAME VARCHAR(50) NOT NULL COMMENT 'The name of the parameter recorded as provided by the external party',
    EXTERNAL_PARAMETER_CODE VARCHAR(50) NOT NULL COMMENT 'Normalized version of the parameter name',
    EXTERNAL_CATEGORY_ID INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Specifies to which exact category belongs the parameter',

    INTERNAL_PARAMETER_CODE VARCHAR(50) DEFAULT NULL,

    IS_AUTOMATICALLY_MAPPED BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Indicates if the mapping to internal parameter was automatic',
    IS_IGNORED BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'If set to true, the parameter will not be imported by the system',

    UPDATED_BY INT UNSIGNED DEFAULT NULL COMMENT 'If value equals to NULL, the mapping was automatically performed by the system' REFERENCES b_user(ID),

    CREATED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UPDATED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(ID),
    UNIQUE KEY(PARTNER_ID, EXTERNAL_PARAMETER_CODE, EXTERNAL_CATEGORY_ID)
)

COMMENT 'A table to store mappings between goods parameters provided by external parties and those used by the marketplace'
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- Globally ignored parameters

INSERT INTO `marketplace_mapping_external_parameters` (`EXTERNAL_PARAMETER_NAME`, `EXTERNAL_PARAMETER_CODE`, `IS_IGNORED`) VALUES
('Дополнительный текст', 'дополнительныйтекст', 1);

-- END: Globally ignored parameters

-- Globally mapped parameters

INSERT INTO `marketplace_mapping_external_parameters` (`EXTERNAL_PARAMETER_NAME`, `EXTERNAL_PARAMETER_CODE`, `INTERNAL_PARAMETER_CODE`) VALUES
('Производитель', 'производитель', 'VENDOR'),
('URL', 'url', 'IMPORT_OFFER_URL'),
('Артикул', 'артикул', 'IMPORT_OFFER_SKU'),
('Вес', 'вес', 'WEIGHT_G'),
('Длина', 'длина', 'LENGTH_CM');

CREATE TABLE IF NOT EXISTS `marketplace_locks` (
 `key_id` varchar(64) COLLATE utf8_bin NOT NULL,
 `key_token` varchar(44) COLLATE utf8_bin NOT NULL,
 `key_expiration` int(10) unsigned NOT NULL,
 PRIMARY KEY (`key_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_bin;