<?
/**
 * Created by BFS
 * Author: Bogdan Fenyuk
 * www.bogdanfenyuk.com
*/

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use Rybalka\Marketplace\Partners\ImportTable;

Loc::loadMessages(__FILE__);

Class ryb_import extends CModule {
	var $exclusionAdminFiles;

	function __construct() {
		$arModuleVersion = array();
		include(__DIR__."/version.php");

		$this->exclusionAdminFiles = array(
			"..",
			".",
			"menu.php",
			"operation_desription.php",
			"task_description.php"
		);

		$this->MODULE_ID = "ryb.import";
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("RYB_IMPORT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("RYB_IMPORT_MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage("RYB_IMPORT_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("RYB_IMPORT_PARTNER_URI");

		$this->MODULE_SORT = 1;
		$this->SHOW_SUPER_ADMIN_GROUP_RIGHT = "Y";
		$this->MODULE_GROUP_RIGHTS = "Y";
	}

	public function GetPath($notDocumentRoot = false) {
		if($notDocumentRoot)
			return str_ireplace(Application::getDocumentRoot(), "", dirname(__DIR__));
		else
			return dirname(__DIR__);
	}

	public function isVersionD7() {
		return CheckVersion(\Bitrix\Main\ModuleManager::getVersion("main"), "14.00.00");
	}

	public function DoInstall() {
		global $APPLICATION;
		$this->InstallDB();
		$this->checkAgents();
		$this->InstallFiles();
		RegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile(Loc::getMessage("RYB_IMPORT_INSTALL_TITLE"), $this->GetPath()."/install/step.php");
		return true;
	}

	public function DoUninstall() {
		global $APPLICATION;

		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();

		if($request["step"] < 2) {
			$APPLICATION->IncludeAdminFile(Loc::getMessage("RYB_IMPORT_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep1.php");
		} elseif($request["step"] == 2) {
			if(!array_key_exists('savedata', $_REQUEST) || $_REQUEST['savedata'] != 'Y') {
				$this->UnInstallDB();
			}
			$GLOBALS["CACHE_MANAGER"]->CleanAll();

			$this->UnInstallFiles();

			\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

			$APPLICATION->IncludeAdminFile(Loc::getMessage("RYB_IMPORT_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep2.php");
		}
	}

	public function InstallDB()
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/" . $DBType . "/install.sql");
		if (!$this->errors) {
			return true;
		} else {
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
	}

	public function UnInstallDB()
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/" . $DBType . "/uninstall.sql");
		if (!$this->errors) {
			return true;
		} else {
			$APPLICATION->ThrowException(implode("", $this->errors));
            return false;
		}
	}

	public function InstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles(
				$_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/admin",
				$_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin"
			);
		}
		return true;
	}

	public function UnInstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			DeleteDirFiles(
				$_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/admin",
				$_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin"
			);
		}
		return true;
	}

	private function checkAgents()
	{
		ImportTable::checkAgents();
	}
}
?>