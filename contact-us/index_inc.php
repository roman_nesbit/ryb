<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"contact-us",
	array(
		"AJAX_MODE" => "Y", 
		"AJAX_OPTION_SHADOW" => "N", 
		"AJAX_OPTION_JUMP" => "N", 
		"AJAX_OPTION_STYLE" => "N", 
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_FOLDER" => "/contact-us/",
		"SEF_MODE" => "Y",
		"SUCCESS_URL" => "thankyou.php",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "FEEDBACK_FORM"
	)
);
?>