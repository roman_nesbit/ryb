<!DOCTYPE html>
<html xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<? use Rybalka\Marketplace\Util\LoggerFactory;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$logger = (new LoggerFactory())->getLogger('informer');

try
{
	if (!CModule::IncludeModule("iblock"))
	{
        $logger->error('Failed to render banner', [
            'errorMessage' => $e->getMessage()
        ]);
		throw new Exception('No iblock module');
	}

	$res = CIBlockElement::GetList(
		[
			'RAND' => 'ASC'
		],
		[
			'IBLOCK_TYPE' => 'adv',
			'IBLOCK_CODE' => 'informer-for-rybalka-com',
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
		],
		false,
		[
			'nTopCount' => 1
		],
		[
			'NAME',
			'PREVIEW_TEXT',
			'DETAIL_TEXT',
			'DETAIL_PICTURE',
			'PROPERTY_LINK',
		]
	);

	if($item = $res->GetNext())
	{
	    $text = strip_tags(trim(
	        !empty($item['~DETAIL_TEXT'])
                ? $item['~DETAIL_TEXT']
                : $item['~PREVIEW_TEXT']
        ));
		?><a href="<?=$item['PROPERTY_LINK_VALUE']?>?utm_source=rybalka.com&utm_campaign=blogsandalbums&utm_medium=internal" title="<?=$text?>" target="_blank"><img width="667px" height="160px" src="<?=CFile::GetPath($item['DETAIL_PICTURE'])?>" alt="<?=$text?>" /></a><?
	}
} catch(Exception $e) {

    $logger->error('Failed to render banner', [
        'errorMessage' => $e->getMessage()
    ]);
}
?>
</body>
</html>