<b>Осуществляете ли Вы доставку до квартиры и подъем на этаж?</b><br>
 <br>
 Пожалуйста, обратите внимание, что адресная доставка осуществляется, согласно расценок компании “Новая Почта”, до входа в здание. Доставка товаров из разделов “Лодки и моторы”, “Авто и мото”, согласовываются персонально с менеджером. Стоимость ручного заноса в квартиру зависит от: габаритов техники, сложности подъема и наличия лифта (куда товар помещается в упаковке).<br>
 <br>
 <b>Есть ли возможность заказать товар с доставкой в другую страну?</b><br>
 <br>
 Рыбалка это состояние души и если вы увидели товары которые недоступны в вашей стране, мы сделаем все возможное, чтобы их доставить вам, так как мы сами рыболовы. Делайте заказ, и описывайте ваши желания в комментариях к заказу. Мы что-то придумаем.<br>
 <br>
 <b>Сколько дней товар находится в пункте выдачи?</b><br>
 <br>
 В отделении курьерских служб «Нова Пошта» и «Укрпошта» заказ будет находиться в течение 3х рабочих дней. По истечению данного срока, заказ будет помещен на хранение с дополнительной оплатой согласно тарифов почтового оператора, либо автоматически возвращается отправителю. В любом критическом случае менеджер перезвонит вам. Мы договоримся и решим проблему с задержкой получения товара.<br>
 &nbsp; &nbsp;&nbsp;<br>
 <b>Требуется ли дополнительная оплата за перевод средств при наложенном платеже в отделении Новой Почты?</b><br>
 <br>
 Дополнительная оплата за пересылку средств при наложенном платеже с вашей стороны составит 2% от суммы товара + 20 грн. Рекомендуем вам делать предоплату после заказа, так вы сэкономите деньги. Например, товар стоимостью 1000 грн, при наложенном платеже обойдется вам в 40 грн. дополнительно. В данный момент мы ведем переговоры и работаем над тем, чтобы дополнительные платежи при наложенном платеже, были исключены при доставкой Новой Почты. Этот пункт может измениться в любую минуту. Об этом будет сообщено в наших СМИ и соц. сетях.&nbsp;<br>
 <br>
 В любом случае стоимость заказа, согласованная с менеджером при подтверждении заказа, является окончательной. Если мы нарушим это правило, то это будет за наш счет.<br>
 &nbsp; &nbsp;&nbsp;<br>
 <b>Есть ли возможность отказаться от части заказа в пункте выдачи?</b><br>
 <br>
 К сожалению, возможности отказаться от части заказа нет. Вы можете отказаться от неактуального заказа полностью и оформить новую заявку.<br>
 <br>
 <b>Что делать, если товар был поврежден при перевозке?</b><br>
 <br>
 В случае повреждения товара, или неполной комплектации заказа — необходимо отказаться от получения товара и его оплаты, а также, составить акт (претензию). В случае, если данная ситуация будет иметь место – просим Вас сообщите нам о случившемся по тел.: +380671141463 или +380958368667&nbsp; &nbsp;&nbsp;<br>
 <br>
 <b>Возможно ли оплатить заказ картой при получении?</b><br>
 &nbsp; &nbsp; <br>
 В данный момент, курьерскую адресную доставку осуществляет Новая Почта. У курьеров Новой Почты должен присутствовать POS терминал. В любом случае перед доставкой курьер с вами свяжется и вы можете уточнить у него наличие POS терминала.<br>
 &nbsp; &nbsp; <br>
 <b>Возможно ли купить товар в рассрочку?</b><br>
 &nbsp; &nbsp; <br>
 К сожалению, в данный момент возможности приобретать товары в рассрочку нет. Но мы ведем переговоры с банками об этой услуге. Этот пункт может измениться в любую минуту. Об этом будет сообщено в наших СМИ и соц. сетях.<br>
 &nbsp; &nbsp; <br>
 Вы можете оформить заказ на интересующий Вас товар, доступный у нас на сайте, в кредит. Также, если Вы являетесь клиентом ПриватБанка, мы можем предложить Вам рассмотреть вариант оформления заказа с помощью услуг «Мгновенная рассрочка» и «Оплата частями».<br>
 &nbsp; &nbsp; <br>
 <b>Какова процедура оформления "Мгновенной рассрочки" от ПриватБанка?</b><br>
 <br>
 Для оформления "Мгновенной рассрочки", Вам необходимо добавить заинтересовавшие Вас товары в "Корзину" и выбрать способ оплаты покупки «В кредит». Далее необходимо выбрать желаемый вид кредита — "Мгновенная рассрочка".<br>
 <br>
 Пожалуйста, обратите внимание, что данная услуга предоставляется: для определённых товаров, возле которых присутствует специальный знак "Мгновенная рассрочка".<br>
 <br>
 Данная слуга доступна только владельцам карт ПриватБанка.&nbsp;<br>
 Оформление "Мгновенной рассрочки" от ПриватБанка возможно на срок от 1 до 24 месяцев.<br>
 Каждые 30 дней с момента оплаты товара, банк будет автоматически списывать с Вашей карты сумму платежа равными частями.<br>
 Обязательным условием для оформления заказа является наличие на карте средств в сумме, эквивалентной сумме первого платежа.<br>
 Сумма комиссии по сервису «Мгновенная рассрочка» — 2,9 % ежемесячно. Обращаем Ваше внимание на то, что в случае использования кредитной карты ПриватБанка, с Вашей карты может быть взыскан дополнительный процент за использование кредитных средств.<br>
 Для получения более подробной информации относительно условий погашения кредита, Вы можете обратиться в службу поддержки ПриватБанка.<br>
 <br>
 <b>Какова процедура оформления заказа с помощью «Оплаты частями» от ПриватБанка?</b><br>
 <br>
 Для оформления заказа с помощью услуги "Оплата Частями" от ПриватБанка, Вам необходимо добавить заинтересовавшие Вас товары в "Корзину" и выбрать способ оплаты покупки «Оплата частями».<br>
 <br>
 Пожалуйста, обратите внимание, что данная услуга предоставляется:<br>
 - для определённых товаров, возле которых присутствует специальный знак "Оплата частями"<br>
 - доступна только для владельцев карт ПриватБанка.<br>
 Оформление услуги "Оплата Частями" от ПриватБанка возможно на срок от 1 до 9 месяцев.<br>
 Каждые 30 дней с момента оплаты товара, банк будет автоматически списывать с Вашей карты сумму платежа равными частями.<br>
 Обязательным условием для оформления заказа является наличие на карте средств в сумме, эквивалентной сумме первого платежа.<br>
 Обращаем Ваше внимание на то, что в случае списания платежа с кредитных средств карты, дополнительно удерживается 4% комиссии за платеж.<br>
 Стоимость приобретаемого товара не может быть ниже 300 грн. Для того, чтобы узнать максимальную сумму покупки по сервису — отправьте SMS на номер 10060 с текстом chast.<br>
 Для получения более подробной информации относительно условий предоставления услуги, Вы можете обратиться в службу поддержки ПриватБанка.<br>
 После заполнения формы на нашем сайте, с Вами свяжется наш менеджер для согласования условий доставки Вашего заказа.<br>
 &nbsp; &nbsp;&nbsp;<br>
 <b>Есть ли возможность оплаты заказа платежными картами Visa и MasterCard?</b><br>
 &nbsp; &nbsp;&nbsp;<br>
 Да, у Вас есть возможность оплатить заказ картами Visa и MasterCard.<br>
 &nbsp; &nbsp;&nbsp;<br>
 Обращаем Ваше внимание, что Вы можете оплатить заказ онлайн картой Visa и MasterCard.<br>
 При оформлении заказа у нас на сайте, в графе "Оплата" Вам нужно выбрать «Visa/MasterCard»<br>
 После этого Вы будете переадресованы на страницу системы безопасных платежей, где Вам необходимо подтвердить оплату.<br>
 Пожалуйста, обратите внимание, получить товар, оплаченный платежной картой, на кого оформлен заказ, поэтому при получении заказа обязательно нужно иметь при себе удостоверение личности.<br>
 <br>
 <b>Возможно ли оплатить заказ кредитной картой из-за рубежа?</b><br>
 &nbsp; &nbsp;&nbsp;<br>
 Вы можете оплатить заказ онлайн любой картой Visa и MasterCard.<br>
 &nbsp; &nbsp;&nbsp;<br>
 Однако, просим обратить Ваше внимание на возможные ограничения при переводе средств в другую страну со стороны Вашего банка. Учитывая это, мы, к сожалению, не даем гарантию, что оплата будет успешной.<br>
 <br>
 <b>Куда и на каких условиях осуществляется доставка?</b><br>
 &nbsp; &nbsp;&nbsp;<br>
 Условия доставки зависят от региона. Мы доставляем товары по всей Украине, кроме АР Крым и некоторых регионов Донецкой и Луганской областей.<br>
 <br>
 Вы также можете сами забрать заказ в отделениях Новой Почты.<br>
 <br>
 <b>Как я могу оплатить свой заказ?</b><br>
 <br>
 Сейчас доступны такие способы оплаты:<br>
 &nbsp; &nbsp;&nbsp;<br>
 - наличная оплата в отделении Новой Почты или Укрпочты;<br>
 - безналичная оплата<br>
 - оплата картами Visa и MasterCard<br>
 <br>
 <b>Как осуществляется доставка по Украине?</b><br>
 <br>
 Доставка по Украине производится с помощью курьерской службы "Новая Почта"<br>
 <br>
 <b>Как оплатить товар по безналичному расчету? Являетесь ли вы плательщиком НДС?</b><br>
 <br>
 Вы можете оплатить заказ банковским переводом либо с помощью платежных карт Visa и MasterCard любого банка на ФОП. На данный момент мы не не являемся плательщиком НДС.<br>
 <br>
 <b>Что нужно для получения товара, оплаченного по безналичному расчету и доставленного Новой Почтой?</b><br>
 <br>
 Для частных лиц:<br>
 <br>
 паспорт.<br>
 <br>
 Для юридических лиц и СПД:<br>
 <br>
 доверенность, выписанная на предъявителя<br>
копия свидетельства плательщика НДС (если есть).<br>
без оформления доверенности товар может получить директор предприятия лично, с заверением расходных накладных круглой печатью предприятия&nbsp;<br>
 <br>
 <b>Возможна ли оплата заказа банковской картой?</b><br>
 <br>
 Вы можете оплатить заказ онлайн любой картой Visa и MasterCard любого банка без комиссии.<br>
 <br>
 Оплата с помощью платежных карт осуществляется следующим способом:<br>
 <br>
 во время оформления заказа на сайте, Вам будет предложено сделать выбор способа оплаты. В графе "Оплата" вам нужно выбрать «Visa/MasterCard». После этого Вы будете переадресованы на страницу системы безопасных платежей ПриватБанка, где Вам необходимо будет подтвердить оплату либо с вами свяжется менеджер для уточнения вопроса оплаты.<br>
 <br>
 Пожалуйста, обратите внимание, получить товар, оплаченный платежной картой, может только тот клиент, на ФИО которого оформлен заказ, поэтому при получении заказа обязательно нужно иметь при себе паспорт.<br>
 <br>
 <b>Возможно ли при получении примерить одежду/обувь?</b><br>
 &nbsp;<br>
 Да, для этого случае адресной доставки или доставки в отделение курьерской службы Новая Почта, у Вас будет возможность примерить только верхнюю одежду или обувь.<br>
 <br>
 <b>Какова стоимость адресной доставки по Украине?</b><br>
 <br>
 Стоимость доставки заказов по вашему адресу осуществляется курьерской службой Новой Почты. Стоимость доставки уточняйте у менеджера при оформлении.<br>
 <br>
 <b>На какие товары предоставляется гарантия?</b><br>
 <br>
 На товары в нашем магазине предоставляется гарантия, подтверждающая обязательства по отсутствию в товаре заводских дефектов. Гарантия предоставляется на срок от 2-х недель до 36 месяцев в зависимости от сервисной политики производителя. Срок гарантии указан в описании каждого товара на нашем сайте. Подтверждением гарантийных обязательств служит гарантийный талон производителя, или гарантийный талон Rybalka.ua.<br>
 <br>
 Пожалуйста, проверьте комплектность и отсутствие дефектов в товаре при его получении (комплектность определяется описанием изделия или руководством по его эксплуатации).<br>
 <br>
 <b>Куда обращаться за гарантийным обслуживанием?</b><br>
 <br>
 При возникновении любого гарантийного случая просим Вас сообщите нам о случившемся по тел.: +380671141463 или +380958368667 . Менеджер быстро решит, как быть в данной ситуации.<br>
 <br>
 Гарантийным обслуживанием занимаются сервисные центры, авторизованные производителями.<br>
 <br>
 Адреса и телефоны сервисных центров вы можете найти на гарантийном талоне или по адресу — полный список сервисных центров.<br>
 <br>
 Право на бесплатное гарантийное обслуживание дает гарантийный талон, в котором указываются: модель; серийный номер; гарантийный срок; дата продажи товара.<br>
 <br>
 Пожалуйста, сохраняйте его в течение всего срока эксплуатации.<br>
 <br>
 Срок ремонта определяется авторизованным СЦ, в случае возникновения проблем с сервис-партнером, вы можете обратиться в точку продажи.<br>
 <br>
 <b>Я могу обменять или вернуть товар?</b><br>
 <br>
 Да, вы можете обменять или вернуть товар в течение 14 дней после покупки. Это право гарантирует вам «Закон о защите прав потребителя».<br>
 <br>
 Чтобы использовать эту возможность, пожалуйста убедитесь что:<br>
 <br>
 товар, не был в употреблении и не имеет следов использования: царапин, сколов, потёртостей, например, на счётчике телефона не более 5 минут разговоров, программное обеспечение не подвергалось изменениям и т. п.;<br>
 товар полностью укомплектован и не нарушена целостность упаковки;<br>
 сохранены все ярлыки и заводская маркировка.<br>
 <br>
 Если товар не работает, обмен или возврат товара производится только при наличии заключения сервисного центра, авторизованного производителем, о том, что условия эксплуатации не нарушены.<br>
 <br>
 Оформить заявку на возврат можно сообщив нам о проблеме по тел.: +380671141463 или +380958368667&nbsp; &nbsp;&nbsp;<br>
 <br>
 <b>Сервисный центр не может отремонтировать мой товар в гарантийный период.</b><br>
 <br>
 Если в гарантийный период товар, который вы у нас купили, вышел из строя по вине производителя и не может быть отремонтирован в авторизованном сервисном центре, мы обменяем товар на аналогичный или вернём деньги.<br>
 <br>
 Для этого, пожалуйста, предоставьте нам:<br>
 <br>
 - товар с полной комплектацией;&nbsp;<br>
 - гарантийный талон;<br>
 - документ подтверждающий оплату;<br>
 - заключение сервисного центра с отметкой о том, что товар имеет «существенный недостаток».<br>
 <br>
 <b>В каких случаях гарантия не предоставляется?</b><br>
 <br>
 Rybalka.ua может отказать в гарантийном ремонте если:<br>
 <br>
 нарушена сохранность гарантийных пломб;<br>
 есть механические или иные повреждения, которые возникли вследствие умышленных или неосторожных действий покупателя или третьих лиц;<br>
 нарушены правила использования, изложенные в эксплуатационных документах;<br>
 было произведено несанкционированное вскрытие, ремонт или изменены внутренние коммуникации и компоненты товара, изменена конструкция или схемы товара;<br>
 неправильно заполнен гарантийный талон;<br>
 серийный или IMEI номер, находящийся в памяти изделия, изменён, стёрт или не может быть установлен.<br>
 <br>
 Гарантийные обязательства не распространяются на следующие неисправности:<br>
 <br>
 естественный износ или исчерпание ресурса;<br>
 случайные повреждения, причиненные клиентом или повреждения, возникшие вследствие небрежного отношения или использования (воздействие жидкости, запыленности, попадание внутрь корпуса посторонних предметов и т. п.);<br>
 повреждения в результате стихийных бедствий (природных явлений);<br>
 повреждения, вызванные аварийным повышением или понижением напряжения в электросети или неправильным подключением к электросети;<br>
 повреждения, вызванные дефектами системы, в которой использовался данный товар, или возникшие в результате соединения и подключения товара к другим изделиям;<br>
 повреждения, вызванные использованием товара не по назначению или с нарушением правил эксплуатации.<br>
 <br>
 Согласно статьи 9 Закона Украины «О защите прав потребителей». Кабинетом Министров Украины утвержден перечень товаров надлежащего качества, которые не подлежат обмену или возврату.<br>
 <br>
 К таким товарам относятся:<br>
 <br>
 перопуховые изделия<br>
 детские игрушкимягкие<br>
 детские игрушки резиновые надувные<br>
 перчатки<br>
 тюлегардинные и кружевные полотна<br>
 белье нательное<br>
 белье постельное<br>
 чулочно-носочныеизделия<br>
 печатные издания<br>
 диски для лазерных систем считывания с записью<br>
 товары для новорожденных (пеленки, соски, бутылочки для кормления т.д.)<br>
 товары для личной гигиены (например: эпиляторы, электробритвы, машинки для стрижки волос)<br>
 продовольственные товары (детское питание и т.д.)<br>
 <br>
 <b>Распространяется ли гарантия на уцененные или акционные товары?</b><br>
 <br>
 Да, гарантия распространяется и на товары из категории «уценка» и «скидка».<br>
 <br>
 Мы обязательно фиксируем внешнее состояние товара на момент его продажи и сообщаем об этом покупателю.<br>
 <br>
 <br>