<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "интернет, магазин, рыбалка, rybalka, катушка, удилище, приманка, одежда, обувь, экипировка, эхолот, навигатор, мотор, лодка, шпули");
$APPLICATION->SetPageProperty("description", "Интернет магазин Rybalka. Снасти и экипировка для рыбалки и охоты. Удилища, катушки, приманки, эхолоты. Доставка по всей Украине.");
$APPLICATION->SetTitle("Интернет-супермаркет Rybalka: снасти, катушки, удилища, приманки, аксессуары для рыбалки ");

use Bitrix\Main\Type\DateTime;
?>

<section class="homeTop">
	<div class="categoriesNavBox">
		<? $APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"nav",
			array(
				"ADD_SECTIONS_CHAIN" => "Y",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT_ELEMENTS" => "Y",
				"FILTER_NAME" => "sectionsFilter",
				"IBLOCK_ID" => "4",
				"IBLOCK_TYPE" => "catalog",
				"SECTION_CODE" => "",
				"SECTION_FIELDS" => array("", ""),
				"SECTION_ID" => "",
				"SECTION_URL" => "",
				"SECTION_USER_FIELDS" => array("UF_ICON_ID"),
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "1",
				"VIEW_MODE" => "LINE",
				"SHOW_MORE" => "Y",
			)
		); ?>
	</div>
	<div class="mainBannersWrap">
		<div class="mainBannersBox">
			<? $APPLICATION->IncludeComponent(
				"bfs:carousel.block",
				".default",
				array(
					"COMPONENT_TEMPLATE" => ".default",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"SECTION_ID" => "198"
				),
				false
			); ?>
		</div>
		<div class="bottomBox"><? $APPLICATION->IncludeComponent(
	"bfs:single.banner.block", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PICTURE" => "/upload/medialibrary/fe5/fe50618f0d07ecc27b29c20768c61308.png",
		"PICTURE_ALT" => "Наручный беспроводный эхолот Lucky® Fish Finder Rambo NEW 20'",
		"LINK" => "https://rybalka.ua/p/naruchnyy-besprovodnoy-ekholot-lucky-fish-finder-rambo-new-20/"
	),
	false
); ?></div>
	</div>
	<div class="sideBanners">
		<div>
			<? $APPLICATION->IncludeComponent(
	"bfs:single.banner.block", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PICTURE" => "/upload/medialibrary/043/043b61e5745eea5aa2b79b8b6be60b11.png",
		"LINK" => "https://rybalka.ua/p/silikonovyy-vibrokhvost-dlya-mikrodzhiga-fox-5sm-slink-108-s%D1%8Aedobnyy-20sht-dzfjlb/",
		"PICTURE_ALT" => "Виброхвост Fox Slink со скидкой"
	),
	false
); ?>
		</div>
		<div>
			<? $APPLICATION->IncludeComponent(
	"bfs:single.banner.block", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PICTURE" => "/upload/medialibrary/d89/d89d6aba8d96c2527863e01915acbbc4.png",
		"LINK" => "/p/vobler-fox-bent-minnow-mi047-11-5sm-11-8gr-col-1-aorjjc/",
		"PICTURE_ALT" => "Воблер Fox Bent Minnow"
	),
	false
); ?>
		</div>
		<div>
			<? $APPLICATION->IncludeComponent(
	"bfs:single.banner.block", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PICTURE" => "/upload/medialibrary/587/5877c2f7b24a9221e26ae04b4cca2447.png",
		"LINK" => "https://rybalka.ua/p/silikonovyy-cherv%D1%8C-fox-10sm-crawler-5-s%D1%8Aedobnyy-6sht-cgovir/",
		"PICTURE_ALT" => "Силиконовые  черви Fox Crawler"
	),
	false
); ?>
		</div>
	</div>
</section>
<? $APPLICATION->IncludeComponent(
	"bfs:catalog.filtered.carousel",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT" => "16",
		"FILTER" => "{\"!PROPERTY_RECOMMENDED\":false}",
		"SORT_FIELD" => "PROPERTY_RECOMMENDED",
		"SORT_FIELD2" => "RAND",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => "ASC",
		"TITLE" => "Мы рекомендуем",
		"LINK_TO_ALL" => "/catalog/recommended/",
		"HIDE_NOT_AVAILABLE" => "Y"
	),
	false
); ?>
<? $APPLICATION->IncludeComponent(
	"bfs:catalog.filtered.carousel",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT" => "16",
		"FILTER" => "{\"PROPERTY_PROMOTIONAL\":\"Y\"}",
		"SORT_FIELD" => "RAND",
		"SORT_FIELD2" => "",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => "",
		"TITLE" => "Акционные товары",
		"LINK_TO_ALL" => "/catalog/promotional/",
		"HIDE_NOT_AVAILABLE" => "Y"
	),
	false
); ?>
<?
$daysToAssumeAsNew = \Bitrix\Main\Config\Option::get('grain.customsettings', 'PRODUCT_NEW_PERIOD');
$lastTimestampForNew = AddToTimeStamp(['DD' => -$daysToAssumeAsNew], MakeTimeStamp(new DateTime));
$lastDayForNew = ConvertTimeStamp($lastTimestampForNew, 'SHORT');

$APPLICATION->IncludeComponent(
	"bfs:catalog.filtered.carousel",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT" => "16",
		"FILTER" => "{\">=DATE_CREATE\":\"" . $lastDayForNew . "\"}",
		"SORT_FIELD" => "RAND",
		"SORT_FIELD2" => "",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => "",
		"TITLE" => "Новые товары",
		"LINK_TO_ALL" => "/catalog/new/",
		"HIDE_NOT_AVAILABLE" => "Y"
	),
	false
); ?>
<? $APPLICATION->IncludeComponent(
	"bfs:universal.block", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"VIEW" => "COL2",
		"BANNER_OR_CAROUSEL" => "BANNER",
		"PICTURE" => "/upload/medialibrary/357/357d7978de869f368d36bf28707888bb.jpg",
		"LINK" => "https://rybalka.ua/p/katushka-piscifun-viper-ii-2000/",
		"PRODUCTS" => array(
			0 => "7273",
			1 => "8086",
			2 => "",
		),
		"TITLE" => "В тренде",
		"SECTION_ID" => "198",
		"PICTURE_ALT" => "Piscifun Viper II -15%"
	),
	false
); ?>
<? $APPLICATION->IncludeComponent(
	"bfs:tackle.reviews.carousel", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT" => "16",
		"FILTER" => "{}",
		"SORT_FIELD" => "DATE_CREATE",
		"SORT_FIELD2" => "",
		"SORT_ORDER" => "DESC",
		"SORT_ORDER2" => "",
		"TITLE" => "Обзоры и тесты",
		"LINK_TO_ALL" => "/tackle-reviews/"
	),
	false
); ?>
<? $APPLICATION->IncludeComponent("bfs:search.phrases.block", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COUNT" => "20",
		"TITLE" => "Что ищут"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>